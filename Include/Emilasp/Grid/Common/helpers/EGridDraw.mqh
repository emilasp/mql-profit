//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\libraries\Vendors\comment.mqh>

#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>

#include <Emilasp\helpers\DrawHelper.mqh>
//#include <Emilasp\Grid\Common\EGrid.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridDraw
{
private:
   CComment          comment;
public:
                     EGridDraw::EGridDraw();

   void              drawOrder(int gridId, EGridOrder * order, ENUM_POSITION_TYPE direct);
   void              drawGridInfoPanel(string &lines[], int index);
   void              drawGridInfo(string &lines[], int index);
   void              clearGridObjects(int gridId);
   void              drawSignal(int gridId, MqlTick &tick, ENUM_POSITION_TYPE direct);
   void              drawLine(int gridId, string name,double price,int lineColor,ENUM_LINE_STYLE lineStyle=2);
   void              drawArrow(int gridId, string name, double price, datetime time, int iconCode, int clr, int chartId = 0, int windowId = 0);
   string            getGridPrefixName(int gridId);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridDraw::EGridDraw()
{

}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDraw::drawOrder(int gridId, EGridOrder * order, ENUM_POSITION_TYPE direct)
{
   string name = getGridPrefixName(gridId) + IntegerToString(direct) + ":" + IntegerToString(order.level);
   if(order.isVirtual) {
      if(direct == POSITION_TYPE_BUY)
         DrawHelper::moveHorizontalLine(name, order.tradeData.priceOpen, clrBlueViolet, STYLE_DASH);
      else
         DrawHelper::moveHorizontalLine(name, order.tradeData.priceOpen, clrRosyBrown, STYLE_DASH);
   }
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDraw::drawGridInfoPanel(string &lines[], int index)
{
   DrawHelper::drawInfoPanel(comment,  getGridPrefixName(index) + "info", lines, index, 20, index * 100);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDraw::drawGridInfo(string &lines[], int index)
{
   int baseTop = 20 * (index + 1);
   int total = ArraySize(lines);
   for(int i=0; i<total; i++) {
      DrawHelper::drawLabel(getGridPrefixName(index) + "info:fast:" + IntegerToString(i),lines[i], 10, baseTop + i * 20);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDraw::clearGridObjects(int gridId)
{
   int TotalObject = ObjectsTotal(0);

   for(int i=0; i<TotalObject; i++) {
      string name = ObjectName(0, i, 0);
      string nameToDelete = getGridPrefixName(gridId);
      
      int find = StringFind(name, nameToDelete, 0);
      if (StringFind(name, nameToDelete, 0) >= 0)
       ObjectDelete(0, name);
   }
}
void EGridDraw::drawSignal(int gridId, MqlTick &tick, ENUM_POSITION_TYPE direct)
{
   int icon = direct == POSITION_TYPE_BUY ? 233 : 234;

   DrawHelper::drawArrow(getGridPrefixName(gridId) + tick.time,tick.time,tick.ask,159,clrGold, 0, 0, 28);
}

void EGridDraw::drawArrow(int gridId, string name, double price, datetime time, int iconCode, int clr, int chartId = 0, int windowId = 0)
{
   DrawHelper::drawArrow(getGridPrefixName(gridId) + name,time,price,iconCode,clr, chartId, windowId, 28);
}


void EGridDraw::drawLine(int gridId, string name,double price,int lineColor,ENUM_LINE_STYLE lineStyle=2)
{
    DrawHelper::moveHorizontalLine(getGridPrefixName(gridId) + name,price,lineColor, lineStyle);
}
string EGridDraw::getGridPrefixName(int gridId)
{
   return "grid:" + IntegerToString(gridId) + ":";
}

//+------------------------------------------------------------------+
