//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>
#include <Math\Stat\Math.mqh>

#include <Generic\HashMap.mqh>
#include <Arrays\Array.mqh>
#include <Arrays\ArrayString.mqh>
#include <Arrays\ArrayObj.mqh>

#include <Emilasp\Grid\Common\InfoPanel.mqh>

#include <Emilasp\Grid\Common\helpers\EGridDraw.mqh>

#include <Emilasp\Grid\Common\EGridDataProcessor.mqh>

#include <Emilasp\Grid\Common\base\EGridStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridSignalStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridProfitStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridPiramidingStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridTradeStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridLotStrategy.mqh>

#include <Emilasp\Grid\Common\EGridStatistics.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGrid : public CObject
{
private:


public:
   int               id;
   int               magic;

   ENUM_POSITION_TYPE positionType;

   CArrayObj         *orders;
   CArrayObj         *ordersPiramiding;
   CArrayObj         *ordersFinished;
   MqlTick           lastTick;

   CTradePanel       TradePanel;
   
   EGridDraw          *drawer;

   EGridStatistics   *statistics;
   EGridDataProcessor *dataProcessor;

   bool              isActive;


   EGridTradeStrategy      *tradeStrategy;
   EGridStrategy           *gridStrategy;
   EGridSignalStrategy     *signalStrategy;
   EGridProfitStrategy     *profitStrategy;
   EGridPiramidingStrategy *piramidingStrategy;
   EGridLotStrategy        *lotStrategy;

   void             ~EGrid() {};
   void              EGrid(ENUM_POSITION_TYPE _positionType, int _spredMax, int deviationMax);
   void              initStrategy(EGridStrategyAbstract *strategy);
   // Events
   void              onInit();
   void              onTick();
   void              onNewBar();
   void              reset();
   void              OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);
   void              OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam);
   
   void              setGridStrategy(EGridStrategy *_strategy)
   {
      gridStrategy = _strategy;
   };
   void              setGridSignalStrategy(EGridSignalStrategy *_strategy)
   {
      signalStrategy = _strategy;
   };
   void              setGridProfitStrategy(EGridProfitStrategy *_strategy)
   {
      profitStrategy = _strategy;
   };
   void              setGridPiramidingStrategy(EGridPiramidingStrategy *_strategy)
   {
      piramidingStrategy = _strategy;
   };
   void              setGridLotStrategy(EGridLotStrategy *_strategy)
   {
      lotStrategy = _strategy;
   };

   void              setTradeStrategy(EGridTradeStrategy *_strategy)
   {
      tradeStrategy = _strategy;
   };

   void              setGridTradeStrategy(EGridTradeStrategy *_tradeStrategy)
   {
      tradeStrategy = _tradeStrategy;
   };

   void              setNotActiveGridByOpenPositions();
   void              startGrid();
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              EGrid::EGrid(ENUM_POSITION_TYPE _positionType, int _spredMax, int deviationMax)
{
   Print("Grider: constructor()");

   positionType=_positionType;
   drawer = new EGridDraw();

   orders = new CArrayObj();
   ordersPiramiding = new CArrayObj();
   ordersFinished = new CArrayObj();
}

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::initStrategy(EGridStrategyAbstract *strategy)
{
   strategy.magic = magic;
   strategy.gridId = id;
   strategy.positionType = positionType;
   strategy.drawer = drawer;
   strategy.orders = orders;
   strategy.ordersPiramiding = ordersPiramiding;
   strategy.ordersFinished = ordersFinished;

   strategy.statistics = statistics;
   strategy.dataProcessor = dataProcessor;
   
   strategy.onInit();
}

/** EVENTS **/
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::onInit()
{
   Print("Grider: onInit()");

// Init Strategies
   statistics = new EGridStatistics(magic, id, positionType, drawer, orders, ordersFinished);
   statistics.onInit();

   initStrategy(tradeStrategy);
   initStrategy(gridStrategy);
   initStrategy(signalStrategy);
   initStrategy(piramidingStrategy);
   initStrategy(lotStrategy);
   
   gridStrategy.lotStrategy = lotStrategy;
   gridStrategy.tradeStrategy = tradeStrategy;
   
   piramidingStrategy.lotStrategy = lotStrategy;
   piramidingStrategy.tradeStrategy = tradeStrategy;
   
//---
// Create Trade Panel
   TradePanel.Create(ChartID(), 0, id, "Trade Panel", statistics, 20, 20, 200, 200);
   TradePanel.Run();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::onTick()
{
   SymbolInfoTick(_Symbol, lastTick);

   tradeStrategy.onTick(lastTick);
   gridStrategy.onTick(lastTick);
   piramidingStrategy.onTick(lastTick);
   lotStrategy.onTick(lastTick);

   if (tradeStrategy.trade.isNewBar(1) ) {
      tradeStrategy.onOnNewBar();
      gridStrategy.onOnNewBar();
      signalStrategy.onOnNewBar();
      piramidingStrategy.onOnNewBar();
      lotStrategy.onOnNewBar();

      if(!isActive && orders.Total() == 0) {
         if (signalStrategy.isSignal(gridStrategy.positionType)) {
            startGrid();
         }
      }
   }

   if(isActive) {
      ///////// TODO
      setNotActiveGridByOpenPositions();
   
      if(gridStrategy.isNextLevelTouch()) {
         gridStrategy.tradeNextOrder();
      }
   
      // Pirammising
      if(orders.Total() == 1) {
         if(piramidingStrategy.isNextLevelTouch()) {
            piramidingStrategy.tradeNextOrder();
         }
      }
   }

   statistics.onTick();
   TradePanel.onTick();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::startGrid()
{
   Print("Grider: startGrid(), isActive=" + isActive);

   if (gridStrategy.tradeNextOrder()) {
      isActive = true;

      drawer.drawSignal(id, lastTick, gridStrategy.positionType);
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::setNotActiveGridByOpenPositions()
{
//Print("Grider: setNotActiveGridByopenPositions()");

   int countOpenPositions = tradeStrategy.trade.getPositionCount(gridStrategy.positionType);
   if(countOpenPositions == 0) {
      reset();
      tradeStrategy.updateStatByOrders(true);
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::onNewBar()
{
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void              EGrid::reset()
{
   Print("Grider: reset()");

   for(int i=0; i<orders.Total(); i++) {
      EGridOrder * order = orders.Detach(i);
      order.isClosed = true;
      ordersFinished.Add(order);
   }
   orders.Clear();

   for(int i=0; i<ordersPiramiding.Total(); i++) {
      EGridOrder * order = ordersPiramiding.Detach(i);
      order.isClosed = true;
      ordersFinished.Add(order);
   }
   ordersPiramiding.Clear();

   ordersFinished.Clear();
   
   tradeStrategy.onReset();
   gridStrategy.onReset();
   signalStrategy.onReset();
   //profitStrategy.onReset();
   piramidingStrategy.onReset();
   lotStrategy.onReset();

   if(isActive) {
      Print("Grider: reset(): isActive = false");
      isActive = false;
      drawer.clearGridObjects(id);
   }

   statistics.onReset();
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGrid::OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   statistics.OnTradeTransaction(trans, request, result);
}
void EGrid::OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   //TradePanel.ChartEvent(id,lparam,dparam,sparam);
}



//+------------------------------------------------------------------+
