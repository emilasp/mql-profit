//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>


enum ENUM_LOT_STRATEGY
  {   
      LOT_STRATEGY_BASE_LOT = 0,
      LOT_STRATEGY_MARTINGALE = 1,
      LOT_STRATEGY_FIBO = 2,
      LOT_STRATEGY_ARIFMETIK = 3,
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridLotStrategy : public EGridStrategyAbstract {
 private:
 protected:
    double lotBase;
    
    ENUM_LOT_STRATEGY lotStrategy;
    
 public:
      EGridLotStrategy::EGridLotStrategy();

      // Events
      void onInit();
      void onTick(MqlTick &_lastTick);
      void onOnNewBar();
      void onReset();
      
      virtual double getLot(EGridOrder *order) {return lotBase;};
};

EGridLotStrategy::EGridLotStrategy()
{
}



void EGridLotStrategy::onInit()
{
}
void EGridLotStrategy::onTick(MqlTick &_lastTick)
{
}
void EGridLotStrategy::onOnNewBar()
{
}
void EGridLotStrategy::onReset()
{
}