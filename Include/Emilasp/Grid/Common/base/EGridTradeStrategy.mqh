//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Trade\SymbolInfo.mqh>

#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>
#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>

#include <Emilasp\Grid\Common\EGridOrder.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridTradeStrategy : public EGridStrategyAbstract {
private:
protected:
   int deviation;
   int spredMax;
  
   bool              updateStatOrderTrade(EGridOrder *order, int index);
   
public:
   CoreTradeHadge    *trade;
   
   double priceZeroForAllOrders;
   double priceZeroForLastOrder;

   EGridTradeStrategy::EGridTradeStrategy(int _deviation, int _spredMax);
   
      // Events
   void              onInit();
   void              onTick(MqlTick &_lastTick);
   void              onOnNewBar();
   void              onReset();
   // Grid

   
   bool              tradeOrder(EGridOrder *order, double lot, int sl = 5000);

   bool              updateStatByOrders(bool hardUpdate);
   bool              finishGrid();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridTradeStrategy::EGridTradeStrategy(int _deviation, int _spredMax) {
   deviation = _deviation;
   spredMax = _spredMax;
   
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic, deviation);

   trade.interruptionOfLossesInit(2, 30);
}

bool EGridTradeStrategy::tradeOrder(EGridOrder *order, double lot, int sl = 5000) {
   if(spredMax < statistics.stats.spred && statistics.stats.spred > 0) {
      Print("Grider: Запрещаем торговлю так как высокий спред!");
      return false;
   }

   // PRE SetUp order
   order.tradeData.dateOpen  = lastTick.time;
   order.tradeData.priceOpen = isBuyType() ? lastTick.bid : lastTick.ask;
   order.lot = lot;
   
   string orderID = order.getGeneratedID(magic, positionType, lastTick);

   ulong ticketFinded = -1;
   bool isTradeSuccess = false;
   
   double marginForOrder;
   OrderCalcMargin(isBuyType() ? ORDER_TYPE_BUY : ORDER_TYPE_SELL, _Symbol, order.lot, SymbolInfoDouble(_Symbol, SYMBOL_ASK), marginForOrder);
   double freeMargin = AccountInfoDouble(ACCOUNT_FREEMARGIN);
   if(freeMargin < marginForOrder) {
         Print("Закончилась свободная маржа.. нет возможности открывать оредра!");
         return false;
   }
   
   if(isBuyType())
      isTradeSuccess = trade.Buy(order.lot, sl, sl, orderID);
   if(isSellType())
      isTradeSuccess = trade.Sell(order.lot, sl, sl, orderID);


   if(orderID != NULL && isTradeSuccess) {
      int total=PositionsTotal();
      for(int i=0; i<total; i++) {
         ulong ticket=PositionGetTicket(i);
         if(PositionSelectByTicket(ticket))
            if(PositionGetInteger(POSITION_MAGIC)==trade.MagicNumber)
               if(PositionGetString(POSITION_SYMBOL) == _Symbol) {
                  string comment = PositionGetString(POSITION_COMMENT);
                  if(orderID == comment) {
                     order.ticket = ticket;
                     //order.tradeData.priceOpen =  PositionGetDouble(POSITION_PRICE_OPEN);
                     order.tradeData.swap =  PositionGetDouble(POSITION_SWAP);
                     break;
                  }
               }
      }
   }
   
   updateStatByOrders(true);
   
   return true;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridTradeStrategy::updateStatOrderTrade(EGridOrder *order, int index)
{
   if(order.ticket == 0)
      Print("Не удалось обновить статистику у ордера: отсутствует тикет! index=" + IntegerToString(index));

   if(!order.isClosed) {
      if(trade.m_Position.SelectByTicket(order.ticket)) {
         order.tradeData.profit = trade.m_Position.Profit();
         order.tradeData.commission = trade.m_Position.Commission();
         //order.tradeData.priceOpen = trade.m_Position.PriceOpen();
         order.tradeData.swap = trade.m_Position.Swap();

         return true;
      } else {
         //Print("Не удалось обновить статистику у ордера: SelectByTicket(order.ticket) не сработал!! index=" + IntegerToString(index));
      }
   } else {
      if(!order.isStatUpdated) {
         //Print("Order NOT UYPDATE STAT" + DoubleToString(order.tradeData.priceClose));
      }
      return true;
   }

   //Print("Не удалось обновить статистику у ордера: SelectByTicket == false!! index=" + IntegerToString(index));
   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridTradeStrategy::updateStatByOrders(bool hardUpdate)
{
   statistics.updateBaseStats();

   double openOrdersSum = 0;
   statistics.stats.openOrdersLots = 0;
   statistics.stats.openOrders = 0;

   int total = orders.Total();
   for(int i=0; i<total; i++) {
      EGridOrder *order = orders.At(i);
      if(order == NULL)
         continue;
      
      if(hardUpdate || !order.isStatUpdated) {
         updateStatOrderTrade(order, i);
      }

      statistics.stats.addTrade(order);

      if(!order.isClosed) {
         statistics.stats.openOrders += 1;
         statistics.stats.openOrdersLots += order.lot;
         
         double spred = (isBuyType() ? statistics.stats.spred : -1 * statistics.stats.spred) * _Point;
         openOrdersSum += (order.tradeData.priceOpen + spred) * order.lot;
         
         priceZeroForLastOrder = order.tradeData.priceOpen * order.lot;
      }
   }

   if(total > 0) {
      priceZeroForAllOrders = openOrdersSum / statistics.stats.openOrdersLots;
      
      statistics.stats.currentAvgZero = priceZeroForAllOrders;
      
      drawer.drawLine(gridId, "currentAvgZero", statistics.stats.currentAvgZero, clrBlue, STYLE_DOT);
   }

   return true;
}

//+------------------------------------------------------------------+
bool EGridTradeStrategy::finishGrid() {
   if(trade.closePositions(positionType)) {
      return true;
   }
   return false;
}
void EGridTradeStrategy::onInit() {
}

void EGridTradeStrategy::onTick(MqlTick &_lastTick) {
   lastTick=_lastTick;
   updateStatByOrders(false);
}

void EGridTradeStrategy::onOnNewBar() {
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridTradeStrategy::onReset() {
}
//+------------------------------------------------------------------+
