//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>
#include <Emilasp\Signals\Manager\SignalManager.mqh>
#include <Emilasp\Signals\Base\ASignalBase.mqh>

#include <Emilasp\Grid\Common\base\EGridTradeStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridLotStrategy.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridPiramidingStrategy : public EGridStrategyAbstract
{
private:
protected:
   SignalManager     *signalManager;

   bool              enabled;

   double            lotBase;
   int               levelHeightPt;
   int               breakEven; // Безубыток в пунктах

   bool trailingSlByAtrEnable;
   double trailingSlByAtrKoef;

   double            trailingStopPrice;

   int                 getTrailingStopDistanceBase();
   double              getNextLevelDistanceByStrategy();
   double              getNextLevelPrice();
   
public:
   EGridTradeStrategy *tradeStrategy;
   EGridLotStrategy   *lotStrategy;

                     EGridPiramidingStrategy::EGridPiramidingStrategy();

   // Events
   void              onInit();
   void              onTick(MqlTick &_lastTick);
   void              onOnNewBar();
   void              onReset();
   ///
   EGridOrder        *getLastOrder();

   bool                isNextLevelTouch();
   bool                needFinishGrid();

   double            getTrailingPriceCurrent();
   void              trailingStopProcess();
   bool              tradeNextOrder();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridPiramidingStrategy::EGridPiramidingStrategy()
{
   signalManager = new SignalManager();

   enabled = true;
   trailingStopPrice=0;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridOrder *EGridPiramidingStrategy::getLastOrder()
{
   EGridOrder *lastOrder;
   if(ordersPiramiding.Total() == 0) {
      lastOrder = orders.At(orders.Total()-1);
   } else {
      lastOrder = ordersPiramiding.At(ordersPiramiding.Total()-1);
   }
   return lastOrder;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridPiramidingStrategy::isNextLevelTouch()
{
   if(isBuyType()) {
      return lastTick.bid > getNextLevelPrice();
   } else {
      return lastTick.ask < getNextLevelPrice();
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridPiramidingStrategy::needFinishGrid()
{
   tradeStrategy.finishGrid();
   return true;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridPiramidingStrategy::tradeNextOrder()
{
   Print("Grider: tradeVirtualOrder()");

   EGridOrder *order = new EGridOrder(ordersPiramiding.Total());

   if (tradeStrategy.tradeOrder(order, lotBase)) { // lotStrategy.getLot(order)
      ordersPiramiding.Add(order);

      tradeStrategy.updateStatByOrders(true);

      drawer.drawLine(gridId, "nextLevelPiram", getNextLevelPrice(), clrCoral, STYLE_DASHDOTDOT);
      return true;
   }

   Print("Ошибка: При создании ордера не удалось получить тикет");

   return false;
}

int EGridPiramidingStrategy::getTrailingStopDistanceBase()
{
   int distance = levelHeightPt;
   if(trailingSlByAtrEnable) {
      dataProcessor.getSlAtr(PERIOD_CURRENT, 14, trailingSlByAtrKoef, 15);
   }
   return distance;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridPiramidingStrategy::getNextLevelDistanceByStrategy()
{
   return levelHeightPt * _Point;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridPiramidingStrategy::getNextLevelPrice()
{
   EGridOrder * lastOrder = getLastOrder();

   double distance = getNextLevelDistanceByStrategy();

   if(isBuyType()) {
      return lastOrder.tradeData.priceOpen + distance;
   } else {
      return lastOrder.tradeData.priceOpen - distance;
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridPiramidingStrategy::getTrailingPriceCurrent()
{
   EGridOrder *lastOpenOrder = ordersPiramiding.At(ordersPiramiding.Total() - 1);
   if(isBuyType()) {
      double trailPrice = lastTick.bid - getTrailingStopDistanceBase() * _Point;
      if(trailPrice > trailingStopPrice) {
         trailingStopPrice = trailPrice;
      }
   }
   if (isSellType() || trailingStopPrice == 0) {
      double trailPrice = lastTick.ask + getTrailingStopDistanceBase() * _Point;
      if (trailPrice < trailingStopPrice || trailingStopPrice == 0) {
        trailingStopPrice = trailPrice;
      }
   }
   return trailingStopPrice;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridPiramidingStrategy::trailingStopProcess()
{
   if(ordersPiramiding.Total() > 0 && statistics.stats.equityGrid > 0) {
      EGridOrder *lastOpenOrder = ordersPiramiding.At(ordersPiramiding.Total() - 1);

      if(enabled && lastOpenOrder != NULL) {

         double tralStartPrice = lastOpenOrder.tradeData.priceOpen;
         double tralCurrentPrice = getTrailingPriceCurrent();

         if(tralCurrentPrice > 0) {
            drawer.drawLine(gridId, "tral:piramid:current", tralCurrentPrice, clrYellow, STYLE_DOT);

            if(isBuyType()) {
               if(lastTick.bid < trailingStopPrice) {
                  needFinishGrid();
               }
            }
            if (isSellType()) {
               if (lastTick.ask > trailingStopPrice) {
                  needFinishGrid();
               }
            }
         }
      }
   } else if (orders.Total() == 1 && ordersPiramiding.Total() == 0) {
       drawer.drawLine(gridId, "nextLevelPiram", getNextLevelPrice(), clrCoral, STYLE_DASHDOTDOT);
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridPiramidingStrategy::onInit()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridPiramidingStrategy::onTick(MqlTick &_lastTick)
{
   lastTick = _lastTick;

   trailingStopProcess();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridPiramidingStrategy::onOnNewBar()
{
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridPiramidingStrategy::onReset()
{
// Add orders to grid.ordersFinished
   ordersPiramiding.Clear();
   trailingStopPrice=0;
}
//+------------------------------------------------------------------+
