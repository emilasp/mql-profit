//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridProfitStrategy : public EGridStrategyAbstract
{
private:
protected:
   bool              trailingVirtualEnabled;
   int               trailingVirtualStartDistance;
   int               virtualStopBUtoTralMinDistance;

   double            trailingVirtualDynamicEquityKoef;

   int               equityLossClose;
   int               equityAvgClose;

   double            trailingVirtualPriceStop;

   int               virtualStopPriceDistanceLevel;
   double            virtualStopPriceDistanceLevelKoef;

   double            getVirtualStopPrice();

public:
   int               lastTradeLevel;

                     EGridProfitStrategy::EGridProfitStrategy();

   // Events
   void              onInit();
   void              onTick(MqlTick &_lastTick);
   void              onOnNewBar();
   void              onReset();

   void              trailingStopVirtualProcess();

   bool              needFinishGridByVirtualStop();
   void              calculateStats();

   virtual bool      needFinishGrid()
   {
      return false;
   };
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridProfitStrategy::EGridProfitStrategy()
{
   trailingVirtualEnabled = true;
   lastTradeLevel=0;
   trailingVirtualPriceStop=0;
   trailingVirtualDynamicEquityKoef = 1;
   virtualStopPriceDistanceLevel = 200;
   virtualStopPriceDistanceLevelKoef = 0.3;
   virtualStopBUtoTralMinDistance=10;
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::onInit()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::onTick(MqlTick &_lastTick)
{
   lastTick = _lastTick;

   //calculateStats();
   //trailingStopVirtualProcess();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::onOnNewBar()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::trailingStopVirtualProcess()
{
   /**if(trailingVirtualEnabled && getCountOpenOrders() > 0 && ordersPiramiding.Total() == 0) {
      double currentPrice = trader.trade.getTickPriceBidOrAskForTrade(positionType, lastTick);

      // Берем цену точки безуьыточности и добавляем n пунктов * level || n * lot от последнего уровня
      // Смысл рассчитать меньший откат для больших уровней

      double stopPrice = getVirtualStopPrice();
      
      if(isBuyType())
          DrawHelper::moveHorizontalLine(drawer.getGridPrefixName(gridId) + IntegerToString(gridId) + "stopPrice", stopPrice + (statistics.stats.spred + virtualStopBUtoTralMinDistance) * _Point, clrCoral, STYLE_DASH);
      else 
         DrawHelper::moveHorizontalLine(drawer.getGridPrefixName(gridId) + IntegerToString(gridId) + "stopPrice", stopPrice - (statistics.stats.spred + virtualStopBUtoTralMinDistance) * _Point, clrCoral, STYLE_DASH);
         
      
      if(isBuyType() && currentPrice >= stopPrice) {
         int pointsStop = (int)((stopPrice - statistics.stats.currentAvgZero) / _Point);
         double newPriceStop = currentPrice - (pointsStop + statistics.stats.spred + virtualStopBUtoTralMinDistance) * _Point;

         if((trailingVirtualPriceStop < newPriceStop || trailingVirtualPriceStop == 0) && newPriceStop > statistics.stats.currentAvgZero)
            trailingVirtualPriceStop = newPriceStop;
      }

      if(isSellType() && currentPrice <= stopPrice) {
         int pointsStop = (int)((statistics.stats.currentAvgZero - stopPrice) / _Point);
         double newPriceStop = currentPrice + (pointsStop + statistics.stats.spred + virtualStopBUtoTralMinDistance) * _Point;

         if((trailingVirtualPriceStop > newPriceStop || trailingVirtualPriceStop == 0) && newPriceStop < statistics.stats.currentAvgZero)
            trailingVirtualPriceStop = newPriceStop;
      }

      DrawHelper::moveHorizontalLine(drawer.getGridPrefixName(gridId) + IntegerToString(gridId), trailingVirtualPriceStop, clrBlueViolet, STYLE_DASH);
   }*/
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridProfitStrategy::getVirtualStopPrice()
{
   EGridOrder *lastOrder = getLastOpenOrder();

   double distance = (virtualStopPriceDistanceLevel / (lastOrder.level + 1) / virtualStopPriceDistanceLevelKoef) * _Point;

   if(isBuyType())
      statistics.stats.priceAvgCloseMoment = statistics.stats.currentAvgZero + distance;
   else
      statistics.stats.priceAvgCloseMoment = statistics.stats.currentAvgZero - distance;

   return statistics.stats.priceAvgCloseMoment;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridProfitStrategy::needFinishGridByVirtualStop()
{
   /**if(trailingVirtualEnabled && getCountOpenOrders() > 0) {
      double currentPrice = trad.trade.getTickPriceBidOrAskForTrade(positionType, lastTick);
      double stopPrice = getVirtualStopPrice();

      if(trailingVirtualPriceStop > 0) {
         bool closePositions = false;
         if (isBuyType() && trailingVirtualPriceStop >  currentPrice)
            return true;
         if (isSellType() && trailingVirtualPriceStop <  currentPrice)
            return true;
      }
   }*/
   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::calculateStats()
{
   double openOrdersSum = 0;
   statistics.stats.openOrdersLots = 0;
   statistics.stats.openOrders = 0;

   int total = orders.Total();
   for(int i=0; i<total; i++) {
      EGridOrder *order = orders.At(i);
      if(!order.isClosed) {
         statistics.stats.openOrders += 1;
         statistics.stats.openOrdersLots += order.lot;
         openOrdersSum += order.tradeData.priceOpen * order.lot; // equity
      }
   }

   if(statistics.stats.openOrdersLots > 0) {
      statistics.stats.currentAvgZero = openOrdersSum / statistics.stats.openOrdersLots;

      drawer.drawLine(gridId, "currentAvgZero", statistics.stats.currentAvgZero, clrBlue, STYLE_DOT);
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridProfitStrategy::onReset()
{
   lastTradeLevel = 0;
   trailingVirtualPriceStop = 0;
}
//+------------------------------------------------------------------+
