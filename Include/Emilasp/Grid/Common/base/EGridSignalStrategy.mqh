//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>
#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>
#include <Emilasp\Signals\Manager\SignalManager.mqh>
#include <Emilasp\Signals\Base\ASignalBase.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridSignalStrategy : public EGridStrategyAbstract {
private:
   
protected:
   bool debugIsSignal;
   SignalManager *signalManager;
   
   
public:
      EGridSignalStrategy::EGridSignalStrategy();
      
      void addSignal(ASignalBase *signal);
      bool isSignal(ENUM_POSITION_TYPE direct);
};

EGridSignalStrategy::EGridSignalStrategy()
{
   signalManager = new SignalManager();
   debugIsSignal = false;
}

void EGridSignalStrategy::addSignal(ASignalBase *signal)
{
   signalManager.addSignal(signal, ENUM_SIGNAL_CONCATENATE_TYPE_AND);
}

bool EGridSignalStrategy::isSignal(ENUM_POSITION_TYPE direct)
{ 
   if(debugIsSignal)
      return true;
      
   SignalResult signalResult = signalManager.getSignalResult();
   if(direct == POSITION_TYPE_BUY && signalResult.buy)
     return true;
     
   if(direct == POSITION_TYPE_SELL && signalResult.sell)
     return true;
     
   return false;
}


