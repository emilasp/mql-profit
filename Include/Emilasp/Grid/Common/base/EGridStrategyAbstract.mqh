//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>

#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>
#include <Emilasp\Grid\Common\helpers\EGridDraw.mqh>
#include <Emilasp\Grid\Common\EGridStatistics.mqh>

#include <Emilasp\Grid\Common\EGridDataProcessor.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridStrategyAbstract {
private:
protected:
   int getCountOpenOrders();
   double getLotSumOpenOrders();
   EGridOrder *getLastOpenOrder();
   
public:
   int               magic;
   int               gridId;
   MqlTick           lastTick;
   EGridDraw         *drawer;


   CArrayObj         *orders;
   CArrayObj         *ordersPiramiding;  
   CArrayObj         *ordersFinished;

   EGridStatistics   *statistics;
   EGridDataProcessor *dataProcessor;

   ENUM_POSITION_TYPE  positionType;

   EGridStrategyAbstract::EGridStrategyAbstract();

   // Events
   virtual void              onInit() {};
   virtual void              onTick(MqlTick &_lastTick) {};
   virtual void              onOnNewBar() {};
   virtual void              onReset() {};

   bool                      isBuyType();
   bool                      isSellType();
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridStrategyAbstract::EGridStrategyAbstract() {
}
//+------------------------------------------------------------------+
bool EGridStrategyAbstract::isBuyType() {
   return positionType == POSITION_TYPE_BUY;
}
//+------------------------------------------------------------------+
bool EGridStrategyAbstract::isSellType() {
   return positionType == POSITION_TYPE_SELL;
}
//+------------------------------------------------------------------+
int EGridStrategyAbstract::getCountOpenOrders() {
   return orders.Total();
}

double EGridStrategyAbstract::getLotSumOpenOrders() {
   double lot = 0;
   int total = orders.Total();
   for(int i=0; i<total; i++) {
      EGridOrder *order = orders.At(i);
      lot += order.lot;
   }
   return lot;
}

EGridOrder *EGridStrategyAbstract::getLastOpenOrder() {
   EGridOrder *order;
   int levels = orders.Total();
   
   if(levels > 0)
      order = orders.At(levels - 1);
   return order;
}


//+------------------------------------------------------------------+
