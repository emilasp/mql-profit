//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>

#include <Emilasp\Grid\Common\base\EGridStrategyAbstract.mqh>
#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>
#include <Emilasp\Grid\Common\base\EGridTradeStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridLotStrategy.mqh>


enum ENUM_FILTER_SIGNAL
  {
      FILTER_SIGNAL_NONE = 0,
      FILTER_SIGNAL_RSI_1 = 1,
      FILTER_SIGNAL_ELINES = 2
  };

//+------------------------------------------------------------------++
//| Класс отвечает за работу самой сетки - глобально:                 |
//| 1. Построение уровней                                             |
//| 2. Создание ордеров                                               |
//| 3. Отслеживание ордеров                                           |
//+------------------------------------------------------------------++
class EGridStrategy : public EGridStrategyAbstract
{
private:
protected:
   ENUM_FILTER_SIGNAL filterSignal;

   double            getNextLevelDistanceByStrategy();
   void              trailingStopProcess();
   
   // Eperimental
   bool isFilterStop();
   int filterSignalHandler;
   
protected:
   int               levels;
   int               levelHeightPt;
   int               minBarsFromLastOrder;
   bool              showGridVirtualLevel;

   double            trailingStopPrice;

   int               trailingStopDistanceLevelBase; // Базовая дистанция между безубытком и тралом
   int               trailingStopMinDistanceFromBu; // Добавленная минимальная дистанция в пт
   double            trailingStopDynamicEquityKoef; // Коэффициент уменьшения дистанции до трала

   bool trailingSlByAtrEnable;
   double trailingSlByAtrKoef;
   
   int               getTrailingStopDistanceBase();
   double            getTrailingStartPrice(); // Получаем цену от которой заработает трал
   double            getTrailingPriceCurrent(); // Текущаяя цена трала

public:
   EGridTradeStrategy *tradeStrategy;
   EGridLotStrategy   *lotStrategy;

                     EGridStrategy::EGridStrategy();

   // Events
   void              onInit();
   void              onTick(MqlTick &_lastTick);
   void              onOnNewBar();
   void              onReset();
   // Grid
   double              getNextLevelPrice();
   bool                isNextLevelTouch();
   bool                needFinishGrid();

   //double              getPriceLevelByByTick(int level, MqlTick &_lastTick);
   // Trade
   bool              tradeNextOrder();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridStrategy::EGridStrategy()
{
   minBarsFromLastOrder = 0;
   filterSignalHandler=-1;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStrategy::trailingStopProcess()
{
   if(orders.Total() > 1) {
      double tralStartPrice = getTrailingStartPrice();
      double tralCurrentPrice = getTrailingPriceCurrent();

      if((isBuyType() && tralCurrentPrice > tralStartPrice) || (isSellType() && tralCurrentPrice < tralStartPrice)) {
         drawer.drawLine(gridId, "tral:start", tralStartPrice, clrAqua, STYLE_DASHDOTDOT);
      }
      drawer.drawLine(gridId, "tral:current", tralCurrentPrice, clrAntiqueWhite, STYLE_DASHDOTDOT);

      if(tralCurrentPrice > 0) {
         if(isBuyType() && lastTick.bid < tralCurrentPrice) {
            needFinishGrid();
         }
         if (isSellType() && lastTick.ask > tralCurrentPrice) {
            needFinishGrid();
         }
      }
   }
}

int EGridStrategy::getTrailingStopDistanceBase()
{
   int distance = trailingStopDistanceLevelBase;
   if(trailingSlByAtrEnable) {
      dataProcessor.getSlAtr(PERIOD_CURRENT, 14, trailingSlByAtrKoef, 15);
   }
   return distance;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridStrategy::getTrailingStartPrice()
{
   EGridOrder *lastOrder = getLastOpenOrder();

   double lot = lotStrategy.getLot(lastOrder);

   double distance = (getTrailingStopDistanceBase() / (lastOrder.level + 1) / trailingStopDynamicEquityKoef) * _Point;

   double startPrice = isBuyType() ? tradeStrategy.priceZeroForAllOrders + distance : tradeStrategy.priceZeroForAllOrders - distance;
   return startPrice;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridStrategy::getTrailingPriceCurrent()
{
   double trailingStartPrice = getTrailingStartPrice();

   if(isBuyType() && lastTick.ask > trailingStartPrice) {
      int pointsStop = (int)((trailingStartPrice - tradeStrategy.priceZeroForAllOrders) / _Point);
      double newPriceStop = lastTick.ask - (pointsStop + statistics.stats.spred + trailingStopMinDistanceFromBu) * _Point;

      if((trailingStopPrice < newPriceStop || trailingStopPrice == 0) && newPriceStop > tradeStrategy.priceZeroForAllOrders)
         trailingStopPrice = newPriceStop;
   }
   if (isSellType() && lastTick.bid < trailingStartPrice) {
      int pointsStop = (int)((tradeStrategy.priceZeroForAllOrders - trailingStartPrice) / _Point);
      double newPriceStop = lastTick.bid + (pointsStop + statistics.stats.spred + trailingStopMinDistanceFromBu) * _Point;

      if((trailingStopPrice > newPriceStop || trailingStopPrice == 0) && newPriceStop < tradeStrategy.priceZeroForAllOrders)
         trailingStopPrice = newPriceStop;
   }

   return trailingStopPrice;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridStrategy::isNextLevelTouch()
{
   if (orders.Total() == 0)
      return false;

   if(isFilterStop())
      return false;

   EGridOrder *lastOrder = orders.At(orders.Total() - 1);
   
   int barIndex = iBarShift(_Symbol, _Period, lastOrder.tradeData.dateOpen, false);
   if(barIndex < minBarsFromLastOrder)
      return false;

   if(isBuyType()) {
      return lastTick.ask < getNextLevelPrice();
   } else {
      return lastTick.bid > getNextLevelPrice();
   }
}


bool EGridStrategy::isFilterStop() {
   bool isDeny = false;
   IndicatorResult result;
   
   switch(filterSignal)
     {
      case  FILTER_SIGNAL_RSI_1:
        result = dataProcessor.getFilterRsi(positionType, PERIOD_CURRENT, 7);
        
        isDeny = !result.isAllow;
        
         if(result.isAllow) {
            if(isBuyType())
               drawer.drawArrow(gridId, "sig" + IntegerToString(lastTick.time_msc), result.buffer[1], lastTick.time, 233, clrGreen, 0, 2);
            else
               drawer.drawArrow(gridId, "sig" + IntegerToString(lastTick.time_msc), result.buffer[1], lastTick.time, 234, clrGreen, 0, 2);
         }
        break;
        case  FILTER_SIGNAL_ELINES:
        result = dataProcessor.getFilterElines(positionType, PERIOD_CURRENT, 50);
        
        isDeny = !result.isAllow;
        
         if(result.isAllow) {
            if(isBuyType())
               drawer.drawArrow(gridId, "sig" + IntegerToString(lastTick.time_msc), result.buffer[1], lastTick.time, 233, clrGreen, 0, 2);
            else
               drawer.drawArrow(gridId, "sig" + IntegerToString(lastTick.time_msc), result.buffer[1], lastTick.time, 234, clrGreen, 0, 2);
         }
        break;
      default:
        break;
     }
   return isDeny;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridStrategy::needFinishGrid()
{
   tradeStrategy.finishGrid();
   return true;//
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridStrategy::tradeNextOrder()
{
   Print("Grider: tradeVirtualOrder()");

   EGridOrder *order = new EGridOrder(orders.Total());

   if (tradeStrategy.tradeOrder(order, lotStrategy.getLot(order))) {
      orders.Add(order);

      tradeStrategy.updateStatByOrders(true);

      drawer.drawLine(gridId, "nextLevel", getNextLevelPrice(), clrCoral, STYLE_DASHDOTDOT);
      return true;
   }

   Print("Ошибка: При создании ордера не удалось получить тикет");

   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridStrategy::getNextLevelDistanceByStrategy()
{
   return levelHeightPt * _Point;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridStrategy::getNextLevelPrice()
{
   EGridOrder *lastOrder = orders.At(orders.Total()-1);
   double distance = getNextLevelDistanceByStrategy();

   if(isBuyType()) {
      return lastOrder.tradeData.priceOpen - distance;
   } else {
      return lastOrder.tradeData.priceOpen + distance;
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStrategy::onInit()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStrategy::onTick(MqlTick &_lastTick)
{
   lastTick = _lastTick;

   trailingStopProcess();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStrategy::onOnNewBar()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStrategy::onReset()
{
   trailingStopPrice = 0;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
