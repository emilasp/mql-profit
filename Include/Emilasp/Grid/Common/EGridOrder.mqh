//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict


#include <Emilasp\helpers\MathHelper.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

#include <Generic\ArrayList.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

struct TradeData {
   double            profit;
   double            priceOpen;
   double            priceClose;
   
   int               levelClose;
   
   double            commission; // Комиссия по сделке
   double            swap;       // Накопленный своп при закрытии
   double            fee;        // Оплата за проведение сделки, начисляется сразу после совершения сделки

   double            tp;
   double            sl;
   
   datetime          dateOpen;
   datetime          dateClose;

                     TradeData()
   {
      profit = 0;
      priceOpen = 0;
      priceClose = 0;
      levelClose = 0;
      commission=0;
      swap=0;
      fee=0;
      tp=0; 
      sl=0;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridOrder : public CObject
{
private:
protected:
public:
   string            ID;
   ulong             ticket;
   int               level;
   double            lot;
   MqlTick           openTick;
   TradeData         tradeData;

   bool              isStatUpdated;
   bool              isVirtual;
   bool              isClosed;
   bool              isTrail;

   void             ~EGridOrder() {};
   void              EGridOrder(int _level);
   
   string            getGeneratedID(int magic, ENUM_POSITION_TYPE positionType, MqlTick &tick);
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridOrder::EGridOrder(int _level)
{
   ticket = 0;
   level = _level;

   isStatUpdated = false;
   isVirtual = true;
   isClosed = false;
   isTrail  = false;
}
//+------------------------------------------------------------------+

string EGridOrder::getGeneratedID(int magic, ENUM_POSITION_TYPE positionType, MqlTick &tick)
{
   ID = "o:" + IntegerToString(magic) + ":" + IntegerToString(positionType) + ":" + StringSubstr(IntegerToString(tick.time_msc), 5) + ":" + DoubleToString(tick.ask * 1000, 2);
   return ID;
}