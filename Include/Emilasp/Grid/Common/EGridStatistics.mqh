//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Grid\Common\helpers\EGridEnums.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>
#include <Emilasp\Grid\Common\helpers\EGridDraw.mqh>

struct Statistic {
   double            balance;
   double            equity;
   double            equityGrid;
   double            margin;
   double            marginFree;
   double            marginFreeMin;
   double            credit;

   double            currentAvgZero;
   double            priceAvgCloseMoment;

   double            profit;
   double            profitSucces;
   double            profitLoss;

   int            spred;

   int               tradeAll;
   int               tradeSuccess;
   int               tradeLoss;

   int               equityAvgCloseMoment;

   int               openOrders;
   double            openOrdersLots;

   int               sequencesLoss[11001];
   int               sequencesSuccess[11001];
   int               sequencesSuccessCount;
   int               sequencesLossCount;



   void              Statistic()
   {
      marginFreeMin=0;
      reset();
   }

   void              reset()
   {
      balance = 0;
      equity = 0;
      equityGrid = 0;
      margin=0;
      marginFree=0;
      credit=0;

      currentAvgZero=0;

      profit = 0;
      profitSucces = 0;
      profitLoss = 0;

      tradeAll = 0;
      tradeSuccess = 0;
      tradeLoss = 0;

      openOrders=0;

      equityAvgCloseMoment=0;
      priceAvgCloseMoment=0;

      ArrayFill(sequencesLoss, 0, 11000, 0);
      ArrayFill(sequencesSuccess, 0, 11000, 0);
      sequencesSuccessCount=0;
      sequencesLossCount=0;
   }

   void              addTrade(EGridOrder *order)
   {
      double profitOrder = order.isClosed ? order.tradeData.profit : 0;
      double equityOrder = !order.isClosed ? order.tradeData.profit : 0;

      equityGrid += equityOrder;

      tradeAll++;
      profit += profitOrder;

      if(order.tradeData.profit > 0) {
         tradeSuccess++;
         profitSucces += profitOrder;

         sequencesSuccessCount++;
         //if(sequencesLossCount != 0) sequencesLoss[sequencesLossCount]++;
         sequencesLossCount = 0;
      } else {
         tradeLoss++;
         profitLoss += profitOrder;

         sequencesLossCount++;
         //if(sequencesSuccessCount != 0) sequencesSuccess[sequencesSuccessCount]++;
         sequencesSuccessCount = 0;
      }
   }
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridStatistics
{
private:
   int               magic;
   ENUM_POSITION_TYPE positionType;
   int               gridId;

   EGridDraw         *drawer;
   CArrayObj         *orders;
   CArrayObj         *ordersFinished;

   bool              updateByOrders(CArrayObj *orders, bool hardUpdate);
   bool              updateOrderTradeStat(EGridOrder *order, int index);

public:
   Statistic         stats;
   
   bool              gridIsActive;

   int               atrHandle;
   double            atrBuffer[2];

   void              EGridStatistics(int _magic, int _gridId, ENUM_POSITION_TYPE _positionType, EGridDraw *_drawer, CArrayObj *_orders, CArrayObj *_ordersFinished);

   void              onInit();
   void              onTick();
   void              onReset();
   void              OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);

   void              updateBaseStats();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridStatistics::EGridStatistics(int _magic, int _gridId, ENUM_POSITION_TYPE _positionType, EGridDraw *_drawer, CArrayObj *_orders, CArrayObj *_ordersFinished)
{
   magic=_magic;
   gridId=_gridId;
   positionType = _positionType;
   drawer=_drawer;
   orders = _orders;
   ordersFinished = _ordersFinished;
   
   gridIsActive=false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStatistics::updateBaseStats()
{
   stats.balance = AccountInfoDouble(ACCOUNT_BALANCE);
   stats.equity = AccountInfoDouble(ACCOUNT_EQUITY)  - stats.balance;
   stats.margin = AccountInfoDouble(ACCOUNT_MARGIN);
   stats.marginFree = AccountInfoDouble(ACCOUNT_FREEMARGIN);
   stats.credit = AccountInfoDouble(ACCOUNT_CREDIT);
   stats.spred = SymbolInfoInteger(Symbol(),SYMBOL_SPREAD);
   
   if(stats.marginFreeMin > stats.marginFree || stats.marginFreeMin == 0)
     stats.marginFreeMin = stats.marginFree;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStatistics::onReset()
{
   stats.reset();
   updateBaseStats();
}


// Events
void EGridStatistics::onInit()
{
   updateBaseStats();

   //atrHandle = iATR(_Symbol, PERIOD_CURRENT, 14);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStatistics::onTick()
{
   updateBaseStats();

   ArraySetAsSeries(atrBuffer, true);
   CopyBuffer(atrHandle, 0, 0, 2, atrBuffer);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridStatistics::OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   if(trans.type!=TRADE_TRANSACTION_DEAL_ADD)return;
   if(!HistoryDealSelect(trans.deal))return;

   int Magic = HistoryDealGetInteger(trans.deal, DEAL_MAGIC);

   if(HistoryDealGetInteger(trans.deal, DEAL_MAGIC)!=magic)return;
   if(HistoryDealGetInteger(trans.deal, DEAL_ENTRY)!=DEAL_ENTRY_OUT)return;
   if(HistoryDealGetString(trans.deal, DEAL_SYMBOL)!=_Symbol)return;

   long     deal_entry        =0;
   double   deal_profit       =0.0;
   string   deal_symbol       ="";
   long     deal_pos_id       =0;
   double   deal_commission   =0.0;
   if(HistoryDealSelect(trans.deal)) {
      deal_entry=HistoryDealGetInteger(trans.deal, DEAL_ENTRY);
      deal_profit=HistoryDealGetDouble(trans.deal, DEAL_PROFIT);
      deal_symbol=HistoryDealGetString(trans.deal, DEAL_SYMBOL);
      deal_pos_id=HistoryDealGetInteger(trans.deal, DEAL_POSITION_ID);
      deal_commission=HistoryDealGetDouble(trans.deal, DEAL_COMMISSION);

      bool orderUpdated = false;
      int total = orders.Total();
      for(int i=0; i<total; i++) {
         EGridOrder *order = orders.At(i);
         if(order.ticket == deal_pos_id) {
            order.isStatUpdated = true;
            order.tradeData.profit = HistoryDealGetDouble(trans.deal, DEAL_PROFIT);
            order.tradeData.commission = HistoryDealGetDouble(trans.deal, DEAL_COMMISSION);
            //order.tradeData.priceOpen = HistoryOrderGetDouble(trans.deal, ORDER_PRICE_OPEN);
            order.tradeData.priceClose = HistoryOrderGetDouble(trans.deal, ORDER_PRICE_CURRENT);
            order.tradeData.swap = HistoryDealGetDouble(trans.deal, DEAL_SWAP);
            order.tradeData.fee = HistoryDealGetDouble(trans.deal, DEAL_FEE);
            order.tradeData.tp = HistoryDealGetDouble(trans.deal, DEAL_TP);
            order.tradeData.sl = HistoryDealGetDouble(trans.deal, DEAL_SL);

            orderUpdated = true;
         }
      }

      if(!orderUpdated) {
         total = ordersFinished.Total();
         for(int i=0; i<total; i++) {
            EGridOrder *order = orders.At(i);
            if(order != NULL && order.ticket == deal_pos_id) {
               order.isStatUpdated = true;
               order.tradeData.profit = HistoryDealGetDouble(trans.deal, DEAL_PROFIT);
               order.tradeData.commission = HistoryDealGetDouble(trans.deal, DEAL_COMMISSION);
               //order.tradeData.priceOpen = HistoryOrderGetDouble(trans.deal, ORDER_PRICE_OPEN);
               order.tradeData.priceClose = HistoryOrderGetDouble(trans.deal, ORDER_PRICE_CURRENT);
               order.tradeData.swap = HistoryDealGetDouble(trans.deal, DEAL_SWAP);
               order.tradeData.fee = HistoryDealGetDouble(trans.deal, DEAL_FEE);
               order.tradeData.tp = HistoryDealGetDouble(trans.deal, DEAL_TP);
               order.tradeData.sl = HistoryDealGetDouble(trans.deal, DEAL_SL);

               orderUpdated = true;
            }
         }
      }

      //if(deal_symbol==Symbol()) {
      //   if(deal_entry==DEAL_ENTRY_IN)
      //      int myticket=deal_pos_id;
      //   if(deal_entry==DEAL_ENTRY_OUT)
      //      Print("Commission = ", DoubleToString(deal_commission, 2));
      //}
   }
}

//+------------------------------------------------------------------+
/*Statistics EGridTrade::getStatisticForOpenPositions(int magic)
{
   Statistics stats;

   for(int i=PositionsTotal()-1; i>=0; i--)
      if(trade.m_Position.SelectByIndex(i)) // selects the position by index for further access to its properties
         if(trade.m_Position.Symbol()==trade.m_symbol.Name() && trade.m_Position.Magic()==magic) {
            double profit=trade.m_Position.Swap()+trade.m_Position.Profit();

            stats.addTrade(profit);
         }
//---
   return stats;
}*/
//+------------------------------------------------------------------+
