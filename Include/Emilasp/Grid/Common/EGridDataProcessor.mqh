//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>
#include <Emilasp\libraries\DictionaryObj.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Grid\Common\InfoPanel.mqh>
#include <Emilasp\Grid\Common\helpers\EGridDraw.mqh>


enum ENUM_DATA_INDICATOR {
   DATA_INDICATOR_RSI = 1,
   DATA_INDICATOR_MOMENTUM = 2,
   DATA_INDICATOR_WPR = 3,
   DATA_INDICATOR_ATR = 4,
   DATA_INDICATOR_ELINES = 5
};

struct IndicatorResult {
   double            buffer[];
   bool              isAllow;

                     IndicatorResult()
   {
      isAllow=false;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridDataProcessorIndicator : public CObject
{
private:
   ENUM_DATA_INDICATOR indicatorType;
   string            name;
   int               handle;
   int               bufferNum;
   int               bars;
   int               bufferBars;
public:

   double            buffer[];
   double            buffer2[];

                     EGridDataProcessorIndicator(ENUM_DATA_INDICATOR _indicatorType, string _name, int _bufferNum, int _bars, int _bufferBars = 2)
   {
      indicatorType = _indicatorType;
      name = _name;
      bufferNum = _bufferNum;
      bars = _bars;
      bufferBars = _bufferBars;

      ArrayResize(buffer, bufferBars);
      ArraySetAsSeries(buffer, true);

      switch(indicatorType) {
      case  DATA_INDICATOR_ATR:
         handle = iATR(_Symbol, PERIOD_CURRENT, bars);
         break;
      case  DATA_INDICATOR_RSI:
         handle = iRSI(_Symbol, _Period, bars, PRICE_CLOSE);
         break;
      case  DATA_INDICATOR_MOMENTUM:
         handle = iMomentum(_Symbol, PERIOD_CURRENT, bars, PRICE_CLOSE);
         break;
      case  DATA_INDICATOR_WPR:
         handle = iWPR(_Symbol, PERIOD_CURRENT, bars);
         break;
      case  DATA_INDICATOR_ELINES:
         handle = iCustom(_Symbol, PERIOD_CURRENT, "Emilasp/Lines", bars);
         break;
      }

      updateData();
   }

   int               updateData()
   {
      return CopyBuffer(handle, bufferNum, 0, bufferBars, buffer);
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridDataProcessor
{
private:
   int               magic;
   MqlTick           lastTick;

   CTradePanel       TradePanel;

   DictionaryObj     indicators;

   string                         getNameIndicator(ENUM_DATA_INDICATOR indicatorType, ENUM_TIMEFRAMES tf, int bars, int bufferNum);
   EGridDataProcessorIndicator   *getData(ENUM_DATA_INDICATOR indicatorType, ENUM_TIMEFRAMES tf, int bars, int bufferNum, int bufferBars = 2);

   bool              isNewBar()
   {
      static datetime lastbar;
      datetime curbar = iTime(_Symbol, PERIOD_CURRENT, 0);
      if(lastbar!=curbar) {
         lastbar=curbar;
         return (true);
      } else {
         return(false);
      }
   }



public:
   void              EGridDataProcessor(int _magic);

   void              onInit();
   void              onTick();
   void              onNewBar();
   void              onReset();

   IndicatorResult              getFilterRsi(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars);
   IndicatorResult              getFilterMomentum(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars);
   IndicatorResult              getFilterWpr(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars);
   IndicatorResult              getFilterElines(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars);

   int               getSlAtr(ENUM_TIMEFRAMES tf, int bars, double koef, int minSlPt);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridDataProcessor::EGridDataProcessor(int _magic)
{
   magic=_magic;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string   EGridDataProcessor::getNameIndicator(ENUM_DATA_INDICATOR indicatorType, ENUM_TIMEFRAMES tf, int bars, int bufferNum)
{
   return IntegerToString(indicatorType) + ":" + IntegerToString(tf) + ":" + IntegerToString(bars) + ":" + IntegerToString(bufferNum);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridDataProcessorIndicator   *EGridDataProcessor::getData(ENUM_DATA_INDICATOR indicatorType, ENUM_TIMEFRAMES tf, int bars, int bufferNum, int bufferBars = 2)
{
   string name = getNameIndicator(indicatorType, tf, bars, bufferNum);
   EGridDataProcessorIndicator *indicatorData;

   if(indicators.isset(name)) {
      indicatorData = indicators.get(name);
   } else {
      indicatorData = new EGridDataProcessorIndicator(indicatorType, name, bufferNum, bars, bufferBars);
   }

   return indicatorData;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
IndicatorResult EGridDataProcessor::getFilterRsi(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars)
{
   bool isAllow = true;
   EGridDataProcessorIndicator *indicatorData = getData(DATA_INDICATOR_RSI, tf, bars, 0);
   if(positionType == POSITION_TYPE_BUY) {
      isAllow = indicatorData.buffer[1] < 30 && indicatorData.buffer[0] > 30;
   } else {
      isAllow = indicatorData.buffer[1] > 70 && indicatorData.buffer[0] < 70;
   }
   IndicatorResult result;
   ArrayCopy(result.buffer, indicatorData.buffer);
   result.isAllow = isAllow;
   
   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
IndicatorResult EGridDataProcessor::getFilterElines(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars)
{
   bool isAllow = true;
   EGridDataProcessorIndicator *indicatorData = getData(DATA_INDICATOR_ELINES, tf, bars, 5);
   if(positionType == POSITION_TYPE_BUY) {
      isAllow = indicatorData.buffer[1] == clrRed && indicatorData.buffer[0] == clrRed;
   } else {
            isAllow = indicatorData.buffer[1] == clrGreen && indicatorData.buffer[0] == clrGreen;
   }
   IndicatorResult result;
   ArrayCopy(result.buffer, indicatorData.buffer);
   result.isAllow = isAllow;
   
   return result;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
IndicatorResult EGridDataProcessor::getFilterMomentum(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars)
{
   IndicatorResult result;
   return result;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
IndicatorResult EGridDataProcessor::getFilterWpr(ENUM_POSITION_TYPE positionType, ENUM_TIMEFRAMES tf, int bars)
{
   IndicatorResult result;
   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int EGridDataProcessor::getSlAtr(ENUM_TIMEFRAMES tf, int bars, double koef, int minSlPt)
{
   int sl = minSlPt;

   EGridDataProcessorIndicator *indicatorData = getData(DATA_INDICATOR_ATR, tf, bars, 0, 1);

   double atrPt = indicatorData.buffer[0] * 10000;
   atrPt *= koef;
   return atrPt < minSlPt ? minSlPt : (int)atrPt;
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
// Events
void EGridDataProcessor::onInit()
{
//statistics = grid.statistics;
//drawer     = grid.drawer;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDataProcessor::onTick()
{
   if(isNewBar()) {
      int total = indicators.total();
      for(int i=0; i<total; i++) {
         EGridDataProcessorIndicator *indicatorData = indicators.atValue(i);
         indicatorData.updateData();
      }
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDataProcessor::onNewBar()
{
   
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridDataProcessor::onReset()
{
}

//+------------------------------------------------------------------+
