//+------------------------------------------------------------------+
//|                                                    InfoPanel.mqh |
//|                                  Copyright 2021, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Ltd."
#property link      "https://www.mql5.com"

#include <Controls\Dialog.mqh>

#include <Controls\Dialog.mqh>
#include <Controls\Label.mqh>
#include <Controls\Button.mqh>

#include <Emilasp\Grid\Common\EGridStatistics.mqh>

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
#define  Y_STEP   (int)(ClientAreaHeight()/18/4)      // height step between elements
#define  Y_WIDTH  (int)(ClientAreaHeight()/14)        // height of element
#define  BORDER   (int)(ClientAreaHeight()/24)        // distance between border and elements  

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CTradePanel : public CAppDialog
{
private:
   long              chart;
   int               subwindow;
   int               gridId;

   CLabel            *labels[12];
   CLabel            *values[12];

   //--- Create Label object
   virtual void      MinimizePanel(void);
   bool              CreateLabel(CLabel &object, const string name, const uint x, const uint y, bool isRight = false);
   bool              addParameter(int row, const string label, const string value, bool isRight = false, int clr = clrBlack);
public:
   EGridStatistics   *statistics;

                     CTradePanel(void) {};
                    ~CTradePanel(void) {};
   virtual bool      Create(const long _chart, const int _subwin, int _gridId, const string name, EGridStatistics *_statistics, const int x1, int y1, const int x2, int y2);
   void              onTick();
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CTradePanel::onTick()
{
   addParameter(0, "Active: ", statistics.gridIsActive, false);
   addParameter(1, "Balance: ", DoubleToString(statistics.stats.balance, 2), false);
   addParameter(2, "Free margin: ", DoubleToString(statistics.stats.marginFree, 1), false, statistics.stats.marginFree > (statistics.stats.balance/2) ? clrGreen : clrRed);
   addParameter(3, "Min free margin: ", DoubleToString(statistics.stats.marginFreeMin, 1), false, statistics.stats.marginFreeMin > (statistics.stats.balance/2) ? clrGreen : clrRed);
   addParameter(4, "Equity: ", DoubleToString(statistics.stats.equityGrid, 2), true, statistics.stats.equityGrid >= 0 ? clrGreen : clrRed);
   addParameter(5, "Spred: ", IntegerToString(statistics.stats.spred), false);
   
   addParameter(6, "Orders: ", IntegerToString(statistics.stats.openOrders), false);
   addParameter(7, "Lots: ", DoubleToString(statistics.stats.openOrdersLots, 2), true);

   addParameter(8, "Equity Price F grid: ", DoubleToString(statistics.stats.priceAvgCloseMoment, 5), true);
   addParameter(9, "Price zero: ", DoubleToString(statistics.stats.currentAvgZero, 5), true);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTradePanel::addParameter(int row, const string label, const string value, bool isRight = false, int clr = clrBlack)
{
   CLabel *labelObj;

   labelObj = labels[row];
   if(labelObj == NULL) {
      int widthFull =  ClientAreaWidth();
      int widthFirst = (int)(widthFull / 2);
      int widthSecond = (int)(widthFirst / 2);

      int y = BORDER + row * Y_WIDTH * 1.4;;
      int xLabel;
      int xValue;

      xLabel = BORDER;
      xValue = widthFull;

      CLabel *labelObj = new CLabel();
      CLabel *valueObj = new CLabel();

      labelObj.FontSize(Y_WIDTH);
      valueObj.FontSize(Y_WIDTH);

      CreateLabel(labelObj, "label:" + label, xLabel, y, false);
      CreateLabel(valueObj, "value:" + label, xValue, y, true);
      
      labelObj.Color(clrDarkBlue);
      
      labels[row] = labelObj;
      values[row] = valueObj;

   }
   
   values[row].Color(clr);

   labels[row].Text(label);
   values[row].Text(value);

   return true;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTradePanel::Create(const long _chart, const int _subwin, int _gridId, const string name, EGridStatistics *_statistics, const int x1=20, int y1=20, const int x2=320, int y2=420)
{
   chart = _chart;
   subwindow = _subwin;
   gridId = _gridId;

   statistics = _statistics;

   if(gridId == 1) {
      y1 += y2;
      y2 += y2;
   }

// At first call create function of parents class
   if(!CAppDialog::Create(chart, name, subwindow, x1, y1, x2, y2)) {
      return false;
   }

   return true;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTradePanel::CreateLabel(CLabel &object, const string name, const uint x, const uint y, bool isRight = false)
{
// All objects must have separate name
   string nameFull=m_name+"Label"+name;
//--- Call Create function
   if(!object.Create(chart, nameFull, subwindow, x, y, 0, 0)) {
      return false;
   }

//--- Align text to Dialog box's grid
   ObjectSetInteger(chart, object.Name(), OBJPROP_ANCHOR, isRight ? ANCHOR_RIGHT_UPPER : ANCHOR_LEFT_UPPER);
//ObjectSetInteger(chart, object.Name(), OBJPROP_ANCHOR, (align==left ? ANCHOR_LEFT_UPPER : (align==right ? ANCHOR_RIGHT_UPPER : ANCHOR_UPPER)));
//--- Add object to controls
   if(!Add(object)) {
      return false;
   }
   return true;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CTradePanel::MinimizePanel(void)
  {
//--- переменная для получения панели быстрой торговли
   long one_click_visible=-1;  // 0 - панели быстрой торговли нет 
   if(!ChartGetInteger(chart,CHART_SHOW_ONE_CLICK,0,one_click_visible))
     {
      //--- выведем сообщение об ошибке в журнал "Эксперты"
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- минимальный отступ для свернутой панели приложения
   int min_y_indent=28;
   if(one_click_visible)
      min_y_indent=100;  // отступ, если быстрая торговля показана на графике
//--- получим текущий отступ для свернутой панели приложения
   int current_y_top=m_min_rect.top;
   int current_y_bottom=m_min_rect.bottom;
   int height=current_y_bottom-current_y_top;
//--- вычислим новый минимальный отступ от верха для свернутой панели приложения
   if(m_min_rect.top!=min_y_indent)
     {
      m_min_rect.top=min_y_indent;
      //--- сместим также нижнюю границу свернутой иконки
      m_min_rect.bottom=m_min_rect.top+height;
     }
//--- теперь можно вызвать метод базового класса
   CAppDialog::Minimize();
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
