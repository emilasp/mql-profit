//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Grid\Common\EGrid.mqh>
#include <Emilasp\Grid\Common\EGridDataProcessor.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridManager
{
private:
   int               magic;
   EGridDataProcessor *dataProcessor;
   
public:
   CArrayObj         grids;
   bool              isFinishDifferent;

   void              EGridManager(int _magic, bool _isFinishDifferent);

   void              onInit();
   void              onTick();
   void              OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);
   void              OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam);
   void              addGrid(EGrid *grid);
   void              finishOnProfit();

   void              reset();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridManager::EGridManager(int _magic, bool _isFinishDifferent)
{
   magic=_magic;
   isFinishDifferent = _isFinishDifferent;
   
   dataProcessor = new EGridDataProcessor(magic);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridManager::addGrid(EGrid *grid)
{
   grid.magic = magic;
   grid.id = grids.Total();
   grid.dataProcessor = dataProcessor;
   
   grids.Add(grid);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridManager::finishOnProfit()
{
//double profitFull = 0;
//int total = grids.Total();
//for(int i=0; i<total; i++) {
//EGrid *grid = grids.At(i);
//double gridProfit = grid.statistics.profit;
//profitFull += gridProfit;

// Check to finish
//}
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridManager::reset()
{
   int total = grids.Total();
   for(int i=0; i<total; i++) {
      EGrid *grid = grids.At(i);
      grid.reset();
   }
   
   dataProcessor.onReset();
}


// Events
void EGridManager::onInit()
{
   int total = grids.Total();
   for(int i=0; i<total; i++) {
      EGrid *grid = grids.At(i);
      grid.onInit();
   }
   
   dataProcessor.onInit();

//ChartSetInteger ( ChartID(), CHART_SHOW_TRADE_LEVELS, false );
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridManager::onTick()
{
   int total = grids.Total();
   for(int i=0; i<total; i++) {
      EGrid *grid = grids.At(i);
      grid.onTick();
   }

   finishOnProfit();
   dataProcessor.onTick();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridManager::OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   int total = grids.Total();
   for(int i=0; i<total; i++) {
      EGrid *grid = grids.At(i);
      grid.OnTradeTransaction(trans, request, result);
   }
}
void EGridManager::OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   int total = grids.Total();
   for(int i=0;i<total;i++) {
      EGrid * grid = grids.At(i);
      grid.OnChartEvent(id, lparam, dparam, sparam);
   }
}
//+------------------------------------------------------------------+
