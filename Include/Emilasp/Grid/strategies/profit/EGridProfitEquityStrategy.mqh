//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridProfitStrategy.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridProfitEquityStrategy : public EGridProfitStrategy {
private:
public:
   EGridProfitEquityStrategy::EGridProfitEquityStrategy(int _equityLossClose, int _virtualStopBUtoTralMinDistance, bool _trailingVirtualEnabled, int _trailingVirtualStartDistance, double _trailingVirtualDynamicEquityKoef, int _virtualStopPriceDistanceLevel, double _virtualStopPriceDistanceLevelKoef);
   bool              needFinishGrid();
   void              trailingStopVirtualProcess();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridProfitEquityStrategy::EGridProfitEquityStrategy(int _equityLossClose, int _virtualStopBUtoTralMinDistance, bool _trailingVirtualEnabled, int _trailingVirtualStartDistance, double _trailingVirtualDynamicEquityKoef, int _virtualStopPriceDistanceLevel, double _virtualStopPriceDistanceLevelKoef) {
   trailingVirtualEnabled = _trailingVirtualEnabled;
   equityLossClose = _equityLossClose;
   trailingVirtualStartDistance = _trailingVirtualStartDistance;
   trailingVirtualDynamicEquityKoef=_trailingVirtualDynamicEquityKoef;
   
   virtualStopPriceDistanceLevel = _virtualStopPriceDistanceLevel;
   virtualStopPriceDistanceLevelKoef = _virtualStopPriceDistanceLevelKoef;
   virtualStopBUtoTralMinDistance = _virtualStopBUtoTralMinDistance;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EGridProfitEquityStrategy::needFinishGrid() {
   double balance = AccountInfoDouble(ACCOUNT_BALANCE);
   double equity = AccountInfoDouble(ACCOUNT_EQUITY)  - balance;

   //double equityAvgCal = 3 * (lastTradeLevel + 1);

   if(lastTradeLevel > 0 ) {
      EGridOrder *order = orders.At(lastTradeLevel);
      datetime lastOrderDate = order.tradeData.dateOpen;
      int barIndex = iBarShift(_Symbol,_Period,order.tradeData.dateOpen,false);
      if(barIndex > 20) {
         //return true;
      }
   }

   if(!trailingVirtualEnabled && lastTradeLevel > 2 && equity > equityAvgClose) {//
      return true;
   }

   if(equity < equityLossClose) {
      return true;
   }

   if(needFinishGridByVirtualStop()) {
      return true;
   }

   return false;
}

//+------------------------------------------------------------------+
