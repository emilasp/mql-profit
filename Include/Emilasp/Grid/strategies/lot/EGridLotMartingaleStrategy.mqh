//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridLotStrategy.mqh>
#include <Emilasp\Grid\Common\EGridOrder.mqh>



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridLotMartingaleStrategy :public EGridLotStrategy
{
private:
   double            koef;

public:
                     EGridLotMartingaleStrategy::EGridLotMartingaleStrategy(double _lotBase, ENUM_LOT_STRATEGY _lotStrategy, double _koef);

   double            getLot(EGridOrder *order);
   double            getLotMartingale(int level);
   double            getLotArifmetikProgression(int level);
   double            getLotFibo(int level);
   int               getFibo(int level);
   void              onReset();

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridLotMartingaleStrategy::EGridLotMartingaleStrategy(double _lotBase, ENUM_LOT_STRATEGY _lotStrategy, double _koef)
{
   lotBase=_lotBase;
   koef=_koef;
   lotStrategy = _lotStrategy;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridLotMartingaleStrategy::getLot(EGridOrder *order)
{
   double lot;

   switch(lotStrategy) {
   case  LOT_STRATEGY_MARTINGALE:
      lot = getLotMartingale(order.level);
      break;
   case  LOT_STRATEGY_ARIFMETIK:
      lot = getLotArifmetikProgression(order.level);
      break;
   case  LOT_STRATEGY_FIBO:
      lot = getLotFibo(order.level);
      break;

   default:
      lot = lotBase;
      break;
   }

   return (double)MathFloor(lot*100)/100;;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridLotMartingaleStrategy::getLotMartingale(int level)
{
   return level > 0 ? level * lotBase * koef : lotBase;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridLotMartingaleStrategy::getLotArifmetikProgression(int level)
{
   return level > 0 ? level * lotBase : lotBase;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double EGridLotMartingaleStrategy::getLotFibo(int level)
{
   int fibo = getFibo(level);
   return fibo > 0 ? fibo * lotBase : lotBase;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int EGridLotMartingaleStrategy::getFibo(int level)
{
   if (level == 0 ) return 0;
   if (level == 1 || level == 2) {
      return 1;
   } else {
      return getFibo(level - 1) + getFibo(level -2);
   }
}



//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void EGridLotMartingaleStrategy::onReset()
{
}
//+------------------------------------------------------------------+
