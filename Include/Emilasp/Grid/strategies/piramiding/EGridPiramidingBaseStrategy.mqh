//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridPiramidingStrategy.mqh>
#include <Emilasp\Signals\StochasticSignal.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridPiramidingBaseStrategy : public EGridPiramidingStrategy
{
private:
 double lotLevelKoef;
public:
       EGridPiramidingBaseStrategy::EGridPiramidingBaseStrategy(bool _enabled, double lot, int _levelDistance, int _breakEven, double _lotLevelKoef, bool _trailingSlByAtrEnable, double _trailingSlByAtrKoef);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridPiramidingBaseStrategy::EGridPiramidingBaseStrategy(bool _enabled, double lot, int _levelHeightPt, int _breakEven, double _lotLevelKoef, bool _trailingSlByAtrEnable, double _trailingSlByAtrKoef)
{
   lotBase=lot;
   levelHeightPt=_levelHeightPt;
   enabled = _enabled;
   
   breakEven=_breakEven;
   lotLevelKoef=_lotLevelKoef;
   
   trailingSlByAtrEnable = _trailingSlByAtrEnable;
   trailingSlByAtrKoef=_trailingSlByAtrKoef;
   
   //StochasticSignal *stochSignal = new StochasticSignal(PERIOD_CURRENT, 7, 3, 3, 80, 20);
   //stochSignal.setSignalsSettings(ENTER_TYPE_STOCH_LEVEL_INTERSECT, EXIT_TYPE_STOCH_LEVEL_INTERSECT);
   
   //signalManager.addSignal(stochSignal, ENUM_SIGNAL_CONCATENATE_TYPE_AND);
}

//+------------------------------------------------------------------+
