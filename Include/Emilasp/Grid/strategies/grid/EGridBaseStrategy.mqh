//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridStrategy.mqh>
//+------------------------------------------------------------------++
//| Класс отвечает за работу самой сетки - глобально:                 |
//| 1. Построение уровней                                             |
//| 2. Создание ордеров                                               |            
//| 3. Отслеживание ордеров                                           |
//+------------------------------------------------------------------++
class EGridBaseStrategy : public EGridStrategy {
private:
protected:
   double getNextPriceByStrategy(double currentPrice, int levels);

public:
   EGridBaseStrategy::EGridBaseStrategy(int _levels, int _heightPt, int _minBarsFromLastOrder, bool _showGridVirtualLevels, int tsDistanceLevelBase, int tsMinDistanceFromBu, double tsDynamicEquityKoef, ENUM_FILTER_SIGNAL _filterSignal, bool _trailingSlByAtrEnable, double _trailingSlByAtrKoef);

};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EGridBaseStrategy::EGridBaseStrategy(int _levels, int _heightPt, int _minBarsFromLastOrder, bool _showGridVirtualLevels, int tsDistanceLevelBase, int tsMinDistanceFromBu, double tsDynamicEquityKoef, ENUM_FILTER_SIGNAL _filterSignal, bool _trailingSlByAtrEnable, double _trailingSlByAtrKoef) {
   levels=_levels;
   levelHeightPt=_heightPt;
   minBarsFromLastOrder=_minBarsFromLastOrder;
   
   trailingStopDistanceLevelBase = tsDistanceLevelBase;
   trailingStopMinDistanceFromBu = tsMinDistanceFromBu;
   trailingStopDynamicEquityKoef = tsDynamicEquityKoef;
   
   filterSignal = _filterSignal;
   
   trailingSlByAtrEnable = _trailingSlByAtrEnable;
   trailingSlByAtrKoef=_trailingSlByAtrKoef;
}

