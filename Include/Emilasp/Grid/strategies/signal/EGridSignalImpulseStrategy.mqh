//+------------------------------------------------------------------+
//|                                          Sample_TrailingStop.mqh |
//|                                        MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Grid\Common\base\EGridSignalStrategy.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EGridSignalImpulseStrategy : public EGridSignalStrategy {
 private:
 public:
      EGridSignalImpulseStrategy::EGridSignalImpulseStrategy(bool _debugIsSignal = false);

};

EGridSignalImpulseStrategy::EGridSignalImpulseStrategy(bool _debugIsSignal = false)
{
   debugIsSignal = _debugIsSignal;
}