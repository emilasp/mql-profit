//+------------------------------------------------------------------+
//|                                                 em_chanal_MA.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 5
#property indicator_plots   5
//--- plot Label1
#property indicator_label1  "MA"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "UP"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "DN"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
//--- plot Label4
#property indicator_label4  "SELL"
#property indicator_type4   DRAW_ARROW
#property indicator_color4  clrRed
#property indicator_width4  1
//--- plot Label5
#property indicator_label5  "BUY"
#property indicator_type5   DRAW_ARROW
#property indicator_color5  clrBlue
#property indicator_width5  1
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];
double         Label4Buffer[];
double         Label5Buffer[];

input int bar=144; // MA period

int h_RSI;

int up=70;
int dn=30;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,Label4Buffer,INDICATOR_DATA);
   SetIndexBuffer(4,Label5Buffer,INDICATOR_DATA);

   h_RSI=iRSI(_Symbol,_Period,7,PRICE_CLOSE);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<bar)
         continue;

      double sum=0;
      for(int j=0; j<bar; j++)
         sum += close[i-j];
      Label1Buffer[i] = sum/bar;


      double max=-DBL_MAX;
      double min=DBL_MAX;

      for(int j=0; j<bar; j++)
        {
         if(max < high[i-j])
            max = high[i-j];

         if(min > low[i-j])
            min = low[i-j];
        }

      Label2Buffer[i]=max;
      Label3Buffer[i]=min;
      
      double RSI[];
      ArraySetAsSeries(RSI, true);
      CopyBuffer(h_RSI, 0, time[i], 1, RSI);
      
      Label4Buffer[i]=0;
      Label5Buffer[i]=0;
      
      if(RSI[0]>up)
         Label4Buffer[i]=Label2Buffer[i];

      if(RSI[0]<dn)
         Label5Buffer[i]=Label3Buffer[i];
      
     }

//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
