//+------------------------------------------------------------------+
//|                                             em_fractal_osc_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_minimum -1
#property indicator_maximum 1
#property indicator_buffers 1
#property indicator_plots   1
//--- plot Label1
#property indicator_label1  "Label1"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- indicator buffers
double         Label1Buffer[];

int h;

datetime start=D'01.01.2020';
input int bar=90;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);

   string name = "em_fractal";
   h = iCustom(_Symbol, _Period, name);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<bar)
         continue;
      if(time[i] < start)
         continue;

      double FRC_UP[];
      ArraySetAsSeries(FRC_UP, true);
      CopyBuffer(h, 0, time[i], bar, FRC_UP);

      double FRC_DN[];
      ArraySetAsSeries(FRC_DN, true);
      CopyBuffer(h, 1, time[i], bar, FRC_DN);

      int moment=0;

// Analog * w(tikc values))
// Errors find



      for(int j=0; j<bar; j++)
        {
         if(FRC_UP[j]>0)
            moment+=j;
         if(FRC_DN[j]>0)
            moment-=j;
        }
        
        Label1Buffer[i]=0;
        
        if(moment>0)
         Label1Buffer[i]=1;
        if(moment<0)
         Label1Buffer[i]=-1;
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
