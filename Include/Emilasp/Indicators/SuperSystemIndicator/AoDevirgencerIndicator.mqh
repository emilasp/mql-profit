//+------------------------------------------------------------------+
//|                                             MacdIndicator.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\Indicators\SuperSystemIndicator\Common\IndicatorBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AoDevirgencerIndicator : public IndicatorBase
{
private:
   int barsToAnalize;

   bool FillArraysFromBuffers();
   void calculateBuffers();
   double LinearRegr(int newN, double &X[], double &Y[], int N);

protected:
   void onNewTick();
   void onNewBar();

public:
   int OnInit(string name, int _barsToAnalize);
   void OnDeinit(const int reason);
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int AoDevirgencerIndicator::OnInit(string name, int _barsToAnalize)
{
   barsToAnalize=_barsToAnalize;          // period of the Jaw line

   addBuffer("ao", DATA, false, false);
   addBuffer("aoColor", DATA, false, false);
   addBuffer("divergence", DATA, false, false);

   handle=iAO(_Symbol, _Period);
//--- if the handle is not created
   if(handle==INVALID_HANDLE) {
      //--- tell about the failure and output the error code
      PrintFormat("Failed to create handle of the iAlligator indicator for the symbol %s/%s, error code %d",
                  name, EnumToString(_Period), GetLastError());
      //--- the indicator is stopped early
      return(INIT_FAILED);
   }

   return IndicatorBase::OnInit(name);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AoDevirgencerIndicator::calculateBuffers()
{
   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);

   BufferIndicator *AoBuffer = getBuffer("ao");
   BufferIndicator *AoColorBuffer = getBuffer("aoColor");
   BufferIndicator *DivBuffer = getBuffer("divergence");

   DivBuffer.buffer[bars_calculated] = 0;

   bool isIntersectBull = AoColorBuffer.buffer[bars_calculated] < 0 && AoColorBuffer.buffer[bars_calculated-1] > 0;
   bool isIntersectBear = AoColorBuffer.buffer[bars_calculated] > 0 && AoColorBuffer.buffer[bars_calculated-1] < 0;

   double maximums[];
   double minimums[];
   double timesMax[];
   double timesMin[];

   if (isIntersectBull || isIntersectBear) {
      for(int i=0; i < barsToAnalize; i++) {
         double aoValPrev = AoBuffer.buffer[bars_calculated-(i+2)];
         double aoVal = AoBuffer.buffer[bars_calculated-(i+1)];
         double aoValNext = AoBuffer.buffer[bars_calculated-i];

         if (isIntersectBull && aoValPrev < aoVal && aoVal > aoValNext) {
            ArrayResize(maximums, ArraySize(maximums) + 1);
            ArrayResize(timesMax, ArraySize(timesMax) + 1);
            maximums[ArraySize(maximums)-1] = aoVal;
            timesMax[ArraySize(timesMax)-1] = i;

            DrawHelper::drawArrow("arrMax" + TimeToString(time[i]), time[i], aoVal, 159, clrGreen, 0, 1);
         }
         if (isIntersectBear && aoValPrev > aoVal && aoVal < aoValNext) {
            ArrayResize(minimums, ArraySize(minimums) + 1);
            ArrayResize(timesMin, ArraySize(timesMin) + 1);
            minimums[ArraySize(minimums)-1] = aoVal;
            timesMin[ArraySize(timesMin)-1] = i;

            DrawHelper::drawArrow("arrMin" + TimeToString(time[i]), time[i], aoVal, 159, clrRed, 0, 1);
         }
      }
   }


   // Draw
   //DrawHelper::drawLabel("labelIP", "LastIntersec price: " + IntegerToString(lastPriceIntersec), 20, 60);
   //DrawHelper::drawLabel("labelStrength", "Strength: " + DoubleToString(BufferStrength.buffer[bars_calculated], 2), 20, 80);
}

//+------------------------------------------------------------------+
//| Формула предлагаемая мной                                        |
//| Рассчет коэффициентов A и B в уравнении                          |
//| y(x)=A*x+B                                                       |
//| используються формулы https://forum.mql4.com/ru/10780/page5&nbsp;      |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool AoDevirgencerIndicator::FillArraysFromBuffers()
{
   ResetLastError();
//--- fill a part of the JawsBuffer array with values from the indicator buffer that has 0 index
   if(CopyBuffer(handle, 0, 0, values_to_copy, getBuffer("ao").buffer) < 0) {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the indicator, error code %d", GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
   }
   if(CopyBuffer(handle, 1, 0, values_to_copy, getBuffer("aoColor").buffer) < 0) {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the indicator, error code %d", GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
   }   
   
//--- everything is fine
   return(true);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AoDevirgencerIndicator::onNewTick()
{


};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AoDevirgencerIndicator::onNewBar()
{
   if(!FillArraysFromBuffers()) {}

   calculateBuffers();
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AoDevirgencerIndicator::OnDeinit(const int reason)
{
   IndicatorBase::OnDeinit(reason);
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double AoDevirgencerIndicator::LinearRegr(int newN, double &X[], double &Y[], int N)
{
   double a, b;

   double mo_X = 0.0, mo_Y = 0.0, var_0 = 0.0, var_1 = 0.0;

   for ( int i = 0; i < N; i ++ ) {
      mo_X +=X[i];
      mo_Y +=Y[i];
   }
   mo_X /=N;
   mo_Y /=N;

   for (int i = 0; i < N; i ++ ) {
      var_0 +=(X[i]-mo_X)*(Y[i]-mo_Y);
      var_1 +=(X[i]-mo_X)*(X[i]-mo_X);
   }
   a = var_0 / var_1;
   b = mo_Y - a * mo_X;

   return a*newN + b;
}
//+------------------------------------------------------------------+
