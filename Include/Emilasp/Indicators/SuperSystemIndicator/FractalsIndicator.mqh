//+------------------------------------------------------------------+
//|                                             MacdIndicator.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\Indicators\SuperSystemIndicator\Common\IndicatorBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AlligatorIndicator : public IndicatorBase
{
private:
   double         JawsBuffer[];
   double         TeethBuffer[];
   double         LipsBuffer[];

   int                  jaw_period;          // period of the Jaw line
   int                  jaw_shift;            // shift of the Jaw line
   int                  teeth_period;         // period of the Teeth line
   int                  teeth_shift;          // shift of the Teeth line
   int                  lips_period;          // period of the Lips line
   int                  lips_shift;           // shift of the Lips line
   ENUM_MA_METHOD       MA_method;    // method of averaging of the Alligator lines
   ENUM_APPLIED_PRICE   applied_price;// type of price used for calculation of Alligator

   bool FillArraysFromBuffers();

protected:
   void onNewTick();
   void onNewBar();

public:
   int OnInit(string name, int _jaw_period, int _jaw_shift, int _teeth_period, int _teeth_shift, int _lips_period, int _lips_shift, ENUM_MA_METHOD _MA_method, ENUM_APPLIED_PRICE _applied_price);
   void OnDeinit(const int reason);
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int AlligatorIndicator::OnInit(string name, int _jaw_period, int _jaw_shift, int _teeth_period, int _teeth_shift, int _lips_period, int _lips_shift, ENUM_MA_METHOD _MA_method, ENUM_APPLIED_PRICE _applied_price)
{
   jaw_period=_jaw_period;          // period of the Jaw line
   jaw_shift=_jaw_shift;            // shift of the Jaw line
   teeth_period=_teeth_period;         // period of the Teeth line
   teeth_shift=_teeth_shift;          // shift of the Teeth line
   lips_period=_lips_period;          // period of the Lips line
   lips_shift=_lips_shift;           // shift of the Lips line
   MA_method=_MA_method;
   applied_price=_applied_price;

//--- assignment of arrays to indicator buffers
   SetIndexBuffer(0, JawsBuffer, INDICATOR_DATA);
   SetIndexBuffer(1, TeethBuffer, INDICATOR_DATA);
   SetIndexBuffer(2, LipsBuffer, INDICATOR_DATA);
//--- set shift of each line
   PlotIndexSetInteger(0, PLOT_SHIFT, jaw_shift);
   PlotIndexSetInteger(1, PLOT_SHIFT, teeth_shift);
   PlotIndexSetInteger(2, PLOT_SHIFT, lips_shift);


   handle=iAlligator(_Symbol,_Period,jaw_period,jaw_shift,teeth_period, teeth_shift,lips_period,lips_shift,MA_method,applied_price);
//--- if the handle is not created
   if(handle==INVALID_HANDLE)
     {
      //--- tell about the failure and output the error code
      PrintFormat("Failed to create handle of the iAlligator indicator for the symbol %s/%s, error code %d",
                  name,
                  EnumToString(_Period),
                  GetLastError());
      //--- the indicator is stopped early
      return(INIT_FAILED);
     }

   return IndicatorBase::OnInit(name);
};


 bool AlligatorIndicator::FillArraysFromBuffers() {
    ResetLastError();
//--- fill a part of the JawsBuffer array with values from the indicator buffer that has 0 index
   if(CopyBuffer(handle,0,-jaw_shift,values_to_copy,JawsBuffer)<0)
     {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d",GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
     }
 
//--- fill a part of the TeethBuffer array with values from the indicator buffer that has index 1
   if(CopyBuffer(handle,1,-teeth_shift,values_to_copy,TeethBuffer)<0)
     {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d",GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
     }
 
//--- fill a part of the LipsBuffer array with values from the indicator buffer that has index 2
   if(CopyBuffer(handle,2,-lips_shift,values_to_copy,LipsBuffer)<0)
     {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d",GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
     }
//--- everything is fine
   return(true);
 }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::onNewTick()
{
   

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::onNewBar()
{
   if(!FillArraysFromBuffers()) {
      
   };
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::OnDeinit(const int reason)
{
   IndicatorBase::OnDeinit(reason);
};
//+------------------------------------------------------------------+
