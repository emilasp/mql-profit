//+------------------------------------------------------------------+
//|                                             MacdIndicator.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\Indicators\SuperSystemIndicator\Common\IndicatorBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AlligatorIndicator : public IndicatorBase
{
private:
   int                  jaw_period;          // period of the Jaw line
   int                  jaw_shift;            // shift of the Jaw line
   int                  teeth_period;         // period of the Teeth line
   int                  teeth_shift;          // shift of the Teeth line
   int                  lips_period;          // period of the Lips line
   int                  lips_shift;           // shift of the Lips line
   ENUM_MA_METHOD       MA_method;    // method of averaging of the Alligator lines
   ENUM_APPLIED_PRICE   applied_price;// type of price used for calculation of Alligator

   int minBarsIntersec;
   int historyBarsAnalizeCount; // количество баров для поиска минимального и максимального значения аллигатора(в начале)
   double flatPercentByMin; // Считаем, что флет если дистанция аллигатор меньше чем flatPercentByMin от minDistance
   int trandByFlatDistance; // Через сколько баров после пересечения считаем, что тренд наступил.
   int angleGoodColor;

   double minAlligatorDistance;
   double maxAlligatorDistance;
   int lastPriceIntersec;

   bool FillArraysFromBuffers();
   void calculateAlligatorDistancesFromHistory();
   void calculateBuffers();
   double LinearRegr(int newN, double &X[], double &Y[], int N);

protected:
   void onNewTick();
   void onNewBar();

public:
   int OnInit(string name, int _jaw_period, int _jaw_shift, int _teeth_period, int _teeth_shift, int _lips_period, int _lips_shift, ENUM_MA_METHOD _MA_method, ENUM_APPLIED_PRICE _applied_price, int _historyBarsAnalizeCount, double _flatPercentByMin, int _trandByFlatDistance, int _angleGoodColor);
   void OnDeinit(const int reason);
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int AlligatorIndicator::OnInit(string name, int _jaw_period, int _jaw_shift, int _teeth_period, int _teeth_shift, int _lips_period, int _lips_shift, ENUM_MA_METHOD _MA_method, ENUM_APPLIED_PRICE _applied_price, int _historyBarsAnalizeCount, double _flatPercentByMin, int _trandByFlatDistance, int _angleGoodColor)
{
   jaw_period=_jaw_period;          // period of the Jaw line
   jaw_shift=_jaw_shift;            // shift of the Jaw line
   teeth_period=_teeth_period;         // period of the Teeth line
   teeth_shift=_teeth_shift;          // shift of the Teeth line
   lips_period=_lips_period;          // period of the Lips line
   lips_shift=_lips_shift;           // shift of the Lips line
   MA_method=_MA_method;
   applied_price=_applied_price;

   minBarsIntersec = 2;
   historyBarsAnalizeCount = _historyBarsAnalizeCount;
   flatPercentByMin = _flatPercentByMin;
   trandByFlatDistance = _trandByFlatDistance;
   angleGoodColor = _angleGoodColor;
   
   minAlligatorDistance = EMPTY_VALUE;
   maxAlligatorDistance = 0;
   lastPriceIntersec = EMPTY_VALUE;

   addBuffer("jaws", DATA, false, false);
   addBuffer("teeth", DATA, false, false);
   addBuffer("lips", DATA, false, false);

   addBuffer("strength", DATA, true, false);
   addBuffer("angle", DATA, true, false);
   addBuffer("type", DATA, true, false);
   addBuffer("length", DATA, true, false);
   addBuffer("p2lips", DATA, true, false);

//--- assignment of arrays to indicator buffers
   //SetIndexBuffer(0, JawsBuffer, INDICATOR_DATA);
   //SetIndexBuffer(1, TeethBuffer, INDICATOR_DATA);
   //SetIndexBuffer(2, LipsBuffer, INDICATOR_DATA);

   //SetIndexBuffer(3, BufferStrength, INDICATOR_DATA);
   //SetIndexBuffer(4, BufferAngle, INDICATOR_DATA);
   //SetIndexBuffer(5, BufferType, INDICATOR_DATA);
   //SetIndexBuffer(6, BufferLength, INDICATOR_DATA);

//--- set shift of each line
   PlotIndexSetInteger(0, PLOT_SHIFT, jaw_shift);
   PlotIndexSetInteger(1, PLOT_SHIFT, teeth_shift);
   PlotIndexSetInteger(2, PLOT_SHIFT, lips_shift);


   handle=iAlligator(_Symbol, _Period, jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, MA_method, applied_price);
//--- if the handle is not created
   if(handle==INVALID_HANDLE) {
      //--- tell about the failure and output the error code
      PrintFormat("Failed to create handle of the iAlligator indicator for the symbol %s/%s, error code %d",
                  name, EnumToString(_Period), GetLastError());
      //--- the indicator is stopped early
      return(INIT_FAILED);
   }

   return IndicatorBase::OnInit(name);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::calculateBuffers()
{
   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);
   
   BufferIndicator *JawsBuffer = getBuffer("jaws");
   BufferIndicator *TeethBuffer = getBuffer("teeth");
   BufferIndicator *LipsBuffer = getBuffer("lips");

   BufferIndicator *BufferStrength = getBuffer("strength");
   BufferIndicator *BufferAngle = getBuffer("angle");
   BufferIndicator *BufferP2lips = getBuffer("p2lips");
   BufferIndicator *typeBuffer = getBuffer("type");
   BufferIndicator *lengthBuffer = getBuffer("length");

   //ArrayResize(BufferStrength.buffer, bars_calculated + 1);

   // BufferStrength
   double teethVal = TeethBuffer.buffer[bars_calculated];
   double lipsVal  = LipsBuffer.buffer[bars_calculated];

   double distance = teethVal > lipsVal ? teethVal-lipsVal : lipsVal-teethVal;
   BufferStrength.buffer[bars_calculated] = distance / maxAlligatorDistance;

   // Price to lips distance
   BufferP2lips.buffer[bars_calculated] = MathAbs(close[0] -  LipsBuffer.buffer[bars_calculated]) / _Point;
 
   // Last price intersec
   
   if (lastPriceIntersec == EMPTY_VALUE) {
      int limitHistoryBars = 1000;
      int size = ArraySize(JawsBuffer.buffer);
      int currentIndexFromZero = 0;

      for(int i=size-minBarsIntersec; i>=0; i--) {
         if (i<(size-limitHistoryBars)) {
            break;
         }
         double high_price = high[currentIndexFromZero]; 
         double low_price  = low[currentIndexFromZero];

         if (high_price > TeethBuffer.buffer[i] && low_price < TeethBuffer.buffer[i]) {
            lastPriceIntersec = currentIndexFromZero;
            lengthBuffer.buffer[i] = 0;
            break;
         }
         currentIndexFromZero++;
      }
   } else {
      double high_price = high[0];
      double low_price  = low[0];

      if (high_price > TeethBuffer.buffer[bars_calculated] && low_price < TeethBuffer.buffer[bars_calculated]) {
         lastPriceIntersec = 0;
      } else {
         lastPriceIntersec++;
      }
   }
   
   lengthBuffer.buffer[bars_calculated] = lastPriceIntersec != EMPTY_VALUE ? lastPriceIntersec : -1;


   // Angle
   BufferAngle.buffer[bars_calculated] = 0;
   if (lastPriceIntersec != EMPTY_VALUE && ArraySize(JawsBuffer.buffer) > lastPriceIntersec) {
      double iterations[];
      double prices[];
      double alligatorVals[];
      ArrayResize(iterations, lastPriceIntersec);
      ArrayResize(prices, lastPriceIntersec);
      ArrayResize(alligatorVals, lastPriceIntersec);

      for(int i=0; i<lastPriceIntersec; i++) {
         iterations[i] = i;

         if (teethVal < low[0]) {
            prices[i] = low[i];
         }
         if (teethVal > high[0]) {
            prices[i] = high[i];
         }

         alligatorVals[i] = JawsBuffer.buffer[bars_calculated - i];
      }

      if (lastPriceIntersec >= minBarsIntersec) {
         datetime t1 = time[0];
         datetime t2 = time[lastPriceIntersec];

         //double price_y1 = lastPriceIntersec > 4 ? LinearRegr(0, iterations, prices, lastPriceIntersec) : prices[0];
         double price_y1 = LinearRegr(0, iterations, prices, lastPriceIntersec);

         if (price_y1 > 0) {
            double a_y1 = LinearRegr(0, iterations, alligatorVals, lastPriceIntersec);
            double a_y2 = LinearRegr(lastPriceIntersec, iterations, alligatorVals, lastPriceIntersec);

            double price_y2 = (a_y2 + LinearRegr(lastPriceIntersec, iterations, prices, lastPriceIntersec)) / 2;

            if (a_y1 > 0 && a_y2 > 0) {
               double x = price_y2-price_y1;
               double y = a_y2-a_y1;

               double angle = MathArctan(x/y) * 180 / 3.1415926535;

               if (y > 0) {
                  angle -= 45;
               } else {
                  angle -= 45;
               }

               if (round(angle) != 45 && round(angle) != 0 && round(angle) > -100) {
                  BufferAngle.buffer[bars_calculated] = angle;
                  
                  DrawHelper::drawTrendLine(0, 0, "linePrice", t1, price_y1, t2, price_y2, clrAntiqueWhite);
                  DrawHelper::drawTrendLine(0, 0, "lineAlligator", t1, a_y1, t2, a_y2, angle > angleGoodColor ? clrWhite  : clrAqua, 0, angle > angleGoodColor ? 3  : 1);
               }
            }
         }
      }
   }

   // Draw
   //DrawHelper::drawLabel("labelIP", "LastIntersec price: " + IntegerToString(lastPriceIntersec), 20, 60);
   //DrawHelper::drawLabel("labelStrength", "Strength: " + DoubleToString(BufferStrength.buffer[bars_calculated], 2), 20, 80);
}

//+------------------------------------------------------------------+
//| Формула предлагаемая мной                                        |
//| Рассчет коэффициентов A и B в уравнении                          |
//| y(x)=A*x+B                                                       |
//| используються формулы https://forum.mql4.com/ru/10780/page5&nbsp;      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double AlligatorIndicator::LinearRegr(int newN, double &X[], double &Y[], int N)
{
   double a, b;

   double mo_X = 0.0, mo_Y = 0.0, var_0 = 0.0, var_1 = 0.0;

   for ( int i = 0; i < N; i ++ ) {
      mo_X +=X[i];
      mo_Y +=Y[i];
   }
   mo_X /=N;
   mo_Y /=N;

   for (int i = 0; i < N; i ++ ) {
      var_0 +=(X[i]-mo_X)*(Y[i]-mo_Y);
      var_1 +=(X[i]-mo_X)*(X[i]-mo_X);
   }
   a = var_0 / var_1;
   b = mo_Y - a * mo_X;

   return a*newN + b;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::calculateAlligatorDistancesFromHistory()
{
   if (minAlligatorDistance == EMPTY_VALUE && bars_calculated > historyBarsAnalizeCount) {
      //double Jaws[];
      double Teeth[];
      double Lips[];
      //ArraySetAsSeries(Jaws, true);
      ArraySetAsSeries(Teeth, true);
      ArraySetAsSeries(Lips, true);
      //CopyBuffer(handle, 5, 0, historyBarsAnalizeCount, Jaws);
      CopyBuffer(handle, 1, 0, historyBarsAnalizeCount, Teeth);
      CopyBuffer(handle, 2, 0, historyBarsAnalizeCount, Lips);

      for(int i=0; i<historyBarsAnalizeCount; i++) {
         //double jawVal = Jaws[i];
         double teethVal = Teeth[i];
         double lipsVal = Lips[i];

         double distance = teethVal > lipsVal ? teethVal-lipsVal : lipsVal-teethVal;

         if (minAlligatorDistance == EMPTY_VALUE || minAlligatorDistance > distance) {
            minAlligatorDistance = distance;
         }

         if (maxAlligatorDistance < distance) {
            maxAlligatorDistance = distance;
         }
      }
   }
   DrawHelper::drawLabel("labelAllDist", "max: " + DoubleToString(maxAlligatorDistance * 10000, 2), 20, 20);
   //DrawHelper::drawLabel("labelAllDist2", "min: " + DoubleToString(minAlligatorDistance * 10000, 2), 20, 40);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool AlligatorIndicator::FillArraysFromBuffers()
{
   ResetLastError();
//--- fill a part of the JawsBuffer array with values from the indicator buffer that has 0 index
   if(CopyBuffer(handle, 0, -jaw_shift, values_to_copy, getBuffer("jaws").buffer) < 0) {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
   }

//--- fill a part of the TeethBuffer array with values from the indicator buffer that has index 1
   if(CopyBuffer(handle, 1, -teeth_shift, values_to_copy, getBuffer("teeth").buffer) < 0) {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
   }

//--- fill a part of the LipsBuffer array with values from the indicator buffer that has index 2
   if(CopyBuffer(handle, 2, -lips_shift, values_to_copy, getBuffer("lips").buffer) < 0) {
      //--- if the copying fails, tell the error code
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
   }
//--- everything is fine
   return(true);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::onNewTick()
{


};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::onNewBar()
{
   if(!FillArraysFromBuffers()) {}

   calculateAlligatorDistancesFromHistory();
   calculateBuffers();
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AlligatorIndicator::OnDeinit(const int reason)
{
   IndicatorBase::OnDeinit(reason);
};
//+------------------------------------------------------------------+
