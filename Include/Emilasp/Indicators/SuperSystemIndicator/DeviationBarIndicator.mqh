//+------------------------------------------------------------------+
//|                                             MacdIndicator.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\Indicators\SuperSystemIndicator\Common\IndicatorBase.mqh>

enum ENUM_CLOSE_TYPES {
   ALL=1,
   ONLY_ONE_SECTOR=2,
   ONLY_DIFFERENT_SECTOR=3,
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class DeviationBarIndicator : public IndicatorBase
{
private:
   double         BufferSignal[];
   double         BufferOpenType[];
   double         BufferPrevBarType[];

   ENUM_CLOSE_TYPES prevBarType;
   double minCloseFilterPercent;

   double lastClosePercent;

protected:
   void onNewTick();
   void onNewBar();

public:
   int OnInit(string name, ENUM_CLOSE_TYPES _prevBarType, double _minCloseFilterPercent);
   void OnDeinit(const int reason);
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int DeviationBarIndicator::OnInit(string name, ENUM_CLOSE_TYPES _prevBarType, double _minCloseFilterPercente)
{
   prevBarType = _prevBarType;
   minCloseFilterPercent = _minCloseFilterPercente;
   lastClosePercent = 0;



//--- assignment of arrays to indicator buffers
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferOpenType, INDICATOR_DATA);
   SetIndexBuffer(2, BufferPrevBarType, INDICATOR_DATA);
   
   ArraySetAsSeries(BufferSignal, true);
   ArraySetAsSeries(BufferOpenType, true);
   
   return IndicatorBase::OnInit(name, false);
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DeviationBarIndicator::onNewTick()
{
   BufferSignal[0] = 0;
   BufferOpenType[0] = 0;
   //BufferPrevBarType[i] = 0;

   ResetLastError();

   double fullHeight = high[0] - low[0];
   double closeHeight = close[0] -low[0];
   double openHeight = open[0] -low[0];

   if (fullHeight > 0) {
      double openPercent = openHeight / fullHeight;
      double closePercent = closeHeight / fullHeight;
      lastClosePercent = closePercent;

      if (low[0] < low[1] && low[0] < low[2]) {
         if (closePercent > minCloseFilterPercent) {
            BufferSignal[0] = closePercent * -1;
            BufferOpenType[0] = openPercent > 0.5 ? 1 : 2;
         }
      }

      if (high[0] > high[1] && high[0] > high[2]) {
         if ((1 - closePercent) > minCloseFilterPercent) {
            BufferSignal[0] = 1 - closePercent;
            BufferOpenType[0] = openPercent < 0.5 ? 1 : 2;
         }
      }


      // Filter
      /*if (BufferSignal[i] != 0) {
         if (prevBarType == ONLY_ONE_SECTOR && BufferOpenType[i] != 1) {
            BufferSignal[i] = 0;
         }
         if (prevBarType == ONLY_DIFFERENT_SECTOR && BufferOpenType[i] != 2) {
            BufferSignal[i] = 0;
         }
      }*/


      if (BufferSignal[0] != 0) {
         if (BufferSignal[0] > 0) {
            DrawHelper::drawArrow("long:price:" + IntegerToString(bars_calculated), time[0], high[0] + 30 * _Point, 172, clrRed, 0, 0, 14);
         }
         if (BufferSignal[0] < 0) {
            DrawHelper::drawArrow("short:price:" + IntegerToString(bars_calculated), time[0], low[0] - 15 * _Point, 172, clrGreen, 0, 0, 14);
         }

         DrawHelper::drawLabel("labelSignal", "signal: " + DoubleToString(BufferSignal[0], 2), 20, 20);
         DrawHelper::drawLabel("label1", "close(p): " + DoubleToString(lastClosePercent, 2), 20, 40);
         DrawHelper::drawLabel("label2", "min percent: " + DoubleToString(minCloseFilterPercent, 2), 20, 60);
      }
   }

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DeviationBarIndicator::onNewBar()
{

};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DeviationBarIndicator::OnDeinit(const int reason)
{
   IndicatorBase::OnDeinit(reason);
};
//+------------------------------------------------------------------+
