//+------------------------------------------------------------------+
//|                                             IndicatorBase.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\Indicators\SuperSystemIndicator\Common\BufferIndicator.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class IndicatorBase
{
private:
   string short_name;
   datetime timeLast;
   bool isHandleIndicator;
   CArrayObj buffers;


protected:
   int handle;
   int bars_calculated;

   int rates_total;
   int prev_calculated;
   datetime time[];
   double open[];
   double high[];
   double low[];
   double close[];
   long tick_volume[];
   long volume[];
   int spread[];

   int values_to_copy;
   datetime       date_start;

   virtual void onNewTick() {};
   virtual void onNewBar() {};

public:
   int OnInit(string name, bool _isHandleIndicator = true);
   int OnCalculate(const int _rates_total, const int _prev_calculated, const datetime &_time[], const double &_open[], const double &_high[], const double &_low[], const double &_close[], const long &_tick_volume[], const long &_volume[], const int &_spread[]);
   void OnDeinit(const int reason);
   int addBuffer(string name, ENUM_BUFFER_TYPES type, bool drawOnPanel, bool drawOnChart);
   BufferIndicator *getBuffer(string name);
   BufferIndicator *getBuffer(int num);

   void drawPanel();
};
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void IndicatorBase::drawPanel()
{
   for(int i=0; i<buffers.Total(); i++) {
      BufferIndicator *indicator = buffers.At(i);
      if(ArraySize(indicator.buffer) > 0) {
         if (indicator.drawOnPanel) {
            DrawHelper::drawLabel(indicator.name, indicator.name + ": " + DoubleToString(indicator.getValue(0), 2), 20, i * 20 + 150, 1);
         }
         if (indicator.drawOnChart) {
            DrawHelper::drawArrow(indicator.name, time[0], indicator.getValue(0), 159, clrGreen);
         }
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int IndicatorBase::addBuffer(string name, ENUM_BUFFER_TYPES type, bool drawOnPanel, bool drawOnChart)
{
   BufferIndicator *indicator = new BufferIndicator(name, type, drawOnPanel, drawOnChart);

   SetIndexBuffer(buffers.Total(), indicator.buffer, INDICATOR_DATA);

   buffers.Add(indicator);

   return buffers.Total();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BufferIndicator *IndicatorBase::getBuffer(string name)
{
   for(int i=0; i<buffers.Total(); i++) {
      BufferIndicator *indicator = buffers.At(i);
      if (indicator.name == name) {
         return indicator;
      }
   }
   return NULL;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BufferIndicator *IndicatorBase::getBuffer(int num)
{
   return buffers.At(num);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int IndicatorBase::OnInit(string name, bool _isHandleIndicator = true)
{
   bars_calculated = 0;
   short_name=name;
   isHandleIndicator = _isHandleIndicator;

   date_start=D'01.11.2019';
   timeLast = EMPTY_VALUE;

   ArraySetAsSeries(time, true);
   ArraySetAsSeries(open, true);
   ArraySetAsSeries(high, true);
   ArraySetAsSeries(low, true);
   ArraySetAsSeries(close, true);
   ArraySetAsSeries(tick_volume, true);
   ArraySetAsSeries(volume, true);
   ArraySetAsSeries(spread, true);

   //buffers = new CArrayObj;

   return INIT_SUCCEEDED;
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int IndicatorBase::OnCalculate(const int _rates_total, const int _prev_calculated, const datetime &_time[], const double &_open[], const double &_high[], const double &_low[], const double &_close[], const long &_tick_volume[], const long &_volume[], const int &_spread[])
{
   //if(time[i]<date_start)
   //continue;

   rates_total = _rates_total;
   prev_calculated = _prev_calculated;

   //time = _time;

   //ArrayFree(time);
   //ArrayFree(open);
   //ArrayFree(high);
   //ArrayFree(low);
   //ArrayFree(close);
   //ArrayFree(tick_volume);
   //ArrayFree(volume);
   //ArrayFree(spread);

   //ArrayResize(time, ArraySize(_time) + 1);
   //ArrayResize(open, ArraySize(_open) + 1);
   //ArrayResize(high, ArraySize(_high) + 1);
   //ArrayResize(low, ArraySize(_low) + 1);
   //ArrayResize(close, ArraySize(_close) + 1);
   //ArrayResize(tick_volume, ArraySize(_tick_volume) + 1);
   //ArrayResize(volume, ArraySize(_volume) + 1);
   //ArrayResize(spread, ArraySize(_spread) + 1);

   int copyLastCount = 1000;
   ArrayResize(time, copyLastCount);
   ArrayResize(open, copyLastCount);
   ArrayResize(high, copyLastCount);
   ArrayResize(low, copyLastCount);
   ArrayResize(close, copyLastCount);
   ArrayResize(tick_volume, copyLastCount);
   ArrayResize(volume, copyLastCount);
   ArrayResize(spread, copyLastCount);

   ArrayCopy(time, _time, 0, ArraySize(_time) - copyLastCount);
   ArrayCopy(open, _open, 0, ArraySize(_open) - copyLastCount);
   ArrayCopy(high, _high, 0, ArraySize(_high) - copyLastCount);
   ArrayCopy(low, _low, 0, ArraySize(_low) - copyLastCount);
   ArrayCopy(close, _close, 0, ArraySize(_close) - copyLastCount);
   ArrayCopy(tick_volume, _tick_volume, 0, ArraySize(_tick_volume) - copyLastCount);
   ArrayCopy(volume, _volume, 0, ArraySize(_volume) - copyLastCount);
   ArrayCopy(spread, _spread, 0, ArraySize(_spread) - copyLastCount);

   ArrayRemove(time, 0, 1);
   ArrayRemove(open, 0, 1);
   ArrayRemove(high, 0, 1);
   ArrayRemove(low, 0, 1);
   ArrayRemove(close, 0, 1);
   ArrayRemove(tick_volume, 0, 1);
   ArrayRemove(volume, 0, 1);
   ArrayRemove(spread, 0, 1);

   int calculated;
   if (isHandleIndicator) {
      calculated=BarsCalculated(handle);
      if(calculated<=0) {
         PrintFormat("BarsCalculated() returned %d, error code %d", calculated, GetLastError());
         return(0);
      }
   } else {
      calculated = prev_calculated;

      //for(int i=0; i<buffers.Total(); i++) {
      //   BufferIndicator *indicator = buffers.At(i);
      //   ArrayResize(indicator.buffer, calculated);
      //}
   }

//--- if it is the first start of calculation of the indicator or if the number of values in the iAlligator indicator changed
//---or if it is necessary to calculated the indicator for two or more bars (it means something has changed in the price history)
   if(prev_calculated==0 || calculated!=bars_calculated || rates_total>prev_calculated+1) {
      //--- if the JawsBuffer array is greater than the number of values in the iAlligator indicator for symbol/period, then we don't copy everything
      //--- otherwise, we copy less than the size of indicator buffers
      if(calculated>rates_total)
         values_to_copy=rates_total;
      else
         values_to_copy=calculated;
   } else {
      //--- it means that it's not the first time of the indicator calculation, and since the last call of OnCalculate()
      //--- for calculation not more than one bar is added
      values_to_copy=(rates_total-prev_calculated)+1;
   }

//--- fill the arrays with values of the Alligator indicator
//--- if FillArraysFromBuffer returns false, it means the information is nor ready yet, quit operation


   onNewTick();
   drawPanel();

   // NEW BAR
   if(rates_total - prev_calculated == 1 && (timeLast == EMPTY_VALUE || timeLast != time[0])) {
      timeLast = time[0];
      onNewBar();
   }


//--- display the service message on the chart
   //Comment(StringFormat("%s ==>  Updated value in the indicator %s: %d", TimeToString(TimeCurrent(), TIME_DATE|TIME_SECONDS), short_name, values_to_copy));
//--- memorize the number of values in the Alligator indicator
   bars_calculated=calculated;
//--- return the prev_calculated value for the next call
   return(rates_total);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void IndicatorBase::OnDeinit(const int reason)
{

};
//+------------------------------------------------------------------+
