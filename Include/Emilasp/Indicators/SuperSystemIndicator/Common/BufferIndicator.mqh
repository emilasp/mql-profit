//+------------------------------------------------------------------+
//|                                             IndicatorBase.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>

enum ENUM_BUFFER_TYPES {
   DATA=1
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class BufferIndicator : public CObject
{
private:
   
protected:

   
public:
   string name;
   ENUM_BUFFER_TYPES type;
   
   double buffer[];
   
   bool drawOnPanel;
   bool drawOnChart;
   
   int arrowId;
   
   
   void BufferIndicator(string _name, ENUM_BUFFER_TYPES _type, bool _drawOnPanel, bool _drawOnChart);
   double getValue(int index);
};
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void BufferIndicator::BufferIndicator(string _name, ENUM_BUFFER_TYPES _type, bool _drawOnPanel, bool _drawOnChart)
{
   name=_name;
   type=_type;
   drawOnPanel=_drawOnPanel;
   drawOnChart=_drawOnChart;
   
   arrowId=219;
};

double BufferIndicator::getValue(int index)
{
   return buffer[ArraySize(buffer) - (1 + index)];
}
