//+------------------------------------------------------------------+
//|                                               CoreTradeHadge.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayLong.mqh>
#include <Trade\SymbolInfo.mqh>
#include <Emilasp\trade\CoreTrade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CoreTradeHadge : public CoreTrade
{
private:

public:
                     CoreTradeHadge(ENUM_TIMEFRAMES _timeframe, int _magic);
                    ~CoreTradeHadge();

   int               getPositionCount(ENUM_POSITION_TYPE typePosition);
   CArrayLong        *getTicketPositions(ENUM_POSITION_TYPE typePosition);
   int               closePositions(ENUM_POSITION_TYPE typePosition);
   bool              TrallingStop(int tralPt);
   bool              TrallingStop(int tralPt, ENUM_POSITION_TYPE positionType);

   void              OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTradeHadge::CoreTradeHadge(ENUM_TIMEFRAMES _timeframe, int _magic)
   :CoreTrade()
{
   Init(_timeframe, _magic);

   interruptionOfLossesEnabled = false;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTradeHadge::~CoreTradeHadge()
{
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CArrayLong *CoreTradeHadge::getTicketPositions(ENUM_POSITION_TYPE typePosition)
{
   CArrayLong *tickets = new CArrayLong();
   int total=PositionsTotal();
   int count = 0;
   for(int i=0; i<total; i++) {
      ulong ticket=PositionGetTicket(i);
      if(PositionSelectByTicket(ticket))
         if(PositionGetInteger(POSITION_MAGIC)==MagicNumber)
            if(PositionGetString(POSITION_SYMBOL) == _Symbol)
               if(PositionGetInteger(POSITION_TYPE)==typePosition)
                  tickets.Add(ticket);
   }

   return tickets;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int CoreTradeHadge::getPositionCount(ENUM_POSITION_TYPE typePosition)
{
   CArrayLong tickets = getTicketPositions(typePosition);
   return tickets.Total();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int CoreTradeHadge::closePositions(ENUM_POSITION_TYPE typePosition)
{
   CArrayLong tickets = getTicketPositions(typePosition);

   int count = tickets.Total();
   for(int i=0; i<count; i++)
      PositionClose(tickets.At(i));

   return count;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTradeHadge::TrallingStop(int tralPt, ENUM_POSITION_TYPE positionType)
{
   bool res = true;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);
   double price = PositionGetDouble(POSITION_PRICE_OPEN);

   if(positionType == POSITION_TYPE_BUY) {
      CArrayLong ticketsBuy = getTicketPositions(POSITION_TYPE_BUY);
      for(int i=0; i < ticketsBuy.Total(); i++) {
         ulong ticket = ticketsBuy.At(i);
         if(PositionSelectByTicket(ticket))
            if(PositionGetDouble(POSITION_PROFIT) > 0)
               if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY) {
                  double price=last_tick.bid - tralPt * _Point;
                  if(PositionGetDouble(POSITION_PRICE_OPEN) < price)
                     if(PositionGetDouble(POSITION_SL) < price)
                        PositionModify(ticket, price, 0);
               }
      }

   }
   if(positionType == POSITION_TYPE_SELL) {
      CArrayLong ticketsSell = getTicketPositions(POSITION_TYPE_SELL);
      for(int i=0; i < ticketsSell.Total(); i++) {
         ulong ticket = ticketsSell.At(i);
         if(PositionSelectByTicket(ticket))
            if(PositionGetDouble(POSITION_PROFIT) > 0)
               if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL) {
                  double price=last_tick.ask + tralPt * _Point;
                  if(PositionGetDouble(POSITION_PRICE_OPEN) > price)
                     if(PositionGetDouble(POSITION_SL) > price || PositionGetDouble(POSITION_SL) == 0)
                        PositionModify(ticket, price, 0);
               }
      }
   }

   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTradeHadge::TrallingStop(int tralPt)
{
   TrallingStop(tralPt, POSITION_TYPE_BUY);
   TrallingStop(tralPt, POSITION_TYPE_SELL);
   
   return true;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void CoreTradeHadge::OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   if(trans.type!=TRADE_TRANSACTION_DEAL_ADD)return;
   if(!HistoryDealSelect(trans.deal))return;

   int Magic = HistoryDealGetInteger(trans.deal, DEAL_MAGIC);

//if(HistoryDealGetInteger(trans.deal, DEAL_MAGIC)!=trade.MagicNumber)return;
   if(HistoryDealGetInteger(trans.deal, DEAL_ENTRY)!=DEAL_ENTRY_OUT)return;
   if(HistoryDealGetString(trans.deal, DEAL_SYMBOL)!=_Symbol)return;
   long reason=HistoryDealGetInteger(trans.deal, DEAL_REASON);
   if(reason==DEAL_REASON_SL) {
      // Stop Loss
   } else if(reason==DEAL_REASON_TP) {
      // Take Profit
   }

   double profit = HistoryDealGetDouble(trans.deal, DEAL_PROFIT);

   if (profit < 0) {
      tradeStat.addLoss(profit);
   } else if (profit > 0) {
      tradeStat.addProfit(profit);
   }

   bool isLoss = isInterruptionOfLosses();
}
//+------------------------------------------------------------------+
