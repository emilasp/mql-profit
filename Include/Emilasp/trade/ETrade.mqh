//+------------------------------------------------------------------+
//|                                                       ETrade.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ETrade : public CTrade
  {
private:
   int               magicNumber;
   int               BARS;

public:
                     ETrade(int magic);
                    ~ETrade();
   bool              INIT();
   double            getAddPricePt(double price, int pt);
   void              onTick();
   bool              isNewBar();

   CSymbolInfo        m_symbol;
   CPositionInfo      m_position;
   CAccountInfo       m_account;
   CTrade             m_trade;


   double            point;

  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ETrade::ETrade(int magic)
  {
   magicNumber = magic;

   INIT();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ETrade::~ETrade()
  {
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool             ETrade::INIT(void)
  {
   m_symbol.Name(Symbol());
   if(!m_symbol.RefreshRates())
     {
      Print("Error RefreshRates. Bid=", DoubleToString(m_symbol.Bid(), Digits()),
            ", Ask=", DoubleToString(m_symbol.Ask(), Digits()));
      return(false);
     }

   m_trade.SetExpertMagicNumber(magicNumber);
   m_trade.SetMarginMode();
   m_trade.SetTypeFillingBySymbol(Symbol());

   int digits = 1;
   if(m_symbol.Digits() == 3 || m_symbol.Digits() == 5)
      digits = 10;

   point = m_symbol.Point() * digits;
   m_trade.SetDeviationInPoints(3 * digits);

   return(true);
  }





//+------------------------------------------------------------------+
double ETrade::getAddPricePt(double price, int pt)
  {
//m_symbol.RefreshRates();
   int digits = 1;
   if(m_symbol.Digits() == 3 || m_symbol.Digits() == 5)
      digits = 10;

   double point = m_symbol.Point() * digits;
   return price + pt * point;
  }

//+------------------------------------------------------------------+
void ETrade::onTick(void)
  {
   m_symbol.RefreshRates();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ETrade::isNewBar()
  {
   static datetime LastBar = 0;
   datetime ThisBar = (datetime)SeriesInfoInteger(_Symbol,PERIOD_CURRENT,SERIES_LASTBAR_DATE);
   if(LastBar != ThisBar)
     {
      LastBar = ThisBar;
      return true;
     }
   return false;
  }
//+------------------------------------------------------------------+
