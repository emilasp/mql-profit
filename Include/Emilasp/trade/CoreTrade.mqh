//+------------------------------------------------------------------+
//|                                                    CoreTrade.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

#include <Emilasp\libraries\Vendors\comment.mqh>


struct TradesStat {
   int               all;
   int               profitsCount;
   int               lossesCount;
   int               continuousLossesCount;
   int               continuousProfitsCount;

   double            profit;
   double            profitLoss;
   double            profitProfit;

   void              TradesStat()
   {
      all = 0;
      profitsCount = 0;
      lossesCount = 0;
      continuousLossesCount = 0;
      continuousProfitsCount = 0;

      profit = 0;
      profitLoss = 0;
      profitProfit = 0;
   }

   void              addProfit(double _profit)
   {
      all++;
      profitsCount++;
      continuousProfitsCount++;
      continuousLossesCount = 0;
      profit += _profit;
      profitProfit += _profit;
   }

   void              addLoss(double _profit)
   {
      all++;
      lossesCount++;
      continuousLossesCount++;
      continuousProfitsCount = 0;
      profit += _profit;
      profitLoss += _profit;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CoreTrade : public CTrade
{
private:
   int               deviation;       //--- установим допустимое проскальзывание в пунктах при совершении покупки/продажи
   datetime          lastBar[10];

   CComment comment;

protected:
   TradesStat        tradeStat;
   bool              isTrailingStop;
   bool              isBreakevenSl;
   int               trailingStopTR;
   bool              trailingStopRemoveTP;


   bool              interruptionOfLossesEnabled;
   int               interruptionOfLossesMaxCount;
   int               interruptionOfLossesMinuts;
   datetime          interruptionOfLossesBannedToDate;


public:
   CoreTrade();
   ~CoreTrade();

   int               MagicNumber; //--- зададим MagicNumber для идентификации своих ордеров

   bool              Init(ENUM_TIMEFRAMES _timeframe, int _magic, int _deviation = 10);

   bool              Buy(double volume, int takeProfit, int stopLoss, string comment = NULL);
   bool              Sell(double volume, int takeProfit, int stopLoss, string comment = NULL);
   bool              BuyStop(string symbol, double volume, double price, double takeProfit, double stopLoss, string comment = NULL);
   bool              SellStop(string symbol, double volume, double price, double takeProfit, double stopLoss, string comment = NULL);
   double            calcTakeProfit(ENUM_POSITION_TYPE type, int takeProfit);
   double            calcStopLoss(ENUM_POSITION_TYPE type, int stopLoss);
   bool              RefreshRates();
   int               getPositionCount();
   int               accountInit();
   bool              isNewBar(int key);
   void              trailingStopInit(int _trailingStopTR, bool _trailingStopRemoveTP);
   void              breakevenSlEnable(bool eanbled);
   void              trailingStopRun();
   void              breakevenSlRun();

   /** Ограничение убытков: запрещаем торговать minuts минут, если было более maxLosses убыточных сделок подряд **/
   void              interruptionOfLossesInit(int maxLosses, int minuts);
   bool              isInterruptionOfLosses();
   bool              isInterruptionOfLossesDateBanned();

   /** Martingale **/
   bool              isMartingale;
   void              martingaleInit(double lotAdded);
   double            getLot(double lot, int historyDays = 2);
   double getTickPriceBidOrAskForTrade(ENUM_POSITION_TYPE positionType, MqlTick &tick);

   void              OnTick();

   //openPositionBuy();
   //openPositionSell();
   //переменная для хранения символа
   ENUM_TIMEFRAMES   timeframe;                                    //переменная для хранения таймфрейма
   CPositionInfo     m_Position;    // структура для получения информации о позициях
   CAccountInfo      m_Account;     // объект для работы со счетом
   CTrade            m_Trade;       // структура для выполнения торговых операций
   CSymbolInfo       m_symbol;
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTrade::CoreTrade()
{
//tradeStat = TradesStat();
   isBreakevenSl = false;
   isTrailingStop = false;
   isMartingale   = false;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTrade::~CoreTrade()
{
}
//+------------------------------------------------------------------+
bool CoreTrade::Init(ENUM_TIMEFRAMES _timeframe, int _magic, int _deviation = 10)
{
   m_symbol.Name(Symbol());
   RefreshRates();

   timeframe = _timeframe;

   MagicNumber=_magic;
   deviation=_deviation;

   ArrayFill(lastBar, 0, 9, 0);

// Устанваливаем преднастройки для торговли
   m_Trade.SetExpertMagicNumber(MagicNumber);
   m_Trade.SetDeviationInPoints(deviation);           // Устанавливаем допустимое проскальзывание
   m_Trade.SetTypeFilling(ORDER_FILLING_RETURN);      //--- режим заполнения ордера, нужно использовать тот режим, который разрешается сервером
   m_Trade.SetAsyncMode(true);                        //--- какую функцию использовать для торговли: true - OrderSendAsync(), false - OrderSend()
   m_Trade.SetMarginMode();                           // Устанавливаем режим расчета маржи в соответствии с настройками текущего счета.
   m_Trade.SetTypeFillingBySymbol(m_symbol.Name());     // Устанавливаем тип ордера по исполнению согласно настройкам указанного символа

   return true;
}

//+------------------------------------------------------------------+
void CoreTrade::OnTick()
{
   trailingStopRun();
   breakevenSlRun();

   if(isNewBar(6)) {
      //DrawHelper::drawLabel("LB", (lastBar[1] ? TimeToString(lastBar[1]) : " - "), 10, 10, 0, 0);
      //DrawHelper::drawLabel("TInfo1", tradesInfo, 10, 20, 0, 0);
      //DrawHelper::drawLabel("TInfo2", tradesInfo2, 10, 30, 0, 0);
      //DrawHelper::drawLabel("TInfo3", tradesInfo3, 10, 40, 0, 0);
      //DrawHelper::drawLabel("TInfo4", "Lot: " + IntegerToString(getLot(0.1)), 10, 50, 0, 0);

      string lines[4];

      lines[0] = StringFormat("Trades: all(%s),  +%s, -%s", IntegerToString(tradeStat.all), IntegerToString(tradeStat.profitsCount), IntegerToString(tradeStat.lossesCount) + "[" + IntegerToString(tradeStat.continuousLossesCount) + "]");
      lines[1] = StringFormat("Profit: all(%s),  +%s, %s", DoubleToString(tradeStat.profit, 2), DoubleToString(tradeStat.profitProfit, 2), DoubleToString(tradeStat.profitLoss, 2));
      lines[2] = "Loss break: "+ (interruptionOfLossesBannedToDate != NULL ? TimeToString(interruptionOfLossesBannedToDate, TIME_MINUTES|TIME_SECONDS) : " - ");
      lines[3] = "Price: "+DoubleToString(SymbolInfoDouble(_Symbol, SYMBOL_BID), _Digits);


      DrawHelper::drawInfoPanel(comment, "Base panel", lines, 0, 20, 30);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTrade::Buy(double volume, int takeProfit, int stopLoss, string comment = NULL)
{
   if (interruptionOfLossesEnabled && isInterruptionOfLossesDateBanned())
      return(false);

   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }

   volume = getLot(volume);

   double price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_ASK);
   double price_SL = price-stopLoss*_Point;
   double price_TP = price+takeProfit*_Point;

   return Buy(volume, m_symbol.Name(), price, price_SL, price_TP, comment);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTrade::Sell(double volume, int takeProfit, int stopLoss, string comment = NULL)
{
   if (interruptionOfLossesEnabled && isInterruptionOfLossesDateBanned())
      return(false);

   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }

   volume = getLot(volume);

   double price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_BID);
   double price_SL = price+stopLoss*_Point;
   double price_TP = price-takeProfit*_Point;

   return Sell(volume, m_symbol.Name(), price, price_SL, price_TP, comment);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTrade::BuyStop(string symbol, double volume, double price, double takeProfit, double stopLoss, string comment = NULL)
{
   if (interruptionOfLossesEnabled && isInterruptionOfLossesDateBanned())
      return(false);

   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }

   volume = getLot(volume);

   return BuyStop(volume, price, symbol, stopLoss, takeProfit);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTrade::SellStop(string symbol, double volume, double price, double takeProfit, double stopLoss, string comment = NULL)
{
   if (interruptionOfLossesEnabled && isInterruptionOfLossesDateBanned())
      return(false);

   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }

   volume = getLot(volume);

   return SellStop(volume, price, symbol, takeProfit, stopLoss);
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CoreTrade::calcTakeProfit(ENUM_POSITION_TYPE type, int takeProfit)
{
   double price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_ASK);
   double price_TP = price+takeProfit*_Point;

   if(type == POSITION_TYPE_SELL) {
      price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_BID);
      price_TP = price-takeProfit*_Point;
   }

   return price_TP;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CoreTrade::calcStopLoss(ENUM_POSITION_TYPE type, int stopLoss)
{
   double price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_ASK);
   double price_SL = price-stopLoss*_Point;

   if(type == POSITION_TYPE_SELL) {
      price    = SymbolInfoDouble(m_symbol.Name(), SYMBOL_BID);
      price_SL = price+stopLoss*_Point;
   }
   return price_SL;
}




// Accaunt info
int CoreTrade::accountInit()
{
//--- получим номер счета, на котором запущен советник
   long login=m_Account.Login();
   Print("Login=", login);
//--- выясним тип счета
   ENUM_ACCOUNT_TRADE_MODE account_type=m_Account.TradeMode();
//--- если счет оказался реальным, прекращаем работу эксперта немедленно!
   if(account_type==ACCOUNT_TRADE_MODE_REAL) {
      MessageBox("Работа на реальном счете запрещена, выходим", "Эксперт запущен на реальном счете!");
      return(-1);
   }
//--- выведем тип счета
   Print("Тип счета: ", EnumToString(account_type));
//--- выясним, можно ли вообще торговать на данном счете
   if(m_Account.TradeAllowed())
      Print("Торговля на данном счете разрешена");
   else
      Print("Торговля на счете запрещена: возможно, вход был совершен по инвест-паролю");
//--- выясним, разрешено ли торговать на счете с помощью эксперта
   if(m_Account.TradeExpert())
      Print("Автоматическая торговля на счете разрешена");
   else
      Print("Запрещена автоматическая торговля с помощью экспертов и скриптов");
//--- допустимое количество ордеров задано или нет
   int orders_limit=m_Account.LimitOrders();
   if(orders_limit!=0)
      Print("Максимально допустимое количество действующих отложенных ордеров: ", orders_limit);
//--- выведем имя компании и сервера
   Print(m_Account.Company(), ": server ", m_Account.Server());
//--- напоследок выведем баланс и текущую прибыль на счете
   Print("Balance=", m_Account.Balance(), "  Profit=", m_Account.Profit(), "   Equity=", m_Account.Equity());
   Print(__FUNCTION__, "  completed"); //---
   return 1;
}




// Обновляем котировки текущей пары
bool CoreTrade::RefreshRates()
{
   if(!m_symbol.RefreshRates()) {
      Print("Не удалось обновить котировки валютной пары!");
      return false;
   }

   if(m_symbol.Ask() == 0 || m_symbol.Bid() == 0) {
      return false;
   }
   return true;
}


/**
* Сколько позиций уже открыто
**/
int CoreTrade::getPositionCount()
{
   int count = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--) {
      if(m_Position.SelectByIndex(i)) {
         int pos =  m_Position.Magic();
         string sym1 = m_symbol.Name();
         string sym2 = m_Position.Symbol();

         if(m_Position.Symbol() == m_symbol.Name() && m_Position.Magic() == MagicNumber)
            count++;
      }
   }
   return count;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CoreTrade::isNewBar(int key)
{
   datetime currentBar = (datetime)SeriesInfoInteger(_Symbol, PERIOD_CURRENT, SERIES_LASTBAR_DATE);
   if(lastBar[key] != currentBar) {
      lastBar[key] = currentBar;
      return true;
   }
   return false;
}


//---------------------------------------------------------------------
//  Возвращает признак появления нового бара:
//---------------------------------------------------------------------
//  - если возвращает 1, то есть новый  бар;
//---------------------------------------------------------------------
//int CheckNewBar()
//{
//  MqlRates  current_rates[1];
//
//  ResetLastError();
//  if(CopyRates(Symbol(), Period(), 0, 1, current_rates)!= 1)
//  {
//    Print("Ошибка копирования CopyRates, Код = ", GetLastError());
//    return(0);
//  }
//
//  if(current_rates[0].tick_volume>1)
//  {
//    return(0);
//  }
//
//  return(1);
//}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreTrade::breakevenSlRun()
{
}
//+------------------------------------------------------------------+
void CoreTrade::trailingStopRun()
{
   if(isTrailingStop && PositionSelect(_Symbol)) {
      if(PositionGetDouble(POSITION_PROFIT)) {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);

         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price + trailingStopTR*_Point) {
               price = last_tick.bid - trailingStopTR*_Point;
               if(price > PositionGetDouble(POSITION_SL)) {
                  PositionModify(_Symbol, price, trailingStopRemoveTP ? 0 : PositionGetDouble(POSITION_TP));

                  isBreakevenSl = false;
               }
            }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
            if(last_tick.ask<price - trailingStopTR*_Point) {
               price = last_tick.ask + trailingStopTR*_Point;
               if(price < PositionGetDouble(POSITION_SL) || PositionGetDouble(POSITION_SL) == NULL) {
                  PositionModify(_Symbol, price, trailingStopRemoveTP ? 0 : PositionGetDouble(POSITION_TP));

                  isBreakevenSl = false;
               }
            }
         }
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreTrade::trailingStopInit(int _trailingStopTR, bool _trailingStopRemoveTP)
{
   isTrailingStop = true;
   trailingStopTR = _trailingStopTR;
   trailingStopRemoveTP = _trailingStopRemoveTP;
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreTrade::breakevenSlEnable(bool eanbled)
{
   isBreakevenSl = eanbled;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreTrade::interruptionOfLossesInit(int maxLosses, int minuts)
{
   interruptionOfLossesEnabled = true;
   interruptionOfLossesMaxCount = maxLosses;
   interruptionOfLossesMinuts = minuts;
}




//+------------------------------------------------------------------+
//|      Прерывание убытков: проверяем достигли ли мы максимального чилса убыточных сделок подряд
//|      И запускаем бан                                                        |
//+------------------------------------------------------------------+
bool CoreTrade::isInterruptionOfLosses()
{
   if (interruptionOfLossesEnabled && tradeStat.continuousLossesCount > interruptionOfLossesMaxCount) {
      tradeStat.continuousLossesCount = 0;

      interruptionOfLossesBannedToDate = TimeCurrent() + interruptionOfLossesMinuts * 60;
      return true;
   }
   return false;
}

//+------------------------------------------------------------------+
//|     Прерывание убытков: проверяем есть ли бан по времени         |
//+------------------------------------------------------------------+
bool CoreTrade::isInterruptionOfLossesDateBanned()
{
   if (interruptionOfLossesBannedToDate != NULL) {
      if (interruptionOfLossesBannedToDate > TimeCurrent()) {
         return true;
      } else {
         interruptionOfLossesBannedToDate = NULL;
      }
   }
   return false;
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreTrade::martingaleInit(double lotAdded)
{
   isMartingale = true;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CoreTrade::getLot(double lot, int historyDays = 2)
{
   if (isMartingale) {
      bool ord;
      double TotalLot=0;
      HistorySelect(TimeCurrent()-historyDays*86400, TimeCurrent());
      for ( int i=HistoryDealsTotal()-1; i>=0; i-- ) {
         ulong ticket=HistoryDealGetTicket(i);
         ord=HistoryDealSelect(ticket);

         int magic = HistoryDealGetInteger(ticket, DEAL_MAGIC);

         if ( ord && HistoryDealGetString(ticket, DEAL_SYMBOL) == _Symbol
              //&& HistoryDealGetInteger(ticket, DEAL_MAGIC) == MagicNumber
              && HistoryDealGetInteger(ticket, DEAL_ENTRY) == DEAL_ENTRY_OUT ) {
            if ( HistoryDealGetDouble(ticket, DEAL_PROFIT) < 0 ) {
               TotalLot+=HistoryDealGetDouble(ticket, DEAL_VOLUME);
            } else {
               break;
            }
         }
      }
      return TotalLot == 0 ? lot: TotalLot;
   }
   return lot;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
double CoreTrade::getTickPriceBidOrAskForTrade(ENUM_POSITION_TYPE positionType, MqlTick &tick)
{
   if(positionType == POSITION_TYPE_BUY)
      return tick.bid;
   return tick.ask;
}
//+------------------------------------------------------------------+
