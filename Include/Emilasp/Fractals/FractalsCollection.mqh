//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2020, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+


enum ENUM_FRACTAL_TYPES {
   FRACTAL_TYPE_THREE=3,
   FRACTAL_TYPE_FIVE=5,
   FRACTAL_TYPE_FIVE_STRICT=6,
};

struct Fractal {
   bool isEmpty;
   ENUM_FRACTAL_TYPES type;
   int direct;
   int index;
   datetime time;
   double price;
   double high;
   double low;
   
   void Fractal()
   {
      direct=0;
      isEmpty=true;
      
      index=EMPTY_VALUE;
      price = EMPTY_VALUE;
   }
   void set(int _direct, int _index, datetime _time, double _price, double _high = 0, double _low = 0)
   {
      isEmpty=false;
      direct=_direct;
      index=_index;
      time=_time;
      price=_price;
      high=_high;
      low=_low;
   }
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class FractalsCollection
{
private:
   int size;
public:
   Fractal fractals[];

   Fractal getFractal(ENUM_FRACTAL_TYPES fractalType, int i, const datetime &time[], const double &high[], const double &low[]);

   void FractalsCollection()
   {
      size = 10;
   }

   void Add(ENUM_FRACTAL_TYPES type, int bar, int direct, datetime time, double value, bool asFirst = false)
   {
      Fractal fractal;
      fractal.type = type;
      fractal.direct = direct;
      fractal.index = bar;
      fractal.price = value;
      fractal.time = time;

      Add(fractal, asFirst);
   }

   void Add(Fractal &fractal, bool asFirst = false)
   {
      if (asFirst)
         ArrayReverse(fractals);
      int sizeCurrent = ArraySize(fractals);
      ArrayResize(fractals, sizeCurrent+1);
      fractals[sizeCurrent] = fractal;
      if (asFirst)
         ArrayReverse(fractals);

      if (size < sizeCurrent) {
         ArrayRemove(fractals, size, 1);
      }
   }
};


//////////////////////////////////////



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Fractal FractalsCollection::getFractal(ENUM_FRACTAL_TYPES fractalType, int i, const datetime &time[], const double &high[], const double &low[])
{
   Fractal fractal;
   fractal.type = fractalType;
   // Up fractal
   double up_0 = high[i];
   double up_1 = high[i-1];
   double up_2 = high[i-2];
   double up_3 = high[i-3];
   double up_4 = high[i-4];


   if (fractal.type == FRACTAL_TYPE_THREE) {
      fractal.type = FRACTAL_TYPE_THREE;
      if (up_1 > up_0 && up_1 > up_2)
         fractal.set(1, i-1, time[i-1], high[i-1], high[i-1], low[i-1]);
   }

   if (up_2 > up_1 && up_2 > up_3) {
      if (fractal.type == FRACTAL_TYPE_FIVE_STRICT) {
         if (up_1 > up_0 && up_3 > up_4)
            fractal.set(1, i-2, time[i-2], high[i-2], high[i-2], low[i-2]);
      }
      if (fractal.type == FRACTAL_TYPE_FIVE) {
         if (up_2 > up_0 && up_2 > up_4)
            fractal.set(1, i-2, time[i-2], high[i-2], high[i-2], low[i-2]);
      }
   }

   // DWN fractal
   double dw_0 = low[i];
   double dw_1 = low[i-1];
   double dw_2 = low[i-2];
   double dw_3 = low[i-3];
   double dw_4 = low[i-4];

   int centralNum = fractalType == FRACTAL_TYPE_THREE ? i-1 : i-2;

   if (fractal.type == FRACTAL_TYPE_THREE) {
      if (dw_1 < dw_0 && dw_1 < dw_2)
         fractal.set(-1, i-1, time[i-1], low[i-1], high[i-1], low[i-1]);
   }

   if (dw_2 < dw_1 && dw_2 < dw_3) {
      if (fractal.type == FRACTAL_TYPE_FIVE_STRICT) {
         if (dw_1 < dw_0 && dw_3 < dw_4)
            fractal.set(-1, i-2, time[i-2], low[i-2], high[i-2], low[i-2]);
      }
      if (fractal.type == FRACTAL_TYPE_FIVE) {
         if (dw_2 < dw_0 && dw_2 < dw_4)
            fractal.set(-1, i-2, time[i-2], low[i-2], high[i-2], low[i-2]);
      }
   }

   return fractal;
};

