//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Lines\LinesRobotBase.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ChartPoint : public CObject
{
public:
   int               index;
   bool              isBreakdown;
   int               direct;
   datetime          date;
   double            price;
   int               continuesCount;

   void              ChartPoint(int _index, int _direct, datetime _date, double _price, bool _isBreakdown, int _continuesCount)
   {
      index = _index;
      date = _date;
      price = _price;
      direct = _direct;
      isBreakdown = _isBreakdown;
      continuesCount = _continuesCount;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class LinesPatternBase
{
private:

protected:
   LinesRobotBase    *linesRobotBase;
   CArrayObj         points;

   void              fillPoints(int maxPoints);

public:
   int               patternDirect;
   bool              isPattern;
   
   virtual void      calculatePattern();
   virtual void      drawPattern();

   void              LinesPatternBase::LinesPatternBase(LinesRobotBase *_linesRobotBase);
};
//+------------------------------------------------------------------+
LinesPatternBase::LinesPatternBase(LinesRobotBase *_linesRobotBase)
{
   linesRobotBase = _linesRobotBase;
   isPattern = false;
   patternDirect = 0;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPatternBase::fillPoints(int maxPoints)
{
   points.Clear();

   int countExtremums = ArraySize(linesRobotBase.BufferExtermumsAll);
   for(int i=0; i<countExtremums; i++) {
      bool isBreakdown    = linesRobotBase.BufferLineType[i] == 1;
      int  continuesCount = linesRobotBase.TrandContinuesBuffer[i];

      if(linesRobotBase.BufferExtermumsAll[i] > 0) {
         datetime date = iTime(_Symbol, PERIOD_CURRENT, i);
         ChartPoint *point = new ChartPoint(i, 1, date, linesRobotBase.BufferExtermumsAll[i], isBreakdown, continuesCount);
         points.Add(point);
      }
      if(linesRobotBase.BufferExtermumsAll[i] < 0) {
         datetime date = iTime(_Symbol, PERIOD_CURRENT, i);
         ChartPoint *point = new ChartPoint(i, -1, date, linesRobotBase.BufferExtermumsAll[i] * -1, isBreakdown, continuesCount);
         points.Add(point);
      }

      if (points.Total() >= maxPoints) {
         break;
      }
   }
}
//+------------------------------------------------------------------+
