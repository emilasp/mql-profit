//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\helpers\DrawHelper.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_GET_EXTREMUM_TYPES {
   GET_EXTREMUM_TYPE_RSI = 1, // RSI
   GET_EXTREMUM_TYPE_PIVOT = 2
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class LinesRobotBase : public CObject
{
private:
   int               senssetiveOverLevelpt; // Чувствительность к пробитию уровня
   int               minImpulseLevelDistancePt; // Минимальное количество пунктов между уровнями
   ENUM_GET_EXTREMUM_TYPES type; // Способ определения уровня
   int               indicatorPeriod; // период Индикатора
   int               indicatorLevelTop; // Зона перекупленности Индикатора
   int               indicatorLevelBottom; // Зона перепроданности Индикатора

   int               handleATR;
   int               handleTFcur;
   int               handleTFnxt1;
   int               handleTFnxt2;

   void              fillBuffers();
   string            getPatternText(int patternVal);
protected:
   bool              fillBuffersOnlyOnNewBar;
   bool              drawInfoPanel;

public:
   double            BufferExtermumsPrice[]; // буфер со всеми ценами экстремумов
   double            BufferExtermumsAll[]; // буфер со всеми экстремумами: 0 - нет, val > 0 - есть максимум, val < 0 - есть минимум
   double            BufferLineType[]; // буфер с типами линий: 0 - нет, 1 - пробойный, 2 - внутриканальный
   double            PatternBuffer[]; // текущий паттерн: 0 - флет/треугольник, 1 - бычий импульс, 2 - бычья коррекция, -1 - меджвежий импульс, -2 - медвежья коррекция
   double            SignalBuffer[]; // все сигналы: 0 - нет, 1 - пробой, 2 - отбой
   double            VolatilyBuffer[]; // Разница между линией поддержки и сопротивления
   double            TrandContinuesBuffer[]; // буфер с числом подряд пробоев: -n - пробои медвежьи, 0 - не было пробоев(был флет/треугольник), +n - пробои бычьи
   double            TrandStrengthBuffer[]; // сила тренда - чем меньше средняя коррекция к среднему импульсу, тем больше сила тренда
   double            BufferAtr[];

   double            PatternBufferNxt1[];
   double            TrandContinuesBufferNxt1[];
   double            PatternBufferNxt2[];
   double            TrandContinuesBufferNxt2[];
   // -----------------------------------------------------
   void             ~LinesRobotBase() {};
   void              LinesRobotBase(bool _fillBuffersOnlyOnNewBar = true, bool _drawInfoPanel = true);

   void              onInit();
   void              onTick();
   void              onNewBar();
   void              onDeinit();

   void              setupIndicator(ENUM_GET_EXTREMUM_TYPES _type, int _senssetiveOverLevelpt, int _minImpulseLevelDistancePt, int _indicatorPeriod, int _indicatorLevelTop, int _indicatorLevelBottom);
   void              updateInfoPanel();
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesRobotBase::LinesRobotBase(bool _fillBuffersOnlyOnNewBar = true, bool _drawInfoPanel = true)
{
   fillBuffersOnlyOnNewBar = _fillBuffersOnlyOnNewBar;
   drawInfoPanel = _drawInfoPanel;
}
//+------------------------------------------------------------------+
void LinesRobotBase::onInit()
{
   handleTFcur = iCustom(_Symbol, _Period, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleTFnxt1 = iCustom(_Symbol, PERIOD_M15, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleTFnxt2 = iCustom(_Symbol, PERIOD_H1, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleATR   = iATR(_Symbol, _Period, 13);
   
   ArraySetAsSeries(BufferExtermumsAll, true);

   ArraySetAsSeries(SignalBuffer, true);
   ArraySetAsSeries(BufferLineType, true);
   ArraySetAsSeries(PatternBuffer, true);
   ArraySetAsSeries(TrandContinuesBuffer, true);
   ArraySetAsSeries(VolatilyBuffer, true);
   ArraySetAsSeries(BufferAtr, true);

   ArraySetAsSeries(PatternBufferNxt1, true);
   ArraySetAsSeries(TrandContinuesBufferNxt1, true);
   ArraySetAsSeries(PatternBufferNxt2, true);
   ArraySetAsSeries(TrandContinuesBufferNxt2, true);

   if(drawInfoPanel) {
      DrawHelper::drawRectangleLabel(0, "settings", 0, 0, 0, 300, 100, clrBlack, BORDER_FLAT, CORNER_LEFT_UPPER, 255, STYLE_SOLID, 1, false, false);
   }
}

//+------------------------------------------------------------------+
void LinesRobotBase::onTick()
{
   if(!fillBuffersOnlyOnNewBar) {
      fillBuffers();
      updateInfoPanel();
   }
}
//+------------------------------------------------------------------+
void LinesRobotBase::onNewBar()
{
   if(fillBuffersOnlyOnNewBar) {
      fillBuffers();
      updateInfoPanel();
   }
}
//+------------------------------------------------------------------+
void LinesRobotBase::onDeinit()
{
}

//+------------------------------------------------------------------+
void LinesRobotBase::fillBuffers()
{
   CopyBuffer(handleTFcur, 5, 0, 1000, SignalBuffer);
   CopyBuffer(handleTFcur, 3, 0, 1000, BufferLineType);
   CopyBuffer(handleTFcur, 4, 0, 1000, PatternBuffer);
   CopyBuffer(handleTFcur, 7, 0, 1000, TrandContinuesBuffer);
   CopyBuffer(handleTFcur, 6, 0, 1000, VolatilyBuffer);
   CopyBuffer(handleTFcur, 2, 0, 1000, BufferExtermumsAll);

   CopyBuffer(handleTFnxt1, 4, 0, 1, PatternBufferNxt1);
   CopyBuffer(handleTFnxt1, 7, 0, 1, TrandContinuesBufferNxt1);
   CopyBuffer(handleTFnxt2, 4, 0, 1, PatternBufferNxt2);
   CopyBuffer(handleTFnxt2, 7, 0, 1, TrandContinuesBufferNxt2);

   CopyBuffer(handleATR, 0, 0, 2, BufferAtr);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesRobotBase::setupIndicator(ENUM_GET_EXTREMUM_TYPES _type, int _senssetiveOverLevelpt, int _minImpulseLevelDistancePt, int _indicatorPeriod, int _indicatorLevelTop, int _indicatorLevelBottom)
{
   type = _type;
   senssetiveOverLevelpt = _senssetiveOverLevelpt;
   minImpulseLevelDistancePt = _minImpulseLevelDistancePt;
   indicatorPeriod = _indicatorPeriod;
   indicatorLevelTop = _indicatorLevelTop;
   indicatorLevelBottom = _indicatorLevelBottom;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesRobotBase::updateInfoPanel()
{
   if(drawInfoPanel) {
      DrawHelper::drawLabel("Pattern", "Pattern: " + getPatternText(PatternBuffer[0]) + " : " + getPatternText(PatternBufferNxt1[0]) + " : " + getPatternText(PatternBufferNxt2[0]), 10, 0);
      DrawHelper::drawLabel("Continues", "Continues: " + IntegerToString(TrandContinuesBuffer[0]) + " : " + IntegerToString(TrandContinuesBufferNxt1[0]) + " : " + IntegerToString(TrandContinuesBufferNxt2[0]), 10, 20);
      DrawHelper::drawLabel("Volatily", "Volatily: " + IntegerToString(VolatilyBuffer[0], 1), 10, 40);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string LinesRobotBase::getPatternText(int patternVal)
{
   switch(patternVal) {
   case 1:
      return "↑ imp";
   case 2:
      return "↑ corr";
   case -1:
      return "↓ imp";
   case -2:
      return "↓ corr";
   }
   return "FLET";
}
//+------------------------------------------------------------------+
