//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Lines\LinesPatternBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class LinesPattern123: public LinesPatternBase
{
private:
   int               sensitivePt;
protected:

public:
   void              calculatePattern();
   void              drawPattern();

   void              LinesPattern123(LinesRobotBase *_linesPatternIndicatorData, int _sensitivePt);
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPattern123::LinesPattern123(LinesRobotBase *_linesRobotBase, int _sensitivePt) : LinesPatternBase(_linesRobotBase)
{
   sensitivePt = _sensitivePt;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPattern123::calculatePattern()
{
   isPattern = false;
   fillPoints(100);

   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);

   double sensitive = sensitivePt * _Point;

   if(points.Total() < 3) {
      return;
   }

// For Long
   ChartPoint *point1 = points.At(0);
   ChartPoint *point2 = points.At(1);
   ChartPoint *point3 = points.At(2);
   ChartPoint *point4 = points.At(3);

//Пверяем, что последовательность экстремумов верная
   if (point1.direct != -1 || point2.direct != 1 || point3.direct != -1 || point4.direct != 1) {
      return;
   }

// Проверяем, что цена коснулась уровня 2 максимума
   if (point2.price < lastTick.ask - sensitive || point2.price > lastTick.ask) {
      return;
   }

// Проверяем, что предыдущий минимум меньше или равен последнему
   if (point3.price >= point1.price) {
      return;
   }
// Проверяем, что предыдущий максимум больше или равен последнему
   if (point4.price <= point2.price) {
      return;
   }

   isPattern = true;
   drawPattern();
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPattern123::drawPattern()
{
   ChartPoint *point1 = points.At(0);
   ChartPoint *point2 = points.At(1);
   ChartPoint *point3 = points.At(2);
   ChartPoint *point4 = points.At(3);
   
   DrawHelper::drawArrow("123:point1", point1.date, point1.price + (point1.direct > 0 ? 60 : -20) * _Point, 159, clrGreenYellow);
   DrawHelper::drawArrow("123:point2", point2.date, point2.price + (point2.direct > 0 ? 60 : -20) * _Point, 159, clrRed);
   DrawHelper::drawArrow("123:point3", point3.date, point3.price + (point3.direct > 0 ? 60 : -20) * _Point, 159, clrGreenYellow);
   DrawHelper::drawArrow("123:point4", point4.date, point4.price + (point4.direct > 0 ? 60 : -20) * _Point, 159, clrRed);
}
//+------------------------------------------------------------------+
