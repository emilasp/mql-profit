//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Emilasp\Lines\LinesPatternBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class LinesPatternBreakdown: public LinesPatternBase
{
private:
   int               continuesMinCount;
   double            lastMaximum;
   double            lastMimimum;

protected:

public:
   void              calculatePattern();
   void              drawPattern();

   void              LinesPatternBreakdown(LinesRobotBase *_linesPatternIndicatorData, int _continuesMinCount);
};
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPatternBreakdown::LinesPatternBreakdown(LinesRobotBase *_linesRobotBase, int _continuesMinCount) : LinesPatternBase(_linesRobotBase)
{
   continuesMinCount = _continuesMinCount;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPatternBreakdown::calculatePattern()
{
   isPattern = false;
   patternDirect = 0;
   fillPoints(100);

   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);

   if(points.Total() < 2) {
      return;
   }

   CArrayObj minimums;
   CArrayObj maximums;

   for(int i=0; i<points.Total(); i++) {
      ChartPoint *point = points.At(i);
      if (point != NULL) {
         if (point.direct < 0) {
            minimums.Add(point);
         }
         if (point.direct > 0) {
            maximums.Add(point);
         }
      }
   }


// For Long


//1. min_1 > min_2 && min_2 < min_n, [n - задается в параметрах]
//2. price > max_1
// Смена тенденции:
// Long:
// - цена опустилась ниже последнего минимума
// - послдений максимум ниже предыдущего





// !!!!!! ИДЕЯ!
// Добавляем индикатору чувствительности - теперь определяем экстремум по 4 > 3, 3 > 2, 2 < 1, 1 < 0 - 2 - точка экстремума! 






   if (minimums.Total() >= 4) {
      ChartPoint *pointMin1 = minimums.At(0);
      ChartPoint *pointMin2 = minimums.At(1);
      ChartPoint *pointMin3 = minimums.At(2);
      ChartPoint *pointMin4 = minimums.At(3);


      if (pointMin1.price > pointMin2.price && pointMin2.price < pointMin3.price && pointMin3.price < pointMin4.price) {
         if (pointMin1.price != lastMimimum) {
            lastMimimum = pointMin1.price;
            patternDirect = 1;
            isPattern = true;
            drawPattern();
            int tt = 10;
            
            //DrawHelper::drawLabel("tmp", "Last minimum: " + DoubleToString,int xdistance=10,int ydistance=20,int corner=0,int windowId=0)
         }
      }

   }
//if (!point1.isBreakdown) {
//   return;
//}
//if(linesRobotBase.TrandContinuesBuffer[1] >= continuesMinCount) {

//}
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LinesPatternBreakdown::drawPattern()
{
   ChartPoint *point1 = points.At(0);

   DrawHelper::drawArrow("123:point1", point1.date, point1.price + (point1.direct > 0 ? 60 : -20) * _Point, 159, clrGreenYellow);
}
//+------------------------------------------------------------------+
