// OrdersManager

#include <Emilasp\BwSystem\AnalizeManager.mqh>
#include <Emilasp\BwSystem\Common\StopLossManager.mqh>
#include <Emilasp\BwSystem\Common\PyramidingManager.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>
#include <Emilasp\libraries\Telegram\TelegramChat.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>

#include <Emilasp\TradeCore\TradeCorePositions.mqh>
#include <Emilasp\TradeCore\TradeCoreOrders.mqh>
#include <Emilasp\TradeCore\TcOrder.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrdersManager
{
private:
   int magic;
   TelegramChat    *chat;
   TradeCorePositions *positions;
   TradeCoreOrders *orders;

   double volumeBase;

   bool isModeDebug;
   bool isModeNotification;
   bool isModeLearning;
   bool clearOrdersIfCreateRealOrder;
   bool clearOrdersIfNoCreateRealOnNextBar;

   datetime lastCreateTimeOrder;
   datetime lastCreateTimePosition;

   void trailingStop();
   void pyramiding();
   double getVolume(TcOrder &order);
   void signals();
   void drawPanel();

public:
   AnalizeManager *analizeManager;
   StopLossManager *stopLossManager;
   PyramidingManager *pyramidingManager;

   void OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);

   void OrdersManager(int _magic, double _volumeBase, bool _clearOrdersIfCreateRealOrder, bool _clearOrdersIfNoCreateRealOnNextBar, bool _isModeNotification, bool _isModeLearning, bool _isModeDebug)
   {
      magic=_magic;
      volumeBase=_volumeBase;

      isModeNotification=_isModeNotification;
      isModeLearning=_isModeLearning;
      isModeDebug=_isModeDebug;
      clearOrdersIfCreateRealOrder=_clearOrdersIfCreateRealOrder;
      clearOrdersIfNoCreateRealOnNextBar=_clearOrdersIfNoCreateRealOnNextBar;

      chat = new TelegramChat("2012454312:AAE7rY93NOjYiKI-_LzWTVe1c51FQUilyWE", "-518922786");

      int deviation = 10;
      positions = new TradeCorePositions(_Symbol, magic, deviation);
      orders = new TradeCoreOrders(_Symbol, magic, deviation);
   }

   void OnTick()
   {
      ordersRefresh();

      trailingStop(); // +
      pyramiding();// +
      signals(); // + Создаем отлоденные ордера по сигналам
      ordersCancel(); // закрываем ордера по разным условиям
      positionsClose(); // закрываем позиции по разным условиям

      drawPanel();

      //redrawOrders(orders.orders); // Возможно сдедаем автоотрисовку по OnTick()
   }

   void ordersRefresh()      
   {
      positions.refresh();
      orders.refresh();
   }

   void ordersCancel()
   {
      TcOrder *order = orders.getLastOrder();
      if (!order.isEmpty) {
         if (order.positionType == POSITION_TYPE_BUY && order.sl > analizeManager.lastTick.ask) {
            orders.orderClose(order);
         }
         if (order.positionType == POSITION_TYPE_SELL && order.sl < analizeManager.lastTick.bid) {
            orders.orderClose(order);
         }
         
         if (clearOrdersIfNoCreateRealOnNextBar) {
            int barIndexOrder = iBarShift(symbol,_Period,order.timeOpen,false);
            int barIndexCurrent = analizeManager.barsCount;//iBarShift(symbol,_Period,TimeCurrent(),false);
            
            if (clearOrdersIfNoCreateRealOnNextBar && barIndexOrder < barIndexCurrent) {
               orders.orderClose(order);
            }
         }
      }
   }

   void positionsClose()
   {
      TcOrder *order = positions.getLastOrder();
      if (!order.isEmpty) {
         //positions.closeAll(order.positionType);
      }
   }
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrdersManager::OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   positions.refresh();
   orders.refresh();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrdersManager::trailingStop()
{
   if (positions.Total() > 0)   {
      TcOrder *order = positions.getLastOrder();

      if (!order.isEmpty) {
         double newStopLoss = stopLossManager.getTralStopLoss(order);

         if (newStopLoss != EMPTY_VALUE && order.sl != newStopLoss) {
            positions.orderModify(newStopLoss, 0);
            
            TesterHelper::pause();
            int tt = 10;
         }
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrdersManager::pyramiding()
{
   if (pyramidingManager.enabled && positions.Total() > 0) {
      sSignal signal = analizeManager.getSignalForUpPosition();

      if (signal.Buy || signal.Sell) {
         positions.refresh();
         TcOrder *order = positions.getLastOrder();

         if (signal.Buy && order.positionType == POSITION_TYPE_BUY) {
            positions.Buy(getVolume(order), order.sl, 0, SLTP_PRICE);
            order.step++;
         }
         if (signal.Sell && order.positionType == POSITION_TYPE_SELL) {
            positions.Sell(getVolume(order), order.sl, 0, SLTP_PRICE);
            order.step++;
         }
      }
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double OrdersManager::getVolume(TcOrder &order)
{
   double volume = order.volume;
   if (pyramidingManager.enabled) {
      int size = ArraySize(pyramidingManager.pyramidingVolumeSteps);
      int step = order.step < size ? order.step : size-1;
      volume = order.volume * pyramidingManager.pyramidingVolumeSteps[step];
   }
   return volume;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrdersManager::signals()
{
   sSignal signal = analizeManager.getSignalForTrade();

   int positionsCount = positions.Total();
   int ordersCount = orders.Total();

   if ((signal.Buy || signal.Sell) && positionsCount == 0  && ordersCount == 0) {
      ENUM_POSITION_TYPE positionType = signal.Buy ? POSITION_TYPE_BUY : POSITION_TYPE_SELL;

      if (positionType == POSITION_TYPE_BUY) {
         double price = analizeManager.lastBars[0].high + 2 * _Point;
         double sl = stopLossManager.getStopLossBase(POSITION_TYPE_BUY, price);
         double tp = price + (price - sl) * 3;

         orders.BuyStop(volumeBase, price, sl, tp, SLTP_PRICE, 0, NULL);
      }
      if (positionType == POSITION_TYPE_SELL) {
         double price = analizeManager.lastBars[0].low - 2 * _Point;
         double sl = stopLossManager.getStopLossBase(POSITION_TYPE_SELL, price);
         double tp = price + (price - sl) * 3;

         orders.SellStop(volumeBase, price, sl, tp, SLTP_PRICE, 0, NULL);
      }
      if (isModeLearning) {
         TesterHelper::pause();
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrdersManager::drawPanel()
{
   DrawHelper::drawLabel("v-orders-count", "count: " + IntegerToString(orders.Total()) + "(P:" + positions.Total() + ")", 100, 20, 3);
   if (analizeManager.isNewBar) {
      TcOrder *order = orders.getLastOrder();
      if (order.isEmpty) {
         TcOrder *order = positions.getLastOrder();
      }
      if (!order.isEmpty) {
         DrawHelper::drawLabel("v-order-price", "price: " + DoubleToString(order.priceOpen), 100, 40, 3);
         DrawHelper::drawLabel("v-order-sl", "sl: " + DoubleToString(order.sl), 100, 60, 3);
         DrawHelper::drawLabel("v-order-tp", "tp: " + DoubleToString(order.tp), 100, 80, 3);
      }
   }
}




///






//+------------------------------------------------------------------+
