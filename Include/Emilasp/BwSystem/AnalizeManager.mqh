//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>
#include <Emilasp\BwSystem\Common\SignalInfo.mqh>

enum ENUM_CLOSE_TYPES {
   ALL=1,
   ONLY_ONE_SECTOR=2,
   ONLY_DIFFERENT_SECTOR=3,
};


struct sSignal {
   bool              Buy;
   bool              Sell;
   void              sSignal()
   {
      Buy=false;
      Sell=false;
   }

   bool hasSignal()
   {
      return Buy || Sell;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AnalizeManager
{
private:
   int handleFTrand, handleDevBar, handleAlligator, handleFractals, handleAO, handleAtr;
   CArrayObj signalsInfo;

public:
   CoreTradeHadge *trade;

   bool trandFractalFilterEnabled;
   int trandFractalType;
   int trandFractalTf1;
   int trandFractalTf2;
   int trandFractalTf3;

   ENUM_CLOSE_TYPES prevBarType;
   double minCloseFilterPercent;

   int alligatoHistoryBarsAnalizeCount;
   double alligatoFlatPercentByMin;
   int alligatorTrandByFlatDistance;

   int fractalTypeForInd;

   MqlRates lastBars[];
   MqlTick lastTick;
   int barsCount;
   double lastArtValue;
    
   bool isNewBar;

   string lastSignalInfo;

   sSignal getSignalAO(int index);
   sSignal getSignalDeviationBar(int index);
   sSignal getSignalFractal(int index);
   sSignal getSignalAlligator(int index);
   sSignal getSignalFractalTrand(int index);

   sSignal getSignalForTrade();
   sSignal getSignalForUpPosition();
   
   void setAtrLastValue();
   void drawSignalsForTrade();

   void addSignalInfo(string nameIndicator, string nameBuffer, double value, string comment = "")
   {
      SignalInfo *info = new SignalInfo(nameIndicator, nameBuffer, value, comment);
      signalsInfo.Add(info);
   }

   void OnInit()
   {
      ArraySetAsSeries(lastBars, true);

      isNewBar=true;

      handleFTrand = iCustom(_Symbol, _Period, "Emilasp\\BW\\bw_fractals_trands", trandFractalType, PERIOD_CURRENT, trandFractalTf1, trandFractalTf2, trandFractalTf3);
      handleDevBar = iCustom(_Symbol, _Period, "Emilasp\\BW\\bw_deviation_bar", prevBarType, minCloseFilterPercent);
      handleFractals = iCustom(_Symbol, _Period, "Emilasp\\BW\\bw_fractals", 5);
      handleAlligator = iCustom(_Symbol, _Period, "Emilasp\\BW\\bw_alligator_test_99", alligatoHistoryBarsAnalizeCount, alligatoFlatPercentByMin, alligatorTrandByFlatDistance);
      handleAO = iAO(_Symbol, _Period);
      handleAtr=iATR(Symbol(),PERIOD_CURRENT,14);
   }

   void OnTick()
   {
      if(!SymbolInfoTick(Symbol(), lastTick)) {
         Print("SymbolInfoTick() failed, error = ", GetLastError());
      }
      barsCount = Bars(_Symbol, _Period);

      isNewBar = trade.isNewBar(5);

      SymbolInfoTick(_Symbol, lastTick);

      if (isNewBar) {
         TsHelper::fillRates(10, lastBars);
      }
      
      setAtrLastValue(); 
   }
};
//+------------------------------------------------------------------+

void AnalizeManager::setAtrLastValue() {
   double Atr[];
   ArraySetAsSeries(Atr, true);
   CopyBuffer(handleAtr, 0, 0, 1, Atr);
   addSignalInfo("Atr", "0", Atr[0]);
   lastArtValue = Atr[0];
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalAO(int index)
{
   sSignal res;
   double AO[];
   ArraySetAsSeries(AO, true);
   CopyBuffer(handleAO, 1, 0, 4, AO);
   addSignalInfo("AO", "1", AO[0]);
   res.Buy = AO[0] > 0 && AO[1] > 0 && AO[2] > 0 && AO[3] < 0;
   res.Sell = AO[0] < 0 && AO[1] < 0 && AO[2] < 0 && AO[3] > 0;
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalDeviationBar(int index)
{
   sSignal res;
   double DeviationBar[];
   ArraySetAsSeries(DeviationBar, true);
   CopyBuffer(handleDevBar, 0, 0, 2, DeviationBar);
   addSignalInfo("DB(1)", "0", DeviationBar[1]);
   res.Buy = DeviationBar[1] < 0;
   res.Sell = DeviationBar[1] > 0;
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalFractal(int index)
{
   sSignal res;
   double Fractals[];
   ArraySetAsSeries(Fractals, true);
   CopyBuffer(handleFractals, 2, 0, 5, Fractals);
   addSignalInfo("Fr(1)", "0", Fractals[1]);
   res.Buy = Fractals[1] > 0;
   res.Sell = Fractals[1] < 0;
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalAlligator(int index)
{
   sSignal res;
   double AlligatorLips[];
   double AlligatorStrength[];
   double AlligatorAngle[];
   double AlligatorLength[];
   double AlligatorP2Lips[];
   ArraySetAsSeries(AlligatorLips, true);
   ArraySetAsSeries(AlligatorStrength, true);
   ArraySetAsSeries(AlligatorAngle, true);
   ArraySetAsSeries(AlligatorLength, true);
   ArraySetAsSeries(AlligatorP2Lips, true);
   CopyBuffer(handleAlligator, 2, 0, 10, AlligatorLips);
   CopyBuffer(handleAlligator, 3, 0, 10, AlligatorStrength);
   CopyBuffer(handleAlligator, 4, 0, 10, AlligatorAngle);
   CopyBuffer(handleAlligator, 6, 0, 10, AlligatorLength);
   CopyBuffer(handleAlligator, 7, 0, 10, AlligatorP2Lips);

   addSignalInfo("Al(1)", "angle", AlligatorAngle[0]);
   addSignalInfo("Al(1)", "str", AlligatorStrength[0]);
   addSignalInfo("Al(1)", "p2l", AlligatorP2Lips[0]);
   addSignalInfo("Al(1)", "length", AlligatorLength[0]);
   
   res.Buy = AlligatorAngle[1] > 20 && lastTick.ask < AlligatorLips[1];
   res.Sell = AlligatorAngle[1] > 20 && lastTick.bid > AlligatorLips[1];
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalFractalTrand(int index)
{
   sSignal res;
   double TrandBuffer[], TrandBuffer0[], TrandBuffer1[], TrandBuffer2[], TrandBuffer3[];
   ArraySetAsSeries(TrandBuffer, true);
   ArraySetAsSeries(TrandBuffer0, true);
   ArraySetAsSeries(TrandBuffer1, true);
   ArraySetAsSeries(TrandBuffer2, true);
   ArraySetAsSeries(TrandBuffer3, true);
   CopyBuffer(handleFTrand, 0, 0, 2, TrandBuffer);
   CopyBuffer(handleFTrand, 1, 0, 2, TrandBuffer0);
   CopyBuffer(handleFTrand, 2, 0, 2, TrandBuffer1);
   CopyBuffer(handleFTrand, 3, 0, 2, TrandBuffer2);
   CopyBuffer(handleFTrand, 4, 0, 2, TrandBuffer3);

   addSignalInfo("FrTr", "trends0", TrandBuffer[1]);
   addSignalInfo("FrTr", "trends1", TrandBuffer1[1]);
   addSignalInfo("FrTr", "trends2", TrandBuffer2[1]);
   addSignalInfo("FrTr", "trends3", TrandBuffer3[1]);

   res.Buy = TrandBuffer0[1] > 0;
   res.Sell = TrandBuffer0[1] < 0;
   return res;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalForUpPosition()
{
   sSignal result = getSignalAO(0);

   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal AnalizeManager::getSignalForTrade()
{
   signalsInfo.Clear();

   sSignal result;

////// Deviation bar ////////
   sSignal resDevBar = getSignalDeviationBar(0);
////// Alligator ////////
   sSignal resAlligator = getSignalAlligator(0);
   if (resDevBar.hasSignal()) {
      result.Buy = resDevBar.Buy && resAlligator.Buy;
      result.Sell = resDevBar.Sell && resAlligator.Sell;
   }
////// Trand filter ////////
   if (trandFractalFilterEnabled) {
      //sSignal resFractaTrand = getSignalFractalTrand(0);
      sSignal resFractaTrand = getSignalFractal(0);
      
      result.Buy = result.Buy && resFractaTrand.Buy;
      result.Sell = result.Sell && resFractaTrand.Sell;
   }

   drawSignalsForTrade();

   return result;
}
//+------------------------------------------------------------------+
void AnalizeManager::drawSignalsForTrade()
{
   for(int i=0; i<signalsInfo.Total(); i++) {
      SignalInfo *info = signalsInfo.At(i);
      DrawHelper::drawLabel("AnalizePanel" + IntegerToString(i),info.nameIndicator + ":" + info.nameBuffer + ": " + DoubleToString(info.value), 400, 20 * i + 20, 3);
   }
}
//+------------------------------------------------------------------+
