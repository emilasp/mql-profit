//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderReal : public CObject
{
public:
   long ticket;
   int step;
   ENUM_POSITION_TYPE positionType;  // Tип позиции
   
   MqlTick tick;
   double   volume;          // Объём позиции
   double   price;           // Текущая цена позиции
   double   sl;              // Stop Loss позиции
   double   tp;              // Take Profit позиции

   long     magic;             // Магический номер
   string   symbol;           // Символ
   string   comment;          // Комментарий
   double   swap;            // Своп
   double   commission;      // Комиссия
   double   cprice;          // Текущая цена позиции
   double   profit;          // Прибыль/убыток позиции


   datetime time;           // Время открытия позиции
   long     id;                // Идентификатор позиции

   void OrderReal(long _ticket)
   {
      ticket=_ticket;
      step=0;
      
      fillOrderByTicket();
   }
   void nextStep() {
      step++;
   }
   
   void fillOrderByTicket()
   {
      PositionSelectByTicket(ticket);

      symbol=PositionGetString(POSITION_SYMBOL);
      comment=PositionGetString(POSITION_COMMENT);
      magic=PositionGetInteger(POSITION_MAGIC);
      price=PositionGetDouble(POSITION_PRICE_OPEN);
      cprice=PositionGetDouble(POSITION_PRICE_CURRENT);
      sl=PositionGetDouble(POSITION_SL);
      tp=PositionGetDouble(POSITION_TP);
      positionType=(ENUM_POSITION_TYPE)PositionGetInteger(POSITION_TYPE);
      volume=PositionGetDouble(POSITION_VOLUME);
      commission=PositionGetDouble(POSITION_COMMISSION);
      swap=PositionGetDouble(POSITION_SWAP);
      profit=PositionGetDouble(POSITION_PROFIT);
      time=(datetime)PositionGetInteger(POSITION_TIME);
      id=PositionGetInteger(POSITION_IDENTIFIER);
   }
};
//+------------------------------------------------------------------+
