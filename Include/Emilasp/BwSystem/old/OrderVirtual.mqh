//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>

enum ENUM_ORDER_VIRTUAL_STAGE {
   ORDER_VIRTUAL_STAGE_CANCEL = 0,
   ORDER_VIRTUAL_STAGE_NEW = 1,
   ORDER_VIRTUAL_STAGE_PROCESS = 2,
   ORDER_VIRTUAL_STAGE_CLOSE = 3,
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderVirtual : public CObject
{
public:
   ENUM_ORDER_VIRTUAL_STAGE stage;
   ENUM_POSITION_TYPE positionType;  // Tип позиции
   MqlTick tickStart;
   MqlTick tickClose;
   int bar;
   double volume;
   double price;
   double sl;
   double tp;
   double profit;

   void OrderVirtual(MqlTick &_tickStart, int _bar, ENUM_POSITION_TYPE _positionType, double _volume, double _price, double _sl, double _tp)
   {
      stage = ORDER_VIRTUAL_STAGE_NEW;
      tickStart=_tickStart;
      bar=_bar;
      positionType=_positionType;
      volume=_volume;
      price=_price;
      sl=_sl;
      tp=_tp;
   }
   
   void changeStage(MqlTick &_tickClose, ENUM_ORDER_VIRTUAL_STAGE _stage)
   {
     stage = _stage;
     tickClose=_tickClose;
     profit = positionType==POSITION_TYPE_BUY ? tickClose.ask - tickStart.ask : tickStart.ask - tickClose.ask;
   }
};

//+------------------------------------------------------------------+
