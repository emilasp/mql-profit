//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\BwSystem\AnalizeManager.mqh>
#include <Emilasp\BwSystem\Common\StopLossManager.mqh>
#include <Emilasp\BwSystem\Common\OrderReal.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>



enum ENUM_PYRAMIDING_TYPE {
   PYRAMIDING_DEV_VAR = 1,
   PYRAMIDING_FRACTAL = 2,
   PYRAMIDING_DEV_BAR_AND_FRACTAL = 3,
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderRealManager
{
private:
   int magic;
   CArrayObj      orders;

   bool pyramidingEnabled;
   ENUM_PYRAMIDING_TYPE pyramidingType;
   double pyramidingVolumeSteps[];

   bool isModeLearning;

public:
   CoreTradeHadge *trade;
   AnalizeManager *analizeManager;
   StopLossManager * stopLossManager;

   void OrderRealManager(int _magic, bool _pyramidingEnabled, ENUM_PYRAMIDING_TYPE _pyramidingType, bool _isModeLearning)
   {
      magic=_magic;

      isModeLearning=_isModeLearning;

      pyramidingEnabled=_pyramidingEnabled;
      pyramidingType=_pyramidingType;

      ArrayResize(pyramidingVolumeSteps, 5);
      pyramidingVolumeSteps[0] = 1;
      pyramidingVolumeSteps[1] = 4;
      pyramidingVolumeSteps[2] = 3;
      pyramidingVolumeSteps[3] = 2;
      pyramidingVolumeSteps[4] = 1;

   }

   int getCountOrders()
   {
      return orders.Total();
   }

   CArrayObj *getOrders(ENUM_POSITION_TYPE type = NULL)
   {
      CArrayObj *ordersFinded = new CArrayObj();
      for(int i=0; i < orders.Total(); i++) {
         OrderReal *order = orders.At(i);
         if (order.positionType == type || type == NULL) {
            ordersFinded.Add(order);
         }
      }
      return ordersFinded;
   }

   void fillOrders()
   {
      orders.Clear();
      if(PositionSelect(_Symbol)) {
         double ticket = PositionGetInteger(POSITION_TICKET);
         OrderReal *order = new OrderReal(ticket);
         orders.Add(order);
      }
   }

   void OnTick();
   void trailingStop();
   void pyramiding();
   double getVolume(OrderReal &order);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrderRealManager::OnTick()
{
   if (PositionsTotal() != orders.Total()) {
      fillOrders();
   }

   trailingStop();
   pyramiding();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OrderRealManager::pyramiding()
{
   if (pyramidingEnabled) {
      sSignal signal = analizeManager.getSignalForUpPosition();

      if (signal.Buy || signal.Sell) {
         OrderReal *order = orders.At(0);
         order.fillOrderByTicket();

         if (signal.Buy && order.positionType == POSITION_TYPE_BUY) {
            trade.Buy(getVolume(order), _Symbol, 0, order.sl, 0);
         }
         if (signal.Sell && order.positionType == POSITION_TYPE_SELL) {
            trade.Sell(getVolume(order), _Symbol, 0, order.sl, 0);
         }
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double OrderRealManager::getVolume(OrderReal &order)
{
   double volume = order.volume;
   if (pyramidingEnabled) {
      int size = ArraySize(pyramidingVolumeSteps);
      int step = order.step < size ? order.step : size-1;
      volume = order.volume * pyramidingVolumeSteps[step];
   }
   return volume;
}


//+------------------------------------------------------------------+
void OrderRealManager::trailingStop()
{
   if (orders.Total() > 0 && PositionSelect(_Symbol)) {
      if(!isModeLearning) {
         double newStopLoss = stopLossManager.getTralStopLoss(orders);

         if (newStopLoss != EMPTY_VALUE) {
            trade.PositionModify(_Symbol, newStopLoss, 0);
            fillOrders();
         }
      }
   }
}

//+------------------------------------------------------------------+
