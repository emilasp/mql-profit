//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\BwSystem\AnalizeManager.mqh>
#include <Emilasp\BwSystem\Common\OrderVirtual.mqh>
#include <Emilasp\BwSystem\Common\StopLossManager.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>
#include <Emilasp\libraries\Telegram\TelegramChat.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>

//http://tol64.blogspot.com/2012/11/kak-poluchit-svoystva-poziciy.html


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class OrderVirtualManager
{
private:
   int magic;
   TelegramChat    *chat;

   CArrayObj orders;
   CArrayObj ordersLearning;
   CArrayObj ordersFinished;

   double volumeBase;

   bool isModeDebug;
   bool isModeNotification;
   bool isModeLearning;
   bool clearOrdersIfCreateRealOrder;
   bool clearOrdersIfNoCreateRealOnNextBar;

   int lastOrderRealCreateBar;

public:
   CoreTradeHadge *trade;
   AnalizeManager *analizeManager;
   StopLossManager * stopLossManager;

   void OrderVirtualManager(int _magic, double _volumeBase, bool _clearOrdersIfCreateRealOrder, bool _clearOrdersIfNoCreateRealOnNextBar, bool _isModeNotification, bool _isModeLearning, bool _isModeDebug)
   {
      magic=_magic;
      volumeBase=_volumeBase;

      isModeNotification=_isModeNotification;
      isModeLearning=_isModeLearning;
      isModeDebug=_isModeDebug;
      clearOrdersIfCreateRealOrder=_clearOrdersIfCreateRealOrder;
      clearOrdersIfNoCreateRealOnNextBar=_clearOrdersIfNoCreateRealOnNextBar;

      chat = new TelegramChat("2012454312:AAE7rY93NOjYiKI-_LzWTVe1c51FQUilyWE", "-518922786");
      //---
      /////////////////////////////////////////////
   }

   int getCountOrders()
   {
      return orders.Total();
   }

   void createOrderVirtual(ENUM_POSITION_TYPE type, double volume, double price)
   {
      OrderVirtual *order = new OrderVirtual(analizeManager.lastTick, analizeManager.barsCount, type, volume, price, 0, 0);

      orders.Add(order);
      
      order.sl = stopLossManager.getStopLossBase(orders);
      order.tp = order.price + (order.price - order.sl) * 3;
      
      redrawVirtualOrders(orders);
   };

   void deleteOrderVirtual(int i, ENUM_ORDER_VIRTUAL_STAGE stageOrder)
   {
      OrderVirtual *order = orders.At(i);

      order.changeStage(analizeManager.lastTick, stageOrder);

      if (isModeLearning) {
         ordersLearning.Add(order);
         order.changeStage(analizeManager.lastTick, ORDER_VIRTUAL_STAGE_PROCESS);
      } else {
         ordersFinished.Add(order);
      }

      orders.Detach(i);

      redrawVirtualOrders(orders);
      redrawVirtualOrders(ordersLearning);
   };

   int countVirtualOrders()
   {
      return orders.Total();
   };

   // Создаем ордера
   void OnTick()
   {
      // create and cancel
      for(int i=0; i < orders.Total(); i++) {
         OrderVirtual *order = orders.At(i);

         bool isOrderRealCreate = false;
         // Create real order
         if (order.stage == ORDER_VIRTUAL_STAGE_NEW) {
            if (lastOrderRealCreateBar != analizeManager.barsCount && ordersLearning.Total() == 0) {
               lastOrderRealCreateBar = analizeManager.barsCount;
               if (order.positionType == POSITION_TYPE_BUY && order.price < analizeManager.lastTick.ask) {
                  isOrderRealCreate = createRealOrder(i);
               }
               if (order.positionType == POSITION_TYPE_SELL && order.price > analizeManager.lastTick.bid) {
                  isOrderRealCreate = createRealOrder(i);
               }
            }

            if (isOrderRealCreate) {
               deleteOrderVirtual(i, isModeLearning ? ORDER_VIRTUAL_STAGE_PROCESS : ORDER_VIRTUAL_STAGE_CLOSE);
            }
            // Cancel virtual order
            if (order.positionType == POSITION_TYPE_BUY && order.sl > analizeManager.lastTick.ask) {
               deleteOrderVirtual(i, ORDER_VIRTUAL_STAGE_CANCEL);
            }
            if (order.positionType == POSITION_TYPE_SELL && order.sl < analizeManager.lastTick.bid) {
               deleteOrderVirtual(i, ORDER_VIRTUAL_STAGE_CANCEL);
            }

            if (clearOrdersIfNoCreateRealOnNextBar && order.bar+1 < analizeManager.barsCount) {
               deleteOrderVirtual(i, ORDER_VIRTUAL_STAGE_CANCEL);
            }
         }
      }


      // Learning
      if (isModeLearning) {
         for(int i=0; i < ordersLearning.Total(); i++) {
            OrderVirtual *order = ordersLearning.At(i);

            if (order.stage == ORDER_VIRTUAL_STAGE_PROCESS) {
               if (order.bar != analizeManager.barsCount) {
                  bool isOrderClose = false;
                  // Stop loss
                  if (order.positionType == POSITION_TYPE_BUY && analizeManager.lastTick.ask < order.sl) {
                     isOrderClose = true;
                  }
                  if (order.positionType == POSITION_TYPE_SELL && analizeManager.lastTick.ask > order.sl) {
                     isOrderClose = true;
                  }

                  if (isOrderClose) {
                     order.changeStage(analizeManager.lastTick, ORDER_VIRTUAL_STAGE_CLOSE);

                     ordersFinished.Add(order);
                     ordersLearning.Detach(i);

                     drawLearningOrderFinished(order);
                     TesterHelper::pause();
                  }
               }
            }
         }
      }


      drawPanel();
      trailingStop();

      // Signal to trade
      sSignal signal = analizeManager.getSignalForTrade();

      if ((signal.Buy || signal.Sell) && getCountOrders() == 0  && ordersLearning.Total() == 0 && !PositionSelect(_Symbol)) {
         ENUM_POSITION_TYPE positionType = POSITION_TYPE_BUY;
         if (signal.Sell) {
            positionType = POSITION_TYPE_SELL;
         }
         onSignal(positionType);
      }
   }

   void onSignal(ENUM_POSITION_TYPE positionType)
   {
      if (getCountOrders() == 0) {
         if (positionType == POSITION_TYPE_BUY) {
            double price = analizeManager.lastBars[0].high + 2 * _Point;
            createOrderVirtual(POSITION_TYPE_BUY, volumeBase, price);
         }
         if (positionType == POSITION_TYPE_SELL) {
            double price = analizeManager.lastBars[0].low - 2 * _Point;
            createOrderVirtual(POSITION_TYPE_SELL, volumeBase, price);
         }
      }
   }

   bool createRealOrder(int i)
   {
      OrderVirtual *order = orders.At(i);

      bool success = false;

      if (isModeNotification) {
         string text = "Signal " + _Symbol + ":" + _Period + "; " + (order.positionType == POSITION_TYPE_BUY ? "buy" : "sell");
         string result = chat.sendMessage(text);
         success = true;
      } else {
         if (isModeLearning) {
            TesterHelper::pause();
            success = true;
         } else {
            if (order.positionType == POSITION_TYPE_BUY) {
               success = trade.Buy(order.volume, _Symbol, 0, order.sl, order.tp);
            } else  {
               success = trade.Sell(order.volume, _Symbol, 0, order.sl, order.tp);
            }
         }
      }

      if(isModeDebug && !isModeLearning) {
         TesterHelper::pause();
      }

      return success;
   }

   void trailingStop()
   {
      if (isModeLearning && ordersLearning.Total() > 0) {
         double newStopLoss = stopLossManager.getTralStopLoss(ordersLearning);

         if (newStopLoss != EMPTY_VALUE) {
            OrderVirtual *order = ordersLearning.At(0);
            order.sl = newStopLoss;
            //drawLearningOrder(order);
            redrawVirtualOrders(ordersLearning);
         }
      }
   }


   void drawLearningOrderFinished(OrderVirtual *order)
   {
      string prefixDrawObjects = "learningOrder";
      int symbolStart = order.positionType == POSITION_TYPE_BUY ? 233 : 234;
      int symbolEnd = order.positionType == POSITION_TYPE_SELL ? 233 : 234;
      int colorStart = order.positionType == POSITION_TYPE_BUY ? clrGreen : clrRed;
      int colorFinish = order.positionType == POSITION_TYPE_SELL ? clrGreen : clrRed;
      DrawHelper::drawArrow(prefixDrawObjects + order.tickStart.time, order.tickStart.time, order.tickStart.ask, symbolStart, colorStart, 0, 0, 28);
      DrawHelper::drawArrow(prefixDrawObjects + order.tickClose.time, order.tickClose.time, order.tickClose.ask, symbolEnd, colorFinish, 0, 0, 28);

      DrawHelper::drawTrendLine(0, 0, prefixDrawObjects + "line" + order.tickStart.time, order.tickStart.time, order.tickStart.ask, order.tickClose.time, order.tickClose.ask, clrAqua, STYLE_DOT, 1);
   }

   void redrawVirtualOrders(CArrayObj &ordersToRedraw)
   {
      string orderInfo = "order[" + ordersFinished.Total() + "] - "; 
      // clear draw objects
      int TotalObject = ObjectsTotal(0);

      for(int i=0; i<TotalObject; i++) {
         string name = ObjectName(0, i, 0);
         string nameToDelete = getDrawPrefixName();

         int find = StringFind(name, nameToDelete, 0);
         if (StringFind(name, nameToDelete, 0) >= 0)
            ObjectDelete(0, name);
      }

      // draw orders
      for(int i=0; i < ordersToRedraw.Total(); i++) {
         OrderVirtual *order = ordersToRedraw.At(i);

         if (order != NULL) {
            order.changeStage(analizeManager.lastTick, order.stage);
            
            string name = getDrawPrefixName() + IntegerToString(i);

            if(order.positionType == POSITION_TYPE_BUY) {
               DrawHelper::drawArrow(getDrawPrefixName() + order.tickStart.time, order.tickStart.time, order.price, 159, clrGold, 0, 0, 28);
            } else {
               DrawHelper::drawArrow(getDrawPrefixName() + order.tickStart.time, order.tickStart.time, order.price, 159, clrGold, 0, 0, 28);
            }
            DrawHelper::moveHorizontalLine(name, order.price, clrRosyBrown, STYLE_DASH);
            DrawHelper::moveHorizontalLine(name + "sl", order.sl, clrRed, STYLE_DASH);
            DrawHelper::moveHorizontalLine(name + "tp", order.tp, clrGreen, STYLE_DASH);
            
            orderInfo = "order[" + ordersFinished.Total() + "] profit: " + DoubleToString(order.profit, 3) + ", sl: " + DoubleToString(order.sl, 5); 
         }
      }
      
      DrawHelper::drawLabel("v-order-info", orderInfo, 300, 40, 2);
   }

   void drawPanel()
   {
      DrawHelper::drawLabel("v-orders-count", "count: " + IntegerToString(orders.Total()) + "(L:" + ordersLearning.Total() + ")", 100, 20, 3);
      if (analizeManager.isNewBar) {
         if (orders.Total() > 0) {
            OrderVirtual *order = orders.At(0);

            DrawHelper::drawLabel("v-order-price", "price: " + DoubleToString(order.price), 100, 40, 3);
            DrawHelper::drawLabel("v-order-sl", "sl: " + DoubleToString(order.sl), 100, 60, 3);
            DrawHelper::drawLabel("v-order-tp", "tp: " + DoubleToString(order.tp), 100, 80, 3);
         }
      }
   }

   string getDrawPrefixName()
   {
      return "virtualOrders:";
   }
};




















//+------------------------------------------------------------------+
