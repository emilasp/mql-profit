//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TsHelper
{
   // MqlRates rates[];
// trade.getRates(count, rates);
public:
   static void fillRates(int count, MqlRates &rates[])
   {
      int copied=CopyRates(_Symbol, PERIOD_CURRENT, 0, count, rates);
      if(copied<=0)
         Print("Ошибка копирования ценовых данных ", GetLastError());
      else Print("Скопировано ", ArraySize(rates), " баров");
   }

};

//+------------------------------------------------------------------+
