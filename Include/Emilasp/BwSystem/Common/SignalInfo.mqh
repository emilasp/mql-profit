//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Object.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class SignalInfo : public CObject
{
public:
   string nameIndicator;
   string nameBuffer;
   double value;
   string comment;
   
   void SignalInfo(string _nameIndicator, string _nameBuffer, double _value, string _comment = "") {
      nameIndicator=_nameIndicator;
      nameBuffer=_nameBuffer;
      value=_value;
      comment=_comment;
   }
};

//+------------------------------------------------------------------+
