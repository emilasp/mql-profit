//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\TradeCore\TcOrder.mqh>
#include <Emilasp\BwSystem\AnalizeManager.mqh>

enum ENUM_PYRAMIDING_TYPE {
   PYRAMIDING_DEV_VAR = 1,
   PYRAMIDING_FRACTAL = 2,
   PYRAMIDING_DEV_BAR_AND_FRACTAL = 3,
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class PyramidingManager
{
private:


public:
   bool enabled;
      ENUM_PYRAMIDING_TYPE type;
   double pyramidingVolumeSteps[];
   
   AnalizeManager *analizeManager;

   void PyramidingManager(bool _enabled, bool _type)
   {
      enabled = _enabled;
      type=_type;
      
      ArrayResize(pyramidingVolumeSteps, 5);
      pyramidingVolumeSteps[0] = 1;
      pyramidingVolumeSteps[1] = 4;
      pyramidingVolumeSteps[2] = 3;
      pyramidingVolumeSteps[3] = 2;
      pyramidingVolumeSteps[4] = 1;
   }

};
