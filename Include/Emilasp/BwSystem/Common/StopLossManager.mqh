//+------------------------------------------------------------------+
//|                                                    GridOrder.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\BwSystem\AnalizeManager.mqh>
#include <Emilasp\TradeCore\TcOrder.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>

enum ENUM_STOP_LOSS_TYPE {
   STOP_LOSS_TYPE_ATR = 1,
   STOP_LOSS_TYPE_PREV_BAR = 2,
   STOP_LOSS_TYPE_DEV_BAR = 3,
   STOP_LOSS_TYPE_FRACTALS = 4,
   STOP_LOSS_TYPE_DISTANCE = 5,
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class StopLossManager
{
private:
   bool trailingEnabled;
   ENUM_STOP_LOSS_TYPE trailingType;
   int trailingAddPt;
   double trailingKoef;

   ENUM_STOP_LOSS_TYPE stopLossType;
   int stopLossAddPt;
   double stopLossKoef;

   bool isModeLearning;

public:
   AnalizeManager *analizeManager;

   void StopLossManager(bool _isModeLearning)
   {
      isModeLearning=_isModeLearning;
   }

   void trailingSetup(bool _trailingEnabled, ENUM_STOP_LOSS_TYPE _trailingType, int _trailingAddPt = 0, double _trailingKoef = 1)
   {
      trailingEnabled = _trailingEnabled;
      trailingType = _trailingType;
      trailingAddPt = _trailingAddPt;
      trailingKoef = _trailingKoef;
   }

   void stopLossSetup(ENUM_STOP_LOSS_TYPE _stopLossType, int _stopLossAddPt = 0, double _stopLossKoef = 1)
   {
      stopLossType = _stopLossType;
      stopLossAddPt = _stopLossAddPt;
      stopLossKoef = _stopLossKoef;
   }

   double getStopLossByPrice(ENUM_STOP_LOSS_TYPE stopLossType, ENUM_POSITION_TYPE positiontype, double price);
   double getStopLossBase(ENUM_POSITION_TYPE positionType, double price);
   double getTralStopLoss(TcOrder &order);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double StopLossManager::getStopLossByPrice(ENUM_STOP_LOSS_TYPE slType, ENUM_POSITION_TYPE positiontype, double price)
{
   double stopLoss = EMPTY_VALUE;

   if(slType == STOP_LOSS_TYPE_ATR) {
      double atrDistancePt = analizeManager.lastArtValue * stopLossKoef;
      if(positiontype == POSITION_TYPE_BUY) {
         stopLoss = price - atrDistancePt * _Point;
      } else {
         stopLoss = price + atrDistancePt * _Point;
      }
   }
   if(slType == STOP_LOSS_TYPE_PREV_BAR) {
      double minLow  = EMPTY_VALUE;
      double maxHigh = EMPTY_VALUE;
      for(int i=1;i <= int(trailingKoef);i++) {
         MqlRates bar = analizeManager.lastBars[i];
         if (minLow == EMPTY_VALUE || minLow > bar.low) {
            minLow = bar.low;
         }
         if (maxHigh == EMPTY_VALUE || maxHigh < bar.high) {
            maxHigh = bar.high;
         }
      }
      
      if(positiontype == POSITION_TYPE_BUY) {
         stopLoss = minLow * stopLossKoef - stopLossAddPt * _Point;
      } else {
         stopLoss = maxHigh * stopLossKoef + stopLossAddPt * _Point;
      }
   }
   if(slType == STOP_LOSS_TYPE_DISTANCE) {
      if(positiontype == POSITION_TYPE_BUY) {
         stopLoss = price - stopLossAddPt * _Point;
      } else {
         stopLoss = price + stopLossAddPt * _Point;
      }
   }
   if (slType == STOP_LOSS_TYPE_FRACTALS) {
      sSignal resFractal = analizeManager.getSignalFractal(0);
      if (resFractal.hasSignal()) {
         if (resFractal.Buy) {
            double lows[5];
            lows[0] = analizeManager.lastBars[0].low;
            lows[1] = analizeManager.lastBars[1].low;
            lows[2] = analizeManager.lastBars[2].low;
            lows[3] = analizeManager.lastBars[3].low;
            lows[4] = analizeManager.lastBars[4].low;
            stopLoss = ArrayMinimum(lows, 0, 5);
         }
         if (resFractal.Sell) {
            double highs[5];
            highs[0] = analizeManager.lastBars[0].high;
            highs[1] = analizeManager.lastBars[1].high;
            highs[2] = analizeManager.lastBars[2].high;
            highs[3] = analizeManager.lastBars[3].high;
            highs[4] = analizeManager.lastBars[4].high;
            stopLoss = ArrayMaximum(highs, 0, 5);
         }
      }
   }
   if (slType == STOP_LOSS_TYPE_DEV_BAR) {

   }

   return stopLoss;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double StopLossManager::getStopLossBase(ENUM_POSITION_TYPE positionType, double price)
{
   double stopLoss = getStopLossByPrice(stopLossType, positionType, price);

   return stopLoss != EMPTY_VALUE ? stopLoss : 0;
}

//+------------------------------------------------------------------+
double StopLossManager::getTralStopLoss(TcOrder &order)
{
   double stopLoss = EMPTY_VALUE;

   if (trailingEnabled && !order.isEmpty && (isModeLearning || PositionSelect(_Symbol))) {
      ENUM_POSITION_TYPE positiontype;
      double lastStopLoss;
      double priceOrder;

      positiontype = order.positionType;
      lastStopLoss = order.sl;
      priceOrder = order.priceOpen;


      stopLoss = getStopLossByPrice(trailingType, positiontype, analizeManager.lastTick.ask);


      // Tral
      if (stopLoss != EMPTY_VALUE) {
         // Add to SL
         if (positiontype == POSITION_TYPE_BUY) {
            stopLoss -= trailingAddPt * _Point;
         }
         if (positiontype == POSITION_TYPE_SELL) {
            stopLoss += trailingAddPt * _Point;
         }
         // Check new StopLoss
         if (positiontype == POSITION_TYPE_BUY) {
            if (stopLoss > analizeManager.lastTick.ask && lastStopLoss < stopLoss) {
               stopLoss = EMPTY_VALUE;
            }
         }
         if (positiontype == POSITION_TYPE_SELL) {
            if (stopLoss < analizeManager.lastTick.bid && lastStopLoss > stopLoss) {
               stopLoss = EMPTY_VALUE;
            }
         }
      }
   }

   return stopLoss;
}

//+------------------------------------------------------------------+
