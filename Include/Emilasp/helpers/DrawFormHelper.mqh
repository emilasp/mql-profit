//+------------------------------------------------------------------+
//|                                                   DrawHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\libraries\Vendors\comment.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class DrawFormHelper
{
private:
   string            objects[];
public:
  
  
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Создает кнопку                                                   |
//+------------------------------------------------------------------+
bool static ButtonCreate(const string name="Button", const string text="Buy",
                  const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER,
                  const int x=0, const int y=0,
                  const int               width=50,                 // ширина кнопки
                  const int               height=18,                // высота кнопки
                  // угол графика для привязки
                  // текст
                  const string            font="Tahooma",             // шрифт
                  const int               font_size=10,             // размер шрифта
                  const color             clr=clrBlack,             // цвет текста
                  const color             back_clr=C'236,233,216',  // цвет фона
                  const color             border_clr=clrNONE,       // цвет границы
                  const bool              state=false,              // нажата/отжата
                  const bool              back=false,               // на заднем плане
                  const bool              selection=false,          // выделить для перемещений
                  const bool              hidden=true,              // скрыт в списке объектов
                  const long              chart_ID=0,
                  const int               sub_window=0,
                  const long              z_order=10)                // приоритет на нажатие мышью
{
   ResetLastError();//--- сбросим значение ошибки

   if(ObjectCreate(chart_ID, name, OBJ_BUTTON, sub_window, 0, 0)) {
      ObjectSetInteger(chart_ID, name, OBJPROP_XDISTANCE, x);
      ObjectSetInteger(chart_ID, name, OBJPROP_YDISTANCE, y);
      ObjectSetInteger(chart_ID, name, OBJPROP_XSIZE, width);
      ObjectSetInteger(chart_ID, name, OBJPROP_YSIZE, height);
      ObjectSetInteger(chart_ID, name, OBJPROP_CORNER, corner);
      ObjectSetString(chart_ID, name, OBJPROP_TEXT, text);
      ObjectSetString(chart_ID, name, OBJPROP_FONT, font);
      ObjectSetInteger(chart_ID, name, OBJPROP_FONTSIZE, font_size);
      ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BGCOLOR, back_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BORDER_COLOR, border_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);//--- отобразим на переднем (false) или заднем (true) плане
      ObjectSetInteger(chart_ID, name, OBJPROP_STATE, state);//--- переведем кнопку в заданное состояни
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, selection);//--- включим (true) или отключим (false) режим перемещения кнопки мышью
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
      ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);
   }
//--- успешное выполнение
   return(true);
}
//+------------------------------------------------------------------+
bool static RectLabelCreate(const string name="RectLabel",
                     const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER,
                     const int x=0, const int y=0,
                     const int              width=50,                 // ширина
                     const int              height=18,                // высота
                     const color            back_clr=C'236,233,216',  // цвет фона
                     const ENUM_BORDER_TYPE border=BORDER_SUNKEN,     // тип границы
                     // угол графика для привязки
                     const color            clr=clrRed,               // цвет плоской границы (Flat)
                     const ENUM_LINE_STYLE  style=STYLE_SOLID,        // стиль плоской границы
                     const int              line_width=1,             // толщина плоской границы
                     const bool             back=false,               // на заднем плане
                     const bool             selection=false,          // выделить для перемещений
                     const bool             hidden=true,              // скрыт в списке объектов
                     const long              chart_ID=0,
                     const int               sub_window=0,
                     const long             z_order=0)                // приоритет на нажатие мышью
{
   ResetLastError();//--- сбросим значение ошибки
   if(ObjectCreate(chart_ID, name, OBJ_RECTANGLE_LABEL, sub_window, 0, 0)) {
      ObjectSetInteger(chart_ID, name, OBJPROP_XDISTANCE, x);
      ObjectSetInteger(chart_ID, name, OBJPROP_YDISTANCE, y);
      ObjectSetInteger(chart_ID, name, OBJPROP_XSIZE, width);
      ObjectSetInteger(chart_ID, name, OBJPROP_YSIZE, height);
      ObjectSetInteger(chart_ID, name, OBJPROP_BGCOLOR, back_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BORDER_TYPE, border);
      ObjectSetInteger(chart_ID, name, OBJPROP_CORNER, corner);
      ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_STYLE, style);
      ObjectSetInteger(chart_ID, name, OBJPROP_WIDTH, line_width);
      ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
      ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);
   }
   return(true);
}
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Создает объект "Поле ввода"                                      |
//+------------------------------------------------------------------+
bool static EditCreate(const string name="Edit", const string text="Text",
                const int              x=0,                      // координата по оси X
                const int              y=0,                      // координата по оси Y
                const int              width=50,                 // ширина
                const int              height=18,                // высота 
                const string           font="Tahoma",             // шрифт
                const int              font_size=10,             // размер шрифта
                const ENUM_ALIGN_MODE  align=ALIGN_CENTER,       // способ выравнивания
                const bool             read_only=false,          // возможность редактировать
                const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // угол графика для привязки
                const color            clr=clrBlack,             // цвет текста
                const color            back_clr=clrWhite,        // цвет фона
                const color            border_clr=clrNONE,       // цвет границы
                const bool             back=false,               // на заднем плане
                const bool             selection=false,          // выделить для перемещений
                const bool             hidden=true,              // скрыт в списке объектов
                const long             chart_ID=0,
                const int              sub_window=0,             // номер подокна
                const long             z_order=0)                // приоритет на нажатие мышью
{
   ResetLastError();
//--- создадим поле ввода
   if(ObjectCreate(chart_ID, name, OBJ_EDIT, sub_window, 0, 0)) {
      ObjectSetInteger(chart_ID, name, OBJPROP_XDISTANCE, x);
      ObjectSetInteger(chart_ID, name, OBJPROP_YDISTANCE, y);
      ObjectSetInteger(chart_ID, name, OBJPROP_XSIZE, width);
      ObjectSetInteger(chart_ID, name, OBJPROP_YSIZE, height);
      ObjectSetString(chart_ID, name, OBJPROP_TEXT, text);
      ObjectSetString(chart_ID, name, OBJPROP_FONT, font);
      ObjectSetInteger(chart_ID, name, OBJPROP_FONTSIZE, font_size);
      ObjectSetInteger(chart_ID, name, OBJPROP_ALIGN, align);
      ObjectSetInteger(chart_ID, name, OBJPROP_READONLY, read_only);
      ObjectSetInteger(chart_ID, name, OBJPROP_CORNER, corner);
      ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BGCOLOR, back_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BORDER_COLOR, border_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
      ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);
   }
   return(true);
}

};


