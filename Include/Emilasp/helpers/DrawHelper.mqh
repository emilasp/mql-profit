//+------------------------------------------------------------------+
//|                                                   DrawHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\libraries\Vendors\comment.mqh>
#include <Emilasp\helpers\DrawFormHelper.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class DrawHelper
{
private:
   string            objects[];
public:
   static void       drawLabel(string name, string text, int xdistance = 10, int ydistance = 20, int corner = 0, int windowId = 0);
   static void       drawText(string name, string text, datetime time, double price, int clr = clrAliceBlue);
   static void       moveHorizontalLine(string name, double price, int lineColor, ENUM_LINE_STYLE lineStyle = STYLE_DOT, int width = 1);
   static void       drawArrow(string name, datetime time, double price, int symbolNum = 159, int clr = clrAliceBlue, int chartId = 0, int windowId = 0, int fontSize = 14);
   static void       drawRectangle(string name, datetime leftTime, double leftPrice,  datetime rightTime, double rightPrice, int sdvigPt = 30, int clr = clrAliceBlue);
   static void       drawTrendLine(int chartId, int windId, string name, datetime t1, double v1, datetime t2, double v2, color tlColor, int style = STYLE_SOLID, int width = 1, bool back=true, string desc="");

   static int        deleteObjectsByName(string namePrefix);
   static void       drawInfoPanel(CComment &comment, string name, string &lines[], int startLine, int x, int y);
   
   static void drawRectangleLabel(const long chart_ID=0,               // ID графика
      const string           name="RectLabel",         // имя метки
      const int              sub_window=0,             // номер подокна
      const int              x=0,                      // координата по оси X
      const int              y=0,                      // координата по оси Y
      const int              width=50,                 // ширина
      const int              height=18,                // высота
      const color            back_clr=C'236,233,216',  // цвет фона
      const ENUM_BORDER_TYPE border=BORDER_SUNKEN,     // тип границы
      const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // угол графика для привязки
      const color            clr=clrRed,               // цвет плоской границы (Flat)
      const ENUM_LINE_STYLE  style=STYLE_SOLID,        // стиль плоской границы
      const int              line_width=1,             // толщина плоской границы
      const bool             back=false,               // на заднем плане
      const bool             selection=false,          // выделить для перемещений
      const bool             hidden=true,              // скрыт в списке объектов
      const long             z_order=0);                // приоритет на нажатие мышью

};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawRectangleLabel(const long chart_ID=0,               // ID графика
      const string           name="RectLabel",         // имя метки
      const int              sub_window=0,             // номер подокна
      const int              x=0,                      // координата по оси X
      const int              y=0,                      // координата по оси Y
      const int              width=50,                 // ширина
      const int              height=18,                // высота
      const color            back_clr=C'236,233,216',  // цвет фона
      const ENUM_BORDER_TYPE border=BORDER_SUNKEN,     // тип границы
      const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // угол графика для привязки
      const color            clr=clrRed,               // цвет плоской границы (Flat)
      const ENUM_LINE_STYLE  style=STYLE_SOLID,        // стиль плоской границы
      const int              line_width=1,             // толщина плоской границы
      const bool             back=false,               // на заднем плане
      const bool             selection=false,          // выделить для перемещений
      const bool             hidden=true,              // скрыт в списке объектов
      const long             z_order=0)                // приоритет на нажатие мышью
{
//--- сбросим значение ошибки
   ResetLastError();
//--- создадим прямоугольную метку
   if(ObjectCreate(chart_ID, name, OBJ_RECTANGLE_LABEL, sub_window, 0, 0)) {
      ObjectSetInteger(chart_ID, name, OBJPROP_XDISTANCE, x);
      ObjectSetInteger(chart_ID, name, OBJPROP_YDISTANCE, y);
      ObjectSetInteger(chart_ID, name, OBJPROP_XSIZE, width);
      ObjectSetInteger(chart_ID, name, OBJPROP_YSIZE, height);
      ObjectSetInteger(chart_ID, name, OBJPROP_BGCOLOR, back_clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_BORDER_TYPE, border);
      ObjectSetInteger(chart_ID, name, OBJPROP_CORNER, corner);
      ObjectSetInteger(chart_ID, name, OBJPROP_COLOR, clr);
      ObjectSetInteger(chart_ID, name, OBJPROP_STYLE, style);
      ObjectSetInteger(chart_ID, name, OBJPROP_WIDTH, line_width);
      ObjectSetInteger(chart_ID, name, OBJPROP_BACK, back);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTABLE, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_SELECTED, selection);
      ObjectSetInteger(chart_ID, name, OBJPROP_HIDDEN, hidden);
      ObjectSetInteger(chart_ID, name, OBJPROP_ZORDER, z_order);
   }
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawLabel(string name, string text, int xdistance = 10, int ydistance = 20, int corner = 0, int windowId = 0)
{
   if(ObjectFind(0, name) == -1) {
      ObjectCreate(0, name, OBJ_LABEL, windowId, 0, 0);
   }

   ObjectSetInteger(0, name, OBJPROP_CORNER, corner);// Угол привязки
   ObjectSetInteger(0, name, OBJPROP_XDISTANCE, xdistance);// Угол привязки
   ObjectSetInteger(0, name, OBJPROP_YDISTANCE, ydistance);// Угол привязки
   ObjectSetInteger(0, name, OBJPROP_COLOR, clrWhite);// Угол привязки
   ObjectSetInteger(0, name, OBJPROP_BACK, false);
   ObjectSetString(0, name, OBJPROP_TEXT, text);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawText(string name, string text, datetime time, double price, int clr = clrAliceBlue)
{
   if(ObjectFind(0, name) == -1) {
      ObjectCreate(0, name, OBJ_TEXT, 0, 0, 0);
   }

   ObjectSetString(0, name, OBJPROP_TEXT, text);
   ObjectSetInteger(0, name, OBJPROP_COLOR, clr);
   ObjectMove(0, name, 0, time, price);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::moveHorizontalLine(string name, double price, int lineColor, ENUM_LINE_STYLE lineStyle = STYLE_DOT, int width = 1)
{
   if(ObjectFind(0, name) == -1) {
      ObjectCreate(0, name, OBJ_HLINE, 0, 0, 0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
      ObjectSetInteger(0, name, OBJPROP_STYLE, lineStyle);
      ObjectSetInteger(0, name, OBJPROP_WIDTH, width);
      ObjectSetInteger(0, name, OBJPROP_BACK, false);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, false);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, false);
      ObjectSetInteger(0, name, OBJPROP_RAY_RIGHT, false);
      ObjectSetInteger(0, name, OBJPROP_HIDDEN, true);
      ObjectSetInteger(0, name, OBJPROP_ZORDER, 0);
   }
   ObjectSetDouble(0, name, OBJPROP_PRICE, price);
   ObjectSetInteger(0, name, OBJPROP_COLOR, lineColor);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawArrow(string name, datetime time, double price, int symbolNum = 159, int clr = clrAliceBlue, int chartId = 0, int windowId = 0, int fontSize = 14)
{
   if (ObjectFind(chartId, name) != -1) {
      ObjectDelete(chartId, name);
   }
   ObjectCreate(chartId, name, OBJ_ARROW, windowId, time, price);
   ObjectSetInteger(chartId, name, OBJPROP_ARROWCODE, symbolNum);
   ObjectSetString(chartId, name, OBJPROP_FONT, "Wingdings");
   ObjectSetInteger(chartId, name, OBJPROP_FONTSIZE, 14);
   ObjectSetInteger(chartId, name, OBJPROP_COLOR, clr);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawRectangle(string name, datetime leftTime, double leftPrice,  datetime rightTime, double rightPrice, int sdvigPt = 30, int clr = clrAliceBlue)
{
   if (ObjectFind(0, name) == -1) {
      double sdvig = sdvigPt * _Point;
      ObjectCreate(0, name, OBJ_RECTANGLE, 0, leftTime, leftPrice + sdvig, rightTime, rightPrice + sdvig);
   }

   ObjectSetInteger(0, name, OBJPROP_COLOR, clr);
   ObjectSetInteger(0, name, OBJPROP_BACK, false);
   ObjectSetInteger(0, name, OBJPROP_WIDTH, 1);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawTrendLine(int chartId, int windId, string name, datetime t1, double v1, datetime t2, double v2, color tlColor, int style = STYLE_SOLID, int width = 1, bool back=true, string desc="")
{
   if(ObjectFind(chartId, name) != 0) {
      ResetLastError();
      if(!ObjectCreate(chartId, name, OBJ_TREND, windId, t1, v1, t2, v2)) {
         Print(__FUNCTION__, ": failed to draw trend line! Error code: ", GetLastError());
      }
      ;
   } else {
      ObjectMove(chartId, name, 0, t1, v1);
      ObjectMove(chartId, name, 1, t2, v2);
   }
   ObjectSetInteger(chartId, name, OBJPROP_COLOR, tlColor);
   ObjectSetInteger(chartId, name, OBJPROP_RAY,   false);
   ObjectSetInteger(chartId, name, OBJPROP_STYLE, style);
   ObjectSetInteger(chartId, name, OBJPROP_WIDTH, width);
   ObjectSetInteger(chartId, name, OBJPROP_BACK,  false);
//ObjectSetString(tlname, desc);
}


//+------------------------------------------------------------------+
static int DrawHelper::deleteObjectsByName(string namePrefix)
{
   int countDeletedAll;
   int countObjects = ObjectsTotal(0, 0, -1);
   for(int i=0; i<countObjects; i++)
      if(StringFind(ObjectName(0, i, 0, -1), namePrefix, 0) > 0) {
         ObjectDelete(0, ObjectName(0, i, 0, -1));
         countDeletedAll++;
      }
   return countDeletedAll;
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static void DrawHelper::drawInfoPanel(CComment &comment, string name, string &lines[], int startLine, int x, int y)
{

//visual_mode=MQLInfoInteger(MQL_VISUAL_MODE);

   if (ObjectFind(0, name) == -1) {
      if(ChartGetInteger(0, CHART_SHOW_ONE_CLICK))
         y=120;

      comment.Create(name, 20, y);
      //--- panel style
      comment.SetAutoColors(true);
      comment.SetColor(clrDimGray, clrBlack, 255);
      comment.SetFont("Lucida Console", 13, false, 1.7);

      //---
#ifdef __MQL5__
      comment.SetGraphMode(MQLInfoInteger(MQL_TESTER));
#endif
   }

   int totalLines = ArraySize(lines);
   for(int i=0; i<totalLines; i++) {
      string line = lines[i];

      comment.SetText(i + startLine, line, clrDodgerBlue);
   }

   comment.Show();
}
//+------------------------------------------------------------------+
