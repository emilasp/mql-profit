//+------------------------------------------------------------------+
//|                                                   CoreHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayString.mqh>
#include <Files\File.mqh>
#include <Files\FileTxt.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\libraries\json\JAson.mqh>

CFileTxt     *File;


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CoreHelper
{
private:


public:
   void              addPatternToFile(string symbol, string period, string baseName,  string &features[]);
   string            PeriodToStr(ENUM_TIMEFRAMES period);
   int               findStringInArray(string& array[], string value);
   double            getSumBodyRates(MqlRates& rates[], int start, int end, bool isAbs);

   static void         saveDataToCsv(string file, string data, bool append);
   static void         saveDataToFileAsJsonArray(string file, CJAVal& arrayItems, bool append);
   void static       CoreHelper::readFileToArray(string FileName, CArrayString &rows);
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CoreHelper::addPatternToFile(string symbol, string period, string baseName,  string &features[])
{
   string filename = symbol;
   StringConcatenate(baseName, filename, symbol, "_", period, ".CSV");

   int iMaxBar=TerminalInfoInteger(TERMINAL_MAXBARS);
   string format="%G,%G,%G,%G,%d";

   int size = ArraySize(features);
   if(size>0) {
      // Открыть файл
      File.Open(filename, FILE_WRITE, 9);

      string data = "";
      for(int i=0; i<size; i++) {
         data += features[i];

         if (size > i + 1)
            data += ",";

      }
      data += "\n";
      File.WriteString(data);
      File.Close();
   }
   Comment("OK. готово... ");
}



//+------------------------------------------------------------------+
//| Конвертацмя периода в строку                                     |
//+------------------------------------------------------------------+
string CoreHelper::PeriodToStr(ENUM_TIMEFRAMES period)
{
   string strper = "---";
//---
   switch(period) {
   case PERIOD_MN1 :
      strper="MN1";
      break;
   case PERIOD_W1 :
      strper="W1";
      break;
   case PERIOD_D1 :
      strper="D1";
      break;
   case PERIOD_H1 :
      strper="H1";
      break;
   case PERIOD_H2 :
      strper="H2";
      break;
   case PERIOD_H3 :
      strper="H3";
      break;
   case PERIOD_H4 :
      strper="H4";
      break;
   case PERIOD_H6 :
      strper="H6";
      break;
   case PERIOD_H8 :
      strper="H8";
      break;
   case PERIOD_H12 :
      strper="H12";
      break;
   case PERIOD_M1 :
      strper="M1";
      break;
   case PERIOD_M2 :
      strper="M2";
      break;
   case PERIOD_M3 :
      strper="M3";
      break;
   case PERIOD_M4 :
      strper="M4";
      break;
   case PERIOD_M5 :
      strper="M5";
      break;
   case PERIOD_M6 :
      strper="M6";
      break;
   case PERIOD_M10 :
      strper="M10";
      break;
   case PERIOD_M12 :
      strper="M12";
      break;
   case PERIOD_M15 :
      strper="M15";
      break;
   case PERIOD_M20 :
      strper="M20";
      break;
   case PERIOD_M30 :
      strper="M30";
      break;
   }
//---
   return(strper);
}
//+------------------------------------------------------------------+



///////////////// BASE /////////////////////////
int CoreHelper::findStringInArray(string& array[], string value)
{
   int size = ArraySize(array);
   for(int i = 0; i < size; i++) {
      if(array[i] == value) {
         return i;
      }
   }
   return -1;
}


/**
* Получаем сумму тел свечей за определенное количетсво баров
**/
double CoreHelper::getSumBodyRates(MqlRates& rates[], int start, int end, bool isAbs)
{
   double sum = 0;
   for(int i = start; i <= end; i++) {
      if(isAbs)
         sum += MathAbs(rates[i].open - rates[i].close);
      else
         sum += rates[i].open - rates[i].close;
   }
   return sum;
}


//  bool HLineMove(const long   chart_ID=0, const string name="HLine", double price=0)
//  {
//   if(!price)
//      price=SymbolInfoDouble(Symbol(),SYMBOL_BID);
//   ResetLastError();
//   if(!ObjectMove(chart_ID,name,0,0,price))
//     {
//      Print(__FUNCTION__,
//            ": не удалось переместить горизонтальную линию! Код ошибки = ",GetLastError());
//      return(false);
//     }
//--- успешное выполнение
//   return(true);
//  }





// Core Cocket
void static CoreHelper::saveDataToCsv(string file, string data, bool append)
{
//string InpFileName = "yyy\\Files\\exportFromMql.csv";
   data += "\r\n";
   PrintFormat("Путь к файлу: %s\\Files\\", TerminalInfoString(TERMINAL_DATA_PATH));
   int file_handle;

   if(append) {
      file_handle=FileOpen(file, FILE_READ|FILE_WRITE|FILE_CSV|FILE_ANSI|FILE_COMMON);
   } else {
      file_handle=FileOpen(file, FILE_WRITE|FILE_CSV|FILE_ANSI|FILE_COMMON);
   }

   if(file_handle != INVALID_HANDLE) {
      PrintFormat("Файл %s открыт для записи. %s. %s", file, TerminalInfoString(TERMINAL_COMMONDATA_PATH));
      PrintFormat("Путь к файлу: %s\\Files\\", TerminalInfoString(TERMINAL_DATA_PATH));

      FileSeek(file_handle, 0, SEEK_END);
      FileWriteString(file_handle, data, StringLen(data));
      FileFlush(file_handle);
      FileClose(file_handle);

      PrintFormat("Данные записаны, файл %s закрыт", file);
   } else {
      PrintFormat("Не удалось открыть файл %s, Код ошибки = %d", file, GetLastError());
   }
}
//+------------------------------------------------------------------+

// C:\Users\Emilasp\AppData\Roaming\MetaQuotes\Terminal\Common\Files
void static CoreHelper::saveDataToFileAsJsonArray(string file, CJAVal& arrayItems, bool append)
{

// Read file

//string InpFileName = "yyy\\Files\\exportFromMql.csv";

   int quantsCount = arrayItems["quants"].Size();

   string data = "";
   for(int i=0; i<quantsCount; i++) {
      CJAVal quant = arrayItems["quants"][i];
      data += quant.Serialize() + "," + "\n\r";
   }

   PrintFormat("Путь к файлу: %s\\Files\\", TerminalInfoString(TERMINAL_DATA_PATH));
   int file_handle;

//if(append)
//  {
   file_handle=FileOpen(file, FILE_READ|FILE_WRITE|FILE_TXT|FILE_ANSI|FILE_COMMON);
//  }
//else
//  {
//file_handle=FileOpen(file, FILE_WRITE|FILE_TXT|FILE_ANSI|FILE_COMMON);
//  }

   if(file_handle != INVALID_HANDLE) {
      PrintFormat("Файл %s открыт для записи. %s. %s", file, TerminalInfoString(TERMINAL_COMMONDATA_PATH));
      PrintFormat("Путь к файлу: %s\\Files\\", TerminalInfoString(TERMINAL_DATA_PATH));

      FileSeek(file_handle, 0, SEEK_END);
      FileWriteString(file_handle, data, StringLen(data));
      FileFlush(file_handle);
      FileClose(file_handle);

      PrintFormat("Данные записаны, файл %s закрыт", file);
   } else {
      PrintFormat("Не удалось открыть файл %s, Код ошибки = %d", file, GetLastError());
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void static CoreHelper::readFileToArray(string FileName, CArrayString &rows)
{
   ResetLastError();
   int h=FileOpen(FileName, FILE_WRITE|FILE_READ|FILE_ANSI|FILE_TXT|FILE_COMMON);
   if(h==INVALID_HANDLE) {
      int ErrNum=GetLastError();
      printf("Ошибка открытия файла %s # %i", FileName, ErrNum);
   }
   int cnt=0; // при помощи этой переменной будем считать количество строк файла
   while(!FileIsEnding(h)) {
      string str=FileReadString(h); // читаем очередную строку из файла
      // обрезаем пробелы слева и справа, чтобы выявить и не использовать пустые строки
      StringTrimLeft(str);
      StringTrimRight(str);
      if(str!="") {
         rows.Add(str);
         cnt++; // увеличиваем счетчик прочитанных строк
      }
   }
   FileClose(h);
}
