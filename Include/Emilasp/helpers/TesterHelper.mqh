//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2020, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Emil Fernando."
#property link      "http://www.mql5.com"
#property strict

#import "user32.dll"
void keybd_event(int bVk, int bScan, int dwFlags, int dwExtraInfo);
#import
#define VK_SPACE 0x20 //Space
#define VK_RETURN 0x0D //Return - Enter Key
#define KEYEVENTF_KEYUP 0x0002  //Key up

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TesterHelper
{
private:
public:
   static void       pause(bool isEnter = false)
   {
      if(MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE)) {
         if(isEnter) {
            keybd_event(VK_SPACE, 0, 0, 0);
         } else {
            keybd_event(VK_SPACE, 0, 0, 0);
            keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
         }
      }
   }
};
//+------------------------------------------------------------------+
