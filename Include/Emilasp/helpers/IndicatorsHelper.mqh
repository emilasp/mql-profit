//+------------------------------------------------------------------+
//|                                             IndicatorsHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


class IndicatorsHelper
  {
   private:          


   public:
       virtual bool iGetArray(const int handle,const int buffer,const int start_pos, const int count,double &arr_buffer[]);
  };
     
     
  /**
  *** Получаем массив с буфером пользовательского индикатора
  *
  *   double up[],down[];
  *   ArraySetAsSeries(up,true);
  *   ArraySetAsSeries(down,true);
  *   int start_pos=0,count=13;
  *   if(!indicatorsHelper.iGetArray(handle_TL,0,start_pos,count,up) || !indicatorsHelper.iGetArray(handle_TL,1,start_pos,count,down))
  *    {
  *     return;
  *    }
  *
  *
  **/
  bool IndicatorsHelper::iGetArray(const int handle,const int buffer,const int start_pos, const int count,double &arr_buffer[])
  {
   bool result=true;
   if(!ArrayIsDynamic(arr_buffer))
     {
      PrintFormat("ERROR! EA: %s, FUNCTION: %s, this a no dynamic array!",__FILE__,__FUNCTION__);
      return(false);
     }
   ArrayFree(arr_buffer);
//--- reset error code
   ResetLastError();
//--- fill a part of the iBands array with values from the indicator buffer
   int copied=CopyBuffer(handle,buffer,start_pos,count,arr_buffer);
   if(copied!=count)
     {
      //--- if the copying fails, tell the error code
      PrintFormat("ERROR! EA: %s, FUNCTION: %s, amount to copy: %d, copied: %d, error code %d",
                  __FILE__,__FUNCTION__,count,copied,GetLastError());
      //--- quit with zero result - it means that the indicator is considered as not calculated
      return(false);
     }
   return(result);
  }