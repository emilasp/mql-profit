//+------------------------------------------------------------------+
//|                                                   BarsHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


class BarsHelper
  {
   private:          


   public:
       static double getAvgSizeBarBody(MqlRates& rates_array[], int countBars);
       static double getAvgSizeBarBody(int countBars, const double &open[], const double &close[]);
       static int chainSomeBars(int index, int countBars, const double &open[], const double &close[]);
       
  };
     
     
   /**
  * Возвращает средний размер тела свечи
  **/
 static  double BarsHelper::getAvgSizeBarBody(MqlRates& rates_array[], int countBars)
  {
      double closeSum = 0;
      double openSum = 0;
      for (int i = 0; i < countBars; i++) {
         openSum += rates_array[i].open;
         closeSum += rates_array[i].close;
      }
      
      return MathAbs(openSum - closeSum);
  }
   static  double BarsHelper::getAvgSizeBarBody(int countBars, const double &open[], const double &close[])
  {
      double closeSum = 0;
      double openSum = 0;
      for (int i = 0; i < countBars; i++) {
         openSum += open[i];
         closeSum += close[i];
      }
      
      return MathAbs(openSum - closeSum);
  }
  
  //+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
static int BarsHelper::chainSomeBars(int index, int countBars, const double &open[], const double &close[])
  {
   int countLong = 0;
   int countShort = 0;
   for(int i=0; i<countBars; i++)
     {
      if(open[index - i] < close[index - i])
         countLong++;
      if(open[index - i] > close[index - i])
         countShort++;
     }
   if(countLong > 0 && countShort < 2)
      return 1;
   if(countShort > 0 && countLong < 2)
      return -1;
   return 0;
  }

