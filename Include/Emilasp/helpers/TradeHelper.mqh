//+------------------------------------------------------------------+
//|                                                  TradeHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
class TradeHelper {
 private:

 public:
   TradeHelper();
   ~TradeHelper();
   double getAddPricePt(double price, int pt);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TradeHelper::TradeHelper() {
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TradeHelper::~TradeHelper() {
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double TradeHelper::getAddPricePt(double price, int pt) {
   return price + pt * point;
}