//+------------------------------------------------------------------+
//|                                                  TradeSignal.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


struct sSignal {
   bool              Buy;
   bool              Sell;

   void              sSignal()
   {
      Buy = false;
      Sell = false;
   }
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CSignal
{
private:
   int               m_handle[];

public:
   void              Set_1(string file, string &v[], int par);
   sSignal           GetSignal(int indx);
                     CSignal();
                    ~CSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CSignal::CSignal()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CSignal::~CSignal()
{
}
//+------------------------------------------------------------------+
void CSignal::Set_1(string file, string &V[], int par)
{
   int total = ArraySize(V);
   ArrayResize(m_handle, total);

   for(int i=0; i<total; i++)
      m_handle[i] = iCustom(V[i], PERIOD_M1, file, par);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal CSignal::GetSignal(int indx)
{
   sSignal sig;
   double bs[];
   ArraySetAsSeries(bs, true);
   CopyBuffer(m_handle[indx], 0, 0, 1, bs);
   if(bs[0] > 0)
      sig.Buy=true;
   if(bs[0] < 0)
      sig.Sell=true;
   return sig;
}
//+------------------------------------------------------------------+
