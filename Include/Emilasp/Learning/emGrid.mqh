//+------------------------------------------------------------------+
//|                                                       emGrid.mqh |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"

#include <Trade\Trade.mqh>

//---
#define STR1 ObjectCreate(0, name, OBJ_HLINE,0,0,0)
#define STR2 ObjectSetDouble(0,name,OBJPROP_PRICE,line_price)
#define STR3 ObjectSetInteger(0,name,OBJPROP_STYLE,STYLE_DOT)
#define STR4 ObjectSetInteger(0,name,OBJPROP_COLOR, clrBlue)

#define STR STR1;STR2;STR3;STR4;

//---
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };



struct sGrid
  {
   int               h;
   int               dist;// в пунктах
   void              sGrid()
     {
      h=50;
      dist=100;
     }
   void              Set(int H, int DIST)
     {
      h=H;
      dist=DIST;
     }
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class emGrid
  {
private:
   sGrid             m_up;
   sGrid             m_dn;
   bool              m_on_fixed;
   CTrade            m_trade;
   double            m_lot_min;
   double            m_lot_max;
   double            m_lot_k;
   double            m_lot[];
   int               m_line_max;// count lines lots
   int               m_line; // Levels
   int               m_direct; //  start direct position
   bool              m_trend;
   double            m_SL;
   double            m_TP;
   double            m_price_up;
   double            m_price_dn;
   string            m_name_up;
   string            m_name_dn;




public:
   ///   bool              Init(sGrid &up, sGrid &dn, double lot=0.01, bool trend = false);
   bool              InitGrid(int _h, int _dist, double _k_grid=1, bool _on_trend=false);
   bool              InitLot(double _minlot, double _maxlot, double _klot=1.0);
   bool              Init(int sl, int tp);
   void              DrawGrid(double price);
   void              DrawGrid(double price, bool direct); // buy - true, sell - false
   void              TradeGrid(double lot);
   void              Tick(sSignal &signal);
   void              VisualGrid();
                     emGrid();
                    ~emGrid();

   void              Visual(sSignal &signal)
     {
      double O[];
      ArraySetAsSeries(O, true);
      CopyOpen(_Symbol,_Period,0,1,O);
      datetime T[];
      ArraySetAsSeries(T, true);
      CopyTime(_Symbol,_Period,0,1,T);

      string name = "BUY" + (string)T[0];
      if(signal.Buy)
        {
         ObjectCreate(0, name, OBJ_ARROW_BUY,0,0,0);
         ObjectSetDouble(0,name,OBJPROP_PRICE,O[0]);
         ObjectSetInteger(0,name,OBJPROP_TIME,T[0]);
        }
      name = "SELL" + (string)T[0];
      if(signal.Sell)
        {
         ObjectCreate(0, name, OBJ_ARROW_SELL,0,0,0);
         ObjectSetDouble(0,name,OBJPROP_PRICE,O[0]);
         ObjectSetInteger(0,name,OBJPROP_TIME,T[0]);
        }
     }

  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void emGrid::VisualGrid(void)
  {
   datetime T[];
   ArraySetAsSeries(T, true);
   CopyTime(_Symbol,_Period,0,1,T);
// UP
   string name = m_name_up + (string)T[0];
   double line_price=ObjectGetDouble(0, m_name_up, OBJPROP_PRICE);
   STR
//DWN
   name = m_name_dn + (string)T[0];
   line_price=ObjectGetDouble(0, m_name_dn, OBJPROP_PRICE);
   STR
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              emGrid::InitGrid(int _h, int _dist, double _k_grid=1, bool _on_trend=false)
  {
   m_up.Set(_h,_dist);
   int dn_h = (int)(_h * _k_grid);
   m_dn.Set(dn_h, _dist);

   m_trend = _on_trend;

   return true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              emGrid::InitLot(double _minlot, double _maxlot, double _klot=1.0)
  {
   m_lot_min=_minlot;
   m_lot_max=_maxlot;
   m_lot_k=_klot;

   int count=0;
   double lot=m_lot_min;
   double sum_lot=lot;
   do
     {
      count++;
      lot      *= m_lot_k;
      sum_lot  += lot;
     }
   while(sum_lot < m_lot_max);
   m_line_max = count;
   ArrayResize(m_lot, count);

   m_lot[0] = m_lot_min;
   for(int i=0; i<count; i++)
     {
      m_lot[i] = NormalizeDouble(m_lot[i-1] * m_lot_k,2);

     }
     Print(m_line_max);
      ArrayPrint(m_lot);
   return true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool              emGrid::Init(int sl, int tp)
  {
   m_SL=sl;
   m_TP=tp;
   return true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void emGrid::Tick(sSignal &signal)
  {
   if(PositionSelect(_Symbol))
     {
      double profit = PositionGetDouble(POSITION_PROFIT);

      if(profit < -m_SL || profit > m_TP)
        {
         m_trade.PositionClose(_Symbol);
         m_on_fixed = false;
        }
     }


   bool on_signal=signal.Buy || signal.Sell;
   if(on_signal)
      Visual(signal);

   if(!PositionSelect(_Symbol))
      if(!m_trend)
         if(!m_on_fixed)
           {
            MqlTick last_tick;
            SymbolInfoTick(_Symbol, last_tick);


            DrawGrid(last_tick.bid);

            if(on_signal)
               m_on_fixed = true;
           }
// --- trade
   if(m_on_fixed)
     {
      if(!PositionSelect(_Symbol))
        {
         m_direct = 0;
         m_line=0;
        }

      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      // --- BUY
      if(last_tick.ask < m_price_dn)
        {
         int n_line = m_line;
         if(PositionSelect(_Symbol))
            if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
               if(n_line > 0)
                  n_line = m_line - 1;

         bool res=false;
         if(m_line_max > m_line)
            res=m_trade.Buy(m_lot[n_line]);

         if(res)
           {
            VisualGrid();
            DrawGrid(last_tick.bid, true);

            if(PositionSelect(_Symbol))
              {
               if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
                  if(m_line_max - 1 > m_line)
                     m_line++;
               if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
                  if(n_line > 0)
                     m_line--;
              }
           }
        }
      // SELL---
      if(last_tick.bid > m_price_up)
        {
         int n_line = m_line;
         if(PositionSelect(_Symbol))
            if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
               if(n_line > 0)
                  n_line = m_line - 1;

         bool res=false;
         if(m_line_max > m_line)
            res=m_trade.Sell(m_lot[n_line]);
         if(res)
           {
            VisualGrid();
            DrawGrid(last_tick.bid, false);

            if(PositionSelect(_Symbol))
              {
               if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
                  if(m_line_max - 1 > m_line)
                     m_line++;
               if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
                  if(n_line > 0)
                     m_line--;
              }
           }
        }


     }
// ---
   if(!m_trend)
     {
     }
  }
//+------------------------------------------------------------------+
void emGrid::TradeGrid(double lot)
  {
   if(m_on_fixed)
     {
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      DrawGrid(last_tick.bid);
     }
  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void emGrid::DrawGrid(double price, bool direct)
  {
//--- BUY
   if(direct)
     {
      // UP
      ObjectCreate(0, m_name_up, OBJ_HLINE,0,0,0);
      m_price_up=price + (m_up.dist + m_dn.dist - m_dn.h) * _Point;
      ObjectSetDouble(0,m_name_up,OBJPROP_PRICE,m_price_up);
      //DWN
      ObjectCreate(0, m_name_dn, OBJ_HLINE,0,0,0);
      m_price_dn=price - m_dn.h * _Point;
      ObjectSetDouble(0,m_name_dn,OBJPROP_PRICE,m_price_dn);
     }
   else
     {
      // UP
      ObjectCreate(0, m_name_up, OBJ_HLINE,0,0,0);
      m_price_up=price + m_up.h * _Point;
      ObjectSetDouble(0,m_name_up,OBJPROP_PRICE,m_price_up);
      //DWN
      ObjectCreate(0, m_name_dn, OBJ_HLINE,0,0,0);
      m_price_dn=price - (m_up.dist + m_dn.dist - m_up.h) * _Point;
      ObjectSetDouble(0,m_name_dn,OBJPROP_PRICE,m_price_dn);
     }

  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void emGrid::DrawGrid(double price)
  {
// UP
   ObjectCreate(0, m_name_up, OBJ_HLINE,0,0,0);
   m_price_up=price + m_up.dist * _Point;
   ObjectSetDouble(0,m_name_up,OBJPROP_PRICE,m_price_up);
//DWN
   ObjectCreate(0, m_name_dn, OBJ_HLINE,0,0,0);
   m_price_dn=price - m_dn.dist * _Point;
   ObjectSetDouble(0,m_name_dn,OBJPROP_PRICE,m_price_dn);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
emGrid::emGrid()
  {
   m_on_fixed=false;
   m_lot_min=0.01;

   m_name_up = "up";
   m_name_dn = "dn";

   m_line = 0;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
emGrid::~emGrid()
  {
//UP
   ObjectDelete(0, m_name_up);

//DWN
   ObjectDelete(0, m_name_dn);
  }
//+------------------------------------------------------------------+
