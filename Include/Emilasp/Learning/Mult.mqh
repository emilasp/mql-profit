//+------------------------------------------------------------------+
//|                                                         Mult.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>
#include <Emilasp\Learning\Signal.mqh> 
#include <Emilasp\Learning\Filter.mqh> 

class CEmTrade : public CTrade
{
public:
   bool              emBuy(const string symbol=NULL, const double volume=0.01,const int sl=100,const int tp=100,double price=0.0,const string comment="");
   bool              emSell(const string symbol=NULL, const double volume=0.01,const int sl=100,const int tp=100,double price=0.0,const string comment="");
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
};



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CMult
{
private:
   string            m_V[];
   int               m_total_valut;
   double            m_lot;
   bool              m_on_MM;
   CSignal           m_generator;
   CEmTrade          m_trade;
   sSignal           m_signal[];


public:
   void              Trade();
   void              SetValuta(string &arr[]);
   void              SetValuta(string val);
   void              SetMM(double lots, bool mm=false);
   void              SetSignal(string file, int period);
     
   void              CloseAll(double sl, double tp);
   
                     CMult();
                    ~CMult();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CMult::CMult()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CMult::~CMult()
{
}
//+------------------------------------------------------------------+
void CMult::Trade()
{
   for(int i=0; i<m_total_valut; i++) {
      if(!PositionSelect(m_V[i])) {
         //Buy
         if(m_generator.GetSignal(i).Buy)
           m_trade.emBuy(m_V[i], m_lot, 500);
         //Sell
         if(m_generator.GetSignal(i).Sell)
           m_trade.emSell(m_V[i], m_lot, 500);
      }
   }
}
//+------------------------------------------------------------------+
void CMult::SetValuta(string &arr[])
{
   m_total_valut = ArrayCopy(m_V, arr);
}
void CMult::SetValuta(string val)
{
   m_total_valut=StringSplit(val,StringGetCharacter(" ", 0),m_V);
}


void CMult::SetSignal(string file, int period)
{
   m_generator.Set_1(file, m_V, period);
}


//+------------------------------------------------------------------+
void CMult::CloseAll(double sl, double tp)
{
   double profit=0;
   
   for(int i=0;i<m_total_valut;i++)
      if(PositionSelect(m_V[i]))
         profit += PositionGetDouble(POSITION_PROFIT);
   
   if(profit > tp || profit < -sl)
     for(int i=0;i<m_total_valut;i++)
         m_trade.PositionClose(m_V[i]);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CMult::SetMM(double lots, bool mm=false)
{
   m_lot = lots;
   m_on_MM = mm;
}


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const string symbol=NULL, const double volume=0.01,const int sl=100,const int tp=100,double price=0.0,const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double point = SymbolInfoDouble(symbol_name,SYMBOL_POINT);
   double price_SL=price-sl*point;
   double price_TP=price+tp*point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
}
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const string symbol=NULL, const double volume=0.01,const int sl=100,const int tp=100,double price=0.0,const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double point = SymbolInfoDouble(symbol_name,SYMBOL_POINT);
   double price_SL=price+sl*point;
   double price_TP=price-tp*point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
