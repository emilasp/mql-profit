//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


#include <Emilasp\Quants\Base\AQuantBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TimeQuant : public AQuantBase
{
private:

protected:

public:
                     TimeQuant(int _collectCurrentQuant);
                    ~TimeQuant();

   int               valueToQuant(datetime value);
   MqlDateTime       quantToValue(int quant);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TimeQuant::TimeQuant(int _collectCurrentQuant)
   : AQuantBase()
{
   collectCurrentQuant = _collectCurrentQuant;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TimeQuant::~TimeQuant()
{
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int TimeQuant::valueToQuant(datetime value)
{
   int res=0;

   MqlDateTime time;
   TimeToStruct(value, time);

   double minut = (time.day_of_week - 1) * 24 * 60 + time.hour * 60 + time.min;
   double q = MathFloor(minut / 15);
   res = (int)q;
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MqlDateTime TimeQuant::quantToValue(int quant)
{
   MqlDateTime res;
   double m = quant * 15;
   double w = MathFloor(m / 24 / 60) + 1;
   double h = MathFloor((m-(w-1)*24*60)/60);
   double min = MathFloor((m-(w-1)*24*60)-h*60);

   res.day_of_week = (int)w;
   res.hour = (int)h;
   res.min = (int)min;

   return res;
}
//+------------------------------------------------------------------+
