//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayInt.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AQuantBase : public CObject
{
private:

protected:
   CArrayInt         quants;
public:
                     AQuantBase();
                    ~AQuantBase();

   int               collectCurrentQuant;

   bool              isCurrentCollectQuant(int quant);

   void              setAllowQuants(int &_quants[]);
   void              setAllowQuants(string _quants);

   bool              allowTradeQuant(int quant);
   virtual int       calculateQuant();

};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantBase::AQuantBase()
{

}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantBase::~AQuantBase()
{
}

//+------------------------------------------------------------------+
//| Выставляется в методе OnInit()
//| Передаем сюда отобранные кванты для фильтрации сделок            |
//+------------------------------------------------------------------+
void AQuantBase::setAllowQuants(int &_quants[])
{
   for(int i=0; i<ArraySize(_quants); i++) {
      quants.Add(_quants[i]);
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::setAllowQuants(string _quants)
{
   string quantsStr[];
   ushort separator = StringGetCharacter(",", 0);
   int count = StringSplit(_quants, separator, quantsStr);

   int quants[];
   ArrayResize(quants, ArraySize(quantsStr));
   for(int i=0; i<ArraySize(quantsStr); i++) {
      quants[i] = StringToInteger(quantsStr[i]);
   }

   if (count > 0) {
      setAllowQuants(quants);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool AQuantBase::allowTradeQuant(int quant)
{
   for(int i=0; i<quants.Total(); i++) {
      int selQuant = quants.At(i);
      if (selQuant == quant) {
         return true;
      }
   }

   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int       AQuantBase::calculateQuant()
{
   return 0;
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|  Текущий рассчитанный квант равен кванту в тестере               |
//|  Разрешаем торговлю                                              |
//+------------------------------------------------------------------+
bool AQuantBase::isCurrentCollectQuant(int quant)
{
   if(quant==collectCurrentQuant) {
      return true;
   }
   return false;
}
//+------------------------------------------------------------------+
