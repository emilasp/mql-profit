//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Emilasp\Quants\Base\AQuantBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class QuantManager : public CObject
{
private:
   AQuantBase quant;
   
protected:
   
public:
                     QuantManager();
                    ~QuantManager();
                    
                    void collectQuants();
                    void allowTradeQuant();


};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
QuantManager::QuantManager()
{
   
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
QuantManager::~QuantManager()
{


}
