//+------------------------------------------------------------------+
//|                                                   DrawHelper.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
class CurrentTrade
{
public:
   ENUM_POSITION_TYPE positionType;
   double            enterPrice;
   double            exitPrice;
   datetime          enterDate;
   datetime          exitDate;
   double            tp;
   double            sl;
   int               tpPt;
   int               slPt;

   double            result;
   bool              isActive;

   void              CurrentTrade()
   {
      result = 0;
      isActive = false;
   }

   void              finish(double _exitPrice)
   {
      exitPrice = _exitPrice;
      if (positionType == POSITION_TYPE_BUY) {
         result = exitPrice - enterPrice;
      } else {
         result = enterPrice - exitPrice;
      }
      isActive = false;
   }

   void              initTrade(ENUM_POSITION_TYPE _positionType, int _tpPt, int _slPt)
   {
      isActive = true;
      double price = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
      result = 0;
      positionType = _positionType;
      if (positionType == POSITION_TYPE_BUY) {
         tpPt = _tpPt;
         slPt = _slPt * -1;
      } else {
         price = SymbolInfoDouble(_Symbol, SYMBOL_BID);
         tpPt = _tpPt * -1;
         slPt = _slPt;
      }

      enterPrice = price;
      tp = enterPrice + tpPt * _Point;
      sl = enterPrice + slPt * _Point;
   }
};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TradesStats
{
public:
   int               allCount;
   int               profitCount;
   int               lossCount;
   double            profitSum;
   double            lossSum;

   void              TradesStat()
   {
      allCount = 0;
      profitCount = 0;
      lossCount = 0;
      profitSum = 0;
      lossSum = 0;
   }

   void              addFinishedTrade(CurrentTrade &trade)
   {
      allCount++;
      if(trade.result > 0) {
         profitCount++;
         profitSum += trade.result;
      } else {
         lossCount++;
         lossSum += trade.result;
      }
   }
};
//+------------------------------------------------------------------+
