//+------------------------------------------------------------------+
//|                                             MartingaleStrategy.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>
#include <Emilasp\TradePanel\TradePanelStructs.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TradePanel
{
private:
   double            risk;
   double            volume;
   int               tpPt;
   int               slPt;
   double            volumeMin;

   CoreTradeHadge    *trade;
   CurrentTrade      *currentTrade;
   TradesStats       *stats;
   MqlTick           lastTick;

   bool isCalcVolume;
   bool isCalcRisk;
   
   void              finishedTrade(double price);
   void              setVolume(ENUM_POSITION_TYPE positionType);

public:
                     TradePanel(CoreTradeHadge *_trade, int _tpPt, int _slPt, double _volumeMin, double _volume, double _risk);

   void              checkButtonsStates();
   void              onInit();
   void              onTick();
   void              onChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam);
   void              onDeinit();

};


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TradePanel::TradePanel(CoreTradeHadge *_trade, int _tpPt, int _slPt, double _volumeMin, double _volume, double _risk)
{
   trade = _trade;
   currentTrade= new CurrentTrade;
   stats = new TradesStats;
   tpPt = _tpPt;
   slPt = _slPt;
   volumeMin=_volumeMin;
   volume=_volume;
   risk=_risk;
   
   isCalcVolume = volume == 0;
   isCalcRisk = risk == 0;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::onTick()
{
   SymbolInfoTick(_Symbol, lastTick);

   if(MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE)) {
      checkButtonsStates();
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::onChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   if(!MQLInfoInteger(MQL_TESTER) && !MQLInfoInteger(MQL_VISUAL_MODE)) {
      checkButtonsStates();
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::checkButtonsStates()
{
   if(ObjectGetInteger(0, "ButtonBuy", OBJPROP_STATE)) {
      setVolume(POSITION_TYPE_BUY);
      trade.Buy(volume, 40, 20, NULL);
      currentTrade.initTrade(POSITION_TYPE_BUY, tpPt, slPt);
      ObjectSetInteger(0, "ButtonBuy", OBJPROP_STATE, false);
      TesterHelper::pause(true);
   }
   if(ObjectGetInteger(0, "ButtonSell", OBJPROP_STATE)) {
      setVolume(POSITION_TYPE_BUY);
      trade.Sell(volume, 40, 20, NULL);
      currentTrade.initTrade(POSITION_TYPE_SELL, tpPt, slPt);
      ObjectSetInteger(0, "ButtonSell", OBJPROP_STATE, false);
      TesterHelper::pause(true);
   }

   if (currentTrade.isActive) {
      if(currentTrade.positionType == POSITION_TYPE_BUY) {
         double price = trade.getTickPriceBidOrAskForTrade(POSITION_TYPE_BUY, lastTick);
         if(currentTrade.tp <= price || currentTrade.sl >= price) {
            finishedTrade(price);
         }
      } else {
         double price = trade.getTickPriceBidOrAskForTrade(POSITION_TYPE_SELL, lastTick);
         if(currentTrade.tp >= price || currentTrade.sl <= price) {
            finishedTrade(price);
         }
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::onInit()
{
   ChartSetInteger(0, CHART_EVENT_MOUSE_MOVE, 1);//--- включение сообщений о перемещении мыши по окну чарта
   ChartSetInteger(0, CHART_CONTEXT_MENU, 0);//--- отключаем контекстное меню чарта (по правой кнопке)
   ChartSetInteger(0, CHART_CROSSHAIR_TOOL, 0);//--- отключаем перекрестие (по средней кнопке)
   ChartRedraw();//--- принудительное обновление свойств графика гарантирует готовность к обработке событий

   DrawFormHelper::RectLabelCreate("Panel", CORNER_LEFT_UPPER, 60, 120, 150, 180, C'236,233,216');
   DrawFormHelper::ButtonCreate("ButtonBuy", "Buy", CORNER_LEFT_UPPER, 70, 180, 50, 18, "Tahoma", 10, clrBlack, clrGreen, clrGray);
   DrawFormHelper::ButtonCreate("ButtonSell", "Sell", CORNER_LEFT_UPPER, 140, 180, 50, 18, "Tahoma", 10, clrBlack, clrRed, clrOrangeRed);
   DrawFormHelper::EditCreate("EditRisk", DoubleToString(risk), 70, 200, 50, 18, "Tahoma", 10, ALIGN_CENTER);
   DrawFormHelper::EditCreate("EditVolume", DoubleToString(volume), 140, 200, 50, 18, "Tahoma", 10, ALIGN_CENTER, true);
   DrawHelper::drawLabel("profitCount",IntegerToString(stats.profitCount),70,220);
   DrawHelper::drawLabel("profitCount",IntegerToString(stats.allCount),140,220);
   DrawHelper::drawLabel("profitCount",IntegerToString(stats.lossCount),200,220);
   
   DrawHelper::drawRectangleLabel(0, "settings", 0, 0, 0, 300, 100, clrBlack, BORDER_FLAT, CORNER_LEFT_UPPER, 255, STYLE_SOLID, 1, false, false);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::onDeinit()
{
   ObjectDelete(0, "Panel");
   ObjectDelete(0, "ButtonBuy");
   ObjectDelete(0, "ButtonSell");
   ObjectDelete(0, "EditRisk");
   ObjectDelete(0, "EditVolume");
   ObjectDelete(0, "settings");
   ObjectDelete(0, "Counts");
   ObjectDelete(0, "Prices");
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::setVolume(ENUM_POSITION_TYPE positionType)
{
   if (isCalcVolume) {
      double price = trade.getTickPriceBidOrAskForTrade(positionType, lastTick);
      double slPrice = price + (positionType == POSITION_TYPE_BUY ? -1 : 1) * slPt * _Point;

      double balance = AccountInfoDouble(ACCOUNT_BALANCE);
      double riskSum = balance * risk;
      double volumeLocal = riskSum/(price - slPrice);
      if (volumeLocal < volumeMin) {
         volumeLocal = volumeMin;
      }
      volume = NormalizeDouble(volumeLocal, 2);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TradePanel::finishedTrade(double price)
{
   currentTrade.finish(price);
   stats.addFinishedTrade(currentTrade);
   TesterHelper::pause();

   DrawHelper::drawLabel("Counts", "All: " + IntegerToString(stats.allCount) + ", profit: " + IntegerToString(stats.profitCount) + ", loss: " + IntegerToString(stats.lossCount), 10, 65);
   DrawHelper::drawLabel("Prices", "profit$: " + DoubleToString(stats.profitSum) + ", loss$: " + DoubleToString(stats.lossSum), 10, 80);
}
//+------------------------------------------------------------------+
