//+------------------------------------------------------------------+
//|                                                   TradePanel.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#property description "Панель для полуавтоматической торговли."

#include <Controls\Dialog.mqh>
#include <Controls\Button.mqh>
#include <Controls\CheckBox.mqh>
#include <Controls\ComboBox.mqh>
#include <Controls\Edit.mqh>
#include <Controls\Label.mqh>

#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Trade\AccountInfo.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
//--- indents and gaps
#define INDENT_LEFT                         (11)      // indent from left (with allowance for border width)
#define INDENT_TOP                          (11)      // indent from top (with allowance for border width)
#define CONTROLS_GAP_X                      (5)       // gap by X coordinate
//--- for buttons
#define BUTTON_TRADE_WIDTH                  (100)     // size by X coordinate
#define BUTTON_TRADE_HEIGHT                 (60)      // size by Y coordinate
#define BUTTON_HEIGHT                       (20)      // size by Y coordinate
//---
//+------------------------------------------------------------------+
//| Class CAppWindowTwoButtons                                       |
//| Usage: main dialog of the Controls application                   |
//+------------------------------------------------------------------+
class ETradePanel : public CAppDialog
  {
private:
   CButton           m_button_buy;                       // the button object
   CButton           m_button_sell;                      // the button object
   CButton           m_button_close_all;
   // the button object
   CButton           m_trail_enable;
   CButton           m_reorder_enable;
   CButton           m_show_enable;

   CEdit             m_edit_lot;
   CEdit             m_edit_sl;
   CEdit             m_edit_tp;
   CEdit             m_edit_trail;


   CLabel            m_label_ask;
   CLabel            m_label_bid;

   CLabel            m_label_balance;
   CLabel            m_label_balance_text;
   CLabel            m_label_equity; //Equity
   CLabel            m_label_equity_label; //Equity
   
   double            lot;
   bool              isTRailingStop;

private:
   //--- Create Label object
   bool              CreateLabel(const long chart,const int subwindow,CLabel &object, string name,const string text,const uint x,const uint y, ENUM_ANCHOR_POINT anchorAlign);
   bool              CreateButton(const long chart,const int subwindow,CButton &object, string name,const string text,const uint x,const uint y,const uint x_size,const uint y_size);
   bool              CreateEdit(const long chart,const int subwindow,CEdit &object, string name,const string text,const uint x,const uint y,const uint x_size,const uint y_size);

protected:
   CPositionInfo     m_position;                      // trade position object
   CTrade            m_trade;                         // trading object
   CAccountInfo      m_account;                       // account info wrapper

public:
                     ETradePanel(void);
                    ~ETradePanel(void);
   //--- create
   virtual bool      Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2);
   //--- chart event handler
   virtual bool      OnEvent(const int id,const long &lparam, const double &dparam, const string &sparam);

protected:
   virtual void      Minimize(void);
   //--- create dependent controls

   //--- handlers of the dependent controls events
   void              OnClickButtonBuy(void);
   void              OnClickButtonSell(void);
   void              OnClickButtonCloseAll(void);
   void              OnChangeEditLot(void);

  };


//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
ETradePanel::ETradePanel(void)
  {
   lot=0.01;
   isTRailingStop=false;
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
ETradePanel::~ETradePanel(void)
  {
  }
//+------------------------------------------------------------------+
//| Create                                                           |
//+------------------------------------------------------------------+
bool ETradePanel::Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2)
  {
   if(!CAppDialog::Create(chart,name,subwin,x1,y1,x2,y2))
      return(false);
//--- create dependent controls
   CreateLabel(chart,subwin,m_label_ask, "test ASK","ask",55,5,0);
   CreateLabel(chart,subwin,m_label_bid, "BID","bid",55,5,0);
   CreateEdit(chart,subwin,m_edit_lot, "EditLot","0.1",33,35,60,20);
   CreateButton(chart,subwin,m_button_buy, "ButtonBuy","BUY",33,65,60,20);
   CreateButton(chart,subwin,m_button_sell, "ButtonSell","SELL",33,95,60,20);
   CreateButton(chart,subwin,m_button_close_all,"ButtonClaseAll","CLOSE",33,125,60,20);
//--- succeed
   return(true);
  }


//+------------------------------------------------------------------+
//| Event handler                                                    |
//+------------------------------------------------------------------+
void ETradePanel::OnClickButtonBuy(void)
  {
   Print("Buy click");
      //m_trade.Buy(lot);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ETradePanel::OnClickButtonSell(void)
  {
   Print("Sell click");
      //m_trade.Sell(lot);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ETradePanel::OnClickButtonCloseAll(void)
  {
   if(m_account.TradeMode()==ACCOUNT_TRADE_MODE_DEMO)
      for(int i=PositionsTotal()-1; i>=0; i--) // returns the number of current positions
         if(m_position.SelectByIndex(i)) // selects the position by index for further access to its properties
            if(m_position.Symbol()==Symbol())
               m_trade.PositionClose(m_position.Ticket()); // close a position by the specified symbol
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ETradePanel::OnChangeEditLot(void)
  {
   string test = (double)ObjectGetString(0, "EditLot",OBJPROP_TEXT);
   Print("OnChange edit Lot: " + m_edit_lot.Text() + ", " + test);
//lot = m_edit_lot.Text();

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ETradePanel::Minimize(void)
  {
//--- переменная для получения панели быстрой торговли
   long one_click_visible=-1;  // 0 - панели быстрой торговли нет
   if(!ChartGetInteger(m_chart_id,CHART_SHOW_ONE_CLICK,0,one_click_visible))
     {
      //--- выведем сообщение об ошибке в журнал "Эксперты"
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- минимальный отступ для свернутой панели приложения
   int min_y_indent=28;
   if(one_click_visible)
      min_y_indent=100;  // отступ, если быстрая торговля показана на графике
//--- получим текущий отступ для свернутой панели приложения
   int current_y_top=m_min_rect.top;
   int current_y_bottom=m_min_rect.bottom;
   int height=current_y_bottom-current_y_top;
//--- вычислим новый минимальный отступ от верха для свернутой панели приложения
   if(m_min_rect.top!=min_y_indent)
     {
      m_min_rect.top=min_y_indent;
      //--- сместим также нижнюю границу свернутой иконки
      m_min_rect.bottom=m_min_rect.top+height;
     }
//--- теперь можно вызвать метод базового класса
   CAppDialog::Minimize();
  }



//+------------------------------------------------------------------+
bool ETradePanel::CreateLabel(const long chart,const int subwindow,CLabel &object, string name,const string text,const uint x,const uint y,ENUM_ANCHOR_POINT anchorAlign)
  {
   if(!object.Create(chart,name,subwindow,x,y,0,0))
      return false;
   if(!object.Text(text))
      return false;

   ObjectSetInteger(chart,object.Name(),OBJPROP_ANCHOR, anchorAlign);

   if(!Add(object))
      return false;
   return true;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ETradePanel::CreateButton(const long chart,const int subwindow,CButton &object, string name,const string text,const uint x,const uint y,const uint x_size,const uint y_size)
  {
   if(!object.Create(chart,name,subwindow,x,y,x+x_size,y+y_size))
      return false;
   if(!object.Text(text))
      return false;
//--- set button flag to unlock
   object.Locking(false);

   if(!object.Pressed(false))
      return false;
   if(!Add(object))
      return false;
   return true;
  }


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ETradePanel::CreateEdit(const long chart,const int subwindow,CEdit &object, string name,const string text,const uint x,const uint y,const uint x_size,const uint y_size)
  {
   if(!object.Create(chart,name,subwindow,x,y,x+x_size,y+y_size))
      return false;
   if(!object.Text(text))
      return false;
   if(!object.TextAlign(ALIGN_CENTER))
      return false;
   if(!object.ReadOnly(false))
      return false;
   if(!Add(object))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Event Handling                                                   |
//+------------------------------------------------------------------+
EVENT_MAP_BEGIN(ETradePanel)
   ON_EVENT(ON_CLICK,m_button_buy,OnClickButtonBuy)
   ON_EVENT(ON_CLICK,m_button_sell,OnClickButtonSell)
   ON_EVENT(ON_CLICK,m_button_close_all,OnClickButtonCloseAll)
   ON_EVENT(ON_END_EDIT,m_edit_lot,OnChangeEditLot)
EVENT_MAP_END(CAppDialog)