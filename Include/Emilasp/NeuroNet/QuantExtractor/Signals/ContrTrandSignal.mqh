//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantSignalBase.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\structs\Info.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ContrTrandSignal : public AQuantSignalBase
{
private:
protected:
   
public:
   bool              isSignal();
                     ContrTrandSignal(ENUM_POSITION_TYPE _typePosition, Info &_info);
                    ~ContrTrandSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ContrTrandSignal::ContrTrandSignal(ENUM_POSITION_TYPE _typePosition, Info &_info)
   : AQuantSignalBase()
{
   typePosition=_typePosition;
   info = _info;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ContrTrandSignal::isSignal()
{
   double H[];
   ArraySetAsSeries(H, true);
   CopyHigh(_Symbol, PERIOD_CURRENT, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);
   CopyLow(_Symbol, PERIOD_CURRENT, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
   if(typePosition == POSITION_TYPE_BUY && L[1] > last_tick.bid)
      return true;

   if(typePosition == POSITION_TYPE_SELL && H[1] < last_tick.bid)
      return true;

   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ContrTrandSignal::~ContrTrandSignal()
{
}
