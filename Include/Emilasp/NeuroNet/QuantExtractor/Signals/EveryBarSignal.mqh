//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantSignalBase.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\TradeQuants.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\structs\Info.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class EveryBarSignal : public AQuantSignalBase
{
private:
   TradeQuants *trader;
   
protected:
   
public:
   bool              isSignal();
                     EveryBarSignal(ENUM_POSITION_TYPE _typePosition, TradeQuants &_trader, Info &_info);
                    ~EveryBarSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EveryBarSignal::EveryBarSignal(ENUM_POSITION_TYPE _typePosition, TradeQuants &_trader, Info &_info)
   : AQuantSignalBase()
{
   typePosition=_typePosition;
   trader = &_trader;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool EveryBarSignal::isSignal()
{
   if (trader.trade.isNewBar(2))
      return true;
   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
EveryBarSignal::~EveryBarSignal()
{
}
