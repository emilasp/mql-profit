//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class LaguerreQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     LaguerreQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int rsiPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
LaguerreQuantCalc::LaguerreQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int rsiPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "LaguerreRSI";

   config.AddInt("rsiPeriod", rsiPeriod);
   config.AddInt("priceType", priceType);
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void LaguerreQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iCustom(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), "Vendors\\Laguerre_RSI_ng", config.GetAsInt("rsiPeriod"), config.GetAsInt("priceType"));
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    LaguerreQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   if(typePosition == POSITION_TYPE_BUY)
      calculateAndSetQuantForPeriod(index, buffers, 0.15, 0.30, 0.15, true);
   else
      calculateAndSetQuantForPeriod(index, buffers, 0.85, 0.30, 0.15, true);
}
//+------------------------------------------------------------------+
