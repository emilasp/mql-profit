//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class MomentumQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     MomentumQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MomentumQuantCalc::MomentumQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "Momentum";

   config.AddInt("maPeriod", maPeriod);
   config.AddInt("priceType", priceType);
   
   config.Add("quantCalc", "normal");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void MomentumQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iMomentum(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("maPeriod"), config.GetAsInt("priceType"));
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    MomentumQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   if(typePosition == POSITION_TYPE_BUY)
      calculateAndSetQuantForPeriod(index, buffers, 100.0, 200, 200, true);
   else
      calculateAndSetQuantForPeriod(index, buffers, 100.0, 200, 200, true);
}
//+------------------------------------------------------------------+
