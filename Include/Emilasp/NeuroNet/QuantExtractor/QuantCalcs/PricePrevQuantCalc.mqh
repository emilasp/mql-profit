//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayObj.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class PricePrevQuantCalc: public AQuantCalcBase
{
private:
   CArrayObj         *bars;
   
protected:
   void            calculateQuantLogic(int index);

public:
                     PricePrevQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int prebBarsCount, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
PricePrevQuantCalc::PricePrevQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int prebBarsCount, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "PricePrev";
   
   config.AddInt("prebBarsCount", prebBarsCount);
   config.AddBool("quantDrawLevels", showLevels);

   bars = new CArrayObj();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PricePrevQuantCalc::calculateQuantLogic(int index)
{
   int prebBarsCount         = config.GetAsInt("prebBarsCount");
   ENUM_TIMEFRAMES timeFrame = timeFrames.At(index);
   
   MqlRates rates[];
   ArraySetAsSeries(rates,true);
   
   if(CopyRates(NULL,timeFrame,0,prebBarsCount,rates) <= 0)
      Print("Ошибка копирования ценовых данных ",GetLastError());

   bars.Clear();
   
   for(int i=0;i<prebBarsCount;i++)
     {
         Bar* bar = new Bar(rates[i]);
         
         if (i > 0) {
            Bar* firstBar = new Bar(rates[0]);
            bar.normalizeByBar(firstBar);
         }
         
         bars.Add(bar);
     }
     
     string raws = "";
     for(int i=1;i<prebBarsCount;i++)
     {
         Bar* bar = bars.At(i);
         
         raws += bar.close;
         if (i < prebBarsCount - 1) {
            raws += ",";
         }
     }
     
     resultQuants.Add(raws);
     resultRaws.Add(raws);
}