//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class CciQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     CciQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CciQuantCalc::CciQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "CCI";

   config.AddInt("maPeriod", maPeriod);
   config.AddInt("priceType", priceType);
   
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

void CciQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iCCI(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("maPeriod"), config.GetAsInt("priceType"));
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    CciQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   if(typePosition == POSITION_TYPE_SELL)
      calculateAndSetQuantForPeriod(index, buffers, 100.0, 200.0, 200.0, true);
   else
      calculateAndSetQuantForPeriod(index, buffers, -100.0, 200.0, 200.0, true);
}
//+------------------------------------------------------------------+
