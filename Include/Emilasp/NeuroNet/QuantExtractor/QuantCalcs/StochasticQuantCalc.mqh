//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class StochasticQuantCalc: public AQuantCalcBase
{
private:
   
protected:
   void            calculateQuantLogic(int index);

public:
   void              init();
                     StochasticQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int period, int kPeriod, int dPeriod, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StochasticQuantCalc::StochasticQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int period, int kPeriod, int dPeriod, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "Stochastic";
   
   config.AddInt("period", period);
   config.AddInt("kPeriod", kPeriod);
   config.AddInt("dPeriod", dPeriod);
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);
   
   config.AddBool("quantDrawLevels", showLevels);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void StochasticQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iStochastic(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("period"), config.GetAsInt("kPeriod"), config.GetAsInt("dPeriod"), MODE_SMA, STO_LOWHIGH);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    StochasticQuantCalc::calculateQuantLogic(int index)
{
   int buffers[2] = {0,1};
   if(typePosition == POSITION_TYPE_BUY)
      calculateAndSetQuantForPeriod(index, buffers, 20.0, 30.0, 20.0, true);
   else
      calculateAndSetQuantForPeriod(index, buffers, 80.0, 20.0, 30.0, true);
}