//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class BbQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     BbQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int calcPeriod, int shift, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BbQuantCalc::BbQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int calcPeriod, int shift, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "Bbands";

   config.AddInt("calcPeriod", calcPeriod);
   config.AddInt("shift", shift);
   config.AddInt("priceType", priceType);
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void BbQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iBands(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("calcPeriod"), config.GetAsInt("shift"), 2, config.GetAsInt("priceType"));
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    BbQuantCalc::calculateQuantLogic(int index)
{
   double Buffer[];
   ArraySetAsSeries(Buffer, true);
   int res = CopyBuffer(handles[index], 0, 0, 2, Buffer);

   if(res<0)
      info.addErrorCalculate(name + ": не удалось загрузить сигнал для ТФ " + IntegerToString(timeFrames[index]));

   MqlTick price;
   SymbolInfoTick(Symbol(), price);

   string direct = getDirect(Buffer[0], Buffer[1]);
   string directByPrice = getDirect(Buffer[0], price.ask);
   
   double distance = 20 * 0.00001 * config.GetAsDouble("incPt");
   string quant = calcQuantNumber(Buffer[0], price.ask, distance, distance, false);
   
   string raws  = DoubleToString(Buffer[0]) + "," + DoubleToString(Buffer[1]) + ";";

   

   if(index == timeFrames.Total() - 1)
     drawQuantLevels(0, quant, price.ask);

   resultQuants.Add(directByPrice + direct + quantNumberPad(IntegerToString(quant)));
   resultRaws.Add(raws);
}
//+------------------------------------------------------------------+
