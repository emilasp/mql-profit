//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class RsiQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     RsiQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RsiQuantCalc::RsiQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int maPeriod, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "RSI";

   config.AddInt("maPeriod", maPeriod);
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void RsiQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i] = iRSI(_Symbol, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("maPeriod"), PRICE_CLOSE);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void RsiQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   if(typePosition == POSITION_TYPE_BUY)
      calculateAndSetQuantForPeriod(index, buffers, 30, 30, 30, true);
   else
      calculateAndSetQuantForPeriod(index, buffers, 70, 30, 30, true);
}
//+------------------------------------------------------------------+
