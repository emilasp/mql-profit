//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class RocQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     RocQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int period, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RocQuantCalc::RocQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int period, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "LaguerreRSI";

   config.AddInt("period", period);
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

void RocQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iCustom(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), "Vendors\\roc", config.GetAsInt("period"));
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    RocQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   calculateAndSetQuantForPeriod(index, buffers, 0, 0.2, 0.2, true);
}
//+------------------------------------------------------------------+
