//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class ImpulseCalc: public AQuantCalcBase
{
private:
   int barsPattern;
   int barsAvgCandle;
   int powerImpulse;
   bool onlyFillDown;
   
protected:
   void              init();
   void              calculateQuantLogic(int index);

public:
                     ImpulseCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int _barsPattern, int _barsAvgCandle, int _powerImpulse, bool _onlyFillDown, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ImpulseCalc::ImpulseCalcImpulseCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int _barsPattern, int _barsAvgCandle, int _powerImpulse, bool _onlyFillDown, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "Impulse";
   
   barsPattern = _barsPattern;
   barsAvgCandle = _barsAvgCandle
   powerImpulse = _powerImpulse
   onlyFillDown = _onlyFillDown
   
   config.AddBool("quantDrawLevels", showLevels);

   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iMA(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), maPeriod, 0, maMethod, priceType);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    ImpulseCalc::calculateQuantLogic(int index)
{
   double Buffer[];
   ArraySetAsSeries(Buffer, true);
   int res = CopyBuffer(handles[index], 0, 0, 2, Buffer);

   if(res<0)
      info.addErrorCalculate(name + ": не удалось загрузить сигнал для ТФ " + IntegerToString(timeFrames[index]));

   MqlTick price;
   SymbolInfoTick(Symbol(), price);

   string direct = getDirect(Buffer[0], Buffer[1]);
   string directByPrice = getDirect(Buffer[0], price.ask);
   
   double distance = 20 * 0.00001 * config.GetAsDouble("incPt");
   string quant = calcQuantNumber(Buffer[0], price.ask, distance, distance, false);
   
   string raws  = DoubleToString(Buffer[0]) + "," + DoubleToString(Buffer[1]) + ";";

   

   if(index == timeFrames.Total() - 1)
     drawQuantLevels(0, quant, price.ask);

   resultQuants.Add(directByPrice + direct + quantNumberPad(IntegerToString(quant)));
   resultRaws.Add(raws);
}
//+------------------------------------------------------------------+
