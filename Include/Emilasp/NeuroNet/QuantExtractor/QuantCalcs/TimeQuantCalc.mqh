//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>
/**
*
**/
class TimeQuantCalc: public AQuantCalcBase
{
private:
protected:
   
   void            calculateQuantLogic(int index);

public:
                     TimeQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TimeQuantCalc::TimeQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info)
   : AQuantCalcBase()
{
   info=_info;

   name          = "time";
   typePosition  =_typePosition;
   
   timeFrames.Clear();
   timeFrames.Add(PERIOD_M1);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    TimeQuantCalc::calculateQuantLogic(int index)
{
   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);

   MqlDateTime time;
   TimeToStruct(lastTick.time, time);

   double minut = (time.day_of_week - 1) * 24 * 60 + time.hour * 60 + time.min;
   string quant = IntegerToString((int)MathFloor(minut / 15));
   
   resultQuants.Add(IntegerToString(quant));
   resultRaws.Add(IntegerToString(minut));
}
