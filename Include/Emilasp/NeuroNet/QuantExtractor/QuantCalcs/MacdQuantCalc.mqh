//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class MacdQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     MacdQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int fast, int slow, int signal, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MacdQuantCalc::MacdQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int fast, int slow, int signal, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "MACD";

   config.AddInt("fast", fast);
   config.AddInt("slow", slow);
   config.AddInt("signal", signal);
   config.AddInt("price", priceType);
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

void MacdQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iMACD(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("fast"), config.GetAsInt("slow"), config.GetAsInt("signal"), config.GetAsInt("price"));
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    MacdQuantCalc::calculateQuantLogic(int index)
{
   int buffers[2] = {0, 1};

   calculateAndSetQuantForPeriod(index, buffers, 0, 0.0006, 0.0006, true);
}
//+------------------------------------------------------------------+
