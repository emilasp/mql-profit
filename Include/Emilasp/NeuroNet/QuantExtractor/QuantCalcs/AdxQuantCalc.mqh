//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class AdxQuantCalc: public AQuantCalcBase
{
private:

protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     AdxQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int adxPeriod, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AdxQuantCalc::AdxQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int adxPeriod, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "ADX";

   config.AddInt("adxPeriod", adxPeriod);

   config.Add("quantCalc", "normal");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);

   config.AddBool("quantDrawLevels", showLevels);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AdxQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iADX(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("adxPeriod"));
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    AdxQuantCalc::calculateQuantLogic(int index)
{
   int buffers[3] = {0, 1, 2};

   calculateAndSetQuantForPeriod(index, buffers, 0, 90, 0, true);
}
//+------------------------------------------------------------------+
