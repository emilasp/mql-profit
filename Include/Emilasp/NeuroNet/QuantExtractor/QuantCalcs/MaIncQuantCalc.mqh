//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class MaIncQuantCalc: public AQuantCalcBase
{
private:
   int incPt;
   
protected:
   void              calculateQuantLogic(int index);

public:
   void              init();
                     MaIncQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int _incPt, int maPeriod, ENUM_APPLIED_PRICE priceType, ENUM_MA_METHOD maMethod, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MaIncQuantCalc::MaIncQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int _incPt, int maPeriod, ENUM_APPLIED_PRICE priceType, ENUM_MA_METHOD maMethod, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "MaInc";
   
   incPt = _incPt;

   config.AddInt("maPeriod", maPeriod);
   config.AddInt("priceType", priceType);
   config.AddInt("maMethod", maMethod);
   config.AddInt("incPt", _incPt);
   
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);
   
   config.AddBool("quantDrawLevels", showLevels);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void MaIncQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i]=iMA(NULL, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("maPeriod"), 0, (ENUM_MA_METHOD)config.GetAsInt("maMethod"), config.GetAsInt("priceType"));
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    MaIncQuantCalc::calculateQuantLogic(int index)
{
   double Buffer[];
   ArraySetAsSeries(Buffer, true);
   int res = CopyBuffer(handles[index], 0, 0, 2, Buffer);

   if(res<0)
      info.addErrorCalculate(name + ": не удалось загрузить сигнал для ТФ " + IntegerToString(timeFrames[index]));

   MqlTick price;
   SymbolInfoTick(Symbol(), price);

   string direct = getDirect(Buffer[0], Buffer[1]);
   string directByPrice = getDirect(Buffer[0], price.ask);
   
   double distance = 20 * 0.00001 * config.GetAsDouble("incPt");
   string quant = calcQuantNumber(Buffer[0], price.ask, distance, distance, false);
   
   string raws  = DoubleToString(Buffer[0]) + "," + DoubleToString(Buffer[1]) + ";";

   

   if(index == timeFrames.Total() - 1)
     drawQuantLevels(0, quant, price.ask);

   resultQuants.Add(directByPrice + direct + quantNumberPad(IntegerToString(quant)));
   resultRaws.Add(raws);
}
//+------------------------------------------------------------------+
