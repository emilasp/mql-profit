//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

/**
*
**/
class OsMaQuantCalc: public AQuantCalcBase
{
private:
   
protected:
   void            calculateQuantLogic(int index);

public:
   void              init();
                     OsMaQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int fast, int slow, int signal, ENUM_APPLIED_PRICE priceType, bool showLevels = false);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
OsMaQuantCalc::OsMaQuantCalc(ENUM_POSITION_TYPE _typePosition, Info &_info, int quantPartLength, int quantCount, int fast, int slow, int signal, ENUM_APPLIED_PRICE priceType, bool showLevels = false)
   : AQuantCalcBase()
{
   info=_info;

   typePosition =_typePosition;
   name         = "OsMA";
   
   config.AddInt("fast", fast);
   config.AddInt("slow", slow);
   config.AddInt("signal", signal);
   config.AddInt("priceType", priceType);
   
   config.Add("quantCalc", "fibonacci");
   config.AddInt("quantPartLength", quantPartLength);
   config.AddInt("quantCount", quantCount);
   
   config.AddBool("quantDrawLevels", showLevels);
   
   string tt = config.Get("quantCalc");
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OsMaQuantCalc::init()
{
   for(int i=0; i < timeFrames.Total(); i++) {
      handles[i] = iOsMA(_Symbol, (ENUM_TIMEFRAMES)timeFrames.At(i), config.GetAsInt("fast"), config.GetAsInt("slow"), config.GetAsInt("signal"), config.GetAsInt("priceType"));
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OsMaQuantCalc::calculateQuantLogic(int index)
{
   int buffers[1] = {0};
   
   calculateAndSetQuantForPeriod(index, buffers, 0, 0.001, 0.001, true);
}