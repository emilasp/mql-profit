//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantBase.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\Signals\ContrTrandSignal.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\TimeQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\RsiQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\StochasticQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\CciQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\MomentumQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\MaIncQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\WprQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\MacdQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\AdxQuantCalc.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\QuantCalcs\OsMaQuantCalc.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ContrTrandQuant : public AQuantBase
{
private:
   int               handle;
protected:
   bool              calculate();
public:
                     ContrTrandQuant(ENUM_POSITION_TYPE _typePosition, int &_timeFrames[], bool _enableTrade, bool _enableCollect);
                    ~ContrTrandQuant();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ContrTrandQuant::ContrTrandQuant(ENUM_POSITION_TYPE _typePosition, int &_timeFrames[], bool _enableTrade, bool _enableCollect)
   : AQuantBase(20, 120, 20, 120, 20)
{
   magic = 123324;
   apiKey = "svr4356yhxdv";
   name = "ContrTrand";

   limitOpenQuants = 0;
   batch           = 100;
   typePosition = _typePosition;

   enableTrade = _enableTrade;
   enableCollect=_enableCollect;

   setTimeFrames(_timeFrames);

   trader = new TradeQuants(magic);
   requester = new ServerRequests(apiKey, (string)trader.trade.m_Account.Login(), trader.trade.m_symbol.Name(), typePosition, "auditory.sport-run.ru");

// Signal
   signal = new ContrTrandSignal(typePosition, info);


// Settings
   bool DEBUG = false;
   TesterHideIndicators(!DEBUG);

// Add Quants calculators
   addCalculator(new TimeQuantCalc(typePosition, info));
   addCalculator(new RsiQuantCalc(typePosition, info, 2, 10, 7, DEBUG));
   addCalculator(new StochasticQuantCalc(typePosition, info, 2, 14, 5, 3, 3, DEBUG));
   addCalculator(new CciQuantCalc(typePosition, info, 2, 14, 14, PRICE_OPEN, DEBUG));
   addCalculator(new CciQuantCalc(typePosition, info, 2, 14, 14, PRICE_CLOSE, DEBUG));
   addCalculator(new MomentumQuantCalc(typePosition, info, 2, 14, 14, PRICE_OPEN, DEBUG));
   addCalculator(new WprQuantCalc(typePosition, info, 2, 10, 14), false);
   addCalculator(new MacdQuantCalc(typePosition, info, 2, 22, 12, 26, 9, PRICE_OPEN, DEBUG));
   addCalculator(new AdxQuantCalc(typePosition, info, 2, 22, 14), DEBUG);
   addCalculator(new OsMaQuantCalc(typePosition, info, 2, 22, 12, 26, 9, PRICE_CLOSE, DEBUG));

   addCalculator(new MaIncQuantCalc(typePosition, info, 2, 30, 30, 144, PRICE_OPEN, MODE_SMA, DEBUG), true);
   addCalculator(new MaIncQuantCalc(typePosition, info, 2, 40, 3, 21, PRICE_OPEN, MODE_SMA, DEBUG), true);
   addCalculator(new MaIncQuantCalc(typePosition, info, 2, 40, 2, 7, PRICE_OPEN, MODE_SMA, DEBUG), true);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ContrTrandQuant::~ContrTrandQuant()
{
}
//+------------------------------------------------------------------+
