//+------------------------------------------------------------------+
//|                                                TimeQuantCalc.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>




/**
*
**/
class TradeQuants
{
private:


public:
   CoreTradeHadge    *trade;
                     TradeQuants(int magic);
                    ~TradeQuants();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TradeQuants::TradeQuants(int magic)
{
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_M1, magic);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
TradeQuants::~TradeQuants()
{
}
//+------------------------------------------------------------------+
