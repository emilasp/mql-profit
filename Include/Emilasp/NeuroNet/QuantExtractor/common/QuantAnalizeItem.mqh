//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Arrays\ArrayString.mqh>

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Level : public CObject
{
public:
   string             id;
   ENUM_POSITION_TYPE typePosition;

   bool              isFinished;
   datetime          finishedTime;

   int               TP;
   int               SL;

   double            price;
   double            MinPrice;
   double            MaxPrice;

   double            TpPrice;
   double            SlPrice;

   double            profit;
   double            slump;

                     Level::Level(ENUM_POSITION_TYPE _typePosition, string _id, double _price, int _TP, int _SL)
   {
      isFinished = false;

      typePosition = _typePosition;
      id=_id;

      price = _price;
      TP = _TP;
      SL = _SL;

      MaxPrice = price;
      MinPrice = price;

      calcLevels();

      DrawHelper::moveHorizontalLine("quant:level:tp:" + id, DoubleToString(TpPrice), clrGreen, STYLE_DASH);
      DrawHelper::moveHorizontalLine("quant:level:sl:" + id, DoubleToString(SlPrice), clrRed, STYLE_DASH);
   }

   void              calcLevels()
   {
      if(typePosition == POSITION_TYPE_BUY) {
         TpPrice = price + TP * _Point;
         SlPrice = price - SL * _Point;
      } else {
         TpPrice = price - TP * _Point;
         SlPrice = price + SL * _Point;
      }
   }

   void              update(double _price, datetime time)
   {
      if(_price > MaxPrice)
         MaxPrice = _price;
      if(_price < MinPrice)
         MinPrice = _price;

      // Finih Level
      if(typePosition == POSITION_TYPE_BUY && (_price > TpPrice || _price < SlPrice)) {
         isFinished = true;
         profit = _price - price;
         slump = price - MinPrice;
      }
      if(typePosition == POSITION_TYPE_SELL && (_price < TpPrice || _price > SlPrice)) {
         isFinished = true;
         profit = price - _price;
         slump = MaxPrice - price;
      }

      if(isFinished) {
         finishedTime = time;

         ObjectDelete(0, "quant:level:tp:" + id);
         ObjectDelete(0, "quant:level:sl:" + id);
      }
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ResultCalculator : public CObject
{
public:
   string name;
   string config;

   CArrayInt    timeframes;
   CArrayString numbers;
   CArrayString raws;
 
                     ResultCalculator::ResultCalculator(AQuantCalcBase &calculator)
   {
      calculator.calculateQuant();
      
      name=calculator.name;
      config=calculator.config.ToString();
   
      for(int i=0;i<calculator.resultQuants.Total();i++)
        {
            timeframes.Add(calculator.timeFrames.At(i));
            numbers.Add(calculator.resultQuants.At(i));
            raws.Add(calculator.resultRaws.At(i));
        }
   }
};


/**
*
**/
class QuantAnalizeItem : public CObject
{
private:
   void              createLevels();
   void              drawQuant(int clr, int iconNum, bool withLevels);
   
public:
   ENUM_POSITION_TYPE typePosition;

   bool              isFinished;

   datetime          time;
   double            price;
   double            balance;

   int               TpMin;
   int               TpMax;
   int               SlMin;
   int               SlMax;
   int               step;

   CArrayObj         levels;
   CArrayObj         results;

   string            getId();
   void              update(MqlTick &lastTick);
   void              addCalculators(CArrayObj &_calculators);

                     QuantAnalizeItem(ENUM_POSITION_TYPE _typePosition, MqlTick &lastTick, double _balance, int _TpMin, int _TpMax, int _SlMin, int _SlMax, int _step);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
QuantAnalizeItem::QuantAnalizeItem(ENUM_POSITION_TYPE _typePosition, MqlTick &lastTick, double _balance, int _TpMin, int _TpMax, int _SlMin, int _SlMax, int _step)
{
   typePosition = _typePosition;

   isFinished = false;

   time = lastTick.time;
   price = lastTick.bid;
   balance = _balance;

   TpMin = _TpMin;
   TpMax = _TpMax;
   SlMin = _SlMin;
   SlMax = _SlMax;
   step = _step;

   createLevels();

   drawQuant(clrCrimson, 232, true);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void QuantAnalizeItem::addCalculators(CArrayObj &_calculators)
{
   for(int i=0; i<_calculators.Total(); i++) {
      AQuantCalcBase *calculator = _calculators.At(i);
      results.Add(new ResultCalculator(calculator));
   }
}
//+------------------------------------------------------------------+
void QuantAnalizeItem::createLevels()
{
   int TP = TpMin;
   while(TP >= TpMin && TP <= TpMax) {
      int SL = SlMin;
      while(SL >= SlMin && SL <= SlMax) {
         string levelId = IntegerToString(levels.Total()) + TimeToString(time);
         levels.Add(new Level(typePosition, levelId, price, TP, SL));
         SL += step;
      }
      TP += step;
   }
}
//+------------------------------------------------------------------+
void QuantAnalizeItem::update(MqlTick &lastTick)
{
   for(int i=0; i<levels.Total(); i++) {
      Level *level = levels.At(i);
      if(!level.isFinished) {
         level.update(lastTick.bid, lastTick.time);
      }
   }

   bool hasOpenLevel = false;
   for(int i=0; i<levels.Total(); i++) {
      Level *level = levels.At(i);
      if (!level.isFinished) {
         hasOpenLevel = true;
         break;
      }
   }

   if(!hasOpenLevel) {
      isFinished = true;
      drawQuant(clrOrange, 233, false);
   }
}


//+------------------------------------------------------------------+
void QuantAnalizeItem::drawQuant(int clr, int iconNum, bool withLevels)
{
   string id = getId();
   DrawHelper::drawArrow(id + "signal", time, price, iconNum, clr);

   if(withLevels) {
      for(int i=0; i<levels.Total(); i++) {
         Level *level = levels.At(i);


      }
   }
}


//+------------------------------------------------------------------+
string QuantAnalizeItem::getId()
{
   return "quant:" + TimeToString(time);
}

//+------------------------------------------------------------------+
