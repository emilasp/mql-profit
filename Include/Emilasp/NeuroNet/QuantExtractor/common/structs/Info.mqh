//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Arrays\ArrayString.mqh>


struct Info {
   CArrayString      errors;
   CArrayString      errorsCalculate;

   int               quantsAll;
   int               quantsOpen;
   int               quantsFinished;
   int               quantsSend;

   string            additionalInfoString;

   void              Info()
   {
      quantsAll = 0;
      quantsOpen = 0;
      quantsFinished = 0;
      quantsSend = 0;
   }

   void              addErrorCalculate(string error)
   {
      errorsCalculate.Add(error);
      errors.Add(error);
   }

   bool              hasErrorsCalculate()
   {
      return errorsCalculate.Total() > 0;
   }

   string            getInfo()
   {
      return "All: " + IntegerToString(quantsAll) + ", Open: " + IntegerToString(quantsOpen) + ", Finish: " + IntegerToString(quantsFinished) + ", Sends: " + IntegerToString(quantsSend)
             + ", Errors: " + IntegerToString(errorsCalculate.Total()) + "/" + IntegerToString(errors.Total());
   }
};
//+------------------------------------------------------------------+
