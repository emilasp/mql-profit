//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Arrays\ArrayString.mqh>


struct PredictData {
   double probe;
   int tp;
   int sl;
   double volume;
};