//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\common\structs\Info.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AQuantSignalBase
{
private:
protected:
    ENUM_POSITION_TYPE typePosition;
    Info                 info;
    
public:
   virtual bool      isSignal() {return true;}
                     AQuantSignalBase();
                    ~AQuantSignalBase();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantSignalBase::AQuantSignalBase()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantSignalBase::~AQuantSignalBase()
{
}
