//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayInt.mqh>
#include <Arrays\ArrayString.mqh>

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\libraries\Dictionary.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\structs\Info.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AQuantCalcBase : public CObject
{
private:


protected:
   ENUM_POSITION_TYPE typePosition;
   int                chartWindowNum;

   int                handles[10];
   Info               info;

   void               drawQuantLevels(int chartId, string quantNumber, double zero);
   void               setQuantLevels(bool directToTop, int quantsCount, double start, double distance, bool asFibonacce = false);
   string             getQuantByLevels(double value, double startZero);

   virtual void       calculateQuantLogic(int index) {};
   string             calcQuantNumber(double value, double zeroInput, double distanceTopInput, double distanceBottomInput, bool asFibonacce = false);
   void               calculateAndSetQuantForPeriod(int index, int &buffers[], double zeroInput, double distanceTopInput, double distanceBottomInput, bool withDirect = false);
   string             getDirect(double value1, double value2);
   string             quantNumberPad(string quantNumber);

   double             levelsTop[];
   double             levelsBottom[];

public:
   string             name;

   DictionaryString   config;

   CArrayInt          timeFrames;
   CArrayString       resultQuants;
   CArrayString       resultRaws;
   
   virtual void       init() {};
   void              calculateQuant();
   void              setChartWindow(int windowNum);
   void              setTimeFrames(int &_timeFrames[]);

                     AQuantCalcBase();
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantCalcBase::AQuantCalcBase()
{
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantCalcBase::setChartWindow(int windowNum)
{
   chartWindowNum = windowNum;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantCalcBase::calculateQuant()
{
   resultQuants.Clear();
   resultRaws.Clear();

   for(int i=0; i<timeFrames.Total(); i++) {
      calculateQuantLogic(i);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string    AQuantCalcBase::quantNumberPad(string quantNumber)
{
   string prependZeros = "";
   for(int i=StringLen(quantNumber); i<config.GetAsInt("quantPartLength"); i++)
      prependZeros += "0";
   return prependZeros + quantNumber;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string    AQuantCalcBase::getDirect(double value1, double value2)
{
   string direct;
   if (value1 > value2)
      direct = "1";
   else
      direct = "2";
   return direct;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
string AQuantCalcBase::calcQuantNumber(double value, double zeroInput, double distanceTopInput, double distanceBottomInput, bool asFibonacce = false)
{
   int quantsCount = config.GetAsInt("quantCount");

   int quantsBothCount[2] = {};
   quantsBothCount[0] = quantsCount / 2;
   quantsBothCount[1] = quantsCount / 2;

   if(distanceTopInput != 0 && distanceBottomInput == 0) {
      quantsBothCount[0] = quantsCount;
      quantsBothCount[1] = 0;
   }
   if(distanceTopInput == 0 && distanceBottomInput != 0) {
      quantsBothCount[0] = 0;
      quantsBothCount[1] = quantsCount;
   }

   setQuantLevels(true, quantsBothCount[0], zeroInput, distanceTopInput, asFibonacce);
   setQuantLevels(false, quantsBothCount[1], zeroInput, distanceBottomInput, asFibonacce);

// Top
   int quant = getQuantByLevels(value, zeroInput);

   return IntegerToString(quant);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantCalcBase::drawQuantLevels(int chartId, string quantNumber, double zero)
{
   if (config.GetAsBool("quantDrawLevels")) {
      MqlTick price;
      SymbolInfoTick(Symbol(), price);

      for(int i=0; i<ArraySize(levelsTop); i++) {
         string name = "quant:" + name + ":top:" + ":" + i;
         DrawHelper::drawArrow(name, price.time, levelsTop[i], 159, clrGreen, 0, chartId);
      }
      for(int i=0; i<ArraySize(levelsBottom); i++) {
         string name = "quant:" + name + ":bottom:" + ":" + i;
         DrawHelper::drawArrow(name, price.time, levelsBottom[i], 159, clrRed, 0, chartId);
      }
      DrawHelper::drawArrow("quant:" + name + ":zero", price.time, zero, 159, clrOrange, 0, chartId);
      
      string quantFull = "";
      for(int i=0;i<resultQuants.Total();i++)
        {
         quantFull += resultQuants.At(i) + "_";
        }
      
      DrawHelper::drawLabel("quant:" + name + ":number", quantFull, 210, 30, 3, chartId);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string AQuantCalcBase::getQuantByLevels(double value, double startZero)
{
   int quantsTop = ArraySize(levelsTop);
   int quantsBottom = ArraySize(levelsBottom);

   int quant = 0;
   if(quantsTop > 0 && value >= startZero) {
      quant = quantsTop - 1;
      for(int i=0; i<quantsTop; i++)
         if(value <= levelsTop[i]) {
            quant = i;
            break;
         }
      quant += quantsBottom; // Возможно не нужно если буедт директ всегда
   }

   if(quantsBottom > 0 && value < startZero) {
      quant = 0;
      for(int i=0; i<quantsBottom; i++)
         if(value >= levelsBottom[i]) {
            quant = quantsBottom - i;
            break;
         }
   }
   return IntegerToString(quant);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    AQuantCalcBase::setQuantLevels(bool directToTop, int quantsCount, double start, double distance, bool asFibonacce = false)
{
   int levels[30] = {1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6 , 7, 7, 7, 8, 8, 8, 8, 8};

   double levelsCalc[] = {};
   ArrayResize(levelsCalc, quantsCount);

   if(quantsCount > 0) {
      // Get Distance
      int allNumbers = 0;
      if(asFibonacce)
         for(int i=0; i<quantsCount; i++) {
            allNumbers += levels[i];
         } else
         allNumbers = quantsCount;

      double delta = distance / allNumbers;

      // get Levels
      int sum=0;
      for(int i=0; i < quantsCount; i++) {
         if(asFibonacce)
            sum += levels[i];
         else
            sum += 1;

         if(directToTop)
            levelsCalc[i] = start + sum * delta;
         else
            levelsCalc[i] = start - sum * delta;
      }

      // Set Levels
      int levelsSize = ArraySize(levelsCalc);
      if(directToTop) {
         ArrayResize(levelsTop, levelsSize);
         for(int i=0; i<levelsSize; i++)
            levelsTop[i] = levelsCalc[i];
      } else {
         ArrayResize(levelsBottom, levelsSize);
         for(int i=0; i<levelsSize; i++)
            levelsBottom[i] = levelsCalc[i];
      }
   }
}

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void    AQuantCalcBase::calculateAndSetQuantForPeriod(int index, int &buffers[], double zeroInput, double distanceTopInput, double distanceBottomInput, bool withDirect = false)
{
   ENUM_TIMEFRAMES timeFrame = timeFrames.At(index);

   string quant = "";
   string raws = "";
   for(int i=0; i<ArraySize(buffers); i++) {
      double Buffer[];
      ArraySetAsSeries(Buffer, true);
      int res = CopyBuffer(handles[index], buffers[i], 0, 2, Buffer);

      if(res<0)
         info.addErrorCalculate(name + "(" + IntegerToString(i) + "): не удалось загрузить сигнал для ТФ " + IntegerToString(timeFrame));

      string type = config.Get("quantCalc");
      string quantPart;
      if(config.Get("quantCalc") == "normal")
         quantPart = calcQuantNumber(Buffer[0], zeroInput, distanceTopInput, distanceBottomInput);
      if(config.Get("quantCalc") == "fibonacci")
         quantPart = calcQuantNumber(Buffer[0], zeroInput, distanceTopInput, distanceBottomInput, true);

      string direct = "";
      if(withDirect)
         direct = getDirect(Buffer[0], Buffer[1]);

      quant += direct + quantNumberPad(quantPart);
      raws  += DoubleToString(Buffer[0]) + "," + DoubleToString(Buffer[1]) + ";";
   }

   resultQuants.Add(quant);
   resultRaws.Add(raws);
   
   if(index == timeFrames.Total() - 1)
     drawQuantLevels(chartWindowNum, quant, zeroInput);
}
//+------------------------------------------------------------------+
void  AQuantCalcBase::setTimeFrames(int &_timeFrames[]) {   
   timeFrames.AddArray(_timeFrames);
}