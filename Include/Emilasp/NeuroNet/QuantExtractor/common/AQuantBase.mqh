//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Arrays\ArrayString.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\helpers\CoreHelper.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

#include <Emilasp\NeuroNet\QuantExtractor\common\QuantAnalizeItem.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\structs\Info.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\structs\PredictData.mqh>

#include <Emilasp\NeuroNet\QuantExtractor\common\TradeQuants.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantSignalBase.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\ServerRequests.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AQuantBase : public CObject
{
private:
   void                 drawInfo();
   void                 addOpenQuants(MqlTick &lastTick);
   void                 updateOpenQuants(MqlTick &lastTick);
   PredictData          predictCurrentBar(MqlTick &lastTick);
   void                 predictAndTradeBar(MqlTick &lastTick);

protected:
   int                magic;
   string             name;
   string             apiKey;

   int                limitOpenQuants;
   int                batch;
   
   int                timeFrames[];
   
   AQuantSignalBase   *signal;
   CArrayObj         quantCalcs;

   CArrayObj         quantsOpen;
   CArrayObj         quantsFinish;

   ServerRequests    *requester;
   TradeQuants       *trader;
   
   Info                 info;

   int               TpMin;
   int               TpMax;
   int               SlMin;
   int               SlMax;
   int               step;

   ////// Methods


   virtual void      saveData(string type) {}; // формируем данные из признаков
   string            saveDataToFile(string type, string separate = ";"); // формируем данные из признаков

   void              tradeQuant();
   void              addCalculator(AQuantCalcBase *calculator, bool baseChart = false);

   void              reset();
   void              drawQuant(bool isSuccess);

public:
   ENUM_POSITION_TYPE typePosition;
   bool               enableCollect;
   bool               enableTrade;

                     AQuantBase(int _TpMin, int _TpMax, int _SlMin, int _SlMax, int _step);

   bool              onTick(); // вызывается на каждом сигнале
   void              setTimeFrames(int &_timeFrames[]);

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AQuantBase::AQuantBase(int _TpMin, int _TpMax, int _SlMin, int _SlMax, int _step)
{
   name  = "Не установлено";

   magic = 123324;
   apiKey = "svr4356yhxdv";

   enableTrade = false;

   reset();

   TpMin = _TpMin;
   TpMax = _TpMax;
   SlMin = _SlMin;
   SlMax = _SlMax;
   step = _step;

   limitOpenQuants = 0;
   batch           = 0;

   quantCalcs.FreeMode(false);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool AQuantBase::onTick()
{
   MqlTick lastTick;
   SymbolInfoTick(_Symbol, lastTick);


   datetime time = lastTick.time;
// Check signal - add quant
   if(signal.isSignal()) {
      if(trader.trade.isNewBar(6)) {
         if(enableCollect) {
            addOpenQuants(lastTick); // Запускаем коллект признаков
         }
// Trade
         if(enableTrade) {
            predictAndTradeBar(lastTick); // Запускаем торговлю
         }
      }
   }
// calculate open quants
   updateOpenQuants(lastTick);

// Batch processed
   if(quantsFinish.Total() >= batch) {
      // Отправляем квант на сервер
      //ServerResponse response = requester.quantAddBatch(quantsFinish, 0.01);

      CJAVal dataToSave = requester.getBatchDataJson(quantsFinish);
      //CoreHelper::saveDataToCsv("export.quants.txt", dataToSave.Serialize(), true);
      CoreHelper::saveDataToFileAsJsonArray("export.quants.txt", dataToSave, true);
      
      quantsFinish.Clear();

      // Grapth objects clear
      DrawHelper::deleteObjectsByName("quant:");
      info.quantsSend++;
   }

   drawInfo();

   reset();

   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::addCalculator(AQuantCalcBase *calculator, bool baseChart = false)
{
   calculator.setTimeFrames(timeFrames);
   calculator.init();
   
   quantCalcs.Add(calculator);

   if (baseChart)
      calculator.setChartWindow(0);
   else
      calculator.setChartWindow(quantCalcs.Total());
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::addOpenQuants(MqlTick &lastTick)
{
   int count = quantsOpen.Total();

   double balance = trader.trade.m_Account.Balance();

   if(limitOpenQuants == 0 || count < limitOpenQuants) {
      QuantAnalizeItem *newAnalizeItem = new QuantAnalizeItem(typePosition, lastTick, balance, TpMin, TpMax, SlMin, SlMax, step);

      newAnalizeItem.addCalculators(quantCalcs);

      quantsOpen.Add(newAnalizeItem);
      info.quantsAll++;
      info.quantsOpen++;
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::updateOpenQuants(MqlTick &lastTick)
{
   for(int i=0; i<quantsOpen.Total(); i++) {
      QuantAnalizeItem * quantAnalize = quantsOpen.At(i);

      quantAnalize.update(lastTick);

      if(quantAnalize.isFinished) {
         quantsFinish.Add(quantsOpen.Detach(i));

         info.quantsOpen--;
         info.quantsFinished++;
      }
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::drawInfo()
{
   DrawHelper::drawLabel("Info 1", info.getInfo());
//DrawHelper::drawLabel("Info 2", info.additionalInfoString, 10, 50);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::drawQuant(bool isSuccess)
{
   DrawHelper::moveHorizontalLine("TP0", 0, clrGray);
   DrawHelper::moveHorizontalLine("SL0", 0, clrGray);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string AQuantBase::saveDataToFile(string type, string separate = ";")
{
   if(info.hasErrorsCalculate()) {
      return "";
   }

   string fileName = "export.quant." + name + "." + type + ".csv";

//CoreHelper::saveDataToCsv(fileName, arrayToRow(features), true);

//info.savePatternCount++;

   return "";
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::reset()
{
   info.errorsCalculate.Clear();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AQuantBase::tradeQuant()
{

}

PredictData AQuantBase::predictCurrentBar(MqlTick &lastTick) {
   CArrayObj quants;
   double balance = trader.trade.m_Account.Balance();
   QuantAnalizeItem *newAnalizeItem = new QuantAnalizeItem(typePosition, lastTick, balance, TpMin, TpMax, SlMin, SlMax, step);
   newAnalizeItem.addCalculators(quantCalcs);
   
   quants.Add(newAnalizeItem);

   ServerResponse response = requester.quantPredict(quants);
   
   PredictData result;
   result.probe = response.data["probe"].ToDbl();
   result.volume = response.data["volume"].ToDbl();
   result.tp = response.data["tp"].ToInt();
   result.sl = response.data["sl"].ToInt();
   return result;
}

void AQuantBase::predictAndTradeBar(MqlTick &lastTick) {
   PredictData predictProbe = predictCurrentBar(lastTick);
   
   Print("Predict: ", predictProbe.probe, ", ", predictProbe.volume, ", ", predictProbe.tp,", ", predictProbe.sl);
   
   if(predictProbe.probe > 0.6)
     {
     if(typePosition == POSITION_TYPE_BUY)
       {
         trader.trade.Buy(predictProbe.volume, predictProbe.tp, predictProbe.sl);
       }
     if(typePosition == POSITION_TYPE_SELL)
       {
         trader.trade.Sell(predictProbe.volume, predictProbe.tp, predictProbe.sl);
       }
     }
}

void  AQuantBase::setTimeFrames(int &_timeFrames[]) {
int totalSize = ArraySize(_timeFrames);
   ArrayResize(timeFrames, totalSize);
   for(int i=0;i<totalSize;i++)
     {
      timeFrames[i] = _timeFrames[i];
     }
}