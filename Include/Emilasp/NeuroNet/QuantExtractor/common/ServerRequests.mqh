//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Emilasp\libraries\Request.mqh>
#include <Emilasp\libraries\json\JAson.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\QuantAnalizeItem.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\common\AQuantCalcBase.mqh>

#import "Dll2.dll"
string  Request(string serv, string page, string content);
#import

//Также для эстетики кода определим используемые имена констант из wininet.h.
#define OPEN_TYPE_PRECONFIG     0           // использовать конфигурацию по умолчанию
#define FLAG_KEEP_CONNECTION    0x00400000  // не разрывать соединение
#define FLAG_PRAGMA_NOCACHE     0x00000100  // не кешировать страницу
#define FLAG_RELOAD             0x80000000  // получать страницу с сервера при обращении к ней
#define SERVICE_HTTP            3           // требуемый протокол

string routeQuantPredict = "/robot/trade/quant/predict";
string routeQuantAdd     = "/robot/trade/quant/level/add";

struct ServerResponse {
   bool              isSuccess;
   string            message;
   string            route;
   CJAVal            data;

   void              ServerResponse()
   {
      isSuccess = false;
      message = "";
      route = "";
   }

   void              setRessponse(string _route, bool _isSuccess, string _message, CJAVal &_data)
   {
      route = _route;
      isSuccess=_isSuccess;
      message = _message;
      data = _data;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ServerRequests
{
private:
   string            apiKey;
   string            account;
   string            symbol;
   int               typePosition;
   string            host;
   
public:
                     ServerRequests(string _apiKey, string _account, string _symbol, ENUM_POSITION_TYPE _typePosition, string _host);

   ServerResponse    sendRequest(string route, CJAVal &jsonData, int timeOut = 10000);
   
   CJAVal            getBatchDataJson(CArrayObj &quantAnalizeItems);

   ServerResponse    quantPredict(CArrayObj &quantAnalizeItems);
   ServerResponse    quantAddBatch(CArrayObj &quantAnalizeItems);
   ServerResponse    quantAddSingle(QuantAnalizeItem &analizeItem);
   
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ServerRequests::ServerRequests(string _apiKey, string _account, string _symbol, ENUM_POSITION_TYPE _typePosition, string _host)
{
   apiKey = _apiKey;
   account = _account;
   symbol=_symbol;
   typePosition = (int)_typePosition;
   host = _host;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ServerResponse ServerRequests::sendRequest(string route, CJAVal &jsonData, int timeOut = 10000)
{
   ResetLastError();

   ServerResponse response;
   CJAVal data;
   
   string toSendData = jsonData.Serialize();
   
   string resp = makeRequest(host, route, jsonData.Serialize());

   data.Deserialize(resp);

   if(data["status"].ToBool()) {
      bool status = (bool)data["status"].ToBool();
      string message = data["message"].ToStr();
      if(status)
         response.setRessponse(route, true, message, data["data"]);
      else
         response.setRessponse(route, false, message, data["data"]);
   } else {
      response.setRessponse(route, false, GetLastError(), data);
   }
   return response;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
ServerResponse ServerRequests::quantAddSingle(QuantAnalizeItem &analizeItem)
{
   CArrayObj quantAnalizeItems;
   quantAnalizeItems.Add(&analizeItem);

   return quantAddBatch(quantAnalizeItems);
}
//+------------------------------------------------------------------+
ServerResponse ServerRequests::quantAddBatch(CArrayObj &quantAnalizeItems)
{
   CJAVal object = getBatchDataJson(quantAnalizeItems);

   return sendRequest(routeQuantAdd, object);
}

ServerResponse ServerRequests::quantPredict(CArrayObj &quantAnalizeItems)
{
   CJAVal object = getBatchDataJson(quantAnalizeItems);

   return sendRequest(routeQuantPredict, object);
}


//+------------------------------------------------------------------+
CJAVal ServerRequests::getBatchDataJson(CArrayObj &quantAnalizeItems)
{
   CJAVal object;
   object["apiKey"]=apiKey;
   object["account"]=account;
   object["symbol"]=symbol;
   object["typePosition"]=(int)typePosition;
   
   for(int i=0; i<quantAnalizeItems.Total(); i++) {
      QuantAnalizeItem *quantAnalize = quantAnalizeItems.At(i);
      CJAVal quantItem;

      quantItem["dateTime"]=TimeToString(quantAnalize.time);
      quantItem["price"]=quantAnalize.price;

      // Calculator results Data
      int count = quantAnalize.results.Total();
      for(int t=0; t < count; t++) {
         ResultCalculator *result = quantAnalize.results.At(t);

         CJAVal quantResultCalc;
         quantResultCalc["name"]=result.name;
         quantResultCalc["config"]=result.config;

         for(int k=0; k < result.numbers.Total(); k++) {
            quantResultCalc["timeframes"].Add(result.timeframes.At(k));
            quantResultCalc["numbers"].Add(result.numbers.At(k));
            quantResultCalc["raws"].Add(result.raws.At(k));
         }

         quantItem["calculators"].Add(quantResultCalc);
      }

      for(int j=0; j<quantAnalize.levels.Total(); j++) {
         Level *level = quantAnalize.levels.At(j);
         CJAVal quantLevel;

         quantLevel["tp"] = level.TP;
         quantLevel["sl"] = level.SL;
         quantLevel["tpPrice"] = level.TpPrice;
         quantLevel["slPrice"] = level.SlPrice;
         quantLevel["minPrice"] = level.MinPrice;
         quantLevel["maxPrice"] = level.MaxPrice;
         quantLevel["profit"] = level.profit;
         quantLevel["slump"] = level.slump;

         quantLevel["finishedTime"] = TimeToString(level.finishedTime);

         quantItem["levels"].Add(quantLevel);
      }

      object["quants"].Add(quantItem);
      //ja.Clear();
   }

   string tt = object["quants"].Serialize();

   return object;
}
