//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Arrays\ArrayString.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\helpers\CoreHelper.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

#include <Emilasp\NeuroNet\PatternExtractor\common\InfoStruct.mqh>
#include <Emilasp\NeuroNet\PatternExtractor\common\InfoSignalBarsStruct.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class NeuroNetSignalPattern : public CObject
{
private:
   double               SlPrice;
   double               TpPrice;

   int                  barsAfterPattern;

   void                 drawInfo();

protected:
   string             name;

   ENUM_POSITION_TYPE typePosition;

   int               skipAfterPatternBars;

   int               barsPatternCount;
   int               barsSignalCount;

   CArrayObj         *barsPattern;
   CArrayObj         *barsSignal;

   Bar               *startSignalBar;

   InfoSignalBarsStruct infoSignalBars;
   Info                 info;

   CArrayString      *features;
   CArrayString      *featuresColumns;

   int               SL;
   int               TP;

   int               emptyBarsForSnapShotCount; // Сколько баров прошло с последней записи
   int               emptyBarsForSnapShotLength; // Через сколько баров записывать данные

   ////// Methods
   void              addFeatureColumns(string featureName, string &subNames[], int countBars);
   void              addFeatureColumns(string featureName, string subName, int countBars);
   string            arrayToRow(CArrayString &array, string separate = ",");

   void              addFeatureBars(int countBars);
   void              addFeatureBarsTypical(int countBars);
   void              addFeatureVolume(int countBars);
   void              addFeatureMA(string symbol, ENUM_TIMEFRAMES timeFrame, int period, int shift, ENUM_MA_METHOD method, ENUM_APPLIED_PRICE appliedPrice, int countBars);
   void              addFeatureRSI(string symbol, ENUM_TIMEFRAMES timeFrame, int period, ENUM_APPLIED_PRICE applied_price, int countBars);
   void              addFeatureStochastic(string symbol, ENUM_TIMEFRAMES timeFrame, int Kperiod, int Dperiod, int slowing, ENUM_MA_METHOD ma_method, ENUM_STO_PRICE price_field, int countBars);
   void              addFeatureCCI(string symbol, ENUM_TIMEFRAMES timeFrame, int period, ENUM_APPLIED_PRICE applied_price, int countBars);
   void              addFeatureMultiTfIndicatorsGrid(ENUM_TIMEFRAMES &timeFrames[], int countBars);
   void              addFeatureMaOverGrid(int &periods[], int countBars);
   
   void              saveDataEmpty();

   virtual void      setFeatures() {} // Формируем массив с признаками и присваиваем в features (бары,индикаторы,время)
   virtual bool      isPattern()
   {
      return true;  // Определен паттерн на barsPatternCount интервале
   }
   virtual bool      isSuccess();   // Сработало условие SUCCESS(если есть профит)
   virtual bool      isLoss();      // Сработало условие на провал(достигнута максимальная просадка)
   
   
   virtual void      saveData(string type) {}; // формируем данные из признаков
   string            saveDataToFile(string type, string separate = ";"); // формируем данные из признаков
   void              reset(); // сброс
   void              loadBars();
   void              drawPattern(bool isSuccess);
   
public:
                     NeuroNetSignalPattern(ENUM_POSITION_TYPE type);
                    ~NeuroNetSignalPattern();

   bool              calculate(); // вызывается на каждом сигнале

};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
NeuroNetSignalPattern::NeuroNetSignalPattern(ENUM_POSITION_TYPE type)
{
   name = "Не установлено";

   typePosition = type;

   barsPattern = new CArrayObj;
   barsSignal = new CArrayObj;

   features = new CArrayString;
   featuresColumns = new CArrayString;

   emptyBarsForSnapShotCount = 0;

   reset();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::loadBars()
{
   int barsCountAll = barsPatternCount + barsSignalCount;

   MqlRates rates[];
   ArraySetAsSeries(rates, true);
   int copied = CopyRates(_Symbol, _Period, 0, barsCountAll, rates);

   for(int i = 0; i < barsCountAll; i++) {
      Bar *bar = new Bar(rates[i]);

      if(i >= barsSignalCount) {
         barsPattern.Add(bar);
      } else {
         barsSignal.Add(bar);
      }

      if(i == barsSignalCount) {
         startSignalBar = bar;

         if(typePosition == POSITION_TYPE_BUY) {
            TpPrice = startSignalBar.close + TP * _Point;
            SlPrice = startSignalBar.close - SL * _Point;
         } else {
            TpPrice = startSignalBar.close - TP * _Point;
            SlPrice = startSignalBar.close + SL * _Point;
         }
      }
   }

   infoSignalBars.setBarsInfo(typePosition, barsSignal, SL, TP);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool NeuroNetSignalPattern::calculate()
{
   drawInfo();

   reset();
   loadBars();

   DrawHelper::drawArrow("signalCurrent", startSignalBar.time, startSignalBar.close, 232, clrCoral);

   barsAfterPattern++;
   emptyBarsForSnapShotCount++;

   bool needSkip = skipAfterPatternBars > 0 && skipAfterPatternBars > barsAfterPattern;
   if(!needSkip && isPattern()) {
      barsAfterPattern = 0;

      info.patternsAll++;

      if(isLoss()) {
         saveData("loss");
         //saveDataToFile("loss");
         info.patternsLoss++;

         drawPattern(false);
         return false;
      } else {
         if(isSuccess()) {
            saveData("loss");
            //saveDataToFile("success");
            info.patternsSuccess++;

            drawPattern(true);
            return true;
         } else {
            saveData("loss");
            //saveDataToFile("not-tp"); // Паттерн найден, но не отработал по профиту
            info.patternsEmpty++;
         }
      }

   } else {
      DrawHelper::moveHorizontalLine("TP0", TpPrice, clrWhiteSmoke);
      DrawHelper::moveHorizontalLine("SL0", SlPrice, clrWhiteSmoke);
      // Паттерн не найден - заполняем файл с элементами без паттерна
      saveDataEmpty();
   }

   return false;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::drawInfo()
{
   DrawHelper::drawLabel("Info 1", info.getInfo());
   DrawHelper::drawLabel("Info 2", info.additionalInfoString, 10, 50);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::saveDataEmpty()
{
   if(emptyBarsForSnapShotLength < emptyBarsForSnapShotCount) {
      emptyBarsForSnapShotCount = 0;
      saveDataToFile("empty-random");

      info.patternsNo++;
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::drawPattern(bool isSuccess)
{
   DrawHelper::moveHorizontalLine("TP0", 0, clrGray);
   DrawHelper::moveHorizontalLine("SL0", 0, clrGray);

   DrawHelper::moveHorizontalLine("TP", TpPrice, clrGreen);
   DrawHelper::moveHorizontalLine("SL", SlPrice, clrRed);

   int arrowSymbol = typePosition == POSITION_TYPE_BUY ? 233 : 234;
   int colorArrow = isSuccess ? clrGreen : clrRed;
   DrawHelper::drawArrow("signal:" + startSignalBar.time, startSignalBar.time, startSignalBar.close, arrowSymbol, colorArrow);

   int clr = isSuccess ? clrOrange : clrWhite;

   Bar *barFirst = barsPattern.At(barsPatternCount - 1);
   Bar *barLast  = barsSignal.At(0);
   DrawHelper::drawRectangle("patternSuccess" + startSignalBar.time, barFirst.time, SlPrice, barLast.time, TpPrice, 0, colorArrow);

   if(infoSignalBars.takeProfitBar != NULL)
      DrawHelper::drawArrow("patternTp:" + startSignalBar.time, infoSignalBars.takeProfitBar.time, infoSignalBars.takeProfitBar.high, 221, clrGreenYellow);

   if(infoSignalBars.stopLossBar != NULL)
      DrawHelper::drawArrow("patternSL:" + startSignalBar.time, infoSignalBars.stopLossBar.time, infoSignalBars.stopLossBar.low, 222, clrOrangeRed);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string NeuroNetSignalPattern::saveDataToFile(string type, string separate = ";")
{
   setFeatures();

   if(info.hasErrorsCalculate()) {
      return "";
   }

   string fileName = "export.pattern." + name + "." + type + ".csv";

   if(!FileIsExist(fileName, FILE_COMMON)) {
      CoreHelper::saveDataToCsv(fileName, arrayToRow(featuresColumns), true);
   }

   CoreHelper::saveDataToCsv(fileName, arrayToRow(features), true);

   info.savePatternCount++;

   return "";
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string NeuroNetSignalPattern::arrayToRow(CArrayString &array, string separate = ",")
{
   int size = array.Total();
   string row = "";
   for(int i = 0; i < size; i++) {
      row += array.At(i);

      if(i + 1 < size)
         row += separate;
   }
   return row;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::reset()
{
   barsPattern.Clear();
   barsSignal.Clear();

   features.Clear();
   featuresColumns.Clear();

   info.errorsCalculate.Clear();
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
NeuroNetSignalPattern::~NeuroNetSignalPattern()
{
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool NeuroNetSignalPattern::isSuccess()
{
   return infoSignalBars.isTakeProfit();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool NeuroNetSignalPattern::isLoss()
{
   return infoSignalBars.isStopLoss();
}
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureColumns(string featureName, string &subNames[], int countBars)
{
   int subCount = ArraySize(subNames);
   for(int i = 0; i < countBars; i++) {
      for(int j = 0; j < subCount; j++) {
         featuresColumns.Add(featureName + "_" + subNames[j] + "_B:" + IntegerToString(i + 1));
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureColumns(string featureName, string subName, int countBars)
{
   string subCols[1];
   subCols[0] = subName;

   addFeatureColumns(featureName, subCols, countBars);
}










////////////// FEATURES ////////////////

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureBars(int countBars)
{
   string subCols[4] = {"H", "L", "O", "C"};
   addFeatureColumns("bar", subCols, countBars);

   for(int i = 0; i < countBars; i++) {
      Bar *bar = barsPattern.At(i);

      features.Add(DoubleToString(bar.high));
      features.Add(DoubleToString(bar.low));
      features.Add(DoubleToString(bar.open));
      features.Add(DoubleToString(bar.close));
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureBarsTypical(int countBars)
{
   addFeatureColumns("bar", "T", countBars);

   for(int i = 0; i < countBars; i++) {
      Bar *bar = barsPattern.At(i);

      features.Add(DoubleToString((bar.high + bar.low + bar.close)/3));
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureVolume(int countBars)
{
   addFeatureColumns("ind", "V", countBars);

   double BufferData[];
   ArraySetAsSeries(BufferData, true);
   int handle = iVolume(_Symbol, _Period, 0);

   Bar *patternFirstBar = barsPattern.At(0);
   if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
      for(int i = 0; i < countBars; i++) {
         features.Add(DoubleToString(BufferData[i]));
      }
   } else {
      info.addErrorCalculate("addFeatureVolume: copy buffer error");
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureMA(string symbol, ENUM_TIMEFRAMES timeFrame, int period, int shift, ENUM_MA_METHOD method, ENUM_APPLIED_PRICE appliedPrice, int countBars)
{
   addFeatureColumns("ind", "MA", countBars);

   double BufferData[];
   ArraySetAsSeries(BufferData, true);
   int handle = iMA(symbol, timeFrame, period, shift, method, appliedPrice);

   Bar *patternFirstBar = barsPattern.At(0);
   if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
      for(int i = 0; i < countBars; i++) {
         features.Add(DoubleToString(BufferData[i]));
      }
   } else {
      info.addErrorCalculate("addFeatureMA: copy buffer error");
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureRSI(string symbol, ENUM_TIMEFRAMES timeFrame, int period, ENUM_APPLIED_PRICE applied_price, int countBars)
{
   addFeatureColumns("ind", "RSI", countBars);

   double BufferData[];
   ArraySetAsSeries(BufferData, true);
   int handle = iRSI(symbol, timeFrame, period, applied_price);

   Bar *patternFirstBar = barsPattern.At(0);
   if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
      for(int i = 0; i < countBars; i++) {
         features.Add(DoubleToString(BufferData[i]));
      }
   } else {
      info.addErrorCalculate("addFeatureRSI: copy buffer error");
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureStochastic(string symbol, ENUM_TIMEFRAMES timeFrame, int Kperiod, int Dperiod, int slowing, ENUM_MA_METHOD ma_method, ENUM_STO_PRICE price_field, int countBars)
{
   addFeatureColumns("ind", "STO", countBars);

   double BufferData[];
   ArraySetAsSeries(BufferData, true);
   int handle = iStochastic(symbol, timeFrame, Kperiod, Dperiod, slowing, ma_method, price_field);

   Bar *patternFirstBar = barsPattern.At(0);
   if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
      for(int i = 0; i < countBars; i++) {
         features.Add(DoubleToString(BufferData[i]));
      }
   } else {
      info.addErrorCalculate("addFeatureStochastic: copy buffer error");
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureCCI(string symbol, ENUM_TIMEFRAMES timeFrame, int period, ENUM_APPLIED_PRICE applied_price, int countBars)
{
   addFeatureColumns("ind", "CCI", countBars);

   double BufferData[];
   ArraySetAsSeries(BufferData, true);
   int handle = iCCI(symbol, timeFrame, period, applied_price);

   Bar *patternFirstBar = barsPattern.At(0);
   if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
      for(int i = 0; i < countBars; i++) {
         features.Add(DoubleToString(BufferData[i]));
      }
   } else {
      info.addErrorCalculate("addFeatureCCI: copy buffer error");
   }
}



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureMaOverGrid(int &periods[], int countBars)
{
   Bar *patternFirstBar = barsPattern.At(0);

   int countPeriods = ArraySize(periods);
   for(int i = 0; i < countPeriods; i++) {
      int period = periods[i];
      addFeatureColumns("ind_" + IntegerToString(period), "MA_grid_over", countBars);

      double BufferData[];
      ArraySetAsSeries(BufferData, true);
      int handle = iMA(_Symbol, _Period, period, 0, MODE_SMA, PRICE_TYPICAL);

      if(CopyBuffer(handle, 0, patternFirstBar.time, countBars, BufferData)) {
         for(int i = 0; i < countBars; i++) {
            string featureValue = "0";
            if(typePosition == POSITION_TYPE_BUY && BufferData[i] < patternFirstBar.high)
               featureValue = "1";

            if(typePosition == POSITION_TYPE_SELL && BufferData[i] > patternFirstBar.low)
               featureValue = "1";

            features.Add(featureValue);
         }
      } else {
         info.addErrorCalculate("addFeatureCCI: copy buffer error");
      }
   }
}
//+------------------------------------------------------------------+
void NeuroNetSignalPattern::addFeatureMultiTfIndicatorsGrid(ENUM_TIMEFRAMES &timeFrames[], int countBars)
{
   Bar *patternFirstBar = barsPattern.At(0);

   int countTimeFrames = ArraySize(timeFrames);
   for(int i = 0; i < countTimeFrames; i++) {
      ENUM_TIMEFRAMES timeFrame = timeFrames[i];

      string inds[] = {"mtf_grid_RSI", "mtf_grid_STO", "mtf_grid_CCI"};
      addFeatureColumns("ind_" + IntegerToString(timeFrame), inds, countBars);


      double BufferDataRSI[];
      ArraySetAsSeries(BufferDataRSI, true);
      int handleRsi = iRSI(_Symbol, _Period, 14, PRICE_CLOSE);

      double BufferDataSTO[];
      ArraySetAsSeries(BufferDataSTO, true);
      int handleSto = iStochastic(_Symbol, _Period, 5, 3, 3, MODE_SMA, STO_LOWHIGH);

      double BufferDataCCI[];
      ArraySetAsSeries(BufferDataCCI, true);
      int handleCci = iCCI(_Symbol, _Period, 14, PRICE_TYPICAL);

      if(CopyBuffer(handleRsi, 0, patternFirstBar.time, countBars + 1, BufferDataRSI)) {
         if(CopyBuffer(handleSto, 0, patternFirstBar.time, countBars + 1, BufferDataSTO)) {
            if(CopyBuffer(handleCci, 0, patternFirstBar.time, countBars + 1, BufferDataCCI)) {
               for(int i = 0; i < countBars; i++) {
                  //Bar *bar = barsPattern.At(i);

                  string signalRsi = "0";
                  string signalSto = "0";
                  string signalCci = "0";

                  if(typePosition == POSITION_TYPE_BUY) {
                     if(BufferDataRSI[i] < 30 && BufferDataRSI[i+1] > 30) // RSI
                        signalRsi = "1";

                     if(BufferDataSTO[i] < 20 && BufferDataSTO[i+1] > 20) // STO
                        signalSto = "1";

                     if(BufferDataCCI[i] > -100 && BufferDataCCI[i+1] < -100) // CCI
                        signalCci = "1";
                  }
                  if(typePosition == POSITION_TYPE_SELL) {
                     if(BufferDataRSI[i] > 70 && BufferDataRSI[i+1] < 70) // RSI
                        signalRsi = "1";

                     if(BufferDataSTO[i] > 80 && BufferDataSTO[i+1] < 80) // STO
                        signalSto = "1";

                     if(BufferDataCCI[i] < 100 && BufferDataCCI[i+1] > 100) // CCI
                        signalCci = "1";
                  }

                  features.Add(signalRsi);
                  features.Add(signalSto);
                  features.Add(signalCci);
               }
            } else {
               info.addErrorCalculate("addFeatureMultiTfIndicatorsGrid: CCI copy buffer error");
            }
         } else {
            info.addErrorCalculate("addFeatureMultiTfIndicatorsGrid: STO copy buffer error");
         }
      } else {
         info.addErrorCalculate("addFeatureMultiTfIndicatorsGrid: RSI copy buffer error");
      }
   }
}
//+------------------------------------------------------------------+
