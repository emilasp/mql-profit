//+------------------------------------------------------------------+
//|                              BetaTestAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\PatternExtractor\NeuroNetSignalPattern.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class BetaTestAlphaNeurolPattern : public NeuroNetSignalPattern
{
private:
   
protected:
   
   void      setFeatures(); // Формируем массив с признаками и присваиваем в features (бары,индикаторы,время)
   bool      isPattern();   // Определен паттерн на barsPatternCount интервале

public:
                     BetaTestAlphaNeurolPattern();
                    ~BetaTestAlphaNeurolPattern();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BetaTestAlphaNeurolPattern::BetaTestAlphaNeurolPattern()
   : NeuroNetSignalPattern(POSITION_TYPE_BUY, 20, 20)
{
   name = "Consolidation";
   
   TP = 100;
   SL = 100;
   
   skipAfterPatternBars = 20;
   
   emptyBarsForSnapShotLength = 30;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BetaTestAlphaNeurolPattern::~BetaTestAlphaNeurolPattern()
{
}
//+------------------------------------------------------------------+
void BetaTestAlphaNeurolPattern::setFeatures()
{
   int countBars = 10;

   addFeatureBars(countBars);
   addFeatureVolume(countBars);
   
   addFeatureMA(_Symbol, _Period, 7, 0, MODE_SMA, PRICE_TYPICAL, countBars);
   addFeatureRSI(_Symbol, _Period, 14, PRICE_CLOSE, countBars);
   addFeatureStochastic(_Symbol, _Period, 5, 3, 3, MODE_SMA, STO_LOWHIGH, countBars);
   addFeatureCCI(_Symbol, _Period, 14, PRICE_TYPICAL, countBars);
   addFeatureMaOverPrice(_Symbol, _Period, 100, 0, MODE_EMA,PRICE_TYPICAL, 1);
   addFeatureMaIntersection(_Symbol, _Period, 7, 14, 0, MODE_SMA, PRICE_TYPICAL, countBars);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool BetaTestAlphaNeurolPattern::isPattern()
{
   if (!isSuccess() || isLoss()) {
      return false;
   }

   Print("Is Pattern: price max - " + maxSignalBar.high + ", tp: " + (startSignalBar.close + TP * _Point));
   Print("Is Pattern: price min - " + minSignalBar.low + ", sl: " + (startSignalBar.close - SL * _Point));
   return true;
}
