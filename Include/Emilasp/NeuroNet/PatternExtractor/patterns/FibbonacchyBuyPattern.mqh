//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\PatternExtractor\NeuroNetSignalPattern.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class FibbonacchyBuyPattern : public NeuroNetSignalPattern 
{
private:
   int handle;
protected:
   void              setFeatures(); // Формируем массив с признаками и присваиваем в features (бары,индикаторы,время)
   bool              isPattern();   // Определен паттерн на barsPatternCount интервале
   void              saveData(string type);
public:
                     FibbonacchyBuyPattern();
                    ~FibbonacchyBuyPattern();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FibbonacchyBuyPattern::FibbonacchyBuyPattern()
   : NeuroNetSignalPattern(POSITION_TYPE_BUY)
{
   name = "Fib";

   barsPatternCount = 100;
   barsSignalCount = 60;

   TP = 100;
   SL = 100;

   skipAfterPatternBars = 20;

   emptyBarsForSnapShotLength = 3000;
   
   handle = iCustom(_Symbol, _Period, "Learning/em_fibo");
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FibbonacchyBuyPattern::~FibbonacchyBuyPattern()
{
}
//+------------------------------------------------------------------+
void FibbonacchyBuyPattern::setFeatures()
{
   int countBars = 20;
   
   //addFeatureBarsTypical(countBars);
   addFeatureBars(countBars);
   addFeatureVolume(countBars);

   addFeatureMA(_Symbol, _Period, 7, 0, MODE_SMA, PRICE_TYPICAL, countBars);
   addFeatureRSI(_Symbol, _Period, 14, PRICE_CLOSE, countBars);
   addFeatureStochastic(_Symbol, _Period, 5, 3, 3, MODE_SMA, STO_LOWHIGH, countBars);
   addFeatureCCI(_Symbol, _Period, 14, PRICE_TYPICAL, countBars);
   
   int periods[6] = {5,14, 21, 48, 100, 200};
   addFeatureMaOverGrid(periods, countBars);
   
   ENUM_TIMEFRAMES timeFrames[6] = {PERIOD_M2, PERIOD_M3, PERIOD_M5, PERIOD_M15, PERIOD_M30, PERIOD_H1};
   addFeatureMultiTfIndicatorsGrid(timeFrames, countBars);
   
   //addFeatureMaOverPrice(_Symbol, _Period, 100, 0, MODE_EMA, PRICE_TYPICAL, 1);
   //addFeatureMaIntersection(_Symbol, _Period, 7, 14, 0, MODE_SMA, PRICE_TYPICAL, countBars);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool FibbonacchyBuyPattern::isPattern()
{
   Bar *prevPatternBar  = barsPattern.At(1);
   Bar *firstPatternBar = barsPattern.At(0);
   
   double LastLevelFibBuf[];
   if(CopyBuffer(handle, 10,firstPatternBar.time, 1, LastLevelFibBuf)) {
      double lastLevelFibPrice = LastLevelFibBuf[0];
      
      if (firstPatternBar.low > lastLevelFibPrice && prevPatternBar.low < lastLevelFibPrice)
         return true;
   }
   
   return false;
}

//+------------------------------------------------------------------+
void FibbonacchyBuyPattern::saveData(string type)
{
   saveDataToFile(type);
}
