//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\NeuroNet\PatternExtractor\NeuroNetSignalPattern.mqh>

struct ConsolidationAvg {
   double            avgBar;
   double            maxHeigthBar;
   double            maxPercent;
   void              ConsolidationAvg()
   {
      avgBar = 0;
      maxHeigthBar = 0;
      maxPercent = 0;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ConsolidationNeurolPatternScreenShot : public NeuroNetSignalPattern
{
private:

protected:
   void              setFeatures(); // Формируем массив с признаками и присваиваем в features (бары,индикаторы,время)
   bool              isPattern();   // Определен паттерн на barsPatternCount интервале

   ConsolidationAvg  getAvgBar(int from, int countBars);
   void              saveData(string type);
public:
                     ConsolidationNeurolPatternScreenShot();
                    ~ConsolidationNeurolPatternScreenShot();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationNeurolPatternScreenShot::ConsolidationNeurolPatternScreenShot()
   : NeuroNetSignalPattern(POSITION_TYPE_BUY)
{
   name = "Consolidation";

   barsPatternCount = 60;
   barsSignalCount = 20;

   TP = 100;
   SL = 100;

   skipAfterPatternBars = 20;

   emptyBarsForSnapShotLength = 300;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationNeurolPatternScreenShot::~ConsolidationNeurolPatternScreenShot()
{
}
//+------------------------------------------------------------------+
void ConsolidationNeurolPatternScreenShot::setFeatures()
{
   int countBars = 10;
   
   //addFeatureBars(countBars);
   //addFeatureVolume(countBars);

   //addFeatureMA(_Symbol, _Period, 7, 0, MODE_SMA, PRICE_TYPICAL, countBars);
   //addFeatureRSI(_Symbol, _Period, 14, PRICE_CLOSE, countBars);
   //addFeatureStochastic(_Symbol, _Period, 5, 3, 3, MODE_SMA, STO_LOWHIGH, countBars);
   //addFeatureCCI(_Symbol, _Period, 14, PRICE_TYPICAL, countBars);
   //addFeatureMaOverPrice(_Symbol, _Period, 100, 0, MODE_EMA, PRICE_TYPICAL, 1);
   //addFeatureMaIntersection(_Symbol, _Period, 7, 14, 0, MODE_SMA, PRICE_TYPICAL, countBars);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ConsolidationNeurolPatternScreenShot::isPattern()
{
   int isConsolidationPercent = 45;
   
   int barsToCalcAvgBase = 50;
   int barsToCalcAvgPattern = 10;

   ConsolidationAvg avgBarPattern = getAvgBar(1, barsToCalcAvgPattern - 1);
   ConsolidationAvg avgBarBase    = getAvgBar(barsToCalcAvgPattern, barsToCalcAvgBase - 1);

   double consolidationPercent = avgBarPattern.avgBar / avgBarBase.avgBar * 100;
   
   info.additionalInfoString = DoubleToString(consolidationPercent) + "%,  Avg: " + DoubleToString(avgBarPattern.avgBar) + "/" + avgBarBase.avgBar;
   
   bool hasConsolidation = consolidationPercent < isConsolidationPercent;
   
   if (hasConsolidation) {
      Bar *bar = barsPattern.At(0);
      return bar.isBigBody();
   }
   
   return false;
}




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationAvg ConsolidationNeurolPatternScreenShot::getAvgBar(int from, int countBars)
{
   ConsolidationAvg avg;

   double sumBars = 0;
   double maxPrice = 0;
   double minPrice = DBL_MAX;

   for(int i = 0; i < countBars; i++) {
      Bar *bar = barsPattern.At(from + i);
      
      
      sumBars += bar.bodySize;

      if(maxPrice < bar.high)
         maxPrice = bar.high;
      if(minPrice > bar.low)
         minPrice = bar.low;
   }

   avg.avgBar = sumBars / countBars;
   avg.maxHeigthBar = maxPrice - minPrice;
   avg.maxPercent = avg.avgBar / avg.maxHeigthBar * 100;

   return avg;
}

//+------------------------------------------------------------------+
void ConsolidationNeurolPatternScreenShot::saveData(string type)
{
   string fileName = "screen_" + type + IntegerToString(info.savePatternCount) + ".png";
   if(ChartScreenShot(0, fileName, 800, 600))
      Print("Screen SHOT !!!!!!! >>> " + fileName);
}
