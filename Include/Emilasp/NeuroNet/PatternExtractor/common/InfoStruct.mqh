//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Arrays\ArrayString.mqh>


struct Info {
   CArrayString      errors;
   CArrayString      errorsCalculate;

   int               patternsAll;
   int               patternsSuccess;
   int               patternsLoss;
   int               patternsEmpty;
   int               patternsNo;
   int               savePatternCount;

   string            additionalInfoString;

   void              Info()
   {
      patternsAll = 0;
      patternsSuccess = 0;
      patternsLoss = 0;
      patternsEmpty = 0;
      patternsNo = 0;

      savePatternCount = 0;
   }

   void              addErrorCalculate(string error)
   {
      errorsCalculate.Add(error);
      errors.Add(error);
   }

   bool              hasErrorsCalculate()
   {
      return errorsCalculate.Total() > 0;
   }

   string            getInfo()
   {
      return "S/L/E/N/O: " + IntegerToString(patternsSuccess) + "/" + IntegerToString(patternsLoss) + "/" + IntegerToString(patternsEmpty)
             + "/" + IntegerToString(patternsNo) +  "/" + IntegerToString(patternsAll)
             + ", Errors: " + IntegerToString(errorsCalculate.Total()) + "/" + IntegerToString(errors.Total());
   }
};
//+------------------------------------------------------------------+
