//+------------------------------------------------------------------+
//|                                                   InfoStruct.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Arrays\ArrayString.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Arrays\ArrayObj.mqh>


struct InfoSignalBarsStruct {
   ENUM_POSITION_TYPE typePosition;

   CArrayObj         bars;

   Bar               *stopLossBar;   // Первый бар где сработал SL
   Bar               *takeProfitBar; // Первый бар где сработал TP
   Bar               *maxBar;        // Максимальная цена в сигнале
   Bar               *minBar;        // Минимальная цена в сигнале

   int                sl;
   int                tp;

   void              setBarsInfo(ENUM_POSITION_TYPE _typePosition, CArrayObj &barsSignal, int _sl, int _tp)
   {
      typePosition = _typePosition;
      bars = barsSignal;

      sl = _sl;
      tp = _tp;

      stopLossBar   = NULL;
      takeProfitBar = NULL;
      maxBar = NULL;
      minBar = NULL;
      
      init();
   }


   void              init()
   {
      Bar *startBar = bars.At(bars.Total() - 1);

      bool hasEndPattern = false;
      for(int i = bars.Total() - 1; i >= 0 ; i--) {
         Bar *bar = bars.At(i);

         /// Max and Min ///
         if(maxBar == NULL || maxBar.high < bar.high)
            maxBar = bar;

         if(minBar == NULL || minBar.low > bar.low)
            minBar = bar;

         /// Stop Loss and TakeProfit ///
         if(!hasEndPattern) {
            if(typePosition == POSITION_TYPE_BUY) {
               double takeProfitPrice = startBar.open + tp * _Point;
               double stopLossPrice   = startBar.open - sl * _Point;

               if (stopLossPrice > bar.low) {
                  stopLossBar = bar;
                  hasEndPattern = true;
               }

               if (takeProfitPrice < bar.high) {
                  takeProfitBar = bar;
                  hasEndPattern = true;
               }
            }
            if(typePosition == POSITION_TYPE_SELL) {
               double takeProfitPrice = startBar.open - tp * _Point;
               double stopLossPrice   = startBar.open + sl * _Point;

               if (stopLossPrice < bar.high) {
                  stopLossBar = bar;
                  hasEndPattern = true;
               }

               if (takeProfitPrice > bar.low) {
                  takeProfitBar = bar;
                  hasEndPattern = true;
               }
            }
         }
      }
   }

   bool              isTakeProfit()
   {
      if(takeProfitBar == NULL)
         return false;

      if(stopLossBar != NULL && takeProfitBar.time >= stopLossBar.time)
         return false;

      return true;
   }

   bool              isStopLoss()
   {
      if(stopLossBar == NULL)
         return false;

      if(takeProfitBar != NULL && stopLossBar.time >= takeProfitBar.time)
         return false;

      return true;
   }
};
//+------------------------------------------------------------------+
