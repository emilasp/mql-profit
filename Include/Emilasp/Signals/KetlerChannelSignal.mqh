//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class KetlerChannelSignal : public ASignalBase
{
private:
protected:
  
public:

     KetlerChannelSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade);
     ~KetlerChannelSignal();
     
     SignalResult getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KetlerChannelSignal::KetlerChannelSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade)
   : ASignalBase()
{
   name = "KetlerChannelSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;

   handler = iCustom(_Symbol,_Period,"Vendors/keltner_channel_oscillator");
}


SignalResult KetlerChannelSignal::getSignal()
{
   result.reset();

   bool isSignalBuy = false;
   bool isSignalSell = false;

////// Ketler ////////
   double IndUpper[];
   ArraySetAsSeries(IndUpper, true);
   CopyBuffer(handler, 2, 0, 2, IndUpper);
   double IndLower[];
   ArraySetAsSeries(IndLower, true);
   CopyBuffer(handler, 3, 0, 2, IndLower);
   double IndPrice[];
   ArraySetAsSeries(IndPrice, true);
   CopyBuffer(handler, 4, 0, 2, IndPrice);
 
   
// BUY
   if (IndPrice[1] < IndLower[1] && IndPrice[0] > IndLower[0]){
      isSignalBuy = true;
   }   
// SELL
   if (IndPrice[1] > IndUpper[1] && IndPrice[0] < IndUpper[0]) {
      isSignalSell = true;
   }
  
   if (isSignalBuy) {
      result.buy = true;
   }

   if (isSignalSell) {
      result.sell = true;
   }

   return result;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
KetlerChannelSignal::~KetlerChannelSignal()
{
}
