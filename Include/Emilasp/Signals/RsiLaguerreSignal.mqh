//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class RsiLaguerreSignal: public ASignalBase
{
private:
protected:
   double               upLine;
   double               dwnLine;

public:

                     RsiLaguerreSignal(ENUM_TIMEFRAMES _timeFrame, int rsiPeriod, double _upLine, double _dwnLine);

   SignalResult      getSignal();

   //void              setSignalsSettings(ENUM_ENTER_TYPE enterType, ENUM_EXIT_TYPE exitType);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RsiLaguerreSignal::RsiLaguerreSignal(ENUM_TIMEFRAMES _timeFrame, int rsiPeriod, double _upLine, double _dwnLine)
   : ASignalBase()
{
   name = "setSignalsSettings";
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;


   upLine = _upLine;
   dwnLine = _dwnLine;

   handler = iCustom(_Symbol, _Period, "Vendors/Laguerre_RSI_ng", rsiPeriod, PRICE_CLOSE, 1.001, _upLine, _dwnLine);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//void RsiLaguerreSignal::setSignalsSettings(ENUM_ENTER_TYPE enterType, ENUM_EXIT_TYPE exitType)
//{
//   typeExit = exitType;
//   typeEnter = enterType;
//}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult RsiLaguerreSignal::getSignal()
{
   result.reset();


   int lengthTrandPattern = 14;
   int lenghtTrandBars = 20;

   Buffer buffer = getBuffer(0, 0, lenghtTrandBars + 2, true);

   int lenghtTrand = 0;
   for(int i=0; i<lenghtTrandBars; i++) {
      if(i < 2) continue;

      if(buffer.values[i] > dwnLine && buffer.values[i] < upLine )
         break;
      lenghtTrand++;
   }

   if(lenghtTrand >= lengthTrandPattern) {
      if(buffer.values[1] < dwnLine && buffer.values[0] > dwnLine) {
         result.buy = true;
      }
      if(buffer.values[1] > upLine && buffer.values[0] < upLine) {
         result.sell = true;
      }
   }

//if(bS.values[1] > 70 && bS.values[1] > upLine && bS.values[0] < upLine) {
//   result.buyExit= true;
//}
// if(bS.values[1] < 30 && bS.values[1] < dwnLine && bS.values[0] > dwnLine) {
//    result.sellExit= true;
// }

   return result;
}
//+------------------------------------------------------------------+
