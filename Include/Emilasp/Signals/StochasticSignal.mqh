//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>

enum ENUM_ENTER_TYPE {
   ENTER_TYPE_STOCH_NONE = 0,
   ENTER_TYPE_STOCH_LEVEL_INTERSECT = 1,
   ENTER_TYPE_STOCH_SIGNAL_INTERSECT = 2
};


enum ENUM_EXIT_TYPE {
   EXIT_TYPE_STOCH_NONE = 0,
   EXIT_TYPE_STOCH_LEVEL_INTERSECT = 1,
   EXIT_TYPE_STOCH_SIGNAL_INTERSECT = 2
};


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class StochasticSignal: public ASignalBase
{
private:
   int               handlerParent;
protected:
   int               upLine;
   int               dwnLine;

   ENUM_EXIT_TYPE    typeExit;
   ENUM_ENTER_TYPE   typeEnter;

public:

                     StochasticSignal(ENUM_TIMEFRAMES _timeFrame, int kPeriod, int dPeriod, int slowing, int _upLine, int _dwnLine); //, CoreTradeHadge * _trade

   SignalResult      getSignal();

   void              setSignalsSettings(ENUM_ENTER_TYPE enterType, ENUM_EXIT_TYPE exitType);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StochasticSignal::StochasticSignal(ENUM_TIMEFRAMES _timeFrame, int kPeriod, int dPeriod, int slowing, int _upLine, int _dwnLine)//, CoreTradeHadge * _trade
   : ASignalBase()
{
   name = "StochasticSignal";
//trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;

//iconCustomBuy = 159;
//iconCustomSell = 159;

   upLine = _upLine;
   dwnLine = _dwnLine;

   handler = iStochastic(_Symbol, timeFrame, kPeriod, dPeriod, slowing, MODE_SMA, STO_LOWHIGH);
   handlerParent = iStochastic(_Symbol, PERIOD_H1, kPeriod, dPeriod, slowing, MODE_SMA, STO_LOWHIGH);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void StochasticSignal::setSignalsSettings(ENUM_ENTER_TYPE enterType, ENUM_EXIT_TYPE exitType)
{
   typeExit = exitType;
   typeEnter = enterType;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult StochasticSignal::getSignal()
{
   result.reset();

   Buffer bK = getBuffer(0, MAIN_LINE, 2, true);
   Buffer bS = getBuffer(1, SIGNAL_LINE, 2, true);

   bufferFirstValue = bS.values[0];

   if(typeEnter == ENTER_TYPE_STOCH_LEVEL_INTERSECT) {
      if(bS.values[1] < dwnLine && bS.values[0] > dwnLine) {
         result.buy = true;
      }
      if(bS.values[1] > upLine && bS.values[0] < upLine) {
         result.sell = true;
      }
   } else if(typeEnter == ENTER_TYPE_STOCH_SIGNAL_INTERSECT) {
      if(bS.values[1] > bK.values[1] && bS.values[0] < bK.values[0]) {
         result.buy = true;
      }
      if(bS.values[1] < bK.values[1] && bS.values[0] > bK.values[0]) {
         result.sell = true;
      }
   }

   if(typeExit == EXIT_TYPE_STOCH_LEVEL_INTERSECT) {
      if(bS.values[1] > 70 && bS.values[1] > upLine && bS.values[0] < upLine) {
         result.buyExit= true;
      }
      if(bS.values[1] < 30 && bS.values[1] < dwnLine && bS.values[0] > dwnLine) {
         result.sellExit= true;
      }
   } else if(typeExit == EXIT_TYPE_STOCH_SIGNAL_INTERSECT) {
      if(bS.values[1] > 70 && bS.values[1] < bK.values[1] && bS.values[0] > bK.values[0]) {
         result.buyExit= true;
      }
      if(bS.values[1] < 30 && bS.values[1] > bK.values[1] && bS.values[0] < bK.values[0]) {
         result.sellExit= true;
      }
   }

   double BufferParent[];
   ArraySetAsSeries(BufferParent, true);
   CopyBuffer(handlerParent, 0, 0, 2, BufferParent);

   if(BufferParent[0] < 30) {
      result.sell = false;
   }
   if(BufferParent[0] > 70) {
      result.buy = false;
   }

   MqlRates rates[];
   ArraySetAsSeries(rates, true);
   int countRates = 1;
   if(CopyRates(NULL, 0, 0, countRates, rates) > 0) {
      if(rates[0].close < rates[0].open) {
         result.sell = false;
      }
      if(rates[0].close > rates[0].open) {
         result.buy = false;
      }
   } else {
      Print("Ошибка копирования ценовых данных ", GetLastError());
   }


   return result;
}
//+------------------------------------------------------------------+
