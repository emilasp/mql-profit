//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

enum ENUM_SIGNAL_TYPE {
   ENUM_SIGNAL_TYPE_ENTER = 1,        // Точка входа
   ENUM_SIGNAL_TYPE_EXIT = 2,         // Точка выхода
   ENUM_SIGNAL_TYPE_CONFIRMATION = 3  // Подтверждение: например тренд
};

struct Buffer {
   bool              isDraw;
   double            values[];

   void              Buffer()
   {
      isDraw=false;
   }
};

struct SignalResult {
   bool              buy;
   bool              sell;
   bool              buyExit;
   bool              sellExit;

   void              SignalResult()
   {
      buy=false;
      sell=false;
      buyExit=false;
      sellExit=false;
   }

   void              reset()
   {
      buy = false;
      sell = false;
      buyExit=false;
      sellExit=false;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ASignalBase : public CObject
{
private:

protected:
   string             name;
   ENUM_TIMEFRAMES   timeFrame;
   CoreTradeHadge *  trade;
   int               handler;

   Buffer            buffers[];

   string            type;
   SignalResult      result;

   string            infoText;

   int               iconCustomBuy;
   int               iconCustomSell;

   double            bufferFirstValue;

public:
                     ASignalBase();
                    ~ASignalBase();

   virtual           SignalResult getSignal();
   void              draw();
   string            getInfoText(bool addName = false);
   void              setInfoText(string text);
   Buffer            getBuffer(int index, int bufNum, int bars, bool isDraw = false);

   int               chartWindowDrawSignal;

};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ASignalBase::ASignalBase()
{
   name = "Не установлено";
   infoText = "";
   iconCustomBuy = 0;
   iconCustomSell = 0;

   chartWindowDrawSignal = 0;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ASignalBase::~ASignalBase()
{


}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult ASignalBase::getSignal()
{
   return result;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Buffer ASignalBase::getBuffer(int index, int bufNum, int bars, bool isDraw = false)
{
   ResetLastError();

   int countBuffers = ArraySize(buffers);

   if(index > countBuffers -1) {
      Buffer buffer;
      ArrayResize(buffers, index + 1);
      ArraySetAsSeries(buffer.values, true);
      
      buffers[index] = buffer;
   }
   ArrayFree(buffers[index].values);
   
   CopyBuffer(handler, bufNum, 0, bars, buffers[index].values);
   
   return buffers[index];
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ASignalBase::draw()
{
//if (trade.isNewBar()) {
// Draw

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

   int iconCodeBuy = 233;
   int iconCodeSell = 234;
   int iconCodeBuyExit = 203;
   int iconCodeSellExit = 203;

   if (type == ENUM_SIGNAL_TYPE_CONFIRMATION) {
      int iconCodeBuy = 159;
      int iconCodeSell = 159;
   }

   if(iconCustomBuy > 0)
      iconCodeBuy = iconCustomBuy;
   if(iconCustomSell > 0)
      iconCodeSell = iconCustomSell;

   if(chartWindowDrawSignal > 0) {
      if (result.buy) {
         DrawHelper::drawArrow("buy:" + name + last_tick.time, last_tick.time, bufferFirstValue, iconCodeBuy, clrGreen, 0, chartWindowDrawSignal);
      }
      if (result.sell) {
         DrawHelper::drawArrow("sell:" + name + last_tick.time, last_tick.time, bufferFirstValue, iconCodeSell, clrRed, 0, chartWindowDrawSignal);
      }

      if (result.buyExit) {
         DrawHelper::drawArrow("buyExit:" + name + last_tick.time, last_tick.time, bufferFirstValue, iconCodeBuyExit, clrGreen, 0, chartWindowDrawSignal, 10);
      }
      if (result.sellExit) {
         DrawHelper::drawArrow("sellExit:" + name + last_tick.time, last_tick.time, bufferFirstValue, iconCodeSellExit, clrRed, 0, chartWindowDrawSignal, 10);
      }

      string info = getInfoText(false);
      if (StringLen(info) > 0)
         DrawHelper::drawLabel("Chart:" + name, info, 10, 20, 0, chartWindowDrawSignal);
   } else {
      if(type != ENUM_SIGNAL_TYPE_CONFIRMATION) {
         if (result.buy) {
            DrawHelper::drawArrow("buy:" + name + last_tick.time, last_tick.time, last_tick.bid, iconCodeBuy, clrGreen);
         }
         if (result.sell) {
            DrawHelper::drawArrow("sell:" + name + last_tick.time, last_tick.time, last_tick.bid, iconCodeSell, clrRed);
         }
      }
   }


}

//+------------------------------------------------------------------+
void ASignalBase::setInfoText(string text)
{
   infoText = text;
}
//+------------------------------------------------------------------+
string ASignalBase::getInfoText(bool addName = false)
{
   string line = "";
   if(infoText != "") {
      if (addName)
         line = name + ": ";
      line += infoText;
   }

   return line;
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
