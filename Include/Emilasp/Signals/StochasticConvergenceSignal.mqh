//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class StochasticConvergenceSignal : public ASignalBase
{
private:
protected:
  
public:
      double maxDiff;
      double minDiffNext;
     StochasticConvergenceSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, double _maxDiff, double _minDiffNext);
     ~StochasticConvergenceSignal();
     
     SignalResult getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StochasticConvergenceSignal::StochasticConvergenceSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, double _maxDiff, double _minDiffNext)
   : ASignalBase()
{
   name = "StochasticConvergenceSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;
   
   maxDiff = _maxDiff;
   minDiffNext = _minDiffNext;
   
   handler = iCustom(_Symbol,_Period,"Emilasp/StochasticConvergence", maxDiff, minDiffNext);
   
   //int handle1 = iStochastic(_Symbol, PERIOD_CURRENT, 14, 3, 3, MODE_SMA, STO_LOWHIGH);
   //int handle2 = iStochastic(_Symbol, PERIOD_CURRENT, 21, 7, 3, MODE_SMA, STO_LOWHIGH);
   //int handle3 = iStochastic(_Symbol, PERIOD_CURRENT, 33, 14, 3, MODE_SMA, STO_LOWHIGH);
}


SignalResult StochasticConvergenceSignal::getSignal()
{
   result.reset();

   bool isSignalBuy = false;
   bool isSignalSell = false;

   double IndSignal[];
   ArraySetAsSeries(IndSignal, true);
   CopyBuffer(handler, 0, 0, 2, IndSignal);
   double IndDirect[];
   ArraySetAsSeries(IndDirect, true);
   CopyBuffer(handler, 1, 0, 2, IndDirect);
 
   
// BUY
   if (IndSignal[0] < 0){
      isSignalBuy = true;
   }   
// SELL
   if (IndSignal[0] > 0) {
      isSignalSell = true;
   }
  
   if (isSignalBuy) {
      result.buy = true;
   }

   if (isSignalSell) {
      result.sell = true;
   }

   return result;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StochasticConvergenceSignal::~StochasticConvergenceSignal()
{
}
