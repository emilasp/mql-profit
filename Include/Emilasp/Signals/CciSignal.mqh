//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CciSignal: public ASignalBase
{
private:
protected:
   int upLine;
   int dwnLine;

public:

                     CciSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int periodCci, int _upLine, int _dwnLine);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CciSignal::CciSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int periodCci, int _upLine, int _dwnLine)
   : ASignalBase()
{
   name = "CciSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;
   
   iconCustomBuy = 159;
   iconCustomSell = 159;
   
   handler = iCCI(_Symbol, timeFrame,periodCci,PRICE_CLOSE);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult CciSignal::getSignal()
{
   result.reset();

   Buffer buffer = getBuffer(0, 0, 2, true);

   setInfoText("Buffer: " + DoubleToString(buffer.values[0], 4));
 
// BUY
   if(buffer.values[1] < buffer.values[0])
     {
         result.buy = true;
     }

// SELL
    if(buffer.values[1] > buffer.values[0])
     {
          result.sell = true;
     }

   return result;
}
//+------------------------------------------------------------------+
