//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class SuperPassbandFilterSignal: public ASignalBase
{
private:
protected:

public:

                     SuperPassbandFilterSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SuperPassbandFilterSignal::SuperPassbandFilterSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade)
   : ASignalBase()
{
   name = "SmartTrmsSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;

   handler = iCustom(_Symbol, timeFrame, "Vendors/2/Super_Passband_Filter_2");
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult SuperPassbandFilterSignal::getSignal()
{
   result.reset();

   bool isSignalBuy = false;
   bool isSignalSell = false;

   double IndUpper[];
   ArraySetAsSeries(IndUpper, true);
   CopyBuffer(handler, 2, 0, 3, IndUpper);
//
   double IndLine[];
   ArraySetAsSeries(IndLine, true);
   CopyBuffer(handler, 5, 0, 3, IndLine);
   double IndSignal[];
   ArraySetAsSeries(IndSignal, true);
   CopyBuffer(handler, 6, 0, 25, IndSignal);

// Вывести на экран боковой тренд: IndUpper[0] - IndLower[0]  !!!!!!!!!!!!!!!

   int zeroLength = 0;
   if (IndSignal[0] != 0) {
      int firstZeroBar = 0;
      int lastZeroBar = 0;
      int totalBars = ArraySize(IndSignal);
      for(int i=0; i<totalBars; i++) {
         if(i != 0) {
            if (IndSignal[i-1] != 0 && IndSignal[i] == 0) {
               firstZeroBar = i;
            }

            if (IndSignal[i-1] == 0 && IndSignal[i] != 0 && IndSignal[i] != IndSignal[0]) {
               lastZeroBar = i;
               zeroLength = lastZeroBar - firstZeroBar;
               break;
            }
         }
      }
   }
   
   if (zeroLength > 0 && zeroLength < 20) {
      int tt = 10;
   }


   //setInfoText("Канал - " + DoubleToString(IndUpper[0], 6));
   setInfoText("Переворот: " + IntegerToString(zeroLength) + ", channel: " + IndUpper[0]);


// BUY
   if (IndSignal[2] == 0 && IndSignal[1] == 1 && IndSignal[0] == 1 && zeroLength > 0 && zeroLength < 10 && IndUpper[0] < 0.00008) { // && IndUpper[0] < 0.00008
      isSignalBuy = true;
   }
//if (IndSignal[1] == 2 && IndSignal[0] == 0) {
//   if (IndLine[1] > IndLine[2]) {
//      isSignalBuy = true;
//   }
//}
// SELL
   if (IndSignal[1] == 0 && IndSignal[0] == 2) {
      //isSignalSell = true;
   }


   if (isSignalBuy) {
      result.buy = true;
   }

   if (isSignalSell) {
      result.sell = true;
   }

   return result;
}
//+------------------------------------------------------------------+
