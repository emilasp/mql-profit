//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class RsiDivergenceSignal: public ASignalBase
{
private:
protected:
   int               upLine;
   int               dwnLine;

public:

                     RsiDivergenceSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int rsiPeriod, int _upLine, int _dwnLine);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
RsiDivergenceSignal::RsiDivergenceSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int rsiPeriod, int _upLine, int _dwnLine)
   : ASignalBase()
{
   name = "RsiDivergenceSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;


   upLine = _upLine;
   dwnLine = _dwnLine;
   
   handler = iCustom(_Symbol, _Period, "Vendors/RSI_Divergence", rsiPeriod, PRICE_CLOSE, _upLine, _dwnLine);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult RsiDivergenceSignal::getSignal()
{
   result.reset();
      
   Buffer buffer = getBuffer(0, 4, 2, true);
   
      if(buffer.values[1] < dwnLine && buffer.values[0] > dwnLine) {
         result.buy = true;
      }
      if(buffer.values[1] > upLine && buffer.values[0] < upLine) {
         result.sell = true;
      }


  
      //if(bS.values[1] > 70 && bS.values[1] > upLine && bS.values[0] < upLine) {
      //   result.buyExit= true;
      //}
     // if(bS.values[1] < 30 && bS.values[1] < dwnLine && bS.values[0] > dwnLine) {
     //    result.sellExit= true;
     // }

   return result;
}
