//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AdxSignal: public ASignalBase
{
private:
protected:

public:

                     AdxSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int _adxTrandPerid);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AdxSignal::AdxSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade, int _adxTrandPerid)
   : ASignalBase()
{
   name = "AdxSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;
   
   iconCustomBuy = 159;
   iconCustomSell = 159;
   
   handler = iADX(_Symbol, timeFrame, _adxTrandPerid);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult AdxSignal::getSignal()
{
   result.reset();

   Buffer bPlus   = getBuffer(0, 1, 2, true);
   Buffer bMinus  = getBuffer(1, 2, 2, true);
   Buffer bSignal = getBuffer(2, 0, 2, true);
   
   
   setInfoText("DI+: " + DoubleToString(bPlus.values[0], 4) + ", DI-: " + DoubleToString(bMinus.values[0], 4) + ", ADX: " + DoubleToString(bSignal.values[0], 4));
      
// BUY
   if(bSignal.values[1] < bSignal.values[0]) {
      result.buy = true;
   }

// SELL
   if(bSignal.values[1] > bSignal.values[0]) {
      result.sell = true;
   }

   return result;
}
//+------------------------------------------------------------------+
