//+------------------------------------------------------------------+
//|                                        NeuroNetSignalPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>
#include <Emilasp\Signals\Base\ASignalBase.mqh>
//#include <Emilasp\trade\CoreTradeHadge.mqh>
//#include <Emilasp\helpers\DrawHelper.mqh>

#include <Emilasp\libraries\Vendors\comment.mqh>


enum ENUM_SIGNAL_CONCATENATE_TYPE {
   ENUM_SIGNAL_CONCATENATE_TYPE_AND = 1,
   ENUM_SIGNAL_CONCATENATE_TYPE_OR = 2,
   ENUM_SIGNAL_CONCATENATE_TYPE_VOTE = 3
};


struct SignalSettings {
   ENUM_SIGNAL_CONCATENATE_TYPE concatenateType;
   int               weight;

   bool              isDraw;

   void              SignalSettings::SignalSettings(void)
   {
      isDraw=false;
      weight=0;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class SignalManager : public CObject
{
private:
   CArrayObj         signals;
   SignalSettings    signalSettings[];
   CComment          comment;

   CArrayObj         *getArraySignalsByType(ENUM_SIGNAL_CONCATENATE_TYPE type);

protected:

public:
                     SignalManager();
                    ~SignalManager();

   void              addSignal(ASignalBase *signal, ENUM_SIGNAL_CONCATENATE_TYPE concatenateType, bool isDraw = false, int weight = 0, int chartDraw = 0);

   double            getSignalResultAbsolute();
   SignalResult      getSignalResult();

   void              drawSignalsInfo();
   void              OnTick();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalManager::SignalManager()
{
   signals.FreeMode(false);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalManager::~SignalManager()
{

}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SignalManager::addSignal(ASignalBase *signal, ENUM_SIGNAL_CONCATENATE_TYPE concatenateType, bool isDraw = false, int weight = 0, int chartDraw = 0)
{
   if (chartDraw > 0 ){
      signal.chartWindowDrawSignal = chartDraw;
   }

   signals.Add(signal);

   SignalSettings settings;
   settings.concatenateType = concatenateType;
   settings.weight = weight;
   settings.isDraw = isDraw;

   int total = signals.Total();
   ArrayResize(signalSettings, total);
   signalSettings[total - 1] = settings;
}

//+------------------------------------------------------------------+
//| TODO: доделать: Сначала ИЛИ, а затем И рассчитывать для общего result                                                             |
//+------------------------------------------------------------------+
SignalResult SignalManager::getSignalResult()
{
   SignalResult result;

   int totalSignals = signals.Total();
   for(int i=0; i<totalSignals; i++) {
      ASignalBase *signal        = signals.At(i);
      SignalSettings settings   = signalSettings[i];
      SignalResult resultSignal = signal.getSignal();

      if (settings.isDraw) {
         signal.draw();
      }

      // Enter
      if (settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_AND) {
         if (resultSignal.buy == true) {
            if (i == 0 || result.buy == true) {
               result.buy = true;
            }
         } else {
            result.buy = false;
         }

         if (resultSignal.sell == true) {
            if (i == 0 || result.sell == true) {
               result.sell = true;
            }
         } else {
            result.sell = false;
         }
      } else if(settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_OR) {
         if (resultSignal.buy == true) {
            result.buy = true;
         }
         if (resultSignal.sell == true) {
            result.sell = true;
         }
      } else if(settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_VOTE) {

      }
      
      
      // Exit
      if (settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_AND) {
         if (resultSignal.buyExit == true) {
            if (i == 0 || result.buyExit == true) {
               result.buyExit = true;
            }
         } else {
            result.buyExit = false;
         }

         if (resultSignal.sellExit == true) {
            if (i == 0 || result.sellExit == true) {
               result.sellExit = true;
            }
         } else {
            result.sellExit = false;
         }
      } else if(settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_OR) {
         if (resultSignal.buyExit == true) {
            result.buyExit = true;
         }
         if (resultSignal.sellExit == true) {
            result.sellExit = true;
         }
      } else if(settings.concatenateType == ENUM_SIGNAL_CONCATENATE_TYPE_VOTE) {

      }
   }

   return result;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double SignalManager::getSignalResultAbsolute()
{
   double result = 0;
// calculate

   return result;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|  TODO: Решить как находить settings для Сигнала в массиве с измененными индексами                                                  |
//+------------------------------------------------------------------+
CArrayObj *SignalManager::getArraySignalsByType(ENUM_SIGNAL_CONCATENATE_TYPE type)
{
   CArrayObj *array = new CArrayObj();

   int total = signals.Total();
   for(int i=0; i<total; i++) {
      ASignalBase *signal        = signals.At(i);
      SignalSettings settings   = signalSettings[i];

      if(settings.concatenateType == type) {
         array.Add(signal);
      }
   }

   return array;
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SignalManager::drawSignalsInfo()
{
   string lines[];

   int total = signals.Total();
   for(int i=0; i<total; i++) {
      ASignalBase *signal        = signals.At(i);
      SignalSettings settings   = signalSettings[i];

      string signalLine = signal.getInfoText(true);
      if(signalLine != "") {
         int size = ArraySize(lines) + 1;
         ArrayResize(lines, size);
         lines[size - 1] = signalLine;
      }
   }

   if(ArraySize(lines) > 0) {
      DrawHelper::drawInfoPanel(comment, "Signals panel", lines, 0, 20, 150);
   }

}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SignalManager::OnTick()
{
   drawSignalsInfo();
}
//+------------------------------------------------------------------+
