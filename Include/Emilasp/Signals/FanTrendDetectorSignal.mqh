//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class FanTrendDetectorSignal: public ASignalBase
{
private:
protected:

public:

                     FanTrendDetectorSignal(ENUM_TIMEFRAMES _timeFrame);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FanTrendDetectorSignal::FanTrendDetectorSignal(ENUM_TIMEFRAMES _timeFrame)
   : ASignalBase()
{
   name = "FanTrendDetectorSignal";
   type = ENUM_SIGNAL_TYPE_CONFIRMATION;
   timeFrame = _timeFrame;

   handler = iCustom(_Symbol, _Period, "Vendors/FanTrendDetector");
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult FanTrendDetectorSignal::getSignal()
{
   result.reset();

   Buffer buffer = getBuffer(0, 0, 1, true);

   int res = (int)buffer.values[0];

   if (res > 0) {
      result.buy = true;
   }

   if (res < 0) {
      result.sell = true;
   }

   return result;
}
//+------------------------------------------------------------------+
