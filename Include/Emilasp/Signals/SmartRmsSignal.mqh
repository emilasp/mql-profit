//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class SmartRmsSignal: public ASignalBase
{
private:
protected:

public:

                     SmartRmsSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade);

   SignalResult      getSignal();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SmartRmsSignal::SmartRmsSignal(ENUM_TIMEFRAMES _timeFrame, CoreTradeHadge * _trade)
   : ASignalBase()
{
   name = "SmartTrmsSignal";
   trade = _trade;
   type = ENUM_SIGNAL_TYPE_ENTER;
   timeFrame = _timeFrame;

   handler = iCustom(_Symbol, _Period, "Vendors/smartmrs");
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult SmartRmsSignal::getSignal()
{
   result.reset();

   bool isSignalBuy = false;
   bool isSignalSell = false;

////// Ketler ////////
   double IndUpper[];
   ArraySetAsSeries(IndUpper, true);
   CopyBuffer(handler, 0, 0, 3, IndUpper);
   double IndLower[];
   ArraySetAsSeries(IndLower, true);
   CopyBuffer(handler, 1, 0, 3, IndLower);

// BUY
   if ((IndUpper[1] > 0 && IndUpper[2] > 0 || (IndUpper[0] <= 0 && IndUpper[1] <= 0 && IndUpper[2] > 0)) && IndUpper[0] <= 0) {
      isSignalSell = true;
   }
// SELL
   if ((IndLower[1] > 0 && IndLower[2] > 0 || (IndLower[0] <= 0 && IndLower[1] <= 0 && IndLower[2] > 0)) && IndLower[0] <= 0) {
      isSignalBuy = true;
   }

   if (isSignalBuy) {
      result.buy = true;
   }

   if (isSignalSell) {
      result.sell = true;
   }

   return result;
}
//+------------------------------------------------------------------+
