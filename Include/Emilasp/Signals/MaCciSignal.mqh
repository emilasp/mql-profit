//+------------------------------------------------------------------+
//|                              ConsolidationAlphaNeurolPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

//#include <stderror.mqh>
//#include <stdlib.mqh>


#include <Emilasp\Signals\Base\ASignalBase.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class MaCciSignal : public ASignalBase
  {
private:

protected:
   double               upLine;
   double               dwnLine;

public:

                     MaCciSignal(ENUM_TIMEFRAMES _timeFrame, int period);

   SignalResult      getSignal();

   //void              setSignalsSettings(ENUM_ENTER_TYPE enterType, ENUM_EXIT_TYPE exitType);
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MaCciSignal::MaCciSignal(ENUM_TIMEFRAMES _timeFrame, int period)
   : ASignalBase()
  {
   name = "MaCciSignal";
   timeFrame = _timeFrame;


   handler = iMA(_Symbol, _Period, period, 0,MODE_SMA, PRICE_CLOSE);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalResult MaCciSignal::getSignal()
  {
   result.reset();


   MqlTick           lastTick;
   SymbolInfoTick(_Symbol, lastTick);

   Buffer buffer = getBuffer(0, 0, 1, true);

   if(buffer.values[0] < lastTick.ask)
     {
      result.buy = true;
     }
   if(buffer.values[0] > lastTick.ask)
     {
      result.sell = true;
     }

   return result;
  }
//+------------------------------------------------------------------+
