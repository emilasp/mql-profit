//+------------------------------------------------------------------+
//|                                                  CDictionary.mqh |
//|                                 Copyright 2015, Vasiliy Sokolov. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Vasiliy Sokolov."
#property link      "http://www.mql5.com"
#property strict
#include <Object.mqh>
#include <Arrays\ArrayString.mqh>
//+------------------------------------------------------------------+
//| Контейнер для хранения элементов CObject                         |
//+------------------------------------------------------------------+
class DictionaryString : public CObject
{
private:
   CArrayString      keys;
   CArrayString      values;

public:
   void              Add(string key, string value)
   {
      Remove(key);
      keys.Add(key);
      values.Add(value);
   }

   void              AddBool(string key, bool value)
   {
      if(value)
         Add(key, "true");
      else
         Add(key, "false");
   }
   void              AddInt(string key, int value)
   {
      Add(key, IntegerToString(value));
   }
   void              AddDouble(string key, double value)
   {
      Add(key, DoubleToString(value));
   }

   void              Update(string key, string value)
   {
      Add(key,value);
   }

   string            Get(string key)
   {
      for(int i=0;i<keys.Total();i++)
         if(key == keys[i])
            return values.At(i);
   
      return "";
   }


   bool               GetAsBool(string key)
   {
      if(Get(key) == "true")
         return true;
      return false;
   }
   int               GetAsInt(string key)
   {
      return StringToInteger(Get(key));
   }
   double            GetAsDouble(string key)
   {
      return StringToDouble(Get(key));
   }

   string            AtKey(int index)
   {
      return keys.At(index);
   }

   string            AtValue(int index)
   {
      return values.At(index);
   }

   int               Total()
   {
      return values.Total();
   }

   void              Remove(string key)
   {
      int position = keys.SearchFirst(key);

      if(position != -1) {
         keys.Delete(position);
         values.Delete(position);
      }
   }

   string            ToString(string keyValueSeparate = ":", string pairSeparate = ",")
   {
      int total = keys.Total();
      string data = "";
      for(int i=0; i<total; i++) {
         data += keys.At(i) + keyValueSeparate + values.At(i);
         if((i+1) < total)
            data += pairSeparate;
      }
      return data;
   }
};
//+------------------------------------------------------------------+
