//+------------------------------------------------------------------+
//|                                                      Request.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+



#import "wininet.dll"
int InternetAttemptConnect(int x);
int InternetOpenW(string sAgent,int lAccessType,string sProxyName="",string sProxyBypass="",uint lFlags=0);
int InternetOpenUrlW(int hInternetSession,string sUrl,string sHeaders="",int lHeadersLength=0,uint lFlags=0,int lContext=0);
int InternetReadFile(int hFile,uchar &sBuffer[],int lNumBytesToRead,int &lNumberOfBytesRead[]);
int HttpOpenRequestW(int hConnect,string Verb,string Object_Name,string Version,string Referer,int AcceptTypes,uint dwFlags,int dwContext);
int HttpSendRequestW(int hRequest,string lpszHeaders,int dwHeadersLength,uchar &lpOptional[],int dwOptionalLength);
int InternetConnectW(int hInternetSession,string lpszServerName,int nServerPort,string lpszUserName,string lpszPassword,int nService,int nFlags,int dwContext);
int InternetCloseHandle(int hInet);
#import

#define INTERNET_SERVICE_HTTP	        3           // сервис Http
#define INTERNET_FLAG_PRAGMA_NOCACHE  0x00000100  // не кешировать страницу
#define INTERNET_FLAG_RELOAD			  0x80000000  // получать страницу с сервера при обращении к ней
#define INTERNET_FLAG_KEEP_CONNECTION 0x00400000  // не разрывать соединение
#define OPEN_TYPE_PRECONFIG           0           // использовать конфигурацию по умолчанию
#define MOVEFILE_REPLACE_EXISTING 0x1


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string makeRequest(string Host, string PathPHP, string Data) 
 {  
//--- инициализация параметров 
  string Vers         = "HTTP/1.1"; 
  string nill         = "";
  string TypeRequest  = "POST";                 
  //bool   toFile       = false;                 // Флаг чтения данных из инета в файл
  bool   toString     = true;                    // Флаг чтения данных из инета в переменную
  string Head         = "Content-Type: application/json"; // заголовок
  int    hConnect     = 0;
  int    hSession     = 0;
  int    Port         = 80;
  string User         = "";
  string Pass         = "";
  int    Service      = INTERNET_SERVICE_HTTP;
  string Data_In      = "";
  
  //--- Массив для запроса POST
  uchar  Data_Out[];
  ArrayInitialize(Data_Out,0);
	 		  
   if (TypeRequest == "GET")  { PathPHP = PathPHP+"?"+Data;}
   if (TypeRequest == "POST") { StringToCharArray(Data,Data_Out,0,StringLen(Data));}		  
   
   if (Open_Connect(Host, Port, User, Pass, Service, hConnect, hSession) == false) return ("");	
   
//--- создаем дескриптор запроса
   int hRequest=HttpOpenRequestW(hConnect,TypeRequest,PathPHP,Vers,nill,0,INTERNET_FLAG_KEEP_CONNECTION|INTERNET_FLAG_RELOAD|INTERNET_FLAG_PRAGMA_NOCACHE,0);
   if (hRequest<=0){Print ("Ошибка обновления данных !!!"); Close_Connect(hConnect,hSession); return("");}
   //else Print ("Запрос открыт hRequest = ",hRequest);

//--- отправляем запрс   
   int hSend=HttpSendRequestW(hRequest,Head,StringLen(Head),Data_Out,ArraySize(Data_Out));
   if(hSend<=0) { Print ("Ошибка обновления данных !!!"); InternetCloseHandle(hRequest); Close_Connect(hConnect,hSession); return(""); }
   
   if(hSend>0) Data_In = ReadPage(hRequest);
//--- закрываем все хендлы     
     InternetCloseHandle(hRequest); //Print ("Запрос HttpOpenRequestW закрыт hRequest = ",hRequest);  
     Close_Connect(hConnect,hSession);
   return (Data_In);
 } 
 
 //+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Open_Connect(string host,int port,string user, string pass, int service, int &connect, int &session)
{
//---Проверка на соединения с интернетом   
   int IntAtCon = InternetAttemptConnect(0); //проверим наличие интернет соединения
   if(IntAtCon!=0){ Print("Нет связи c интернетом !!!"); return(false) ; }

//---Открытие сессии
   session = InternetOpenW("My", OPEN_TYPE_PRECONFIG, "", "", 0); 
   if (session <=0 ) {Print ("Ошибка получения данных !!!"); return(false);}
 //  else Print ("Сессия открыта InetHandle = ",hSession);

//---Подключение к серверу 
    connect = InternetConnectW(session, host, port, user, pass, service, 0, 0); 
    if (connect<=0) {Print ("Ошибка получения данных !!!"); InternetCloseHandle(session); return(false);}
  //  else Print ("Подключение InternetConnectW прошло успешно intH = ",hConnect);
 return (true);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Close_Connect(int connect, int session)
{
     InternetCloseHandle(connect);// Print ("Подключение InternetConnectW отключено hConnect = ",hConnect); 
     InternetCloseHandle(session);// Print ("Сессия закрыта hSession = ",hSession);
return;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string ReadPage(int hRequest)
{  
   string Out_data = "";
	uchar in_data[1024];
	ArrayInitialize(in_data,0);
	int dwBytesRead[1];
   ArrayInitialize(dwBytesRead,0); 
	string to_str_data=""; 
	int h=-1;
	
	while(InternetReadFile(hRequest, in_data, 1024, dwBytesRead)) 
	{
	   if (dwBytesRead[0] <= 0) break;
		string str = CharArrayToString(in_data, 0, dwBytesRead[0],CP_UTF8);
		to_str_data += str;
 	}
 	
	Out_data = to_str_data;
	Print ("OUT = ", Out_data);
	return(Out_data);
}

string UrlEncode(const string text)
     {
      string result=NULL;
      int length=StringLen(text);
      for(int i=0; i<length; i++)
        {
         ushort ch=StringGetCharacter(text,i);

         if((ch>=48 && ch<=57) || // 0-9
            (ch>=65 && ch<=90) || // A-Z
            (ch>=97 && ch<=122) || // a-z
            (ch=='!') || (ch=='\'') || (ch=='(') ||
            (ch==')') || (ch=='*') || (ch=='-') ||
            (ch=='.') || (ch=='_') || (ch=='~')
            )
           {
            result+=ShortToString(ch);
           }
         else
           {
            if(ch==' ')
               result+=ShortToString('+');
            else
              {
               uchar array[];
               int total=ShortToUtf8(ch,array);
               for(int k=0;k<total;k++)
                  result+=StringFormat("%%%02X",array[k]);
              }
           }
        }
      return result;
    }

int ShortToUtf8(const ushort _ch,uchar &out[])
{
      //---
      if(_ch<0x80)
        {
         ArrayResize(out,1);
         out[0]=(uchar)_ch;
         return(1);
        }
      //---
      if(_ch<0x800)
        {
         ArrayResize(out,2);
         out[0] = (uchar)((_ch >> 6)|0xC0);
         out[1] = (uchar)((_ch & 0x3F)|0x80);
         return(2);
        }
      //---
      if(_ch<0xFFFF)
        {
         if(_ch>=0xD800 && _ch<=0xDFFF)//Ill-formed
           {
            ArrayResize(out,1);
            out[0]=' ';
            return(1);
           }
         else if(_ch>=0xE000 && _ch<=0xF8FF)//Emoji
           {
            int ch=0x10000|_ch;
            ArrayResize(out,4);
            out[0] = (uchar)(0xF0 | (ch >> 18));
            out[1] = (uchar)(0x80 | ((ch >> 12) & 0x3F));
            out[2] = (uchar)(0x80 | ((ch >> 6) & 0x3F));
            out[3] = (uchar)(0x80 | ((ch & 0x3F)));
            return(4);
           }
         else
           {
            ArrayResize(out,3);
            out[0] = (uchar)((_ch>>12)|0xE0);
            out[1] = (uchar)(((_ch>>6)&0x3F)|0x80);
            out[2] = (uchar)((_ch&0x3F)|0x80);
            return(3);
           }
        }
      ArrayResize(out,3);
      out[0] = 0xEF;
      out[1] = 0xBF;
      out[2] = 0xBD;
      return(3);
}