//+------------------------------------------------------------------+
//|                                                  CDictionary.mqh |
//|                                 Copyright 2015, Vasiliy Sokolov. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Vasiliy Sokolov."
#property link      "http://www.mql5.com"
#property strict
#include <Object.mqh>
#include <Arrays\ArrayString.mqh>
#include <Arrays\ArrayObj.mqh>
//+------------------------------------------------------------------+
//| Контейнер для хранения элементов CObject                         |
//+------------------------------------------------------------------+
class DictionaryObj : public CObject
{
private:
   CArrayString      keys;
   CArrayObj         values;

public:
   void              add(string key, CObject *value)
   {
      remove(key);
      keys.Add(key);
      values.Add(value);
   }


   void              update(string key, CObject *value)
   {
      add(key,value);
   }

   CObject * get(string key)
   {
      CObject *value;
      for(int i=0;i<keys.Total();i++)
         if(key == keys[i])
            return values.At(i);
   
      return value;
   }

   bool           isset(string name)
   {
      return keys.Search(name) != -1;
   }

   string            atKey(int index)
   {
      return keys.At(index);
   }

   CObject *atValue(int index)
   {
      return values.At(index);
   }

   int               total()
   {
      return values.Total();
   }

   void              remove(string key)
   {
      int position = keys.SearchFirst(key);

      if(position != -1) {
         keys.Delete(position);
         values.Delete(position);
      }
   }
};
//+------------------------------------------------------------------+
