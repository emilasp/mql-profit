//+------------------------------------------------------------------+
//|                                                  CDictionary.mqh |
//|                                 Copyright 2015, Vasiliy Sokolov. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Vasiliy Sokolov."
#property link      "http://www.mql5.com"
#property strict


#include <Emilasp\libraries\Request.mqh>
#include <Emilasp\libraries\json\JAson.mqh>

#import "Dll2.dll"
string  Request(string serv, string page, string content);
#import


/*
Trading_signals_Emil
ChatId: 
id[from]:
https://api.telegram.org/bot{$bot}/sendMessage
GET https://api.telegram.org/bot2{token}/sendMessage?chat_id={chatId}&text={text}
*/

class TelegramChat
{
private:
   string botToken;
   string chatId;

public:
   void TelegramChat(string _botToken, string _chatId) {
      botToken=_botToken;
      chatId=_chatId;
   }

   string sendMessage(string text) {
         return makeRequest("api.telegram.org", "bot" + botToken + "/sendMessage?&parse_mode=markdown&chat_id=" + chatId + "&text=" + text, "{}");
   }
};
//+------------------------------------------------------------------+
