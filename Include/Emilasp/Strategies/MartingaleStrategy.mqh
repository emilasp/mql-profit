//+------------------------------------------------------------------+
//|                                             MartingaleStrategy.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\CoreTrade.mqh>
#include <Trade\DealInfo.mqh>

class MartingaleStrategy
  {
   private:          
      CoreTrade     *m_CoreTrade;
      CSymbolInfo    a_symbol;
      CPositionInfo  a_position;
      
      double         Lots;
      double         Multiplier;
      ushort         Step;
      double         Profit;
      double eStep;
      
      double last_price;
      int last_pos_type;
      double last_lots;
      ulong MagicNumber;

      
   public:
      MartingaleStrategy(CoreTrade *m_coreTrade, ulong magicNumber, double lots, double multiplier, ushort step, double profit, double estep);
      ~MartingaleStrategy();
  
       virtual void onTickExpert(bool isBuy, bool isSell);
       virtual void OnTradeTransactionExpert(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result);
       
       virtual bool OpenBuy(double alot);
       virtual bool OpenSell(double alot);
       
       virtual double CalcLots(double lots);
       virtual double CalcProfit(ENUM_POSITION_TYPE pos_type);
       virtual void CloseAll(ENUM_POSITION_TYPE pos_type);
  };
     
     
MartingaleStrategy::MartingaleStrategy(CoreTrade *m_coreTrade, ulong magicNumber, double lots, double multiplier, ushort step, double profit, double estep)
{    
   MagicNumber  = magicNumber;
   m_CoreTrade  = m_coreTrade;
   a_symbol     = m_CoreTrade.m_symbol;
   Lots         = lots;
   Multiplier   = multiplier;
   Step         = step;
   Profit       = profit;
   
   
   last_price     = 0;
   last_pos_type  = -1;
   last_lots      = 0;
   eStep          = estep;
   
}    
void MartingaleStrategy::onTickExpert(bool isBuy, bool isSell) 
{
   if (CalcProfit(POSITION_TYPE_BUY) >= Profit)
      CloseAll(POSITION_TYPE_BUY);

   if (CalcProfit(POSITION_TYPE_SELL) >= Profit)
      CloseAll(POSITION_TYPE_SELL);

   int count = m_CoreTrade.PosCount();

   // Это первый вход в рынок
   if(count < 1)
   {
      if (isBuy) {
         OpenBuy(Lots);
         return;
      }
      if (isSell) {
         OpenSell(Lots);
         return;
      }
   }

    double ask = a_symbol.Ask();
    double toBuy = last_price + eStep;
    double toSell = last_price - eStep;
    
   
   if (last_price != 0 && last_pos_type >=0 && last_lots != 0){
      // Если уже есть позиции buy - доливаем
      if (last_pos_type == POSITION_TYPE_BUY){ 
         if (a_symbol.Ask() <= last_price - eStep)
         {
            double next_lots = CalcLots(last_lots * Multiplier);

            if (next_lots != 0) {
               CloseAll(POSITION_TYPE_BUY);
               OpenSell(next_lots);
            }
         }
      }

      // Если уже есть позиции sell - доливаем
      if (last_pos_type == POSITION_TYPE_SELL){
        
      
         // Если цена ушла вниз, то рассчитываем новый объем и доливаем
         if (a_symbol.Ask() >= last_price + eStep)
         {
            double next_lots = CalcLots(last_lots * Multiplier);

            if (next_lots != 0) {
               CloseAll(POSITION_TYPE_SELL);
               OpenBuy(next_lots);
            }
         }
      }
   }
}
     
void MartingaleStrategy::OnTradeTransactionExpert(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   // Если происходит добавление позиции в рынок
   // Проверяем, что позиция наша(могут быть ручные позиции или другой советник работает)
   if (trans.type == TRADE_TRANSACTION_DEAL_ADD)
   {
      long deal_type  = -1;
      long deal_entry = -1;
      long deal_magic = 0;

      double deal_volume = 0;
      double deal_price  = 0;
      string deal_symbol = "";
 
      CDealInfo      m_deal;  
      if(HistoryDealSelect(trans.deal) == true)
      {
         m_deal.Ticket(trans.deal);
         
         deal_magic = m_deal.Magic();
         deal_type = m_deal.Type();
         deal_entry = m_deal.Entry();
         
         deal_volume = m_deal.Volume();
         deal_price  = m_deal.Price();
         deal_symbol = m_deal.Symbol();
      } else {
         return;
      }
      
      
      if (deal_symbol == a_symbol.Name() && deal_magic == MagicNumber){ 
         if (deal_entry == DEAL_ENTRY_IN && (deal_type == DEAL_TYPE_BUY || deal_type == DEAL_TYPE_SELL)) {
            last_pos_type  = (deal_type == DEAL_TYPE_BUY) ? POSITION_TYPE_BUY : POSITION_TYPE_SELL;
            last_price     = deal_price;
            last_lots      = deal_volume;
         } else if (deal_entry == DEAL_ENTRY_OUT) {// Эта сделка выход, то сбрасываем переменные
            last_pos_type  = -1;
            last_price     = 0;
            last_lots      = 0;
         }
      }
   }
}     
 
/**
* Open buy
**/
bool MartingaleStrategy::OpenBuy(double alot)
{
   if (alot == 0){
      Print("Error lot = 0");
      return false;
   }
   
   int    digits=(int)SymbolInfoInteger(a_symbol.Name(),SYMBOL_DIGITS); // количество знаков после запятой
   double point=SymbolInfoDouble(a_symbol.Name(),SYMBOL_POINT);         // пункт
   double bid=SymbolInfoDouble(a_symbol.Name(),SYMBOL_BID);  // текущая цена для закрытия LONG
   double open_price=SymbolInfoDouble(a_symbol.Name(),SYMBOL_ASK);//--- получим текущую цену открытия для LONG позиций
   
   // Set SL and TP
   double stopLoss = 50;
   double SL = 0;
   double TP = 0;           
   if (stopLoss > 0) {
      SL = bid - stopLoss * point;                                   // ненормализованное значение SL
      SL = NormalizeDouble(SL,digits);                              // нормализуем Stop Loss
   }
   

   if (m_CoreTrade.m_Trade.Buy(alot, a_symbol.Name(),a_symbol.Ask(), 0, 0)) {
      if (m_CoreTrade.m_Trade.ResultDeal() == 0) {
         Print("Ошибка открытия позиции на покупку");
         return false;
      }
   }
   return true;
}
/**
* Open sell
**/
bool MartingaleStrategy::OpenSell(double alot)
{
   if (alot == 0){
      Print("Error lot = 0");
      return false;
   }
   
   int    digits=(int)SymbolInfoInteger(a_symbol.Name(),SYMBOL_DIGITS); // количество знаков после запятой
   double point=SymbolInfoDouble(a_symbol.Name(),SYMBOL_POINT);         // пункт
   double bid=SymbolInfoDouble(a_symbol.Name(),SYMBOL_BID);  // текущая цена для закрытия LONG
   double open_price=SymbolInfoDouble(a_symbol.Name(),SYMBOL_ASK);//--- получим текущую цену открытия для LONG позиций
   
   // Set SL and TP
   double stopLoss = 5;
   double SL = 0;
   double TP = 0;           

   if (stopLoss > 0) {
      SL = bid + stopLoss * point;                                   // ненормализованное значение SL
      SL = NormalizeDouble(SL,digits);                              // нормализуем Stop Loss
   }
   Print("SL: " + SL);
    if (m_CoreTrade.m_Trade.Sell(alot, a_symbol.Name(),a_symbol.Bid(), 0, 0)) {
      if (m_CoreTrade.m_Trade.ResultDeal() == 0) {
         Print("Ошибка открытия позиции на продажу");
         return false;
      }
   }
   return true;
}    


/**
* Закрываем позиции
**/
void MartingaleStrategy::CloseAll(ENUM_POSITION_TYPE pos_type)
{
   for(int i=0;i<PositionsTotal();i++) {
      if (a_position.SelectByIndex(i)) {
         if (a_position.PositionType() == pos_type && a_position.Magic() == MagicNumber && a_position.Symbol() == a_symbol.Name()) {
            m_CoreTrade.m_Trade.PositionClose(a_position.Ticket());
         }
      }
   }
}


/**
* Вычисляем профит
**/
double MartingaleStrategy::CalcProfit(ENUM_POSITION_TYPE pos_type)
{
   //Print("CalcProfit start");
   double profit = 0;

   for(int i=0;i<PositionsTotal();i++) {
      //Print("CalcProfit begin: " + i);
      if (a_position.SelectByIndex(i)) {
         string sym1 = a_position.Symbol();
         string sym2 = a_symbol.Name();
      
         if (a_position.PositionType() == pos_type && a_position.Magic() == MagicNumber && a_position.Symbol() == a_symbol.Name()) {
            profit += a_position.Profit();
         }
      }
   }
   //Print("CalcProfit end");
   return profit;
}

/**
* Вычисляем следующий объем
**/
double MartingaleStrategy::CalcLots(double lots)
{
   double new_lots = NormalizeDouble(lots, 2);
   double step_lots = a_symbol.LotsStep();

   if (step_lots > 0)
      new_lots = step_lots * MathFloor(new_lots / step_lots);

   if (new_lots < a_symbol.LotsMin())
      new_lots = a_symbol.LotsMin();

   if (new_lots > a_symbol.LotsMax())
      new_lots = a_symbol.LotsMax();

   return new_lots;
}