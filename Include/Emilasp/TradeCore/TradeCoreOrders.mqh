//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2020, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, Emilasp."
#property link      ""

#include <Arrays\ArrayObj.mqh>
#include <MQL_Easy/MQL_Easy.mqh>

#include <Emilasp\TradeCore\TcOrder.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TradeCoreOrders
{
private:
   string symbol;
   int magic;
   int deviation;

   COrder s_orders;
   CExecute s_execute;

   CArrayObj orders;

public:
      int refresh();

   int Total(ENUM_POSITION_TYPE type = NULL, ENUM_TC_ORDER_TYPE orderType = ORDER_TYPE_NONE)
   {
      int count = 0;
      
      int total = s_orders.GroupTotal();
      
       for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if ((order.positionType == type || type == NULL) && (order.type == orderType || orderType == NULL)) {
            count++;
         }
      }
      return count;
   }

   long BuyStop(double volume, double price, double sl=0.000000, double tp=0.000000, ENUM_SLTP_TYPE sltpType=0, datetime expiration=0, string comment=NULL)
   {
      return s_execute.Order(TYPE_ORDER_BUYSTOP, volume, price, sl, tp, sltpType, expiration, deviation, comment);
   }
   long SellStop(double volume, double price, double sl=0.000000, double tp=0.000000, ENUM_SLTP_TYPE sltpType=0, datetime expiration=0, string comment=NULL)
   {
      return s_execute.Order(TYPE_ORDER_SELLSTOP, volume, price, sl, tp, sltpType, expiration, deviation, comment);
   }
   long BuyLimit(double volume, double price, double sl=0.000000, double tp=0.000000, ENUM_SLTP_TYPE sltpType=0, datetime expiration=0, string comment=NULL)
   {
      return s_execute.Order(TYPE_ORDER_BUYLIMIT, volume, price, sl, tp, sltpType, expiration, deviation, comment);
   }
   long SellLimit(double volume, double price, double sl=0.000000, double tp=0.000000, ENUM_SLTP_TYPE sltpType=0, datetime expiration=0, string comment=NULL)
   {
      return s_execute.Order(TYPE_ORDER_SELLLIMIT, volume, price, sl, tp, sltpType, expiration, deviation, comment);
   }

   TcOrder *getLastOrder()
   {
      if (orders.Total() > 0) {
         return orders.At(0);
      } else {
         TcOrder *order = new TcOrder();
         order.isEmpty = true;
         return order;
      }
   }
   
   TcOrder *getOrderByTicket(long ticket)
   {
      for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if (order.ticket == ticket) {
            return order;
         }
      }

      TcOrder *order = new TcOrder();
      order.isEmpty = true;
      return order;
   }

   int closeAll(ENUM_TC_ORDER_TYPE orderType, ENUM_POSITION_TYPE positionType = NULL)
   {
      int count = 0;
      for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if (order.type == orderType) {
            if (positionType == NULL || positionType == order.positionType) {
               orderClose(order);
               count++;
            }
         }
      }
      return count;
   }

   bool orderClose(TcOrder &order, uint tries = 20)
   {
      s_orders.SelectByTicket(order.ticket);
      s_orders[0].Close(tries);
      return true;
   }

   bool orderModify(double priceOpen = WRONG_VALUE, double sl = WRONG_VALUE, double tp = WRONG_VALUE, ENUM_SLTP_TYPE sltpPar = SLTP_PRICE, datetime expiration = WRONG_VALUE)
   {
      return s_orders.Modify(priceOpen, sl, tp, sltpPar, expiration);
   }


   //double getTotalVolume(ENUM_TC_ORDER_TYPE orderType);

   string getLastError()
   {
      return s_orders.Error.GetLastErrorCustom();
   }

   void TradeCoreOrders(string _symbol, int _magic, int _deviation)
   {
      symbol = _symbol;
      magic = _magic;
      deviation=_deviation;

      s_orders.SetGroupSymbol(symbol);
      s_orders.SetGroupMagicNumber(magic);

      s_execute.SetSymbol(symbol);
      s_execute.SetMagicNumber(magic);
   }

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int TradeCoreOrders::refresh()
{
   orders.Clear();

   //-- return the total number of active trades with specific symbol, magic number and type,
   //-- ignoring all the others
   int total = s_orders.GroupTotal();

   //-- iterate through specific group of trades
   for(int i = 0; i < total; i++) {
      //-- select a position by its index
      if(s_orders.SelectByIndex(i)) {
         TcOrder *order = new TcOrder();
         order.type = ORDER_TYPE_POSITION; //ORDER_TYPE
         order.positionType = s_orders.GetType();
         order.closeType = ORDER_CLOSE_TYPE_NONE;

         order.ticket = s_orders.GetTicket();
         order.volume = s_orders.GetVolume();
         order.timeOpen = s_orders.GetTimeSetUp();
         order.priceOpen = s_orders.GetPriceOpen();
         order.sl = s_orders.GetStopLoss();
         order.tp = s_orders.GetTakeProfit();
         //order.swap = s_orders.GetSwap();
         order.comment = s_orders.GetComment();

         orders.Add(order);
      } else {
         //-- if the position is not selected a default message error with user-friendly description
         //-- will be displayed in the expert tab
         //-- also, you can retrieve the error information
         int errorCode = s_orders.Error.GetLastErrorCode();
         //-- make custom actions
      }
   }
   return 1;
}

//+------------------------------------------------------------------+
