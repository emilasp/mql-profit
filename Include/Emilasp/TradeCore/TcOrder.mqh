//+------------------------------------------------------------------+
//|                                                    CoreTrade.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>

enum ENUM_TC_ORDER_TYPE {
   ORDER_TYPE_NONE = 0,
   ORDER_TYPE_POSITION = 1,
   ORDER_TYPE_ORDER_STOP = 2,
   ORDER_TYPE_ORDER_LIMIT = 3
};

enum ENUM_TC_ORDER_CLOSE_TYPE {
   ORDER_CLOSE_TYPE_NONE = 0,
   ORDER_CLOSE_TYPE_CANCEL = 1,
   ORDER_CLOSE_TYPE_SL = 2,
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TcOrder : public CObject
{
private:

public:
   ENUM_TC_ORDER_TYPE type;
   ENUM_POSITION_TYPE positionType;
   ENUM_TC_ORDER_CLOSE_TYPE closeType;
   bool isHistory;
   bool isEmpty;

   long ticket;
   double volume;
   datetime timeOpen;
   double priceOpen;
   datetime timeClose;
   double priceClose;
   double sl;
   double tp;
   string comment;

   double swap;
   double profit;

   int step;

   void TcOrder()
   {
      isHistory = false;
      isEmpty = false;
      step=0;
   }

};
//+------------------------------------------------------------------+
