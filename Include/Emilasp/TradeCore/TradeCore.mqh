#property copyright "Copyright 2022, Emilasp."
#property link      ""

#include <Arrays\ArrayObj.mqh>
#include <MQL_Easy/MQL_Easy.mqh>


class TradeCore
{
    private:
        string symbol;
        int magic;
        int deviation;
        
        CExecute s_execute;
        CPosition s_position;
        COrder s_order;
        CPrinter s_printer;       
        
        CArrObject positions;
        CArrObject orders;
        CArrObject historyAll;
         
        int refreshHistory();
        int refreshPositions();
        int refreshOrders();
         
    public:
        int Total(ENUM_TC_ORDER_TYPE orderType, POSITION_TYPE type = NULL);
        
        long Buy(double volume, double sl, double tp, ENUM_SLTP_TYPE sltpType, comment) {
            long ticket = s_execute.Position(POSITION_TYPE_BUY,volume,sl,tp,sltpType,deviation,comment);
            refreshPositions();
        }
        long Sell(double volume, double sl, double tp, ENUM_SLTP_TYPE sltpType, comment) {
            long ticket = s_execute.Position(POSITION_TYPE_SELL,volume,sl,tp,sltpType,deviation,comment);
            refreshPositions();
        }
        long BuyStop(double volume,double price,double sl=0.000000,double tp=0.000000,ENUM_SLTP_TYPE sltpType=0,datetime expiration=0,string comment=NULL) {
            s_execute.Order(TYPE_ORDER_BUYSTOP,volume,price,sl,tp,sltpType,expiration,deviation,comment);
            refreshOrders();
        }
        long SellStop(double volume,double price,double sl=0.000000,double tp=0.000000,ENUM_SLTP_TYPE sltpType=0,datetime expiration=0,string comment=NULL) {
            s_execute.Order(TYPE_ORDER_SELLSTOP,volume,price,sl,tp,sltpType,expiration,deviation,comment);
            refreshOrders();
        }
        long BuyLimit(double volume,double price,double sl=0.000000,double tp=0.000000,ENUM_SLTP_TYPE sltpType=0,datetime expiration=0,string comment=NULL) {
            s_execute.Order(TYPE_ORDER_BUYLIMIT,volume,price,sl,tp,sltpType,expiration,deviation,comment);
            refreshOrders();
        }
        long SellLimit(double volume,double price,double sl=0.000000,double tp=0.000000,ENUM_SLTP_TYPE sltpType=0,datetime expiration=0,string comment=NULL) {
            s_execute.Order(TYPE_ORDER_SELLLIMIT,volume,price,sl,tp,sltpType,expiration,deviation,comment);
            refreshOrders();
        }
        
        
        TcOrder *getOrderByTicket(long ticket) {
                for (int i=0; i<s_position.Total();i++) {
                    TcOrder *order = s_position.At(i);
                    if (order.ticket == ticket) {
                        return order;
                    }
                }
                for (int i=0; i<orders.Total();i++) {
                    TcOrder *order = orders.At(i);
                    if (order.ticket == ticket) {
                        return order;
                    }
                }
            for (int i=0; i<historyAll.Total();i++) {
                TcOrder *order = orders.At(i);
                if (order.ticket == ticket) {
                    return order;
                }
            }
            
            TcOrder *order = new TcOrder();
            order.isEmpty = true;
            return order;
        }
        
        int closeAll(ENUM_TC_ORDER_TYPE orderType, POSITION_TYPE positionType = NULL) {
            if (orderType == ORDER_TYPE_POSITION || orderType == NULL) {
                for (int i=0; i<positions.Total();i++) {
                    TcOrder *order = orders.At(i);
                    if (positionType == NULL || positionType == order.positiontype) {
                        orderClose(order);
                    }
                }
            } 
            if (orderType != ORDER_TYPE_POSITION || orderType == NULL) {
                for (int i=0; i<orders.Total();i++) {
                    TcOrder *order = orders.At(i);
                    if (order.type == orderType) {
                        if (positionType == NULL || positionType == order.positiontype) {
                            orderClose(order);
                        }
                    }
                }
            }
        }
        
        bool orderClose(TcOrder &order, double volume = EMPTY_VALUE, uint tries = 20) {
            if (order.type == ORDER_TYPE_POSITION) {
                position.SelectByTicket(order.ticket);
                if (volumePar == EMPTY_VALUE) {
                    s_position[0].Close(tries); 
                } else {
                    s_position[0].ClosePartial(volume, tries); 
                }
            } else {
                s_order.SelectByTicket(order.ticket);
                if (volumePar == EMPTY_VALUE) {
                    s_order[0].Close(tries); 
                } 
            }
        }
        
        bool orderModify(double stopLossPar = WRONG_VALUE, double takeProfitPar = WRONG_VALUE, ENUM_SLTP_TYPE sltpPar = SLTP_PRICE) {
            (double priceOpenPar = WRONG_VALUE,double stopLossPar = WRONG_VALUE,double takeProfitPar = WRONG_VALUE,
                                       ENUM_SLTP_TYPE sltpPar = SLTP_PRICE, datetime expirationPar = WRONG_VALUE)
        }
        
        bool orderModify(double stopLossPar = WRONG_VALUE, double takeProfitPar = WRONG_VALUE, ENUM_SLTP_TYPE sltpPar = SLTP_PRICE) {
            (double priceOpenPar = WRONG_VALUE,double stopLossPar = WRONG_VALUE,double takeProfitPar = WRONG_VALUE,
                                       ENUM_SLTP_TYPE sltpPar = SLTP_PRICE, datetime expirationPar = WRONG_VALUE)
        }

        
        double getTotalVolume(ENUM_TC_ORDER_TYPE orderType);
        double getTotalProfit(ENUM_TC_ORDER_TYPE orderType);
        string getLastError();
    
    
        void TradeCore(string _symbol, int _magic, int deviation) {
            symbol = _symbol;
            magic = _magic;
            deviation=_deviation;
            
            execute.SetSymbol(symbol);
            execute.SetMagicNumber(magic);
            position.SetGroupSymbol(symbol);
            position.SetGroupMagicNumber(magic);
        }
    
}

int TradeCore::refreshPositions()
{
   CPosition position(symbol,magic, GROUP_POSITIONS_BUYS);
   
   //-- return the total number of active trades with specific symbol, magic number and type, 
   //-- ignoring all the others 
   int totalPositions = position.GroupTotal(); 

   //-- iterate through specific group of trades
   for(int i = 0; i < totalPositions; i++){
      //-- select a position by its index
      if(position.SelectByIndex(i)){
         long ticket = position.GetTicket();
         double openPrice = position.GetPriceOpen();
         datetime openTime = position.GetTimeOpen();
         double profit = position.GetProfit();
         Print("#ticket: "+ticket+", openPrice: "+openPrice+", openTime:  +openTime+", profit:"+profit);
      }else{
         //-- if the position is not selected a default message error with user-friendly description 
         //-- will be displayed in the expert tab
         //-- also, you can retrieve the error information 
         int errorCode = position.Error.GetLastErrorCode();   
         //-- make custom actions      
      }
   }
//-- Examples with group methods
position.GroupTotalProfit(); // returns the total profit of a group 
position.GroupTotalVolume(); // returns the total volume of a group
position.GroupCloseAll(10);  // close all trades of a group. The number 10 is the tries in case of
                             // failure, by default is 20.    
}

int TradeCore::refreshOrders()
{
    
}

#include <Object.mqh>


enum ENUM_TC_ORDER_TYPE {
    ORDER_TYPE_POSITION = 1,
    ORDER_TYPE_ORDER_STOP = 2,
    ORDER_TYPE_ORDER_LIMIT = 3
}

enum ENUM_TC_ORDER_CLOSE_TYPE {
    ORDER_CLOSE_TYPE_CANCEL = 1,
    ORDER_CLOSE_TYPE_SL = 2,
}

class TcOrder : public CObject
{
    private:
        
    public:
        ENUM_TC_ORDER_TYPE type;
        ENUM_POSITION_TYPE positionType;
        ENUM_TC_ORDER_CLOSE_TYPE closeType;
        bool isHistory;
        bool isEmpty;
        
        long ticket;
        double volume;
        datetime timeOpen;
        double priceOpen;
        datetime timeClose;
        double priceClose;
        double sl;
        double tp;
        string comment;
        
        double swap;
        double profit; 
        
        void TcOrder()
        {
            isHistory = false;
            isEmpty = false;
        }
   
}