//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2020, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, Emilasp."
#property link      ""

#include <Arrays\ArrayObj.mqh>
#include <MQL_Easy/MQL_Easy.mqh>
#include <Emilasp\TradeCore\TcOrder.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class TradeCorePositions
{
private:
   string symbol;
   int magic;
   int deviation;

   CPosition s_position;
   CExecute s_execute;

   CArrayObj orders;

public:
      int refresh();

   int Total(ENUM_POSITION_TYPE type = NULL)
   {
      int total = s_position.GroupTotal();
   
         int count = 0;
      for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if (order.positionType == type || type == NULL) {
            count++;
         }
      }
      return count;
   }

   long Buy(double volume, double sl, double tp, ENUM_SLTP_TYPE sltpType, string comment = NULL)
   {
      return s_execute.Position(TYPE_POSITION_BUY, volume, sl, tp, sltpType, deviation, comment);
   }
   long Sell(double volume, double sl, double tp, ENUM_SLTP_TYPE sltpType, string comment = NULL)
   {
      return s_execute.Position(TYPE_POSITION_SELL, volume, sl, tp, sltpType, deviation, comment);
   }

   TcOrder *getLastOrder()
   {
      if (orders.Total() > 0) {
         return orders.At(0);
      } else {
         TcOrder *order = new TcOrder();
         order.isEmpty = true;
         return order;
      }
   }
   
   TcOrder *getOrderByTicket(long ticket)
   {
      for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if (order.ticket == ticket) {
            return order;
         }
      }

      TcOrder *order = new TcOrder();
      order.isEmpty = true;
      return order;
   }

   int closeAll(ENUM_POSITION_TYPE positionType = NULL)
   {
      for (int i=0; i<orders.Total(); i++) {
         TcOrder *order = orders.At(i);
         if (positionType == NULL || positionType == order.positionType) {
            orderClose(order);
         }
      }
      return 1;
   }

   bool orderClose(TcOrder &order, double volume = EMPTY_VALUE, uint tries = 20)
   {
      s_position.SelectByTicket(order.ticket);
      if (volume == EMPTY_VALUE) {
         s_position[0].Close(tries);
      } else {
         s_position[0].ClosePartial(volume, tries);
      }
      return true;
   }

   bool orderModify(double sl = WRONG_VALUE, double tp = WRONG_VALUE, ENUM_SLTP_TYPE sltpPar = SLTP_PRICE)
   {
      return s_position.Modify(sl, tp, sltpPar);
   }



   double getTotalVolume(ENUM_TC_ORDER_TYPE orderType)
   {
      return s_position.GroupTotalVolume();
   }
   double getTotalProfit(ENUM_TC_ORDER_TYPE orderType)
   {
      return s_position.GroupTotalProfit();
   }

   string getLastError()
   {
      return s_position.Error.GetLastErrorCustom();
   }


   void TradeCorePositions(string _symbol, int _magic, int _deviation)
   {
      symbol = _symbol;
      magic = _magic;
      deviation=_deviation;

      s_position.SetGroupSymbol(symbol);
      s_position.SetGroupMagicNumber(magic);

      s_execute.SetSymbol(symbol);
      s_execute.SetMagicNumber(magic);
   }

};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int TradeCorePositions::refresh()
{
   orders.Clear();

   //-- return the total number of active trades with specific symbol, magic number and type,
   //-- ignoring all the others
   int totalPositions = s_position.GroupTotal();

   //-- iterate through specific group of trades
   for(int i = 0; i < totalPositions; i++) {
      //-- select a position by its index
      if(s_position.SelectByIndex(i)) {
         TcOrder *order = new TcOrder();
         order.type = ORDER_TYPE_POSITION;
         order.positionType = s_position.GetType();
         order.closeType = ORDER_CLOSE_TYPE_NONE;

         order.ticket = s_position.GetTicket();
         order.volume = s_position.GetVolume();
         order.timeOpen = s_position.GetTimeOpen();
         order.priceOpen = s_position.GetPriceOpen();
         order.sl = s_position.GetStopLoss();
         order.tp = s_position.GetTakeProfit();
         order.swap = s_position.GetSwap();
         order.profit = s_position.GetProfit();
         order.comment = s_position.GetComment();

         orders.Add(order);
      } else {
         //-- if the position is not selected a default message error with user-friendly description
         //-- will be displayed in the expert tab
         //-- also, you can retrieve the error information
         int errorCode = s_position.Error.GetLastErrorCode();
         //-- make custom actions
      }
   }
   return 1;
}
//+------------------------------------------------------------------+
