//+------------------------------------------------------------------+
//|                                                  BarsPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\BarsPattern\PrevBarsModel.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class BarsPattern : public CObject {
 private:
 protected:
   string            id;
   string            shortName;
   int               barsIndexFrom;
   CArrayObj         *barObjects;
   PrevBarsModel     *prevBarsModel;

 public:
   bool              isPattern;
   ENUM_POSITION_TYPE positionType;

   double            rectBottomPrice;
   datetime          rectBottomTime;
   double            rectTopPrice;
   datetime          rectTopTime;

                     BarsPattern();
                    ~BarsPattern();
   bool              addBars(int length, int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[]);
   void              drawInfo();
   double            getMinLow();
   double            getMaxHigh();
   virtual void      calculatePattern();
   void              setRectSize();
   Bar               *getFirstBar();
   Bar               *getLastBar();

};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BarsPattern::BarsPattern() {
   shortName = "Не установлено";
   barObjects = new CArrayObj;

   isPattern = false;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BarsPattern::~BarsPattern() {
}
//+------------------------------------------------------------------+
bool BarsPattern::addBars(int length, int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[]) {
   id = shortName + ":" + length + ":" + time[from];
   barsIndexFrom = from;
   for(int i = 0; i < length; i++) {
      Bar *bar = new Bar(time[from - i], open[from - i], close[from - i], high[from - i], low[from - i]);
      barObjects.Add(bar);
   }
   setRectSize();
   return true;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void BarsPattern::setRectSize() {
   if(barObjects.Total() > 1) {
      rectBottomPrice = getMinLow() - 50 * _Point;
      rectBottomTime = getFirstBar().time;
      rectTopPrice = getMaxHigh() + 20 * _Point;
      rectTopTime = getLastBar().time;

      int dateDelta = PeriodSeconds() * .5;
      if(rectBottomTime < rectTopTime) {
         rectBottomTime -= dateDelta;
         rectTopTime += dateDelta;
      } else {
         rectBottomTime += dateDelta;
         rectTopTime -= dateDelta;
      }
   }
}


//+------------------------------------------------------------------+
void BarsPattern::drawInfo() {
   if(isPattern) {
      string nameRect = id + "_rect";
      string nameText = id + "_text";

      string text = shortName;
      if(ObjectFind(0, nameRect) == -1) {
         int colorVal = clrHoneydew;
         int sdvig = 600  * _Point;
         if(positionType == POSITION_TYPE_SELL) {
            colorVal = clrMistyRose;
            sdvig = 600  * _Point;
         }

         ObjectCreate(0, nameRect, OBJ_RECTANGLE, 0, rectBottomTime, rectBottomPrice + sdvig, rectTopTime, rectTopPrice + sdvig);

         ObjectCreate(0, nameText, OBJ_TEXT, 0, rectBottomTime, getMinLow() -  sdvig);

         ObjectSetInteger(0, nameText, OBJPROP_ANCHOR, ANCHOR_LEFT);
         ObjectSetDouble(0, nameText, OBJPROP_ANGLE, -90);
         ObjectSetString(0, nameText, OBJPROP_TEXT, text);
         ObjectSetInteger(0, nameText, OBJPROP_COLOR, clrGold);

         ObjectSetInteger(0, nameRect, OBJPROP_COLOR, colorVal);
         ObjectSetInteger(0, nameRect, OBJPROP_BACK, false);
         ObjectSetInteger(0, nameRect, OBJPROP_WIDTH, 1);
      }
   }
}
//+------------------------------------------------------------------+
double BarsPattern::getMaxHigh() {
   double max = 0;
   for(int i = 0; i < barObjects.Total(); i++) {
      Bar * bar = barObjects.At(i);
      if(max < bar.high)
         max = bar.high;
   }
   return max;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double BarsPattern::getMinLow() {
   double min = DBL_MAX;
   for(int i = 0; i < barObjects.Total(); i++) {
      Bar * bar = barObjects.At(i);
      if(min > bar.low)
         min = bar.low;
   }
   return min;
}
//+------------------------------------------------------------------+
Bar *BarsPattern::getFirstBar() {
   Bar * bar = barObjects.At(0);
   return bar;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Bar *BarsPattern::getLastBar() {
   Bar * bar = barObjects.At(barObjects.Total() - 1);
   return bar;
}
//+------------------------------------------------------------------+
