//+------------------------------------------------------------------+
//|                                              PatternsManager.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayObj.mqh>

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>

/** All patterns **/
#include <Emilasp\BarsPattern\Patterns\AbsorptionPattern.mqh>
#include <Emilasp\BarsPattern\Patterns\PinokioPattern.mqh>
#include <Emilasp\BarsPattern\Patterns\BarEat2PrevBarsPattern.mqh>

class PatternsManager {
 private:
   CArrayObj         *m_patterns;

   int               m_depth;

 public:
                     PatternsManager(int depth);
                    ~PatternsManager();

   void              DrawPattern(BarsPattern &pattern);
   void              SetPatterns();
   void              Calculate();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
PatternsManager::PatternsManager(int depth) {
   m_patterns = new CArrayObj;
   m_depth = depth;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
PatternsManager::~PatternsManager() {
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PatternsManager::SetPatterns() {
   int depthPattern = 30;

   MqlRates rates[];
   //ArraySetAsSeries(rates,true);
   int copied = CopyRates(_Symbol, _Period, 0, m_depth, rates);

   double open[];
   double close[];
   double low[];
   double high[];
   datetime time[];
   
   ArrayResize(open, m_depth);
   ArrayResize(close, m_depth);
   ArrayResize(low, m_depth);
   ArrayResize(high, m_depth);
   ArrayResize(time, m_depth);
   
   
   for(int i=0;i<m_depth;i++)
     {
      open[i] = rates[i].open;
      close[i] = rates[i].close;
      low[i] = rates[i].low;
      high[i] = rates[i].high;
      time[i] = rates[i].time;
     }

   for(int i = depthPattern; i < m_depth; i++) {
      int lastBarsType = BarsHelper::chainSomeBars(i, 4, open, close);
      double avgBar = BarsHelper::getAvgSizeBarBody(depthPattern, open, close);

      AbsorptionPattern * pattern1 = new AbsorptionPattern(i, time, open, close, high, low);
      pattern1.calculatePattern();
      pattern1.drawInfo();

      BarEat2PrevBarsPattern * pattern2 = new BarEat2PrevBarsPattern(i, time, open, close, high, low);
      pattern2.calculatePattern();
      pattern2.drawInfo();

      PinokioPattern * pattern3 = new PinokioPattern(i, time, open, close, high, low);
      pattern3.calculatePattern();
      pattern3.drawInfo();
   }

}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PatternsManager::Calculate() {
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PatternsManager::DrawPattern(BarsPattern &pattern) {
}
//+------------------------------------------------------------------+
