//+------------------------------------------------------------------+
//|                                            AbsorptionPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class AbsorptionPattern : public BarsPattern
  {
private:
public:
                     AbsorptionPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[]);
                    ~AbsorptionPattern();
   void              calculatePattern();
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AbsorptionPattern::AbsorptionPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[])
   : BarsPattern()
  {
   shortName = "Absorption";

   addBars(2, from, time, open, close, high, low);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
AbsorptionPattern::~AbsorptionPattern()
  {
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void AbsorptionPattern::calculatePattern()
  {
//Print("Calculate pattern");
   Bar *barCurrent = barObjects.At(0);
   Bar *barPrev = barObjects.At(1);

   if(barCurrent.isLong != barPrev.isLong)
      if(barCurrent.high > barPrev.high && barCurrent.low < barPrev.low && barCurrent.bodySize > barPrev.bodySize)
        {
         isPattern = true;
         if(barCurrent.isLong)
           {
            positionType=POSITION_TYPE_BUY;
           }

         if(barCurrent.isShort)
           {
            positionType=POSITION_TYPE_SELL;
           }
        }
  }
//+------------------------------------------------------------------+
