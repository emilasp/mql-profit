//+------------------------------------------------------------------+
//|                                            AbsorptionPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class PinokioPattern : public BarsPattern
  {
private:
public:
                     PinokioPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[]);
                    ~PinokioPattern();
   void              calculatePattern();
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
PinokioPattern::PinokioPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[])
   : BarsPattern()
  {
   shortName = "Pinokio";
  
   addBars(3, from, time, open, close, high, low);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
PinokioPattern::~PinokioPattern()
  {
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PinokioPattern::calculatePattern()
  {
//Print("Calculate pattern");
   Bar *barLeft = barObjects.At(0);
   Bar *barCenter = barObjects.At(1);
   Bar *barRight = barObjects.At(2);

   if(barCenter.isLong)
     {
      if(barCenter.bodySize > 0 && barCenter.shadeHigh / barCenter.bodySize > 3)
        {
         double centralBarNoisePolovina = (barCenter.high - barCenter.shadeHigh / 2);
         if(barLeft.high < centralBarNoisePolovina)
           {
            if(barRight.high < centralBarNoisePolovina)
              {
               if(barRight.close < barCenter.low)
                 {
                  positionType = POSITION_TYPE_SELL;
                  isPattern = true;
                 }
              }
           }
        }
     }

   if(barCenter.isShort)
     {
      if(barCenter.bodySize > 0 && barCenter.shadeLow / barCenter.bodySize > 3)
        {
         double centralBarNoisePolovina = (barCenter.low + barCenter.shadeLow / 2);
         if(barLeft.low > centralBarNoisePolovina)
           {
            if(barRight.low > centralBarNoisePolovina)
              {
               if(barRight.close > barCenter.high)
                 {
                  positionType = POSITION_TYPE_BUY;
                  isPattern = true;
                 }
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
