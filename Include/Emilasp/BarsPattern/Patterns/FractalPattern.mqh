//+------------------------------------------------------------------+
//|                                            AbsorptionPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class FractalPattern : public BarsPattern {
 private:
   int               m_bars;

 public:
                     FractalPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[], int bars = 5);
                    ~FractalPattern();
   void              calculatePattern();
   void              calculateFor3Bars();
   void              calculateFor5Bars();

};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FractalPattern::FractalPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[], int bars = 5)
   : BarsPattern() {
   shortName = "Fractal";

   m_bars = bars;

   addBars(m_bars, from, time, open, close, high, low);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
FractalPattern::~FractalPattern() {
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void FractalPattern::calculatePattern() {
   if(m_bars == 3) {
      calculateFor3Bars();
   } else {
      calculateFor5Bars();
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void FractalPattern::calculateFor3Bars() {
   Bar *barLeft = barObjects.At(2);
   Bar *barCenter = barObjects.At(1);
   Bar *barRight = barObjects.At(0);

   if(barLeft.high <= barCenter.high && barRight.high < barCenter.high) {
      positionType = POSITION_TYPE_BUY;
      isPattern = true;
   }

   if(barLeft.low >= barCenter.low && barRight.low > barCenter.low) {
      positionType = POSITION_TYPE_SELL;
      isPattern = true;
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void FractalPattern::calculateFor5Bars() {
   Bar *barLeft = barObjects.At(4);
   Bar *barLeftCenter = barObjects.At(3);
   Bar *barCenter = barObjects.At(2);
   Bar *barRightCenter = barObjects.At(1);
   Bar *barRight = barObjects.At(0);


   if(barLeft.high <= barLeftCenter.high && barLeftCenter.high <= barCenter.high)
      if (barCenter.high > barRightCenter.high && barRightCenter.high > barRight.high) {
         positionType = POSITION_TYPE_BUY;
         isPattern = true;
      }

   if(barLeft.low >= barLeftCenter.low && barLeftCenter.low >= barCenter.low)
      if (barCenter.low < barRightCenter.low && barRightCenter.low < barRight.low) {
         positionType = POSITION_TYPE_SELL;
         isPattern = true;
      }
}

//+------------------------------------------------------------------+
