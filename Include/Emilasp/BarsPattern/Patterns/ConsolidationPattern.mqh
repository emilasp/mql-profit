//+------------------------------------------------------------------+
//|                                            AbsorptionPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>

struct ConsolidationAvg {
   double            avgBar;
   double            maxHeigthBar;
   double            maxPercent;
   void              ConsolidationAvg()
   {
      avgBar = 0;
      maxHeigthBar = 0;
      maxPercent = 0;
   }
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ConsolidationPattern : public BarsPattern
{
private:
   int               barsPatternStart;
public:
                     ConsolidationPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[], int barsForAvg, int _barsPatternStart);
                    ~ConsolidationPattern();
   void              calculatePattern();
   ConsolidationAvg  getAvgBar(int startBar, int countBars);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationPattern::ConsolidationPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[], int barsForAvg, int _barsPatternStart)
   : BarsPattern()
{
   shortName = "Consolidation";

   barsPatternStart = _barsPatternStart;

   addBars(barsForAvg, from, time, open, close, high, low);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationPattern::~ConsolidationPattern()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ConsolidationPattern::calculatePattern()
{
   int countPatternBars = 10;
   ConsolidationAvg avgBarSize = getAvgBar(barsPatternStart + , 20);
   ConsolidationAvg consolidationBarSize = getAvgBar(barsPatternStart, consolidationBars);

   Label1Buffer[i] = consolidationBarSize.avgBar / avgBarSize.avgBar * 100;
   Label2Buffer[i] = Label1Buffer[i] < isConsolidationPercent ? 1 : 0;

   Label3Buffer[i] = consolidationBarSize.maxPercent + 10;
   Label4Buffer[i] = consolidationBarSize.maxPercent < 10 ? 1 : 0;

//Print("Calculate pattern");
   Bar *barCurrent = barObjects.At(0);
   Bar *barPrev = barObjects.At(1);

   if(barCurrent.isLong != barPrev.isLong)
      if(barCurrent.high > barPrev.high && barCurrent.low < barPrev.low && barCurrent.bodySize > barPrev.bodySize) {
         isPattern = true;
         if(barCurrent.isLong) {
            positionType = POSITION_TYPE_BUY;
         }

         if(barCurrent.isShort) {
            positionType = POSITION_TYPE_SELL;
         }
      }
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationAvg ConsolidationPattern::getAvgBar(int startBar, int countBars)
{
   ConsolidationAvg avg;

   double sumBars = 0;
   double maxPrice = 0;
   double minPrice = DBL_MAX;

   for(int i = 0; i < barObjects.Total(); i++) {
      if (i - startBar > countBars)
         break;
         
      if(i >= startBar) {
         Bar bar = barObjects.At(i);
         sumBars += MathAbs(bar.close - bar.open);

         if(maxPrice < bar.high)
            maxPrice = bar.high;
         if(minPrice > bar.low)
            minPrice = bar.low;
      }
   }

   avg.avgBar = sumBars / countBars;
   avg.maxHeigthBar = maxPrice - minPrice;
   avg.maxPercent = avg.avgBar / avg.maxHeigthBar * 100;

   return avg;
}
//+------------------------------------------------------------------+
