//+------------------------------------------------------------------+
//|                                            AbsorptionPattern.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\BarsPattern.mqh>


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class BarEat2PrevBarsPattern : public BarsPattern
  {
private:
public:
                     BarEat2PrevBarsPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[]);
                    ~BarEat2PrevBarsPattern();
   void              calculatePattern();
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BarEat2PrevBarsPattern::BarEat2PrevBarsPattern(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[])
   : BarsPattern()
  {
   shortName = "BarEat2BarsBar";
   
   prevBarsModel = new PrevBarsModel(from, time, open, close, high, low);
   
   addBars(3, from, time, open, close, high, low);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
BarEat2PrevBarsPattern::~BarEat2PrevBarsPattern()
  {
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void BarEat2PrevBarsPattern::calculatePattern()
  {
   Bar *barCurrent = barObjects.At(0);
   Bar *barPrev = barObjects.At(1);
   Bar *barPrev2 = barObjects.At(2);
   
   double prevBarsBodySizes = barPrev.bodySize + barPrev2.bodySize;
   bool isBodyBar = (barCurrent.shadeHigh + barCurrent.shadeLow) > 0
    && (barCurrent.bodySize / (barCurrent.shadeHigh + barCurrent.shadeLow)) > 2;
    
   if(isBodyBar && prevBarsBodySizes && (barCurrent.bodySize / prevBarsBodySizes > 1))
     {
      if(barCurrent.isLong && barPrev.isShort && barPrev2.isShort && prevBarsModel.longBarsAlong > 2)
        {
        positionType=POSITION_TYPE_BUY;
         isPattern = true;
        }
      if(barCurrent.isShort && barPrev.isLong && barPrev2.isLong && prevBarsModel.shortBarsAlong > 2)
        {
        positionType=POSITION_TYPE_SELL;
          isPattern = true;
        }
     }
  }
//+------------------------------------------------------------------+
