//+------------------------------------------------------------------+
//|                                                          Bar.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Object.mqh>
#include <Emilasp\helpers\BarsHelper.mqh>

enum BAR_SIZE
  {
   BAR_SIZE_BIG = 1,
   BAR_SIZE_SMALL = 2,
   BAR_SIZE_NORMAL = 1
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Bar : public CObject
  {
private:
   void calculate();
protected:
public:
   datetime time;
   double open;
   double close;
   double high;
   double low;
    
   double            fullSize;
   double            bodySize;
   double            shadeHigh;
   double            shadeLow;
   BAR_SIZE          barSizeType;
   BAR_SIZE          shadeSizeType;

   bool              isLong;
   bool              isShort;

                     Bar(datetime timeBar, const double openBar, const double closeBar, const double highBar, const double lowBar);
                     Bar(MqlRates &rate);
                    ~Bar();
   bool              isSmallFullBarBody(double avgBar, double koef = 0.5);
   bool              isBigFullBar(double avgBar, double koef = 1.3);
   bool              isSmallBarBody(double avgBar, double koef = 0.5);
   bool              isBigBarBody(double avgBar, double koef = 1.3);
   bool              isMaribosu(bool full = true, double koef = 0.02);
   bool              isDodji();
   bool              isMolotUp();
   bool              isMolotDown();
   
   bool              isBigBody(double koef = 0.3);
   void              normalizeByBar(Bar * firstBar);

  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Bar::Bar(datetime timeBar, const double openBar, const double closeBar, const double highBar, const double lowBar)
  {
   time=timeBar;
   open = openBar;
   close = closeBar;
   high = highBar;
   low = lowBar;
  
   calculate();
  }
  
Bar::Bar(MqlRates &rate)
  {
   time=rate.time;
   open = rate.open;
   close = rate.close;
   high = rate.high;
   low = rate.low;
  
   calculate();
  }
  
void Bar::calculate()
  {
   isLong = close > open;
   isShort = !isLong;

   if(isLong)
     {
      bodySize = close - open;
      shadeHigh = high - close;
      shadeLow = open - low;
     }
   if(isShort)
     {
      bodySize = open - close;
      shadeHigh = high - open;
      shadeLow = close - low;
     }

   fullSize = high - low;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Bar::~Bar()
  {
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isBigBarBody(double avgBar, double koef = 1.3)
  {
   return bodySize / avgBar > koef;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isSmallBarBody(double avgBar, double koef = 0.5)
  {
   return bodySize / avgBar < koef;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isBigFullBar(double avgBar, double koef = 1.3)
  {
   return fullSize / avgBar > koef;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isSmallFullBarBody(double avgBar, double koef = 0.5)
  {
   return fullSize / avgBar < koef;
  }
//+------------------------------------------------------------------+
bool Bar::isMaribosu(bool full = true, double koef = 0.02)
  {
   if(full)
     {
      if((shadeLow < bodySize * koef && shadeHigh < bodySize * koef) && bodySize > 0)
         return true;
     }
   else
     {
      if((shadeLow < bodySize * koef || shadeHigh < bodySize * koef) && bodySize > 0)
         return true;
     }
   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isDodji()
  {
   return bodySize < (shadeHigh + shadeLow) * 0.03;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Bar::isMolotUp()
  {
   return shadeLow > bodySize * 2 && shadeHigh < bodySize * 0.1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool  Bar::isMolotDown()
  {
   return shadeHigh > bodySize * 2 && shadeLow < bodySize * 0.1;
  }
  
//+------------------------------------------------------------------+
 bool  Bar::isBigBody(double koef = 0.3) {
   return bodySize > 0 && (shadeHigh + shadeLow) / bodySize < koef;
 }
 //+------------------------------------------------------------------+
 void Bar::normalizeByBar(Bar * firstBar) {  
   open = open - firstBar.open;
   close = close - firstBar.close;
   high = high - firstBar.high;
   low = low - firstBar.low;
   shadeHigh = shadeHigh - firstBar.shadeHigh;
   shadeLow = shadeLow - firstBar.shadeLow;
 }