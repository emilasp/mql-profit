//+------------------------------------------------------------------+
//|                                                PrebBarsModel.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"

#include <Object.mqh>
#include <Arrays\ArrayObj.mqh>

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>

//+------------------------------------------------------------------+
class PrevBarsModel
  {
public:
   CArrayObj         *barObjects;

   int               longBarsAlong;
   int               shortBarsAlong;

   int               bigBarsAlong;
   int               smallBarsAlong;

   void              PrevBarsModel(int from, const datetime &time[], const double &open[], const double &close[], const double &high[], const double &low[], int depth = 5)
     {
      longBarsAlong = 0;
      shortBarsAlong = 0;
      bigBarsAlong = 0;
      smallBarsAlong = 0;

      for(int i=1; i<depth; i++)
        {
         Bar *bar = new Bar(time[from-i], open[from-i], close[from-i], high[from-i], low[from-i]);
         //barObjects.Add(bar);

         if(bar.isLong && !shortBarsAlong)
           {
            longBarsAlong++;
           }
         if(bar.isShort && !shortBarsAlong)
           {
            shortBarsAlong++;
           }
        }
     }
  };
//+------------------------------------------------------------------+
