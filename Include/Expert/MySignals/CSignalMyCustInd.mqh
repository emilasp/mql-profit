//+------------------------------------------------------------------+
//|                                              SignalEnvelopes.mqh |
//|                   Copyright 2009-2013, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#include <Expert\ExpertSignal.mqh>
// wizard description start
//+------------------------------------------------------------------+
//| Description of the class                                         |
//| Title=Signals of indicator 'Envelopes'                           |
//| Type=SignalAdvanced                                              |
//| Name=MyCustomIndicator                                           |
//| ShortName=MyCustomIndicator                                      |
//| Class=CSignalMyCustInd                                           |
//| Page=signal_envelopes                                            |
//| Parameter=PeriodFast,int,12,Period of fast EMA                   |
//| Parameter=PeriodSlow,int,24,Period of slow EMA                   |
//| Parameter=PeriodSignal,int,9,Period of averaging of difference   |
//| Parameter=Applied,ENUM_APPLIED_PRICE,PRICE_CLOSE,Prices series   |
//+------------------------------------------------------------------+
// wizard description end
//+------------------------------------------------------------------+
//| Class CSignalMyCustInd.                                          |
//| Purpose: Класс генератора торговых сигналов на основе            |
//|          пользовательского индикатора.                           |
//| Является производным от класса CExpertSignal.                    |
//+------------------------------------------------------------------+
class CSignalMyCustInd : public CExpertSignal
  {
protected:
   CiCustom          m_mci;            // объект-индикатор "MyCustomIndicator"
   //--- настраиваемые параметры
   int               m_period_fast;    // "период быстрой EMA"
   int               m_period_slow;    // "период медленной EMA"
   int               m_period_signal;  // "период усреднения разности"
   ENUM_APPLIED_PRICE m_applied;       // "тип цены"
   //--- "веса" рыночных моделей (0-100)
   int               m_pattern_0;      // model 0 "the oscillator has required direction"
   int               m_pattern_1;      // model 1 "индикатор растет - buy; индикатор падает - sell"

public:
                     CSignalMyCustInd(void);
                    ~CSignalMyCustInd(void);
   //--- методы установки настраиваемых параметров
   void              PeriodFast(int value)               { m_period_fast=value;           }
   void              PeriodSlow(int value)               { m_period_slow=value;           }
   void              PeriodSignal(int value)             { m_period_signal=value;         }
   void              Applied(ENUM_APPLIED_PRICE value)   { m_applied=value;               }
   //--- методы настраивания "весов" рыночных моделей
   void              Pattern_0(int value)                { m_pattern_0=value;        }
   void              Pattern_1(int value)                { m_pattern_1=value;        }
   //--- метод проверки настроек
   virtual bool      ValidationSettings(void);
   //--- метод создания индикатора и таймсерий
   virtual bool      InitIndicators(CIndicators *indicators);
   //--- методы проверки, если модели рынка сформированы
   virtual int       LongCondition(void);
   virtual int       ShortCondition(void);
   
protected:
   //--- метод инициализации индикатора
   bool              InitMyCustomIndicator(CIndicators *indicators);
   //--- методы получения данных
   //- получение значения индикатора
   double            Main(int ind) { return(m_mci.GetData(0,ind));      }
   //- получение значения сигнальной линии
   double            Signal(int ind) { return(m_mci.GetData(1,ind));    }
   //- разница между двумя соседними значениями индикатора
   double            DiffMain(int ind) { return(Main(ind)-Main(ind+1)); }
   int               StateMain(int ind);
   double            State(int ind) { return(Main(ind)-Signal(ind)); }
   //- подготавливает данные для поиска
   bool              ExtState(int ind);
   //- ищет рыночную модель с указанными параметрами
   bool              CompareMaps(int map,int count,bool minimax=false,int start=0);
  };
  
//+------------------------------------------------------------------+
//| Конструктор                                                      |
//+------------------------------------------------------------------+
CSignalMyCustInd::CSignalMyCustInd(void) : m_period_fast(12),
                                           m_period_slow(24),
                                           m_period_signal(9),
                                           m_applied(PRICE_CLOSE),
                                           m_pattern_0(10),
                                           m_pattern_1(50)
  {
//--- initialization of protected data
   m_used_series=USE_SERIES_HIGH+USE_SERIES_LOW;
  }
  
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CSignalMyCustInd::~CSignalMyCustInd(void)
  {
  }
//+------------------------------------------------------------------+
//| Проверка параметров защищенных данных                            |
//+------------------------------------------------------------------+
bool CSignalMyCustInd::ValidationSettings(void)
  {
//--- validation settings of additional filters
   if(!CExpertSignal::ValidationSettings())
      return(false);
//--- initial data checks
   if(m_period_fast>=m_period_slow)
     {
      printf(__FUNCTION__+": slow period must be greater than fast period");
      return(false);
     }
//--- ok
   return(true);
  }

//+------------------------------------------------------------------+
//| Создание индикаторов.                                            |
//+------------------------------------------------------------------+
bool CSignalMyCustInd::InitIndicators(CIndicators *indicators)
  {
//--- check of pointer is performed in the method of the parent class
//---
//--- инициализация индикаторов и таймсерий дополнительных фильтров
   if(!CExpertSignal::InitIndicators(indicators))
      return(false);
//--- создание и инициализация пользовательского индикатора
   if(!InitMyCustomIndicator(indicators))
      return(false);
//--- ok
   return(true);
  }
  
//+------------------------------------------------------------------+
//| Инициализация индикаторов.                                       |
//+------------------------------------------------------------------+
bool CSignalMyCustInd::InitMyCustomIndicator(CIndicators *indicators)
  {
//--- добавление объекта в коллекцию
   if(!indicators.Add(GetPointer(m_mci)))
     {
      printf(__FUNCTION__+": error adding object");
      return(false);
     }
//--- задание параметров индикатора
   MqlParam parameters[4];
//---
   parameters[0].type=TYPE_STRING;
   parameters[0].string_value="Examples\\MACD.ex5";
   parameters[1].type=TYPE_INT;
   parameters[1].integer_value=m_period_fast;
   parameters[2].type=TYPE_INT;
   parameters[2].integer_value=m_period_slow;
   parameters[3].type=TYPE_INT;
   parameters[3].integer_value=m_period_signal;
//--- инициализация объекта
   if(!m_mci.Create(m_symbol.Name(),0,IND_CUSTOM,4,parameters))
     {
      printf(__FUNCTION__+": error initializing object");
      return(false);
     }
//--- количество буферов
   if(!m_mci.NumBuffers(4)) return(false);
//--- ok
   return(true);
  }
  
//+------------------------------------------------------------------+
//| "Голосование" за то, что цена будет расти.                       |
//+------------------------------------------------------------------+
int CSignalMyCustInd::LongCondition(void)
  {
   int result=0;
   int idx   =StartIndex();
//--- check direction of the main line
   if(DiffMain(idx)>0.0)
     {
      //--- основная линия направлена вверх, и это подтверждает возможность роста цен
      if(IS_PATTERN_USAGE(0))
         result=m_pattern_0;      // "confirming" signal number 0
      //--- if the model 1 is used, look for a reverse of the main line
      if(IS_PATTERN_USAGE(1) && DiffMain(idx+1)<0.0)
         result=m_pattern_1;      // signal number 1
     }
//--- return the result
   return(result);
  }
  
//+------------------------------------------------------------------+
//| "Голосование" за то, что цена упадет.                            |
//+------------------------------------------------------------------+
int CSignalMyCustInd::ShortCondition(void)
  {
   int result=0;
   int idx   =StartIndex();
//--- check direction of the main line
   if(DiffMain(idx)<0.0)
     {
            //--- основная линия направлена вниз, и это подтверждает возможность снижения цен
      if(IS_PATTERN_USAGE(0))
         result=m_pattern_0;      // "confirming" signal number 0
      //--- if the model 1 is used, look for a reverse of the main line
      if(IS_PATTERN_USAGE(1) && DiffMain(idx+1)>0.0)
         result=m_pattern_1;      // signal number 1
     }
//--- return the result
   return(result);
  }