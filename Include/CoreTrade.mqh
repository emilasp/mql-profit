//+------------------------------------------------------------------+
//|                                                    CoreTrade.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <Trade\AccountInfo.mqh>

   CTrade           m_Trade;       // структура для выполнения торговых операций
   CPositionInfo    m_Position;    // структура для получения информации о позициях 
   CAccountInfo     m_Account;     // объект для работы со счетом
class CoreTrade
  {
private:          
      int MagicNumber; //--- зададим MagicNumber для идентификации своих ордеров
      int deviation;       //--- установим допустимое проскальзывание в пунктах при совершении покупки/продажи

public:
   CoreTrade(string symbolL, ENUM_TIMEFRAMES timeframeL, int magic);
  ~CoreTrade();
  
  virtual void openPositionBuy(double volume, int takeProfit, int stopLoss);
  virtual void openPositionSell(double volume, int takeProfit, int stopLoss);
  virtual void putToSocketData(string file, string data);
  
  virtual int accountInit();
                    
                    //openPositionBuy();
                    //openPositionSell();
                    
                    
   string            symbol;                                       //переменная для хранения символа
   ENUM_TIMEFRAMES   timeframe;                                    //переменная для хранения таймфрейма

  
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTrade::CoreTrade(string symbolL, ENUM_TIMEFRAMES timeframeL, int magic)
  {
      symbol = symbolL;
      timeframe = timeframeL;
      
      MagicNumber=magic;
      deviation=10;

      m_Trade.SetExpertMagicNumber(MagicNumber);
      m_Trade.SetDeviationInPoints(deviation);
      m_Trade.SetTypeFilling(ORDER_FILLING_RETURN);//--- режим заполнения ордера, нужно использовать тот режим, который разрешается сервером
      m_Trade.SetAsyncMode(true);//--- какую функцию использовать для торговли: true - OrderSendAsync(), false - OrderSend()
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CoreTrade::~CoreTrade()
  {
  }
//+------------------------------------------------------------------+


void CoreTrade::openPositionBuy(double volume, int takeProfit, int stopLoss) {
    if(m_Position.Select(symbol))                             //если уже существует позиция по этому символу
     {
        if(m_Position.PositionType()==POSITION_TYPE_SELL) {
         m_Trade.PositionClose(symbol);  //и тип этой позиции Sell, то закрываем ее
        }
        if(m_Position.PositionType()==POSITION_TYPE_BUY) {
         return;                             //а если тип этой позиции Buy, то выходим
        }
     }
    //m_Trade.Buy(0.1,symbol);                                  //если дошли сюда, значит позиции нет, открываем ее
    
   int    digits=(int)SymbolInfoInteger(symbol,SYMBOL_DIGITS); // количество знаков после запятой
   double point=SymbolInfoDouble(symbol,SYMBOL_POINT);         // пункт
   double bid=SymbolInfoDouble(symbol,SYMBOL_BID);  // текущая цена для закрытия LONG
   double open_price=SymbolInfoDouble(symbol,SYMBOL_ASK);//--- получим текущую цену открытия для LONG позиций
   
   // Set SL and TP
   double SL = 0;
   double TP = 0;           
   if (takeProfit > 0) {
       TP = bid + takeProfit * point;                                   // ненормализованное значение TP
       TP = NormalizeDouble(TP,digits);   // нормализуем Take Profit
   }
   if (stopLoss > 0) {
      SL = bid - stopLoss * point;                                   // ненормализованное значение SL
      SL = NormalizeDouble(SL,digits);                              // нормализуем Stop Loss
   }
   //
   string comment=StringFormat("Buy %s %G lots at %s, SL=%s TP=%s", symbol,volume, DoubleToString(open_price,digits), DoubleToString(SL,digits), DoubleToString(TP,digits));
   
   if(!m_Trade.Buy(volume,symbol,open_price,SL,TP,comment)) {
      Print("Метод Buy() потерпел неудачу. Код возврата=",m_Trade.ResultRetcode(), ". Описание кода: ",m_Trade.ResultRetcodeDescription());
     } else {
      Print("Метод Buy() выполнен успешно. Код возврата=",m_Trade.ResultRetcode(), " (",m_Trade.ResultRetcodeDescription(),")");
     }
}

void CoreTrade::openPositionSell(double volume, int takeProfit, int stopLoss) {

   if(m_Position.Select(symbol))                             //если уже существует позиция по этому символу
     {
        if(m_Position.PositionType()==POSITION_TYPE_BUY){
         m_Trade.PositionClose(symbol);   //и тип этой позиции Buy, то закрываем ее
        }
        if(m_Position.PositionType()==POSITION_TYPE_SELL){
         return;                            //а если тип этой позиции Sell, то выходим
        }
     }
   //m_Trade.Sell(0.1,symbol);                                 //если дошли сюда, значит позиции нет, открываем ее

   
   int    digits=(int)SymbolInfoInteger(symbol,SYMBOL_DIGITS); // количество знаков после запятой
   double point=SymbolInfoDouble(symbol,SYMBOL_POINT);         // пункт
   double bid=SymbolInfoDouble(symbol,SYMBOL_BID);  // текущая цена для закрытия LONG
   double open_price=SymbolInfoDouble(symbol,SYMBOL_ASK);//--- получим текущую цену открытия для LONG позиций
   
   // Set SL and TP
   double SL = 0;
   double TP = 0;           
   if (takeProfit > 0) {
       TP = bid - takeProfit * point;                                   // ненормализованное значение TP
       TP = NormalizeDouble(TP,digits);   // нормализуем Take Profit
   }
   if (stopLoss > 0) {
      SL = bid + stopLoss * point;                                   // ненормализованное значение SL
      SL = NormalizeDouble(SL,digits);                              // нормализуем Stop Loss
   }
   //
   string comment=StringFormat("Buy %s %G lots at %s, SL=%s TP=%s", symbol,volume, DoubleToString(open_price,digits), DoubleToString(SL,digits), DoubleToString(TP,digits));
   
   if(!m_Trade.Buy(volume,symbol,open_price,SL,TP,comment)) {
      Print("Метод Buy() потерпел неудачу. Код возврата=",m_Trade.ResultRetcode(), ". Описание кода: ",m_Trade.ResultRetcodeDescription());
     } else {
      Print("Метод Buy() выполнен успешно. Код возврата=",m_Trade.ResultRetcode(), " (",m_Trade.ResultRetcodeDescription(),")");
     }
}




// Core Cocket
void CoreTrade::putToSocketData(string file, string data) {
 //string InpFileName = "yyy\\Files\\exportFromMql.dat";
 int file_handle=FileOpen(file,FILE_WRITE|FILE_CSV|FILE_ANSI|FILE_COMMON);
   if(file_handle!=INVALID_HANDLE)
     {
      PrintFormat("Файл %s открыт для записи",file);
      PrintFormat("Путь к файлу: %s\\Files\\",TerminalInfoString(TERMINAL_DATA_PATH));
      FileWriteString(file_handle,data+"\r\n");
      FileClose(file_handle);
      PrintFormat("Данные записаны, файл %s закрыт",file);
     } else {
       PrintFormat("Не удалось открыть файл %s, Код ошибки = %d",file,GetLastError());
     }
}


// Accaunt info 
int CoreTrade::accountInit()
{
//--- получим номер счета, на котором запущен советник
   long login=m_Account.Login();
   Print("Login=",login);
//--- выясним тип счета
   ENUM_ACCOUNT_TRADE_MODE account_type=m_Account.TradeMode();
//--- если счет оказался реальным, прекращаем работу эксперта немедленно!
   if(account_type==ACCOUNT_TRADE_MODE_REAL)
     {
      MessageBox("Работа на реальном счете запрещена, выходим","Эксперт запущен на реальном счете!");
      return(-1);
     }
//--- выведем тип счета    
   Print("Тип счета: ",EnumToString(account_type));
//--- выясним, можно ли вообще торговать на данном счете
   if(m_Account.TradeAllowed())
      Print("Торговля на данном счете разрешена");
   else
      Print("Торговля на счете запрещена: возможно, вход был совершен по инвест-паролю");
//--- выясним, разрешено ли торговать на счете с помощью эксперта
   if(m_Account.TradeExpert())
      Print("Автоматическая торговля на счете разрешена");
   else
      Print("Запрещена автоматическая торговля с помощью экспертов и скриптов");
//--- допустимое количество ордеров задано или нет
   int orders_limit=m_Account.LimitOrders();
   if(orders_limit!=0)Print("Максимально допустимое количество действующих отложенных ордеров: ",orders_limit);
//--- выведем имя компании и сервера
   Print(m_Account.Company(),": server ",m_Account.Server());
//--- напоследок выведем баланс и текущую прибыль на счете
   Print("Balance=",m_Account.Balance(),"  Profit=",m_Account.Profit(),"   Equity=",m_Account.Equity());
   Print(__FUNCTION__,"  completed"); //---
   return 1;
}