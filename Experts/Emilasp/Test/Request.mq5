//+------------------------------------------------------------------+
//|                                                      Request.mq5 |
//|                                   Copyright 2020, Aleksey Osipov |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Aleksey Osipov"
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\libraries\Request.mqh>

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
 
//---

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   string request = "{\"dataInput\":{\"first\":10,\"last\":99}}";
   Print ("Запрос: ",request); 
   string resp = makeRequest("auditory.sport-run.ru", "test", request);
   Print(resp);

  }
//+------------------------------------------------------------------+

