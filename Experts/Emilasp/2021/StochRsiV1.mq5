//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>
/////////////

enum TRAND_BY {
   TRAND_MA  = 1,
   TRAND_ADX = 2
};

int magic = 71239023;

input ENUM_TIMEFRAMES timeFrame    = PERIOD_H1;
input bool signalStochEnable       = true;  // Stochastic signal 1
input double signalStochLevelUp    = 85.0;  // Stochastic: level UP
input double signalStochLevelDown  = 20.0;  // Stochastic: level DOWN
input int signalStochKperiod       = 7; // Stochastic: K period
input int signalStochDperiod       = 3; // Stochastic: D period
input int signalStochSlowing       = 3; // Stochastic: Slowing

input double volume = 0.01;
input int SL=70;
input int TP=50;
input bool trailingStop = true; // Trailing STOP
input int TR            = 50; // Trailing pips

/////////////////////
int handle_STOCH;

CoreTradeHadge    *trade;

bool on_tral = false;

struct sSignal {
   bool              Buy;
   bool              Sell;
   void              sSignal()
   {
      Buy=false;
      Sell=false;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
{
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

   bool is_STOCH_signal_buy = false;
   bool is_STOCH_signal_sell = false;

////// Stochastic ////////
   double Stochastic[];
   ArraySetAsSeries(Stochastic, true);
   CopyBuffer(handle_STOCH, 0, 0, 2, Stochastic);
   double StochasticSignal[];
   ArraySetAsSeries(StochasticSignal, true);
   CopyBuffer(handle_STOCH, 1, 0, 2, StochasticSignal);
// BUY
   if (Stochastic[1] < signalStochLevelDown && Stochastic[0] < signalStochLevelDown)
      if (Stochastic[1] < Stochastic[0])
         is_STOCH_signal_buy = true;
 
// SELL
   if (Stochastic[1] > signalStochLevelUp && Stochastic[0] > signalStochLevelUp)
      if (Stochastic[1] > Stochastic[0])
         is_STOCH_signal_sell = true;
  

////// RSI ////////
   double CCI[];
   ArraySetAsSeries(CCI, true);


   if (is_STOCH_signal_buy) {
      res.Buy = true;
   }

   if (is_STOCH_signal_sell) {
      res.Sell = true;
   }

   return res;
}
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_M1, magic);

   handle_STOCH = iStochastic(_Symbol, _Period, signalStochKperiod, signalStochDperiod, signalStochSlowing, MODE_SMA, STO_LOWHIGH);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
   if(!PositionSelect(_Symbol)) {
      on_tral = false;

      sSignal signal = Buy_or_Sell();
      int trand = trandCalc();
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      
      //if (trade.isNewBar()) {
      //   // Draw
      //   if (signal.Buy) {
      //      DrawHelper::drawArrow("buy:" + last_tick.time, last_tick.time, last_tick.bid, 233, clrGreen);
      //   }
      //   if (signal.Sell) {
      //      DrawHelper::drawArrow("sell:" + last_tick.time, last_tick.time, last_tick.bid, 234, clrRed);
      //   }
      //}
      


      if(trand > 0 && signal.Buy) {
         trade.Buy(volume, TP, SL);
      }

      if(trand < 0 && signal.Sell) {
         trade.Sell(volume, TP, SL);
      }
   }

   trailing();
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Тренд: берем последние n(3) свечей на старшем ТФ и анализируем их
//| Если все свечи в одну сторону, то это тренд.
//| Доработать: Анализировать тени свечей                                                           |
//+------------------------------------------------------------------+
int trandCalc()
{
   int res = 0;

   MqlRates rates[];
   ArraySetAsSeries(rates,true);
   
   if(CopyRates(NULL,PERIOD_H4,0,100,rates) <= 0)
      Print("Ошибка копирования ценовых данных ",GetLastError());

   Bar *bar0 = new Bar(rates[0]);
   Bar *bar1 = new Bar(rates[1]);
   Bar *bar2 = new Bar(rates[2]);
   
   if (bar0.isLong && bar1.isLong && bar2.isLong) {
      res = 1;
   }
   if (bar0.isShort && bar1.isShort && bar2.isShort) {
      res = -1;
   }
   
   return res;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void trailing()
{
   if(trailingStop && PositionSelect(_Symbol)) {
      if(PositionGetDouble(POSITION_PROFIT)) {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point) {
               on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > PositionGetDouble(POSITION_SL)) {
                  trade.PositionModify(_Symbol, price, 0);
               }
            }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
            if(last_tick.ask<price-TR*_Point) {
               on_tral = true;
               price = last_tick.ask + TR*_Point;
               if(price < PositionGetDouble(POSITION_SL) || PositionGetDouble(POSITION_SL) == NULL) {
                  trade.PositionModify(_Symbol, price, 0);
               }
            }
         }
      }
   }
}
//+------------------------------------------------------------------+
