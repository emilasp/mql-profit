//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#import "user32.dll"
void keybd_event(int bVk, int bScan, int dwFlags, int dwExtraInfo);
#import
#define VK_SPACE 0x20 //Space
#define VK_RETURN 0x0D //Return - Enter Key
#define KEYEVENTF_KEYUP 0x0002  //Key up

#include <Emilasp\Grid\Common\EGrid.mqh>
#include <Emilasp\Grid\Common\EGridManager.mqh>

#include <Emilasp\Grid\strategies\grid\EGridBaseStrategy.mqh>
#include <Emilasp\Grid\strategies\lot\EGridLotMartingaleStrategy.mqh>
#include <Emilasp\Grid\strategies\profit\EGridProfitEquityStrategy.mqh>
#include <Emilasp\Grid\strategies\piramiding\EGridPiramidingBaseStrategy.mqh>
#include <Emilasp\Grid\Common\base\EGridTradeStrategy.mqh>
#include <Emilasp\Grid\strategies\signal\EGridSignalImpulseStrategy.mqh>

#include <Emilasp\Signals\Manager\SignalManager.mqh>
#include <Emilasp\Signals\Base\ASignalBase.mqh>
#include <Emilasp\Signals\StochasticSignal.mqh>
//#include <Emilasp\Signals\RsiLaguerreSignal.mqh>
//#include <Emilasp\Signals\FanTrendDetectorSignal.mqh>
///

/////////////


input group           "Робот"
input int magic = 34531845; // ID Робота [если запускается несколько инстансов, то нужно задать разный ID]

input group           "Trading"
input double volume = 0.01; // Базовый лот
input int spredMax = 15; // Максимальный спред для разрешения открытия сетки
input int deviationMax = 10; // Максимальное проскальзывание

input group           "Grid"
input bool longEnabled=false; // Включить сделки в Long
input bool shortEnabled=true; // Включить сделки в Short

input int levels=100;
input int levelHeight=50; // Дистанция между уровнями
input int minBarsFromLastOrder=1; // Открывать ордера с разницей в n баров
input double gridCloseByEquityLoss = -5000; // Максимально допустимая просадка

input group           "Experimental"
input ENUM_FILTER_SIGNAL filterType = FILTER_SIGNAL_NONE;
input bool trailingSlByAtrEnable = false; // Усреднение: Trailing SL по АТР
input double trailingSlByAtrKoef = 2; // Усреднение: Trailing SL по АТР коэффициент

input bool trailingSlByAtrPiramEnable = false; // Пираммидинг: Trailing SL по АТР
input double trailingSlByAtrPiramKoef = 3; // Пираммидинг: Trailing SL по АТР коэффициент

input group           "Lot strategy"
input ENUM_LOT_STRATEGY lotStrategyType = LOT_STRATEGY_MARTINGALE;
input double volumeMartingale = 2;

input group           "Trailing Stop"
//input bool trailingStopVirtual  = true; // Trailing STOP virt: Включение
input int trailingStopDistanceLevelBase = 100; // Дистанция от безубытка до старта трала
input double trailingStopDynamicEquityKoef = 0.5; // Коэффициент влияния количества ордеров на расстояние до старта трала
input int trailingStopMinDistanceFromBu = 10; // Минимальная дистанция от безубытка до трала

input group           "Пирамидинг"
input bool pirmdEnabled = false; // Piramiding: Включаем
input double pirmdLot = 0.01; // Piramiding: Лот
input int pirmdSL = 50; // Piramiding: TrailingStop
input int pirmdKoef = 1; // Piramiding: Множитель лота на каждый ордер

input group           "Signal"
input ENUM_ENTER_TYPE EnterType = ENTER_TYPE_STOCH_LEVEL_INTERSECT; // Тип входа по Стохастику[По уровню или пересечению с сигнальной линией]

input group           "Debug"
input bool isSignalTrue = false; // Открывать сетку без сигнала
input bool pauseOnTesterTrade = true; // Останавливать тестер при наступлении сделки
input bool showGridVirtualLevels = false; // Показывать уровни виртуальных ордеров


EGridManager *gridManager;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{ 
//---
   StochasticSignal *stochSignalLong = new StochasticSignal(_Period, 7, 3, 3, 80, 20);
   stochSignalLong.setSignalsSettings(EnterType, EXIT_TYPE_STOCH_LEVEL_INTERSECT);

   EGridBaseStrategy *gridStategyLong     = new EGridBaseStrategy(levels, levelHeight, minBarsFromLastOrder, showGridVirtualLevels, trailingStopDistanceLevelBase, trailingStopMinDistanceFromBu, trailingStopDynamicEquityKoef, filterType, trailingSlByAtrEnable, trailingSlByAtrKoef);
   //EGridProfitEquityStrategy *profitStategyLong = new EGridProfitEquityStrategy(gridCloseByEquityLoss, virtualStopBUtoTralMinDistance, trailingStopVirtual, trailingStopVirtualPt, trailingVirtualDynamicEquityKoef, virtualStopPriceDistanceLevel, virtualStopPriceDistanceLevelKoef);
   EGridPiramidingBaseStrategy *piramidingStategyLong = new EGridPiramidingBaseStrategy(pirmdEnabled, pirmdLot, pirmdSL, pirmdSL, pirmdKoef, trailingSlByAtrPiramEnable, trailingSlByAtrPiramKoef);
   EGridLotMartingaleStrategy *lotStategyLong = new EGridLotMartingaleStrategy(volume, lotStrategyType, volumeMartingale);
   EGridTradeStrategy *tradeStategyLong = new EGridTradeStrategy(deviationMax, spredMax);
   EGridSignalImpulseStrategy *signalStategyLong = new EGridSignalImpulseStrategy(isSignalTrue);
   signalStategyLong.addSignal(stochSignalLong);

   EGrid *gridLong = new EGrid(POSITION_TYPE_BUY, spredMax, deviationMax);
   gridLong.setGridLotStrategy(lotStategyLong);
   gridLong.setTradeStrategy(tradeStategyLong);
   gridLong.setGridStrategy(gridStategyLong);
   gridLong.setGridSignalStrategy(signalStategyLong);
   //gridLong.setGridProfitStrategy(profitStategyLong);
   gridLong.setGridPiramidingStrategy(piramidingStategyLong);


//////
   //FanTrendDetectorSignal *fanTrandSignalShort = new FanTrendDetectorSignal(_Period);
   StochasticSignal *stochSignalShort = new StochasticSignal(_Period, 7, 3, 3, 80, 20);
   stochSignalShort.setSignalsSettings(EnterType, EXIT_TYPE_STOCH_LEVEL_INTERSECT);
   //RsiLaguerreSignal *rsiLaguerreSignal = new RsiLaguerreSignal(PERIOD_CURRENT, 32, 0.85, 0.15);

   EGridBaseStrategy *gridStategyShort     = new EGridBaseStrategy(levels, levelHeight, minBarsFromLastOrder, showGridVirtualLevels, trailingStopDistanceLevelBase, trailingStopMinDistanceFromBu, trailingStopDynamicEquityKoef, filterType, trailingSlByAtrEnable, trailingSlByAtrKoef);
   //EGridProfitEquityStrategy *profitStategyShort = new EGridProfitEquityStrategy(gridCloseByEquityLoss, virtualStopBUtoTralMinDistance, trailingStopVirtual, trailingStopVirtualPt, trailingVirtualDynamicEquityKoef, virtualStopPriceDistanceLevel, virtualStopPriceDistanceLevelKoef);
   EGridPiramidingBaseStrategy *piramidingStategyShort = new EGridPiramidingBaseStrategy(pirmdEnabled, pirmdLot, pirmdSL, pirmdSL, pirmdKoef, trailingSlByAtrPiramEnable, trailingSlByAtrPiramKoef);
   EGridLotMartingaleStrategy *lotStategyShort = new EGridLotMartingaleStrategy(volume, lotStrategyType, volumeMartingale);
   EGridTradeStrategy *tradeStategyShort = new EGridTradeStrategy(deviationMax, spredMax);
   EGridSignalImpulseStrategy *signalStategyShort = new EGridSignalImpulseStrategy(isSignalTrue);
   signalStategyShort.addSignal(stochSignalShort);
   //signalStategyShort.addSignal(stochSignalShort);
   //signalStategyShort.addSignal(rsiLaguerreSignal);

   EGrid *gridShort = new EGrid(POSITION_TYPE_SELL, spredMax, deviationMax);
   gridShort.setGridLotStrategy(lotStategyShort);
   gridShort.setTradeStrategy(tradeStategyShort);
   gridShort.setGridStrategy(gridStategyShort);
   gridShort.setGridSignalStrategy(signalStategyShort);
   //gridShort.setGridProfitStrategy(profitStategyShort);
   gridShort.setGridPiramidingStrategy(piramidingStategyShort);


///////
   gridManager = new EGridManager(magic, true);
   
   if(longEnabled)
      gridManager.addGrid(gridLong);
      
   if(shortEnabled)
      gridManager.addGrid(gridShort);
   
   gridManager.onInit();

   return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   gridManager.onTick();
}

//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   gridManager.OnTradeTransaction(trans, request, result);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void  pauseTester()
{
   if(pauseOnTesterTrade && MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE)) {
      keybd_event(VK_SPACE, 0, 0, 0);
      keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
   }
}


void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   

   gridManager.OnChartEvent(id, lparam, dparam, sparam);
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
