//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>
#include <Emilasp\Quants\TimeQuant.mqh>


input int SL=100; // SL
input int TP=100; // TP

input int quant=1;
input bool isCollectQuants = true;
input string tradeQuants;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
{
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01, const int sl=100, const int tp=100, const string symbol=NULL, double price=0.0, const string comment="");
   bool              emSell(const double volume=0.01, const int sl=100, const int tp=100, const string symbol=NULL, double price=0.0, const string comment="");
};
//--
struct sSignal {
   bool              Buy;
   bool              Sell;

   void              sSignal()
   {
      Buy = false;
      Sell = false;
   }
};






bool on_trade=false;


CEmTrade trade;

TimeQuant *quantObject = new TimeQuant(quant);


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
{
   sSignal res;

   double H[];
   ArraySetAsSeries(H, true);// Indexation from right to left
   CopyHigh(_Symbol, PERIOD_CURRENT, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);// Indexation from right to left
   CopyLow(_Symbol, PERIOD_CURRENT, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
   if(L[1] > last_tick.bid) {
      res.Buy = true;
   }

//--- SELL
   if(H[1] < last_tick.bid) {
      res.Sell = true;
   }

   return res;
}


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   quantObject.setAllowQuants(tradeQuants);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);
   MqlDateTime time;
   TimeToStruct(last_tick.time, time);

   on_trade=false;

   int currentQuant = quantObject.valueToQuant(last_tick.time);

   if (isCollectQuants) {
      on_trade=quantObject.isCurrentCollectQuant(currentQuant);
   } else {
      on_trade = quantObject.allowTradeQuant(currentQuant);
   }


   if(on_trade) {
      sSignal signal = Buy_or_Sell();

      if(signal.Buy == true) {
         if(!PositionSelect(_Symbol)) {
            trade.emBuy();
         }
      }
      if(signal.Sell == true) {
         if(!PositionSelect(_Symbol)) {
            trade.emBuy();
         }
      }
   }
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
{
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01, const int sl=100, const int tp=100, const string symbol=NULL, double price=0.0, const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name, SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name, ORDER_TYPE_BUY, volume, price, price_SL, price_TP, comment));
}
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01, const int sl=100, const int tp=100, const string symbol=NULL, double price=0.0, const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name, SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name, ORDER_TYPE_SELL, volume, price, price_SL, price_TP, comment));
}
//+------------------------------------------------------------------+
