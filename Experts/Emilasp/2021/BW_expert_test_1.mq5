//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\BwSystem\OrdersManager.mqh>
#include <Emilasp\BwSystem\AnalizeManager.mqh>
#include <Emilasp\BwSystem\Common\StopLossManager.mqh>
#include <Emilasp\BwSystem\Common\PyramidingManager.mqh>
#include <Emilasp\BwSystem\Common\TsHelper.mqh>
/////////////

int magic = 71222023;

input group "Base"
input double volume = 0.01;
input int SL=70;
input int TP=50;

input group "StopLoss"
input ENUM_STOP_LOSS_TYPE stopLossType = STOP_LOSS_TYPE_PREV_BAR; // Trailing STOP type
input double stopLossAdditionalPt = 0;
input double stopLossKoef = 1;

input group "Trailing"
input bool trailingStop = true; // Trailing STOP
input ENUM_STOP_LOSS_TYPE trailingStopType = STOP_LOSS_TYPE_PREV_BAR; // Trailing STOP type
input double trailingAdditionalPt = 0;
input double trailingKoef = 3;

input bool pyramidingEnabled = true;
input ENUM_PYRAMIDING_TYPE pyramidingType = PYRAMIDING_FRACTAL;

input bool clearOrdersIfCreateRealOrder; // Удалять виртуальные ордера при реальной сделке
input bool clearOrdersIfNoCreateRealOnNextBar; // Удалять виртуальные ордера если небыло реальной следки на сл баре
input bool isModeNotification;
input bool isModeDebug;

input group "Signal: deviation bar"
input ENUM_CLOSE_TYPES prevBarType = ALL;
input double minCloseFilterPercent = 0.7;

input group "Signal: Alligator"
input int alligatoHistoryBarsAnalizeCount = 200; // количество баров для поиска минимального и максимального значения аллигатора(в начале)
input double alligatoFlatPercentByMin = 1.5; // Считаем, что флет если дистанция аллигатор меньше чем flatPercentByMin от minDistance
input int alligatorTrandByFlatDistance = 6; // Через сколько баров после пересечения считаем, что тренд наступил.

input group "Signal: Trand fractal Timeframes"
input bool trandFractalFilterEnabled=false;
input int trandFractalType=3;
input int trandFractalTf1=PERIOD_M15;
input int trandFractalTf2=PERIOD_H1;
input int trandFractalTf3=PERIOD_H4;

input group "Signal: Fractals"
input int fractalTypeForInd=5;

input group "Learning"
input bool modeLearning=true;



/////////////////////

CoreTradeHadge      *trade;
OrdersManager    *ordersManager;
AnalizeManager      *analizeManager;
StopLossManager      *stopLossManager;
PyramidingManager      *pyramidingManager;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(_Period, magic);

   analizeManager = new AnalizeManager();
   analizeManager.trade = trade;
   
   analizeManager.trandFractalFilterEnabled=true;
   analizeManager.trandFractalType=trandFractalType;
   analizeManager.trandFractalTf1=trandFractalTf1;
   analizeManager.trandFractalTf2=trandFractalTf2;
   analizeManager.trandFractalTf3=trandFractalTf3;

   analizeManager.prevBarType=prevBarType;
   analizeManager.minCloseFilterPercent=minCloseFilterPercent;
   analizeManager.trandFractalType=fractalTypeForInd;

   analizeManager.alligatoHistoryBarsAnalizeCount=alligatoHistoryBarsAnalizeCount;
   analizeManager.alligatoFlatPercentByMin=alligatoFlatPercentByMin;
   analizeManager.alligatorTrandByFlatDistance=alligatorTrandByFlatDistance;

   
   stopLossManager = new StopLossManager(modeLearning);
   stopLossManager.stopLossSetup(STOP_LOSS_TYPE_PREV_BAR,stopLossAdditionalPt,stopLossKoef);
   stopLossManager.trailingSetup(true,STOP_LOSS_TYPE_PREV_BAR,trailingAdditionalPt,trailingKoef);
   stopLossManager.analizeManager = analizeManager;
   
   pyramidingManager = new PyramidingManager(pyramidingEnabled,pyramidingType);

   ordersManager = new OrdersManager(magic, volume, clearOrdersIfCreateRealOrder, clearOrdersIfNoCreateRealOnNextBar, isModeNotification, modeLearning, true);

   ordersManager.analizeManager = analizeManager;
   ordersManager.stopLossManager = stopLossManager;
   ordersManager.pyramidingManager = pyramidingManager;
   
   analizeManager.OnInit();

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}



//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   analizeManager.OnTick();
   ordersManager.OnTick();
}
//+------------------------------------------------------------------+
