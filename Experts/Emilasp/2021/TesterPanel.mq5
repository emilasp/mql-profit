//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>
#include <Emilasp\libraries\Telegram\TelegramChat.mqh>
#include <Emilasp\TradePanel\TradePanel.mqh>
/////////////

input group "Робот"
input int magicRobot = 0;

input group "Настройки тренажера"
int tpPt = 40;
int slPt = 20;

double risk = 0.005;
double volume=0;
double volumeMin=0.1;

CoreTradeHadge    *trade;
TradePanel *tradePanel;

int magic = magicRobot;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if(magic == 0)
      magic = MathRand() + MathRand();

//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);
/////////////////////////////////////////////

   tradePanel = new TradePanel(trade, tpPt, slPt, volumeMin);
   tradePanel.onInit();

   return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   tradePanel.onTick();

//if(trade.isNewBar(5)) {
//}
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   tradePanel.onChartEvent(id, lparam, dparam, sparam);
}
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   tradePanel.onDeinit();
}


//+------------------------------------------------------------------+
