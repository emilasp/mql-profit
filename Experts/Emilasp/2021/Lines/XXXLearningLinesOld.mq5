//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#import "user32.dll"
void keybd_event(int bVk, int bScan, int dwFlags, int dwExtraInfo);
#import
#define VK_SPACE 0x20 //Space
#define VK_RETURN 0x0D //Return - Enter Key
#define KEYEVENTF_KEYUP 0x0002  //Key up

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>

/////////////

enum ENUM_GET_EXTREMUM_TYPES {
   GET_EXTREMUM_TYPE_RSI = 1 // RSI
};

input group "Робот"
input int magicRobot = 0;
input bool pauseOnTesterTrade = true; // Останавливать по сигналу

input group "Настройки индикатора"
input int senssetiveOverLevelpt=5; // Чувствительность к пробитию уровня
input int minImpulseLevelDistancePt=15; // Минимальное количество пунктов между уровнями

input ENUM_GET_EXTREMUM_TYPES type  = GET_EXTREMUM_TYPE_RSI; // Способ определения уровня
input int indicatorPeriod = 4; // период Индикатора
input int indicatorLevelTop = 65; // Зона перекупленности Индикатора
input int indicatorLevelBottom = 35; // Зона перепроданности Индикатора




int handleATR;
int handleTFcur;
int handleTFnxt1;
int handleTFnxt2;

double         BufferExtermumsPrice[]; // буфер со всеми ценами экстремумов
double         BufferExtermumsAll[]; // буфер со всеми экстремумами: 0 - нет, 1 - максимум, -1 - минимум
double         BufferLineType[]; // буфер с типами линий: 0 - нет, 1 - пробойный, 2 - внутриканальный
double         PatternBuffer[]; // текущий паттерн: 0 - флет/треугольник, 1 - бычий импульс, 2 - бычья коррекция, -1 - меджвежий импульс, -2 - медвежья коррекция
double         SignalBuffer[]; // все сигналы: 0 - нет, 1 - пробой, 2 - отбой
double         VolatilyBuffer[]; // Разница между линией поддержки и сопротивления
double         TrandContinuesBuffer[]; // буфер с числом подряд пробоев: -n - пробои медвежьи, 0 - не было пробоев(был флет/треугольник), +n - пробои бычьи
double         TrandStrengthBuffer[]; // сила тренда - чем меньше средняя коррекция к среднему импульсу, тем больше сила тренда
double         BufferAtr[];

double         PatternBufferNxt1[];
double         TrandContinuesBufferNxt1[];
double         PatternBufferNxt2[];
double         TrandContinuesBufferNxt2[];

CoreTradeHadge    *trade;

int magic = magicRobot;

int tick = 0;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if(magic == 0)
      magic = MathRand() + MathRand();
      
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);
/////////////////////////////////////////////

   handleTFcur = iCustom(_Symbol, _Period, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleTFnxt1 = iCustom(_Symbol, PERIOD_M15, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleTFnxt2 = iCustom(_Symbol, PERIOD_H1, "Emilasp\\LinesV3", senssetiveOverLevelpt, minImpulseLevelDistancePt, type, indicatorPeriod);
   handleATR   = iATR(_Symbol, _Period, 13);

   ArraySetAsSeries(SignalBuffer, true);
   ArraySetAsSeries(BufferLineType, true);
   ArraySetAsSeries(PatternBuffer, true);
   ArraySetAsSeries(TrandContinuesBuffer, true);
   ArraySetAsSeries(VolatilyBuffer, true);
   ArraySetAsSeries(BufferAtr, true);
   
   ArraySetAsSeries(PatternBufferNxt1, true);
   ArraySetAsSeries(TrandContinuesBufferNxt1, true);
   ArraySetAsSeries(PatternBufferNxt2, true);
   ArraySetAsSeries(TrandContinuesBufferNxt2, true);
   
   
   DrawHelper::drawRectangleLabel(0, "settings", 0, 0, 0, 300, 100, clrBlack, BORDER_FLAT,CORNER_LEFT_UPPER, 255,STYLE_SOLID, 1, false, false);
   
   return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   CopyBuffer(handleTFcur, 5, 0, 2, SignalBuffer);
   CopyBuffer(handleTFcur, 3, 0, 2, BufferLineType);
   CopyBuffer(handleTFcur, 4, 0, 2, PatternBuffer);
   CopyBuffer(handleTFcur, 7, 0, 1, TrandContinuesBuffer);
   CopyBuffer(handleTFcur, 6, 0, 1, VolatilyBuffer);
 
   CopyBuffer(handleTFnxt1, 4, 0, 1, PatternBufferNxt1);
   CopyBuffer(handleTFnxt1, 7, 0, 1, TrandContinuesBufferNxt1);
   CopyBuffer(handleTFnxt2, 4, 0, 1, PatternBufferNxt2);
   CopyBuffer(handleTFnxt2, 7, 0, 1, TrandContinuesBufferNxt2);
   
   CopyBuffer(handleATR, 0, 0, 2, BufferAtr);
   
   tick++;
   if(trade.isNewBar(5)) {
      if(SignalBuffer[1] == 1) {
         pauseTester();
         
         datetime prevBarTime = iTime(_Symbol,_Period, 1);
         double prevBarHigh   = iHigh(_Symbol,_Period, 1);
         
         DrawHelper::drawArrow("Pointer" + IntegerToString(tick), prevBarTime, prevBarHigh + 50 * _Point, 159, clrGreenYellow);
         
         if(BufferLineType[1] == 1) {
            tick++;
         }
         if(BufferLineType[1] == 2) {
            tick++;
         }
      }
      if(SignalBuffer[1] == -1) {
        pauseTester();
        
        datetime prevBarTime = iTime(_Symbol,_Period, 1);
        double prevBarLow    = iLow(_Symbol,_Period, 1);
        
        DrawHelper::drawArrow("Pointer" + IntegerToString(tick), prevBarTime, prevBarLow -  30 * _Point, 159, clrGreenYellow);
      }
   }

   DrawHelper::drawLabel("Pattern", "Pattern: " + getPatternText(PatternBuffer[0]) + " : " + getPatternText(PatternBufferNxt1[0]) + " : " + getPatternText(PatternBufferNxt2[0]), 10, 20);
   DrawHelper::drawLabel("Continues", "Continues: " + IntegerToString(TrandContinuesBuffer[0]) + " : " + IntegerToString(TrandContinuesBufferNxt1[0]) + " : " + IntegerToString(TrandContinuesBufferNxt2[0]), 10, 40);
   DrawHelper::drawLabel("Volatily", "Volatily: " + IntegerToString(VolatilyBuffer[0], 1), 10, 60);
}


//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}

string getCurrentTF() {
   switch(Period()) {
   case PERIOD_M1:
      return("M1");
   case PERIOD_M5:
      return("M5");
   case PERIOD_M15:
      return("M15");
   case PERIOD_M30:
      return("M30");
   case PERIOD_H1:
      return("H1");
   case PERIOD_H4:
      return("H4");
   case PERIOD_D1:
      return("D1");
   case PERIOD_W1:
      return("W1");
   case PERIOD_MN1:
      return("MN1");
   default:
      return("Unknown timeframe");
   }
}
//+------------------------------------------------------------------+
string getPatternText(int patternVal)
{
   switch(patternVal) {
   case 1:
      return "↑ imp";
   case 2:
      return "↑ corr";
   case -1:
      return "↓ imp";
   case -2:
      return "↓ corr";
   }
   return "FLET";
}
//+------------------------------------------------------------------+

void  pauseTester()
{
   if(pauseOnTesterTrade && MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE)) {
      keybd_event(VK_SPACE, 0, 0, 0);
      keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
   }
}

/**ResetLastError();
int result=
if(result==-1)
  {
   Print("CopyBuffer error# ", GetLastError());
   return;
  }
if(result<1)
  {
   Print("Ordered ",IntegerToString(1),", received ",IntegerToString(result));
   return;
  }*/
//+------------------------------------------------------------------+
