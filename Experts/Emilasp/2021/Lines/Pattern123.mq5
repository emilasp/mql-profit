//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\Lines\LinesRobotBase.mqh>
#include <Emilasp\Lines\Patterns\LinesPattern123.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>
/////////////

input group "Робот"
input int magicRobot = 0;
input bool pauseOnTesterTrade = true; // Останавливать по сигналу

input group "Настройки паттерна"
input int senssetiveLinePt=20; // Чувствительность к уровню паттерна

input group "Настройки индикатора"
input int senssetiveOverLevelpt=5; // Чувствительность к пробитию уровня
input int minImpulseLevelDistancePt=15; // Минимальное количество пунктов между уровнями

input ENUM_GET_EXTREMUM_TYPES type  = GET_EXTREMUM_TYPE_RSI; // Способ определения уровня
input int indicatorPeriod = 6; // период Индикатора
input int indicatorLevelTop = 65; // Зона перекупленности Индикатора
input int indicatorLevelBottom = 35; // Зона перепроданности Индикатора

CoreTradeHadge    *trade;
LinesRobotBase    *linesRobotBase;
LinesPattern123   *pattern;
int magic = magicRobot;

int tick = 0;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if(magic == 0)
      magic = MathRand() + MathRand();

//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);
/////////////////////////////////////////////
   linesRobotBase = new LinesRobotBase();
   linesRobotBase.setupIndicator(type, senssetiveOverLevelpt, minImpulseLevelDistancePt, indicatorPeriod, indicatorLevelTop, indicatorLevelBottom);
   linesRobotBase.onInit();


   pattern = new LinesPattern123(linesRobotBase, senssetiveLinePt);

   return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   linesRobotBase.onTick();

   tick++;
   if(trade.isNewBar(5)) {
      linesRobotBase.onNewBar();

      pattern.calculatePattern();

      if(pattern.isPattern) {
         int tt = 10;
      }
   }
}


//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   linesRobotBase.onDeinit();
}
//+------------------------------------------------------------------+
