//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\Lines\LinesRobotBase.mqh>
#include <Emilasp\helpers\TesterHelper.mqh>
#include <Emilasp\TradePanel\TradePanel.mqh>

/////////////

input group "Робот"
input int magicRobot = 0;
input bool pauseOnTesterTrade = true; // Останавливать по сигналу

input group "Настройки индикатора"
input int senssetiveOverLevelpt=5; // Чувствительность к пробитию уровня
input int minImpulseLevelDistancePt=15; // Минимальное количество пунктов между уровнями

input ENUM_GET_EXTREMUM_TYPES type  = GET_EXTREMUM_TYPE_RSI; // Способ определения уровня
input int indicatorPeriod = 4; // период Индикатора
input int indicatorLevelTop = 65; // Зона перекупленности Индикатора
input int indicatorLevelBottom = 35; // Зона перепроданности Индикатора

input group "Настройки тренажера"
input int tpPt = 40;
input int slPt = 20;

input double risk = 0;
input double volume=0.1;
input double volumeMin=0.1;

CoreTradeHadge    *trade;
TradePanel        *tradePanel;
LinesRobotBase    *linesRobotBase;

int magic = magicRobot;

int tick = 0;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if(magic == 0)
      magic = MathRand() + MathRand();

//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);
/////////////////////////////////////////////
   tradePanel = new TradePanel(trade, tpPt, slPt, volumeMin, volume, risk);
   tradePanel.onInit();

   linesRobotBase = new LinesRobotBase();
   linesRobotBase.setupIndicator(type, senssetiveOverLevelpt, minImpulseLevelDistancePt, indicatorPeriod, indicatorLevelTop, indicatorLevelBottom);
   linesRobotBase.onInit();
   
   return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   linesRobotBase.onTick();

   tick++;
   if(trade.isNewBar(5)) {
      linesRobotBase.onNewBar();

      if(linesRobotBase.SignalBuffer[1] == 1) {
         if(pauseOnTesterTrade) {
            TesterHelper::pause();
         }

         datetime prevBarTime = iTime(_Symbol, _Period, 1);
         double prevBarHigh   = iHigh(_Symbol, _Period, 1);

         DrawHelper::drawArrow("Pointer" + IntegerToString(tick), prevBarTime, prevBarHigh + 50 * _Point, 159, clrGreenYellow);

         if(linesRobotBase.BufferLineType[1] == 1) {
            tick++;
         }
         if(linesRobotBase.BufferLineType[1] == 2) {
            tick++;
         }
      }
      if(linesRobotBase.SignalBuffer[1] == -1) {
         if(pauseOnTesterTrade) {
            TesterHelper::pause();
         }

         datetime prevBarTime = iTime(_Symbol, _Period, 1);
         double prevBarLow    = iLow(_Symbol, _Period, 1);

         DrawHelper::drawArrow("Pointer" + IntegerToString(tick), prevBarTime, prevBarLow -  30 * _Point, 159, clrGreenYellow);
      }
   }

   tradePanel.onTick();
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
   tradePanel.onChartEvent(id, lparam, dparam, sparam);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   linesRobotBase.onDeinit();
   tradePanel.onDeinit();
}
//+------------------------------------------------------------------+
