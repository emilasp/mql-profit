//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\trade\CoreTradeHadge.mqh>

#include <Emilasp\libraries\Telegram\TelegramChat.mqh>

/////////////

enum ENUM_SIGNAL_TYPE
  {
   SIGNAL_TYPE_REBOUND = 1,
   SIGNAL_TYPE_BREAKDOWN = 2
  };

input group "Робот"
input int magicRobot = 0;

input group "Настройки индикатора"
input int levelsCount=5;
input int minBarLevelDistance=5;
input int minDistanceToNarrowChannel=5;
input int overLevelSenssetive=5;
input int impulseLevelSenssetivePt=35;

input ENUM_SIGNAL_TYPE signalType=SIGNAL_TYPE_REBOUND;

int handleTFcur;
int handleTF1;
int handleTF2;
int handleATR;

double BreakOutBarBuffer[];
double PatternBuffer[];
double TrandContinuesBuffer[];
double VolatilyBuffer[];
double SignalReboundBuffer[];
double BufferAtr[];

CoreTradeHadge    *trade;
TelegramChat    *chat;

int magic = magicRobot;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if(magic == 0)
      magic = MathRand() + MathRand();

   chat = new TelegramChat("2012454312:AAE7rY93NOjYiKI-_LzWTVe1c51FQUilyWE", "-518922786");

//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);
/////////////////////////////////////////////

   handleTFcur = iCustom(_Symbol, _Period, "Emilasp\\LinesV2", levelsCount, minBarLevelDistance, minDistanceToNarrowChannel, overLevelSenssetive, impulseLevelSenssetivePt);
   handleATR   = iATR(_Symbol, _Period, 13);

   ArraySetAsSeries(SignalReboundBuffer, true);
   ArraySetAsSeries(BreakOutBarBuffer, true);
   ArraySetAsSeries(PatternBuffer, true);
   ArraySetAsSeries(TrandContinuesBuffer, true);
   ArraySetAsSeries(VolatilyBuffer, true);
   ArraySetAsSeries(BufferAtr, true);

   return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   CopyBuffer(handleTFcur, 8, 0, 1, PatternBuffer);
   CopyBuffer(handleTFcur, 9, 0, 2, BreakOutBarBuffer);
   CopyBuffer(handleTFcur, 10, 0, 2, SignalReboundBuffer);
   CopyBuffer(handleTFcur, 5, 0, 1, TrandContinuesBuffer);
   CopyBuffer(handleTFcur, 4, 0, 1, VolatilyBuffer);
   CopyBuffer(handleATR, 0, 0, 2, BufferAtr);


   if(trade.isNewBar(5)) {
      if(signalType == SIGNAL_TYPE_REBOUND && SignalReboundBuffer[1] != 0) {
         sendSignalToChat(SignalReboundBuffer[1], signalType);
      }
      if(signalType == SIGNAL_TYPE_BREAKDOWN && BreakOutBarBuffer[1] != 0) {
         sendSignalToChat(BreakOutBarBuffer[1], signalType);
      }
   }

   DrawHelper::drawLabel("Pattern", "Pattern: " + getPatternText(PatternBuffer[0]), 10, 20);
   DrawHelper::drawLabel("Continues", "Continues: " + IntegerToString(TrandContinuesBuffer[0]), 10, 40);
   DrawHelper::drawLabel("Volatily", "Volatily: " + IntegerToString(VolatilyBuffer[0], 1), 10, 60);
}


//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void sendSignalToChat(double signal, ENUM_SIGNAL_TYPE typeSignal)
{
   double balance = AccountInfoDouble(ACCOUNT_BALANCE);
   double equity = AccountInfoDouble(ACCOUNT_EQUITY)  - balance;
   double margin = AccountInfoDouble(ACCOUNT_MARGIN);
   double marginFree = AccountInfoDouble(ACCOUNT_FREEMARGIN);
   double credit = AccountInfoDouble(ACCOUNT_CREDIT);
   int spred = SymbolInfoInteger(Symbol(), SYMBOL_SPREAD);

   string data = _Symbol + " " + getCurrentTF() + ": ";
   string name = signalType == SIGNAL_TYPE_REBOUND ? "**Rebound" : "**BreakDown";
   string text = data + name + (signal > 0 ? " UP**" : " DOWN**") + ", equity: " + DoubleToString(equity, 2) + ", Spread: " + IntegerToString(spred);

   Print(text);

   string sddf = chat.sendMessage(text);
   int tt = 10;
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}

string getCurrentTF() {
   switch(Period()) {
   case PERIOD_M1:
      return("M1");
   case PERIOD_M5:
      return("M5");
   case PERIOD_M15:
      return("M15");
   case PERIOD_M30:
      return("M30");
   case PERIOD_H1:
      return("H1");
   case PERIOD_H4:
      return("H4");
   case PERIOD_D1:
      return("D1");
   case PERIOD_W1:
      return("W1");
   case PERIOD_MN1:
      return("MN1");
   default:
      return("Unknown timeframe");
   }
}
//+------------------------------------------------------------------+
string getPatternText(int patternVal)
{
   switch(patternVal) {
   case 1:
      return "Bull impulse";
   case 2:
      return "Bull correction";
   case -1:
      return "Bear impulse";
   case -2:
      return "Bear correction";
   }
   return "FLET";
}
//+------------------------------------------------------------------+


/**ResetLastError();
int result=
if(result==-1)
  {
   Print("CopyBuffer error# ", GetLastError());
   return;
  }
if(result<1)
  {
   Print("Ordered ",IntegerToString(1),", received ",IntegerToString(result));
   return;
  }*/
//+------------------------------------------------------------------+
