//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#import "user32.dll"
void keybd_event(int bVk, int bScan, int dwFlags, int dwExtraInfo);
#import
#define VK_SPACE 0x20 //Space
#define VK_RETURN 0x0D //Return - Enter Key
#define KEYEVENTF_KEYUP 0x0002  //Key up


#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>

#include <Emilasp\Signals\Manager\SignalManager.mqh>
#include <Emilasp\Signals\Base\ASignalBase.mqh>
#include <Emilasp\Signals\KetlerChannelSignal.mqh>
#include <Emilasp\Signals\SmartRmsSignal.mqh>
#include <Emilasp\Signals\FanTrendDetectorSignal.mqh>
#include <Emilasp\Signals\SuperPassbandFilterSignal.mqh>
#include <Emilasp\Signals\StochasticSignal.mqh>
#include <Emilasp\Signals\CciSignal.mqh>
#include <Emilasp\Signals\AdxSignal.mqh>
#include <Emilasp\Signals\RsiLaguerreSignal.mqh>
#include <Emilasp\Signals\StochasticConvergenceSignal.mqh>
///



/////////////

int magic = 71239023;

input double volume = 0.01;
input int SL=50;
input int TP=100;

input bool breakeven = true; // breakeven STOP: Включение
input bool trailingStop = true; // Trailing STOP: Включение
input int TR            = 50; // Trailing pips
input ENUM_ENTER_TYPE EnterType    = ENTER_TYPE_STOCH_LEVEL_INTERSECT; // Тип входа по Стохастику[По уровню или пересечению с сигнальной линией]
input ENUM_EXIT_TYPE ExitType      = EXIT_TYPE_STOCH_SIGNAL_INTERSECT; // Тип выхода по Стохастику[По уровню или пересечению с сигнальной линией]

input bool enabledStoFilter = false; // Stochastic
input bool enabledCciFilter = false; // CCI фильтр
input bool enabledAdxFilter = false; // ADX фильтр

input bool pauseOnTesterTrade = true; // Останавливать тестер при наступлении сделки

input double maxDiff = 2.5;
input double minDiffNext = 3.5;

CoreTradeHadge    *trade;

SignalManager * signalManager;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_CURRENT, magic);
   if (trailingStop)
      trade.trailingStopInit(TR, true);
   //trade.breakevenSl(breakeven);
//trade.martingaleInit(volume);
   trade.interruptionOfLossesInit(2, 30);


   signalManager = new SignalManager();
   
//signalManager.addSignal(new KetlerChannelSignal(PERIOD_CURRENT, trade), ENUM_SIGNAL_CONCATENATE_TYPE_OR, true, 100);
//signalManager.addSignal(new SmartRmsSignal(PERIOD_CURRENT, trade), ENUM_SIGNAL_CONCATENATE_TYPE_OR, true, 100);
//signalManager.addSignal(new FanTrendDetectorSignal(PERIOD_CURRENT, trade), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100);
//signalManager.addSignal(new SuperPassbandFilterSignal(PERIOD_CURRENT, trade), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100);
   
   
   
   // Test RsiLaguerreSignal
   //signalManager.addSignal(new RsiLaguerreSignal(PERIOD_CURRENT, trade, 32, 0.85, 0.15), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100, 1);
   //signalManager.addSignal(new AdxSignal(PERIOD_CURRENT, trade, 140), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100, 2);
   ///////////////////////////
   
   
   signalManager.addSignal(new StochasticConvergenceSignal(PERIOD_CURRENT, trade, maxDiff, minDiffNext), ENUM_SIGNAL_CONCATENATE_TYPE_OR, true, 100);
   
   
   int indexSignal = 1;
   if(enabledStoFilter) {
      ///signalManager.addSignal(stochSignal, ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100, indexSignal);
      //indexSignal++;
   }
   if(enabledCciFilter) {
      //signalManager.addSignal(new CciSignal(PERIOD_CURRENT, trade, 14, 100, -100), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100, indexSignal);
      //indexSignal++;
   }
   if(enabledAdxFilter) {
      //signalManager.addSignal(new AdxSignal(PERIOD_CURRENT, trade, 140), ENUM_SIGNAL_CONCATENATE_TYPE_AND, true, 100, indexSignal);
      //indexSignal++;
   }

   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
   if (trade.isNewBar(1) ) {
      SignalResult result = signalManager.getSignalResult();

      if(!PositionSelect(_Symbol)) {
         if(result.buy) {
            trade.Buy(volume, TP, SL);
            pauseTester();
         }

         if(result.sell) {
            trade.Sell(volume, TP, SL);
            pauseTester();
         }

         if(result.buyExit) {
            trade.closePositions(POSITION_TYPE_BUY);
         }

         if(result.sellExit) {// && currentBar.isShort && prevBar.isShort
            trade.closePositions(POSITION_TYPE_SELL);
         }
      }
   }

   trade.OnTick();

   if (trade.isNewBar(2) ) {
      signalManager.OnTick();
   }
}

//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   trade.OnTradeTransaction(trans, request, result);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void  pauseTester()
{
   if(pauseOnTesterTrade && MQLInfoInteger(MQL_TESTER) && MQLInfoInteger(MQL_VISUAL_MODE)) {
      keybd_event(VK_SPACE, 0, 0, 0);
      keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
   }
}
//+------------------------------------------------------------------+
