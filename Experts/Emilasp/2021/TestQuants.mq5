//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
#include <Emilasp\BarsPattern\Bar.mqh>

#include <Emilasp\Signals\Base\ASignalBase.mqh>
#include <Emilasp\Signals\KetlerChannelSignal.mqh>
#include <Emilasp\Signals\SmartRmsSignal.mqh>

#include <Emilasp\Quants\TimeQuant.mqh>

/////////////

int magic = 71239023;

input ENUM_TIMEFRAMES timeFrame    = PERIOD_M5;

input double volume = 0.01;
input int SL=100;
input int TP=100;
input bool trailingStop = true; // Trailing STOP
input int TR            = 50; // Trailing pips

input int quant=1;
input bool isCollectQuants = true;
input bool isFullTrade = false;
input string tradeQuants;


CoreTradeHadge    *trade;
//KetlerChannelSignal *signal;
SmartRmsSignal *signal;

TimeQuant *quantObject;

bool on_trade=false;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(timeFrame, magic);
   trade.trailingStopInit(TR, true);

   //signal = new KetlerChannelSignal(PERIOD_M5, trade);
   signal = new SmartRmsSignal(PERIOD_M5, trade);

   quantObject = new TimeQuant(quant);
   quantObject.setAllowQuants(tradeQuants);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);
   MqlDateTime time;
   TimeToStruct(last_tick.time, time);

   on_trade=false;

   int currentQuant = quantObject.valueToQuant(last_tick.time);

   if (isCollectQuants) {
      on_trade=quantObject.isCurrentCollectQuant(currentQuant);
   } else {
      on_trade = quantObject.allowTradeQuant(currentQuant);
   }

   if (trade.isNewBar()) {
      if(on_trade || isFullTrade) {
         if(!PositionSelect(_Symbol)) {
            SignalResult result = signal.getSignal();
            signal.draw();

            if(result.buy) {
               trade.Buy(volume, TP, SL);
            }

            if(result.sell) {
               trade.Sell(volume, TP, SL);
            }
         }
      }
   }

   trade.trailingStopRun();
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
