//+------------------------------------------------------------------+
//|                                NeuroExportFeaturesForPattern.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\QuantExtractor\ContrTrandQuant.mqh>
#include <Emilasp\NeuroNet\QuantExtractor\EveryBarQuant.mqh>

#include <Emilasp\NeuroNet\QuantExtractor\common\ServerRequests.mqh>
#include <Emilasp\trade\ETrade.mqh>

#include <Emilasp\libraries\json\JAson.mqh>
#include <Arrays\ArrayObj.mqh>

//#import "Dll2.dll"
//string  Request(string serv,string page,string content);
//#import



//FibbonacchyBuyPattern *patternExtractor;
AQuantBase *quant;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   int timeFrames[3] = {PERIOD_M5, PERIOD_M15, PERIOD_H1};
   //quant = new ContrTrandQuant(POSITION_TYPE_SELL, timeFrames, false, true);
   quant = new EveryBarQuant(POSITION_TYPE_BUY, timeFrames, false, true, false); // type, enableTrade, enableCollect, debug
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   quant.onTick();
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
{
//---

}
//+------------------------------------------------------------------+
