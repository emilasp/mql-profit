//+------------------------------------------------------------------+
//|                                NeuroExportFeaturesForPattern.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\NeuroNet\PatternExtractor\patterns\FibbonacchyBuyPattern.mqh>
#include <Emilasp\NeuroNet\PatternExtractor\patterns\ConsolidationNeurolPatternScreenShot.mqh>

#include <Emilasp\trade\ETrade.mqh>

ETrade *trade;

//FibbonacchyBuyPattern *patternExtractor;
ConsolidationNeurolPatternScreenShot *patternExtractor;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   trade = new ETrade(2342342);
   
   //patternExtractor = new FibbonacchyBuyPattern();
   patternExtractor = new ConsolidationNeurolPatternScreenShot();
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//--- trade.onTick();
   trade.m_symbol.RefreshRates();

   if(trade.isNewBar())
     {
      patternExtractor.calculate();
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   
  }
//+------------------------------------------------------------------+
