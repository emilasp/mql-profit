//+------------------------------------------------------------------+
//|                                               ExpertDimaBase.mqh |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\ArrayObj.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ExpertBase
{
private:
   CArrayObj         signals;


public:
                     ExpertBase();
                    ~ExpertBase();
   void              onTick();
   void              onInit();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExpertBase::ExpertBase()
{
}

void ExpertBase::onInit()
{
}

void ExpertBase::onTick()
{
}








//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExpertBase::~ExpertBase()
{
}
//+------------------------------------------------------------------+
