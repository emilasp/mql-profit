//+------------------------------------------------------------------+
//|                                                    FirstTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\CoreTradeHadge.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>
/////////////

enum TRAND_BY {
   TRAND_MA  = 1,
   TRAND_ADX = 2
};

int magic = 71239023;

input bool signalStoch1Enable       = true;  // Stochastic signal 1
input bool signalStoch2Enable       = true;  // Stochastic signal 2
input double signalStochLevelUp    = 80.0;  // Stochastic: level UP
input double signalStochLevelDown  = 20.0;  // Stochastic: level DOWN
input int signalStochKperiod       = 5; // Stochastic: K period
input int signalStochDperiod       = 3; // Stochastic: D period
input int signalStochSlowing       = 3; // Stochastic: Slowing

input bool signalCci1Enable                 = true;  // CCI signal 1
input bool signalCci2Enable                 = true;  // CCI signal 2
input double signalCciLevelUp               = 100.0;  // CCI: level UP
input double signalCciLevelDown             = -100.0;  // CCI: level DOWN
input int signalCciMAperiod                 = 14; // CCI: period
input ENUM_APPLIED_PRICE signalCciPriceType = PRICE_CLOSE; // CCI: Price type

input bool signalAnd = true; // Оба сигнала должны сработать

input TRAND_BY trandBy = TRAND_MA; // Тип определения тренда

input double volume = 0.01;
input int SL=100;
input int TP=100;
input bool trailingStop = true; // Trailing STOP
input int TR            = 50; // Trailing pips

input int TrandMaPeriod = 21;
input int TrandAdxPeriod = 21;
input ENUM_TIMEFRAMES TrandMaTimeframe = PERIOD_D1; // Тренд Таймфрейм
/////////////////////
int handle_STOCH;
int handle_CCI;

int handle_MA;
int handle_ADX;

CoreTradeHadge    *trade;

bool on_tral = false;

struct sSignal {
   bool              Buy;
   bool              Sell;
   void              sSignal()
   {
      Buy=false;
      Sell=false;
   }
};

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
{
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

   bool is_CCI_signal_buy1 = false;
   bool is_CCI_signal_buy2 = false;
   bool is_CCI_signal_sell1 = false;
   bool is_CCI_signal_sell2 = false;

   bool is_STOCH_signal_buy1 = false;
   bool is_STOCH_signal_buy2 = false;
   bool is_STOCH_signal_sell1 = false;
   bool is_STOCH_signal_sell2 = false;

////// Stochastic ////////
   double Stochastic[];
   ArraySetAsSeries(Stochastic, true);
   CopyBuffer(handle_STOCH, 0, 0, 2, Stochastic);
   double StochasticSignal[];
   ArraySetAsSeries(StochasticSignal, true);
   CopyBuffer(handle_STOCH, 1, 0, 2, StochasticSignal);
// BUY
   if (Stochastic[1] < signalStochLevelDown && Stochastic[0] > signalStochLevelDown) {
      is_STOCH_signal_buy1 = true;
   }
   if (Stochastic[0] < signalStochLevelDown) {
      is_STOCH_signal_buy2 = true;
   }
// SELL
   if (Stochastic[1] > signalStochLevelUp && Stochastic[0] < signalStochLevelUp) {
      is_STOCH_signal_sell1 = true;
   }
   if (Stochastic[0] > signalStochLevelUp) {
      is_STOCH_signal_sell2 = true;
   }


////// CCI ////////
   double CCI[];
   ArraySetAsSeries(CCI, true);
   CopyBuffer(handle_CCI, 0, 0, 3, CCI);
// BUY
   if (CCI[0] < signalCciLevelDown && CCI[2] > CCI[1] && CCI[1] < CCI[0] && signalStoch1Enable) {
      is_CCI_signal_buy1 = true;
   }
   if (CCI[2] > CCI[1] && CCI[1] < CCI[0] && signalStoch2Enable) {
      is_CCI_signal_buy2 = true;
   }
// SELL
   if (CCI[0] > signalCciLevelUp && CCI[2] < CCI[1] && CCI[1] > CCI[0] && signalStoch1Enable) {
      is_CCI_signal_sell1 = true;
   }
   if (CCI[2] < CCI[1] && CCI[1] > CCI[0] && signalStoch2Enable) {
      is_CCI_signal_sell2 = true;
   }

   int trand = trandCalc();

   if (signalAnd) {
      if(trand > 0 && is_STOCH_signal_buy1 && is_CCI_signal_buy1) {
         res.Buy=true;
      }
      if(trand > 0 && is_STOCH_signal_buy2 && is_CCI_signal_buy2) {
         res.Buy=true;
      }
      if(trand < 0 && is_STOCH_signal_sell1 && is_CCI_signal_sell1) {
         res.Sell=true;
      }
      if(trand < 0 && is_STOCH_signal_sell2 && is_CCI_signal_sell2) {
         res.Sell=true;
      }
   } else {
      if(trand > 0 && is_STOCH_signal_buy1 || is_CCI_signal_buy1) {
         res.Buy=true;
      }
      if(trand > 0 && is_STOCH_signal_buy2 || is_CCI_signal_buy2) {
         res.Buy=true;
      }
      if(trand < 0 && is_STOCH_signal_sell1 || is_CCI_signal_sell1) {
         res.Sell=true;
      }
      if(trand < 0 && is_STOCH_signal_sell2 || is_CCI_signal_sell2) {
         res.Sell=true;
      }
   }
// Draw
   if (res.Buy) {
      DrawHelper::drawArrow("buy:" + last_tick.time, last_tick.time, last_tick.bid, 233, clrGreen);
   }
   if (res.Sell) {
      DrawHelper::drawArrow("sell:" + last_tick.time, last_tick.time, last_tick.bid, 234, clrRed);
   }
   return res;
}
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   trade = new CoreTradeHadge(_Period, magic);
   trade.Init(PERIOD_M1, magic);

   handle_STOCH = iStochastic(_Symbol, _Period, signalStochKperiod, signalStochDperiod, signalStochSlowing, MODE_SMA, STO_LOWHIGH);
   handle_CCI = iCCI(_Symbol, _Period, signalCciMAperiod, signalCciPriceType);
   handle_MA = iMA(_Symbol, TrandMaTimeframe, TrandMaPeriod, 0, MODE_EMA, PRICE_CLOSE);
   handle_ADX = iADX(_Symbol, _Period, TrandAdxPeriod);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
   if(!PositionSelect(_Symbol)) {
      on_tral = false;

      sSignal signal=Buy_or_Sell();

      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);

      if(signal.Buy) {
         trade.Buy(volume, TP, SL);
      }

      if(signal.Sell) {
         trade.Sell(volume, TP, SL);
      }
   }

   trailing();
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int trandCalc()
{
   int res = 0;

   if (trandBy == TRAND_MA) {
      double MA[];
      ArraySetAsSeries(MA, true);
      CopyBuffer(handle_MA, 0, 0, 2, MA);

      if (MA[1] < MA[0]) {
         res = 1;
      }
      if (MA[1] > MA[0]) {
         res = -1;
      }
   }

   if (trandBy == TRAND_ADX) {
      double         ADXBuffer[];
      double         DI_plusBuffer[];
      double         DI_minusBuffer[];

      ArraySetAsSeries(ADXBuffer, true);
      ArraySetAsSeries(DI_plusBuffer, true);
      ArraySetAsSeries(DI_minusBuffer, true);
      
      
   }

   return res;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void trailing()
{
   if(trailingStop && PositionSelect(_Symbol)) {
      if(PositionGetDouble(POSITION_PROFIT)) {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point) {
               on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > PositionGetDouble(POSITION_SL)) {
                  trade.PositionModify(_Symbol, price, 0);
               }
            }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
            if(last_tick.ask<price-TR*_Point) {
               on_tral = true;
               price = last_tick.ask + TR*_Point;
               if(price < PositionGetDouble(POSITION_SL) || PositionGetDouble(POSITION_SL) == NULL) {
                  trade.PositionModify(_Symbol, price, 0);
               }
            }
         }
      }
   }
}
//+------------------------------------------------------------------+
