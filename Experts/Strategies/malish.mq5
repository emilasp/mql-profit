//+------------------------------------------------------------------+
//|                                                       malish.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\ETrade.mqh>

ETrade *trade;


int handle_ma3_d1;
int handle_ma3_h1;
bool on_enter = false;

int prev_day=0;

input int TR=23;
input int SL=23;
input int TP=80;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   trade = new ETrade(2342342);
   trade.INIT();

   handle_ma3_d1 = iMA(_Symbol, PERIOD_D1,3, 0,MODE_SMA,PRICE_LOW);
   handle_ma3_h1 = iMA(_Symbol, PERIOD_H1,3, 0,MODE_SMA,PRICE_LOW);

   ChartIndicatorAdd(0,0, handle_ma3_h1);
   ChartIndicatorAdd(0,1, handle_ma3_d1);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---

   trade.onTick();

   MqlDateTime start;
   datetime res_start=TimeCurrent(start);
   if(start.day_of_year > prev_day)
     {
      prev_day = start.day_of_year;
      on_enter = true;
     }

// Enter
   if(trade.isNewBar())
     {
      if(!PositionSelect(_Symbol) && on_enter)
        {
         double MA_day[];
         CopyBuffer(handle_ma3_d1,0,0,2,MA_day);
         ArraySetAsSeries(MA_day,true);

         MqlRates ratesDay[];
         ArraySetAsSeries(ratesDay,true);
         int copied=CopyRates(Symbol(),PERIOD_D1,0,2,ratesDay);

         double directBar = ratesDay[1].close - ratesDay[1].open;
         if(directBar > 0 && ratesDay[1].close > MA_day[1])
           {
            //Print("Day prev: " + ratesDay[1].time + ", " + directBar2);
            MqlDateTime barTime;
            datetime res_start=TimeCurrent(barTime);
            if(barTime.hour > 8)
              {
               double MA_hour[];
               CopyBuffer(handle_ma3_h1,0,0,2,MA_hour);
               ArraySetAsSeries(MA_hour,true);

               MqlRates ratesHour[];
               ArraySetAsSeries(ratesHour,true);
               int copied=CopyRates(Symbol(),PERIOD_H1,0,2,ratesHour);

               double directBar = ratesHour[1].close - ratesHour[1].open;
               if(directBar > 0 && ratesHour[1].close > MA_hour[1])
                 {
                  double price = trade.m_symbol.Ask();
                  double price_SL = price - SL * _Point * 10;
                  double price_TP = price + TP * _Point * 10;

                  trade.Buy(0.01,_Symbol,price,price_SL,price_TP);
                  on_enter = false;
                 }
              }
           }
        }
     }

      trailinStop();
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+

void trailinStop()
{
// trail
   if(TR>0 && PositionSelect(_Symbol))
     {
      double stopLoss = PositionGetDouble(POSITION_SL);

      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point)
              {
               //on_tral = true;
               price = last_tick.bid - TR*_Point * 10;
               if(price > stopLoss)
                 {
                  trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
                 }
              }

        }
     }
}