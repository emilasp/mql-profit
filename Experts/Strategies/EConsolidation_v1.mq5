//+------------------------------------------------------------------+
//|                                            EConsolidation_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\trade\ETrade.mqh>

input int TR=30;
input int SL=30;
input int TP=100;
input int period_ma=50;

int countProfitOrders = 0;

int handle_consolidation;
int handle_ma_slow;
int handle_ma_fast;
int handle_stoch;

ETrade *trade;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   trade = new ETrade(2342344);
   trade.INIT();

   handle_consolidation = iCustom(_Symbol, _Period, "Strategies\\EConsolidationIndicator");
   handle_ma_slow  = iMA(_Symbol, _Period, period_ma, 0,MODE_SMA,PRICE_CLOSE);
   handle_ma_fast  = iMA(_Symbol, _Period, 21, 0,MODE_SMA,PRICE_CLOSE);
   handle_stoch=iStochastic(_Symbol,_Period,5,3,3,MODE_SMA,STO_LOWHIGH);

   ChartIndicatorAdd(0,1, handle_consolidation);
   ChartIndicatorAdd(0,2, handle_stoch);
   ChartIndicatorAdd(0,0, handle_ma_slow);
   ChartIndicatorAdd(0,0, handle_ma_fast);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   trade.onTick();

// Trade
   if(!PositionSelect(_Symbol))
     {
       Prf();
     
      double MA[];
      CopyBuffer(handle_ma_slow,0,0,2,MA);
      ArraySetAsSeries(MA,true);

      double Consolidation[];
      CopyBuffer(handle_consolidation,1,0,2,Consolidation);
      ArraySetAsSeries(Consolidation,true);
      double ConsolidationHeigth[];
      CopyBuffer(handle_consolidation,3,0,2,ConsolidationHeigth);
      ArraySetAsSeries(ConsolidationHeigth,true);

      double Stoch[];
      CopyBuffer(handle_stoch,0,0,2,Stoch);
      ArraySetAsSeries(Stoch,true);
      double StochSignal[];
      CopyBuffer(handle_stoch,1,0,2,StochSignal);
      ArraySetAsSeries(StochSignal,true);
      
       double lot = countProfitOrders == 1 ? 1 : 0.01;
      
      if(Stoch[1]<20 && Stoch[1] < StochSignal[1] && Stoch[0] > StochSignal[1])
         if(Consolidation[0] == 1 && Consolidation[1] == 1  && MA[0] > MA[1])// && ConsolidationHeigth[1] == 1
           {
            double price = trade.m_symbol.Ask();
            double price_SL = price - SL * _Point;
            double price_TP = price + TP * _Point;

            trade.Buy(lot,_Symbol,price,price_SL,price_TP);
           }
      if(Stoch[1]>80 && Stoch[1] > StochSignal[1] && Stoch[0] < StochSignal[1])
         if(Consolidation[0] == 1 && Consolidation[1] == 1 && MA[0] < MA[1])// && ConsolidationHeigth[1] == 1
           {
            double price = trade.m_symbol.Bid();
            double price_SL = price + SL * _Point;
            double price_TP = price - TP * _Point;

            trade.Sell(lot,_Symbol,price,price_SL,price_TP);
           }
     }

   trailinStop();
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
void trailinStop()
  {
// trail
   if(TR>0 && PositionSelect(_Symbol))
     {
      double stopLoss = PositionGetDouble(POSITION_SL);

      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point)
              {
               //on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > stopLoss)
                 {
                  trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
                 }
              }

         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(last_tick.ask<price-TR*_Point)
              {
               price = last_tick.ask + TR*_Point;
               if(price < stopLoss)
                 {
                  trade.PositionModify(_Symbol, price, 0);
                 }
              }
           }

        }
     }
  }
//+------------------------------------------------------------------+
void Prf()
  {
   if(countProfitOrders)
     {

     }

   int ticketPrev1 = 0;
   int ticketPrev2 = 0;

   if(!HistorySelect(0,TimeCurrent()))
      return;

   ulong ticket;
   for(int i=0; i<HistoryDealsTotal(); i++)
     {
      if((ticket=HistoryDealGetTicket(i))>0)
        {
         if(ticket > ticketPrev1)
           {
            ticketPrev2 = ticketPrev1;
            ticketPrev1 = ticket;
           }
        }
        
      //Print("Tiket(" + ticket + "): " + HistoryDealGetDouble(ticket,DEAL_PROFIT) + " | " + HistoryDealGetDouble(ticket,DEAL_VOLUME));
     }

   countProfitOrders = 0;

   if(ticketPrev1 > 0 && ticketPrev2 > 0)
     {
      double profitPrev1 = HistoryDealGetDouble(ticketPrev1,DEAL_PROFIT);
      double profitPrev2 = HistoryDealGetDouble(ticketPrev2,DEAL_PROFIT);

      if(profitPrev1 > 0)
        {
         if(profitPrev2 <= 0 && profitPrev1 > 0)
           {
            //Print("IS FERST GOOD ORDER!!!!!");
            countProfitOrders=1;
           }
        }
     }
  }