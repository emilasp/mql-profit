//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input int SL=5; // Level stopLoss [profit]
input int TP=50; //  Level taleProfit [profit]

int SL_pt=100;
int TP_pt=100;
int TR=50;  // Level trals [pt]

bool on_tral = false;

double SL_virtual;
double TP_virtual;
string line_SL_name="sl_virtual_line";


int h;
int h_trend;
//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };




//1. Добавить индикатор тренда ТФ 15м-60м
//2. Добавить индикатор Тренд/Флет
//3. Если тренд и тренд в нашу сторону - входим в рынок
//4. Если наступил флет или сменился тренд - выходим из сделок





//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double BUY[];
   ArraySetAsSeries(BUY, true);
   CopyBuffer(h, 0, 0, 1, BUY);

   double SELL[];
   ArraySetAsSeries(SELL, true);
   CopyBuffer(h, 1, 0, 1, SELL);

   double TREND[];
   ArraySetAsSeries(TREND, true);
   CopyBuffer(h_trend, 2, 0, 1, TREND);

   if(TREND[0] > 0 && BUY[0] > 0)//TREND[0] > 0
      res.Buy = true;

   if(TREND[0] < 0 && SELL[0] > 0)
      res.Sell = true;

   Print("Buy: " + BUY[0] + ", SELL: " + SELL[0]);
   Print("PROFIT: " + SELL[0]);

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   h = iCustom(_Symbol, _Period, "Strategies\\Signals\\str_chanal_ma_3_signal");
   h_trend = iCustom(_Symbol, _Period, "Strategies\\trand_indicator_v1");
   ChartIndicatorAdd(1,0,h);
   ChartIndicatorAdd(1,0,h_trend);

   moveHorizontalLine(line_SL_name, 0, true);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(!PositionSelect(_Symbol))
      on_tral = false;


   if(PositionSelect(_Symbol))
     {
      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);

         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid < SL_virtual)
              {
               SL_virtual=0;
               moveHorizontalLine(line_SL_name, 0);
               on_tral = false;
               trade.PositionClose(_Symbol);
              }

         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(last_tick.ask > SL_virtual)
              {
               SL_virtual=0;
               moveHorizontalLine(line_SL_name, 0);
               on_tral = false;
               trade.PositionClose(_Symbol);
              }

            //price = price + SL * _Point;
            //trade.PositionModify(_Symbol, price, 0);
           }
        }
     }



//---
   sSignal signal = Buy_or_Sell();

   if(signal.Buy == true)
     {
      if(!PositionSelect(_Symbol))
        {
         trade.Buy(0.01);
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            trade.PositionClose(_Symbol);
            trade.Buy(0.01);
           }

        }
     }
   if(signal.Sell == true)
     {
      if(!PositionSelect(_Symbol))
        {
         trade.Sell(0.01);
         //trade.emBuy();
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
            trade.PositionClose(_Symbol);
            trade.Sell(0.01);
            //trade.emBuy();
           }

        }
     }


//--- TRAL
   if(TR>0 && PositionSelect(_Symbol))
     {
      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point)
              {
               on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > SL_virtual)
                 {
                  SL_virtual=price;
                  moveHorizontalLine(line_SL_name, price);
                  //trade.PositionModify(_Symbol, price, 0);
                 }
              }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(last_tick.ask<price-TR*_Point)
              {
               on_tral = true;
               price = last_tick.ask + TR*_Point;
               if(price < SL_virtual)
                 {
                  SL_virtual=price;
                  moveHorizontalLine(line_SL_name, price);
                  //trade.PositionModify(_Symbol, price, 0);
                 }
              }
            //price = price + SL * _Point;
            //trade.PositionModify(_Symbol, price, 0);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   if(PositionSelect(_Symbol) && !on_tral)
     {
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      double spred = last_tick.ask-last_tick.bid;
      // StopLoss
      if(SL>0)
        {
            double pip=_Point;
            if(_Digits==5 || _Digits==3) pip=_Point*10;

         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
            double price=NormalizeDouble(PositionGetDouble(POSITION_PRICE_OPEN)-SL*pip,Digits()); //(Ask-(95*Ask/(100*Lots)),Digits)

            SL_virtual=price;
            moveHorizontalLine(line_SL_name, price);
            //trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
           }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            double price = PositionGetDouble(POSITION_PRICE_OPEN)+SL*pip;
            price=NormalizeDouble(price,Digits()); //
            SL_virtual=price;
            moveHorizontalLine(line_SL_name, price);
            //trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
           }


        }
      // TakeProfit
      if(TP>0)
        {

         if(TP>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_STOPS_LEVEL))
           {
            if(TP>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_FREEZE_LEVEL))
              {
               if(TP*_Point > 2*spred)
                  if(PositionGetDouble(POSITION_TP) == NULL)
                    {
                     // Buy
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price + TP * _Point;
                        //trade.PositionModify(_Symbol, PositionGetDouble(POSITION_SL), price);
                       }
                     // Sell
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price - TP * _Point;
                        //trade.PositionModify(_Symbol, PositionGetDouble(POSITION_SL), price);
                       }
                    }
              }
           }



        }
     }
  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void moveHorizontalLine(string name, double price, bool isCreate = false)
  {
   if(isCreate)
     {
      ObjectCreate(0,name,OBJ_HLINE,0,0,0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
     }
   else
     {
      ObjectSetDouble(0,name,OBJPROP_PRICE,price);
     }
  }
//+------------------------------------------------------------------+
