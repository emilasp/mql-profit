//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>
#include <Emilasp\trade\ETrade.mqh>

ETrade *trade;

int handle_SIGNAL;
int handle_ATR;

int handle_TMA;
int handle_CCI;
int handle_CCI_trand;

bool on_tral = false;
input int TR = 15;
input double SL_koef = 1.1;

int countProfitOrders = 0;

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

   double bufferBuy[];
   CopyBuffer(handle_SIGNAL,2,0,1,bufferBuy);
   ArraySetAsSeries(bufferBuy,true);

   double bufferSell[];
   CopyBuffer(handle_SIGNAL,1,0,1,bufferSell);
   ArraySetAsSeries(bufferSell,true);


//--- BUY
   if(bufferBuy[0] > 0)
     {
      res.Sell = true;
     }

//--- SELL
   if(bufferSell[0] > 0)
     {
      res.Buy = true;
     }

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   trade = new ETrade(2342342);

   handle_SIGNAL = iCustom(_Symbol, _Period, "Strategies\\CCI_tma_v1");
   handle_ATR = iATR(_Symbol, _Period,30);

   handle_TMA = iCustom(_Symbol, _Period, "Extreme_TMA_line_indicator");
   handle_CCI = iCCI(_Symbol, _Period,13,PRICE_CLOSE);
   handle_CCI_trand = iCCI(_Symbol, _Period,100,PRICE_CLOSE);

   ChartIndicatorAdd(0,0, handle_SIGNAL);
   ChartIndicatorAdd(0,0, handle_TMA);
   ChartIndicatorAdd(0,1, handle_CCI);
   ChartIndicatorAdd(0,2, handle_CCI_trand);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   trade.onTick();
   trade.m_symbol.RefreshRates();

   if(trade.isNewBar())
     {
      double bufferATR[];
      CopyBuffer(handle_ATR,0,0,1,bufferATR);
      ArraySetAsSeries(bufferATR,true);

      double atr = bufferATR[0] * SL_koef;

      double slBuy = trade.m_symbol.Ask() - atr;
      double tpBuy = trade.m_symbol.Ask() + atr * 3;

      double slSell = trade.m_symbol.Ask() + atr;
      double tpSell = trade.m_symbol.Ask() - atr * 3;

      sSignal signal = Buy_or_Sell();

      double lot = countProfitOrders == 1 ? 5 : 0.01;

      if(!PositionSelect(_Symbol))
        {
         Prf();

         if(signal.Buy == true)
            trade.Buy(lot, _Symbol, 0, slBuy, tpBuy);


         if(signal.Sell == true)
            trade.Sell(lot, _Symbol, 0, slSell, tpSell);
        }
     }

   if(TR>0 && PositionSelect(_Symbol))
     {

      double SL = PositionGetDouble(POSITION_SL);

      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point)
              {
               on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > SL)
                 {
                  trade.PositionModify(_Symbol, price, 0);
                 }
              }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(last_tick.ask<price-TR*_Point)
              {
               on_tral = true;
               price = last_tick.ask + TR*_Point;
               if(price < SL)
                 {
                  trade.PositionModify(_Symbol, price, 0);
                 }
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   Prf();

  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void moveHorizontalLine(string name, double price, bool isCreate = false)
  {
   if(isCreate)
     {
      ObjectCreate(0,name,OBJ_HLINE,0,0,0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
     }
   else
     {
      ObjectSetDouble(0,name,OBJPROP_PRICE,price);
     }
  }
//+------------------------------------------------------------------+
void Prf()
  {
   if(countProfitOrders)
     {

     }

   int ticketPrev1 = 0;
   int ticketPrev2 = 0;

   if(!HistorySelect(0,TimeCurrent()))
      return;

   ulong ticket;
   for(int i=0; i<HistoryDealsTotal(); i++)
     {
      if((ticket=HistoryDealGetTicket(i))>0)
        {
         if(ticket > ticketPrev1)
           {
            ticketPrev2 = ticketPrev1;
            ticketPrev1 = ticket;
           }
        }
        
      //Print("Tiket(" + ticket + "): " + HistoryDealGetDouble(ticket,DEAL_PROFIT) + " | " + HistoryDealGetDouble(ticket,DEAL_VOLUME));
     }

   countProfitOrders = 0;

   if(ticketPrev1 > 0 && ticketPrev2 > 0)
     {
      double profitPrev1 = HistoryDealGetDouble(ticketPrev1,DEAL_PROFIT);
      double profitPrev2 = HistoryDealGetDouble(ticketPrev2,DEAL_PROFIT);

      if(profitPrev1 > 0)
        {
         if(profitPrev2 <= 0 && profitPrev1 > 0)
           {
            //Print("IS FERST GOOD ORDER!!!!!");
            countProfitOrders=1;
           }
        }
     }
  }
//+------------------------------------------------------------------+
