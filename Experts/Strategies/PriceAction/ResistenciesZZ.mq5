//+------------------------------------------------------------------+
//|                                               ResistenciesZZ.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Arrays\Array.mqh>

int handle;

int depth     = 13;
int deviation = 5;
int backstep  = 3;

int hisoryDepth = 1000;
int priceblur=10;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
//---
   handle = iCustom(_Symbol, _Period, "Examples\\ZigZag", depth, deviation, backstep);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick() {
//---
   double buffer[];
   CopyBuffer(handle_SIGNAL, 0, 0, hisoryDepth, buffer);
   ArraySetAsSeries(buffer, true);
   
   CArray *priceWeights=new CArray;
   
   for(int i=0;i<hisoryDepth;i++)
     {
         if(buffer[i] > 0)
           {
            for(int j=0;j<priceWeights.Total();j++)
              {
               if() {
                  
               }
              }
           }
     }

}



//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade() {
//---

}
//+------------------------------------------------------------------+
