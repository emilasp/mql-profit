//+------------------------------------------------------------------+
//|                                                  AllPatterns.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\PatternsManager.mqh>
#include <Emilasp\trade\ETrade.mqh>


PatternsManager *patternsManager;
ETrade *trade;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
//---
   trade = new ETrade(2342342);
   patternsManager = new PatternsManager(10000);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick() {
//---
   if(trade.isNewBar()) {
      patternsManager.SetPatterns();
   }
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade() {
//---

}
//+------------------------------------------------------------------+
