//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input double SL=10.0; // Level stopLoss [profit]
input double TP=2.0; //  Level taleProfit [profit]

int SL_pt=100;
int TP_pt=200;


int h;
int h_trend;
//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };




//1. Добавить индикатор тренда ТФ 15м-60м
//2. Добавить индикатор Тренд/Флет
//3. Если тренд и тренд в нашу сторону - входим в рынок
//4. Если наступил флет или сменился тренд - выходим из сделок





//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double BUY[];
   ArraySetAsSeries(BUY, true);
   CopyBuffer(h, 0, 0, 1, BUY);

   double SELL[];
   ArraySetAsSeries(SELL, true);
   CopyBuffer(h, 1, 0, 1, SELL);

   double TREND[];
   ArraySetAsSeries(TREND, true);
   CopyBuffer(h_trend, 2, 0, 1, TREND);

   if(TREND[0] > 0 && BUY[0] > 0)//TREND[0] > 0 
      res.Buy = true;

   if(TREND[0] < 0 && SELL[0] > 0)
      res.Sell = true;

   Print("Buy: " + BUY[0] + ", SELL: " + SELL[0]);
   Print("PROFIT: " + SELL[0]);

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   h = iCustom(_Symbol, _Period, "Strategies\\Signals\\str_chanal_ma_3_signal");
   h_trend = iCustom(_Symbol, _Period, "Strategies\\trand_indicator_v1");
   ChartIndicatorAdd(1,0,h);
   ChartIndicatorAdd(1,0,h_trend);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(PositionSelect(_Symbol))
     {
      //if(PositionGetDouble(POSITION_PROFIT) > TP)
      //{
      //   Print("CLOSE BY TP");
      //   trade.PositionClose(_Symbol);
      //   }

      //if(PositionGetDouble(POSITION_PROFIT) < -SL) {
      //   Print("CLOSE BY SL");
      //    trade.PositionClose(_Symbol);
      //}
     }
//---


   sSignal signal = Buy_or_Sell();

   if(signal.Buy == true)
     {
     Print("Signal BUY");
      if(!PositionSelect(_Symbol))
        {
         trade.emBuy(0.01, SL_pt, TP_pt);
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            trade.PositionClose(_Symbol);
            trade.emBuy(0.01, SL_pt, TP_pt);
           }

        }
     }
     
     
   if(signal.Sell == true && false)
     {
     Print("Signal SELL");
      if(!PositionSelect(_Symbol))
        {
         trade.Sell(0.01);
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
           Print("Position BUY in sell");
            trade.PositionClose(_Symbol);
            trade.Sell(0.01);
           }

        }
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
