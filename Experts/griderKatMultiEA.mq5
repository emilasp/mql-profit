//+------------------------------------------------------------------+
//|                                                    trendmeEA.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#define pdxversion "200.307"
#property copyright "Klymenko Roman (needtome@icloud.com)"
#property link      "https://www.mql5.com/ru/users/needtome"
#property version   pdxversion
#property strict

   enum TypeOfPos
     {
      MY_BUY,
      MY_SELL,
      MY_BUYSTOP,
      MY_BUYLIMIT,
      MY_SELLSTOP,
      MY_SELLLIMIT,
      MY_BUYSLTP,
      MY_SELLSLTP,
      MY_ALLPOS,
     }; 
   enum TypeOfLang{
      MY_ENG, // English
      MY_RUS, // Русский
   }; 
   enum TypeOfLots //Lots Mode
     {
      MY_FIXED,//Fixed
      MY_ARIFMET,// Arifmet
      MY_GEOMET,// Geomet
     };
   enum TypeOfSeries //Series Mode
     {
      MY_SERIES_PLUS,//In the direction of the series
      MY_SERIES_MINUS,// Against the direction of the series
     };
   enum TypeOfMA //MA Type
     {
      MA_FIRST,//Lower or higher moving
      MA_SECOND,// Ascending or descending
     };
   enum TypeOfDir //Direction Type
     {
      MA_DIR_BOTH,//Any
      MA_DIR_LONG,// Only Long
      MA_DIR_SHORT,// Only Short
     };
   enum TypeOfWork //Work Type
     {
      WORK_GRIDER,//Grider
      WORK_GRIDER_BOTH,//Grider both direction
      WORK_REVERSE,// Reverse
     };
   enum AccType //Acc Type
     {
      ACC_BALANCE,//Balance
      ACC_EQUITY,//Equity
     };
   enum ValType //Value Type
     {
      VAL_POINT,//Points
      VAL_PERCENT,//Percent
      VAL_DOLLAR,//Dollar
      VAL_STEPOFGRID,//Steps of grid
     };

input uint        EA_Magic=345543;
input string      my_cmt="giderKatEA"; //Comment
input double      minSpread=0; // Enter only with spread, less
input double      equityLess=0; //Close all when equity is decreased by, $
input bool        replaceFirstSymbol=false; // Replace symbol No. 1 with a graph symbol
input int         closeAtTotal=0; //Close at total profit
input int         maxSymbols=0; // 
input int         maxTotalPos=0;
input double      maxTotalVol=0;
input int         maxHighPoints=0;
input TypeOfWork  workType = WORK_GRIDER;
input uint        griderBothStartAfterStep = 3;
input bool        griderBothCmtSep = false;
input int         cancelKefAboutLastStep = 0; // Cancel KEF about Last Step, gSteps
input bool        useNewLotByCurOffset = false; // useNewLotByCurOffset
input double      takeForPos=0; // takeForPos
input bool        encreaseTakeOnNextStep=false; //
input double      noLongAfterPrice = 0;
input double      noShortBeforePrice = 0;
sinput string delimeter_0="============================"; // Time limits
input int         EnterHour=24; //Входить в, час
input int         EnterMin=0; //Входить в, мин
sinput string delimeter_00="============================"; // Position increase
input AccType     lotXtype=ACC_BALANCE; // lotXtype
input int         lot2x=0; // Double the initial lot upon lotXtype
input int         lot3x=0; // Triple the initial lot upon lotXtype
input int         lot4x=0; // Quadruple the initial lot upon lotXtype
input int         lot5x=0; // Initial lot x5 upon lotXtype
input int         lot6x=0; // Initial lot x6 upon lotXtype
input int         lot7x=0; // Initial lot x7 upon lotXtype
input int         lot8x=0; // Initial lot x8 upon lotXtype
input int         lot9x=0; // Initial lot x9 upon lotXtype
input int         lot10x=0; // Initial lot x10 upon lotXtype
input int         lot11x=0; // Initial lot x11 upon lotXtype
input int         lot15x=0; // Initial lot x15 upon lotXtype
input int         lot20x=0; // Initial lot x20 upon lotXtype
input int         lot25x=0; // Initial lot x25 upon lotXtype

input int         lot2xAfter=0; // Double the initial lot after X steps
input int         lot3xAfter=0; // Triple the initial lot after X steps
input int         lot4xAfter=0; // Quadruple the initial lot after X steps
sinput string delimeter_000="============================"; // Defense lot
input double      defStartDollars=0; // defStartDollars
input double      defStartLotKefs=0; // defStartLotKefs
input double      defCloseLot=0; // defCloseLot
sinput string delimeter_0000="============================"; // Assist lot
input double      assistLot=0; // assistLot
input double      assistTake=0; // assistTake in Dollars
input double      assistStartStep=0; // assiststart after step
sinput string delimeter_111="============================"; // Use all symbols
input bool        allSymb_use=false; // Use trades on all symbols
input double      allSymb_max_prices=0; // Use symbols for max price
input bool        allSymb_marketwatch=false; // Use only MarketWatch
input string      allSymb_path=""; // Path for Symbol containts



sinput string delimeter_01_1="============================"; // ---------- Symbol No. 1 ----------
input bool        useSym_1=true; // 1. Allow trading on this symbol
input string      symName_1=""; // 1. Symbol
input double      Lot_1=0.05; // 1. Lot size
input double      FirstLot_1=0; // 1. First Lot size
input TypeOfLots  typeLot_1=MY_ARIFMET; // 1. Chain increase
input int         maxVol_1 = 0; // 1. Max volume in Lots qty
input double      gridStep_1=30; // 1. Grid size, gridStepVal
input ValType     gridStepVal_1=VAL_POINT; // 1. gridStepVal
input int         curOffsetMultChange_1 = 0; // 1. Each step increase by, points
input TypeOfDir   typeDir_1=MA_DIR_BOTH; // 1. Entry directions
input double      kefForBothClose_1=0.0; // 1. kefForBothClose
input bool        kefForAllPos_1=false; // 1. kefForAllPos
input bool        kefForMaxVolume_1=false; // 1. kefForMaxVolume
input bool        useAllProfitForClose_1=false; // 1. Use All Profit for Partial closure
sinput string delimeter_02_1="=============="; // --- Close all positions
input double      takeProfit_1=0; //1. On profit, takeProfitVal
input ValType     takeProfitVal_1=VAL_DOLLAR; //1. takeProfitVal
input double      takeProfit1step_1=0; //1. When profit in step 1, $
input double      takeProfit2step_1=0; //1. When profit in step 2, $
input double      takeProfit4step_1=0; //1. When profit in step 4, $
input uint        gridStepCountClose_1=2; // 1. Close everything when trading, number
sinput string Trailing_1="==============";//Trailing Settings
input double    TSL_points_1=0; // 1. Trailing Stop dollars
input double    TTL_points_1=0; // 1. Trailing Take add dollars
input double    TSL_points_KEF_1=0; // 1. Trailing Stop for KEF dollars
input double    TTL_points_KEF_1=0; // 1. Trailing Take for KEF add dollars
sinput string Change_Timeframe_1="==============";//Change Timeframe
input uint        change_tf_M15_after_1=0; // 1. Change to M15 if position more
input uint        change_tf_M30_after_1=0; // 1. Change to M30 if position more
input uint        change_tf_H1_after_1=0; // 1. Change to H1 if position more
input uint        change_tf_H4_after_1=0; // 1. Change to H4 if position more
input uint        change_tf_D1_after_1=0; // 1. Change to D1 if position more
sinput string delimeter_07_1="=============="; // --- Towards the bar
input bool        BARS_enabled_1=false; //1. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_1=PERIOD_D1; // 1. Timeframe
input int         BARS_RSI_Period_1=0; //1. Start only on RSI signal with period
sinput string delimeter_08_1="=============="; // --- Series of bars
input int         SERIES_count_1=0; //1. Enter at N bars one way
input TypeOfSeries SERIES_type_1=MY_SERIES_PLUS; // 1. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_1=PERIOD_M15; // 1. Timeframe
input int         SERIES_percent_1=100; //1. Percentage of bars
input bool        SERIES_use_point_1 = false; //1. Use points instead count
sinput string delimeter_09_1="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_1=PERIOD_D1; // 1. Timeframe
input int         MA_PERIOD_1=0; //1. Period
input TypeOfMA    MA_TYPE_1=MA_FIRST; // 1. Type of strategy
input bool        MA_invert_1=false; // 1. Invert signal
input bool        MA_across_price_1=false; // 1. Only when crossing the price with MA
input bool        MA_across_always_1=false; // 1. At all steps when crossing the price
sinput string delimeter_10_1="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_1=PERIOD_M5; // 1. Timeframe
input int         RSI_PERIOD_1=0; //1. Period
input bool        RSI_enter_return_1= false; // 1. Enter when exiting borders
input bool        RSI_only_first_1= false; // 1. Use RSI only on first login
input bool        RSI_invert_1= false; // 1. Invert signal
sinput string delimeter_11_1="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_1=PERIOD_M5; // 1. Timeframe
input int         BB_PERIOD_1=0; //1. Period
input int         BB_SHIFT_1=0; //1. Offset
input double      BB_DEVIATION_1=2; //1. Number of standard deviations
input bool        BB_only_first_1= false; // 1. Use BB only on first login
input bool        BB_INVERT_1=false; //1. Invert signal
sinput string delimeter_12_1="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_1=PERIOD_M5; // 1. Timeframe
input int         SK_PERIOD_1=0; //1. K Period
input int         SD_PERIOD_1=3; //1. D Period
input int         S_Slowing_1=3; //1. Slowing
input bool        S_INVERT_1=false; //1. Invert signal

sinput string delimeter_01_2="============================"; // ---------- Symbol No. 2 ----------
input bool        useSym_2=false; // 2. Allow trading on this symbol
input string      symName_2=""; // 2. Symbol
input double      Lot_2=0.05; // 2. Lot size
input double      FirstLot_2=0; // 2. First Lot size
input TypeOfLots  typeLot_2=MY_ARIFMET; // 2. Chain increase
input int         maxVol_2 = 0; // 2. Max volume in Lots qty
input double      gridStep_2=30; // 2. Grid size, points
input ValType     gridStepVal_2=VAL_POINT; // 2. gridStepVal
input int         curOffsetMultChange_2 = 0; // 2. Each step increase by, points
input TypeOfDir   typeDir_2=MA_DIR_BOTH; // 2. Entry directions
input double      kefForBothClose_2=0.0;
input bool        kefForAllPos_2=false;
input bool        kefForMaxVolume_2=false;
input bool        useAllProfitForClose_2=false; // 2. Use All Profit for Partial closure
sinput string delimeter_02_2="=============="; // --- Close all positions
input double      takeProfit_2=0; //2. On profit, $
input ValType     takeProfitVal_2=VAL_DOLLAR; //2. takeProfitVal
input double      takeProfit1step_2=0; //2. When profit in step 1, $
input double      takeProfit2step_2=0; //2. When profit in step 2, $
input double      takeProfit4step_2=0; //2. When profit in step 4, $
input uint        gridStepCountClose_2=2; // 2. Close everything when trading, number
sinput string Trailing_2="==============";//Trailing Settings
input double    TSL_points_2=0; // 2. Trailing Stop dollars
input double    TTL_points_2=0; // 2. Trailing Take add dollars
input double    TSL_points_KEF_2=0; // 2. Trailing Stop for KEF dollars
input double    TTL_points_KEF_2=0; // 2. Trailing Take for KEF add dollars
sinput string Change_Timeframe_2="==============";//Change Timeframe
input uint        change_tf_M15_after_2=0; // 2. Change to M15 if position more
input uint        change_tf_M30_after_2=0; // 2. Change to M30 if position more
input uint        change_tf_H1_after_2=0; // 2. Change to H1 if position more
input uint        change_tf_H4_after_2=0; // 2. Change to H4 if position more
input uint        change_tf_D1_after_2=0; // 2. Change to D1 if position more
sinput string delimeter_07_2="=============="; // --- Towards the bar
input bool        BARS_enabled_2=false; //2. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_2=PERIOD_D1; // 2. Timeframe
input int         BARS_RSI_Period_2=0; //2. Start only on RSI signal with period
sinput string delimeter_08_2="=============="; // --- Series of bars
input int         SERIES_count_2=0; //2. Enter at N bars one way
input TypeOfSeries SERIES_type_2=MY_SERIES_PLUS; // 2. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_2=PERIOD_M15; // 2. Timeframe
input int         SERIES_percent_2=100; //2. Percentage of bars
input bool        SERIES_use_point_2 = false; //2. Use points instead count
sinput string delimeter_09_2="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_2=PERIOD_D1; // 2. Timeframe
input int         MA_PERIOD_2=0; //2. Period
input TypeOfMA    MA_TYPE_2=MA_FIRST; // 2. Type of strategy
input bool        MA_invert_2=false; // 2. Invert signal
input bool        MA_across_price_2=false; // 2. Only when crossing the price with MA
input bool        MA_across_always_2=false; // 2. Only when crossing the price with MA
sinput string delimeter_10_2="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_2=PERIOD_M5; // 2. Timeframe
input int         RSI_PERIOD_2=0; //2. Period
input bool        RSI_enter_return_2= false; // 2. Enter when exiting borders
input bool        RSI_only_first_2= false; // 2. Use RSI only on first login
input bool        RSI_invert_2= false; // 2. Invert signal
sinput string delimeter_11_2="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_2=PERIOD_M5; // 2. Timeframe
input int         BB_PERIOD_2=0; //2. Period
input int         BB_SHIFT_2=0; //2. Offset
input double      BB_DEVIATION_2=2; //2. Number of standard deviations
input bool        BB_only_first_2= false; // 2. Use BB only on first login
input bool        BB_INVERT_2=false; //2. Invert signal
sinput string delimeter_12_2="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_2=PERIOD_M5; // 2. Timeframe
input int         SK_PERIOD_2=0; //2. K Period
input int         SD_PERIOD_2=3; //2. D Period
input int         S_Slowing_2=3; //2. Slowing
input bool        S_INVERT_2=false; //2. Invert signal

sinput string delimeter_01_3="============================"; // ---------- Symbol No. 3 ----------
input bool        useSym_3=false; // 3. Allow trading on this symbol
input string      symName_3=""; // 3. Symbol
input double      Lot_3=0.05; // 3. Lot size
input double      FirstLot_3=0; // 3. First Lot size
input TypeOfLots  typeLot_3=MY_ARIFMET; // 3. Chain increase
input int         maxVol_3 = 0; // 3. Max volume in Lots qty
input double      gridStep_3=30; // 3. Grid size, points
input ValType     gridStepVal_3=VAL_POINT; // 3. gridStepVal
input int         curOffsetMultChange_3 = 0; // 3. Each step increase by, points
input TypeOfDir   typeDir_3=MA_DIR_BOTH; // 3. Entry directions
input double      kefForBothClose_3=0.0;
input bool        kefForAllPos_3=false;
input bool        kefForMaxVolume_3=false;
input bool        useAllProfitForClose_3=false; // 3. Use All Profit for Partial closure
sinput string delimeter_02_3="=============="; // --- Close all positions
input double      takeProfit_3=0; //3. On profit, $
input ValType     takeProfitVal_3=VAL_DOLLAR; //3. takeProfitVal
input double      takeProfit1step_3=0; //3. When profit in step 1, $
input double      takeProfit2step_3=0; //3. When profit in step 2, $
input double      takeProfit4step_3=0; //3. When profit in step 4, $
input uint        gridStepCountClose_3=2; // 3. Close everything when trading, number
sinput string Trailing_3="==============";//Trailing Settings
input double    TSL_points_3=0; // 3. Trailing Stop dollars
input double    TTL_points_3=0; // 3. Trailing Take add dollars
input double    TSL_points_KEF_3=0; // 3. Trailing Stop for KEF dollars
input double    TTL_points_KEF_3=0; // 3. Trailing Take for KEF add dollars
sinput string Change_Timeframe_3="==============";//Change Timeframe
input uint        change_tf_M15_after_3=0; // 3. Change to M15 if position more
input uint        change_tf_M30_after_3=0; // 3. Change to M30 if position more
input uint        change_tf_H1_after_3=0; // 3. Change to H1 if position more
input uint        change_tf_H4_after_3=0; // 3. Change to H4 if position more
input uint        change_tf_D1_after_3=0; // 3. Change to D1 if position more
sinput string delimeter_07_3="=============="; // --- Towards the bar
input bool        BARS_enabled_3=false; //3. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_3=PERIOD_D1; // 3. Timeframe
input int         BARS_RSI_Period_3=0; //3. Start only on RSI signal with period
sinput string delimeter_08_3="=============="; // --- Series of bars
input int         SERIES_count_3=0; //3. Enter at N bars one way
input TypeOfSeries SERIES_type_3=MY_SERIES_PLUS; // 3. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_3=PERIOD_M15; // 3. Timeframe
input int         SERIES_percent_3=100; //3. Percentage of bars
input bool        SERIES_use_point_3 = false; //3. Use points instead count
sinput string delimeter_09_3="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_3=PERIOD_D1; // 3. Timeframe
input int         MA_PERIOD_3=0; //3. Period
input TypeOfMA    MA_TYPE_3=MA_FIRST; // 3. Type of strategy
input bool        MA_invert_3=false; // 3. Invert signal
input bool        MA_across_price_3=false; // 3. Only when crossing the price with MA
input bool        MA_across_always_3=false; // 3. At all steps when crossing the price
sinput string delimeter_10_3="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_3=PERIOD_M5; // 3. Timeframe
input int         RSI_PERIOD_3=0; //3. Period
input bool        RSI_enter_return_3= false; // 3. Enter when exiting borders
input bool        RSI_only_first_3= false; // 3. Use RSI only on first login
input bool        RSI_invert_3= false; // 3. Invert signal
sinput string delimeter_11_3="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_3=PERIOD_M5; // 3. Timeframe
input int         BB_PERIOD_3=0; //3. Period
input int         BB_SHIFT_3=0; //3. Offset
input double      BB_DEVIATION_3=2; //3. Number of standard deviations
input bool        BB_only_first_3= false; // 3. Use BB only on first login
input bool        BB_INVERT_3=false; //3. Invert signal
sinput string delimeter_12_3="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_3=PERIOD_M5; // 3. Timeframe
input int         SK_PERIOD_3=0; //3. K Period
input int         SD_PERIOD_3=3; //3. D Period
input int         S_Slowing_3=3; //3. Slowing
input bool        S_INVERT_3=false; //3. Invert signal

sinput string delimeter_01_4="============================"; // ---------- Symbol No. 4 ----------
input bool        useSym_4=false; // 4. Allow trading on this symbol
input string      symName_4=""; // 4. Symbol
input double      Lot_4=0.05; // 4. Lot size
input double      FirstLot_4=0; // 4. First Lot size
input TypeOfLots  typeLot_4=MY_ARIFMET; // 4. Chain increase
input int         maxVol_4 = 0; // 4. Max volume in Lots qty
input double      gridStep_4=30; // 4. Grid size, points
input ValType     gridStepVal_4=VAL_POINT; // 4. gridStepVal
input int         curOffsetMultChange_4 = 0; // 4. Each step increase by, points
input TypeOfDir   typeDir_4=MA_DIR_BOTH; // 4. Entry directions
input double      kefForBothClose_4=0.0;
input bool        kefForAllPos_4=false;
input bool        kefForMaxVolume_4=false;
input bool        useAllProfitForClose_4=false; // 4. Use All Profit for Partial closure
sinput string delimeter_02_4="=============="; // --- Close all positions
input double      takeProfit_4=0; //4. On profit, $
input ValType     takeProfitVal_4=VAL_DOLLAR; //4. takeProfitVal
input double      takeProfit1step_4=0; //4. When profit in step 1, $
input double      takeProfit2step_4=0; //4. When profit in step 2, $
input double      takeProfit4step_4=0; //4. When profit in step 4, $
input uint        gridStepCountClose_4=2; // 4. Close everything when trading, number
sinput string Trailing_4="==============";//Trailing Settings
input double    TSL_points_4=0; // 4. Trailing Stop dollars
input double    TTL_points_4=0; // 4. Trailing Take add dollars
input double    TSL_points_KEF_4=0; // 4. Trailing Stop for KEF dollars
input double    TTL_points_KEF_4=0; // 4. Trailing Take for KEF add dollars
sinput string Change_Timeframe_4="==============";//Change Timeframe
input uint        change_tf_M15_after_4=0; // 4. Change to M15 if position more
input uint        change_tf_M30_after_4=0; // 4. Change to M30 if position more
input uint        change_tf_H1_after_4=0; // 4. Change to H1 if position more
input uint        change_tf_H4_after_4=0; // 4. Change to H4 if position more
input uint        change_tf_D1_after_4=0; // 4. Change to D1 if position more
sinput string delimeter_07_4="=============="; // --- Towards the bar
input bool        BARS_enabled_4=false; //4. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_4=PERIOD_D1; // 4. Timeframe
input int         BARS_RSI_Period_4=0; //4. Start only on RSI signal with period
sinput string delimeter_08_4="=============="; // --- Series of bars
input int         SERIES_count_4=0; //4. Enter at N bars one way
input TypeOfSeries SERIES_type_4=MY_SERIES_PLUS; // 4. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_4=PERIOD_M15; // 4. Timeframe
input int         SERIES_percent_4=100; //4. Percentage of bars
input bool        SERIES_use_point_4 = false; //4. Use points instead count
sinput string delimeter_09_4="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_4=PERIOD_D1; // 4. Timeframe
input int         MA_PERIOD_4=0; //4. Period
input TypeOfMA    MA_TYPE_4=MA_FIRST; // 4. Type of strategy
input bool        MA_invert_4=false; // 4. Invert signal
input bool        MA_across_price_4=false; // 4. Only when crossing the price with MA
input bool        MA_across_always_4=false; // 4. At all steps when crossing the price
sinput string delimeter_10_4="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_4=PERIOD_M5; // 4. Timeframe
input int         RSI_PERIOD_4=0; //4. Period
input bool        RSI_enter_return_4= false; // 4. Enter when exiting borders
input bool        RSI_only_first_4= false; // 4. Use RSI only on first login
input bool        RSI_invert_4= false; // 4. Invert signal
sinput string delimeter_11_4="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_4=PERIOD_M5; // 4. Timeframe
input int         BB_PERIOD_4=0; //4. Period
input int         BB_SHIFT_4=0; //4. Offset
input double      BB_DEVIATION_4=2; //4. Number of standard deviations
input bool        BB_only_first_4= false; // 4. Use BB only on first login
input bool        BB_INVERT_4=false; //4. Invert signal
sinput string delimeter_12_4="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_4=PERIOD_M5; // 4. Timeframe
input int         SK_PERIOD_4=0; //4. K Period
input int         SD_PERIOD_4=3; //4. D Period
input int         S_Slowing_4=3; //4. Slowing
input bool        S_INVERT_4=false; //4. Invert signal

sinput string delimeter_01_5="============================"; // ---------- Symbol No. 5 ----------
input bool        useSym_5=false; // 5. Allow trading on this symbol
input string      symName_5=""; // 5. Symbol
input double      Lot_5=0.05; // 5. Lot size
input double      FirstLot_5=0; // 5. First Lot size
input TypeOfLots  typeLot_5=MY_ARIFMET; // 5. Chain increase
input int         maxVol_5 = 0; // 5. Max volume in Lots qty
input double      gridStep_5=30; // 5. Grid size, points
input ValType     gridStepVal_5=VAL_POINT; // 5. gridStepVal
input int         curOffsetMultChange_5 = 0; // 5. Each step increase by, points
input TypeOfDir   typeDir_5=MA_DIR_BOTH; // 5. Entry directions
input double      kefForBothClose_5=0.0;
input bool        kefForAllPos_5=false;
input bool        kefForMaxVolume_5=false;
input bool        useAllProfitForClose_5=false; // 5. Use All Profit for Partial closure
sinput string delimeter_02_5="=============="; // --- Close all positions
input double      takeProfit_5=0; //5. On profit, $
input ValType     takeProfitVal_5=VAL_DOLLAR; //5. takeProfitVal
input double      takeProfit1step_5=0; //5. When profit in step 1, $
input double      takeProfit2step_5=0; //5. When profit in step 2, $
input double      takeProfit4step_5=0; //5. When profit in step 4, $
input uint        gridStepCountClose_5=2; // 5. Close everything when trading, number
sinput string Trailing_5="==============";//Trailing Settings
input double    TSL_points_5=0; // 5. Trailing Stop dollars
input double    TTL_points_5=0; // 5. Trailing Take add dollars
input double    TSL_points_KEF_5=0; // 5. Trailing Stop for KEF dollars
input double    TTL_points_KEF_5=0; // 5. Trailing Take for KEF add dollars
sinput string Change_Timeframe_5="==============";//Change Timeframe
input uint        change_tf_M15_after_5=0; // 5. Change to M15 if position more
input uint        change_tf_M30_after_5=0; // 5. Change to M30 if position more
input uint        change_tf_H1_after_5=0; // 5. Change to H1 if position more
input uint        change_tf_H4_after_5=0; // 5. Change to H4 if position more
input uint        change_tf_D1_after_5=0; // 5. Change to D1 if position more
sinput string delimeter_07_5="=============="; // --- Towards the bar
input bool        BARS_enabled_5=false; //5. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_5=PERIOD_D1; // 5. Timeframe
input int         BARS_RSI_Period_5=0; //5. Start only on RSI signal with period
sinput string delimeter_08_5="=============="; // --- Series of bars
input int         SERIES_count_5=0; //5. Enter at N bars one way
input TypeOfSeries SERIES_type_5=MY_SERIES_PLUS; // 5. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_5=PERIOD_M15; // 5. Timeframe
input int         SERIES_percent_5=100; //5. Percentage of bars
input bool        SERIES_use_point_5 = false; //5. Use points instead count
sinput string delimeter_09_5="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_5=PERIOD_D1; // 5. Timeframe
input int         MA_PERIOD_5=0; //5. Period
input TypeOfMA    MA_TYPE_5=MA_FIRST; // 5. Type of strategy
input bool        MA_invert_5=false; // 5. Invert signal
input bool        MA_across_price_5=false; // 5. Only when crossing the price with MA
input bool        MA_across_always_5=false; // 5. At all steps when crossing the price
sinput string delimeter_10_5="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_5=PERIOD_M5; // 5. Timeframe
input int         RSI_PERIOD_5=0; //5. Period
input bool        RSI_enter_return_5= false; // 5. Enter when exiting borders
input bool        RSI_only_first_5= false; // 5. Use RSI only on first login
input bool        RSI_invert_5= false; // 5. Invert signal
sinput string delimeter_11_5="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_5=PERIOD_M5; // 5. Timeframe
input int         BB_PERIOD_5=0; //5. Period
input int         BB_SHIFT_5=0; //5. Offset
input double      BB_DEVIATION_5=2; //5. Number of standard deviations
input bool        BB_only_first_5= false; // 5. Use BB only on first login
input bool        BB_INVERT_5=false; //5. Invert signal
sinput string delimeter_12_5="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_5=PERIOD_M5; // 5. Timeframe
input int         SK_PERIOD_5=0; //5. K Period
input int         SD_PERIOD_5=3; //5. D Period
input int         S_Slowing_5=3; //5. Slowing
input bool        S_INVERT_5=false; //5. Invert signal

sinput string delimeter_01_6="============================"; // ---------- Symbol No. 6 ----------
input bool        useSym_6=false; // 6. Allow trading on this symbol
input string      symName_6=""; // 6. Symbol
input double      Lot_6=0.05; // 6. Lot size
input double      FirstLot_6=0; // 6. First Lot size
input TypeOfLots  typeLot_6=MY_ARIFMET; // 6. Chain increase
input int         maxVol_6 = 0; // 6. Max volume in Lots qty
input double      gridStep_6=30; // 6. Grid size, points
input ValType     gridStepVal_6=VAL_POINT; // 6. gridStepVal
input int         curOffsetMultChange_6 = 0; // 6. Each step increase by, points
input TypeOfDir   typeDir_6=MA_DIR_BOTH; // 6. Entry directions
input double      kefForBothClose_6=0.0;
input bool        kefForAllPos_6=false;
input bool        kefForMaxVolume_6=false;
input bool        useAllProfitForClose_6=false; // 6. Use All Profit for Partial closure
sinput string delimeter_02_6="=============="; // --- Close all positions
input double      takeProfit_6=0; //6. On profit, $
input ValType     takeProfitVal_6=VAL_DOLLAR; //6. takeProfitVal
input double      takeProfit1step_6=0; //6. When profit in step 1, $
input double      takeProfit2step_6=0; //6. When profit in step 2, $
input double      takeProfit4step_6=0; //6. When profit in step 4, $
input uint        gridStepCountClose_6=2; // 6. Close everything when trading, number
sinput string Trailing_6="==============";//6. Trailing Settings
input double    TSL_points_6=0; // 6. Trailing Stop dollars
input double    TTL_points_6=0; // 6. Trailing Take add dollars
input double    TSL_points_KEF_6=0; // 6. Trailing Stop for KEF dollars
input double    TTL_points_KEF_6=0; // 6. Trailing Take for KEF add dollars
sinput string Change_Timeframe_6="==============";//6. Change Timeframe
input uint        change_tf_M15_after_6=0; // 6. Change to M15 if position more
input uint        change_tf_M30_after_6=0; // 6. Change to M30 if position more
input uint        change_tf_H1_after_6=0; // 6. Change to H1 if position more
input uint        change_tf_H4_after_6=0; // 6. Change to H4 if position more
input uint        change_tf_D1_after_6=0; // 6. Change to D1 if position more
sinput string delimeter_07_6="=============="; // --- Towards the bar
input bool        BARS_enabled_6=false; //6. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_6=PERIOD_D1; // 6. Timeframe
input int         BARS_RSI_Period_6=0; //6. Start only on RSI signal with period
sinput string delimeter_08_6="=============="; // --- Series of bars
input int         SERIES_count_6=0; //6. Enter at N bars one way
input TypeOfSeries SERIES_type_6=MY_SERIES_PLUS; // 6. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_6=PERIOD_M15; // 6. Timeframe
input int         SERIES_percent_6=100; //6. Percentage of bars
input bool        SERIES_use_point_6 = false; //6. Use points instead count
sinput string delimeter_09_6="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_6=PERIOD_D1; // 6. Timeframe
input int         MA_PERIOD_6=0; //6. Period
input TypeOfMA    MA_TYPE_6=MA_FIRST; // 6. Type of strategy
input bool        MA_invert_6=false; // 6. Invert signal
input bool        MA_across_price_6=false; // 6. Only when crossing the price with MA
input bool        MA_across_always_6=false; // 6. At all steps when crossing the price
sinput string delimeter_10_6="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_6=PERIOD_M5; // 6. Timeframe
input int         RSI_PERIOD_6=0; //6. Period
input bool        RSI_enter_return_6= false; // 6. Enter when exiting borders
input bool        RSI_only_first_6= false; // 6. Use RSI only on first login
input bool        RSI_invert_6= false; // 6. Invert signal
sinput string delimeter_11_6="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_6=PERIOD_M5; // 6. Timeframe
input int         BB_PERIOD_6=0; //6. Period
input int         BB_SHIFT_6=0; //6. Offset
input double      BB_DEVIATION_6=2; //6. Number of standard deviations
input bool        BB_only_first_6= false; // 6. Use BB only on first login
input bool        BB_INVERT_6=false; //6. Invert signal
sinput string delimeter_12_6="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_6=PERIOD_M5; // 6. Timeframe
input int         SK_PERIOD_6=0; //6. K Period
input int         SD_PERIOD_6=3; //6. D Period
input int         S_Slowing_6=3; //6. Slowing
input bool        S_INVERT_6=false; //6. Invert signal

sinput string delimeter_01_7="============================"; // ---------- Symbol No. 7 ----------
input bool        useSym_7=false; // 7. Allow trading on this symbol
input string      symName_7=""; // 7. Symbol
input double      Lot_7=0.05; // 7. Lot size
input double      FirstLot_7=0; // 7. First Lot size
input TypeOfLots  typeLot_7=MY_ARIFMET; // 7. Chain increase
input int         maxVol_7 = 0; // 7. Max volume in Lots qty
input double      gridStep_7=30; // 7. Grid size, points
input ValType     gridStepVal_7=VAL_POINT; // 7. gridStepVal
input int         curOffsetMultChange_7 = 0; // 7. Each step increase by, points
input TypeOfDir   typeDir_7=MA_DIR_BOTH; // 7. Entry directions
input double      kefForBothClose_7=0.0;
input bool        kefForAllPos_7=false;
input bool        kefForMaxVolume_7=false;
input bool        useAllProfitForClose_7=false; // 7. Use All Profit for Partial closure
sinput string delimeter_02_7="=============="; // --- Close all positions
input double      takeProfit_7=0; //7. On profit, $
input ValType     takeProfitVal_7=VAL_DOLLAR; //7. takeProfitVal
input double      takeProfit1step_7=0; //7. When profit in step 1, $
input double      takeProfit2step_7=0; //7. When profit in step 2, $
input double      takeProfit4step_7=0; //7. When profit in step 4, $
input uint        gridStepCountClose_7=2; // 7. Close everything when trading, number
sinput string Trailing_7="==============";//Trailing Settings
input double    TSL_points_7=0; // 7. Trailing Stop dollars
input double    TTL_points_7=0; // 7. Trailing Take add dollars
input double    TSL_points_KEF_7=0; // 7. Trailing Stop for KEF dollars
input double    TTL_points_KEF_7=0; // 7. Trailing Take for KEF add dollars
sinput string Change_Timeframe_7="==============";//Change Timeframe
input uint        change_tf_M15_after_7=0; // 7. Change to M15 if position more
input uint        change_tf_M30_after_7=0; // 7. Change to M30 if position more
input uint        change_tf_H1_after_7=0; // 7. Change to H1 if position more
input uint        change_tf_H4_after_7=0; // 7. Change to H4 if position more
input uint        change_tf_D1_after_7=0; // 7. Change to D1 if position more
sinput string delimeter_07_7="=============="; // --- Towards the bar
input bool        BARS_enabled_7=false; //7. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_7=PERIOD_D1; // 7. Timeframe
input int         BARS_RSI_Period_7=0; //7. Start only on RSI signal with period
sinput string delimeter_08_7="=============="; // --- Series of bars
input int         SERIES_count_7=0; //7. Enter at N bars one way
input TypeOfSeries SERIES_type_7=MY_SERIES_PLUS; // 7. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_7=PERIOD_M15; // 7. Timeframe
input int         SERIES_percent_7=100; //7. Percentage of bars
input bool        SERIES_use_point_7 = false; //7. Use points instead count
sinput string delimeter_09_7="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_7=PERIOD_D1; // 7. Timeframe
input int         MA_PERIOD_7=0; //7. Period
input TypeOfMA    MA_TYPE_7=MA_FIRST; // 7. Type of strategy
input bool        MA_invert_7=false; // 7. Invert signal
input bool        MA_across_price_7=false; // 7. Only when crossing the price with MA
input bool        MA_across_always_7=false; // 7. At all steps when crossing the price
sinput string delimeter_10_7="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_7=PERIOD_M5; // 7. Timeframe
input int         RSI_PERIOD_7=0; //7. Period
input bool        RSI_enter_return_7= false; // 7. Enter when exiting borders
input bool        RSI_only_first_7= false; // 7. Use RSI only on first login
input bool        RSI_invert_7= false; // 7. Invert signal
sinput string delimeter_11_7="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_7=PERIOD_M5; // 7. Timeframe
input int         BB_PERIOD_7=0; //7. Period
input int         BB_SHIFT_7=0; //7. Offset
input double      BB_DEVIATION_7=2; //7. Number of standard deviations
input bool        BB_only_first_7= false; // 7. Use BB only on first login
input bool        BB_INVERT_7=false; //7. Invert signal
sinput string delimeter_12_7="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_7=PERIOD_M5; // 7. Timeframe
input int         SK_PERIOD_7=0; //7. K Period
input int         SD_PERIOD_7=3; //7. D Period
input int         S_Slowing_7=3; //7. Slowing
input bool        S_INVERT_7=false; //7. Invert signal

sinput string delimeter_01_8="============================"; // ---------- Symbol No. 8 ----------
input bool        useSym_8=false; // 8. Allow trading on this symbol
input string      symName_8=""; // 8. Symbol
input double      Lot_8=0.05; // 8. Lot size
input double      FirstLot_8=0; // 8. First Lot size
input TypeOfLots  typeLot_8=MY_ARIFMET; // 8. Chain increase
input int         maxVol_8 = 0; // 8. Max volume in Lots qty
input double      gridStep_8=30; // 8. Grid size, points
input ValType     gridStepVal_8=VAL_POINT; // 8. gridStepVal
input int         curOffsetMultChange_8 = 0; // 8. Each step increase by, points
input TypeOfDir   typeDir_8=MA_DIR_BOTH; // 8. Entry directions
input double      kefForBothClose_8=0.0;
input bool        kefForAllPos_8=false;
input bool        kefForMaxVolume_8=false;
input bool        useAllProfitForClose_8=false; // 8. Use All Profit for Partial closure
sinput string delimeter_02_8="=============="; // --- Close all positions
input double      takeProfit_8=0; //8. On profit, $
input ValType     takeProfitVal_8=VAL_DOLLAR; //8. takeProfitVal
input double      takeProfit1step_8=0; //8. When profit in step 1, $
input double      takeProfit2step_8=0; //8. When profit in step 2, $
input double      takeProfit4step_8=0; //8. When profit in step 4, $
input uint        gridStepCountClose_8=2; // 8. Close everything when trading, number
sinput string Trailing_8="==============";//Trailing Settings
input double    TSL_points_8=0; // 8. Trailing Stop dollars
input double    TTL_points_8=0; // 8. Trailing Take add dollars
input double    TSL_points_KEF_8=0; // 8. Trailing Stop for KEF dollars
input double    TTL_points_KEF_8=0; // 8. Trailing Take for KEF add dollars
sinput string Change_Timeframe_8="==============";//Change Timeframe
input uint        change_tf_M15_after_8=0; // 8. Change to M15 if position more
input uint        change_tf_M30_after_8=0; // 8. Change to M30 if position more
input uint        change_tf_H1_after_8=0; // 8. Change to H1 if position more
input uint        change_tf_H4_after_8=0; // 8. Change to H4 if position more
input uint        change_tf_D1_after_8=0; // 8. Change to D1 if position more
sinput string delimeter_07_8="=============="; // --- Towards the bar
input bool        BARS_enabled_8=false; //8. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_8=PERIOD_D1; // 8. Timeframe
input int         BARS_RSI_Period_8=0; //8. Start only on RSI signal with period
sinput string delimeter_08_8="=============="; // --- Series of bars
input int         SERIES_count_8=0; //8. Enter at N bars one way
input TypeOfSeries SERIES_type_8=MY_SERIES_PLUS; // 8. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_8=PERIOD_M15; // 8. Timeframe
input int         SERIES_percent_8=100; //8. Percentage of bars
input bool        SERIES_use_point_8 = false; //8. Use points instead count
sinput string delimeter_09_8="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_8=PERIOD_D1; // 8. Timeframe
input int         MA_PERIOD_8=0; //8. Period
input TypeOfMA    MA_TYPE_8=MA_FIRST; // 8. Type of strategy
input bool        MA_invert_8=false; // 8. Invert signal
input bool        MA_across_price_8=false; // 8. Only when crossing the price with MA
input bool        MA_across_always_8=false; // 8. At all steps when crossing the price
sinput string delimeter_10_8="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_8=PERIOD_M5; // 8. Timeframe
input int         RSI_PERIOD_8=0; //8. Period
input bool        RSI_enter_return_8= false; // 8. Enter when exiting borders
input bool        RSI_only_first_8= false; // 8. Use RSI only on first login
input bool        RSI_invert_8= false; // 8. Invert signal
sinput string delimeter_11_8="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_8=PERIOD_M5; // 8. Timeframe
input int         BB_PERIOD_8=0; //8. Period
input int         BB_SHIFT_8=0; //8. Offset
input double      BB_DEVIATION_8=2; //8. Number of standard deviations
input bool        BB_only_first_8= false; // 8. Use BB only on first login
input bool        BB_INVERT_8=false; //8. Invert signal
sinput string delimeter_12_8="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_8=PERIOD_M5; // 8. Timeframe
input int         SK_PERIOD_8=0; //8. K Period
input int         SD_PERIOD_8=3; //8. D Period
input int         S_Slowing_8=3; //8. Slowing
input bool        S_INVERT_8=false; //8. Invert signal

sinput string delimeter_01_9="============================"; // ---------- Symbol No. 9 ----------
input bool        useSym_9=false; // 9. Allow trading on this symbol
input string      symName_9=""; // 9. Symbol
input double      Lot_9=0.05; // 9. Lot size
input double      FirstLot_9=0; // 9. First Lot size
input TypeOfLots  typeLot_9=MY_ARIFMET; // 9. Chain increase
input int         maxVol_9 = 0; // 9. Max volume in Lots qty
input double      gridStep_9=30; // 9. Grid size, points
input ValType     gridStepVal_9=VAL_POINT; // 9. gridStepVal
input int         curOffsetMultChange_9 = 0; // 9. Each step increase by, points
input TypeOfDir   typeDir_9=MA_DIR_BOTH; // 9. Entry directions
input double      kefForBothClose_9=0.0;
input bool        kefForAllPos_9=false;
input bool        kefForMaxVolume_9=false;
input bool        useAllProfitForClose_9=false; // 9. Use All Profit for Partial closure
sinput string delimeter_02_9="=============="; // --- Close all positions
input double      takeProfit_9=0; //9. On profit, $
input ValType     takeProfitVal_9=VAL_DOLLAR; //9. takeProfitVal
input double      takeProfit1step_9=0; //9. When profit in step 1, $
input double      takeProfit2step_9=0; //9. When profit in step 2, $
input double      takeProfit4step_9=0; //9. When profit in step 4, $
input uint        gridStepCountClose_9=2; // 9. Close everything when trading, number
sinput string Trailing_9="==============";//Trailing Settings
input double    TSL_points_9=0; // 9. Trailing Stop dollars
input double    TTL_points_9=0; // 9. Trailing Take add dollars
input double    TSL_points_KEF_9=0; // 9. Trailing Stop for KEF dollars
input double    TTL_points_KEF_9=0; // 9. Trailing Take for KEF add dollars
sinput string Change_Timeframe_9="==============";//Change Timeframe
input uint        change_tf_M15_after_9=0; // 9. Change to M15 if position more
input uint        change_tf_M30_after_9=0; // 9. Change to M30 if position more
input uint        change_tf_H1_after_9=0; // 9. Change to H1 if position more
input uint        change_tf_H4_after_9=0; // 9. Change to H4 if position more
input uint        change_tf_D1_after_9=0; // 9. Change to D1 if position more
sinput string delimeter_07_9="=============="; // --- Towards the bar
input bool        BARS_enabled_9=false; //9. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_9=PERIOD_D1; // 9. Timeframe
input int         BARS_RSI_Period_9=0; //9. Start only on RSI signal with period
sinput string delimeter_08_9="=============="; // --- Series of bars
input int         SERIES_count_9=0; //9. Enter at N bars one way
input TypeOfSeries SERIES_type_9=MY_SERIES_PLUS; // 9. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_9=PERIOD_M15; // 9. Timeframe
input int         SERIES_percent_9=100; //9. Percentage of bars
input bool        SERIES_use_point_9 = false; //9. Use points instead count
sinput string delimeter_09_9="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_9=PERIOD_D1; // 9. Timeframe
input int         MA_PERIOD_9=0; //9. Period
input TypeOfMA    MA_TYPE_9=MA_FIRST; // 9. Type of strategy
input bool        MA_invert_9=false; // 9. Invert signal
input bool        MA_across_price_9=false; // 9. Only when crossing the price with MA
input bool        MA_across_always_9=false; // 9. At all steps when crossing the price
sinput string delimeter_10_9="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_9=PERIOD_M5; // 9. Timeframe
input int         RSI_PERIOD_9=0; //9. Period
input bool        RSI_enter_return_9= false; // 9. Enter when exiting borders
input bool        RSI_only_first_9= false; // 9. Use RSI only on first login
input bool        RSI_invert_9= false; // 9. Invert signal
sinput string delimeter_11_9="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_9=PERIOD_M5; // 9. Timeframe
input int         BB_PERIOD_9=0; //9. Period
input int         BB_SHIFT_9=0; //9. Offset
input double      BB_DEVIATION_9=2; //9. Number of standard deviations
input bool        BB_only_first_9= false; // 9. Use BB only on first login
input bool        BB_INVERT_9=false; //9. Invert signal
sinput string delimeter_12_9="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_9=PERIOD_M5; // 9. Timeframe
input int         SK_PERIOD_9=0; //9. K Period
input int         SD_PERIOD_9=3; //9. D Period
input int         S_Slowing_9=3; //9. Slowing
input bool        S_INVERT_9=false; //9. Invert signal


sinput string delimeter_01_10="============================"; // ---------- Symbol No. 10 ----------
input bool        useSym_10=false; // 10. Allow trading on this symbol
input string      symName_10=""; // 10. Symbol
input double      Lot_10=0.05; // 10. Lot size
input double      FirstLot_10=0; // 10. First Lot size
input TypeOfLots  typeLot_10=MY_ARIFMET; // 10. Chain increase
input int         maxVol_10 = 0; // 10. Max volume in Lots qty
input double      gridStep_10=30; // 10. Grid size, points
input ValType     gridStepVal_10=VAL_POINT; // 10. gridStepVal
input int         curOffsetMultChange_10 = 0; // 10. Each step increase by, points
input TypeOfDir   typeDir_10=MA_DIR_BOTH; // 10. Entry directions
input double      kefForBothClose_10=0.0;
input bool        kefForAllPos_10=false;
input bool        kefForMaxVolume_10=false;
input bool        useAllProfitForClose_10=false; // 10. Use All Profit for Partial closure
sinput string delimeter_02_10="=============="; // --- Close all positions
input double      takeProfit_10=0; //10. On profit, $
input ValType     takeProfitVal_10=VAL_DOLLAR; //10. takeProfitVal
input double      takeProfit1step_10=0; //10. When profit in step 1, $
input double      takeProfit2step_10=0; //10. When profit in step 2, $
input double      takeProfit4step_10=0; //10. When profit in step 4, $
input uint        gridStepCountClose_10=2; // 10. Close everything when trading, number
sinput string Trailing_10="==============";//Trailing Settings
input double    TSL_points_10=0; // 10. Trailing Stop dollars
input double    TTL_points_10=0; // 10. Trailing Take add dollars
input double    TSL_points_KEF_10=0; // 10. Trailing Stop for KEF dollars
input double    TTL_points_KEF_10=0; // 10. Trailing Take for KEF add dollars
sinput string Change_Timeframe_10="==============";//Change Timeframe
input uint        change_tf_M15_after_10=0; // 10. Change to M15 if position more
input uint        change_tf_M30_after_10=0; // 10. Change to M30 if position more
input uint        change_tf_H1_after_10=0; // 10. Change to H1 if position more
input uint        change_tf_H4_after_10=0; // 10. Change to H4 if position more
input uint        change_tf_D1_after_10=0; // 10. Change to D1 if position more
sinput string delimeter_07_10="=============="; // --- Towards the bar
input bool        BARS_enabled_10=false; //10. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_10=PERIOD_D1; // 10. Timeframe
input int         BARS_RSI_Period_10=0; //10. Start only on RSI signal with period
sinput string delimeter_08_10="=============="; // --- Series of bars
input int         SERIES_count_10=0; //10. Enter at N bars one way
input TypeOfSeries SERIES_type_10=MY_SERIES_PLUS; // 10. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_10=PERIOD_M15; // 10. Timeframe
input int         SERIES_percent_10=100; //10. Percentage of bars
input bool        SERIES_use_point_10 = false; //10. Use points instead count
sinput string delimeter_09_10="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_10=PERIOD_D1; // 10. Timeframe
input int         MA_PERIOD_10=0; //10. Period
input TypeOfMA    MA_TYPE_10=MA_FIRST; // 10. Type of strategy
input bool        MA_invert_10=false; // 10. Invert signal
input bool        MA_across_price_10=false; // 10. Only when crossing the price with MA
input bool        MA_across_always_10=false; // 10. At all steps when crossing the price
sinput string delimeter_10_10="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_10=PERIOD_M5; // 10. Timeframe
input int         RSI_PERIOD_10=0; //10. Period
input bool        RSI_enter_return_10= false; // 10. Enter when exiting borders
input bool        RSI_only_first_10= false; // 10. Use RSI only on first login
input bool        RSI_invert_10= false; // 10. Invert signal
sinput string delimeter_11_10="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_10=PERIOD_M5; // 10. Timeframe
input int         BB_PERIOD_10=0; //10. Period
input int         BB_SHIFT_10=0; //10. Offset
input double      BB_DEVIATION_10=2; //10. Number of standard deviations
input bool        BB_only_first_10= false; // 10. Use BB only on first login
input bool        BB_INVERT_10=false; //10. Invert signal
sinput string delimeter_12_10="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_10=PERIOD_M5; // 10. Timeframe
input int         SK_PERIOD_10=0; //10. K Period
input int         SD_PERIOD_10=3; //10. D Period
input int         S_Slowing_10=3; //10. Slowing
input bool        S_INVERT_10=false; //10. Invert signal


sinput string delimeter_01_11="============================"; // ---------- Symbol No. 11 ----------
input bool        useSym_11=false; // 11. Allow trading on this symbol
input string      symName_11=""; // 11. Symbol
input double      Lot_11=0.05; // 11. Lot size
input double      FirstLot_11=0; // 11. First Lot size
input TypeOfLots  typeLot_11=MY_ARIFMET; // 11. Chain increase
input int         maxVol_11 = 0; // 11. Max volume in Lots qty
input double      gridStep_11=30; // 11. Grid size, points
input ValType     gridStepVal_11=VAL_POINT; // 11. gridStepVal
input int         curOffsetMultChange_11 = 0; // 11. Each step increase by, points
input TypeOfDir   typeDir_11=MA_DIR_BOTH; // 11. Entry directions
input double      kefForBothClose_11=0.0;
input bool        kefForAllPos_11=false;
input bool        kefForMaxVolume_11=false;
input bool        useAllProfitForClose_11=false; // 11. Use All Profit for Partial closure
sinput string delimeter_02_11="=============="; // --- Close all positions
input double      takeProfit_11=0; //11. On profit, $
input ValType     takeProfitVal_11=VAL_DOLLAR; //11. takeProfitVal
input double      takeProfit1step_11=0; //11. When profit in step 1, $
input double      takeProfit2step_11=0; //11. When profit in step 2, $
input double      takeProfit4step_11=0; //11. When profit in step 4, $
input uint        gridStepCountClose_11=2; // 11. Close everything when trading, number
sinput string Trailing_11="==============";//Trailing Settings
input double    TSL_points_11=0; // 11. Trailing Stop dollars
input double    TTL_points_11=0; // 11. Trailing Take add dollars
input double    TSL_points_KEF_11=0; // 11. Trailing Stop for KEF dollars
input double    TTL_points_KEF_11=0; // 11. Trailing Take for KEF add dollars
sinput string Change_Timeframe_11="==============";//Change Timeframe
input uint        change_tf_M15_after_11=0; // 11. Change to M15 if position more
input uint        change_tf_M30_after_11=0; // 11. Change to M30 if position more
input uint        change_tf_H1_after_11=0; // 11. Change to H1 if position more
input uint        change_tf_H4_after_11=0; // 11. Change to H4 if position more
input uint        change_tf_D1_after_11=0; // 11. Change to D1 if position more
sinput string delimeter_07_11="=============="; // --- Towards the bar
input bool        BARS_enabled_11=false; //11. Use bar entry
input    ENUM_TIMEFRAMES   BARS_Timeframe_11=PERIOD_D1; // 11. Timeframe
input int         BARS_RSI_Period_11=0; //11. Start only on RSI signal with period
sinput string delimeter_08_11="=============="; // --- Series of bars
input int         SERIES_count_11=0; //11. Enter at N bars one way
input TypeOfSeries SERIES_type_11=MY_SERIES_PLUS; // 11. Entry direction
input    ENUM_TIMEFRAMES   SERIES_Timeframe_11=PERIOD_M15; // 11. Timeframe
input int         SERIES_percent_11=100; //11. Percentage of bars
input bool        SERIES_use_point_11 = false; //11. Use points instead count
sinput string delimeter_09_11="=============="; // --- MA
input    ENUM_TIMEFRAMES   MA_Timeframe_11=PERIOD_D1; // 11. Timeframe
input int         MA_PERIOD_11=0; //11. Period
input TypeOfMA    MA_TYPE_11=MA_FIRST; // 11. Type of strategy
input bool        MA_invert_11=false; // 11. Invert signal
input bool        MA_across_price_11=false; // 11. Only when crossing the price with MA
input bool        MA_across_always_11=false; // 11. At all steps when crossing the price
sinput string delimeter_10_11="=============="; // --- RSI
input    ENUM_TIMEFRAMES   RSI_Timeframe_11=PERIOD_M5; // 11. Timeframe
input int         RSI_PERIOD_11=0; //11. Period
input bool        RSI_enter_return_11= false; // 11. Enter when exiting borders
input bool        RSI_only_first_11= false; // 11. Use RSI only on first login
input bool        RSI_invert_11= false; // 11. Invert signal
sinput string delimeter_11_11="=============="; // --- Bolinger Bands
input    ENUM_TIMEFRAMES   BB_Timeframe_11=PERIOD_M5; // 11. Timeframe
input int         BB_PERIOD_11=0; //11. Period
input int         BB_SHIFT_11=0; //11. Offset
input double      BB_DEVIATION_11=2; //11. Number of standard deviations
input bool        BB_only_first_11= false; // 11. Use BB only on first login
input bool        BB_INVERT_11=false; //11. Invert signal
sinput string delimeter_12_11="=============="; // --- Stochastic
input    ENUM_TIMEFRAMES   S_Timeframe_11=PERIOD_M5; // 11. Timeframe
input int         SK_PERIOD_11=0; //11. K Period
input int         SD_PERIOD_11=3; //11. D Period
input int         S_Slowing_11=3; //11. Slowing
input bool        S_INVERT_11=false; //11. Invert signal

sinput string delimeter_13="=============="; // --- Other
input TypeOfLang  LANG=MY_RUS; // Language

#ifdef __MQL5__ 
   #include <Trade\Trade.mqh>
   CTrade Trade;

   enum TypeOfFilling //Filling Mode
     {
      FOK,//ORDER_FILLING_FOK
      RETURN,// ORDER_FILLING_RETURN
      IOC,//ORDER_FILLING_IOC
     }; 
   input TypeOfFilling  useORDER_FILLING_RETURN=FOK; //Режим исполнения ордера
#endif 


ValType     takeProfitVal=VAL_DOLLAR;
ValType     gridStepVal=VAL_POINT;
double      FirstLot=0;
TypeOfLots  typeLot=MY_ARIFMET; // Chain increase
int         maxVol = 0; // Max volume
int         curOffsetMultChange = 0;
double      kefForBothClose=0.0;
bool        kefForAllPos=false;
bool        kefForMaxVolume=false;
bool        useAllProfitForClose=false; // Use All Profit for Partial closure
TypeOfDir   typeDir=MA_DIR_BOTH; // Направления входа
double      takeProfit=0; //При прибыли, $
double      takeProfit1step=0; //При прибыли на 1 шаге, $
double      takeProfit2step=0; //При прибыли на 2 шаге, $
double      takeProfit4step=0; //При прибыли на 4 шаге, $
uint        gridStepCountClose=0; // Закрывать все при трейде, номер
double      TSL_points=0; // Trailing Stop dollars
double      TTL_points=0; // Trailing Take add dollars
double      TSL_points_KEF=0; // Trailing Stop for KEF dollars
double      TTL_points_KEF=0; // Trailing Take for KEF add dollars
bool        BARS_enabled=false; //Использовать вход по бару
ENUM_TIMEFRAMES   BARS_Timeframe=PERIOD_D1; // Таймфрейм
int         BARS_RSI_Period=0; //Начинать только по сигналу RSI с периодом
int         SERIES_count=0; //Входить при N баров в одну сторону
TypeOfSeries SERIES_type=MY_SERIES_PLUS; // Направление входа
ENUM_TIMEFRAMES   SERIES_Timeframe=PERIOD_M15; // Таймфрейм
int         SERIES_percent = 100;
bool        SERIES_use_point = false;
ENUM_TIMEFRAMES   MA_Timeframe=PERIOD_D1; // Таймфрейм
int         MA_PERIOD=0; //Период
TypeOfMA    MA_TYPE=MA_FIRST; // Вид стратегии
bool        MA_invert=false; // Инвертировать сигнал
bool        MA_across_price=false; // Только при пересечении цены с MA
bool        MA_across_always=false; // На всех шагах при пересечении цены
ENUM_TIMEFRAMES   RSI_Timeframe=PERIOD_M5; // Таймфрейм
int         RSI_PERIOD=0; //Период
bool        RSI_enter_return= false; // Входить при выходе из границ
bool        RSI_only_first= false; // Использовать RSI только при первом входе
bool        RSI_invert= false; // Инвертировать сигнал
ENUM_TIMEFRAMES   BB_Timeframe=PERIOD_M5; // Таймфрейм
int         BB_PERIOD=0; //Период
int         BB_SHIFT=0; //Смещение
double      BB_DEVIATION=2; //Кол-во стандартных отклонений
bool        BB_only_first= false; // Use BB only on first login
bool        BB_INVERT=false; //Инвертировать сигнал
ENUM_TIMEFRAMES   S_Timeframe=PERIOD_M5; // Timeframe
int         SK_PERIOD=0; //K Period
int         SD_PERIOD=3; //D Period
int         S_Slowing=3; //Slowing
bool        S_INVERT=false; //Invert signal
uint        change_tf_M15_after=0; // Change to M15 if position more
uint        change_tf_M30_after=0; // Change to M30 if position more
uint        change_tf_H1_after=0; // Change to H1 if position more
uint        change_tf_H4_after=0; // Change to H4 if position more
uint        change_tf_D1_after=0; // Change to D1 if position more
double curTakeForPos = 0;

MqlTick lastme;
uint LongPos[11];
uint ShortPos[11];
double LongVol[11];
double ShortVol[11];
string prefix_graph="griderKatMulti_";
string msg_lines[];
uchar lines_size=0;
double curLot;
double mSpread;
int h[11];
int h2[11];
int h3[11];
int h4[11];
int h5[11];
int h_all;
int h2_all;
int h3_all;
int h4_all;
int h5_all;
int hIndex=0;
double buf0[];
double buf1[];
double buf2[];
int cur_offset;
bool noLong=false;
bool noShort=false;
datetime Old_Time[11];
double LastLong=0;
double LastShort=0;
bool RSI_start[11];
bool BB_start[11];
bool S_start[11];
bool MA_start[11];
uint MA_cur_dir[11];
bool need_close[11];
double lastRSI[11];
double lastPrices[11];
double cur_take_prices[11];
double KEF_cur_take_prices[11];
double pointPrice[11];
TypeOfPos lastPosIs[11];
double lastProfit[11];
ulong maxVolTicket = 0;
double maxVolIs = 0;
double maxVolProfit = 0;

ulong minVolTicket = 0;
double minVolIs = 0;
double minVolProfit = 0;

ulong maxLossTicket = 0;
double maxLossIs = 0;
double maxLossProfit = 0;



double maxProfit = 0;
ulong maxProfitTicket = 0;
double maxProfitVol = 0;
double minPriceLevel = 0;
double maxPriceLevel = 0;
int    maxTotalPosCntr=0;
double      maxTotalVolCntr=0;
double curProfitForAllPos = 0;
uint maxCmtNumBuy = 0;
uint maxCmtNumShort = 0;
MqlDateTime curT;
uint lotMultiplier = 1;

double maxCntrMarga = 0;
uint maxCntrSteps = 0;
string cmtData = "";
bool needCmtData = false;
bool defenseExists = false;
double defenseProfit = 0;
ulong defenseTicket = 0;
double defenseVol = 0;

bool assistExists = false;
int countCurSymb = 0;

struct translate{
   string err1;
   string err2;
   string err3;
   string err4;
   string err5;
   string err6;
   string err7;
   string err8;
   string err9;
   string err64;
   string err65;
   string err128;
   string err129;
   string err130;
   string err131;
   string err132;
   string err133;
   string err134;
   string err135;
   string err136;
   string err137;
   string err138;
   string err139;
   string err140;
   string err141;
   string err145;
   string err146;
   string err147;
   string err148;
   string err0;
   string retcode;
   string retcode10004;
   string retcode10006;
   string retcode10007;
   string retcode10010;
   string retcode10011;
   string retcode10012;
   string retcode10013;
   string retcode10014;
   string retcode10015;
   string retcode10016;
   string retcode10017;
   string retcode10018;
   string retcode10019;
   string retcode10020;
   string retcode10021;
   string retcode10022;
   string retcode10023;
   string retcode10024;
   string retcode10025;
   string retcode10026;
   string retcode10027;
   string retcode10028;
   string retcode10029;
   string retcode10030;
   string retcode10031;
   string retcode10032;
   string retcode10033;
   string retcode10034;
   string retcode10035;
   string retcode10036;
   string retcode10038;
   string retcode10039;
   string retcode10040;
   string retcode10041;
   string retcode10042;
   string retcode10043;
   string retcode10044;
};
translate langs;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if(!MQLInfoInteger(MQL_TRADE_ALLOWED)){
      Comment("Торговля для эксперта запрещена! ("+(string) EA_Magic+")");
      return(INIT_SUCCEEDED);
   }
   if( GlobalVariableCheck(prefix_graph+"_equity") ){
      Comment("Equity on begin: "+(string) GlobalVariableGet(prefix_graph+"_equity"));
   }
   init_lang();
   
   for(int i=0; i<11; i++){
      Old_Time[i]=0;
      LongPos[i]=0;
      ShortPos[i]=0;
      LongVol[i]=0;
      ShortVol[i]=0;
      RSI_start[i]=false;
      BB_start[i]=false;
      MA_start[i]=false;
      MA_cur_dir[i]=0;
      need_close[i] = true;
      cur_take_prices[i] = 0;
      lastProfit[i] = 0;
      KEF_cur_take_prices[i] = 0;
      lastPosIs[i] = MY_ALLPOS;
      lastRSI[i] = 0;
      lastPrices[i] = 0;
      pointPrice[i] = 0;
   }
   
   if (!allSymb_use) {
      #ifdef __MQL5__ 
         if( StringLen(symName_1) && useSym_1 ){
            string curSym = symName_1;
            if (replaceFirstSymbol) {
               curSym = _Symbol;
            }
            if(MA_PERIOD_1>0){
               h[0] = iMA(curSym, MA_Timeframe_1, MA_PERIOD_1, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_1>0){
               h2[0] = iRSI(curSym, RSI_Timeframe_1, RSI_PERIOD_1, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_1>0){
               h3[0] = iRSI(curSym, PERIOD_M5, BARS_RSI_Period_1, PRICE_CLOSE);
            }
            if(BB_PERIOD_1>0){
               h4[0] = iBands(curSym, BB_Timeframe_1, BB_PERIOD_1, BB_SHIFT_1, BB_DEVIATION_1, PRICE_CLOSE);
            }
            if( SK_PERIOD_1 > 0 ){
               h5[0] = iStochastic(curSym, S_Timeframe_1, SK_PERIOD_1, SD_PERIOD_1, S_Slowing_1, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_2) && useSym_2 ){
            if( MA_PERIOD_2>0){
               h[1] = iMA(symName_2, MA_Timeframe_2, MA_PERIOD_2, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_2>0){
               h2[1] = iRSI(symName_2, RSI_Timeframe_2, RSI_PERIOD_2, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_2>0){
               h3[1] = iRSI(symName_2, PERIOD_M5, BARS_RSI_Period_2, PRICE_CLOSE);
            }
            if(BB_PERIOD_2>0){
               h4[1] = iBands(symName_2, BB_Timeframe_2, BB_PERIOD_2, BB_SHIFT_2, BB_DEVIATION_2, PRICE_CLOSE);
            }
            if( SK_PERIOD_2 > 0 ){
               h5[1] = iStochastic(symName_2, S_Timeframe_2, SK_PERIOD_2, SD_PERIOD_2, S_Slowing_2, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_3) && useSym_3 ){
            if( MA_PERIOD_3>0){
               h[2] = iMA(symName_3, MA_Timeframe_3, MA_PERIOD_3, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_3>0){
               h2[2] = iRSI(symName_3, RSI_Timeframe_3, RSI_PERIOD_3, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_3>0){
               h3[2] = iRSI(symName_3, PERIOD_M5, BARS_RSI_Period_3, PRICE_CLOSE);
            }
            if(BB_PERIOD_3>0){
               h4[2] = iBands(symName_3, BB_Timeframe_3, BB_PERIOD_3, BB_SHIFT_3, BB_DEVIATION_3, PRICE_CLOSE);
            }
            if( SK_PERIOD_3 > 0 ){
               h5[2] = iStochastic(symName_3, S_Timeframe_3, SK_PERIOD_3, SD_PERIOD_3, S_Slowing_3, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_4) && useSym_4 ){
            if( MA_PERIOD_4>0){
               h[3] = iMA(symName_4, MA_Timeframe_4, MA_PERIOD_4, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_4>0){
               h2[3] = iRSI(symName_4, RSI_Timeframe_4, RSI_PERIOD_4, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_4>0){
               h3[3] = iRSI(symName_4, PERIOD_M5, BARS_RSI_Period_4, PRICE_CLOSE);
            }
            if(BB_PERIOD_4>0){
               h4[3] = iBands(symName_4, BB_Timeframe_4, BB_PERIOD_4, BB_SHIFT_4, BB_DEVIATION_4, PRICE_CLOSE);
            }
            if( SK_PERIOD_4 > 0 ){
               h5[3] = iStochastic(symName_4, S_Timeframe_4, SK_PERIOD_4, SD_PERIOD_4, S_Slowing_4, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_5) && useSym_5 ){
            if( MA_PERIOD_5>0){
               h[4] = iMA(symName_5, MA_Timeframe_5, MA_PERIOD_5, 0, MODE_SMA, PRICE_CLOSE);
            }else if(RSI_PERIOD_5>0){
               h2[4] = iRSI(symName_5, RSI_Timeframe_5, RSI_PERIOD_5, PRICE_CLOSE);
            }else if(BARS_RSI_Period_5>0){
               h3[4] = iRSI(symName_5, PERIOD_M5, BARS_RSI_Period_5, PRICE_CLOSE);
            }else if(BB_PERIOD_5>0){
               h4[4] = iBands(symName_5, BB_Timeframe_5, BB_PERIOD_5, BB_SHIFT_5, BB_DEVIATION_5, PRICE_CLOSE);
            }else if( SK_PERIOD_5 > 0 ){
               h5[4] = iStochastic(symName_5, S_Timeframe_5, SK_PERIOD_5, SD_PERIOD_5, S_Slowing_5, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_6) && useSym_6 ){
            if( MA_PERIOD_6>0){
               h[5] = iMA(symName_6, MA_Timeframe_6, MA_PERIOD_6, 0, MODE_SMA, PRICE_CLOSE);
            }else if(RSI_PERIOD_6>0){
               h2[5] = iRSI(symName_6, RSI_Timeframe_6, RSI_PERIOD_6, PRICE_CLOSE);
            }else if(BARS_RSI_Period_6>0){
               h3[5] = iRSI(symName_6, PERIOD_M5, BARS_RSI_Period_6, PRICE_CLOSE);
            }else if(BB_PERIOD_6>0){
               h4[5] = iBands(symName_6, BB_Timeframe_6, BB_PERIOD_6, BB_SHIFT_6, BB_DEVIATION_6, PRICE_CLOSE);
            }else if( SK_PERIOD_6 > 0 ){
               h5[5] = iStochastic(symName_6, S_Timeframe_6, SK_PERIOD_6, SD_PERIOD_6, S_Slowing_6, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_7) && useSym_7 ){
            if( MA_PERIOD_7>0){
               h[6] = iMA(symName_7, MA_Timeframe_7, MA_PERIOD_7, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_7>0){
               h2[6] = iRSI(symName_7, RSI_Timeframe_7, RSI_PERIOD_7, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_7>0){
               h3[6] = iRSI(symName_7, PERIOD_M5, BARS_RSI_Period_7, PRICE_CLOSE);
            }
            if(BB_PERIOD_7>0){
               h4[6] = iBands(symName_7, BB_Timeframe_7, BB_PERIOD_7, BB_SHIFT_7, BB_DEVIATION_7, PRICE_CLOSE);
            }
            if( SK_PERIOD_7 > 0 ){
               h5[6] = iStochastic(symName_7, S_Timeframe_7, SK_PERIOD_7, SD_PERIOD_7, S_Slowing_7, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_8) && useSym_8 ){
            if( MA_PERIOD_8>0){
               h[7] = iMA(symName_8, MA_Timeframe_8, MA_PERIOD_8, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_8>0){
               h2[7] = iRSI(symName_8, RSI_Timeframe_8, RSI_PERIOD_8, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_8>0){
               h3[7] = iRSI(symName_8, PERIOD_M5, BARS_RSI_Period_8, PRICE_CLOSE);
            }
            if(BB_PERIOD_8>0){
               h4[7] = iBands(symName_8, BB_Timeframe_8, BB_PERIOD_8, BB_SHIFT_8, BB_DEVIATION_8, PRICE_CLOSE);
            }
            if( SK_PERIOD_8 > 0 ){
               h5[7] = iStochastic(symName_8, S_Timeframe_8, SK_PERIOD_8, SD_PERIOD_8, S_Slowing_8, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_9) && useSym_9 ){
            if( MA_PERIOD_9>0){
               h[8] = iMA(symName_9, MA_Timeframe_9, MA_PERIOD_9, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_9>0){
               h2[8] = iRSI(symName_9, RSI_Timeframe_9, RSI_PERIOD_9, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_9>0){
               h3[8] = iRSI(symName_9, PERIOD_M5, BARS_RSI_Period_9, PRICE_CLOSE);
            }
            if(BB_PERIOD_9>0){
               h4[8] = iBands(symName_9, BB_Timeframe_9, BB_PERIOD_9, BB_SHIFT_9, BB_DEVIATION_9, PRICE_CLOSE);
            }
            if( SK_PERIOD_9 > 0 ){
               h5[8] = iStochastic(symName_9, S_Timeframe_9, SK_PERIOD_9, SD_PERIOD_9, S_Slowing_9, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_10) && useSym_10 ){
            if( MA_PERIOD_10>0){
               h[9] = iMA(symName_10, MA_Timeframe_10, MA_PERIOD_10, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_10>0){
               h2[9] = iRSI(symName_10, RSI_Timeframe_10, RSI_PERIOD_10, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_10>0){
               h3[9] = iRSI(symName_10, PERIOD_M5, BARS_RSI_Period_10, PRICE_CLOSE);
            }
            if(BB_PERIOD_10>0){
               h4[9] = iBands(symName_10, BB_Timeframe_10, BB_PERIOD_10, BB_SHIFT_10, BB_DEVIATION_10, PRICE_CLOSE);
            }
            if( SK_PERIOD_10 > 0 ){
               h5[9] = iStochastic(symName_10, S_Timeframe_10, SK_PERIOD_10, SD_PERIOD_10, S_Slowing_10, MODE_SMA, STO_LOWHIGH);
            }
         }
         if( StringLen(symName_11) && useSym_11 ){
            if( MA_PERIOD_11>0){
               h[10] = iMA(symName_11, MA_Timeframe_11, MA_PERIOD_11, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_11>0){
               h2[10] = iRSI(symName_11, RSI_Timeframe_11, RSI_PERIOD_11, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_11>0){
               h3[10] = iRSI(symName_11, PERIOD_M5, BARS_RSI_Period_11, PRICE_CLOSE);
            }
            if(BB_PERIOD_11>0){
               h4[10] = iBands(symName_11, BB_Timeframe_11, BB_PERIOD_11, BB_SHIFT_11, BB_DEVIATION_11, PRICE_CLOSE);
            }
            if( SK_PERIOD_11 > 0 ){
               h5[10] = iStochastic(symName_11, S_Timeframe_11, SK_PERIOD_11, SD_PERIOD_11, S_Slowing_11, MODE_SMA, STO_LOWHIGH);
            }
         }
      #endif 
   }
      
   if( !EventSetTimer(7) ){
      Comment("Timer NOT SET!");
   }else{
      Comment("");
   }
   
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
      ObjectsDeleteAll(0, prefix_graph);
      if (!allSymb_use) {
         #ifdef __MQL5__ 
            if( StringLen(symName_1) && useSym_1 ){
               if( MA_PERIOD_1>0 ){
                  IndicatorRelease(h[0]);
               }
               if( RSI_PERIOD_1>0 ){
                  IndicatorRelease(h2[0]);
               }
               if( BARS_RSI_Period_1>0 ){
                  IndicatorRelease(h3[0]);
               }
               if( BB_PERIOD_1>0 ){
                  IndicatorRelease(h4[0]);
               }
               if( SK_PERIOD_1>0 ){
                  IndicatorRelease(h5[0]);
               }
            }
            if( StringLen(symName_2) && useSym_2 ){
               if( MA_PERIOD_2>0 ){
                  IndicatorRelease(h[1]);
               }
               if( RSI_PERIOD_2>0 ){
                  IndicatorRelease(h2[1]);
               }
               if( BARS_RSI_Period_2>0 ){
                  IndicatorRelease(h3[1]);
               }
               if( BB_PERIOD_2>0 ){
                  IndicatorRelease(h4[1]);
               }
               if( SK_PERIOD_2>0 ){
                  IndicatorRelease(h5[1]);
               }
            }
            if( StringLen(symName_3) && useSym_3 ){
               if( MA_PERIOD_3>0 ){
                  IndicatorRelease(h[2]);
               }
               if( RSI_PERIOD_3>0 ){
                  IndicatorRelease(h2[2]);
               }
               if( BARS_RSI_Period_3>0 ){
                  IndicatorRelease(h3[2]);
               }
               if( BB_PERIOD_3>0 ){
                  IndicatorRelease(h4[2]);
               }
               if( SK_PERIOD_3>0 ){
                  IndicatorRelease(h5[2]);
               }
            }
            if( StringLen(symName_4) && useSym_4 ){
               if( MA_PERIOD_4>0 ){
                  IndicatorRelease(h[3]);
               }
               if( RSI_PERIOD_4>0 ){
                  IndicatorRelease(h2[3]);
               }
               if( BARS_RSI_Period_4>0 ){
                  IndicatorRelease(h3[3]);
               }
               if( BB_PERIOD_4>0 ){
                  IndicatorRelease(h4[3]);
               }
               if( SK_PERIOD_4>0 ){
                  IndicatorRelease(h5[3]);
               }
            }
            if( StringLen(symName_5) && useSym_5 ){
               if( MA_PERIOD_5>0 ){
                  IndicatorRelease(h[4]);
               }
               if( RSI_PERIOD_5>0 ){
                  IndicatorRelease(h2[4]);
               }
               if( BARS_RSI_Period_5>0 ){
                  IndicatorRelease(h3[4]);
               }
               if( BB_PERIOD_5>0 ){
                  IndicatorRelease(h4[4]);
               }
               if( SK_PERIOD_5>0 ){
                  IndicatorRelease(h5[4]);
               }
            }
            if( StringLen(symName_6) && useSym_6 ){
               if( MA_PERIOD_6>0 ){
                  IndicatorRelease(h[5]);
               }
               if( RSI_PERIOD_6>0 ){
                  IndicatorRelease(h2[5]);
               }
               if( BARS_RSI_Period_6>0 ){
                  IndicatorRelease(h3[5]);
               }
               if( BB_PERIOD_6>0 ){
                  IndicatorRelease(h4[5]);
               }
               if( SK_PERIOD_6>0 ){
                  IndicatorRelease(h5[5]);
               }
            }
            if( StringLen(symName_7) && useSym_7 ){
               if( MA_PERIOD_7>0 ){
                  IndicatorRelease(h[6]);
               }
               if( RSI_PERIOD_7>0 ){
                  IndicatorRelease(h2[6]);
               }
               if( BARS_RSI_Period_7>0 ){
                  IndicatorRelease(h3[6]);
               }
               if( BB_PERIOD_7>0 ){
                  IndicatorRelease(h4[6]);
               }
               if( SK_PERIOD_7>0 ){
                  IndicatorRelease(h5[6]);
               }
            }
            if( StringLen(symName_8) && useSym_8 ){
               if( MA_PERIOD_8>0 ){
                  IndicatorRelease(h[7]);
               }
               if( RSI_PERIOD_8>0 ){
                  IndicatorRelease(h2[7]);
               }
               if( BARS_RSI_Period_8>0 ){
                  IndicatorRelease(h3[7]);
               }
               if( BB_PERIOD_8>0 ){
                  IndicatorRelease(h4[7]);
               }
               if( SK_PERIOD_8>0 ){
                  IndicatorRelease(h5[7]);
               }
            }
            if( StringLen(symName_9) && useSym_9 ){
               if( MA_PERIOD_9>0 ){
                  IndicatorRelease(h[8]);
               }
               if( RSI_PERIOD_9>0 ){
                  IndicatorRelease(h2[8]);
               }
               if( BARS_RSI_Period_9>0 ){
                  IndicatorRelease(h3[8]);
               }
               if( BB_PERIOD_9>0 ){
                  IndicatorRelease(h4[8]);
               }
               if( SK_PERIOD_9>0 ){
                  IndicatorRelease(h5[8]);
               }
            }
            if( StringLen(symName_10) && useSym_10 ){
               if( MA_PERIOD_10>0 ){
                  IndicatorRelease(h[9]);
               }
               if( RSI_PERIOD_10>0 ){
                  IndicatorRelease(h2[9]);
               }
               if( BARS_RSI_Period_10>0 ){
                  IndicatorRelease(h3[9]);
               }
               if( BB_PERIOD_10>0 ){
                  IndicatorRelease(h4[9]);
               }
               if( SK_PERIOD_10>0 ){
                  IndicatorRelease(h5[9]);
               }
            }
            if( StringLen(symName_11) && useSym_11 ){
               if( MA_PERIOD_11>0 ){
                  IndicatorRelease(h[10]);
               }
               if( RSI_PERIOD_11>0 ){
                  IndicatorRelease(h2[10]);
               }
               if( BARS_RSI_Period_11>0 ){
                  IndicatorRelease(h3[10]);
               }
               if( BB_PERIOD_11>0 ){
                  IndicatorRelease(h4[10]);
               }
               if( SK_PERIOD_11>0 ){
                  IndicatorRelease(h5[10]);
               }
            }
            
         #endif 
      }
      EventKillTimer();
      
      Print("--- maxCntrMarga: " + (string) maxCntrMarga);
      Print("--- maxCntrSteps: " + (string) maxCntrSteps);
  }
  
void startTimer(string sym, int hI, double Lt, double gStep){
   hIndex=hI;
   curLot=Lt;
   if ( !allSymb_use && needCmtData) {
      prepareCmtData(sym, gStep);
   }
   
   Print(sym);
   
   switch (gridStepVal) {
      case VAL_PERCENT:
         cur_offset = (int) (((lastme.bid/100)*gStep)/SymbolInfoDouble(sym, SYMBOL_POINT));
         break;
      case VAL_DOLLAR:
         cur_offset = (int) (gStep/SymbolInfoDouble(sym, SYMBOL_POINT));
         break;
      default:
         cur_offset=(int) gStep;
         break;
   }
   
   mSpread=minSpread;
   if(SymbolInfoInteger(sym, SYMBOL_DIGITS)==5 || SymbolInfoInteger(sym, SYMBOL_DIGITS)==3){
      cur_offset*=10;
      if(  mSpread>0 ){
         mSpread*=10;
      }
      if(  curOffsetMultChange>0 ){
         curOffsetMultChange*=10;
      }
   }

   SymbolInfoTick(sym, lastme);
   if( lastme.bid==0 ){
      return;
   }
   
   if ( pointPrice[hI] == 0 ) {
      double profit;
      #ifdef __MQL5__ 
         if( OrderCalcProfit(ORDER_TYPE_BUY, sym, Lt, NormalizeDouble(lastme.bid,_Digits), NormalizeDouble(lastme.bid-1*SymbolInfoDouble(sym, SYMBOL_POINT),_Digits), profit) && profit<0 ){
         }
      #else
         profit=(NormalizeDouble(lastme.bid-1*SymbolInfoDouble(sym, SYMBOL_POINT),_Digits)-NormalizeDouble(lastme.bid,_Digits))*Lt* (1 / MarketInfo(sym, MODE_POINT)) * MarketInfo(sym, MODE_TICKVALUE);
      #endif 
      if( profit!=0 ){
         profit=MathAbs(profit);
         pointPrice[hI] = profit;
      }
   }
   
   if ( takeProfit > 0 ) {
      switch (takeProfitVal) {
         case VAL_PERCENT:
            takeProfit = (lastme.bid/100)*takeProfit;
            break;
         case VAL_POINT:
            if (pointPrice[hI] > 0) {
               takeProfit = takeProfit*pointPrice[hI];
            }
            break;
         case VAL_DOLLAR:
            break;
         case VAL_STEPOFGRID:
            if (pointPrice[hI] > 0) {
               takeProfit = takeProfit*cur_offset*pointPrice[hI];
            }
            break;
      }
   }
   
   if ( takeForPos > 0 ) {
      switch (takeProfitVal) {
         case VAL_PERCENT:
            curTakeForPos = (lastme.bid/100)*takeForPos;
            break;
         case VAL_POINT:
            if (pointPrice[hI] > 0) {
               curTakeForPos = curTakeForPos*pointPrice[hI];
            }
            break;
         case VAL_DOLLAR:
            curTakeForPos = takeForPos;
            break;
         case VAL_STEPOFGRID:
            if (pointPrice[hI] > 0) {
               curTakeForPos = takeForPos*cur_offset*pointPrice[hI];
            }
            break;
      }
      if ( encreaseTakeOnNextStep ) {
         if ( LongPos[hIndex] > 1 ) {
            curTakeForPos *= LongPos[hIndex];
         } else if ( ShortPos[hIndex] > 1 ) {
            curTakeForPos *= ShortPos[hIndex];
         }
      }
   } else {
      curTakeForPos = 0;
   }
   
   getmeinfo_btn(sym, (EA_Magic*10) + hIndex);
   
   if (needCmtData && StringLen(cmtData) && hIndex == 10) {
      string tmpStr = cmtData;
      TimeCurrent(curT);
      StringAdd(tmpStr, "upd: " + (string) StructToTime(curT));
      Comment(tmpStr);
   }
   
   if (workType == WORK_GRIDER_BOTH) {
      if( checkTakeProfit(sym, MY_BUY)){
         closeAllPos(sym, MY_BUY);
      }
      if( checkTakeProfit(sym, MY_SELL)){
         closeAllPos(sym, MY_SELL);
      }
   } else {
      need_close[hI]=false;
      if (cur_take_prices[hI] > 0) {
         checkTakeProfit(sym);
         if( lastProfit[hI] - cur_take_prices[hI] >= TTL_points ){
            cur_take_prices[hI] = lastProfit[hI];
         }else if( cur_take_prices[hI] - lastProfit[hI] > TSL_points ){
            need_close[hI]=true;
         }
      } else if( checkTakeProfit(sym)){
         need_close[hI]=true;
         if( TSL_points>0 && TTL_points>0 ){
            need_close[hI]=false;
            if (cur_take_prices[hI] > 0) {
               if( lastProfit[hI] - cur_take_prices[hI] >= TTL_points ){
                  cur_take_prices[hI] = lastProfit[hI];
               }else if( cur_take_prices[hI] - lastProfit[hI] > TSL_points ){
                  need_close[hI]=true;
               }
            } else {
               cur_take_prices[hI] = lastProfit[hI];
            }
         }
      }
      if (need_close[hI] == true) {
         closeAllPos(sym);
      }
   }
      
   if( !pdxIsNewBar(sym, hI) ){
      return;
   }
   
   lastPrices[hI] = lastme.bid;
   
   if ( kefForBothClose > 0 ) {
      switch (takeProfitVal) {
         case VAL_PERCENT:
            kefForBothClose = (lastme.bid/100)*kefForBothClose;
            break;
         case VAL_POINT:
            if (pointPrice[hI] > 0) {
               kefForBothClose *= pointPrice[hI];
            }
            break;
         case VAL_STEPOFGRID:
            if (pointPrice[hI] > 0) {
               kefForBothClose = kefForBothClose*cur_offset*pointPrice[hI];
            }
            break;
      }
   }
   
   startCheckPositions((EA_Magic*10) + hI, sym, Lt);
}
void prepareCmtData(string dataSym, double datagStep) {
   string strA = "";
   if (RSI_PERIOD > 0) {
      StringAdd(strA, "RSI " + (string) RSI_PERIOD);
      if (RSI_invert) {
         StringAdd(strA, " INV");
      }
      if (lastRSI[hIndex] > 0) {
         StringAdd(strA, "(" + (string) MathRound(lastRSI[hIndex]) + ")");
      }
   }

   if (lastPrices[hIndex] > 0) {
      StringAdd(strA, " --- " + (string) ((MathRound(lastPrices[hIndex]*100))/100));
   }
   
   
   StringAdd(cmtData, (string) (hIndex + 1) + ". " + dataSym + ": " + (string) takeProfit + " ( " + (string) kefForBothClose + ") $ - " + (string) datagStep + " p " + strA + "\r\n");
}
void OnTimer()
  {
   if( equityLess>0 && AccountInfoDouble(ACCOUNT_BALANCE)-AccountInfoDouble(ACCOUNT_EQUITY)>= equityLess ){
      closeAllPos();
   }
   if (closeAtTotal > 0 && AccountInfoDouble(ACCOUNT_EQUITY) - AccountInfoDouble(ACCOUNT_BALANCE) > closeAtTotal ) {
      closeAllPos();
   }
   double tmpLot = 0;
   double lotXtypeIS = 0;
   if (lotXtype == ACC_BALANCE) {
      lotXtypeIS = AccountInfoDouble(ACCOUNT_BALANCE);
   } else if (lotXtype == ACC_EQUITY) {
      lotXtypeIS = AccountInfoDouble(ACCOUNT_EQUITY);
   }
   
   if ( lot25x > 0 && lotXtypeIS > lot25x ) {
      tmpLot*= 25;
      lotMultiplier = 25;
   } else if ( lot20x > 0 && lotXtypeIS > lot20x ) {
      tmpLot*= 20;
      lotMultiplier = 20;
   } else if ( lot15x > 0 && lotXtypeIS > lot15x ) {
      tmpLot*= 15;
      lotMultiplier = 15;
   } else if ( lot11x > 0 && lotXtypeIS > lot11x ) {
      tmpLot*= 11;
      lotMultiplier = 11;
   } else if ( lot10x > 0 && lotXtypeIS > lot10x ) {
      tmpLot*= 10;
      lotMultiplier = 10;
   } else if ( lot9x > 0 && lotXtypeIS > lot9x ) {
      tmpLot*= 9;
      lotMultiplier = 9;
   } else if ( lot8x > 0 && lotXtypeIS > lot8x ) {
      tmpLot*= 8;
      lotMultiplier = 8;
   } else if ( lot7x > 0 && lotXtypeIS > lot7x ) {
      tmpLot*= 7;
      lotMultiplier = 7;
   } else if ( lot6x > 0 && lotXtypeIS > lot6x ) {
      tmpLot*= 6;
      lotMultiplier = 6;
   } else if ( lot5x > 0 && lotXtypeIS > lot5x ) {
      tmpLot*= 5;
      lotMultiplier = 5;
   } else if ( lot4x > 0 && lotXtypeIS > lot4x ) {
      tmpLot*= 4;
      lotMultiplier = 4;
   } else if ( lot3x > 0 && lotXtypeIS > lot3x ) {
      tmpLot*= 3;
      lotMultiplier = 3;
   } else if ( lot2x > 0 && lotXtypeIS > lot2x ) {
      tmpLot*= 2;
      lotMultiplier = 2;
   } else {
      lotMultiplier = 1;
   }
   
   if (!StringLen(cmtData)) {
      needCmtData = true;
   } else {
      needCmtData = false;
   }
   
   if (allSymb_use) {
      // поиск нового символа для открытия позиции
      int symb_count = SymbolsTotal(allSymb_marketwatch);
      for (int i = 0; i < symb_count; i++) {
         string curSymb = SymbolName(i, allSymb_marketwatch);
         
         // исключение на основе PATH
         if ( StringLen(allSymb_path) ) {
            string curSymbPath = SymbolInfoString(curSymb, SYMBOL_PATH);
            if ( StringLen(curSymbPath) ) {
               if (StringFind(curSymbPath, allSymb_path) >= 0) {
               } else {
                  continue;
               }
            }
         }
         
         // исключение по максимальной цене
         if (allSymb_max_prices > 0) {
            MqlTick curSymbPrice;
            SymbolInfoTick(curSymb, curSymbPrice);
            if( curSymbPrice.bid==0 ){
               continue;
            }
            if( curSymbPrice.bid > allSymb_max_prices ){
               continue;
            }
         }
         
         #ifdef __MQL5__ 
            if(MA_PERIOD_1>0){
               h_all = iMA(curSymb, MA_Timeframe_1, MA_PERIOD_1, 0, MODE_SMA, PRICE_CLOSE);
            }
            if(RSI_PERIOD_1>0){
               h2_all = iRSI(curSymb, RSI_Timeframe_1, RSI_PERIOD_1, PRICE_CLOSE);
            }
            if(BARS_RSI_Period_1>0){
               h3_all = iRSI(curSymb, PERIOD_M5, BARS_RSI_Period_1, PRICE_CLOSE);
            }
            if(BB_PERIOD_1>0){
               h4_all = iBands(curSymb, BB_Timeframe_1, BB_PERIOD_1, BB_SHIFT_1, BB_DEVIATION_1, PRICE_CLOSE);
            }
            if( SK_PERIOD_1 > 0 ){
               h5_all = iStochastic(curSymb, S_Timeframe_1, SK_PERIOD_1, SD_PERIOD_1, S_Slowing_1, MODE_SMA, STO_LOWHIGH);
            }
         #endif 
         
         takeProfitVal = takeProfitVal_1;
         gridStepVal = gridStepVal_1;
         tmpLot = Lot_1*lotMultiplier;
         FirstLot=FirstLot_1*lotMultiplier;
         typeLot = typeLot_1;
         maxVol = maxVol_1;
         curOffsetMultChange = curOffsetMultChange_1;
         kefForBothClose = kefForBothClose_1*lotMultiplier;
         kefForAllPos = kefForAllPos_1;
         kefForMaxVolume = kefForMaxVolume_1;
         useAllProfitForClose = useAllProfitForClose_1;
         typeDir=typeDir_1;
         takeProfit=takeProfit_1*lotMultiplier;
         takeProfit1step=takeProfit1step_1*lotMultiplier;
         takeProfit2step=takeProfit2step_1*lotMultiplier;
         takeProfit4step=takeProfit4step_1*lotMultiplier;
         gridStepCountClose=gridStepCountClose_1;
         BARS_enabled=BARS_enabled_1;
         BARS_Timeframe=BARS_Timeframe_1;
         BARS_RSI_Period=BARS_RSI_Period_1;
         SERIES_count=SERIES_count_1;
         SERIES_type=SERIES_type_1;
         SERIES_Timeframe=SERIES_Timeframe_1;
         SERIES_percent = SERIES_percent_1;
         SERIES_use_point = SERIES_use_point_1;
         MA_Timeframe=MA_Timeframe_1;
         MA_PERIOD=MA_PERIOD_1;
         MA_TYPE=MA_TYPE_1;
         MA_invert=MA_invert_1;
         MA_across_price=MA_across_price_1;
         MA_across_always=MA_across_always_1;
         RSI_Timeframe=RSI_Timeframe_1;
         RSI_PERIOD=RSI_PERIOD_1;
         RSI_enter_return= RSI_enter_return_1;
         RSI_only_first= RSI_only_first_1;
         RSI_invert= RSI_invert_1;
         BB_Timeframe=BB_Timeframe_1;
         BB_PERIOD=BB_PERIOD_1;
         BB_SHIFT=BB_SHIFT_1;
         BB_DEVIATION=BB_DEVIATION_1;
         BB_only_first= BB_only_first_1;
         BB_INVERT=BB_INVERT_1;
         S_Timeframe=S_Timeframe_1;
         SK_PERIOD=SK_PERIOD_1;
         SD_PERIOD=SD_PERIOD_1;
         S_Slowing=S_Slowing_1;
         S_INVERT=S_INVERT_1;
         TSL_points=TSL_points_1;
         TTL_points=TTL_points_1;
         TSL_points_KEF=TSL_points_KEF_1;
         TTL_points_KEF=TTL_points_KEF_1;
         change_tf_M15_after=change_tf_M15_after_1;
         change_tf_M30_after=change_tf_M30_after_1;
         change_tf_H1_after=change_tf_H1_after_1;
         change_tf_H4_after=change_tf_H4_after_1;
         change_tf_D1_after=change_tf_D1_after_1;
   
         string curSym = curSymb;
         
         startTimer(curSym, 0, tmpLot, gridStep_1);
         
         #ifdef __MQL5__ 
            if( MA_PERIOD_1>0 ){
               IndicatorRelease(h_all);
            }
            if( RSI_PERIOD_1>0 ){
               IndicatorRelease(h2_all);
            }
            if( BARS_RSI_Period_1>0 ){
               IndicatorRelease(h3_all);
            }
            if( BB_PERIOD_1>0 ){
               IndicatorRelease(h4_all);
            }
            if( SK_PERIOD_1>0 ){
               IndicatorRelease(h5_all);
            }
         #endif 
      }
//         ExpertRemove();
      
      
      
   } else {

      if( useSym_1 && StringLen(symName_1) ){
         takeProfitVal = takeProfitVal_1;
         gridStepVal = gridStepVal_1;
         tmpLot = Lot_1*lotMultiplier;
         FirstLot=FirstLot_1*lotMultiplier;
         typeLot = typeLot_1;
         maxVol = maxVol_1;
         curOffsetMultChange = curOffsetMultChange_1;
         kefForBothClose = kefForBothClose_1*lotMultiplier;
         kefForAllPos = kefForAllPos_1;
         kefForMaxVolume = kefForMaxVolume_1;
         useAllProfitForClose = useAllProfitForClose_1;
         typeDir=typeDir_1;
         takeProfit=takeProfit_1*lotMultiplier;
         takeProfit1step=takeProfit1step_1*lotMultiplier;
         takeProfit2step=takeProfit2step_1*lotMultiplier;
         takeProfit4step=takeProfit4step_1*lotMultiplier;
         gridStepCountClose=gridStepCountClose_1;
         BARS_enabled=BARS_enabled_1;
         BARS_Timeframe=BARS_Timeframe_1;
         BARS_RSI_Period=BARS_RSI_Period_1;
         SERIES_count=SERIES_count_1;
         SERIES_type=SERIES_type_1;
         SERIES_Timeframe=SERIES_Timeframe_1;
         SERIES_percent = SERIES_percent_1;
         SERIES_use_point = SERIES_use_point_1;
         MA_Timeframe=MA_Timeframe_1;
         MA_PERIOD=MA_PERIOD_1;
         MA_TYPE=MA_TYPE_1;
         MA_invert=MA_invert_1;
         MA_across_price=MA_across_price_1;
         MA_across_always=MA_across_always_1;
         RSI_Timeframe=RSI_Timeframe_1;
         RSI_PERIOD=RSI_PERIOD_1;
         RSI_enter_return= RSI_enter_return_1;
         RSI_only_first= RSI_only_first_1;
         RSI_invert= RSI_invert_1;
         BB_Timeframe=BB_Timeframe_1;
         BB_PERIOD=BB_PERIOD_1;
         BB_SHIFT=BB_SHIFT_1;
         BB_DEVIATION=BB_DEVIATION_1;
         BB_only_first= BB_only_first_1;
         BB_INVERT=BB_INVERT_1;
         S_Timeframe=S_Timeframe_1;
         SK_PERIOD=SK_PERIOD_1;
         SD_PERIOD=SD_PERIOD_1;
         S_Slowing=S_Slowing_1;
         S_INVERT=S_INVERT_1;
         TSL_points=TSL_points_1;
         TTL_points=TTL_points_1;
         TSL_points_KEF=TSL_points_KEF_1;
         TTL_points_KEF=TTL_points_KEF_1;
         change_tf_M15_after=change_tf_M15_after_1;
         change_tf_M30_after=change_tf_M30_after_1;
         change_tf_H1_after=change_tf_H1_after_1;
         change_tf_H4_after=change_tf_H4_after_1;
         change_tf_D1_after=change_tf_D1_after_1;
   
         string curSym = symName_1;
         if (replaceFirstSymbol) {
            curSym = _Symbol;
         }
         
         startTimer(curSym, 0, tmpLot, gridStep_1);
      }
      if( useSym_2 && StringLen(symName_2) ){
         takeProfitVal = takeProfitVal_2;
         gridStepVal = gridStepVal_2;
         tmpLot = Lot_2*lotMultiplier;
         FirstLot=FirstLot_2*lotMultiplier;
         typeLot = typeLot_2;
         maxVol = maxVol_2;
         curOffsetMultChange = curOffsetMultChange_2;
         kefForBothClose = kefForBothClose_2*lotMultiplier;
         kefForAllPos = kefForAllPos_2;
         kefForMaxVolume = kefForMaxVolume_2;
         useAllProfitForClose = useAllProfitForClose_2;
         typeDir=typeDir_2;
         takeProfit=takeProfit_2*lotMultiplier;
         takeProfit1step=takeProfit1step_2*lotMultiplier;
         takeProfit2step=takeProfit2step_2*lotMultiplier;
         takeProfit4step=takeProfit4step_2*lotMultiplier;
         gridStepCountClose=gridStepCountClose_2;
         BARS_enabled=BARS_enabled_2;
         BARS_Timeframe=BARS_Timeframe_2;
         BARS_RSI_Period=BARS_RSI_Period_2;
         SERIES_count=SERIES_count_2;
         SERIES_type=SERIES_type_2;
         SERIES_Timeframe=SERIES_Timeframe_2;
         SERIES_percent = SERIES_percent_2;
         SERIES_use_point = SERIES_use_point_2;
         MA_Timeframe=MA_Timeframe_2;
         MA_PERIOD=MA_PERIOD_2;
         MA_TYPE=MA_TYPE_2;
         MA_invert=MA_invert_2;
         MA_across_price=MA_across_price_2;
         MA_across_always=MA_across_always_2;
         RSI_Timeframe=RSI_Timeframe_2;
         RSI_PERIOD=RSI_PERIOD_2;
         RSI_enter_return= RSI_enter_return_2;
         RSI_only_first= RSI_only_first_2;
         RSI_invert= RSI_invert_2;
         BB_Timeframe=BB_Timeframe_2;
         BB_PERIOD=BB_PERIOD_2;
         BB_SHIFT=BB_SHIFT_2;
         BB_DEVIATION=BB_DEVIATION_2;
         BB_only_first= BB_only_first_2;
         BB_INVERT=BB_INVERT_2;
         S_Timeframe=S_Timeframe_2;
         SK_PERIOD=SK_PERIOD_2;
         SD_PERIOD=SD_PERIOD_2;
         S_Slowing=S_Slowing_2;
         S_INVERT=S_INVERT_2;
         TSL_points=TSL_points_2;
         TTL_points=TTL_points_2;
         TSL_points_KEF=TSL_points_KEF_2;
         TTL_points_KEF=TTL_points_KEF_2;
         change_tf_M15_after=change_tf_M15_after_2;
         change_tf_M30_after=change_tf_M30_after_2;
         change_tf_H1_after=change_tf_H1_after_2;
         change_tf_H4_after=change_tf_H4_after_2;
         change_tf_D1_after=change_tf_D1_after_2;
      
         startTimer(symName_2, 1, tmpLot, gridStep_2);
      }
      if( useSym_3 && StringLen(symName_3) ){
         takeProfitVal = takeProfitVal_3;
         gridStepVal = gridStepVal_3;
         tmpLot = Lot_3*lotMultiplier;
         FirstLot=FirstLot_3*lotMultiplier;
         typeLot = typeLot_3;
         maxVol = maxVol_3;
         curOffsetMultChange = curOffsetMultChange_3;
         kefForBothClose = kefForBothClose_3*lotMultiplier;
         kefForAllPos = kefForAllPos_3;
         kefForMaxVolume = kefForMaxVolume_3;
         useAllProfitForClose = useAllProfitForClose_3;
         typeDir=typeDir_3;
         takeProfit=takeProfit_3*lotMultiplier;
         takeProfit1step=takeProfit1step_3*lotMultiplier;
         takeProfit2step=takeProfit2step_3*lotMultiplier;
         takeProfit4step=takeProfit4step_3*lotMultiplier;
         gridStepCountClose=gridStepCountClose_3;
         BARS_enabled=BARS_enabled_3;
         BARS_Timeframe=BARS_Timeframe_3;
         BARS_RSI_Period=BARS_RSI_Period_3;
         SERIES_count=SERIES_count_3;
         SERIES_type=SERIES_type_3;
         SERIES_Timeframe=SERIES_Timeframe_3;
         SERIES_percent = SERIES_percent_3;
         SERIES_use_point = SERIES_use_point_3;
         MA_Timeframe=MA_Timeframe_3;
         MA_PERIOD=MA_PERIOD_3;
         MA_TYPE=MA_TYPE_3;
         MA_invert=MA_invert_3;
         MA_across_price=MA_across_price_3;
         MA_across_always=MA_across_always_3;
         RSI_Timeframe=RSI_Timeframe_3;
         RSI_PERIOD=RSI_PERIOD_3;
         RSI_enter_return= RSI_enter_return_3;
         RSI_only_first= RSI_only_first_3;
         RSI_invert= RSI_invert_3;
         BB_Timeframe=BB_Timeframe_3;
         BB_PERIOD=BB_PERIOD_3;
         BB_SHIFT=BB_SHIFT_3;
         BB_DEVIATION=BB_DEVIATION_3;
         BB_only_first= BB_only_first_3;
         BB_INVERT=BB_INVERT_3;
         S_Timeframe=S_Timeframe_3;
         SK_PERIOD=SK_PERIOD_3;
         SD_PERIOD=SD_PERIOD_3;
         S_Slowing=S_Slowing_3;
         S_INVERT=S_INVERT_3;
         TSL_points=TSL_points_3;
         TTL_points=TTL_points_3;
         TSL_points_KEF=TSL_points_KEF_3;
         TTL_points_KEF=TTL_points_KEF_3;
         change_tf_M15_after=change_tf_M15_after_3;
         change_tf_M30_after=change_tf_M30_after_3;
         change_tf_H1_after=change_tf_H1_after_3;
         change_tf_H4_after=change_tf_H4_after_3;
         change_tf_D1_after=change_tf_D1_after_3;
      
         startTimer(symName_3, 2, tmpLot, gridStep_3);
      }
      if( useSym_4 && StringLen(symName_4) ){
         takeProfitVal = takeProfitVal_4;
         gridStepVal = gridStepVal_4;
         tmpLot = Lot_4*lotMultiplier;
         FirstLot=FirstLot_4*lotMultiplier;
         typeLot = typeLot_4;
         maxVol = maxVol_4;
         curOffsetMultChange = curOffsetMultChange_4;
         kefForBothClose = kefForBothClose_4*lotMultiplier;
         kefForAllPos = kefForAllPos_4;
         kefForMaxVolume = kefForMaxVolume_4;
         useAllProfitForClose = useAllProfitForClose_4;
         typeDir=typeDir_4;
         takeProfit=takeProfit_4*lotMultiplier;
         takeProfit1step=takeProfit1step_4*lotMultiplier;
         takeProfit2step=takeProfit2step_4*lotMultiplier;
         takeProfit4step=takeProfit4step_4*lotMultiplier;
         gridStepCountClose=gridStepCountClose_4;
         BARS_enabled=BARS_enabled_4;
         BARS_Timeframe=BARS_Timeframe_4;
         BARS_RSI_Period=BARS_RSI_Period_4;
         SERIES_count=SERIES_count_4;
         SERIES_type=SERIES_type_4;
         SERIES_Timeframe=SERIES_Timeframe_4;
         SERIES_percent = SERIES_percent_4;
         SERIES_use_point = SERIES_use_point_4;
         MA_Timeframe=MA_Timeframe_4;
         MA_PERIOD=MA_PERIOD_4;
         MA_TYPE=MA_TYPE_4;
         MA_invert=MA_invert_4;
         MA_across_price=MA_across_price_4;
         MA_across_always=MA_across_always_4;
         RSI_Timeframe=RSI_Timeframe_4;
         RSI_PERIOD=RSI_PERIOD_4;
         RSI_enter_return= RSI_enter_return_4;
         RSI_only_first= RSI_only_first_4;
         RSI_invert= RSI_invert_4;
         BB_Timeframe=BB_Timeframe_4;
         BB_PERIOD=BB_PERIOD_4;
         BB_SHIFT=BB_SHIFT_4;
         BB_DEVIATION=BB_DEVIATION_4;
         BB_only_first= BB_only_first_4;
         BB_INVERT=BB_INVERT_4;
         S_Timeframe=S_Timeframe_4;
         SK_PERIOD=SK_PERIOD_4;
         SD_PERIOD=SD_PERIOD_4;
         S_Slowing=S_Slowing_4;
         S_INVERT=S_INVERT_4;
         TSL_points=TSL_points_4;
         TTL_points=TTL_points_4;
         TSL_points_KEF=TSL_points_KEF_4;
         TTL_points_KEF=TTL_points_KEF_4;
         change_tf_M15_after=change_tf_M15_after_4;
         change_tf_M30_after=change_tf_M30_after_4;
         change_tf_H1_after=change_tf_H1_after_4;
         change_tf_H4_after=change_tf_H4_after_4;
         change_tf_D1_after=change_tf_D1_after_4;
      
         startTimer(symName_4, 3, tmpLot, gridStep_4);
      }
      if( useSym_5 && StringLen(symName_5) ){
         takeProfitVal = takeProfitVal_5;
         gridStepVal = gridStepVal_5;
         tmpLot = Lot_5*lotMultiplier;
         FirstLot=FirstLot_5*lotMultiplier;
         typeLot = typeLot_5;
         maxVol = maxVol_5;
         curOffsetMultChange = curOffsetMultChange_5;
         kefForBothClose = kefForBothClose_5*lotMultiplier;
         kefForAllPos = kefForAllPos_5;
         kefForMaxVolume = kefForMaxVolume_5;
         useAllProfitForClose = useAllProfitForClose_5;
         typeDir=typeDir_5;
         takeProfit=takeProfit_5*lotMultiplier;
         takeProfit1step=takeProfit1step_5*lotMultiplier;
         takeProfit2step=takeProfit2step_5*lotMultiplier;
         takeProfit4step=takeProfit4step_5*lotMultiplier;
         gridStepCountClose=gridStepCountClose_5;
         BARS_enabled=BARS_enabled_5;
         BARS_Timeframe=BARS_Timeframe_5;
         BARS_RSI_Period=BARS_RSI_Period_5;
         SERIES_count=SERIES_count_5;
         SERIES_type=SERIES_type_5;
         SERIES_Timeframe=SERIES_Timeframe_5;
         SERIES_percent = SERIES_percent_5;
         SERIES_use_point = SERIES_use_point_5;
         MA_Timeframe=MA_Timeframe_5;
         MA_PERIOD=MA_PERIOD_5;
         MA_TYPE=MA_TYPE_5;
         MA_invert=MA_invert_5;
         MA_across_price=MA_across_price_5;
         MA_across_always=MA_across_always_5;
         RSI_Timeframe=RSI_Timeframe_5;
         RSI_PERIOD=RSI_PERIOD_5;
         RSI_enter_return= RSI_enter_return_5;
         RSI_only_first= RSI_only_first_5;
         RSI_invert= RSI_invert_5;
         BB_Timeframe=BB_Timeframe_5;
         BB_PERIOD=BB_PERIOD_5;
         BB_SHIFT=BB_SHIFT_5;
         BB_DEVIATION=BB_DEVIATION_5;
         BB_only_first= BB_only_first_5;
         BB_INVERT=BB_INVERT_5;
         S_Timeframe=S_Timeframe_5;
         SK_PERIOD=SK_PERIOD_5;
         SD_PERIOD=SD_PERIOD_5;
         S_Slowing=S_Slowing_5;
         S_INVERT=S_INVERT_5;
         TSL_points=TSL_points_5;
         TTL_points=TTL_points_5;
         TSL_points_KEF=TSL_points_KEF_5;
         TTL_points_KEF=TTL_points_KEF_5;
         change_tf_M15_after=change_tf_M15_after_5;
         change_tf_M30_after=change_tf_M30_after_5;
         change_tf_H1_after=change_tf_H1_after_5;
         change_tf_H4_after=change_tf_H4_after_5;
         change_tf_D1_after=change_tf_D1_after_5;
      
         startTimer(symName_5, 4, tmpLot, gridStep_5);
      }
      if( useSym_6 && StringLen(symName_6) ){
         takeProfitVal = takeProfitVal_6;
         gridStepVal = gridStepVal_6;
         tmpLot = Lot_6*lotMultiplier;
         FirstLot=FirstLot_6*lotMultiplier;
         typeLot = typeLot_6;
         maxVol = maxVol_6;
         curOffsetMultChange = curOffsetMultChange_6;
         kefForBothClose = kefForBothClose_6*lotMultiplier;
         kefForAllPos = kefForAllPos_6;
         kefForMaxVolume = kefForMaxVolume_6;
         useAllProfitForClose = useAllProfitForClose_6;
         typeDir=typeDir_6;
         takeProfit=takeProfit_6*lotMultiplier;
         takeProfit1step=takeProfit1step_6*lotMultiplier;
         takeProfit2step=takeProfit2step_6*lotMultiplier;
         takeProfit4step=takeProfit4step_6*lotMultiplier;
         gridStepCountClose=gridStepCountClose_6;
         BARS_enabled=BARS_enabled_6;
         BARS_Timeframe=BARS_Timeframe_6;
         BARS_RSI_Period=BARS_RSI_Period_6;
         SERIES_count=SERIES_count_6;
         SERIES_type=SERIES_type_6;
         SERIES_Timeframe=SERIES_Timeframe_6;
         SERIES_percent = SERIES_percent_6;
         SERIES_use_point = SERIES_use_point_6;
         MA_Timeframe=MA_Timeframe_6;
         MA_PERIOD=MA_PERIOD_6;
         MA_TYPE=MA_TYPE_6;
         MA_invert=MA_invert_6;
         MA_across_price=MA_across_price_6;
         MA_across_always=MA_across_always_6;
         RSI_Timeframe=RSI_Timeframe_6;
         RSI_PERIOD=RSI_PERIOD_6;
         RSI_enter_return= RSI_enter_return_6;
         RSI_only_first= RSI_only_first_6;
         RSI_invert= RSI_invert_6;
         BB_Timeframe=BB_Timeframe_6;
         BB_PERIOD=BB_PERIOD_6;
         BB_SHIFT=BB_SHIFT_6;
         BB_DEVIATION=BB_DEVIATION_6;
         BB_only_first= BB_only_first_6;
         BB_INVERT=BB_INVERT_6;
         S_Timeframe=S_Timeframe_6;
         SK_PERIOD=SK_PERIOD_6;
         SD_PERIOD=SD_PERIOD_6;
         S_Slowing=S_Slowing_6;
         S_INVERT=S_INVERT_6;
         TSL_points=TSL_points_6;
         TTL_points=TTL_points_6;
         TSL_points_KEF=TSL_points_KEF_6;
         TTL_points_KEF=TTL_points_KEF_6;
         change_tf_M15_after=change_tf_M15_after_6;
         change_tf_M30_after=change_tf_M30_after_6;
         change_tf_H1_after=change_tf_H1_after_6;
         change_tf_H4_after=change_tf_H4_after_6;
         change_tf_D1_after=change_tf_D1_after_6;
      
         startTimer(symName_6, 5, tmpLot, gridStep_6);
      }
      if( useSym_7 && StringLen(symName_7) ){
         takeProfitVal = takeProfitVal_7;
         gridStepVal = gridStepVal_7;
         tmpLot = Lot_7*lotMultiplier;
         FirstLot=FirstLot_7*lotMultiplier;
         typeLot = typeLot_7;
         maxVol = maxVol_7;
         curOffsetMultChange = curOffsetMultChange_7;
         kefForBothClose = kefForBothClose_7*lotMultiplier;
         kefForAllPos = kefForAllPos_7;
         kefForMaxVolume = kefForMaxVolume_7;
         useAllProfitForClose = useAllProfitForClose_7;
         typeDir=typeDir_7;
         takeProfit=takeProfit_7*lotMultiplier;
         takeProfit1step=takeProfit1step_7*lotMultiplier;
         takeProfit2step=takeProfit2step_7*lotMultiplier;
         takeProfit4step=takeProfit4step_7*lotMultiplier;
         gridStepCountClose=gridStepCountClose_7;
         BARS_enabled=BARS_enabled_7;
         BARS_Timeframe=BARS_Timeframe_7;
         BARS_RSI_Period=BARS_RSI_Period_7;
         SERIES_count=SERIES_count_7;
         SERIES_type=SERIES_type_7;
         SERIES_Timeframe=SERIES_Timeframe_7;
         SERIES_percent = SERIES_percent_7;
         SERIES_use_point = SERIES_use_point_7;
         MA_Timeframe=MA_Timeframe_7;
         MA_PERIOD=MA_PERIOD_7;
         MA_TYPE=MA_TYPE_7;
         MA_invert=MA_invert_7;
         MA_across_price=MA_across_price_7;
         MA_across_always=MA_across_always_7;
         RSI_Timeframe=RSI_Timeframe_7;
         RSI_PERIOD=RSI_PERIOD_7;
         RSI_enter_return= RSI_enter_return_7;
         RSI_only_first= RSI_only_first_7;
         RSI_invert= RSI_invert_7;
         BB_Timeframe=BB_Timeframe_7;
         BB_PERIOD=BB_PERIOD_7;
         BB_SHIFT=BB_SHIFT_7;
         BB_DEVIATION=BB_DEVIATION_7;
         BB_only_first= BB_only_first_7;
         BB_INVERT=BB_INVERT_7;
         S_Timeframe=S_Timeframe_7;
         SK_PERIOD=SK_PERIOD_7;
         SD_PERIOD=SD_PERIOD_7;
         S_Slowing=S_Slowing_7;
         S_INVERT=S_INVERT_7;
         TSL_points=TSL_points_7;
         TTL_points=TTL_points_7;
         TSL_points_KEF=TSL_points_KEF_7;
         TTL_points_KEF=TTL_points_KEF_7;
         change_tf_M15_after=change_tf_M15_after_7;
         change_tf_M30_after=change_tf_M30_after_7;
         change_tf_H1_after=change_tf_H1_after_7;
         change_tf_H4_after=change_tf_H4_after_7;
         change_tf_D1_after=change_tf_D1_after_7;
      
         startTimer(symName_7, 6, tmpLot, gridStep_7);
      }
      if( useSym_8 && StringLen(symName_8) ){
         takeProfitVal = takeProfitVal_8;
         gridStepVal = gridStepVal_8;
         tmpLot = Lot_8*lotMultiplier;
         FirstLot=FirstLot_8*lotMultiplier;
         typeLot = typeLot_8;
         maxVol = maxVol_8;
         curOffsetMultChange = curOffsetMultChange_8;
         kefForBothClose = kefForBothClose_8*lotMultiplier;
         kefForAllPos = kefForAllPos_8;
         kefForMaxVolume = kefForMaxVolume_8;
         useAllProfitForClose = useAllProfitForClose_8;
         typeDir=typeDir_8;
         takeProfit=takeProfit_8*lotMultiplier;
         takeProfit1step=takeProfit1step_8*lotMultiplier;
         takeProfit2step=takeProfit2step_8*lotMultiplier;
         takeProfit4step=takeProfit4step_8*lotMultiplier;
         gridStepCountClose=gridStepCountClose_8;
         BARS_enabled=BARS_enabled_8;
         BARS_Timeframe=BARS_Timeframe_8;
         BARS_RSI_Period=BARS_RSI_Period_8;
         SERIES_count=SERIES_count_8;
         SERIES_type=SERIES_type_8;
         SERIES_Timeframe=SERIES_Timeframe_8;
         SERIES_percent = SERIES_percent_8;
         SERIES_use_point = SERIES_use_point_8;
         MA_Timeframe=MA_Timeframe_8;
         MA_PERIOD=MA_PERIOD_8;
         MA_TYPE=MA_TYPE_8;
         MA_invert=MA_invert_8;
         MA_across_price=MA_across_price_8;
         MA_across_always=MA_across_always_8;
         RSI_Timeframe=RSI_Timeframe_8;
         RSI_PERIOD=RSI_PERIOD_8;
         RSI_enter_return= RSI_enter_return_8;
         RSI_only_first= RSI_only_first_8;
         RSI_invert= RSI_invert_8;
         BB_Timeframe=BB_Timeframe_8;
         BB_PERIOD=BB_PERIOD_8;
         BB_SHIFT=BB_SHIFT_8;
         BB_DEVIATION=BB_DEVIATION_8;
         BB_only_first= BB_only_first_8;
         BB_INVERT=BB_INVERT_8;
         S_Timeframe=S_Timeframe_8;
         SK_PERIOD=SK_PERIOD_8;
         SD_PERIOD=SD_PERIOD_8;
         S_Slowing=S_Slowing_8;
         S_INVERT=S_INVERT_8;
         TSL_points=TSL_points_8;
         TTL_points=TTL_points_8;
         TSL_points_KEF=TSL_points_KEF_8;
         TTL_points_KEF=TTL_points_KEF_8;
         change_tf_M15_after=change_tf_M15_after_8;
         change_tf_M30_after=change_tf_M30_after_8;
         change_tf_H1_after=change_tf_H1_after_8;
         change_tf_H4_after=change_tf_H4_after_8;
         change_tf_D1_after=change_tf_D1_after_8;
      
         startTimer(symName_8, 7, tmpLot, gridStep_8);
      }
      if( useSym_9 && StringLen(symName_9) ){
         takeProfitVal = takeProfitVal_9;
         gridStepVal = gridStepVal_9;
         tmpLot = Lot_9*lotMultiplier;
         FirstLot=FirstLot_9*lotMultiplier;
         typeLot = typeLot_9;
         maxVol = maxVol_9;
         curOffsetMultChange = curOffsetMultChange_9;
         kefForBothClose = kefForBothClose_9*lotMultiplier;
         kefForAllPos = kefForAllPos_9;
         kefForMaxVolume = kefForMaxVolume_9;
         useAllProfitForClose = useAllProfitForClose_9;
         typeDir=typeDir_9;
         takeProfit=takeProfit_9*lotMultiplier;
         takeProfit1step=takeProfit1step_9*lotMultiplier;
         takeProfit2step=takeProfit2step_9*lotMultiplier;
         takeProfit4step=takeProfit4step_9*lotMultiplier;
         gridStepCountClose=gridStepCountClose_9;
         BARS_enabled=BARS_enabled_9;
         BARS_Timeframe=BARS_Timeframe_9;
         BARS_RSI_Period=BARS_RSI_Period_9;
         SERIES_count=SERIES_count_9;
         SERIES_type=SERIES_type_9;
         SERIES_Timeframe=SERIES_Timeframe_9;
         SERIES_percent = SERIES_percent_9;
         SERIES_use_point = SERIES_use_point_9;
         MA_Timeframe=MA_Timeframe_9;
         MA_PERIOD=MA_PERIOD_9;
         MA_TYPE=MA_TYPE_9;
         MA_invert=MA_invert_9;
         MA_across_price=MA_across_price_9;
         MA_across_always=MA_across_always_9;
         RSI_Timeframe=RSI_Timeframe_9;
         RSI_PERIOD=RSI_PERIOD_9;
         RSI_enter_return= RSI_enter_return_9;
         RSI_only_first= RSI_only_first_9;
         RSI_invert= RSI_invert_9;
         BB_Timeframe=BB_Timeframe_9;
         BB_PERIOD=BB_PERIOD_9;
         BB_SHIFT=BB_SHIFT_9;
         BB_DEVIATION=BB_DEVIATION_9;
         BB_only_first= BB_only_first_9;
         BB_INVERT=BB_INVERT_9;
         S_Timeframe=S_Timeframe_9;
         SK_PERIOD=SK_PERIOD_9;
         SD_PERIOD=SD_PERIOD_9;
         S_Slowing=S_Slowing_9;
         S_INVERT=S_INVERT_9;
         TSL_points=TSL_points_9;
         TTL_points=TTL_points_9;
         TSL_points_KEF=TSL_points_KEF_9;
         TTL_points_KEF=TTL_points_KEF_9;
         change_tf_M15_after=change_tf_M15_after_9;
         change_tf_M30_after=change_tf_M30_after_9;
         change_tf_H1_after=change_tf_H1_after_9;
         change_tf_H4_after=change_tf_H4_after_9;
         change_tf_D1_after=change_tf_D1_after_9;
      
         startTimer(symName_9, 8, tmpLot, gridStep_9);
      }
      if( useSym_10 && StringLen(symName_10) ){
         takeProfitVal = takeProfitVal_10;
         gridStepVal = gridStepVal_10;
         tmpLot = Lot_10*lotMultiplier;
         FirstLot=FirstLot_10*lotMultiplier;
         typeLot = typeLot_10;
         maxVol = maxVol_10;
         curOffsetMultChange = curOffsetMultChange_10;
         kefForBothClose = kefForBothClose_10*lotMultiplier;
         kefForAllPos = kefForAllPos_10;
         kefForMaxVolume = kefForMaxVolume_10;
         useAllProfitForClose = useAllProfitForClose_10;
         typeDir=typeDir_10;
         takeProfit=takeProfit_10*lotMultiplier;
         takeProfit1step=takeProfit1step_10*lotMultiplier;
         takeProfit2step=takeProfit2step_10*lotMultiplier;
         takeProfit4step=takeProfit4step_10*lotMultiplier;
         gridStepCountClose=gridStepCountClose_10;
         BARS_enabled=BARS_enabled_10;
         BARS_Timeframe=BARS_Timeframe_10;
         BARS_RSI_Period=BARS_RSI_Period_10;
         SERIES_count=SERIES_count_10;
         SERIES_type=SERIES_type_10;
         SERIES_Timeframe=SERIES_Timeframe_10;
         SERIES_percent = SERIES_percent_10;
         SERIES_use_point = SERIES_use_point_10;
         MA_Timeframe=MA_Timeframe_10;
         MA_PERIOD=MA_PERIOD_10;
         MA_TYPE=MA_TYPE_10;
         MA_invert=MA_invert_10;
         MA_across_price=MA_across_price_10;
         MA_across_always=MA_across_always_10;
         RSI_Timeframe=RSI_Timeframe_10;
         RSI_PERIOD=RSI_PERIOD_10;
         RSI_enter_return= RSI_enter_return_10;
         RSI_only_first= RSI_only_first_10;
         RSI_invert= RSI_invert_10;
         BB_Timeframe=BB_Timeframe_10;
         BB_PERIOD=BB_PERIOD_10;
         BB_SHIFT=BB_SHIFT_10;
         BB_DEVIATION=BB_DEVIATION_10;
         BB_only_first= BB_only_first_10;
         BB_INVERT=BB_INVERT_10;
         S_Timeframe=S_Timeframe_10;
         SK_PERIOD=SK_PERIOD_10;
         SD_PERIOD=SD_PERIOD_10;
         S_Slowing=S_Slowing_10;
         S_INVERT=S_INVERT_10;
         TSL_points=TSL_points_10;
         TTL_points=TTL_points_10;
         TSL_points_KEF=TSL_points_KEF_10;
         TTL_points_KEF=TTL_points_KEF_10;
         change_tf_M15_after=change_tf_M15_after_10;
         change_tf_M30_after=change_tf_M30_after_10;
         change_tf_H1_after=change_tf_H1_after_10;
         change_tf_H4_after=change_tf_H4_after_10;
         change_tf_D1_after=change_tf_D1_after_10;
      
         startTimer(symName_10, 9, tmpLot, gridStep_10);
      }
      if( useSym_11 && StringLen(symName_11) ){
         takeProfitVal = takeProfitVal_11;
         gridStepVal = gridStepVal_11;
         tmpLot = Lot_11*lotMultiplier;
         FirstLot=FirstLot_11*lotMultiplier;
         typeLot = typeLot_11;
         maxVol = maxVol_11;
         curOffsetMultChange = curOffsetMultChange_11;
         kefForBothClose = kefForBothClose_11*lotMultiplier;
         kefForAllPos = kefForAllPos_11;
         kefForMaxVolume = kefForMaxVolume_11;
         useAllProfitForClose = useAllProfitForClose_11;
         typeDir=typeDir_11;
         takeProfit=takeProfit_11*lotMultiplier;
         takeProfit1step=takeProfit1step_11*lotMultiplier;
         takeProfit2step=takeProfit2step_11*lotMultiplier;
         takeProfit4step=takeProfit4step_11*lotMultiplier;
         gridStepCountClose=gridStepCountClose_11;
         BARS_enabled=BARS_enabled_11;
         BARS_Timeframe=BARS_Timeframe_11;
         BARS_RSI_Period=BARS_RSI_Period_11;
         SERIES_count=SERIES_count_11;
         SERIES_type=SERIES_type_11;
         SERIES_Timeframe=SERIES_Timeframe_11;
         SERIES_percent = SERIES_percent_11;
         SERIES_use_point = SERIES_use_point_11;
         MA_Timeframe=MA_Timeframe_11;
         MA_PERIOD=MA_PERIOD_11;
         MA_TYPE=MA_TYPE_11;
         MA_invert=MA_invert_11;
         MA_across_price=MA_across_price_11;
         MA_across_always=MA_across_always_11;
         RSI_Timeframe=RSI_Timeframe_11;
         RSI_PERIOD=RSI_PERIOD_11;
         RSI_enter_return= RSI_enter_return_11;
         RSI_only_first= RSI_only_first_11;
         RSI_invert= RSI_invert_11;
         BB_Timeframe=BB_Timeframe_11;
         BB_PERIOD=BB_PERIOD_11;
         BB_SHIFT=BB_SHIFT_11;
         BB_DEVIATION=BB_DEVIATION_11;
         BB_only_first= BB_only_first_11;
         BB_INVERT=BB_INVERT_11;
         S_Timeframe=S_Timeframe_11;
         SK_PERIOD=SK_PERIOD_11;
         SD_PERIOD=SD_PERIOD_11;
         S_Slowing=S_Slowing_11;
         S_INVERT=S_INVERT_11;
         TSL_points=TSL_points_11;
         TTL_points=TTL_points_11;
         TSL_points_KEF=TSL_points_KEF_11;
         TTL_points_KEF=TTL_points_KEF_11;
         change_tf_M15_after=change_tf_M15_after_11;
         change_tf_M30_after=change_tf_M30_after_11;
         change_tf_H1_after=change_tf_H1_after_11;
         change_tf_H4_after=change_tf_H4_after_11;
         change_tf_D1_after=change_tf_D1_after_11;
      
         startTimer(symName_11, 10, tmpLot, gridStep_11);
      }
   }
}
void checkOtherKEFs(int magic, string symbol, double Lt) {
   double tmpMaxProfit = maxProfit;
   ulong tmpMaxProfitTicket = maxProfitTicket;
   double tmpMaxProfitVol = maxProfitVol;
   
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if(PositionGetSymbol(ti)!=symbol) continue;
         if(magic>0 && PositionGetInteger(POSITION_MAGIC)!=magic) continue;
         
         double curProfit = 0;
         curProfit+=PositionGetDouble(POSITION_PROFIT);
         curProfit+=PositionGetDouble(POSITION_SWAP);
         
         if ( curProfit < 0 ) {
            
            if (cancelKefAboutLastStep > 0) {
               // for sell
               if ( PositionGetDouble(POSITION_PRICE_OPEN) < maxPriceLevel ) {
                  if (maxPriceLevel - cancelKefAboutLastStep*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) < PositionGetDouble(POSITION_PRICE_OPEN) ) {
                     continue;
                  }
               } else if ( PositionGetDouble(POSITION_PRICE_OPEN) > minPriceLevel ) {
                  // for buy
                  if ( minPriceLevel + cancelKefAboutLastStep*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) > PositionGetDouble(POSITION_PRICE_OPEN) ) {
                     continue;
                  }
               }
            }
            
            double minLoss = PositionGetDouble(POSITION_VOLUME) / Lt;
            if (minLoss>0) {
               if ( kefForMaxVolume ) { } else {
                  if (minLoss>1) {
                     curProfit = curProfit/minLoss;
                  }
               }
               
               double resSum = tmpMaxProfit + curProfit;
               if ( resSum > kefForBothClose ) {
                  if ( kefForMaxVolume ) {
                     if (Trade.PositionClosePartial(PositionGetInteger(POSITION_TICKET), PositionGetDouble(POSITION_VOLUME))) {
                        Trade.PositionClose(tmpMaxProfitTicket);
                     }
                  } else {
                     if (Trade.PositionClosePartial(PositionGetInteger(POSITION_TICKET), Lt)) {
                        Trade.PositionClose(tmpMaxProfitTicket);
                     }
                  }
                     
                  cur_take_prices[hIndex] = 0;
                  KEF_cur_take_prices[hIndex] = 0;
                  lastProfit[hIndex] = 0;
                  
                  break;
               }
            }
         }
         
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue;
            if( OrderType()==OP_BUY || OrderType()==OP_SELL ){}else{ continue; }
            if(OrderSymbol()!=symbol) continue;
            if(magic>0 && OrderMagicNumber()!=magic) continue;
            
            double curProfit = 0;
            curProfit+=OrderCommission();
            curProfit+=OrderProfit();
            curProfit+=OrderSwap();
         
            if ( curProfit < 0 ) {
            
               if (cancelKefAboutLastStep > 0) {
                  // for sell
                  if ( OrderOpenPrice() < maxPriceLevel ) {
                     if (maxPriceLevel - cancelKefAboutLastStep*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) < OrderOpenPrice() ) {
                        continue;
                     }
                  } else if ( OrderOpenPrice() > minPriceLevel ) {
                     // for buy
                     if ( minPriceLevel + cancelKefAboutLastStep*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) > OrderOpenPrice() ) {
                        continue;
                     }
                  }
               }
               
               double minLoss = OrderLots() / Lt;
               if (minLoss>0) {
                  if ( kefForMaxVolume ) { } else {
                     if (minLoss>1) {
                        curProfit = curProfit/minLoss;
                     }
                  }
                  
                  double resSum = tmpMaxProfit + curProfit;
                  if ( resSum > kefForBothClose ) {
                     double curPrice = lastme.bid;
                     if (ShortPos[hIndex] > 0) {
                        curPrice = lastme.ask;
                     }
                     if ( kefForMaxVolume ) {
                        if (OrderClose((int) OrderTicket(), OrderLots(), curPrice, 100)) {
                           if (OrderClose((int) tmpMaxProfitTicket, tmpMaxProfitVol, curPrice, 100)){}
                        }
                     } else {
                        if (OrderClose((int) OrderTicket(), Lt, curPrice, 100)) {
                           if (OrderClose((int) tmpMaxProfitTicket, tmpMaxProfitVol, curPrice, 100)){}
                        }
                     }
                        
                     cur_take_prices[hIndex] = 0;
                     KEF_cur_take_prices[hIndex] = 0;
                     lastProfit[hIndex] = 0;
                     
                     break;
                  }
               }            
            
            }


         }
      }
   #endif 

}
void startCheckPositions(int magic, string symbol, double Lt){
   if (AccountInfoDouble(ACCOUNT_MARGIN) > maxCntrMarga) {
      maxCntrMarga = AccountInfoDouble(ACCOUNT_MARGIN);
   }
   if ( LongPos[hIndex] > 0 ) {
      if (LongPos[hIndex] > maxCntrSteps) {
         maxCntrSteps = LongPos[hIndex];
      }
   } else if ( ShortPos[hIndex] > 0 ) {
      if (ShortPos[hIndex] > maxCntrSteps) {
         maxCntrSteps = ShortPos[hIndex];
      }
   }

      if( (LongPos[hIndex]==0 && ShortPos[hIndex] == 0) || (RSI_PERIOD>0 && ( !RSI_only_first || (LongPos[hIndex]==0 && ShortPos[hIndex] == 0) ) ) || (SK_PERIOD>0 && LongPos[hIndex]==0 && ShortPos[hIndex] == 0 ) || ( BB_PERIOD>0 && ( !BB_only_first || (LongPos[hIndex]==0 && ShortPos[hIndex] == 0) ) ) || (MA_PERIOD>0 && MA_across_always) ){
      
         if( mSpread>0 && MathAbs(lastme.bid-lastme.ask) > mSpread*SymbolInfoDouble(symbol, SYMBOL_POINT) ){
            return;
         }
         checkDirection(symbol);
      }
      
      if( LongPos[hIndex] + ShortPos[hIndex] == 0 ){
         if( EnterHour >= 0 && EnterHour < 24 ){
            TimeCurrent(curT);
            if ( curT.hour!=EnterHour ) {
               return;
            }
            if(EnterMin>0 && curT.min!=EnterMin){
               return;
            }
         }
         
         // максимальное количество символов уже открыто
         if (maxSymbols > 0 && maxSymbols <= countCurSymb) {
            return;
         }
      
         double tmpMyLot = curLot;
         if (FirstLot > 0) {
            tmpMyLot = FirstLot;
         }
         if(!noLong){
            if ( maxHighPoints > 0 ) {
               MqlRates rates[];
               ArraySetAsSeries(rates, true);
               if( CopyRates( symbol, PERIOD_MN1, 0, 3, rates)==3 ){
                  double tmpMaxHigh = rates[0].high;
                  if (rates[1].high > tmpMaxHigh) {
                     tmpMaxHigh = rates[1].high;
                  }
                  if (rates[2].high > tmpMaxHigh) {
                     tmpMaxHigh = rates[2].high;
                  }
                  if ( tmpMaxHigh > lastme.bid &&  tmpMaxHigh - lastme.bid < maxHighPoints * SymbolInfoDouble(symbol, SYMBOL_POINT) ) {
                     return;
                  }
               }
            }
            
            if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, tmpMyLot, 0, "1", symbol, magic)){
            }
         }else if(!noShort){
            if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, tmpMyLot, 0, "1", symbol, magic)){
            }
         }
      }else{
      
         double tmpMaxProfit = maxProfit;
         if ( useAllProfitForClose ) {
            tmpMaxProfit = curProfitForAllPos;
         }
         ulong tmpMaxProfitTicket = maxProfitTicket;
         double tmpMaxProfitVol = maxProfitVol;
         uint countPosForKEF = 2;
         
         switch (workType) {
            case WORK_GRIDER:
            case WORK_GRIDER_BOTH:
               
               // close defense
               if (defenseProfit > 0 && defCloseLot > 0 && maxLossIs > 0 ) {
                  double minLoss = maxLossIs / defCloseLot;
                  double tmpMinVolProfit = maxLossProfit;
                  if (minLoss>0) {
                     if (minLoss>1) {
                        tmpMinVolProfit = tmpMinVolProfit/minLoss;
                     }
                     double resSum = defenseProfit + tmpMinVolProfit;
                     if (resSum >= kefForBothClose) {
                        #ifdef __MQL5__ 
                           if ( maxLossIs == defCloseLot ) {
                              Trade.PositionClose(maxLossTicket);
                           } else {
                              Trade.PositionClosePartial(maxLossTicket, defCloseLot);
                           }
                           Trade.PositionClose(defenseTicket);
                           if (ShortPos[hIndex] > 0) {
                              if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, defCloseLot, 0, "777", symbol, magic)){
                              }
                           } else if (LongPos[hIndex] > 0) {
                              if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, defCloseLot, 0, "777", symbol, magic)){
                              }
                           }
                           defenseExists = false;
                           
                        #else
                           double curPrice = lastme.bid;
                           if (ShortPos[hIndex] > 0) {
                              curPrice = lastme.ask;
                           }
                           if (OrderClose((int) maxLossTicket, defCloseLot, curPrice, 100)){}
                           if (OrderClose((int) defenseTicket, defenseVol, curPrice, 100)) {}
                           defenseExists = false;
                        #endif 
                         
                     }
                  }
               }
               
               
               if (defenseExists == true) {
                  // check defense lot kefs
                  if (defStartLotKefs > 0) {
                     if (ShortVol[hIndex] > 0 ) {
                        double tmpSV = defenseVol/defStartLotKefs;
                        if (ShortVol[hIndex] < tmpSV ) {
                           if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, tmpSV-ShortVol[hIndex] , 0, "777", symbol, magic)){
                           }
                        }
                     } else if (LongVol[hIndex] > 0 ) {
                        double tmpSV = defenseVol/defStartLotKefs;
                        if (LongVol[hIndex] < tmpSV) {
                           if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, tmpSV-LongVol[hIndex] , 0, "777", symbol, magic)){
                           }
                        }
                     }
                  }
                  // close one direction pos with profit
                  if (ShortVol[hIndex] > 0 && ShortPos[hIndex] > 1 ){
                     if (checkTakeProfit(symbol, MY_SELL, magic, kefForBothClose)) {
                        closeAllPos(symbol, MY_SELL, magic);
                        if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, ShortVol[hIndex] , 0, "1", symbol, magic)){
                        }
                     }
                  } else if (LongVol[hIndex] > 0 && LongPos[hIndex] > 1 ){
                     if (checkTakeProfit(symbol, MY_BUY, magic, kefForBothClose)) {
                        closeAllPos(symbol, MY_BUY, magic);
                        if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, LongVol[hIndex] , 0, "1", symbol, magic)){
                        }
                     }
                  }
               
               } else {
                  if (FirstLot > 0) {
                     countPosForKEF = 1;
                  }
                  if ( (LongPos[hIndex] + ShortPos[hIndex]) > countPosForKEF && minVolIs > 0 && tmpMaxProfitVol > 0 && tmpMaxProfit > 0 ) {
                     bool KEF_need_close=false;
                     
                     double minLoss = minVolIs / Lt;
                     if (minLoss>0) {
                        if ( kefForMaxVolume ) { } else {
                           if (minLoss>1) {
                              minVolProfit = minVolProfit/minLoss;
                           }
                        }
                        
                        double resSum = tmpMaxProfit + minVolProfit;
                        if (KEF_cur_take_prices[hIndex] > 0) {
                           if( resSum - KEF_cur_take_prices[hIndex] >= TTL_points_KEF ){
                              KEF_cur_take_prices[hIndex] = resSum;
                           }else if( KEF_cur_take_prices[hIndex] - resSum > TSL_points_KEF ){
                              KEF_need_close=true;
                           }
                        } else if ( kefForBothClose > 0 ) {
                           if ( resSum > kefForBothClose ) {
                              KEF_need_close=true;
                              if( TSL_points_KEF>0 && TTL_points_KEF>0 ){
                                 KEF_need_close=false;
                                 if (KEF_cur_take_prices[hIndex] > 0) {
                                    if( resSum - KEF_cur_take_prices[hIndex] >= TTL_points_KEF ){
                                       KEF_cur_take_prices[hIndex] = resSum;
                                    }else if( KEF_cur_take_prices[hIndex] - resSum > TSL_points ){
                                       KEF_need_close=true;
                                    }
                                 } else {
                                    KEF_cur_take_prices[hIndex] = resSum;
                                 }
                              }
                           }
                        }
                        
                     }
                     // close on kefs
                     if (KEF_need_close == true) {
                        double curPrice = lastme.bid;
                        if (ShortPos[hIndex] > 0) {
                           curPrice = lastme.ask;
                        }
                        #ifdef __MQL5__ 
                           if ( kefForMaxVolume ) {
                              Trade.PositionClose(minVolTicket);
                           } else {
                              Trade.PositionClosePartial(minVolTicket, Lt);
                           }
                           if ( useAllProfitForClose ) {
                              closeAllProfitPos(symbol, MY_ALLPOS, magic);
                           } else {
                              Trade.PositionClose(tmpMaxProfitTicket);
                           }
                        #else
                           if ( kefForMaxVolume ) {
                              if (OrderClose((int) minVolTicket, minVolIs, curPrice, 100)){}
                           } else {
                              if (OrderClose((int) minVolTicket, Lt, curPrice, 100)){}
                           }
                           if ( useAllProfitForClose ) {
                              closeAllProfitPos(symbol, MY_ALLPOS, magic);
                           } else {
                              if (OrderClose((int) tmpMaxProfitTicket, tmpMaxProfitVol, curPrice, 100)) {}
                           }
                        #endif 
                        cur_take_prices[hIndex] = 0;
                        KEF_cur_take_prices[hIndex] = 0;
                        lastProfit[hIndex] = 0;
                        
                        if (assistExists) {
                           closeAssist(symbol, magic);
                        }
                     } else if ( KEF_cur_take_prices[hIndex] == 0 && kefForAllPos == true ) {
                        checkOtherKEFs(magic, symbol, Lt);
                     }
                  }               
               }

            
               
               if ( maxTotalPos > 0 && maxTotalPosCntr >= maxTotalPos ) {
                  return;
               }
               if ( maxTotalVol > 0 && maxTotalVolCntr >= maxTotalVol ) {
                  return;
               }
               
               if( cur_offset > 0 && (lastPosIs[hIndex] == MY_BUY || LongPos[hIndex]>0) && (LongPos[hIndex] > 1 || !defenseExists) ){
               
                  double addedLot = Lt;
                  switch(typeLot){
                     case MY_ARIFMET:
                        addedLot = Lt;
                        if ( useNewLotByCurOffset && minPriceLevel > lastme.bid && ( minPriceLevel - lastme.bid )/SymbolInfoDouble(symbol, SYMBOL_POINT) >= 2*cur_offset ) {
                           addedLot = Lt*( (int) ((( minPriceLevel - lastme.bid )/SymbolInfoDouble(symbol, SYMBOL_POINT))/cur_offset));
                        }
                        if (griderBothCmtSep == true) {
                           curLot=maxCmtNumBuy*Lt+addedLot;
                        } else {
                           curLot=maxVolIs+addedLot;
                        }
                        break;
                     case MY_GEOMET:
                        if(LongPos[hIndex]>1) curLot=Lt*(MathPow(2, LongPos[hIndex]-1));
                        break;
                     case MY_FIXED:
                        if (FirstLot > 0) {
                           curLot=FirstLot;
                        } else {
                           curLot=Lt;
                        }
                        break;
                  }
                  if (lot4xAfter > 0 && maxCmtNumBuy > (uint) lot4xAfter ) {
                     curLot *= 4;
                  }else if (lot3xAfter > 0 && maxCmtNumBuy > (uint) lot3xAfter ) {
                     curLot *= 3;
                  }else if (lot2xAfter > 0 && maxCmtNumBuy > (uint) lot2xAfter ) {
                     // for buy
/*
                     if ( lastme.bid > minPriceLevel ) {
                        if (lastme.bid - minPriceLevel > lot2xAfter*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) ) {
                           curLot *= 2;
                        }
                     }
*/
                     curLot *= 2;
                  }
                  
                  if ( maxVol > 0 && curLot > maxVol*Lt ) {
                     curLot = maxVol*Lt;
                  }
                  if( ( RSI_PERIOD>0 && !RSI_only_first ) || ( BB_PERIOD>0 && !BB_only_first ) || (MA_PERIOD>0 && MA_across_always) ){
                     if( !noLong ){
                        if( gridStepCountClose>0 ){
                           if ( maxCmtNumBuy < 1 && gridStepCountClose<=LongPos[hIndex] ) {
                              closeAllPos(symbol, MY_ALLPOS, magic);
                              return;
                           }
                           if ( maxCmtNumBuy > 0 && gridStepCountClose<=maxCmtNumBuy ) {
                              closeAllPos(symbol, MY_ALLPOS, magic);
                              return;
                           }
                        }
                        if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, curLot, 0, (string) (maxCmtNumBuy + 1), symbol, magic)){
                        }
                     }
                  }else if(LastLong-lastme.ask >= (cur_offset+curOffsetMultChange*(LongPos[hIndex]-1))*SymbolInfoDouble(symbol, SYMBOL_POINT) ){
                     if( gridStepCountClose>0 ){
                        if ( maxCmtNumBuy < 1 && gridStepCountClose<=LongPos[hIndex] ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                        if ( maxCmtNumBuy > 0 && gridStepCountClose<=maxCmtNumBuy ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                     }
                     if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, curLot, 0, (string) (maxCmtNumBuy + 1), symbol, magic)){
                     }
                  }
                  
                  if (workType == WORK_GRIDER_BOTH && ShortPos[hIndex] == 0 && LongPos[hIndex] > griderBothStartAfterStep ) {
                     // доп. ордер в сторону движения
                     if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, Lt, 0, "1", symbol, magic)){
                     }
                     return;
                  }
               }
               if( cur_offset > 0 && (lastPosIs[hIndex] == MY_SELL || ShortPos[hIndex]>0) && (ShortPos[hIndex] > 1 || !defenseExists) ){
                  switch(typeLot){
                     case MY_ARIFMET:
                        if (griderBothCmtSep == true) {
                           curLot=maxCmtNumBuy*Lt+Lt;
                        } else {
                           curLot=maxVolIs+Lt;
                        }
                        break;
                     case MY_GEOMET:
                        if(ShortPos[hIndex]>1) curLot=Lt*(MathPow(2, ShortPos[hIndex]-1));
                        break;
                     case MY_FIXED:
                        if (FirstLot > 0) {
                           curLot=FirstLot;
                        } else {
                           curLot=Lt;
                        }
                        break;
                  }
                  if (lot4xAfter > 0 && maxCmtNumShort > (uint) lot4xAfter ) {
                     curLot *= 4;
                  }else if (lot3xAfter > 0 && maxCmtNumShort > (uint) lot3xAfter ) {
                     curLot *= 3;
                  }else if (lot2xAfter > 0 && maxCmtNumShort > (uint) lot2xAfter ) {
                     // for buy
/*
                     if ( lastme.bid > minPriceLevel ) {
                        if (lastme.bid - minPriceLevel > lot2xAfter*cur_offset*SymbolInfoDouble(symbol, SYMBOL_POINT) ) {
                           curLot *= 2;
                        }
                     }
*/
                     curLot *= 2;
                  }
                  if ( maxVol > 0 && curLot > maxVol*Lt ) {
                     curLot = maxVol*Lt;
                  }
                  if( ( RSI_PERIOD>0 && !RSI_only_first ) || ( BB_PERIOD>0 && !BB_only_first ) || (MA_PERIOD>0 && MA_across_always) ){
                     if( !noShort ){
                        if( gridStepCountClose>0 ){
                           if ( maxCmtNumShort < 1 && gridStepCountClose<=ShortPos[hIndex] ) {
                              closeAllPos(symbol, MY_ALLPOS, magic);
                              return;
                           }
                           if ( maxCmtNumShort > 0 && gridStepCountClose<=maxCmtNumShort ) {
                              closeAllPos(symbol, MY_ALLPOS, magic);
                              return;
                           }
                        }
                        if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, curLot, 0, (string) (maxCmtNumShort + 1), symbol, magic)){
                        }
                     }
                  }else if(lastme.bid-LastShort >= (cur_offset+curOffsetMultChange*(ShortPos[hIndex]-1))*SymbolInfoDouble(symbol, SYMBOL_POINT) ){
                     if( gridStepCountClose>0 ){
                        if ( maxCmtNumShort < 1 && gridStepCountClose<=ShortPos[hIndex] ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                        if ( maxCmtNumShort > 0 && gridStepCountClose<=maxCmtNumShort ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                     }
                     if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, curLot, 0, (string) (maxCmtNumShort + 1), symbol, magic)){
                     }
                  }
                  
                  if (workType == WORK_GRIDER_BOTH && ShortPos[hIndex] > griderBothStartAfterStep && LongPos[hIndex] == 0 ) {
                     // доп. ордер в сторону движения
                     if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, Lt, 0, "1", symbol, magic)){
                     }
                  }
               }
         
/* --------- grider work end -------  */            
               break;
            case WORK_REVERSE:
               switch(typeLot){
                  case MY_ARIFMET:
                     curLot=maxVolIs+Lt;
                     break;
                  case MY_GEOMET:
                     curLot=maxVolIs*2;
                     break;
                  case MY_FIXED:
                     if (FirstLot > 0) {
                        curLot=FirstLot;
                     } else {
                        curLot=Lt;
                     }
                     break;
               }
               if ( maxVol > 0 && curLot > maxVol*Lt ) {
                  curLot = maxVol*Lt;
               }

               if( LongPos[hIndex]>0 ){
                  if(LastLong-lastme.ask >= (cur_offset+curOffsetMultChange*(maxCmtNumBuy-1))*SymbolInfoDouble(symbol, SYMBOL_POINT) ){
                     if( gridStepCountClose>0 ){
                        if ( maxCmtNumBuy > 0 && gridStepCountClose<=maxCmtNumBuy ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                     }
                     closeAllPos(symbol);
                     if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, curLot, 0, (string) (maxCmtNumBuy + 1), symbol, magic)){
                     }
                  }
               } else if(ShortPos[hIndex]>0 ){
                  if(lastme.bid-LastShort >= (cur_offset+curOffsetMultChange*(maxCmtNumShort-1))*SymbolInfoDouble(symbol, SYMBOL_POINT) ){
                     if( gridStepCountClose>0 ){
                        if ( maxCmtNumShort > 0 && gridStepCountClose<=maxCmtNumShort ) {
                           closeAllPos(symbol, MY_ALLPOS, magic);
                           return;
                        }
                     }
                     closeAllPos(symbol);
                     if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, curLot, 0, (string) (maxCmtNumShort + 1), symbol, magic)){
                     }
                  }
               }

/* --------- reverse work end ------- */            
               break;
         }
         
      }
}
void checkDirection(string symbol){
   noLong=false;
   noShort=false;

   if(MA_PERIOD>0){
      #ifdef __MQL5__ 
         switch(MA_TYPE){
            case MA_FIRST:
               if (allSymb_use) {
                  CopyBuffer(h_all, 0, 0, 1, buf0);
               } else {
                  CopyBuffer(h[hIndex], 0, 0, 1, buf0);
               }
               if( buf0[0]>lastme.bid ){
                  if( MA_cur_dir[hIndex]==0 ){
                     MA_cur_dir[hIndex]=1;
                  }else if( MA_cur_dir[hIndex]==2 ){
                     MA_cur_dir[hIndex]=1;
                     MA_start[hIndex]=true;
                  }
                  if( MA_invert ){
                     noShort=true;
                  }else{
                     noLong=true;
                  }
                  if( MA_across_price ){
                     if( MA_start[hIndex] ){
                        MqlRates rates[];
                        ArraySetAsSeries(rates, true);
                        if( CopyRates( symbol, PERIOD_H1, 1, 1, rates)==1 ){
                           if( buf0[0] >= rates[0].open ){
                              noShort=true;
                           }else{
                              MA_start[hIndex]=false;
                           }
                        }
                     }else{
                        if( MA_invert ){
                           noLong=true;
                        }else{
                           noShort=true;
                        }
                     }
                  }
               }else if( buf0[0]<lastme.bid ){
                  if( MA_cur_dir[hIndex]==0 ){
                     MA_cur_dir[hIndex]=2;
                  }else if( MA_cur_dir[hIndex]==1 ){
                     MA_cur_dir[hIndex]=2;
                     MA_start[hIndex]=true;
                  }
                  if( MA_invert ){
                     noLong=true;
                  }else{
                     noShort=true;
                  }
                  if( MA_across_price ){
                     if( MA_start[hIndex] ){
                        MqlRates rates[];
                        ArraySetAsSeries(rates, true);
                        if( CopyRates( symbol, PERIOD_H1, 1, 1, rates)==1 ){
                           if( buf0[0] <= rates[0].open ){
                              if( MA_invert ){
                                 noShort=true;
                              }else{
                                 noLong=true;
                              }
                           }else{
                              MA_start[hIndex]=false;
                           }
                        }
                     }else{
                              if( MA_invert ){
                                 noShort=true;
                              }else{
                                 noLong=true;
                              }
                     }
                  }
               }
               break;
            case MA_SECOND:
               if (allSymb_use) {
                  CopyBuffer(h_all, 0, 0, 7, buf0);
               } else {
                  CopyBuffer(h[hIndex], 0, 0, 7, buf0);
               }
               if( buf0[0]>buf0[6] ){
                  if( MA_cur_dir[hIndex]==0 ){
                     MA_cur_dir[hIndex]=1;
                  }else if( MA_cur_dir[hIndex]==2 ){
                     MA_cur_dir[hIndex]=1;
                     MA_start[hIndex]=true;
                  }
                              if( MA_invert ){
                                 noLong=true;
                              }else{
                                 noShort=true;
                              }
               }else if( buf0[0]<buf0[6] ){
                  if( MA_cur_dir[hIndex]==0 ){
                     MA_cur_dir[hIndex]=2;
                  }else if( MA_cur_dir[hIndex]==1 ){
                     MA_cur_dir[hIndex]=2;
                     MA_start[hIndex]=true;
                  }
                              if( MA_invert ){
                                 noShort=true;
                              }else{
                                 noLong=true;
                              }
               }
               break;
         }
      #else
         switch(MA_TYPE){
            case MA_FIRST:
               if( iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 0)>lastme.bid ){
                  if( MA_invert ){
                     noShort=true;
                  }else{
                     noLong=true;
                  }
               }else if( iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 0)<lastme.bid ){
                  if( MA_invert ){
                     noLong=true;
                  }else{
                     noShort=true;
                  }
               }         
               break;
            case MA_SECOND:
               if( iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 0)>iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 6) ){
                  if( MA_invert ){
                     noLong=true;
                  }else{
                     noShort=true;
                  }
               }else if( iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 0)<iMA(symbol, MA_Timeframe, MA_PERIOD, 0, MODE_SMA, PRICE_CLOSE, 6) ){
                  if( MA_invert ){
                     noShort=true;
                  }else{
                     noLong=true;
                  }
               }         
               break;
         }
      #endif 
   }   
   
   if(RSI_PERIOD>0){
      double res1=0;
      double res2=0;
      #ifdef __MQL5__ 
         if (allSymb_use) {
            CopyBuffer(h2_all, 0, 0, 2, buf0);
         } else {
            CopyBuffer(h2[hIndex], 0, 0, 2, buf0);
         }
         res1=buf0[0];
         res2=buf0[1];
      #else
         res1=iRSI(symbol, RSI_Timeframe, RSI_PERIOD, PRICE_CLOSE, 0);
         res2=iRSI(symbol, RSI_Timeframe, RSI_PERIOD, PRICE_CLOSE, 1);
      #endif
      lastRSI[hIndex] = res1;
      if( RSI_start[hIndex] ){
         if( !RSI_enter_return && res1>70 ){
            if( RSI_invert ){
               noShort=true;
            }else{
               noLong=true;
            }
            RSI_start[hIndex]=false;
         }else if( RSI_enter_return && res2>70 && res1<70 ){
            if( RSI_invert ){
               noShort=true;
            }else{
               noLong=true;
            }
            RSI_start[hIndex]=false;
         }else if( !RSI_enter_return && res1<30 ){
            if( RSI_invert ){
               noLong=true;
            }else{
               noShort=true;
            }
            RSI_start[hIndex]=false;
         }else if( RSI_enter_return && res2<30 && res1>30 ){
            if( RSI_invert ){
               noLong=true;
            }else{
               noShort=true;
            }
            RSI_start[hIndex]=false;
         }else{
            noLong=true;
            noShort=true;
         }
      }else{
         noLong=true;
         noShort=true;
         if( (res1<50 && res2>50) || (res2<50 && res1>50) ){
            RSI_start[hIndex]=true;
         }
      }
   }
   
   if(BB_PERIOD>0){
      double res0=0;
      double res1=0;
      double res2=0;
      #ifdef __MQL5__ 
         if (allSymb_use) {
            CopyBuffer(h4_all, 0, 0, 1, buf0);
            CopyBuffer(h4_all, 1, 0, 1, buf1);
            CopyBuffer(h4_all, 2, 0, 1, buf2);
         } else {
            CopyBuffer(h4[hIndex], 0, 0, 1, buf0);
            CopyBuffer(h4[hIndex], 1, 0, 1, buf1);
            CopyBuffer(h4[hIndex], 2, 0, 1, buf2);
         }
         res0=buf0[0];
         res1=buf1[0];
         res2=buf2[0];
      #else
         res0=iBands(symbol, BB_Timeframe, BB_PERIOD, BB_DEVIATION, BB_SHIFT, PRICE_CLOSE, 0, 0);
         res1=iBands(symbol, BB_Timeframe, BB_PERIOD, BB_DEVIATION, BB_SHIFT, PRICE_CLOSE, 1, 0);
         res2=iBands(symbol, BB_Timeframe, BB_PERIOD, BB_DEVIATION, BB_SHIFT, PRICE_CLOSE, 2, 0);
      #endif
      if( BB_start[hIndex] ){
         if( res1<=lastme.bid ){
            if( BB_INVERT ){
               noShort=true;
            }else{
               noLong=true;
            }
            BB_start[hIndex]=false;
         }else if( res2>=lastme.bid ){
            if( BB_INVERT ){
               noLong=true;
            }else{
               noShort=true;
            }
            BB_start[hIndex]=false;
         }else{
            noLong=true;
            noShort=true;
         }
      }else{
         noLong=true;
         noShort=true;
         MqlRates rates[];
         ArraySetAsSeries(rates, true);
         // раньше было CopyRates( symbol, PERIOD_H1, 1, 1, rates)==1 !!!
         if( CopyRates( symbol, PERIOD_H1, 0, 1, rates)==1 ){
            if( (res0<rates[0].open && res0>rates[0].close) || (res0>rates[0].open && res0<rates[0].close) ){
               BB_start[hIndex]=true;
            }
         }
      }
   }
   
   if(SK_PERIOD>0){
      double res1=0;
      double res2=0;
      double res3=0;
      double res4=0;
      #ifdef __MQL5__ 
         if (allSymb_use) {
            CopyBuffer(h5_all, 0, 1, 2, buf0);
            CopyBuffer(h5_all, 1, 1, 2, buf1);
         } else {
            CopyBuffer(h5[hIndex], 0, 1, 2, buf0);
            CopyBuffer(h5[hIndex], 1, 1, 2, buf1);
         }
         res1=buf0[0];
         res2=buf0[1];
         res3=buf1[0];
         res4=buf1[1];
      #else
         res1=iStochastic(symbol, S_Timeframe, SK_PERIOD, SD_PERIOD, 3, MODE_SMA, STO_LOWHIGH, 0, 1);
         res2=iStochastic(symbol, S_Timeframe, SK_PERIOD, SD_PERIOD, 3, MODE_SMA, STO_LOWHIGH, 0, 2);
         res3=iStochastic(symbol, S_Timeframe, SK_PERIOD, SD_PERIOD, 3, MODE_SMA, STO_LOWHIGH, 1, 1);
         res4=iStochastic(symbol, S_Timeframe, SK_PERIOD, SD_PERIOD, 3, MODE_SMA, STO_LOWHIGH, 1, 2);
      #endif
      if(res1<res3 && res2>res4 ) {
         if ( S_INVERT ) {
            noLong=true;
         } else {
            noShort=true;
         }
      } else if (res1>res3 && res2<res4 ) {
         if ( S_INVERT ) {
            noShort=true;
         } else {
            noLong=true;
         }
      } else {
         noLong=true;
         noShort=true;
      }
   }
   
   if( SERIES_count>0 ){
      MqlRates rates[];
      ArraySetAsSeries(rates, true);
      if( CopyRates( symbol, SERIES_Timeframe, 1, SERIES_count, rates)==SERIES_count ){
         bool isOkLong=true;
         bool isOkShort=true;
         
         int countLong = 0;
         int countShort = 0;
         double pointLong = 0;
         double pointShort = 0;
         for( int i=0; i<SERIES_count; i++ ){
            if( rates[i].close>rates[i].open ){
               countLong++;
               pointLong += rates[i].close-rates[i].open;
            }else if( rates[i].close<rates[i].open ){
               countShort++;
               pointShort += rates[i].open-rates[i].close;
            }else{
               countLong++;
               countShort++;
            }
            if (SERIES_percent > 99 || SERIES_percent == 0) {
               switch(SERIES_type){
                  case MY_SERIES_MINUS:
                     if (countLong == SERIES_count ) {
                        isOkLong=false;
                     } else if (countShort == SERIES_count ) {
                        isOkShort=false;
                     }
                     break;
                  default:
                     if (countLong == SERIES_count ) {
                        isOkShort=false;
                     } else if (countShort == SERIES_count ) {
                        isOkLong=false;
                     }
               }
            } else {
               if ( SERIES_use_point ) {
                  pointLong /= SymbolInfoDouble(symbol, SYMBOL_POINT);
                  pointShort /= SymbolInfoDouble(symbol, SYMBOL_POINT);
                  switch(SERIES_type){
                     case MY_SERIES_MINUS:
                        if (pointLong >= ( pointShort + pointLong*(SERIES_percent/100) ) ) {
                           isOkLong=false;
                        } else if (pointShort >= ( pointLong + pointShort*(SERIES_percent/100) ) ) {
                           isOkShort=false;
                        }
                        break;
                     default:
                        if (pointLong >= ( pointShort + pointLong*(SERIES_percent/100) ) ) {
                           isOkShort=false;
                        } else if (pointShort >= ( pointLong + pointShort*(SERIES_percent/100) ) ) {
                           isOkLong=false;
                        }
                  }
               } else {
                  switch(SERIES_type){
                     case MY_SERIES_MINUS:
                        if (countLong >= ( countShort + countLong*(SERIES_percent/100) ) ) {
                           isOkLong=false;
                        } else if (countShort >= ( countLong + countShort*(SERIES_percent/100) ) ) {
                           isOkShort=false;
                        }
                        break;
                     default:
                        if (countLong >= ( countShort + countLong*(SERIES_percent/100) ) ) {
                           isOkShort=false;
                        } else if (countShort >= ( countLong + countShort*(SERIES_percent/100) ) ) {
                           isOkLong=false;
                        }
                  }
               }
            }
            
            if( !isOkShort && !isOkLong ){
               break;
            }
         }
         
         if(!isOkShort){
            noShort=true;
         }
         if(!isOkLong){
            noLong=true;
         }
         if ( isOkLong && isOkShort ) {
            noShort=true;
            noLong=true;
         }
      }else{
         noShort=true;
         noLong=true;
      }
   }
   
   bool BARS_noLong=false;
   bool BARS_noShort=false;
   if(BARS_enabled ){
      MqlRates rates[];
      ArraySetAsSeries(rates, true);
      if( CopyRates( symbol, BARS_Timeframe, 0, 2, rates)==2 ){
         if( rates[1].close > rates[1].open ){
            BARS_noShort=true;
         }else if( rates[1].close < rates[1].open ){
            BARS_noLong=true;
         }else{
            BARS_noShort=true;
            BARS_noLong=true;
         }
      }
      if( BARS_noLong ){
         noLong=true;
            if( BARS_RSI_Period>0 ){
               double res1=0;
               #ifdef __MQL5__ 
                  if (allSymb_use) {
                     CopyBuffer(h3_all, 0, 0, 1, buf0);
                  } else {
                     CopyBuffer(h3[hIndex], 0, 0, 1, buf0);
                  }
                  res1=buf0[0];
               #else
                  res1=iRSI(symbol, PERIOD_M5, BARS_RSI_Period, PRICE_CLOSE, 0);
               #endif
               if( res1<=70 ){
                  noShort=true;
               }
            }
      }
      if( BARS_noShort ){
         noShort=true;
            if( BARS_RSI_Period>0 ){
               double res1=0;
               #ifdef __MQL5__ 
                  if (allSymb_use) {
                     CopyBuffer(h3_all, 0, 0, 1, buf0);
                  } else {
                     CopyBuffer(h3[hIndex], 0, 0, 1, buf0);
                  }
                  res1=buf0[0];
               #else
                  res1=iRSI(symbol, PERIOD_M5, BARS_RSI_Period, PRICE_CLOSE, 0);
               #endif
               if( res1>=30 ){
                  noLong=true;
               }
            }
      }
   }
   
   if( typeDir == MA_DIR_LONG ){
      noShort=true;
   }else if( typeDir == MA_DIR_SHORT ){
      noLong=true;
   }
   
   if ( noLongAfterPrice > 0 && lastme.bid > noLongAfterPrice ) {
      noLong=true;
   }
   if ( noShortBeforePrice > 0 && lastme.bid < noShortBeforePrice ) {
      noShort=true;
   }
   
}
bool pdxIsNewBar(string symbol, int hI){
   datetime New_Time[1];
   
   ENUM_TIMEFRAMES   bar_Timeframe=PERIOD_M5;
   if (  change_tf_D1_after > 0 && (LongPos[hI] > change_tf_D1_after || ShortPos[hI] > change_tf_D1_after) ) {
      bar_Timeframe = PERIOD_D1;
   } else if ( change_tf_H4_after > 0 && (LongPos[hI] > change_tf_H4_after || ShortPos[hI] > change_tf_H4_after)  ) {
      bar_Timeframe = PERIOD_H4;
   } else if ( change_tf_H1_after > 0 && (LongPos[hI] > change_tf_H1_after || ShortPos[hI] > change_tf_H1_after)  ) {
      bar_Timeframe = PERIOD_H1;
   } else if ( change_tf_M30_after > 0 && (LongPos[hI] > change_tf_M30_after || ShortPos[hI] > change_tf_M30_after)  ) {
      bar_Timeframe = PERIOD_M30;
   } else if ( change_tf_M15_after > 0 && (LongPos[hI] > change_tf_M15_after || ShortPos[hI] > change_tf_M15_after)  ) {
      bar_Timeframe = PERIOD_M15;
   }
   
   if(CopyTime(symbol,bar_Timeframe,0,1,New_Time)>0){
      if(Old_Time[hI]!=New_Time[0]){
         Old_Time[hI]=New_Time[0];
         cmtData = "";
         needCmtData = true;
         return true;
      }
   }
   return false;
}

bool checkTakeProfit(string symbol, TypeOfPos type = MY_ALLPOS, uint magic = 0, double needTake = 0){
   if (needTake == 0) {
      needTake = takeProfit;
      if ( curOffsetMultChange > 0 ) {
         if (maxCmtNumBuy > 1) {
            needTake += curOffsetMultChange*(maxCmtNumBuy-1)*pointPrice[hIndex];
         } else if (maxCmtNumShort > 1) {
            needTake += curOffsetMultChange*(maxCmtNumShort-1)*pointPrice[hIndex];
         }
      }
      
      if( takeProfit1step>0 && (LongPos[hIndex]+ShortPos[hIndex])==1 ){
         needTake = takeProfit1step;
      }else if( takeProfit2step>0 && (LongPos[hIndex]+ShortPos[hIndex])==2 ){
         needTake = takeProfit2step;
      }else if( takeProfit4step>0 && (LongPos[hIndex]+ShortPos[hIndex])==4 ){
         needTake = takeProfit4step;
      }
   }

   if( needTake <= 0 ) return false;
   
   double curProfit=0;
   double profit=0;
   if( magic==0 ){
      magic=(EA_Magic)*10 + hIndex;
   }
   double curPrice = 0;
   
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if(PositionGetSymbol(ti)!=symbol) continue;
         if(magic>0 && PositionGetInteger(POSITION_MAGIC)!=magic) continue;
         if(type==MY_BUY && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_BUY ) continue;
         if(type==MY_SELL && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_SELL ) continue;
         
         profit+=PositionGetDouble(POSITION_PROFIT);
         profit+=PositionGetDouble(POSITION_SWAP);
         
         if ( curPrice < PositionGetDouble(POSITION_PRICE_OPEN) ) {
            curPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         }
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue;
            if( OrderType()==OP_BUY || OrderType()==OP_SELL ){}else{ continue; }
            if(OrderSymbol()!=symbol) continue;
            if(magic>0 && OrderMagicNumber()!=magic) continue;
            if(type==MY_BUY && OrderType()!=OP_BUY ) continue;
            if(type==MY_SELL && OrderType()!=OP_SELL ) continue;
            
            profit+=OrderCommission();
            profit+=OrderProfit();
            profit+=OrderSwap();
         
            if ( curPrice < OrderOpenPrice() ) {
               curPrice = OrderOpenPrice();
            }
         }
      }
   #endif 
   
   if (profit > 0 && curPrice > 0 ) {
      if (type == MY_ALLPOS) {
         lastProfit[hIndex] = profit;
      }
      if(profit>needTake){
         return true;
      }
   }
   return false;
}


void closeAllPos(string symbol="", TypeOfPos type = MY_ALLPOS, uint magic = 0){
   if( magic==0 ){
      magic=(EA_Magic)*10 + hIndex;
   }
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if( StringLen(symbol) && PositionGetSymbol(ti)!=symbol) continue;
         if(magic>0 && PositionGetInteger(POSITION_MAGIC)!=magic) continue;
         if(type==MY_BUY && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_BUY ) continue;
         if(type==MY_SELL && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_SELL ) continue;

         Trade.PositionClose(PositionGetInteger(POSITION_TICKET));
      }
      int cntMyPosO=OrdersTotal();
      for(int ti=cntMyPosO-1; ti>=0; ti--){
         ulong orderTicket=OrderGetTicket(ti);
         if(StringLen(symbol) && OrderGetString(ORDER_SYMBOL)!=symbol) continue;
         if(magic>0 && OrderGetInteger(ORDER_MAGIC)!=magic) continue;
         if(type==MY_BUY && OrderGetInteger(ORDER_TYPE)!=ORDER_TYPE_BUY_STOP ) continue;
         if(type==MY_SELL && OrderGetInteger(ORDER_TYPE)!=ORDER_TYPE_SELL_STOP ) continue;
         
         Trade.OrderDelete(orderTicket);
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue; 
            if( StringLen(symbol) && OrderSymbol()!=symbol ) continue;
            if(magic>0 && OrderMagicNumber()!=magic) continue;
            
            if( OrderType()==OP_BUY ){
               if(type==MY_SELL) continue;
               MqlTick latest_price;
               if(!SymbolInfoTick(OrderSymbol(),latest_price)){
                  Alert(GetLastError());
                  return;
               }
               if(!OrderClose(OrderTicket(), OrderLots(),latest_price.bid,100)){
               }
            }else if(OrderType()==OP_SELL){
               if(type==MY_BUY) continue;
               MqlTick latest_price;
               if(!SymbolInfoTick(OrderSymbol(),latest_price)){
                  Alert(GetLastError());
                  return;
               }
               if(!OrderClose(OrderTicket(), OrderLots(),latest_price.ask,100)){
               }
            }else{
               if(type==MY_BUY && OrderType()!=OP_BUYSTOP ) continue;
               if(type==MY_SELL && OrderType()!=OP_SELLSTOP ) continue;
               if(!OrderDelete(OrderTicket())){
               }
            }
                        
         }
      }
   #endif 
   cur_take_prices[hIndex] = 0;
   KEF_cur_take_prices[hIndex] = 0;
   lastProfit[hIndex] = 0;
   lastPosIs[hIndex] = MY_ALLPOS;
   
   if (assistExists) {
      closeAssist(symbol, magic);
   }

}

void closeAllProfitPos(string symbol="", TypeOfPos type = MY_ALLPOS, uint magic = 0){
   if( magic==0 ){
      magic=(EA_Magic)*10 + hIndex;
   }
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if( StringLen(symbol) && PositionGetSymbol(ti)!=symbol) continue;
         if(magic>0 && PositionGetInteger(POSITION_MAGIC)!=magic) continue;
         if(type==MY_BUY && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_BUY ) continue;
         if(type==MY_SELL && PositionGetInteger(POSITION_TYPE)!=POSITION_TYPE_SELL ) continue;

         double curProfit = 0;
         curProfit+=PositionGetDouble(POSITION_PROFIT);
         curProfit+=PositionGetDouble(POSITION_SWAP);
         
         if (curProfit > 0) {
            Trade.PositionClose(PositionGetInteger(POSITION_TICKET));
         }

      }
      int cntMyPosO=OrdersTotal();
      for(int ti=cntMyPosO-1; ti>=0; ti--){
         ulong orderTicket=OrderGetTicket(ti);
         if(StringLen(symbol) && OrderGetString(ORDER_SYMBOL)!=symbol) continue;
         if(magic>0 && OrderGetInteger(ORDER_MAGIC)!=magic) continue;
         if(type==MY_BUY && OrderGetInteger(ORDER_TYPE)!=ORDER_TYPE_BUY_STOP ) continue;
         if(type==MY_SELL && OrderGetInteger(ORDER_TYPE)!=ORDER_TYPE_SELL_STOP ) continue;
         
         Trade.OrderDelete(orderTicket);
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue; 
            if( StringLen(symbol) && OrderSymbol()!=symbol ) continue;
            if(magic>0 && OrderMagicNumber()!=magic) continue;
            
            if( OrderType()==OP_BUY ){
               if(type==MY_SELL) continue;
               
               double curProfit = 0;
               curProfit+=OrderCommission();
               curProfit+=OrderProfit();
               curProfit+=OrderSwap();
               if (curProfit > 0) {
                  MqlTick latest_price;
                  if(!SymbolInfoTick(OrderSymbol(),latest_price)){
                     Alert(GetLastError());
                     return;
                  }
                  if(!OrderClose(OrderTicket(), OrderLots(),latest_price.bid,100)){
                  }
               }
               
            }else if(OrderType()==OP_SELL){
               if(type==MY_BUY) continue;
               
               double curProfit = 0;
               curProfit+=OrderCommission();
               curProfit+=OrderProfit();
               curProfit+=OrderSwap();
               if (curProfit > 0) {
                  MqlTick latest_price;
                  if(!SymbolInfoTick(OrderSymbol(),latest_price)){
                     Alert(GetLastError());
                     return;
                  }
                  if(!OrderClose(OrderTicket(), OrderLots(),latest_price.ask,100)){
                  }
               }
            }else{
               if(type==MY_BUY && OrderType()!=OP_BUYSTOP ) continue;
               if(type==MY_SELL && OrderType()!=OP_SELLSTOP ) continue;
               if(!OrderDelete(OrderTicket())){
               }
            }
                        
         }
      }
   #endif 
   cur_take_prices[hIndex] = 0;
   KEF_cur_take_prices[hIndex] = 0;
   lastProfit[hIndex] = 0;
   lastPosIs[hIndex] = MY_ALLPOS;

}

bool pdxSendOrder(TypeOfPos mytype, double price, double sl, double tp, double volume, ulong position=0, string comment="", string sym="", uint magic=0){
      if( !StringLen(sym) ){
         sym=_Symbol;
      }
      if( magic==0 ){
         magic=(EA_Magic)*10 + hIndex;
      }
      int curDigits=(int) SymbolInfoInteger(sym, SYMBOL_DIGITS);
      if(sl>0){
         sl=NormalizeDouble(sl,curDigits);
      }
      if(tp>0){
         tp=NormalizeDouble(tp,curDigits);
      }
      if(price>0){
         price=NormalizeDouble(price,curDigits);
      }else{
         #ifdef __MQL5__ 
         #else
            MqlTick latest_price;
            SymbolInfoTick(sym,latest_price);
            if( mytype == MY_SELL ){
               price=latest_price.ask;
            }else if( mytype == MY_BUY ){
               price=latest_price.bid;
            }
         #endif 
      }
      if( StringLen(my_cmt) && comment=="" ){
         comment=my_cmt;
      }
      if (mytype == MY_BUY || mytype == MY_SELL) {
         if (comment == "0") {} else {
            lastPosIs[hIndex] = mytype;
         }
      }
      
   #ifdef __MQL5__ 
      ENUM_TRADE_REQUEST_ACTIONS action=TRADE_ACTION_DEAL;
      ENUM_ORDER_TYPE type=ORDER_TYPE_BUY;
      switch(mytype){
         case MY_BUY:
            action=TRADE_ACTION_DEAL;
            type=ORDER_TYPE_BUY;
            break;
         case MY_BUYSLTP:
            action=TRADE_ACTION_SLTP;
            type=ORDER_TYPE_BUY;
            break;
         case MY_BUYSTOP:
            action=TRADE_ACTION_PENDING;
            type=ORDER_TYPE_BUY_STOP;
            break;
         case MY_BUYLIMIT:
            action=TRADE_ACTION_PENDING;
            type=ORDER_TYPE_BUY_LIMIT;
            break;
         case MY_SELL:
            action=TRADE_ACTION_DEAL;
            type=ORDER_TYPE_SELL;
            break;
         case MY_SELLSLTP:
            action=TRADE_ACTION_SLTP;
            type=ORDER_TYPE_SELL;
            break;
         case MY_SELLSTOP:
            action=TRADE_ACTION_PENDING;
            type=ORDER_TYPE_SELL_STOP;
            break;
         case MY_SELLLIMIT:
            action=TRADE_ACTION_PENDING;
            type=ORDER_TYPE_SELL_LIMIT;
            break;
      }
      
      MqlTradeRequest mrequest;
      MqlTradeResult mresult;
      ZeroMemory(mrequest);
      
      mrequest.action = action;
      mrequest.sl = sl;
      mrequest.tp = tp;
      mrequest.symbol = sym;
      if(position>0){
         mrequest.position = position;
      }
      if(StringLen(comment)){
         mrequest.comment=comment;
      }
      if(action!=TRADE_ACTION_SLTP){
         if(price>0){
            mrequest.price = price;
         }
         if(volume>0){
            mrequest.volume = volume;
         }
         mrequest.type = type;
         mrequest.magic = magic;
         switch(useORDER_FILLING_RETURN){
            case FOK:
               mrequest.type_filling = ORDER_FILLING_FOK;
               break;
            case RETURN:
               mrequest.type_filling = ORDER_FILLING_RETURN;
               break;
            case IOC:
               mrequest.type_filling = ORDER_FILLING_IOC;
               break;
         }
         mrequest.deviation=100;
      }
      if(OrderSend(mrequest,mresult)){
         if(mresult.retcode==10009 || mresult.retcode==10008){
            if(action!=TRADE_ACTION_SLTP){
               switch(type){
                  case ORDER_TYPE_BUY:
//                     Alert("Order Buy #:",mresult.order," sl",sl," tp",tp," p",price," !!");
                     break;
                  case ORDER_TYPE_SELL:
//                     Alert("Order Sell #:",mresult.order," sl",sl," tp",tp," p",price," !!");
                     break;
               }
            }else{
//               Alert("Order Modify SL #:",mresult.order," sl",sl," tp",tp," !!");
            }
            return true;
         }else{
            msgErr(GetLastError(), mresult.retcode);
         }
      }
   #else 
      int type=OP_BUY;
      switch(mytype){
         case MY_BUY:
            type=OP_BUY;
            break;
         case MY_BUYSTOP:
            type=OP_BUYSTOP;
            break;
         case MY_BUYLIMIT:
            type=OP_BUYLIMIT;
            break;
         case MY_SELL:
            type=OP_SELL;
            break;
         case MY_SELLSTOP:
            type=OP_SELLSTOP;
            break;
         case MY_SELLLIMIT:
            type=OP_SELLLIMIT;
            break;
      }
      
      if(OrderSend(sym, type, volume, price, 100, sl, tp, comment, magic, 0)<0){
            msgErr(GetLastError());
      }else{
         switch(type){
            case OP_BUY:
               Alert("Order Buy sl",sl," tp",tp," p",price," !!");
               break;
            case OP_SELL:
               Alert("Order Sell sl",sl," tp",tp," p",price," !!");
               break;
            }
            return true;
      }
   
   #endif 
   return false;
}

void msgErr(int err, int retcode=0){
   string curErr="";
   switch(err){
      case 1:
         curErr=langs.err1;
         break;
      case 2:
         curErr=langs.err2;
         break;
      case 3:
         curErr=langs.err3;
         break;
      case 4:
         curErr=langs.err4;
         break;
      case 5:
         curErr=langs.err5;
         break;
      case 6:
         curErr=langs.err6;
         break;
      case 7:
         curErr=langs.err7;
         break;
      case 8:
         curErr=langs.err8;
         break;
      case 9:
         curErr=langs.err9;
         break;
      case 64:
         curErr=langs.err64;
         break;
      case 65:
         curErr=langs.err65;
         break;
      case 128:
         curErr=langs.err128;
         break;
      case 129:
         curErr=langs.err129;
         break;
      case 130:
         curErr=langs.err130;
         break;
      case 131:
         curErr=langs.err131;
         break;
      case 132:
         curErr=langs.err132;
         break;
      case 133:
         curErr=langs.err133;
         break;
      case 134:
         curErr=langs.err134;
         break;
      case 135:
         curErr=langs.err135;
         break;
      case 136:
         curErr=langs.err136;
         break;
      case 137:
         curErr=langs.err137;
         break;
      case 138:
         curErr=langs.err138;
         break;
      case 139:
         curErr=langs.err139;
         break;
      case 140:
         curErr=langs.err140;
         break;
      case 141:
         curErr=langs.err141;
         break;
      case 145:
         curErr=langs.err145;
         break;
      case 146:
         curErr=langs.err146;
         break;
      case 147:
         curErr=langs.err147;
         break;
      case 148:
         curErr=langs.err148;
         break;
      default:
         curErr=langs.err0+": "+(string) err;
   }
   if(retcode>0){
      curErr+=" ";
      switch(retcode){
         case 10004:
            curErr+=langs.retcode10004;
            break;
         case 10006:
            curErr+=langs.retcode10006;
            break;
         case 10007:
            curErr+=langs.retcode10007;
            break;
         case 10010:
            curErr+=langs.retcode10010;
            break;
         case 10011:
            curErr+=langs.retcode10011;
            break;
         case 10012:
            curErr+=langs.retcode10012;
            break;
         case 10013:
            curErr+=langs.retcode10013;
            break;
         case 10014:
            curErr+=langs.retcode10014;
            break;
         case 10015:
            curErr+=langs.retcode10015;
            break;
         case 10016:
            curErr+=langs.retcode10016;
            break;
         case 10017:
            curErr+=langs.retcode10017;
            break;
         case 10018:
            curErr+=langs.retcode10018;
            break;
         case 10019:
            curErr+=langs.retcode10019;
            break;
         case 10020:
            curErr+=langs.retcode10020;
            break;
         case 10021:
            curErr+=langs.retcode10021;
            break;
         case 10022:
            curErr+=langs.retcode10022;
            break;
         case 10023:
            curErr+=langs.retcode10023;
            break;
         case 10024:
            curErr+=langs.retcode10024;
            break;
         case 10025:
            curErr+=langs.retcode10025;
            break;
         case 10026:
            curErr+=langs.retcode10026;
            break;
         case 10027:
            curErr+=langs.retcode10027;
            break;
         case 10028:
            curErr+=langs.retcode10028;
            break;
         case 10029:
            curErr+=langs.retcode10029;
            break;
         case 10030:
            curErr+=langs.retcode10030;
            break;
         case 10031:
            curErr+=langs.retcode10031;
            break;
         case 10032:
            curErr+=langs.retcode10032;
            break;
         case 10033:
            curErr+=langs.retcode10033;
            break;
         case 10034:
            curErr+=langs.retcode10034;
            break;
         case 10035:
            curErr+=langs.retcode10035;
            break;
         case 10036:
            curErr+=langs.retcode10036;
            break;
         case 10038:
            curErr+=langs.retcode10038;
            break;
         case 10039:
            curErr+=langs.retcode10039;
            break;
         case 10040:
            curErr+=langs.retcode10040;
            break;
         case 10041:
            curErr+=langs.retcode10041;
            break;
         case 10042:
            curErr+=langs.retcode10042;
            break;
         case 10043:
            curErr+=langs.retcode10043;
            break;
         case 10044:
            curErr+=langs.retcode10044;
            break;
      }
   }
   
   Alert(curErr);
}

void init_lang(){
   switch(LANG){
      case MY_ENG:
         langs.err1="No error, but unknown result. (1)";
         langs.err2="General error (2)";
         langs.err3="Incorrect parameters (3)";
         langs.err4="Trading server is busy (4)";
         langs.err5="Old client terminal version (5)";
         langs.err6="No connection to the trading server (6)";
         langs.err7="Not enough rights (7)";
         langs.err8="Too frequent requests (8)";
         langs.err9="Invalid operation disruptive server operation (9)";
         langs.err64="Account blocked (64)";
         langs.err65="Invalid account number (65)";
         langs.err128="Expired waiting period for the transaction (128)";
         langs.err129="Invalid price (129)";
         langs.err130="Wrong stop loss (130)";
         langs.err131="Wrong volume (131)";
         langs.err132="The market is closed (132)";
         langs.err133="Trade is prohibited (133)";
         langs.err134="Not enough money to complete the transaction. (134)";
         langs.err135="Price changed (135)";
         langs.err136="No prices (136)";
         langs.err137="Broker busy (137)";
         langs.err138="New prices (138)";
         langs.err139="The order is blocked and is already being processed (139)";
         langs.err140="Only purchase allowed (140)";
         langs.err141="Too many requests (141)";
         langs.err145="Modification is prohibited because the order is too close to the market. (145)";
         langs.err146="Trading subsystem is busy (146)";
         langs.err147="Using the expiration date of the order is prohibited by the broker (147)";
         langs.err148="The number of open and pending orders has reached the limit set by the broker (148)";
         langs.err0="An error occurred while running the request";
         langs.retcode="Reason";
         langs.retcode10004="Requote";
         langs.retcode10006="Request rejected";
         langs.retcode10007="Request canceled by trader";
         langs.retcode10010="Only part of the request was completed";
         langs.retcode10011="Request processing error";
         langs.retcode10012="Request canceled by timeout";
         langs.retcode10013="Invalid request";
         langs.retcode10014="Invalid volume in the request";
         langs.retcode10015="Invalid price in the request";
         langs.retcode10016="Invalid stops in the request";
         langs.retcode10017="Trade is disabled";
         langs.retcode10018="Market is closed";
         langs.retcode10019="There is not enough money to complete the request";
         langs.retcode10020="Prices changed";
         langs.retcode10021="There are no quotes to process the request";
         langs.retcode10022="Invalid order expiration date in the request";
         langs.retcode10023="Order state changed";
         langs.retcode10024="Too frequent requests";
         langs.retcode10025="No changes in request";
         langs.retcode10026="Autotrading disabled by server";
         langs.retcode10027="Autotrading disabled by client terminal";
         langs.retcode10028="Request locked for processing";
         langs.retcode10029="Order or position frozen";
         langs.retcode10030="Invalid order filling type";
         langs.retcode10031="No connection with the trade server";
         langs.retcode10032="Operation is allowed only for live accounts";
         langs.retcode10033="The number of pending orders has reached the limit";
         langs.retcode10034="The volume of orders and positions for the symbol has reached the limit";
         langs.retcode10035="Incorrect or prohibited order type";
         langs.retcode10036="Position with the specified POSITION_IDENTIFIER has already been closed";
         langs.retcode10038="A close volume exceeds the current position volume";
         langs.retcode10039="A close order already exists for a specified position";
         langs.retcode10040="Number of open items exceeded";
         langs.retcode10041="The pending order activation request is rejected, the order is canceled";
         langs.retcode10042="Only long positions are allowed";
         langs.retcode10043="Only short positions are allowed";
         langs.retcode10044="Only position closing is allowed";
         break;
      case MY_RUS:
         langs.err0="Во время выполнения запроса произошла ошибка";
         langs.err1="Нет ошибки, но результат неизвестен (1)";
         langs.err2="Общая ошибка (2)";
         langs.err3="Неправильные параметры (3)";
         langs.err4="Торговый сервер занят (4)";
         langs.err5="Старая версия клиентского терминала (5)";
         langs.err6="Нет связи с торговым сервером (6)";
         langs.err7="Недостаточно прав (7)";
         langs.err8="Слишком частые запросы (8)";
         langs.err9="Недопустимая операция нарушающая функционирование сервера (9)";
         langs.err64="Счет заблокирован (64)";
         langs.err65="Неправильный номер счета (65)";
         langs.err128="Истек срок ожидания совершения сделки (128)";
         langs.err129="Неправильная цена (129)";
         langs.err130="Неправильные стопы (130)";
         langs.err131="Неправильный объем (131)";
         langs.err132="Рынок закрыт (132)";
         langs.err133="Торговля запрещена (133)";
         langs.err134="Недостаточно денег для совершения операции (134)";
         langs.err135="Цена изменилась (135)";
         langs.err136="Нет цен (136)";
         langs.err137="Брокер занят (137)";
         langs.err138="Новые цены (138)";
         langs.err139="Ордер заблокирован и уже обрабатывается (139)";
         langs.err140="Разрешена только покупка (140)";
         langs.err141="Слишком много запросов (141)";
         langs.err145="Модификация запрещена, так как ордер слишком близок к рынку (145)";
         langs.err146="Подсистема торговли занята (146)";
         langs.err147="Использование даты истечения ордера запрещено брокером (147)";
         langs.err148="Количество открытых и отложенных ордеров достигло предела, установленного брокером (148)";
         langs.retcode="Причина";
         langs.retcode10004="Реквота";
         langs.retcode10006="Запрос отклонен";
         langs.retcode10007="Запрос отменен трейдером";
         langs.retcode10010="Заявка выполнена частично";
         langs.retcode10011="Ошибка обработки запроса";
         langs.retcode10012="Запрос отменен по истечению времени";
         langs.retcode10013="Неправильный запрос";
         langs.retcode10014="Неправильный объем в запросе";
         langs.retcode10015="Неправильная цена в запросе";
         langs.retcode10016="Неправильные стопы в запросе";
         langs.retcode10017="Торговля запрещена";
         langs.retcode10018="Рынок закрыт";
         langs.retcode10019="Нет достаточных денежных средств для выполнения запроса";
         langs.retcode10020="Цены изменились";
         langs.retcode10021="Отсутствуют котировки для обработки запроса";
         langs.retcode10022="Неверная дата истечения ордера в запросе";
         langs.retcode10023="Состояние ордера изменилось";
         langs.retcode10024="Слишком частые запросы";
         langs.retcode10025="В запросе нет изменений";
         langs.retcode10026="Автотрейдинг запрещен сервером";
         langs.retcode10027="Автотрейдинг запрещен клиентским терминалом";
         langs.retcode10028="Запрос заблокирован для обработки";
         langs.retcode10029="Ордер или позиция заморожены";
         langs.retcode10030="Указан неподдерживаемый тип исполнения ордера по остатку ";
         langs.retcode10031="Нет соединения с торговым сервером";
         langs.retcode10032="Операция разрешена только для реальных счетов";
         langs.retcode10033="Достигнут лимит на количество отложенных ордеров";
         langs.retcode10034="Достигнут лимит на объем ордеров и позиций для данного символа";
         langs.retcode10035="Неверный или запрещённый тип ордера";
         langs.retcode10036="Позиция с указанным POSITION_IDENTIFIER уже закрыта";
         langs.retcode10038="Закрываемый объем превышает текущий объем позиции";
         langs.retcode10039="Для указанной позиции уже есть ордер на закрытие";
         langs.retcode10040="Количество открытых позиций превышено";
         langs.retcode10041="Запрос на активацию отложенного ордера отклонен, а сам ордер отменен";
         langs.retcode10042="Разрешены только длинные позиции";
         langs.retcode10043="Разрешены только короткие позиции";
         langs.retcode10044="Разрешено только закрывать существующие позиции";
         break;
   }
}
void closeAssist(string symname, int magic){
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if(PositionGetSymbol(ti)!=symname) {
            continue;
         }
         
         if(magic>0 && PositionGetInteger(POSITION_MAGIC) == magic*100 ) {
            // it is assist
            //close assist by take
            Trade.PositionClose(PositionGetInteger(POSITION_TICKET));
            continue;
         }
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue;
            if( OrderType()==OP_BUY || OrderType()==OP_SELL ){}else{ continue; }
            
            if(OrderSymbol()!=symname) {
               continue;
            }

            if(magic>0 && OrderMagicNumber() == magic*100 ) {
               // it is assist
               //close assist by take
               MqlTick latest_price;
               if(SymbolInfoTick(OrderSymbol(),latest_price)){
                  if(OrderType()==OP_BUY){
                     if(!OrderClose(OrderTicket(), OrderLots(),latest_price.bid,100)){
                     }
                  } else if (OrderType()==OP_SELL){
                     if(!OrderClose(OrderTicket(), OrderLots(),latest_price.ask,100)){
                     }
                  }
               }
               continue;
            }
         }
      }
   #endif
   
   assistExists = false;
}
void getmeinfo_btn(string symname, int magic){
   double profit=0;
   double positionExist=false;
   LongVol[hIndex]=0;
   ShortVol[hIndex]=0;
   datetime oldTimeLong=0;
   datetime oldTimeShort=0;
   
   countCurSymb = 0;
   string allCurSymbs = "";
   
   maxTotalPosCntr = 0;
   maxTotalVolCntr = 0;
   LastLong=0;
   LastShort=0;
   
   defenseProfit = 0;
   defenseExists = false;
   defenseTicket = 0;
   defenseVol = 0;
   
   assistExists = false;
   
   curProfitForAllPos = 0;
   
   minVolTicket = 0;
   maxLossTicket = 0;
   maxVolTicket = 0;
   maxProfitTicket = 0;
   minVolIs = 0;
   maxLossIs = 0;
   maxVolIs = 0;
   minVolProfit = 0;
   maxLossProfit = 0;
   maxVolProfit = 0;
   maxProfit = 0;
   maxProfitVol = 0;
   
   maxCmtNumBuy = 0;
   maxCmtNumShort = 0;
   
   minPriceLevel = 0;
   maxPriceLevel = 0;
   
   LongPos[hIndex]=0;
   ShortPos[hIndex]=0;

   // подсчитываем кол-во открытых позиций в Long и Short,
   // и общую прибыль по ним
   #ifdef __MQL5__ 
      int cntMyPos=PositionsTotal();
      for(int ti=cntMyPos-1; ti>=0; ti--){
         if (StringFind(allCurSymbs, "|" + PositionGetString(POSITION_SYMBOL) + "|") == -1) {
            countCurSymb++;
            StringAdd(allCurSymbs, "|" + PositionGetString(POSITION_SYMBOL) + "|");
         }
         if(PositionGetSymbol(ti)!=symname) {
            maxTotalPosCntr++;
            maxTotalVolCntr += PositionGetDouble(POSITION_VOLUME);
            continue;
         } else {
            maxTotalPosCntr++;
            maxTotalVolCntr += PositionGetDouble(POSITION_VOLUME);
         }
         
         if(magic>0 && PositionGetInteger(POSITION_MAGIC) == magic*100 ) {
            // it is assist
            double curProfit = 0;
            curProfit+=PositionGetDouble(POSITION_PROFIT);
            curProfit+=PositionGetDouble(POSITION_SWAP);
            if (assistTake > 0 && curProfit >= assistTake) {
               //close assist by take
               Trade.PositionClose(PositionGetInteger(POSITION_TICKET));
            } else {
               assistExists = true;
            }
            continue;
         }
         
         if(magic>0 && PositionGetInteger(POSITION_MAGIC)!=magic) continue;

         double curProfit = 0;
         curProfit+=PositionGetDouble(POSITION_PROFIT);
         curProfit+=PositionGetDouble(POSITION_SWAP);
         
         string curComment=PositionGetString(POSITION_COMMENT);
         if ( curComment == "0" ) {
            defenseExists = true;
            defenseProfit = curProfit;
            defenseTicket = PositionGetInteger(POSITION_TICKET);
            defenseVol = PositionGetDouble(POSITION_VOLUME);
            continue;
         }
         
         
         positionExist=true;
         
         if (curTakeForPos > 0 && curTakeForPos <= curProfit) {
            Trade.PositionClose(PositionGetInteger(POSITION_TICKET));
            continue;
         }
         
         if (curProfit < 0) {
            if (maxLossProfit >= 0 || maxLossProfit > curProfit) {
               maxLossIs = PositionGetDouble(POSITION_VOLUME);
               maxLossTicket = PositionGetInteger(POSITION_TICKET);
               maxLossProfit = curProfit;
            }
         }
         if (minVolIs == 0 || minVolIs >= PositionGetDouble(POSITION_VOLUME)) {
            if ( minVolIs == PositionGetDouble(POSITION_VOLUME) && minVolProfit < curProfit ) {} else {
               minVolIs = PositionGetDouble(POSITION_VOLUME);
               minVolTicket = PositionGetInteger(POSITION_TICKET);
               minVolProfit = curProfit;
            }
         }
         if (maxVolIs == 0 || maxVolIs <= PositionGetDouble(POSITION_VOLUME)) {
            if ( maxVolIs == PositionGetDouble(POSITION_VOLUME) && maxVolProfit > curProfit ) {} else {
               if (curComment != "777") {
                  maxVolIs = PositionGetDouble(POSITION_VOLUME);
                  maxVolTicket = PositionGetInteger(POSITION_TICKET);
                  maxVolProfit = curProfit;
               }
            }
         }

         if(StringLen(curComment) && curComment != "777"){
            if (griderBothCmtSep == true) {
               if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY){
                  if (maxCmtNumBuy < (uint) StringToInteger(curComment) ) {
                     maxCmtNumBuy = (uint) StringToInteger(curComment);
                  }
               } else {
                  if (maxCmtNumShort < (uint) StringToInteger(curComment) ) {
                     maxCmtNumShort = (uint) StringToInteger(curComment);
                  }
               }
            } else {
               if (maxCmtNumBuy < (uint) StringToInteger(curComment) ) {
                  maxCmtNumBuy = (uint) StringToInteger(curComment);
                  maxCmtNumShort = (uint) StringToInteger(curComment);
               }
            }
         }

         
         if (minPriceLevel == 0 || minPriceLevel > PositionGetDouble(POSITION_PRICE_OPEN)) {
            minPriceLevel = PositionGetDouble(POSITION_PRICE_OPEN);
         }
         if (maxPriceLevel == 0 || maxPriceLevel < PositionGetDouble(POSITION_PRICE_OPEN)) {
            maxPriceLevel = PositionGetDouble(POSITION_PRICE_OPEN);
         }
         
         if ( curProfit > 0 ) {
            if (maxProfit == 0 || maxProfit < curProfit) {
               maxProfit = curProfit;
               maxProfitTicket = PositionGetInteger(POSITION_TICKET);
               maxProfitVol = PositionGetDouble(POSITION_VOLUME);
            }
            curProfitForAllPos += curProfit;
         }
         
         if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY){
            LongVol[hIndex]+=PositionGetDouble(POSITION_VOLUME);
            LongPos[hIndex]++;
            if( !oldTimeLong || oldTimeLong<PositionGetInteger(POSITION_TIME) ){
               oldTimeLong=(datetime) PositionGetInteger(POSITION_TIME);
               LastLong=PositionGetDouble(POSITION_PRICE_OPEN);
            }
         }else{
            ShortVol[hIndex]+=PositionGetDouble(POSITION_VOLUME);
            ShortPos[hIndex]++;
            if( !oldTimeShort || oldTimeShort<PositionGetInteger(POSITION_TIME) ){
               oldTimeShort=(datetime) PositionGetInteger(POSITION_TIME);
               LastShort=PositionGetDouble(POSITION_PRICE_OPEN);
            }
         }
         
         profit += curProfit;
      }
   #else
      int cntMyPos=OrdersTotal();
      if(cntMyPos>0){
         for(int ti=cntMyPos-1; ti>=0; ti--){
            if(OrderSelect(ti,SELECT_BY_POS,MODE_TRADES)==false) continue;
            if( OrderType()==OP_BUY || OrderType()==OP_SELL ){}else{ continue; }
            
            if (StringFind(allCurSymbs, "|" + OrderSymbol() + "|") == -1) {
               countCurSymb++;
               StringAdd(allCurSymbs, "|" + OrderSymbol() + "|");
            }
            if(OrderSymbol()!=symname) {
               maxTotalPosCntr++;
               maxTotalVolCntr += OrderLots();
               continue;
            } else {
               maxTotalPosCntr++;
               maxTotalVolCntr += OrderLots();
            }

            
            if(magic>0 && OrderMagicNumber() == magic*100 ) {
               // it is assist
               double curProfit = 0;
               curProfit+=OrderCommission();
               curProfit+=OrderProfit();
               curProfit+=OrderSwap();
               if (assistTake > 0 && curProfit >= assistTake) {
                  //close assist by take
                  MqlTick latest_price;
                  if(SymbolInfoTick(OrderSymbol(),latest_price)){
                     if(OrderType()==OP_BUY){
                        if(!OrderClose(OrderTicket(), OrderLots(),latest_price.bid,100)){
                        }
                     } else if (OrderType()==OP_SELL){
                        if(!OrderClose(OrderTicket(), OrderLots(),latest_price.ask,100)){
                        }
                     }
                  }
               } else {
                  assistExists = true;
               }
               continue;
            }

            
            if(magic>0 && OrderMagicNumber()!=magic) continue;
   
            double curProfit = 0;
            curProfit+=OrderCommission();
            curProfit+=OrderProfit();
            curProfit+=OrderSwap();
            
            string curComment=OrderComment();
            if ( curComment == "0" ) {
               defenseExists = true;
               defenseProfit = curProfit;
               defenseTicket = OrderTicket();
               defenseVol = OrderLots();
               continue;
            }
            
            
            positionExist=true;
            
            if (curTakeForPos > 0 && curTakeForPos <= curProfit) {
            
               MqlTick latest_price;
               if(SymbolInfoTick(OrderSymbol(),latest_price)){
                  if(OrderType()==OP_BUY){
                     if(!OrderClose(OrderTicket(), OrderLots(),latest_price.bid,100)){
                     }
                  } else if (OrderType()==OP_SELL){
                     if(!OrderClose(OrderTicket(), OrderLots(),latest_price.ask,100)){
                     }
                  }
                  continue;
               }
            }
         
            if (curProfit < 0) {
               if (maxLossProfit >= 0 || maxLossProfit > curProfit) {
                  maxLossIs = OrderLots();
                  maxLossTicket = OrderTicket();
                  maxLossProfit = curProfit;
               }
            }
            if (minVolIs == 0 || minVolIs >= OrderLots()) {
               if ( minVolIs == OrderLots() && minVolProfit < curProfit ) {} else {
                  minVolIs = OrderLots();
                  minVolTicket = OrderTicket();
                  minVolProfit = curProfit;
               }
            }
            if (maxVolIs == 0 || maxVolIs <= OrderLots()) {
               if ( maxVolIs == OrderLots() && maxVolProfit > curProfit ) {} else {
                  if (curComment != "777") {
                     maxVolIs = OrderLots();
                     maxVolTicket = OrderTicket();
                     maxVolProfit = curProfit;
                  }
               }
            }
               
            if(StringLen(curComment) && curComment != "777"){
               if (griderBothCmtSep == true) {
                  if(OrderType()==OP_BUY){
                     if (maxCmtNumBuy < (uint) StringToInteger(curComment) ) {
                        maxCmtNumBuy = (uint) StringToInteger(curComment);
                     }
                  } else {
                     if (maxCmtNumShort < (uint) StringToInteger(curComment) ) {
                        maxCmtNumShort = (uint) StringToInteger(curComment);
                     }
                  }
               } else {
                  if (maxCmtNumBuy < (uint) StringToInteger(curComment) ) {
                     maxCmtNumBuy = (uint) StringToInteger(curComment);
                     maxCmtNumShort = (uint) StringToInteger(curComment);
                  }
               }
            }
            
            
            if (minPriceLevel == 0 || minPriceLevel > OrderOpenPrice()) {
               minPriceLevel = OrderOpenPrice();
            }
            if (maxPriceLevel == 0 || maxPriceLevel < OrderOpenPrice()) {
               maxPriceLevel = OrderOpenPrice();
            }
            
            if ( curProfit > 0 ) {
               if (maxProfit == 0 || maxProfit < curProfit) {
                  maxProfit = curProfit;
                  maxProfitTicket = OrderTicket();
                  maxProfitVol = OrderLots();
               }
            }

            if(OrderType()==OP_BUY){
               LongVol[hIndex]+=OrderLots();
               LongPos[hIndex]++;
               if( !oldTimeLong || oldTimeLong<OrderOpenTime() ){
                  oldTimeLong=OrderOpenTime();
                  LastLong=OrderOpenPrice();
               }
            }else{
               ShortVol[hIndex]+=OrderLots();
               ShortPos[hIndex]++;
               if( !oldTimeShort || oldTimeShort<OrderOpenTime() ){
                  oldTimeShort=OrderOpenTime();
                  LastShort=OrderOpenPrice();
               }
            }
            profit += curProfit;
         }
      }
   #endif
   
   // open defense lot 
   if ( defenseExists == false && profit < 0 && defStartDollars > 0 && defStartDollars <= MathAbs(profit) && defStartLotKefs > 0 ) {
      if (ShortPos[hIndex] == 0 && LongVol[hIndex] > 0 ) {
         if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, MathRound((LongVol[hIndex]*defStartLotKefs)*100)/100 , 0, "0", symname, magic)){
         }
      }else if (LongPos[hIndex] == 0 && ShortVol[hIndex] > 0 ) {
         if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, MathRound((ShortVol[hIndex]*defStartLotKefs)*100)/100 , 0, "0", symname, magic)){
         }
      }
   }
   // open assist lot
   if (!assistExists && assistLot > 0 && assistStartStep > 0 && assistTake > 0) {
      if (ShortPos[hIndex] >= assistStartStep ) {
         if(!pdxSendOrder(MY_BUY, lastme.bid, 0, 0, assistLot , 0, "", symname, magic*100)){
         }
      } else if (LongPos[hIndex] >= assistStartStep ) {
         if(!pdxSendOrder(MY_SELL, lastme.ask, 0, 0, assistLot , 0, "", symname, magic*100)){
         }
      }
   }
}
void createObject(string name, int weight, string title){
   // если кнопки с именем name нет на графике, тогда создать ее
   if(ObjectFind(0, name)<0){
      // определяем смещение относительно правого угла графика, где вывести кнопку
      long offset= ChartGetInteger(0, CHART_WIDTH_IN_PIXELS)-87;
      long offsetY=0;
      for(int ti=0; ti<ObjectsTotal((long) 0); ti++){
         string objName= ObjectName(0, ti);
         if( StringFind(objName, prefix_graph)<0 ){
            continue;
         }
         long tmpOffset=ObjectGetInteger(0, objName, OBJPROP_YDISTANCE);
         if( tmpOffset>offsetY){
            offsetY=tmpOffset;
         }
      }
      
      for(int ti=0; ti<ObjectsTotal((long) 0); ti++){
         string objName= ObjectName(0, ti);
         if( StringFind(objName, prefix_graph)<0 ){
            continue;
         }
         long tmpOffset=ObjectGetInteger(0, objName, OBJPROP_YDISTANCE);
         if( tmpOffset!=offsetY ){
            continue;
         }
         
         tmpOffset=ObjectGetInteger(0, objName, OBJPROP_XDISTANCE);
         if( tmpOffset>0 && tmpOffset<offset){
            offset=tmpOffset;
         }
      }
      offset-=(weight+1);
      if(offset<0){
         offset=ChartGetInteger(0, CHART_WIDTH_IN_PIXELS)-87;
         offsetY+=25;
         offset-=(weight+1);
      }
  
     ObjectCreate(0, name, OBJ_BUTTON, 0, 0, 0);
     ObjectSetInteger(0,name,OBJPROP_XDISTANCE,offset); 
     ObjectSetInteger(0,name,OBJPROP_YDISTANCE,offsetY); 
     ObjectSetString(0,name,OBJPROP_TEXT, title); 
     ObjectSetInteger(0,name,OBJPROP_XSIZE,weight);
     ObjectSetInteger(0,name,OBJPROP_FONTSIZE, 8);
     ObjectSetInteger(0,name,OBJPROP_COLOR, clrBlack);
     ObjectSetInteger(0,name,OBJPROP_YSIZE,25); 
     ObjectSetInteger(0,name,OBJPROP_BGCOLOR, clrLightGray);
     ChartRedraw(0);
  }else{
     ObjectSetString(0,name,OBJPROP_TEXT, title);
  }
}

double OnTester()
{
   double  profit = TesterStatistics(STAT_PROFIT);
   double  max_dd = TesterStatistics(STAT_EQUITY_DD);
   Print("STAT_EQUITY_DD: " + (string) max_dd);
   if ( max_dd == 0 ) return 0.0;
   double  rec_factor = profit/max_dd;
  
   if (profit < 0 && rec_factor < 0) {
      // при убытках
      // вместо фактора восстановления выводим уменьшение баланса депозита в процентах
      double initD = TesterStatistics(STAT_INITIAL_DEPOSIT);
  
      return (profit/initD)*100;
   }
  
//   if ( max_dd > 100 ) rec_factor = 0;
  
   if ( TesterStatistics(STAT_TRADES) < 30 ) rec_factor = 0;

   // при прибыли выводим фактор восстановления
   return rec_factor;
}