//+------------------------------------------------------------------+
//|                                                   NeuroFirst.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <NeuroFirstValidator.mqh>

//--- input parameters
//--- входные параметры
input int      StopLoss=30;      // Stop Loss
input int      TakeProfit=100;   // Take Profit
input int      ADX_Period=8;     // Период ADX
input int      MA_Period=8;      // Период Moving Average
input int      EA_Magic=12345;   // Magic Number советника
input double   Adx_Min=22.0;     // Минимальное значение ADX
input double   Lot=0.1;          // Количество лотов для торговли
//--- глобальные переменные
int adxHandle; // хэндл индикатора ADX
int maHandle;  // хэндл индикатора Moving Average
double plsDI[],minDI[],adxVal[]; // динамические массивы для хранения численных значений +DI, -DI и ADX для каждого бара
double maVal[]; // динамический массив для хранения значений индикатора Moving Average для каждого бара
double p_close; // переменная для хранения значения close бара
int STP, TKP; // будут использованы для значений Stop Loss и Take Profit
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---

 string InpFileName = "yyy\\Files\\exportFromMql.dat";
 int file_handle=FileOpen(InpFileName,FILE_WRITE|FILE_CSV|FILE_ANSI|FILE_COMMON);
   if(file_handle!=INVALID_HANDLE)
     {
      PrintFormat("Файл %s открыт для записи",InpFileName);
      PrintFormat("Путь к файлу: %s\\Files\\",TerminalInfoString(TERMINAL_DATA_PATH));
      //--- подготовим вспомогательные переменные
      string str="test";

      FileWriteString(file_handle,str+"\r\n");

      //--- закрываем файл
      FileClose(file_handle);
      PrintFormat("Данные записаны, файл %s закрыт",InpFileName);
     } else {
       PrintFormat("Не удалось открыть файл %s, Код ошибки = %d",InpFileName,GetLastError());
     }



   Comment("Привет, sad");
   EventSetTimer(60);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   
  }
//+------------------------------------------------------------------+
