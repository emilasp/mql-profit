//+------------------------------------------------------------------+
//|                                           fast-start-example.mq5 |
//|                        Copyright 2012, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2012, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <CoreTrade.mqh>
#include <TrailingStopBase.mqh> // подключение класса трейлинга

#include <Canvas\Canvas.mqh>

//--- входные параметры
input int    EA_Magic           = 239310;// Magic Number советника
input int    StopLoss           =    30; // Stop Loss
input int    TakeProfit         =   50;  // Take Profit
input double Lot                =   0.1; // Количество лотов для торговли
input double TrailingSARStep    =  0.02; // Шаг Parabolic
input double TrailingSARMaximum =   0.1; // Максимум Parabolic

string Obj_Name_1 ="1";
string Obj_Name_2 ="2";

//--- глобальные переменные
int  stoch_1H_handle;
int    MACD_1H_handle;            
int    maHandle;            
  
double plsDI[],minDI[],adxVal[]; // динамические массивы для хранения численных значений +DI, -DI и ADX для каждого бара
double maVal[];                  // динамический массив для хранения значений индикатора Moving Average для каждого бара
double p_close;                  // переменная для хранения значения close бара
int    STP,TKP;                  // будут использованы для значений Stop Loss и Take Profit
//
int               iMA_handle;                                      //переменная для хранения хендла индикатора
double            iMA_buf[];                                       //динамический массив для хранения значений индикатора
double            Close_buf[];                                     //динамический массив для хранения цены закрытия каждого бара
// current Times And prices
datetime Time[];
double High[];
double Low[];

//
string ObjectsName[]; 
datetime ObjectsTime[];
double ObjectsHigh[]; 



string            my_symbol;                                       //переменная для хранения символа
ENUM_TIMEFRAMES   my_timeframe;                                    //переменная для хранения таймфрейма

CoreTrade         *m_CoreTrade;
CParabolicStop Trailing; // создание экземпляра класса 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {



//--- Достаточно ли количество баров для работы
   //--- общее количество баров на графике меньше 60?
   if(Bars(_Symbol,_Period)<60) 
     {
      Alert("На графике меньше 60 баров, советник не будет работать!!");
      return(-1);
     }
   
     
     
     //////
  
  
   m_CoreTrade = new CoreTrade(Symbol(), PERIOD_CURRENT, EA_Magic);
   m_CoreTrade.accountInit();
   m_CoreTrade.putToSocketData("yyy\\Files\\exportFromMql.dat", "New testetsd");
  
   my_symbol=Symbol();                                             //сохраним текущий символ графика для дальнейшей работы советника именно на этом символе
   my_timeframe=PERIOD_CURRENT; 
   
   
   // Set MA indicator                                   //сохраним текущий период графика для дальнейшей работы советника именно на этом периоде
   iMA_handle=iMA(my_symbol,my_timeframe,40,0,MODE_SMA,PRICE_CLOSE);  //подключаем индикатор и получаем его хендл
   if(iMA_handle==INVALID_HANDLE)                                  //проверяем наличие хендла индикатора
   {
      Print("Не удалось получить хендл индикатора");               //если хендл не получен, то выводим сообщение в лог об ошибке
      return(-1);                                                  //завершаем работу с ошибкой
   }
   ChartIndicatorAdd(ChartID(),0,iMA_handle);                      //добавляем индикатор на ценовой график
   ArraySetAsSeries(iMA_buf,true);                                 //устанавливаем индексация для массива iMA_buf как в таймсерии
   ArraySetAsSeries(Close_buf,true);                              //устанавливаем индексация для массива Close_buf как в таймсерии
   
   // Set MACD indicator 1H
   MACD_1H_handle = iMACD(my_symbol,PERIOD_H1, 12, 26, 9, PRICE_CLOSE);
   if(MACD_1H_handle==INVALID_HANDLE)                                  
   {
      Print("Не удалось получить хендл индикатора");              
      return(-1);                                                 
   }
   ChartIndicatorAdd(ChartID(),1,MACD_1H_handle);
   
   // Set MACD indicator 1H
   stoch_1H_handle = iStochastic(my_symbol,PERIOD_H1, 5, 3, 3, MODE_SMA,STO_LOWHIGH);
   if(stoch_1H_handle==INVALID_HANDLE)                                  
   {
      Print("Не удалось получить хендл индикатора");              
      return(-1);                                                 
   }
   ChartIndicatorAdd(ChartID(),1,stoch_1H_handle);
   
   
   
   //--- Инициализация (установка основных параметров)
   Trailing.Init(_Symbol,PERIOD_CURRENT,true,true,false); 
   //--- Установка параметров используемого типа трейлинг стопа
   if(!Trailing.SetParameters(TrailingSARStep,TrailingSARMaximum))
     { 
      Alert("trailing error");
      return(-1);
     }
   Trailing.StartTimer(); // Запуск таймера
   Trailing.On();         // Включение
   
   
   return(0);                                                      //возвращаем 0, инициализация завершена
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   IndicatorRelease(iMA_handle);                                   //удаляет хэндл индикатора и освобождает память занимаемую им
   ArrayFree(iMA_buf);                                             //освобождаем динамический массив iMA_buf от данных
   ArrayFree(Close_buf);                                           //освобождаем динамический массив Close_buf от данных
   //
   //--- Освобождаем хэндлы индикаторов
   IndicatorRelease(MACD_1H_handle);
   IndicatorRelease(stoch_1H_handle);
   IndicatorRelease(maHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
       ////////// Trail Expert
      Trailing.DoStoploss();

// Для сохранения значения времени бара мы используем static-переменную Old_Time.
// При каждом выполнении функции OnTick мы будем сравнивать время текущего бара с сохраненным временем.
// Если они не равны, это означает, что начал строится новый бар.

   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;

// копируем время текущего бара в элемент New_Time[0]
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0)                 // ok, успешно скопировано
     {
      if(Old_Time!=New_Time[0]) // если старое время не равно
        {
         IsNewBar=true;         // новый бар
         if(MQL5InfoInteger(MQL5_DEBUGGING)) Print("Новый бар",New_Time[0],"старый бар",Old_Time);
         Old_Time=New_Time[0];  // сохраняем время бара
        }
     }
   else
     {
      Alert("Ошибка копирования времени, номер ошибки =",GetLastError());
      ResetLastError();
      return;
     }

//--- советник должен проверять условия совершения новой торговой операции только при новом баре
   if(IsNewBar==false)
     {
      return;
     }

 //--- Имеем ли мы достаточное количество баров на графике для работы
   int Mybars=Bars(_Symbol,_Period);
   if(Mybars<60) // если общее количество баров меньше 60
     {
      Alert("На графике менее 60 баров, советник работать не будет!!");
      return;
     } 
  
   

//--- есть ли открытые позиции?
   bool Buy_opened=false;  // переменные, в которых будет храниться информация 
   bool Sell_opened=false; // о наличии соответствующих открытых позиций

   // есть открытая позиция
   if(PositionSelect(_Symbol)==true) 
     {
      if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
        {
         Buy_opened=true;  //это длинная позиция
        }
      else if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
        {
         Sell_opened=true; // это короткая позиция
        }
     }

  
   CopyTime(Symbol(),PERIOD_CURRENT, 0, Bars(Symbol(), PERIOD_CURRENT), Time);
   CopyHigh(Symbol(),PERIOD_CURRENT, 0, Bars(Symbol(), PERIOD_CURRENT), High);
   ArraySetAsSeries(Time, true);
   ArraySetAsSeries(High, true);
   
   //PrintFormat("Bar Time: %s", TimeToString(Time[0]));
   //PrintFormat("Bar Bid: %s", DoubleToString(High[0]));

  
  
  /////////////////////
  /////////////////////
   int err1=0;                                                     //переменная для хранения результатов работы с буфером индикатора
   int err2=0;                                                     //переменная для хранения результатов работы с ценовым графиком
   
   err1=CopyBuffer(iMA_handle,0,1,2,iMA_buf);                      //копируем данные из индикаторного массива в динамический массив iMA_buf для дальнейшей работы с ними
   err2=CopyClose(my_symbol,my_timeframe,1,2,Close_buf);           //копируем данные ценового графика в динамический массив Close_buf  для дальнейшей работы с ними
   if(err1<0 || err2<0)                                            //если есть ошибки
   {
      Print("Не удалось скопировать данные из индикаторного буфера или буфера ценового графика");  //то выводим сообщение в лог об ошибке
      return;                                                                                      //и выходим из функции
   }

   if(iMA_buf[1]>Close_buf[1] && iMA_buf[0]<Close_buf[0])          //если значение индикатора были больше цены закрытия и стали меньше
     {
      createObjectOnChart(Time[0],High[0], "buy a", 30, clrGreen);
      //m_CoreTrade.openPositionBuy(Lot, TakeProfit, StopLoss);
     }
   if(iMA_buf[1]<Close_buf[1] && iMA_buf[0]>Close_buf[0])          //если значение индикатора были меньше цены закрытия и стали больше
     {
      createObjectOnChart(Time[0],High[0], "sell a", 30, clrRed);
      //m_CoreTrade.openPositionSell(Lot, TakeProfit, StopLoss);                               //если дошли сюда, значит позиции нет, открываем ее
     }
     
     ///
     double Main[], Signal[];
     
     CopyBuffer(MACD_1H_handle,0,0,ArraySize(Time), Main);
     CopyBuffer(MACD_1H_handle,1,0,ArraySize(Time), Signal);
     
     ArraySetAsSeries(Main, true);
     ArraySetAsSeries(Signal, true);

     
     if (Main[0] > 0 && Main[0] > Signal[0]) {
      createObjectOnChart(Time[0],High[0], "buy m", 20, clrGreen);
     }
     
     if (Main[0] < 0 && Main[0] < Signal[0]) {
      createObjectOnChart(Time[0],High[0], "sell m", 20, clrRed);
     }
     

     
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
   Trailing.Refresh();
  }
  
  
 string createObjectOnChart(datetime time, double high, string text, int highAdded, int Color) {
   int size = ArraySize(ObjectsName) - 1;
   if (size < 0) {size = 0;}
   
   ArrayResize(ObjectsName, size + 1);
   ArrayResize(ObjectsTime, size + 1);
   ArrayResize(ObjectsHigh, size + 1);
   
   string name = "created_" + TimeToString(time) + DoubleToString(high);
      ObjectsName[size] = name; 
      ObjectsTime[size] = time;
      ObjectsHigh[size] = high + highAdded; 
   
   
   high = high + Point()*highAdded;
   
   if(ObjectFind(ChartID(), name)==-1)                           // Если объекта нет..
      {
       ObjectCreate (ChartID(), name,OBJ_TEXT, 0,time ,high);            // Создаём новый объект
       ObjectSetString(ChartID(),name,OBJPROP_TEXT, text);
       ObjectSetInteger(ChartID(),name,OBJPROP_COLOR, Color);
      }
    ChartRedraw(ChartID()); 
   return name;
 }