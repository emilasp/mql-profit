//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
//---
#include <Trade\Trade.mqh>
class CEmTrade : public CTrade
  {
public:
   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
  };



CEmTrade      trade;
//---
struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };
//---
input int SL=100; // Уровень стоп лосса [пункты]
input int TP=100; // Уровень тейк профит [пункты]
input int quant=1;
//---
struct QW
  {
   int               q1;
   int               q2;
   int               q3;
  };
//---
int h[4];
ENUM_TIMEFRAMES tf[4]= {PERIOD_H4,PERIOD_H1,PERIOD_M15,PERIOD_M1};
double RSI[4];
//---
bool on_trade=false;
int n=0;
int qq[]=
  {
   111,
   211,
   212,
   213,
   221,
   233,
   234,
   244,
   321,
   322,
   333,
   334,
   342,
   344,
   432,
   433,
   442,
   443,
   444
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Quant(double rsi1, double rsi2, double rsi3, double rsi4)
  {
   int res=0;
   double q=MathFloor(rsi4/25);// 0-3
   int A=(int)q;
   int B=A<<2;
   return res=0;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Quant(double &rsi[])
  {
   int res=0;
   for(int i=0; i<3; i++)
     {
      res+=(int)MathFloor(rsi[i]/25);
      res=res<<2;
     }
   res+=(int)MathFloor(rsi[3]/25);
   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
QW Quant(double q)
  {
   QW res= {-1,-1,-1};
   res.q3 =(int)MathFloor(q/100) * 100;
   res.q2 =(int)MathFloor((q-res.q3)/10) * 10;
   res.q1 =(int)(q-res.q3-res.q2);
   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MqlDateTime Quant(int Q)
  {
   MqlDateTime res;
//double m=Q*15;
//double w=MathFloor(m/24/60)+1;
//double h=MathFloor((m-(w-1)*24*60)/60);
//double min=MathFloor((m-(w-1)*24*60-h*60));
//res.day_of_week=(int)w;
//res.hour=(int)h;
//res.min=(int)min;
   return res;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;
   double H[];
   ArraySetAsSeries(H,true);
   CopyHigh(_Symbol,_Period,0,2,H);
   double L[];
   ArraySetAsSeries(L,true);
   CopyLow(_Symbol,_Period,0,2,L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);

//--- BUY
   if(L[1]>last_tick.bid)
      //if(H[1]<last_tick.bid)
     {
      res.Buy=true;
     }
//--- SELL
   if(H[1]<last_tick.bid)
      //if(L[1]>last_tick.bid)
     {
      res.Sell=true;
     }
   return res;
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   for(int i=0; i<4; i++)
      h[i]=iRSI(_Symbol,tf[i],7,PRICE_CLOSE);
//---
   n=ArraySize(qq);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);
   MqlDateTime time;
   TimeToStruct(last_tick.time,time);

   on_trade=false;
//int q=Quant(last_tick.time);
//if(q==quant)
//   on_trade=true;
   for(int i=0; i<4; i++)
     {
      double IND[];
      ArraySetAsSeries(IND,true);
      CopyBuffer(h[i],0,0,1,IND);
      RSI[i]=IND[0];
     }

   int q=Quant(RSI);
   for(int i=0; i<n; i++)
      //if(q==qq[i])
      if(q==quant)
        {
         //Print(q);
         on_trade=true;
         break;
        }

   if(on_trade)
     {
      //---
      sSignal signal=Buy_or_Sell();
      if(signal.Buy)
        {
         //--- покупаем
         if(!PositionSelect(_Symbol))
           {
            trade.emBuy();
           }
        }
      if(signal.Sell)
        {
         //--- продаём
         if(!PositionSelect(_Symbol))
           {
            trade.emSell();
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
  }
//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double price_SL=price-sl*_Point;
   double price_TP=price+tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double price_SL=price+sl*_Point;
   double price_TP=price-tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
