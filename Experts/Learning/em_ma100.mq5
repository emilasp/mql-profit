//+------------------------------------------------------------------+
//|                                                     em_ma100.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
//---
#include <Trade\Trade.mqh>
class CEmTrade : public CTrade
  {
public:
   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              OrderMove(const ulong ticket,const double price,const double sl,const double tp);
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
  };



CEmTrade      trade;
//---
struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };
input int period_MA=100;
input int dist_MA=100;
input int limit_MA=50;
input int SL=100;
input int TP=100;
//---
int h;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);

   double MA[];
   ArraySetAsSeries(MA,true);
   CopyBuffer(h,0,0,1,MA);
   double up=MA[0]+dist_MA*_Point;
   double dn=MA[0]-dist_MA*_Point;
//--- BUY
   if(last_tick.bid<dn)
     {
      res.Buy=true;
     }
//--- SELL
   if(last_tick.bid>up)
     {
      res.Sell=true;
     }
   return res;
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   h=iMA(_Symbol,_Period,period_MA,0,MODE_SMA,PRICE_CLOSE);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(!PositionSelect(_Symbol))
      if(CheckStop())
        {
         sSignal signal=Buy_or_Sell();
         MqlTick last_tick;
         SymbolInfoTick(_Symbol,last_tick);
         //--- BUY
         if(signal.Buy)
           {
            double price=last_tick.bid+limit_MA*_Point;
            double price_SL=price-SL*_Point;
            double price_TP=price+TP*_Point;
            trade.BuyStop(0.01,price,_Symbol,price_SL,price_TP);
           }
         //--- SELL
         if(signal.Sell)
           {
            double price=last_tick.bid-limit_MA*_Point;
            double price_SL=price+SL*_Point;
            double price_TP=price-TP*_Point;
            trade.SellStop(0.01,price,_Symbol,price_SL,price_TP);
           }
        }
//--- подтягивание ордеров за ценой
   if(!PositionSelect(_Symbol))
      if(!CheckStop())
        {
         //--- перемещаем ордера
         ulong ticketOrder;
         uint  total=OrdersTotal();
         if(total>0)
           {
            for(uint i=0; i<total; i++)
              {
               ticketOrder=OrderGetTicket(i);
               if(ticketOrder>0)
                  if(OrderSelect(ticketOrder))
                     if(OrderGetString(ORDER_SYMBOL)==_Symbol)
                        if(limit_MA>=SymbolInfoInteger(_Symbol,SYMBOL_TRADE_STOPS_LEVEL))
                           if(limit_MA>=SymbolInfoInteger(_Symbol,SYMBOL_TRADE_FREEZE_LEVEL))
                             {
                              MqlTick last_tick;
                              SymbolInfoTick(_Symbol,last_tick);
                              //--- BUY
                              if(OrderGetInteger(ORDER_TYPE)==ORDER_TYPE_BUY_STOP)
                                {
                                 double price=NormalizeDouble(last_tick.bid+limit_MA*_Point,_Digits);
                                 if(OrderGetDouble(ORDER_PRICE_OPEN)>price)
                                   {
                                    double price_SL=price-SL*_Point;
                                    double price_TP=price+TP*_Point;
                                    trade.OrderMove(ticketOrder,price,price_SL,price_TP);
                                   }
                                }
                              //--- SELL
                              if(OrderGetInteger(ORDER_TYPE)==ORDER_TYPE_SELL_STOP)
                                {
                                 double price=NormalizeDouble(last_tick.bid-limit_MA*_Point,_Digits);
                                 if(OrderGetDouble(ORDER_PRICE_OPEN)<price)
                                   {
                                    double price_SL=price+SL*_Point;
                                    double price_TP=price-TP*_Point;
                                    trade.OrderMove(ticketOrder,price,price_SL,price_TP);
                                   }
                                }
                             }
              }
           }
        }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CheckStop()
  {
   bool res=false;
   ulong ticketOrder;
   uint  total=OrdersTotal();
   int count=0;
   if(total>0)
     {
      for(uint i=0; i<total; i++)
        {
         ticketOrder=OrderGetTicket(i);
         if(ticketOrder>0)
            if(OrderSelect(ticketOrder))
               if(OrderGetString(ORDER_SYMBOL)==_Symbol)
                 {
                  if(OrderGetInteger(ORDER_TYPE)==ORDER_TYPE_BUY_STOP)
                     count++;
                  if(OrderGetInteger(ORDER_TYPE)==ORDER_TYPE_SELL_STOP)
                     count++;
                 }
        }
      if(count==0)
        {
         res=true;
        }
     }
   else
     {
      res=true;
     }

   return(res);
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double price_SL=price-sl*_Point;
   double price_TP=price+tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double price_SL=price+sl*_Point;
   double price_TP=price-tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Modify specified pending order                                   |
//+------------------------------------------------------------------+
bool CEmTrade::OrderMove(const ulong ticket,const double price,const double sl,const double tp)
  {
//--- clean
   ClearStructures();
//--- setting request
   m_request.action      =TRADE_ACTION_MODIFY;
   m_request.order       =ticket;
   m_request.price       =price;
   m_request.sl          =sl;
   m_request.tp          =tp;
//--- action and return the result
   return(OrderSend(m_request,m_result));
  }
//+------------------------------------------------------------------+
