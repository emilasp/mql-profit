//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input int SL=100; // Level stopLoss [pt]
input int TP=100; //  Level taleProfit [pt]




string line_SL_name = "lineSL";
string line_TP_name = "lineTP";

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double H[];
   ArraySetAsSeries(H, true);// Indexation from right to left
   CopyHigh(_Symbol, PERIOD_CURRENT, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);// Indexation from right to left
   CopyLow(_Symbol, PERIOD_CURRENT, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
//if(H[1] < last_tick.bid)
   if(L[1] > last_tick.bid)
     {
      res.Buy = true;
     }

//--- SELL
   if(H[1] < last_tick.bid)
      //if(L[1] > last_tick.bid)
     {
      res.Sell = true;
     }

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
// Create Lines TP and SL
   moveHorizontalLine(line_SL_name, 0, true);
   moveHorizontalLine(line_TP_name, 0, true);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
  }





//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   sSignal signal = Buy_or_Sell();

   if(signal.Buy == true)
     {
      if(!PositionSelect(_Symbol))
        {
         if(trade.Buy(0.01))
           {
            if(SL>0)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price - SL * _Point;

               moveHorizontalLine(line_SL_name, 0);
              }
            if(TP>0)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price + TP * _Point;

               moveHorizontalLine(line_TP_name, 0);
              }
           }
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(trade.PositionClose(_Symbol))
              {
               moveHorizontalLine(line_SL_name, 0);
               moveHorizontalLine(line_TP_name, 0);
              }

            if(trade.Buy(0.01))
              {
               if(SL>0)
                 {
                  double price = PositionGetDouble(POSITION_PRICE_OPEN);
                  price = price - SL * _Point;

                  moveHorizontalLine(line_SL_name, price);
                 }
               if(TP>0)
                 {
                  double price = PositionGetDouble(POSITION_PRICE_OPEN);
                  price = price + TP * _Point;

                  moveHorizontalLine(line_TP_name, price);
                 }
              }

            //trade.emBuy();
           }

        }
     }
   if(signal.Sell == true)
     {
      if(!PositionSelect(_Symbol))
        {
         bool res = trade.Sell(0.01);
         if(res)
           {
            if(SL>0)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price + SL * _Point;

               moveHorizontalLine(line_SL_name, price);
              }
            if(TP>0)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price - TP * _Point;

               moveHorizontalLine(line_TP_name, price);
              }
           }
         //trade.emBuy();
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
            bool res = trade.PositionClose(_Symbol);
            if(res)
              {
               moveHorizontalLine(line_SL_name, 0);
               moveHorizontalLine(line_TP_name, 0);
              }


            res = trade.Sell(0.01);
            if(res)
              {
               if(SL>0)
                 {
                  double price = PositionGetDouble(POSITION_PRICE_OPEN);
                  price = price + SL * _Point;

                  moveHorizontalLine(line_SL_name, price);
                 }
               if(TP>0)
                 {
                  double price = PositionGetDouble(POSITION_PRICE_OPEN);
                  price = price - TP * _Point;

                  moveHorizontalLine(line_TP_name, price);
                 }
              }
            //trade.emBuy();
           }

        }
     }

//--- virtual SL|TP


   if(PositionSelect(_Symbol))
     {
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      //--- Buy
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
        {
         //--- SL
         string name="SL";
         double price = ObjectGetDouble(0,name,OBJPROP_PRICE);
         if(price > 0)
            if(last_tick.bid < price)
              {
               bool res = trade.PositionClose(_Symbol);
               if(res)
                 {
                  moveHorizontalLine(line_SL_name, 0);
                  moveHorizontalLine(line_TP_name, 0);
                 }
              }
         //--- TP
         name="TP";
         price = ObjectGetDouble(0,name,OBJPROP_PRICE);
         if(price > 0)
            if(last_tick.bid > price)
              {
               bool res = trade.PositionClose(_Symbol);
               if(res)
                 {
                  moveHorizontalLine(line_SL_name, 0);
                  moveHorizontalLine(line_TP_name, 0);
                 }
              }

        }

      //--- Sell
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
        {
         //--- SL
         string name="SL";
         double price = ObjectGetDouble(0,name,OBJPROP_PRICE);
         if(price > 0)
            if(last_tick.ask > price)
              {
               bool res = trade.PositionClose(_Symbol);
               if(res)
                 {
                  moveHorizontalLine(line_SL_name, 0);
                  moveHorizontalLine(line_TP_name, 0);
                 }
              }
         //--- TP
         name="TP";
         price = ObjectGetDouble(0,name,OBJPROP_PRICE);
         if(price > 0)
            if(last_tick.ask < price)
              {
               bool res = trade.PositionClose(_Symbol);
               if(res)
                 {
                  moveHorizontalLine(line_SL_name, 0);
                  moveHorizontalLine(line_TP_name, 0);
                 }
              }

        }

     }
//---

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {

  }

//+------------------------------------------------------------------+
void moveHorizontalLine(string name, double price, bool isCreate = false)
  {
   if(ObjectFind(0, name) == -1)
     {
      ObjectCreate(0,name,OBJ_HLINE,0,0,0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
     }
   else
     {
      ObjectSetDouble(0,name,OBJPROP_PRICE,price);
     }
  }

//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
