//+------------------------------------------------------------------+
//|                                                      em_grid.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp/Learning/emGrid.mqh>

input int      h = 50;     // up_h pt
input int      dist=100;   // up_dist pt
input double   k_grid=1.0;    //Соотношение шага верхней и инижней сетки
input bool     on_trend=false;// For Trend

input double   minlot = 0.01; // лот на одну сделку
input double   maxlot=0.1;    // максимально разрешенный объем позиции
input double   klot=1;        // мартингейл

input double   TP=10.0;       // В валюте депозита
input double   SL=100;        // В валюте депозита


//input int      up_n = 5;
//input int      dn_n=3;
//input int      dn_h=75;// dn_h pt
//input int      dn_dist=50;// dn_dist pt



emGrid grid;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell(ENUM_TIMEFRAMES period = PERIOD_H1)
  {
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);

   double H[];
   ArraySetAsSeries(H, true);
   CopyHigh(_Symbol,period,0,2,H);

   double L[];
   ArraySetAsSeries(L, true);
   CopyLow(_Symbol,period,0,2,L);
//--- BUY
   if(last_tick.bid<L[1])
     {
      res.Buy=true;
     }
//--- SELL
   if(last_tick.bid>H[1])
     {
      res.Sell=true;
     }
   return res;
  }

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   grid.InitGrid(h,  dist,  k_grid,  on_trend);
   grid.InitLot(minlot,  maxlot,  klot);
   grid.Init(SL, TP);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   grid.Tick(Buy_or_Sell(PERIOD_M30));
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
