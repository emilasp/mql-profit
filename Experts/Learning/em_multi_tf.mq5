//+------------------------------------------------------------------+
//|                                                     em_ma100.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
//---
#include <Trade\Trade.mqh>
class CEmTrade : public CTrade
  {
public:
   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              OrderMove(const ulong ticket,const double price,const double sl,const double tp);
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
  };



CEmTrade      trade;
//---
struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };

input int SL=100;
input int TP=100;
//---
int h;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double B[];
   ArraySetAsSeries(B,true); 
   CopyBuffer(h,0,0,1,B);
   double S[];
   ArraySetAsSeries(S,true);
   CopyBuffer(h,1,0,1,S);

//--- BUY
   if(B[0] < S[0])
     {
      res.Buy=true;
     }
//--- SELL
   if(B[0] > S[0])
     {
      res.Sell=true;
     }
   return res;
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   h=iCustom(_Symbol,_Period,"Learning/em_multy_tframe_v1");
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(!PositionSelect(_Symbol))
     {
      sSignal signal=Buy_or_Sell();

      //--- BUY
      if(signal.Buy)
         trade.emBuy(0.01,SL,TP);
      //--- SELL
      if(signal.Sell)
         trade.emSell(0.01,SL,TP);
     }
  }

//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double price_SL=price-sl*_Point;
   double price_TP=price+tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double price_SL=price+sl*_Point;
   double price_TP=price-tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }

//+------------------------------------------------------------------+
