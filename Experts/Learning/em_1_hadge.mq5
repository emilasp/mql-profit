//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };

CEmTrade trade;

input ulong mag=112358;

input int SL=100;
input int TP=100;
input int TR=50;
input int pos=5; // max count orders/positions

datetime prevbar=0;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double H[];
   ArraySetAsSeries(H, true);// Indexation from right to left
   CopyHigh(_Symbol, PERIOD_H1, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);// Indexation from right to left
   CopyLow(_Symbol, PERIOD_H1, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
   if(H[1] < last_tick.bid)
      //if(L[1] > last_tick.bid)
     {
      res.Buy = true;
     }

//--- SELL
//if(H[1] < last_tick.bid)
   if(L[1] > last_tick.bid)
     {
      res.Sell = true;
     }

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   trade.SetExpertMagicNumber(mag);


//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   int total=PositionsTotal(); // all positions
   int count=0;
   for(int i=0; i<total; i++)
     {
      ulong ticket=PositionGetTicket(i);
      if(PositionSelectByTicket(ticket))
         if(PositionGetInteger(POSITION_MAGIC)==mag)
            if(PositionGetString(POSITION_SYMBOL) == _Symbol)
              {
               if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                  count++;
               if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                  count++;
              }
     }


   sSignal signal = Buy_or_Sell();
   if(signal.Buy || signal.Sell)
      if(NewBar(_Period))
        {
         if(signal.Buy == true)
           {
            //--- buy

            //////////////////// DELETE ALL!!! Точно удаляются позиции! //////////////////////////
            ulong tick[];
            total=PositionsTotal();
            ArrayResize(tick, total);
            int count = 0;
            for(int i=0; i<total; i++)
              {
               ulong ticket=PositionGetTicket(i);
               if(PositionSelectByTicket(ticket))
                  if(PositionGetInteger(POSITION_MAGIC)==mag)
                     if(PositionGetString(POSITION_SYMBOL) == _Symbol)
                       {
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                          {
                           tick[count]=ticket;
                           count++;
                          }
                       }
              }

            if(count > 0)
               ArrayResize(tick, count);

            for(int i=0; i<count; i++)
               trade.PositionClose(tick[i]);

            ///////////////////////////////////////////////////////////////

            total=PositionsTotal(); // all positions
            count=0;
            for(int i=0; i<total; i++)
              {
               ulong ticket=PositionGetTicket(i);
               if(PositionSelectByTicket(ticket))
                  if(PositionGetInteger(POSITION_MAGIC)==mag)
                     if(PositionGetString(POSITION_SYMBOL) == _Symbol)
                       {
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                           count++;
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                           count++;
                       }
              }

            if(count<pos)
              {
               trade.emBuy(0.1,SL,TP);
              }
           }
         if(signal.Sell == true)
           {
            //-- sell
            ulong tick[];
            total=PositionsTotal();
            ArrayResize(tick, total);
            int count = 0;
            for(int i=0; i<total; i++)
              {
               ulong ticket=PositionGetTicket(i);
               if(PositionSelectByTicket(ticket))
                  if(PositionGetInteger(POSITION_MAGIC)==mag)
                     if(PositionGetString(POSITION_SYMBOL) == _Symbol)
                       {
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                          {
                           tick[count]=ticket;
                           count++;
                          }
                       }
              }

            if(count > 0)
               ArrayResize(tick, count);

            for(int i=0; i<count; i++)
               trade.PositionClose(tick[i]);

            ///////////////////////////////////////////////////////////////

            total=PositionsTotal(); // all positions
            count=0;
            for(int i=0; i<total; i++)
              {
               ulong ticket=PositionGetTicket(i);
               if(PositionSelectByTicket(ticket))
                  if(PositionGetInteger(POSITION_MAGIC)==mag)
                     if(PositionGetString(POSITION_SYMBOL) == _Symbol)
                       {
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                           count++;
                        if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                           count++;
                       }
              }

            if(count<pos)
              {
               trade.emSell(0.1,SL,TP);
              }
           }
        }

   Check_SL_TP();
   Tral();
  }


//+------------------------------------------------------------------+
//|   TRALL                                                |
//+------------------------------------------------------------------+
bool Tral()
  {
   bool res = false;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);
   double price = PositionGetDouble(POSITION_PRICE_OPEN);


   int total=PositionsTotal();
   for(int i=0; i<total; i++)
     {
      ulong ticket=PositionGetTicket(i);
      if(PositionSelectByTicket(ticket))
         if(PositionGetInteger(POSITION_MAGIC)==mag)
            if(PositionGetString(POSITION_SYMBOL) == _Symbol)
               if(PositionGetDouble(POSITION_PROFIT) > 0)
                 {
                  double price=last_tick.bid - TR * _Point;
                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                     if(PositionGetDouble(POSITION_PRICE_OPEN) < price)
                        if(PositionGetDouble(POSITION_SL) < price)
                            trade.PositionModify(ticket, price, 0);
                  
                  price=last_tick.ask - TR * _Point;
                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                     if(PositionGetDouble(POSITION_PRICE_OPEN) > price)
                        if(PositionGetDouble(POSITION_SL) > price || PositionGetDouble(POSITION_SL) == 0)
                            trade.PositionModify(ticket, price, 0);


                 }

      //               if(PositionGetDouble(POSITION_SL) == 0 || PositionGetDouble(POSITION_TP) == 0)
      //                 {
      //                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
      //                    {
      //                     double price_SL = PositionGetDouble(POSITION_PRICE_OPEN) - SL * _Point;
      //                     double price_TP = PositionGetDouble(POSITION_PRICE_OPEN) + TP * _Point;
      //                     trade.PositionModify(ticket, price_SL, price_TP);
      //                     bool res = true;
      //                    }
      //
      //                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
      //                    {
      //                     double price_SL = PositionGetDouble(POSITION_PRICE_OPEN) + SL * _Point;
      //                     double price_TP = PositionGetDouble(POSITION_PRICE_OPEN) - TP * _Point;
      //                     trade.PositionModify(ticket, price_SL, price_TP);
      //                     bool res = true;
      //                    }
      //                 }
     }

   return res;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Check_SL_TP()
  {
   bool res = false;

   int total=PositionsTotal();
   for(int i=0; i<total; i++)
     {
      ulong ticket=PositionGetTicket(i);
      if(PositionSelectByTicket(ticket))
         if(PositionGetInteger(POSITION_MAGIC)==mag)
            if(PositionGetString(POSITION_SYMBOL) == _Symbol)
               if(PositionGetDouble(POSITION_SL) == 0 || PositionGetDouble(POSITION_TP) == 0)
                 {
                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
                    {
                     double price_SL = PositionGetDouble(POSITION_PRICE_OPEN) - SL * _Point;
                     double price_TP = PositionGetDouble(POSITION_PRICE_OPEN) + TP * _Point;
                     trade.PositionModify(ticket, price_SL, price_TP);
                     bool res = true;
                    }

                  if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
                    {
                     double price_SL = PositionGetDouble(POSITION_PRICE_OPEN) + SL * _Point;
                     double price_TP = PositionGetDouble(POSITION_PRICE_OPEN) - TP * _Point;
                     trade.PositionModify(ticket, price_SL, price_TP);
                     bool res = true;
                    }
                 }
     }

   return false;
  }

//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool NewBar(ENUM_TIMEFRAMES timeframe)
  {
   bool res = false;
   datetime T[];
   ArraySetAsSeries(T, true);
   CopyTime(_Symbol, timeframe, 0, 1, T);

   if(T[0] > prevbar)
     {
      prevbar = T[0];
      res = true;
     }
   return res;
  }
//+------------------------------------------------------------------+
