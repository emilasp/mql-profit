//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input int SL=100; // Level stopLoss [pt]
input int TP=100; //  Level taleProfit [pt]

input int HS = 1;
input int MS = 15;
input int HF = 4;
input int MF = 20;
bool on_trade=false;

double max;
double min;
datetime begin = -1;

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
   double up = ObjectGetDouble(0,"SELL",OBJPROP_PRICE);
   if(up < last_tick.bid)
     {
      res.Sell = true;
     }
   double dn = ObjectGetDouble(0,"BUY",OBJPROP_PRICE);
//--- SELL
   if(dn > last_tick.bid)
     {
      res.Buy = true;
     }

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---


//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   ObjectDelete(0, "BUY");
   ObjectDelete(0, "SELL");
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   MqlDateTime start;
   datetime res_start = TimeCurrent(start);
   if(start.hour == HS && start.min >= MS)
      begin=res_start;


   MqlDateTime finish;
   datetime end = TimeCurrent(finish);
   if(finish.hour == HF && finish.min >= MF)
     {
      double L[];
      double H[];

      CopyHigh(_Symbol,_Period,begin,end,H);
      CopyLow(_Symbol,_Period,begin,end,L);

      min=L[ArrayMinimum(L)];
      max=H[ArrayMaximum(H)];

      moveHorizontalLine("BUY",max);
      moveHorizontalLine("SELL",min);
     }
//---
   if(!on_trade)
     {
      sSignal signal = Buy_or_Sell();

      if(signal.Buy == true)
        {
         if(!PositionSelect(_Symbol))
           {
            trade.emBuy(0.01, SL);
           }
         else
           {
            if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
              {
               trade.PositionClose(_Symbol);
               trade.emBuy(0.01, SL);
              }

           }
        }
      if(signal.Sell == true)
        {
         if(!PositionSelect(_Symbol))
           {
            trade.emSell(0.01, SL);
           }
         else
           {
            if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
              {
               trade.PositionClose(_Symbol);
               trade.emSell(0.01, SL);
              }

           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---


  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void moveHorizontalLine(string name, double price)
  {
  if(ObjectFind(0, name) == -1)
     {
      ObjectCreate(0,name,OBJ_HLINE,0,0,0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
     }
   else
     {
      ObjectSetDouble(0,name,OBJPROP_PRICE,price);
     }
  }
//+------------------------------------------------------------------+
