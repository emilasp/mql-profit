//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
//---
#include <Trade\Trade.mqh>
class CEmTrade : public CTrade
  {
public:
   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
  };



CEmTrade      trade;
//---
struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };
//---
input int SL=100; // Уровень стоп лосса [пункты]
input int TP=100; // Уровень тейк профит [пункты]
input int SL0=50; // Уровень Безубытка [пункты]
input int HS=1;
input int MS=15;
input int HF=4;
input int MF=20;
bool on_trade=false;
double max,min;
datetime begin;

int      prev_day = 0;
bool     on_korobka=false;
bool     on_modify=false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);

   string name="SELL";
   double up=ObjectGetDouble(0,name,OBJPROP_PRICE);
   name="BUY";
   double dn=ObjectGetDouble(0,name,OBJPROP_PRICE);

//--- BUY
   if(last_tick.ask >= up)
     {
      res.Buy=true;
     }
//--- SELL
   if(last_tick.bid <= dn)
     {
      res.Sell=true;
     }
   return res;
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   string name="SELL";
   ObjectCreate(0,name,OBJ_HLINE,0,0,0);
   ObjectSetInteger(0,name,OBJPROP_SELECTABLE,true);
   ObjectSetInteger(0,name,OBJPROP_SELECTED,true);
   name="BUY";
   ObjectCreate(0,name,OBJ_HLINE,0,0,0);
   ObjectSetInteger(0,name,OBJPROP_SELECTABLE,true);
   ObjectSetInteger(0,name,OBJPROP_SELECTED,true);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   string name="SELL";
   ObjectDelete(0,name);
   name="BUY";
   ObjectDelete(0,name);

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   MqlDateTime start;
   datetime res_start=TimeCurrent(start);
   if(start.hour==HS && start.min>=MS)
      begin=res_start;

   if(start.day_of_year > prev_day)
     {
      prev_day = start.day_of_year;
      on_korobka = true;
     }

   if(on_korobka)
     {
      MqlDateTime finish;
      datetime end=TimeCurrent(finish);
      if(finish.hour==HF && finish.min>=MF)
        {
         double H[];
         double L[];
         CopyHigh(_Symbol,_Period,begin,end,H);
         CopyLow(_Symbol,_Period,begin,end,L);
         min=L[ArrayMinimum(L)];
         max=H[ArrayMaximum(H)];
         string name="SELL";
         ObjectSetDouble(0,name,OBJPROP_PRICE,min);
         name="BUY";
         ObjectSetDouble(0,name,OBJPROP_PRICE,max);

         on_korobka = false;
         on_trade = true;
        }
      //---
     }

   if(on_trade)
     {
      sSignal signal=Buy_or_Sell();
      if(signal.Buy)
        {
         //--- покупаем
         if(!PositionSelect(_Symbol))
           {
            trade.emBuy(0.01,SL, TP);
            on_trade = false;
            on_modify=true;
           }
        }
      if(signal.Sell)
        {
         //--- продаём
         if(!PositionSelect(_Symbol))
           {
            trade.emSell(0.01,SL, TP);
            on_trade = false;
            on_modify=true;
           }
        }
     }

//-- Безубыток
   if(SL0 > 0 && on_modify)
      if(PositionSelect(_Symbol))
         if(PositionGetDouble(POSITION_PROFIT) > 0)
           {
            MqlTick last_tick;
            SymbolInfoTick(_Symbol,last_tick);

            if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price + SL0*_Point;
               if(last_tick.bid > price)
                 {
                     trade.PositionModify(_Symbol, PositionGetDouble(POSITION_PRICE_OPEN),PositionGetDouble(POSITION_TP));
                     on_modify=false;
                 }
              }
              
              if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
              {
               double price = PositionGetDouble(POSITION_PRICE_OPEN);
               price = price - SL0*_Point;
               if(last_tick.ask < price)
                 {
                     trade.PositionModify(_Symbol, PositionGetDouble(POSITION_PRICE_OPEN),PositionGetDouble(POSITION_TP));
                     on_modify=false;
                 }
              }
           }

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
  }
//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double price_SL=price-sl*_Point;
   double price_TP=price+tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double price_SL=price+sl*_Point;
   double price_TP=price-tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
