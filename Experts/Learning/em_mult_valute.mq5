//+------------------------------------------------------------------+
//|                                                      em_mult.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Emilasp\Learning\Mult.mqh> 
#include <Emilasp\Learning\Signal.mqh> 


CMult expert;
CSignal signal;

input string valuta="EURUSD USDJPY GBPUSD"; // Валютные пары
input double profit_TP=10.;                 // Убыток в валюте депозита
input double profit_SL=10.;                 // Прибыль в валюте депозита
input double lot=0.01;                      //Лот для валютной пары
input bool   on_MM=false;                  //Выключить/включить ММ
input int    par1=90;
input int    q=0;                            //Quant

string name="em_signal_1";

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create timer
   //EventSetMillisecondTimer(100);
   
   expert.SetValuta(valuta);
   expert.SetMM(lot, on_MM);
   expert.SetSignal(name, par1);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   //EventKillTimer();
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(expert.filter(q))
      return;

   expert.CloseAll(profit_SL,profit_TP);
   expert.Trade();
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
//---
   expert.Trade();
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   
  }
//+------------------------------------------------------------------+
