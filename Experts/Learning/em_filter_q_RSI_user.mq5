//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
//---
#include <Trade\Trade.mqh>
class CEmTrade : public CTrade
{
public:
   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="");
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};
};



CEmTrade      trade;
//---
struct sSignal {
   bool              Buy;
   bool              Sell;
   void              sSignal()
   {
      Buy=false;
      Sell=false;
   }
};
//---
input int SL=100; // Уровень стоп лосса [пункты]
input int TP=100; // Уровень тейк профит [пункты]
input int quant=1;
input bool on_buy = false;
input bool on_sell = false;
//---
struct QW {
   int               q1;
   int               q2;
   int               q3;
};
//---
int h[4];
ENUM_TIMEFRAMES tf[3]= {PERIOD_H1,PERIOD_M15,PERIOD_M1};
double RSI[3];
//---
bool on_trade=false;
bool on_trade_buy=false;
bool on_trade_sell=false;

int n=0;

int n_buy=0;
int n_sell=0;

int q_buy[]= {
   437,
   503,
   445,
   438,
   502,
   446,
   366,
   365,
   508,
   439,
   374,
   300,
   427,
   373,
   375,
   302,
   291,
   347,
   229,
   301,
   294,
   299,
   228,
   367,
   425,
   284,
   283,
   226,
   359,
   276,
   239,
   238,
   230,
   227,
   293,
};
int q_sell[]= {
   303,
   302,
   446,
   444,
   436,
   508,
   287,
   295,
   447,
   373,
   286,
   310,
   309,
   301,
   423,
   501,
   279,
   293,
   429,
   222,
   230,
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Q_fibo(double rsi)
{
   int res=0;
   int sum = 1+1+2+3+5+8+13+21;
   double w = 100.0 / sum;

   int f[8]= {1,1,2,3,5,8,13,21};
   double QQ[8]= {};
   int sumf=0;
   for(int i=0; i<ArraySize(QQ); i++) {
      sumf += f[i];
      QQ[i] = sumf * w;
   }

   for(int i=0; i<8; i++)
      if(rsi < QQ[i]) {
         res = i;
         return res;
      }
   return res;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Q_Buy(double rsi)
{
   int res = Q_fibo(rsi);
   return res=0;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Q_Sell(double rsi)
{
   int res = Q_fibo(100-rsi);
   return res=0;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Quant(double rsi1, double rsi2, double rsi3, double rsi4)
{
   int res=0;
   double q=MathFloor(rsi4/25);// 0-3
   int A=(int)q;
   int B=A<<2;
   return res=0;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Q_Buy(double &rsi[])
{
   int res=0;
//int res = Q_fibo(rsi);
   for(int i=0; i<2; i++) {
      res+=Q_fibo(rsi[i]);
      res=res<<3;
   }
   res+=Q_fibo(rsi[2]);
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Q_Sell(double &rsi[])
{
   int res=0;
//int res = Q_fibo(rsi);
   for(int i=0; i<2; i++) {
      res+=Q_fibo(100.0-rsi[i]);
      res=res<<3;
   }
   res+=Q_fibo(rsi[2]);
   return res;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Quant(double &rsi[])
{
   int res=0;
   for(int i=0; i<2; i++) {
      res+=(int)MathFloor(rsi[i]/25);
      res=res<<3;
   }
   res+=(int)MathFloor(rsi[3]/25);
   return res;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
QW Quant(double q)
{
   QW res= {-1,-1,-1};
   res.q3 =(int)MathFloor(q/100) * 100;
   res.q2 =(int)MathFloor((q-res.q3)/10) * 10;
   res.q1 =(int)(q-res.q3-res.q2);
   return res;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
MqlDateTime Quant(int Q)
{
   MqlDateTime res;
//double m=Q*15;
//double w=MathFloor(m/24/60)+1;
//double h=MathFloor((m-(w-1)*24*60)/60);
//double min=MathFloor((m-(w-1)*24*60-h*60));
//res.day_of_week=(int)w;
//res.hour=(int)h;
//res.min=(int)min;
   return res;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
{
   sSignal res;
   double H[];
   ArraySetAsSeries(H,true);
   CopyHigh(_Symbol,_Period,0,2,H);
   double L[];
   ArraySetAsSeries(L,true);
   CopyLow(_Symbol,_Period,0,2,L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);

//--- BUY
   if(L[1]>last_tick.bid)
      //if(H[1]<last_tick.bid)
   {
      res.Buy=true;
   }
//--- SELL
   if(H[1]<last_tick.bid)
      //if(L[1]>last_tick.bid)
   {
      res.Sell=true;
   }
   return res;
}
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//---
   for(int i=0; i<3; i++)
      h[i]=iRSI(_Symbol,tf[i],7,PRICE_CLOSE);
//---
   n_buy=ArraySize(q_buy);
   n_sell=ArraySize(q_sell);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   MqlTick last_tick;
   SymbolInfoTick(_Symbol,last_tick);
   MqlDateTime time;
   TimeToStruct(last_tick.time,time);

   on_trade=false;
   on_trade_buy=false;
   on_trade_sell=false;
//int q=Quant(last_tick.time);
//if(q==quant)
//   on_trade=true;
   for(int i=0; i<3; i++) {
      double IND[];
      ArraySetAsSeries(IND,true);
      CopyBuffer(h[i],0,0,1,IND);
      RSI[i]=IND[0];
      double tt = IND[0];
   }

   if (on_buy) {
      int q=Q_Buy(RSI);
      for(int i=0; i<n_buy; i++)
         if(q==q_buy[i]) {
            //if(q==quant) {
            //Print(q);
            on_trade=true;
            on_trade_buy=true;
            //break;
         }
   }
   if (on_sell) {
      int q=Q_Sell(RSI);
      for(int i=0; i<n_sell; i++)
         if(q==q_sell[i]) {
            //if(q==quant) {
            //Print(q);
            on_trade=true;
            on_trade_sell=true;
            //break;
         }

   }
   if(on_trade) {
      //---
      sSignal signal=Buy_or_Sell();
      if(signal.Buy)
         if (on_buy)
            if (on_trade_buy) {
               //--- покупаем
               if(!PositionSelect(_Symbol)) {
                  trade.emBuy();
               }
            }
      if(signal.Sell)
         if (on_sell)
            if (on_trade_sell) {
               //--- продаём
               if(!PositionSelect(_Symbol)) {
                  trade.emSell();
               }
            }
   }
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
{
}
//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);
   double price_SL=price-sl*_Point;
   double price_TP=price+tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
}
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100,const string symbol=NULL,double price=0.0,const string comment="")
{
//--- check volume
   if(volume<=0.0) {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
   }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);
   double price_SL=price+sl*_Point;
   double price_TP=price-tp*_Point;
//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
