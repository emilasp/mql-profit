//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input int SL=100; // Level stopLoss [pt]
input int TP=100; //  Level taleProfit [pt]
input int TR=50;  // Level trals [pt]

bool on_tral = false;

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double H[];
   ArraySetAsSeries(H, true);// Indexation from right to left
   CopyHigh(_Symbol, PERIOD_CURRENT, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);// Indexation from right to left
   CopyLow(_Symbol, PERIOD_CURRENT, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
//if(H[1] < last_tick.bid)
   if(L[1] > last_tick.bid)
     {
      res.Buy = true;
     }

//--- SELL
   if(H[1] < last_tick.bid)
      //if(L[1] > last_tick.bid)
     {
      res.Sell = true;
     }

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---



//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {

   if(!PositionSelect(_Symbol))
      on_tral = false;

//---
   sSignal signal = Buy_or_Sell();

   if(signal.Buy == true)
     {
      if(!PositionSelect(_Symbol))
        {
         trade.Buy(0.01);
         //trade.emBuy();
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            trade.PositionClose(_Symbol);
            trade.Buy(0.01);
            //trade.emBuy();
           }

        }
     }
   if(signal.Sell == true)
     {
      if(!PositionSelect(_Symbol))
        {
         trade.Sell(0.01);
         //trade.emBuy();
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
            trade.PositionClose(_Symbol);
            trade.Sell(0.01);
            //trade.emBuy();
           }

        }
     }


//--- TRAL
   if(TR>0 && PositionSelect(_Symbol))
     {
      if(PositionGetDouble(POSITION_PROFIT))
        {
         MqlTick last_tick;
         SymbolInfoTick(_Symbol, last_tick);
         double price = PositionGetDouble(POSITION_PRICE_OPEN);


         // Buy
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            if(last_tick.bid>price+TR*_Point)
              {
               on_tral = true;
               price = last_tick.bid - TR*_Point;
               if(price > PositionGetDouble(POSITION_SL))
                 {
                  trade.PositionModify(_Symbol, price, 0);
                 }
              }
         // Sell
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            if(last_tick.ask<price-TR*_Point)
              {
               on_tral = true;
               price = last_tick.ask + TR*_Point;
               if(price < PositionGetDouble(POSITION_SL) || PositionGetDouble(POSITION_SL) == NULL)
                 {
                  trade.PositionModify(_Symbol, price, 0);
                 }
              }
            //price = price + SL * _Point;
            //trade.PositionModify(_Symbol, price, 0);
           }
        }
     }
//---

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   if(PositionSelect(_Symbol) && !on_tral)
     {
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      double spred = last_tick.ask-last_tick.bid;
      // StopLoss
      if(SL>0)
        {
         if(SL>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_STOPS_LEVEL))
           {
            if(SL>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_FREEZE_LEVEL))
              {
               if(SL*_Point > 2*spred)
                 {
                  if(PositionGetDouble(POSITION_SL) == NULL)
                    {
                     // Buy
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price - SL * _Point;
                        trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
                       }
                     // Sell
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price + SL * _Point;
                        trade.PositionModify(_Symbol, price, PositionGetDouble(POSITION_TP));
                       }
                    }

                 }
              }
           }
        }
      // TakeProfit
      if(TP>0)
        {

         if(TP>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_STOPS_LEVEL))
           {
            if(TP>SymbolInfoInteger(_Symbol, SYMBOL_TRADE_FREEZE_LEVEL))
              {
               if(TP*_Point > 2*spred)
                  if(PositionGetDouble(POSITION_TP) == NULL)
                    {
                     // Buy
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price + TP * _Point;
                        trade.PositionModify(_Symbol, PositionGetDouble(POSITION_SL), price);
                       }
                     // Sell
                     if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
                       {
                        double price = PositionGetDouble(POSITION_PRICE_OPEN);
                        price = price - TP * _Point;
                        trade.PositionModify(_Symbol, PositionGetDouble(POSITION_SL), price);
                       }
                    }
              }
           }



        }
     }

  }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
