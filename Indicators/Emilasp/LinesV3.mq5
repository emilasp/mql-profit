//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 10
#property indicator_plots   1
#property indicator_minimum 0
#property indicator_maximum 100
//--- значения горизонтальных уровней индикатора
#property indicator_level1 35
#property indicator_level2 65
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>

enum ENUM_GET_EXTREMUM_TYPES {
   GET_EXTREMUM_TYPE_RSI = 1, // RSI
   GET_EXTREMUM_TYPE_RSI_2 = 3, // RSI 2
   GET_EXTREMUM_TYPE_PIVOT = 2
};

struct ExtremumResult {
   int               type;
   int               index;
   double            price;
   bool              isBreakOut;

                     ExtremumResult()
   {
      type = 0;
      index = 0;
      price = 0;
      isBreakOut = false;
   }
};

//--- indicator buffers
double         BufferExtermumsPrice[]; // буфер со всеми ценами экстремумов
double         BufferExtermumsAllWithDirect[]; // буфер со всеми экстремумами: 0 - нет, val > 0 - есть максимум, val < 0 - есть минимум
double         BufferLineType[]; // буфер с типами линий: 0 - нет, 1 - пробойный, 2 - внутриканальный
double         PatternBuffer[]; // текущий паттерн: 0 - флет/треугольник, 1 - бычий импульс, 2 - бычья коррекция, -1 - меджвежий импульс, -2 - медвежья коррекция
double         SignalBuffer[]; // все сигналы: 0 - нет, 1 - пробой, 2 - отбой
double         VolatilyBuffer[]; // Разница между линией поддержки и сопротивления
double         TrandContinuesBuffer[]; // буфер с числом подряд пробоев: -n - пробои медвежьи, 0 - не было пробоев(был флет/треугольник), +n - пробои бычьи
double         TrandStrengthBuffer[]; // сила тренда - чем меньше средняя коррекция к среднему импульсу, тем больше сила тренда
double         BufferInd[];

datetime       date_start=D'01.11.2019';

input int senssetiveOverLevelpt=5; // Чувствительность к пробитию уровня
input int minImpulseLevelDistancePt=15; // Минимальное количество пунктов между уровнями
input ENUM_GET_EXTREMUM_TYPES type  = GET_EXTREMUM_TYPE_RSI; // Способ определения уровня
input int indicatorPeriod = 4; // период Индикатора
input int indicatorLevelTop = 65; // Зона перекупленности Индикатора
input int indicatorLevelBottom = 35; // Зона перепроданности Индикатора

input group "Colors"
input color colorLineMaxBreakOut=clrYellowGreen;
input color colorLineMax=clrLightGreen;
input color colorLineMinBreakOut=clrRed;
input color colorLineMin=clrPink;

int handle;

double lastTopLinePrice            = 0;
double lastBottomLinePrice         = 0;
double lastTopLineBreakOutPrice    = 0;
double lastBottomLineBreakOutPrice = 0;
double lastTopLinePricePrev    = 0; // Следующий
double lastBottomLinePricePrev = 0;

double lastPatternt          = 0;
double lastContinues         = 0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferInd, INDICATOR_DATA);
   SetIndexBuffer(1, BufferExtermumsPrice, INDICATOR_CALCULATIONS);
   SetIndexBuffer(2, BufferExtermumsAllWithDirect, INDICATOR_CALCULATIONS);
   SetIndexBuffer(3, BufferLineType, INDICATOR_CALCULATIONS);
   SetIndexBuffer(4, PatternBuffer, INDICATOR_CALCULATIONS);
   SetIndexBuffer(5, SignalBuffer, INDICATOR_CALCULATIONS);
   SetIndexBuffer(6, VolatilyBuffer, INDICATOR_CALCULATIONS);
   SetIndexBuffer(7, TrandContinuesBuffer, INDICATOR_CALCULATIONS);
   SetIndexBuffer(8, TrandStrengthBuffer, INDICATOR_CALCULATIONS);


   switch(type) {
   case  GET_EXTREMUM_TYPE_RSI:
   //case  GET_EXTREMUM_TYPE_RSI_2:
      handle = iRSI(_Symbol, PERIOD_CURRENT, indicatorPeriod, PRICE_CLOSE);
      break;
   default:
      break;
   }

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         BufferExtermumsPrice[i]         = 0;
         BufferExtermumsAllWithDirect[i] = 0;
         BufferLineType[i]               = 0; // буфер с типами линий: 0 - нет, 1 - пробойный, 2 - внутриканальный

         TrandContinuesBuffer[i] = lastContinues;
         TrandStrengthBuffer[i]  = 0;

         // Lines Extremums
         ExtremumResult result = getExtremumResult(type, i, open, close, high, low);

         if(result.type != 0) {
            if(result.type == 1) {
               BufferExtermumsAllWithDirect[i] = result.price;

               if(result.isBreakOut) {
                  lastPatternt = 2;
                  TrandContinuesBuffer[i] = getContinuesCount(i, true);
                  BufferLineType[i] = 1;

                  DrawHelper::moveHorizontalLine("Ind:Line:Top:max", result.price, colorLineMaxBreakOut, STYLE_DASH);
                  DrawHelper::moveHorizontalLine("Ind:Line:Top:max:channel", 0, colorLineMax, STYLE_DASHDOT);
                  //DrawHelper::moveHorizontalLine("Ind:Line:Top:max2", lastTopLinePricePrev, clrOrangeRed, STYLE_DASHDOTDOT);
                  DrawHelper::drawArrow("Ind:Line:Top:max:arrow" + IntegerToString(i), time[i], result.price + 20 * _Point, 159, clrGreenYellow);
               } else {
                  BufferLineType[i] = 2;

                  DrawHelper::moveHorizontalLine("Ind:Line:Top:max:channel", result.price, colorLineMax, STYLE_DASHDOT);
                  DrawHelper::drawArrow("Ind:Line:Top:max:arrow" + IntegerToString(i), time[i], result.price + 20 * _Point, 159, clrGreenYellow);
               }

            }
            if(result.type == -1) {
               BufferExtermumsAllWithDirect[i] = result.price * -1;

               if(result.isBreakOut) {
                  lastPatternt = -2;
                  TrandContinuesBuffer[i] = getContinuesCount(i, false);
                  BufferLineType[i] = 1;

                  DrawHelper::moveHorizontalLine("Ind:Line:Top:min", result.price, colorLineMinBreakOut, STYLE_DASH);
                  DrawHelper::moveHorizontalLine("Ind:Line:Top:min:channel", 0, colorLineMin, STYLE_DASHDOT);
                  //DrawHelper::moveHorizontalLine("Ind:Line:Top:min2", lastBottomLinePricePrev, clrOrangeRed, STYLE_DASHDOTDOT);

                  DrawHelper::drawArrow("Ind:Line:Top:min:arrow" + IntegerToString(i), time[i], result.price - 20 * _Point, 159, clrCoral);
               } else {
                  BufferLineType[i] = 2;

                  DrawHelper::moveHorizontalLine("Ind:Line:Top:min:channel", result.price, colorLineMin, STYLE_DASHDOT);
                  DrawHelper::drawArrow("Ind:Line:Top:min:arrow" + IntegerToString(i), time[i], result.price - 20 * _Point, 159, clrCoral);
               }
            }
         }

         if(BufferLineType[i] > 0) {
            TrandStrengthBuffer[i] = calculateStrengthLine(i, result, high, low, time);
         }


         // Смена тенденции:
         // Long:
         // - цена опустилась ниже последнего минимума
         // - послдений максимум ниже предыдущего

         // Signals
         SignalBuffer[i] = 0;
         /** Простой пробой **/
         bool isLineTopIntersec = lastTopLinePrice > 0 && lastTopLinePrice > open[i] && lastTopLinePrice < close[i];
         bool isLineTopBreakOutIntersec = lastTopLineBreakOutPrice > 0 && lastTopLineBreakOutPrice > open[i] && lastTopLineBreakOutPrice < close[i];
         if(isLineTopIntersec || isLineTopBreakOutIntersec) {
            SignalBuffer[i] = 1;
            lastPatternt = 1;

            //DrawHelper::drawArrow("Upper", time[i], high[i] + 20 * _Point, 159, clrWhite);
         }
         bool isLineBottomIntersec = lastBottomLinePrice > 0 && lastBottomLinePrice < open[i] && lastBottomLinePrice > close[i];
         bool isLineBottomBreakOutIntersec = lastBottomLineBreakOutPrice > 0 && lastBottomLineBreakOutPrice < open[i] && lastBottomLineBreakOutPrice > close[i];
         if(isLineBottomIntersec || isLineBottomBreakOutIntersec) {
            SignalBuffer[i] = -1;
            lastPatternt = -1;

            //DrawHelper::drawArrow("Dwnr", time[i], low[i] - 10 * _Point, 159, clrWhite);
         }

         /** Были k свечей у уровня - ожидаем пробой мощный! **/

         PatternBuffer[i] = lastPatternt;
         VolatilyBuffer[i] = (lastTopLinePrice - lastBottomLinePrice) / _Point;

         // Draw info
         drawInfo(i, time, high, low);
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calculateStrengthLine(int index, ExtremumResult &result, const double &high[], const double &low[], const datetime &time[], int count = 100)
{
   double res = 0;

   int sensetivePt = 5;

   int countTouch = 0;

   int countExtrememBars = ArraySize(BufferExtermumsAllWithDirect);
   for(int i=countExtrememBars-1; i>countExtrememBars-count; i--) {
      //if (i >= countExtrememBars) {
      if (BufferExtermumsAllWithDirect[i] > 0) {
         double hMax = high[i] + sensetivePt * _Point;
         double hMin = high[i] - sensetivePt * _Point;

         if (hMin < result.price && hMax < result.price) {
            countTouch++;
            //DrawHelper::drawArrow("TouchMax:" + IntegerToString(i), time[i], high[i] + 40 * _Point, 159, clrCoral);
         }
      }
      if (BufferExtermumsAllWithDirect[i] < 0) {
         double lMax = low[i] + sensetivePt * _Point;
         double lMin = low[i] - sensetivePt * _Point;
         if (lMin < result.price && lMax < result.price) {
            countTouch++;
            //DrawHelper::drawArrow("TouchMin:" + IntegerToString(i), time[i], low[i] - 40 * _Point, 159, clrCoral);
         }
      }
      //}
   }


   /*for(int i=index;i>index-count;i--) {
      double hMax = high[i] + sensetivePt * _Point;
      double hMin = high[i] - sensetivePt * _Point;
      double lMax = low[i] + sensetivePt * _Point;
      double lMin = low[i] - sensetivePt * _Point;


      if (hMin < result.price && hMax < result.price) {
       countTouch++;
       DrawHelper::drawArrow("TouchMax:" + IntegerToString(i), time[i], high[i] + 40 * _Point, 159, clrCoral);
      }
      if (lMin < result.price && lMax < result.price) {
        countTouch++;
        DrawHelper::drawArrow("TouchMin:" + IntegerToString(i), time[i], low[i] - 40 * _Point, 159, clrCoral);
      }
   }*/

   return res;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getContinuesCount(int currentIndex, bool isMaximum)
{
   if(isMaximum) {
      if(lastContinues >= 0) {
         lastContinues++;
      } else {
         lastContinues = 1;
      }
   } else {
      if(lastContinues <= 0) {
         lastContinues--;
      } else {
         lastContinues = -1;
      }
   }
   return lastContinues;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void  drawInfo(int i, const datetime &time[], const double &high[], const double &low[])
{
   DrawHelper::drawLabel("Pattern", "PatternInd: " + getPatternText(PatternBuffer[i]), 10, 30);
   DrawHelper::drawLabel("Continues", "Continues: " + IntegerToString(TrandContinuesBuffer[i]), 10, 50);
   DrawHelper::drawLabel("Volatily", "Volatily: " + IntegerToString(VolatilyBuffer[i], 1), 10, 70);

   DrawHelper::drawLabel("info", "TYPE: " + IntegerToString(type) + ", period: " + IntegerToString(indicatorPeriod) + ", Strength: " + DoubleToString(TrandStrengthBuffer[i]), 10, 70, 1);
}




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExtremumResult getExtremumResultByRsi(int i, const double &open[], const double &close[], const double &high[], const double &low[])
{
   ExtremumResult result;

   double BufferIndTmp[];
   CopyBuffer(handle, 0, 0, 1, BufferIndTmp);
   BufferInd[i] = BufferIndTmp[0];

   if(BufferInd[i-1] > indicatorLevelTop && BufferInd[i] < indicatorLevelTop) {
      result = getExtremumResultBase(1, i, open, close, high, low);
   }

   if(BufferInd[i-1] < indicatorLevelBottom && BufferInd[i] > indicatorLevelBottom) {
      result = getExtremumResultBase(-1, i, open, close, high, low);
   }

   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExtremumResult getExtremumResultByRsi2(int i, const double &open[], const double &close[], const double &high[], const double &low[])
{
   ExtremumResult result;

   double BufferIndTmp[];
   CopyBuffer(handle, 0, 0, 1, BufferIndTmp);
   BufferInd[i] = BufferIndTmp[0];

   if (i > 4) {
      double val1 = BufferInd[i];
      double val2 = BufferInd[i-1];
      double val3 = BufferInd[i-2];
      double val4 = BufferInd[i-3];
      
      if(BufferInd[i-1] > indicatorLevelTop && BufferInd[i] < indicatorLevelTop) {
         result = getExtremumResultBase(1, i, open, close, high, low);
      }

      if(BufferInd[i-1] < indicatorLevelBottom && BufferInd[i] > indicatorLevelBottom) {
         result = getExtremumResultBase(-1, i, open, close, high, low);
      }
   }

   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExtremumResult getExtremumResultByPivot(int i, const double &open[], const double &close[], const double &high[], const double &low[])
{
   ExtremumResult result;

//int count = ArraySize(open);

   if(i >= 2) {
      if (high[i-2] < high[i-1] && high[i-1] < high[i]) {
         result = getExtremumResultBase(1, i, open, close, high, low);
      }
      if (low[i-2] > low[i-1] && low[i-1] > low[i]) {
         result = getExtremumResultBase(-1, i, open, close, high, low);
      }
   }

   return result;
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExtremumResult getExtremumResultBase(int type, int i, const double &open[], const double &close[], const double &high[], const double &low[])
{
   ExtremumResult result;

   if(type == 1) {
      double newPrice = getExtremumForPeriod(1, i, 10, high, low);
      double distanceMax = lastTopLinePrice - newPrice;

      if(newPrice > lastTopLinePrice) {
         if(distanceMax < minImpulseLevelDistancePt * _Point * -1) { // 0
            result.index = i;
            result.type = 1;
            result.price = newPrice;
            result.isBreakOut = true;
            lastTopLinePrice = result.price;
            lastTopLineBreakOutPrice = result.price;
         }
      } else {
         if(distanceMax > minImpulseLevelDistancePt * _Point) { // 0
            result.index = i;
            result.type = 1;
            result.price = newPrice;
            result.isBreakOut = false;

            lastTopLinePricePrev = lastTopLinePrice;
            lastTopLinePrice = result.price;
         }
      }
   }

   if(type == -1) {
      double newPrice = getExtremumForPeriod(-1, i, 10, high, low);
      double distanceMin = newPrice - lastBottomLinePrice;

      if(newPrice < lastBottomLinePrice) {
         if(distanceMin < minImpulseLevelDistancePt * _Point * -1) { // 0
            result.index = i;
            result.type = -1;
            result.price = newPrice;
            result.isBreakOut = true;

            lastBottomLinePrice = newPrice;
            lastBottomLinePricePrev = lastBottomLinePrice;
            lastBottomLineBreakOutPrice = result.price;
         }
      } else {
         if(distanceMin > minImpulseLevelDistancePt * _Point) { // 0
            result.index = i;
            result.type = -1;
            result.price = newPrice;
            result.isBreakOut = false;
            lastBottomLinePrice = newPrice;
         }
      }
   }

   return result;
}


/**
* Поулчаем Экстремум на дистанции
*/
double getExtremumForPeriod(int type, int indexStart, int count, const double &high[], const double &low[])
{
   double max = NULL;
   double min = NULL;
   for(int i=indexStart - count; i<indexStart; i++) {
      if(max == NULL || max < high[i])
         max = high[i];
      if(min == NULL || min > low[i])
         min = low[i];
   }
   if(type == 1)
      return max;
   return min;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ExtremumResult getExtremumResult(ENUM_GET_EXTREMUM_TYPES type, int i, const double &open[], const double &close[], const double &high[], const double &low[])
{
   ExtremumResult result;
   switch(type) {
   case  GET_EXTREMUM_TYPE_RSI:
      result = getExtremumResultByRsi(i, open, close, high, low);
      break;
   case  GET_EXTREMUM_TYPE_RSI_2:
      result = getExtremumResultByRsi2(i, open, close, high, low);
      break;
   case  GET_EXTREMUM_TYPE_PIVOT:
      result = getExtremumResultByPivot(i, open, close, high, low);
      break;
   default:
      break;
   }
   return result;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPatternText(int patternVal)
{
   switch(patternVal) {
   case 1:
      return "Bull impulse";
   case 2:
      return "Bull correction";
   case -1:
      return "Bear impulse";
   case -2:
      return "Bear correction";
   }
   return "FLET";
}
//+------------------------------------------------------------------+
