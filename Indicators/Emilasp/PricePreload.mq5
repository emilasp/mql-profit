//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
#property indicator_maximum 10
#property indicator_minimum -10
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>

input group "Colors"


//--- indicator buffers
double         BufferSignal[];
double         BufferRatio[];

datetime       date_start=D'01.11.2019';

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferRatio, INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         BufferSignal[i] = 0;
         BufferRatio[i] = 0;

         bool isLong = open[i] > close[i];

         double body = MathAbs(open[i] - close[i]);
         double shadowTop = isLong ? high[i] - close[i] : close[i] - high[i];
         double shadowBottom = isLong ? close[i] - high[i] : high[i] - close[i];
         double aShadow = shadowTop - shadowBottom;

         if (body > 0) {
            double signal = aShadow / body;

            BufferRatio[i] = signal;
         }


         int total = 5;
         double sum = 0;
         double count = 0;
         for(int k=0; k<total; k++) {
            if (BufferRatio[i-k] > 0) {
               sum += BufferRatio[i-k];
               count++;
            }
         }
         if (count > 0) {
            BufferSignal[i] = sum / count;
         }
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
