//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window //indicator_chart_window|indicator_separate_window
#property indicator_buffers 2
#property indicator_plots   0

#include <Emilasp\helpers\DrawHelper.mqh>

double ExtremumsBuffer[]; // все сигналы экстремумы: 0 - нет, 1 - максимум, -1 - минимум
double SignalBuffer[]; // все сигналы: 0 - нет, 1 - бычья дивергенция, -1 - медвежья

datetime date_start=D'01.11.2020';

input group "Основное"
input int barsAnalizeCount=200;   // Количество баров для поиска канала
input int sensitiveLevelPt=5;    // Чувствительность для определения касания
input int minBarsBetweenPoints=2; // Минимальное количество баров между касаниями
input int pointsForPattern=6;     // Минимальное количество касаний для идентификации паттерна

int countMaximums = 0;
double lastMaximumGlobal = 0;
int lastMaximumGlobalIndex = 0;

int countMinimums = 0;
double lastMinimumGlobal = 0;
int lastMinimumGlobalIndex = 0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, SignalBuffer, INDICATOR_DATA);
   SetIndexBuffer(1, ExtremumsBuffer, INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         SignalBuffer[i] = 0;
         ExtremumsBuffer[i] = 0;

         setMaximums(i, high, time);
         setMinimums(i, low, time);
      }

      if(countMaximums >= pointsForPattern && i > (lastMaximumGlobalIndex + 5) && high[i] > lastMaximumGlobal) {
         lastMaximumGlobalIndex = i;
         SignalBuffer[i] = 1;
         DrawHelper::drawArrow("Arrow:max:4", time[i], high[i] + 30 * _Point, 159, clrGreenYellow);
         DrawHelper::moveHorizontalLine("Line:max", high[i], clrOrange, STYLE_DASH);
      }
      
      if(countMinimums >= pointsForPattern && i > (lastMinimumGlobalIndex + 5) && low[i] < lastMinimumGlobal) {
         lastMinimumGlobalIndex = i;
         SignalBuffer[i] = 1;
         DrawHelper::drawArrow("Arrow:min:4", time[i], low[i] - 10 * _Point, 159, clrGreenYellow);
         DrawHelper::moveHorizontalLine("Line:min", low[i], clrOrangeRed, STYLE_DASH);
      }

   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+

void setMinimums(int i, const double &low[], const datetime &time[])
{
// Maximums
   countMinimums = 0;
   int lastMinimumBar = 0;
   double lastMinimum = 0;

   if(lastMinimumGlobalIndex == 0 || i > (lastMinimumGlobalIndex + 5)) {
      for(int j=i-1; j>i-barsAnalizeCount; j--) {
         if(lastMinimum == 0 || (lastMinimum - sensitiveLevelPt*_Point) > low[j]) {
            lastMinimum = low[j];
            lastMinimumBar = j;
            ArrayFill(ExtremumsBuffer, i-barsAnalizeCount, barsAnalizeCount, 0);
            ExtremumsBuffer[j] = low[j];
            countMinimums = 1;
         } else if((lastMinimumBar - j) > minBarsBetweenPoints) {
            if(low[j] > (lastMinimum - sensitiveLevelPt*_Point) && low[j] < (lastMinimum + sensitiveLevelPt*_Point)) {
               lastMinimum = low[j];
               lastMinimumBar = j;
               ExtremumsBuffer[j] = low[j];
               countMinimums++;
            }
         }
         if(countMinimums >= pointsForPattern) {
            lastMinimumGlobal = lastMinimum;
            lastMinimumGlobalIndex = lastMinimumBar;
            break;
         }
      }

      if(countMinimums >= pointsForPattern) {
         int arrowCnt = 0;
         for(int j=i-1; j>i-barsAnalizeCount; j--) {
            if(ExtremumsBuffer[j] > 0) {
               DrawHelper::drawArrow("Arrow:min:" + IntegerToString(arrowCnt), time[j], low[j] - 10 * _Point, 159, clrWhite);
               arrowCnt++;
            }
         }
      }
   }
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setMaximums(int i, const double &high[], const datetime &time[])
{
// Maximums
   countMaximums = 0;
   int lastMaximumBar = 0;
   double lastMaximum = 0;

   if(lastMaximumGlobalIndex == 0 || i > (lastMaximumGlobalIndex + 5)) {
      for(int j=i-1; j>i-barsAnalizeCount; j--) {
         if(lastMaximum == 0 || (lastMaximum + sensitiveLevelPt*_Point) < high[j]) {
            lastMaximum = high[j];
            lastMaximumBar = j;
            ArrayFill(ExtremumsBuffer, i-barsAnalizeCount, barsAnalizeCount, 0);
            ExtremumsBuffer[j] = high[j];
            countMaximums = 1;
         } else if((lastMaximumBar - j) > minBarsBetweenPoints) {
            if(high[j] > (lastMaximum - sensitiveLevelPt*_Point) && high[j] < (lastMaximum + sensitiveLevelPt*_Point)) {
               lastMaximum = high[j];
               lastMaximumBar = j;
               ExtremumsBuffer[j] = high[j];
               countMaximums++;
            }
         }
         if(countMaximums >= pointsForPattern) {
            lastMaximumGlobal = lastMaximum;
            lastMaximumGlobalIndex = lastMaximumBar;
            break;
         }
      }

      if(countMaximums >= pointsForPattern) {
         int arrowCnt = 0;
         for(int j=i-1; j>i-barsAnalizeCount; j--) {
            if(ExtremumsBuffer[j] > 0) {
               DrawHelper::drawArrow("Arrow:max:" + IntegerToString(arrowCnt), time[j], high[j] + 30 * _Point, 159, clrWhite);
               arrowCnt++;
            }
         }
      }
   }
}
//+------------------------------------------------------------------+
