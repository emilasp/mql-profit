//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window|indicator_separate_window
#property indicator_buffers 3
#property indicator_plots   1
//#property indicator_minimum 0
//#property indicator_maximum 100
//--- значения горизонтальных уровней индикатора
//#property indicator_level1 30
//#property indicator_level2 70
//--- в качестве цвета линии горизонтального уровня использован синий цвет
//#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
//#property indicator_levelstyle STYLE_DASHDOTDOT
#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1


#include <Emilasp\helpers\DrawHelper.mqh>

//--- indicator buffers
double BufferInd[];
double ExtremumsBuffer[]; // все сигналы экстремумы: 0 - нет, 1 - максимум, -1 - минимум
double SignalBuffer[]; // все сигналы: 0 - нет, 1 - бычья дивергенция, -1 - медвежья

datetime date_start=D'01.11.2020';

input group "Основное"
input int barsAnalizeCount=20;    // Количество баров для поиска дивергенции
input int minBarsBetweenExtr=8; // Минимальное количество баров между экстремумами
input int maxBarsBetweenDibvPoins=10; // Минимальное количество баров между экстремумами

input group "MACD"
input int periodFast=12;
input int periodSlow=26;
input int periodSignal=9;

int handle;

double lastPatternt          = 0;
double lastContinues         = 0;

struct DivLineResult {
   double            price1;
   double            price2;
   int               index1;
   int               index2;
   double            koef;

   void              DivLineResult()
   {
      index1 = 0;
      index2 = 0;
      price1 = 0;
      price2 = 0;
      koef = 0;
   }
   void              calculate(int _index1, double _price1, int _index2, double _price2)
   {
      index1 = _index1;
      index2 = _index2;
      price1 = _price1;
      price2 = _price2;

      if((price2 - price1) != 0) {
         koef = (index2 - index1)/(price2 - price1);
      }
   }
   bool              hasLine()
   {
      return index1 > 0 && index2 > 0;
   }
};

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferInd, INDICATOR_DATA);
   SetIndexBuffer(1, SignalBuffer, INDICATOR_DATA);
   SetIndexBuffer(2, ExtremumsBuffer, INDICATOR_DATA);

   handle = iMACD(_Symbol, PERIOD_CURRENT, periodFast, periodSlow, periodSignal, PRICE_CLOSE);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         SignalBuffer[i] = 0;

         double BufferIndTmp[];
         CopyBuffer(handle, 1, 0, 1, BufferIndTmp);
         BufferInd[i] = BufferIndTmp[0];

         setExtremums(1, i, barsAnalizeCount, high, ExtremumsBuffer);

         if(ExtremumsBuffer[i-2] > 0) {
            DivLineResult linePriceDiv = getLineDivergenceResult(i, 1, high);

            if(linePriceDiv.hasLine()) {
               DivLineResult lineMacdDiv = getLineDivergenceResult(i, 1, BufferInd);

               if(lineMacdDiv.hasLine()) {
                  SignalBuffer[i] = 1;
                  int rangePoint2 = lineMacdDiv.index2 - linePriceDiv.index2;
                  if(rangePoint2 > (0-maxBarsBetweenDibvPoins) && rangePoint2 < maxBarsBetweenDibvPoins) {
                     datetime time1 = time[lineMacdDiv.index1];
                     datetime time2 = time[lineMacdDiv.index2];
                     double price1 = BufferInd[lineMacdDiv.index1];
                     double price2 = BufferInd[lineMacdDiv.index2];


                     if((linePriceDiv.koef > 0 && lineMacdDiv.koef < 0) || (lineMacdDiv.koef > 0 && linePriceDiv.koef < 0)) {
                                    DrawHelper::drawTrendLine(0, 0, "line:", time[linePriceDiv.index1], high[linePriceDiv.index1], time[linePriceDiv.index2], high[linePriceDiv.index2], clrOrange);

                        DrawHelper::drawArrow("test1:", time[lineMacdDiv.index1], BufferInd[lineMacdDiv.index1], 159, clrOrangeRed, 0, 1);
                        DrawHelper::drawArrow("test2:", time[lineMacdDiv.index2], BufferInd[lineMacdDiv.index2], 159, clrOrangeRed, 0, 1);
                        //DrawHelper::drawTrendLine(0, 1, "line222:", time[lineMacdDiv.index1], BufferInd[lineMacdDiv.index1], time[lineMacdDiv.index2], BufferInd[lineMacdDiv.index2], clrOrangeRed);

                        DrawHelper::drawLabel("divergence", "Divergence:(" + IntegerToString(rangePoint2) + ") price = " + DoubleToString(linePriceDiv.koef, 2) + ", macd = " + DoubleToString(lineMacdDiv.koef, 2), 10, 20);

                     }
                  }
               }
            }
         } else if(ExtremumsBuffer[i-2] < 0) {


            //SignalBuffer[i] = -1;
            DrawHelper::drawArrow("test:" + IntegerToString(i), time[i], high[i]);
         }

      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
DivLineResult getLineDivergenceResult(int i, int direct, const double &values[])
{
   DivLineResult result;

   int    price1Index = 0;
   int    price2Index = 0;
   double price1 = 0;
   double price2 = 0;
   double priceBetweenExtrMax = 0;
   double priceBetweenExtrMin = 0;
   for(int j=i-2; j>=i - barsAnalizeCount; j--) {

      double price = 0;
      if(ExtremumsBuffer[j] == direct) {
         price = values[j];

         if(price1 == 0) {
            price1 = price;
            price1Index = j;
            continue;
         }
         if((price1Index - j) > minBarsBetweenExtr) {
            if(price2 == 0 || (direct > 0 && price2 < price) || (direct < 0 && price2 > price)) {
               price2 = price;
               price2Index = j;
            }
         } else {
            if(priceBetweenExtrMax == 0 || priceBetweenExtrMax < price)
               priceBetweenExtrMax = price;
            if(priceBetweenExtrMin == 0 || priceBetweenExtrMin > price)
               priceBetweenExtrMin = price;
         }
      }
   }

   if(price2 > 0) {
      if(direct > 0 && priceBetweenExtrMax > price2)
         price2Index = 0;
      if(direct < 0 && priceBetweenExtrMin < price2)
         price2Index = 0;
   }

   if(price1Index > 0 && price2Index > 0)
      result.calculate(price1Index, price1, price2Index, price2);

   return result;
}


//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setExtremums(int type, int startIndex, int barsToAnalize, const double &price[], double &ExtremumsBuffer[])
{
   int fromIndex = startIndex - barsToAnalize;

   for(int i=startIndex - 2; i>fromIndex; i--) {
      ExtremumsBuffer[i] = 0;

      if(type == 1) {
         if(price[i-1] < price[i] && price[i+1] < price[i]) {
            ExtremumsBuffer[i] = 1;
         }
      }
      if(type == -1) {

      }
   }
}
//+------------------------------------------------------------------+
