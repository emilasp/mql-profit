//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 12
#property indicator_plots   12


#include <Emilasp\helpers\DrawHelper.mqh>


//--- indicator buffers
double         BufferMaximums[]; // буфер со всеми экстремумами
double         BufferMinimums[];
double         BufferLines[]; // буфер со всеми линиями: 0 - нет, 1 - сопротивления, -1 - поддержка
double         BufferLinesBreakOut[]; // буфер со всеми пробойными линиями: 0 - нет, 1 - сопротивления, -1 - поддержка
double         VolatilyBuffer[]; // Разница между линией поддержки и сопротивления
double         TrandContinuesBuffer[]; // буфер с числом подряд пробоев: -n - пробои медвежьи, 0 - не было пробоев(был флет/треугольник), +n - пробои бычьи
double         TrandStrength1Buffer[]; // сила тренда - чем меньше средняя коррекция к среднему импульсу, тем больше сила тренда
double         TrandStrength2Buffer[]; // сила тренда 2 - сколько и каких проблем было до этого. 5 - пять бычиэьих. -4 - четыре медвежьих. 0 - был флет
double         TrandStrength3Buffer[]; // сила тренда 3 - насколько выше было закрытие свечи во время пробоя. Сем больше, тем сильнее пробой

double         PatternBuffer[]; // текущий паттерн: 0 - флет/треугольник, 1 - бычий импульс, 2 - бычья коррекция, -1 - меджвежий импульс, -2 - медвежья коррекция
double         BreakOutBarBuffer[]; // факт пробоя на текущем баре: 0 - нет пробоя, 1 - пробой сопротивления, -1 - пробой поддержки
double         SignalReboundBuffer[];

datetime       date_start=D'01.11.2020';

//input group "Основное"
input int levelsCount=5;
input int minBarLevelDistance=5;
input int minDistanceToNarrowChannel=5;
input int overLevelSenssetive=5;
input int impulseLevelSenssetivePt=35;


//input group "Пробой"
input int signalReboundDistanceBars=15; // рассчитываем для дистанции. Например для 10 баров.
input int signalReboundSensitivePt=0; // Чувствительность - сколько пунктов считаем, что касание было

//input group "Colors"
input color colorLineMaxBreakOut=clrYellowGreen;
input color colorLineMax=clrLightGreen;
input color colorLineMinBreakOut=clrRed;
input color colorLineMin=clrPink;


double lastBreakOutLinePrice = 0;
double lastTopLinePrice      = 0;
double lastBottomLinePrice   = 0;
double lastPatternt          = 0;
double lastContinues         = 0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferMaximums, INDICATOR_DATA);
   SetIndexBuffer(1, BufferMinimums, INDICATOR_DATA);
   SetIndexBuffer(2, BufferLines, INDICATOR_DATA);
   SetIndexBuffer(3, BufferLinesBreakOut, INDICATOR_DATA);
   SetIndexBuffer(4, VolatilyBuffer, INDICATOR_DATA);
   SetIndexBuffer(5, TrandContinuesBuffer, INDICATOR_DATA);
   SetIndexBuffer(6, TrandStrength1Buffer, INDICATOR_DATA);
   SetIndexBuffer(7, TrandStrength2Buffer, INDICATOR_DATA);
   SetIndexBuffer(8, PatternBuffer, INDICATOR_DATA);
   SetIndexBuffer(9, BreakOutBarBuffer, INDICATOR_DATA);
   SetIndexBuffer(10, SignalReboundBuffer, INDICATOR_DATA);
   SetIndexBuffer(11, TrandStrength3Buffer, INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // Pattern TOP
      if(rates_total - prev_calculated == 1) {
         BufferMaximums[i] = 0;
         BufferMinimums[i] = 0;

         BufferLines[i]          = 0;
         BufferLinesBreakOut[i]  = 0;
         TrandContinuesBuffer[i] = lastContinues;
         TrandStrength1Buffer[i] = 0;
         TrandStrength2Buffer[i] = 0;
         TrandStrength3Buffer[i] = 0;

         // Lines Extremums
         double newMaximum = getExtMaximum(i, high, low);
         double distanceMax = lastTopLinePrice - newMaximum;
         if(newMaximum > 0 && distanceMax != 0) {
            BufferLines[i] = 1;
            if(distanceMax < minDistanceToNarrowChannel * _Point * -1) { // 0
               isMaximumBreakOutChannel(i, newMaximum); // Maximize channel
            }
            if(distanceMax > minDistanceToNarrowChannel * _Point) { // 0
               isMaximumNarrowChannel(i, newMaximum); // Narrow channek
            }
         }

         double newMinimum = getExtMinimum(i, high, low);
         double distanceMin = newMinimum - lastBottomLinePrice;
         if(newMinimum > 0 && distanceMin != 0) {
            BufferLines[i] = -1;
            if(distanceMin < minDistanceToNarrowChannel * _Point * -1) { // 0
               isMinimumBreakOutChannel(i, newMinimum); // Maximize channel
            }
            if(distanceMin > minDistanceToNarrowChannel * _Point) { // 0
               isMinimumNarrowChannel(i, newMinimum); // Narrow channek
            }
         }

         // If FLET
         if(isFlet(i)) {
            //PatternBuffer[i] = 0;
         }

         // BreakOutBarBuffer
         BreakOutBarBuffer[i] = 0;
         if(lastTopLinePrice > 0 && lastTopLinePrice > open[i] && lastTopLinePrice < close[i]) {
            BreakOutBarBuffer[i] = 1;
            lastPatternt = 1;
            lastBreakOutLinePrice = lastTopLinePrice;
         }
         if(lastBottomLinePrice > 0 && lastBottomLinePrice < open[i] && lastBottomLinePrice > close[i]) {
            BreakOutBarBuffer[i] = -1;
            lastPatternt = -1;
            lastBreakOutLinePrice = lastBottomLinePrice;
         }

         // SignalReboundBuffer
         SignalReboundBuffer[i] = 0;
         if(lastBreakOutLinePrice > 0) {
            // Определяем тик на котором было пробитие
            int breakOutBar = -1;
            for(int j=0; j<signalReboundDistanceBars; j++) {
               if(BreakOutBarBuffer[i - j] != 0)  {
                  breakOutBar = i - j;
                  break;
               }
            }

            if(breakOutBar != -1) {
               int retest = -1;
               if(BreakOutBarBuffer[breakOutBar] > 0) {
                  double low2 = getHighLowByType(false, i - 3, high, low);
                  double low1 = getHighLowByType(false, i - 2, high, low);
                  double low0 = getHighLowByType(false, i - 1, high, low);
                 
                  if(low2 > low1 && low1 < low0 && close[i - 1] > open[i - 1]) {
                     retest = 1;
                  }
               }
               if(BreakOutBarBuffer[breakOutBar] < 0) {
                  double high2 = getHighLowByType(true, i - 3, high, low);
                  double high1 = getHighLowByType(true, i - 2, high, low);
                  double high0 = getHighLowByType(true, i - 1, high, low);
                  
                  if(high2 < high1 && high1 > high0 && close[i - 1] < open[i - 1]) {
                     retest = 1;
                  }
               }



               // Проверяем, что не было пробития назад и было касание

               /*int retest = -1;
               for(int j=breakOutBar + 1; j<=i; j++) {
                  if(BreakOutBarBuffer[breakOutBar] > 0) {
                     //if(close[j] < lastBreakOutLinePrice) {
                     //   retest = -1;
                     //   break;
                     //}
                     if(low[j] - signalReboundSensitivePt * _Point < lastBreakOutLinePrice && open[j] > close[j]) {
                        retest = j;
                     }
                  }
                  if(BreakOutBarBuffer[breakOutBar] < 0) {
                     //if(close[j] > lastBreakOutLinePrice) {
                     //   retest = -1;
                     //   break;
                     //}
                     if(high[j] + signalReboundSensitivePt * _Point > lastBreakOutLinePrice && open[j] < close[j]) {
                        retest =  j;
                     }
                  }
               }*/
               if(retest != -1) {
                  // Проверяем, что закрылась первая свеча в нашем направлении
                  // Здесь же сохраняем breakOutBar сигнала, что бы не отправлять второй раз

                  if(BreakOutBarBuffer[breakOutBar] > 0 && close[i] > open[i]) {
                     SignalReboundBuffer[i] = 1;
                  }
                  if(BreakOutBarBuffer[breakOutBar] < 0 && close[i] < open[i]) {
                     SignalReboundBuffer[i] = -1;
                  }
               }
            }
         }

         DrawHelper::drawLabel("BreakOutBarBuffer", "lastBPrice: " + lastBottomLinePrice + ", O: " + open[i] + ", C: " + close[i] + ", BB[i]: " + BreakOutBarBuffer[i], 10, 80);

         PatternBuffer[i] = lastPatternt;

         VolatilyBuffer[i] = (lastTopLinePrice - lastBottomLinePrice) / _Point;

         // Draw info
         drawInfo(i, time, high, low);
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void isMaximumBreakOutChannel(int bufferIndex, double maximum)
{
   lastTopLinePrice  = maximum;
   BufferMaximums[bufferIndex] = maximum;
   BufferLinesBreakOut[bufferIndex] = 1;
   PatternBuffer[bufferIndex] = 2;
   lastPatternt = 2;
   TrandContinuesBuffer[bufferIndex] = getContinuesCount(bufferIndex, true);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void isMaximumNarrowChannel(int bufferIndex, double maximum)
{
   lastTopLinePrice  = maximum;
   BufferMaximums[bufferIndex] = maximum;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void isMinimumBreakOutChannel(int bufferIndex, double minimum)
{
   lastBottomLinePrice  = minimum;
   BufferMinimums[bufferIndex] = minimum;
   BufferLinesBreakOut[bufferIndex] = -1;
   PatternBuffer[bufferIndex] = -2;
   lastPatternt = -2;
   TrandContinuesBuffer[bufferIndex] = getContinuesCount(bufferIndex, false);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void isMinimumNarrowChannel(int bufferIndex, double minimum)
{
   lastBottomLinePrice  = minimum;
   BufferMinimums[bufferIndex] = minimum;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void  drawInfo(int bufferIndex, const datetime &time[], const double &high[], const double &low[])
{
   double maximumPrice = BufferMaximums[bufferIndex];
   double minimumPrice = BufferMinimums[bufferIndex];

   /*double breakOutTopFirst = 0;
   double breakOutBtmFirst = 0;


   int total = ArraySize(BufferMaximums);
   for(int i=bufferIndex;i >= 0;i--) {
      if(i < bufferIndex - 100)
        {

        }
     }*/


   if(BufferLinesBreakOut[bufferIndex] == 1) {
      DrawHelper::moveHorizontalLine("Ind:Line:Top:max", maximumPrice, colorLineMaxBreakOut, STYLE_DASH);
      DrawHelper::moveHorizontalLine("Ind:Line:Top", 0, colorLineMax, STYLE_DASHDOT);
   } else if(BufferLines[bufferIndex] == 1) {
      DrawHelper::moveHorizontalLine("Ind:Line:Top", maximumPrice, colorLineMax, STYLE_DASHDOT);
   }
   if(BufferLinesBreakOut[bufferIndex] == -1) {
      DrawHelper::moveHorizontalLine("Ind:Line:Bottom:max", minimumPrice, colorLineMinBreakOut, STYLE_DASH);
      DrawHelper::moveHorizontalLine("Ind:Line:Bottom", 0, colorLineMin, STYLE_DASHDOT);
   } else if(BufferLines[bufferIndex] == -1) {
      DrawHelper::moveHorizontalLine("Ind:Line:Bottom", minimumPrice, colorLineMin, STYLE_DASHDOT);
   }

   if(SignalReboundBuffer[bufferIndex] != 0) {
      DrawHelper::moveHorizontalLine("Ind:Line:SignalReboundBuffer", lastBreakOutLinePrice, clrOrange, STYLE_DASHDOTDOT);
   }
//DrawHelper::drawLabel("Pattern", "PatternInd: " + getPatternText(PatternBuffer[bufferIndex]), 10, 60);
//DrawHelper::drawLabel("Continues", "Continues: " + IntegerToString(TrandContinuesBuffer[bufferIndex]), 10, 40);
//DrawHelper::drawLabel("Volatily", "Volatily: " + IntegerToString(VolatilyBuffer[bufferIndex], 1), 10, 60);

//if(BreakOutBarBuffer[bufferIndex] == 1)
//   DrawHelper::drawArrow("Upper", time[bufferIndex], high[bufferIndex] + 20 * _Point, 159, clrWhite);
//if(BreakOutBarBuffer[bufferIndex] == -1)
//   DrawHelper::drawArrow("Downer", time[bufferIndex], low[bufferIndex] - 10 * _Point, 159, clrWhite);

   if(SignalReboundBuffer[bufferIndex] == 1)
      DrawHelper::drawArrow("Upper", time[bufferIndex], high[bufferIndex] + 20 * _Point, 159, clrYellow);
   if(SignalReboundBuffer[bufferIndex] == -1)
      DrawHelper::drawArrow("Downer", time[bufferIndex], low[bufferIndex] - 10 * _Point, 159, clrYellow);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string getPatternText(int patternVal)
{
   switch(patternVal) {
   case 1:
      return "Bull impulse";
   case 2:
      return "Bull correction";
   case -1:
      return "Bear impulse";
   case -2:
      return "Bear correction";
   }
   return "FLET";
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isFlet(int bufferIndex, int extremums = 4)
{
   int lastMaximum = 0;
   int lastMinimum = 0;
   int countExtremums = 0;
   int total = ArraySize(BufferLines);
   for(int i=total; i>=0; i--) {
      int curLine = BufferLines[bufferIndex];
      if(curLine > 0) {
         if(lastMaximum == 0) {
            lastMaximum = curLine;
            continue;
         }
         if(lastMaximum >= curLine) {
            lastMaximum = curLine;
            countExtremums++;
         } else {
            return false;
         }
      }
      if(curLine < 0) {
         if(lastMinimum == 0) {
            lastMinimum = curLine;
            continue;
         }
         if(lastMinimum <= curLine) {
            lastMinimum = curLine;
            countExtremums++;
         } else {
            return false;
         }
      }
      if(countExtremums >= extremums)
         return true;
   }
   return false;
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getContinuesCount(int currentIndex, bool isMaximum)
{
   if(isMaximum) {
      if(lastContinues >= 0) {
         lastContinues++;
      } else {
         lastContinues = 1;
      }
   } else {
      if(lastContinues <= 0) {
         lastContinues--;
      } else {
         lastContinues = -1;
      }
   }
   return lastContinues;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getExtMaximum(int startIndex, const double &high[], const double &low[])
{
   double high1 = getHighLowByType(true, startIndex - 4, high, low);
   double high2 = getHighLowByType(true, startIndex - 3, high, low);
   double high3 = getHighLowByType(true, startIndex - 2, high, low);
   double high4 = getHighLowByType(true, startIndex - 1, high, low);
   double high5 = getHighLowByType(true, startIndex, high, low);

   double newExt = 0;
   if(high1 < high2 && high2 < high3 && high3 > high4 && high4 > high5) {
      //int bearsCount = countBearBars(startIndex, startIndex -4, open, close);
      //if(bearsCount==2 || bearsCount==3) {
      newExt = high[startIndex - 2];
      //}
   }

// Добавляем длинные импульсные свечи
   if(newExt == 0) {
      if(high2 < high3 && (high3 - high2) > impulseLevelSenssetivePt * _Point && high3 > high4 && high4 > high5) {
         newExt = high[startIndex - 2];
      }
   }
   return newExt;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getExtMinimum(int startIndex, const double &high[], const double &low[])
{
   double low1 = getHighLowByType(false, startIndex - 4, high, low);
   double low2 = getHighLowByType(false, startIndex - 3, high, low);
   double low3 = getHighLowByType(false, startIndex - 2, high, low);
   double low4 = getHighLowByType(false, startIndex - 1, high, low);
   double low5 = getHighLowByType(false, startIndex, high, low);

   double newExt = 0;
   if(low1 > low2 && low2 > low3 && low3 < low4 && low4 < low5) {
      //int bearsCount = countBearBars(startIndex, startIndex -4, open, close);
      //if(bearsCount==2 || bearsCount==3) {
      newExt = low[startIndex - 2];
      //}
   }
// Добавляем длинные импульсные свечи
   if(newExt == 0) {
      if(low2 > low3 && (low2 - low3) > impulseLevelSenssetivePt * _Point && low3 < low4 && low4 < low5) {
         newExt = low[startIndex - 2];
      }
   }
   return newExt;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getPriceByType(bool isTop, int i, const double &open[], const double &close[])
{
// Bear
   if(isTop) {
      if(open[i] > close[i]) {
         return open[i];
      } else {
         return close[i];
      }
   } else {
      if(open[i] > close[i]) {
         return close[i];
      } else {
         return open[i];
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getHighLowByType(bool isTop, int i, const double &high[], const double &low[])
{
// Bear
   if(isTop) {
      return high[i];
   } else {
      return low[i];
   }
}


//+------------------------------------------------------------------+
int countBearBars(int start, int end, const double &open[], const double &close[])
{
   int bearsCount = 0;

   for(int i=start; i >= end; i--) {
      if(open[i] > close[i])
         bearsCount++;
   }
   return bearsCount;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isBearBar(double open, double close)
{
   return open > close;
}
//+------------------------------------------------------------------+
