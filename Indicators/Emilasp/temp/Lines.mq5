//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 6
#property indicator_plots   5
//--- plot Label1
#property indicator_label1  "top"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "down"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_DOT
#property indicator_width2  1
//--- plot Arrows
#property indicator_label3  "Arrows Top"
#property indicator_type3   DRAW_ARROW
#property indicator_color3  clrGreen
#property indicator_width3  1

#property indicator_label4  "Arrows Bottom"
#property indicator_type4   DRAW_ARROW
#property indicator_color4  clrGreen
#property indicator_width4  1
///
#property indicator_label5  "Trand"
#property indicator_type5   DRAW_COLOR_LINE
#property indicator_color5  clrGray,clrGreen,clrRed
#property indicator_width5  2


#include <Emilasp\helpers\DrawHelper.mqh>


//--- indicator buffers
double         BufferTop[];
double         BufferBottom[];
double         BufferArrowsTop[];
double         BufferArrowsBottom[];
double         BufferTrand[];
double         BufferTrandClr[];

datetime       date_start=D'01.11.2020';

input int barsHist=100;


double lastTopLinePrice = 0;
double lastBottomLinePrice = 0;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferTop, INDICATOR_DATA);
   SetIndexBuffer(1, BufferBottom, INDICATOR_DATA);
   SetIndexBuffer(2, BufferArrowsTop, INDICATOR_DATA);
   SetIndexBuffer(3, BufferArrowsBottom, INDICATOR_DATA);
   SetIndexBuffer(4, BufferTrand, INDICATOR_DATA);
   SetIndexBuffer(5, BufferTrandClr, INDICATOR_COLOR_INDEX);

   PlotIndexSetDouble(2, PLOT_EMPTY_VALUE, 0);
   PlotIndexSetDouble(3, PLOT_EMPTY_VALUE, 0);

   PlotIndexSetInteger(2, PLOT_LINE_COLOR, clrGreen);
   PlotIndexSetInteger(2, PLOT_ARROW, 233);
   PlotIndexSetInteger(2, PLOT_ARROW_SHIFT, -30);

   PlotIndexSetInteger(3, PLOT_LINE_COLOR, clrGreenYellow);
   PlotIndexSetInteger(3, PLOT_ARROW, 234 );
   PlotIndexSetInteger(3, PLOT_ARROW_SHIFT, 30);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // Pattern TOP
      if(rates_total - prev_calculated == 1) {
         double high1 = getHighLowByType(true, i, open, close);
         double high2 = getHighLowByType(true, i - 1, high, low);
         double high3 = getHighLowByType(true, i - 2, high, low);
         double high4 = getHighLowByType(true, i - 3, high, low);
         double high5 = getHighLowByType(true, i - 4, high, low);
         double high6 = getHighLowByType(true, i - 5, high, low);

         if(high1 < high2 && high2 < high3 && high3 > high4 && high4 > high5) {
            int bearsCount = countBearBars(i, i -4, open, close);
            if(bearsCount==2 || bearsCount==3) {
               lastTopLinePrice = high[i - 2];
               DrawHelper::moveHorizontalLine("Ind:Line:Top", lastTopLinePrice, clrWhite, STYLE_DASH);
            }
         }
         // Pattern Bottom

         double low1 = getHighLowByType(false, i, open, close);
         double low2 = getHighLowByType(false, i - 1, high, low);
         double low3 = getHighLowByType(false, i - 2, high, low);
         double low4 = getHighLowByType(false, i - 3, high, low);
         double low5 = getHighLowByType(false, i - 4, high, low);
         double low6 = getHighLowByType(false, i - 5, high, low);

         if(low1 > low2 && low2 > low3 && low3 < low4 && low4 < low5) {
            int bearsCount = countBearBars(i, i -4, open, close);
            if(bearsCount==2 || bearsCount==3) {
               lastBottomLinePrice = low[i - 2];
               DrawHelper::moveHorizontalLine("Ind:Line:Bottom", lastBottomLinePrice, clrYellow, STYLE_DASH);
            }
         }


         // Сигналы
         // TOP
         BufferArrowsTop[i] = 0;
         BufferArrowsBottom[i] = 0;
         if(lastTopLinePrice > 0 && open[i] < lastTopLinePrice && close[i] > lastTopLinePrice) {
            BufferArrowsTop[i] = high[i];
         }
         // Bottom
         if(lastBottomLinePrice > 0 && open[i] > lastBottomLinePrice && close[i] < lastBottomLinePrice) {
            BufferArrowsBottom[i] = low[i];
         }

         BufferTrand[i] = 0;

         int depth = 100;
         int countArrowsToFinish = 3;
         
         int firstIndex = -1;
         int first = 0;
         int second = 0;
         int countArrowsTop = 0;
         int countArrowsBottom = 0;
         for(int k=0; k<depth; k++) {
            if(BufferArrowsTop[i - k] > 0) {
               if(first != 0 && second == 0 && (k - firstIndex) > 5)
                  second = 1;
               if(first == 0) {
                  firstIndex = k;
                  first = 1;
               }
                  

               countArrowsTop++;
            }
              
             if(BufferArrowsBottom[i - k] > 0) {
               if(first != 0 && second == 0 && (k - firstIndex) > 5)
                  second = -1;
               if(first == 0) {
                  firstIndex = k;
                  first = -1;
               }
               countArrowsBottom++;
            } 

            if((countArrowsTop + countArrowsBottom) > countArrowsToFinish)
               break;
         }

         BufferTrand[i] = open[i] - 40 * _Point;

         if(first == 1 && second == 1)
            BufferTrandClr[i] = 1;
         if(first == -1 && second == -1)
            BufferTrandClr[i] = 2;
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getPriceByType(bool isTop, int i, const double &open[], const double &close[])
{
// Bear
   if(isTop) {
      if(open[i] > close[i]) {
         return open[i];
      } else {
         return close[i];
      }
   } else {
      if(open[i] > close[i]) {
         return close[i];
      } else {
         return open[i];
      }
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getHighLowByType(bool isTop, int i, const double &high[], const double &low[])
{
// Bear
   if(isTop) {
      return high[i];
   } else {
      return low[i];
   }
}


//+------------------------------------------------------------------+
int countBearBars(int start, int end, const double &open[], const double &close[])
{
   int bearsCount = 0;

   for(int i=start; i >= end; i--) {
      if(open[i] > close[i])
         bearsCount++;
   }
   return bearsCount;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool isBearBar(double open, double close)
{
   return open > close;
}
//+------------------------------------------------------------------+
