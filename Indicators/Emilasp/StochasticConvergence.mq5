//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
#property indicator_maximum 5
#property indicator_minimum -5
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>

input double maxDiffPrev = 2.5;
input double minDiffNext = 3.5;
double minAtrRation = 1.3;


//--- indicator buffers
double         BufferSignal[];
double         BufferDirect[];

datetime       date_start=D'01.11.2019';

int handle1;
int handle2;
int handle3;
int handle4;
int handle5;

int handleAtr1;
int handleAtr2;

int slowing = 3;

int dPeriod1 = 3;
int dPeriod2 = 7;
int dPeriod3 = 14;
int dPeriod4 = 21;
int dPeriod5 = 44;
int kPeriod1 = 14;
int kPeriod2 = 21;
int kPeriod3 = 33;
int kPeriod4 = 55;
int kPeriod5 = 121;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferDirect, INDICATOR_DATA);
   SetIndexBuffer(1, BufferSignal, INDICATOR_DATA);

//switch(type) {
//case  GET_EXTREMUM_TYPE_RSI:
//case  GET_EXTREMUM_TYPE_RSI_2:
   handle1 = iStochastic(_Symbol, PERIOD_CURRENT, kPeriod1, dPeriod1, slowing, MODE_SMA, STO_LOWHIGH);
   handle2 = iStochastic(_Symbol, PERIOD_CURRENT, kPeriod2, dPeriod2, slowing, MODE_SMA, STO_LOWHIGH);
   handle3 = iStochastic(_Symbol, PERIOD_CURRENT, kPeriod3, dPeriod3, slowing, MODE_SMA, STO_LOWHIGH);
   handle4 = iStochastic(_Symbol, PERIOD_CURRENT, kPeriod4, dPeriod4, slowing, MODE_SMA, STO_LOWHIGH);
   handle5 = iStochastic(_Symbol, PERIOD_CURRENT, kPeriod5, dPeriod5, slowing, MODE_SMA, STO_LOWHIGH);
   
   handleAtr1 = iATR(_Symbol, PERIOD_CURRENT, 30);
   handleAtr2 = iATR(_Symbol, PERIOD_CURRENT, 3);
   
//   break;
//default:
//   break;
//}

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         BufferSignal[i] = 0;
         BufferDirect[i] = 0;

         double         Buffer1[];
         double         Buffer2[];
         double         Buffer3[];
         double         Buffer4[];
         double         Buffer5[];
         double         BufferAtr1[];
         double         BufferAtr2[];
         
         ResetLastError();

         CopyBuffer(handle1, 1, 0, 2, Buffer1);
         CopyBuffer(handle2, 1, 0, 2, Buffer2);
         CopyBuffer(handle3, 1, 0, 2, Buffer3);
         CopyBuffer(handle4, 1, 0, 2, Buffer4);
         CopyBuffer(handle5, 1, 0, 2, Buffer5);
         
         CopyBuffer(handleAtr1, 0, 0, 1, BufferAtr1);
         CopyBuffer(handleAtr2, 0, 0, 1, BufferAtr2);
         
         double values[5];
         values[0] = Buffer1[0];
         values[1] = Buffer2[0];
         values[2] = Buffer3[0];
         values[3] = Buffer4[0];
         values[4] = Buffer4[0]; // TODO

         double maximum = values[ArrayMaximum(values)];
         double minimum = values[ArrayMinimum(values)];

         BufferSignal[i] = maximum - minimum;
         
         double atrRatio = BufferAtr2[0] / BufferAtr1[0];
         
         if (BufferSignal[i-1] < maxDiffPrev && BufferSignal[i] > minDiffNext) { // && atrRatio > minAtrRation
            if (Buffer1[0] > 75) {
               BufferDirect[i] = 1;
               DrawHelper::drawArrow("long:" + IntegerToString(i), time[i], 1, 159, clrRed, 0, 1, 14);
               DrawHelper::drawArrow("long:price:" + IntegerToString(i), time[i], high[i], 159, clrRed, 0, 0, 14);
            }
            if (Buffer1[0] < 25) {
               BufferDirect[i] = -1;
               DrawHelper::drawArrow("short:" + IntegerToString(i), time[i], -1, 159, clrRed, 0, 1, 14);
               DrawHelper::drawArrow("short:price:" + IntegerToString(i), time[i], low[i], 159, clrRed, 0, 0, 14);
            }
         }
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
