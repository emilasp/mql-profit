//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
#property indicator_maximum 5
#property indicator_minimum -5
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>

input double maxDiffPrev = 2.5;
input double minDiffNext = 3.5;
double minAtrRation = 1.3;


//--- indicator buffers
double         BufferUp[];
double         BufferDown[];

datetime       date_start=D'01.11.2019';

int handle1;
int handleAlligator;

int prevTick = 0;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferUp, INDICATOR_DATA);
   SetIndexBuffer(1, BufferDown, INDICATOR_DATA);

//switch(type) {
//case  GET_EXTREMUM_TYPE_RSI:
//case  GET_EXTREMUM_TYPE_RSI_2:
   handle1 = iFractals(_Symbol, PERIOD_CURRENT);
   handleAlligator = iAlligator(_Symbol, PERIOD_CURRENT,13,8,8,5,5,3,MODE_SMA,PRICE_MEDIAN);
   
//   break;
//default:
//   break;
//}

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         prevTick = prev_calculated;
         BufferUp[i] = 0;
         BufferDown[i] = 0;

         double         BufferUpLocal[];
         double         BufferDwnLocal[];
         double         BufferAlligatorFast[];
         
         
         ResetLastError();

         CopyBuffer(handle1, 0, 0, 5, BufferUpLocal);
         CopyBuffer(handle1, 1, 0, 5, BufferDwnLocal);
         CopyBuffer(handleAlligator, 0, 0, 5, BufferAlligatorFast);
         
         BufferUp[i] = BufferUpLocal[1];
         BufferDown[i] = BufferDwnLocal[1];
         
         DrawHelper::drawLabel("profitCount",IntegerToString(BufferUp[i-1]),20,20);
         DrawHelper::drawLabel("profitCount2",IntegerToString(BufferDown[i-1]),20,60);

         if (int(BufferUp[i]) > 0) {
            DrawHelper::drawArrow("long:" + IntegerToString(i), time[i], 1, 159, clrGreen, 0, 1, 14);
            DrawHelper::drawArrow("long:price:" + IntegerToString(i), time[i], high[i] + 30 * _Point, 159, clrGreen, 0, 0, 14);
            DrawHelper::drawArrow("long:price2:" + IntegerToString(i), time[i-2], high[i-2] + 30 * _Point, 217, clrGreenYellow, 0, 0, 14);
         }
         if (int(BufferDown[i]) > 0) {
            DrawHelper::drawArrow("short:" + IntegerToString(i), time[i], -1, 159, clrRed, 0, 1, 14);
            DrawHelper::drawArrow("short:price:" + IntegerToString(i), time[i], low[i] - 15 * _Point, 159, clrRed, 0, 0, 14);
            DrawHelper::drawArrow("short:price2:" + IntegerToString(i), time[i-2], low[i-2] - 15 * _Point, 218, clrOrangeRed, 0, 0, 14);
         }

      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
