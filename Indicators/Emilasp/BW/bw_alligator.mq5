//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window //indicator_chart_window
#property indicator_buffers 10
#property indicator_plots   10

//--- plot Jaws
#property indicator_label1  "Jaws"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Teeth
#property indicator_label2  "Teeth"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Lips
#property indicator_label3  "Lips"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrLime
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1


#include <Emilasp\helpers\DrawHelper.mqh>

input int historyBarsAnalize = 164; // Баров для анализа Аллигатора
input int flatFilterPercentByMin = 1.5; // Процент от минимального ниже которого считаем, что идет флет
input int trandByFlatDistance = 6; // Через сколько баров после последнего пересечения считать, что наступил тренд
input bool stopTrandIfPriceIntersec = true; // Отменяем тренд, если было пересечение с ценой

input int                  jaw_period=13;          // период для линии Челюстей
input int                  jaw_shift=8;            // смещение линии Челюстей
input int                  teeth_period=8;         // период для линии Зубов
input int                  teeth_shift=5;          // смещение линии Челюстей
input int                  lips_period=5;          // период для линии Губ
input int                  lips_shift=3;           // смещение линии Губ
input ENUM_MA_METHOD       MA_method=MODE_SMMA;    // метод усреднения линий Аллигатора
input ENUM_APPLIED_PRICE   applied_price=PRICE_MEDIAN;// тип цены, от которой строится Аллигатор
//--- индикаторные буферы
double         JawsBuffer[];
double         TeethBuffer[];
double         LipsBuffer[];



//--- indicator buffers
double         BufferStrength[]; // Сила тренда 0-1
double         BufferType[]; // Тип: 0-flat, 1-Trand bull, -1 - trand bear
double         BufferAngle[]; // Угол отклонения Аллигатора от цены
double         BufferLenght[]; // i количество баров с последнего бара флета
double         BufferNoIntersecWithPriceCount[]; // Количество баров с момента последнего пересечения с ценой
double         BufferNoIntersecAlligatorCount[]; // Количество баров с момента последнего пересечения с любой линией Аллигатора
double         BufferCloseAlligatorDistance[]; // Пунктов от цены от любой из линий Алигатора

datetime       date_start=D'01.11.2019';

int prevTick = 0;
int handle;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping

   SetIndexBuffer(0,JawsBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,TeethBuffer,INDICATOR_DATA);
   SetIndexBuffer(2,LipsBuffer,INDICATOR_DATA);

   SetIndexBuffer(3, BufferStrength, INDICATOR_DATA);
   SetIndexBuffer(4, BufferType, INDICATOR_DATA);
   SetIndexBuffer(5, BufferAngle, INDICATOR_DATA);
   SetIndexBuffer(6, BufferLenght, INDICATOR_DATA);
   SetIndexBuffer(7, BufferNoIntersecWithPriceCount, INDICATOR_DATA);
   SetIndexBuffer(8, BufferNoIntersecAlligatorCount, INDICATOR_DATA);
   SetIndexBuffer(9, BufferCloseAlligatorDistance, INDICATOR_DATA);

   PlotIndexSetInteger(0,PLOT_SHIFT,jaw_shift);
   PlotIndexSetInteger(1,PLOT_SHIFT,teeth_shift);
   PlotIndexSetInteger(2,PLOT_SHIFT,lips_shift);
//--- determine the symbol the indicator is drawn for
   string name=_Symbol;
//--- delete spaces to the right and to the left
   StringTrimRight(name);
   StringTrimLeft(name);

   handle = iAlligator(_Symbol, PERIOD_CURRENT, jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, MA_method, applied_price);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{

   int values_to_copy;


//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         prevTick = prev_calculated;

         if(!FillArraysFromBuffers(JawsBuffer, jaw_shift, TeethBuffer, teeth_shift, LipsBuffer, lips_shift, handle, 2))
            return(0);
         
         PrintFormat("JawsBuffer %d, %d, %d - " +  DoubleToString(JawsBuffer[i]), 6, JawsBuffer[i], TeethBuffer[i], LipsBuffer[i]);
         
         DrawHelper::drawLabel("labelSignal1", "TF: " + DoubleToString(JawsBuffer[i], 10),   100, 20);
         DrawHelper::drawLabel("labelSignal2", "TF2: " + DoubleToString(TeethBuffer[i], 10), 100, 40);
         DrawHelper::drawLabel("labelSignal3", "TF3: " + DoubleToString(LipsBuffer[i], 6), 100, 60);

         // Filter
         /*if (BufferSignal[i] != 0) {
            if (prevBarType == ONLY_ONE_SECTOR && BufferOpenType[i] != 1) {
               BufferSignal[i] = 0;
            }
            if (prevBarType == ONLY_DIFFERENT_SECTOR && BufferOpenType[i] != 2) {
               BufferSignal[i] = 0;
            }
         }*/
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}

//+------------------------------------------------------------------+
//| Заполняем индикаторные буферы из индикатора iAlligator           |
//+------------------------------------------------------------------+
bool FillArraysFromBuffers(double &jaws_buffer[],  // индикаторный буфер для линии Челюстей
                           int j_shift,            // смещение линии Челюстей
                           double &teeth_buffer[], // индикаторный буфер для линии Зубов
                           int t_shift,            // смещение линии Зубов
                           double &lips_buffer[],  // индикаторный буфер для линии Губ
                           int l_shift,            // смещение линии Губ
                           int ind_handle,         // хэндл индикатора iAlligator
                           int amount              // количество копируемых значений
                          )
{
//--- сбросим код ошибки
   ResetLastError();
//--- заполняем часть массива JawsBuffer значениями из индикаторного буфера под индексом 0
   if(CopyBuffer(ind_handle, 0, -j_shift, amount, jaws_buffer)<0) {
      //--- если копирование не удалось, сообщим код ошибки
      PrintFormat("Не удалось скопировать данные из индикатора iAlligator, код ошибки %d", GetLastError());
      //--- завершим с нулевым результатом - это означает, что индикатор будет считаться нерассчитанным
      return(false);
   }

//--- заполняем часть массива TeethBuffer значениями из индикаторного буфера под индексом 1
   if(CopyBuffer(ind_handle, 1, -t_shift, amount, teeth_buffer)<0) {
      //--- если копирование не удалось, сообщим код ошибки
      PrintFormat("Не удалось скопировать данные из индикатора iAlligator, код ошибки %d", GetLastError());
      //--- завершим с нулевым результатом - это означает, что индикатор будет считаться нерассчитанным
      return(false);
   }

//--- заполняем часть массива LipsBuffer значениями из индикаторного буфера под индексом 2
   if(CopyBuffer(ind_handle, 2, -l_shift, amount, lips_buffer)<0) {
      //--- если копирование не удалось, сообщим код ошибки
      PrintFormat("Не удалось скопировать данные из индикатора iAlligator, код ошибки %d", GetLastError());
      //--- завершим с нулевым результатом - это означает, что индикатор будет считаться нерассчитанным
      return(false);
   }
//--- все получилось
   return(true);
}
//+------------------------------------------------------------------+
