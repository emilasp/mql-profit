//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   3
#property indicator_maximum 1.1
#property indicator_minimum -1.1
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\Fractals\FractalsCollection.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>


input ENUM_FRACTAL_TYPES fractalType = FRACTAL_TYPE_FIVE;
input int patternType = 1;

input int filterMinDistananceFractalsPt = 20;
input int filterAlligatorMinDistancePt = -1;
input bool isModeAdditionalFractals = false;

////////// System
int handleAlligator;

FractalsCollection fractalsAll;
FractalsCollection fractalsOdd;

int lastTrandSignal = 0;
int breakAfterSignalCount = 0;

datetime       date_start=D'01.11.2019';
int prevTick = 0;

//////////--- indicator buffers
double         BufferSignal[];
double         BufferType[];
double         BufferTrand[];


//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferType, INDICATOR_DATA);
   SetIndexBuffer(2, BufferTrand, INDICATOR_DATA);
//---

   handleAlligator = iAlligator(_Symbol, PERIOD_CURRENT, 13, 8, 8, 5, 5, 3, MODE_SMMA, PRICE_MEDIAN);

   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         prevTick = prev_calculated;

         BufferSignal[i] = 0;
         BufferType[i] = 0;
         BufferTrand[i] = lastTrandSignal;

         ResetLastError();

         // Get fractal
         Fractal fractal = fractalsAll.getFractal(fractalType, i, time, high, low);

         // Filter by alligator
         if (!fractal.isEmpty && filterAlligatorMinDistancePt > -1) {
            filterByAlligator(fractal, filterAlligatorMinDistancePt);
         }

         // Process fractal
         if (!fractal.isEmpty) {
            // add fractals to all
            fractalsAll.Add(fractal, true);

            // add fractals to odd
            if (ArraySize(fractalsOdd.fractals) == 0) {
               fractalsOdd.Add(fractal);
            } else {
               if (fractalsOdd.fractals[0].direct != fractal.direct) {
                  int distance = int(MathAbs(fractalsOdd.fractals[0].price - fractal.price) / _Point);
                  if (distance >= filterMinDistananceFractalsPt) {
                     fractalsOdd.Add(fractal, true);
                  }
               } else {
                  // MAx min
                  if (isModeAdditionalFractals) {
                     Fractal last = fractalsOdd.fractals[0];
                     if (fractal.direct = 1 && fractal.price > last.price) {
                        fractalsOdd.fractals[0] = fractal;
                     }
                     if (fractal.direct = -1 && fractal.price < last.price) {
                        fractalsOdd.fractals[0] = fractal;
                     }
                  }
               }

               //
            }

            // Draw fractals
            if (fractal.direct > 0) {
               DrawHelper::drawArrow("long:price:" + IntegerToString(i), fractal.time, fractal.price + 20 * _Point, 172, clrRed, 0, 0, 14);
            }
            if (fractal.direct < 0) {
               DrawHelper::drawArrow("short:price:" + IntegerToString(i), fractal.time, fractal.price - 7 * _Point, 172, clrGreen, 0, 0, 14);
            }
         }


         // Signals
         if (ArraySize(fractalsOdd.fractals) >= 2) {
            Fractal fractalPrev = fractalsOdd.fractals[1];
            
            for(int j=0;j<ArraySize(fractalsOdd.fractals);j++) {
               Fractal fractalOdd = fractalsOdd.fractals[j];
               if (lastTrandSignal == 1 && fractalOdd.direct == -1) {
                  fractalPrev = fractalOdd;
                  break;
               }
               if (lastTrandSignal == -1 && fractalOdd.direct == 1) {
                  fractalPrev = fractalOdd;
                  break;
               }
            }
            
            if (fractalPrev.direct == 1 && close[i] > fractalPrev.price) {
               if(lastTrandSignal != 1)
                  breakAfterSignalCount=0;
               else
                  breakAfterSignalCount++;
               lastTrandSignal = 1;
               
               DrawHelper::drawLabel("labelTrand2", "trand: " + DoubleToString(lastTrandSignal) + ", count: " + IntegerToString(breakAfterSignalCount), 200, 20, 3);
            }

            if (fractalPrev.direct == -1 && close[i] < fractalPrev.price) {
               if(lastTrandSignal != -1)
                  breakAfterSignalCount=0;
               else
                  breakAfterSignalCount++;
               lastTrandSignal = -1;
               
               DrawHelper::drawLabel("labelTrand2", "trand: " + DoubleToString(lastTrandSignal), 200, 20, 3);
            }

            
            if (!fractalPrev.isEmpty) {
               DrawHelper::drawLabel("labelTrand2p", "price: " + DoubleToString(fractalPrev.price, 5), 200, 40, 3);
               DrawHelper::drawTrendLine(0, 0, "Trand2p", fractalPrev.time, fractalPrev.price, time[i], fractalPrev.price, clrGreenYellow, STYLE_DOT, 2);
            }
         }

         BufferTrand[i] = lastTrandSignal;


         // Draw last Fractals Odd
         if (ArraySize(fractalsOdd.fractals) >=2) {
            for(int j=0; j < 2; j++) {
               int colorArrow = fractalsOdd.fractals[j].direct < 0 ? clrGreen : clrRed;
               int iconArrow = fractalsOdd.fractals[j].direct < 0 ? 233 : 234;
               DrawHelper::drawArrow("fr-" + IntegerToString(j), fractalsOdd.fractals[j].time, fractalsOdd.fractals[j].price, iconArrow, colorArrow);
            }
         }
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+








//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool filterByAlligator(Fractal &fractal, int minDistance)
{
   double         JawsBuffer[];
   double         LipsBuffer[];
   ArraySetAsSeries(JawsBuffer, true);
   ArraySetAsSeries(LipsBuffer, true);

   if(CopyBuffer(handleAlligator, 0, -8, 2, JawsBuffer)<0) {
      PrintFormat("Не удалось скопировать данные из индикатора iAlligator, код ошибки %d", GetLastError());
      return(false);
   }
   if(CopyBuffer(handleAlligator, 2, -3, 2, LipsBuffer)<0) {
      PrintFormat("Не удалось скопировать данные из индикатора iAlligator, код ошибки %d", GetLastError());
      return(false);
   }

   double max = JawsBuffer[0] > LipsBuffer[0] ? JawsBuffer[0] : LipsBuffer[0];
   double min = JawsBuffer[0] < LipsBuffer[0] ? JawsBuffer[0] : LipsBuffer[0];

   if (fractal.direct == 1 && fractal.price < (min + minDistance * _Point)) {
      fractal.isEmpty = true;
   }
   if (fractal.direct == -1 && fractal.price > (max - minDistance * _Point)) {
      fractal.isEmpty = true;
   }
   return true;
}
//+------------------------------------------------------------------+
