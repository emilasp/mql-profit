//+------------------------------------------------------------------+
//|                                              Demo_iAlligator.mq5 |
//|                        Copyright 2011, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2011, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property description "The indicator demonstrates how to obtain data"
#property description "of indicator buffers for the iAlligator technical indicator."
#property description "A symbol and timeframe used for calculation of the indicator,"
#property description "are set by the symbol and period parameters."
#property description "The method of creation of the handle is set through the 'type' parameter (function type)."
#property description "All the other parameters are similar to the standard Alligator."


#include <Emilasp\Indicators\SuperSystemIndicator\AoDevirgencerIndicator.mqh>

#property indicator_separate_window
#property indicator_buffers 2
#property indicator_plots   2


//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_HISTOGRAM
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  3

//--- input parameters

input int barsToAnalize = 120; // количество баров для поиска минимального и максимального значения аллигатора(в начале)

AoDevirgencerIndicator *indicator = new AoDevirgencerIndicator;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{

   indicator.OnInit("AoDivergenceIndicator", barsToAnalize);

//--- show the symbol/timeframe the Alligator indicator is calculated for
   string short_name=StringFormat("iAlligator(%s/%s, %d)", _Symbol, EnumToString(_Period), barsToAnalize);
   IndicatorSetString(INDICATOR_SHORTNAME, short_name);
//--- normal initialization of the indicator
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
   return(indicator.OnCalculate(rates_total, prev_calculated, time, open, high, low, close, tick_volume, volume, spread));
}

//+------------------------------------------------------------------+
//| Indicator deinitialization function                              |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   indicator.OnDeinit(reason);
}
//+------------------------------------------------------------------+
