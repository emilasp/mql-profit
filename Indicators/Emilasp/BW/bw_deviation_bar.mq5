//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
#property indicator_maximum 1.1
#property indicator_minimum -1.1
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>


enum ENUM_CLOSE_TYPES {
   ALL=1,
   ONLY_ONE_SECTOR=2,
   ONLY_DIFFERENT_SECTOR=3,
};

input ENUM_CLOSE_TYPES prevBarType = ALL;
input double minCloseFilterPercent = 0.7;

//--- indicator buffers
double         BufferSignal[];
double         BufferOpenType[];
double         BufferPrevBarType[];

datetime       date_start=D'01.11.2019';

int prevTick = 0;
double lastClosePercent = 0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferOpenType, INDICATOR_DATA);
   SetIndexBuffer(2, BufferPrevBarType, INDICATOR_DATA);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1) {
         prevTick = prev_calculated;

         //i--;

         BufferSignal[i] = 0;
         BufferOpenType[i] = 0;
         //BufferPrevBarType[i] = 0;

         ResetLastError();

         double fullHeight = high[i] - low[i];
         double closeHeight = close[i] -low[i];
         double openHeight = open[i] -low[i];

         if (fullHeight > 0) {
            double openPercent = openHeight / fullHeight;
            double closePercent = closeHeight / fullHeight;
            lastClosePercent = closePercent;
            
            if (low[i] < low[i-1] && low[i] < low[i-2]) {
               if (closePercent > minCloseFilterPercent) {
                  BufferSignal[i] = closePercent * -1;
                  BufferOpenType[i] = openPercent > 0.5 ? 1 : 2;
               }
            }

            if (high[i] > high[i-1] && high[i] > high[i-2]) {
               if ((1 - closePercent) > minCloseFilterPercent) {
                  BufferSignal[i] = 1 - closePercent;
                  BufferOpenType[i] = openPercent < 0.5 ? 1 : 2;
               }
            }


            // Filter
            /*if (BufferSignal[i] != 0) {
               if (prevBarType == ONLY_ONE_SECTOR && BufferOpenType[i] != 1) {
                  BufferSignal[i] = 0;
               }
               if (prevBarType == ONLY_DIFFERENT_SECTOR && BufferOpenType[i] != 2) {
                  BufferSignal[i] = 0;
               }
            }*/


            if (BufferSignal[i] != 0) {
               if (BufferSignal[i] > 0) {
                  DrawHelper::drawArrow("long:price:" + IntegerToString(i), time[i], high[i] + 30 * _Point, 172, clrRed, 0, 0, 14);
               }
               if (BufferSignal[i] < 0) {
                  DrawHelper::drawArrow("short:price:" + IntegerToString(i), time[i], low[i] - 15 * _Point, 172, clrGreen, 0, 0, 14);
               }
               
               DrawHelper::drawLabel("labelSignal", "signal: " + DoubleToString(BufferSignal[i], 2), 20, 20);
               DrawHelper::drawLabel("label1", "close(p): " + DoubleToString(lastClosePercent, 2), 20, 40);
               DrawHelper::drawLabel("label2", "min percent: " + DoubleToString(minCloseFilterPercent, 2), 20, 60);
            }
         }
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
