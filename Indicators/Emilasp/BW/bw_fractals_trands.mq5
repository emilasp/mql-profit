//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 5
#property indicator_plots   5
#property indicator_maximum 4
#property indicator_minimum -4
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -1
#property indicator_level2 1
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>

enum ENUM_FRACTAL_TYPES {
   THREE=2,
   FIVE=3,
   FIVE_STRICT=4,
};

input ENUM_FRACTAL_TYPES fractalType = FIVE;

input ENUM_TIMEFRAMES tf0 = PERIOD_CURRENT;
input ENUM_TIMEFRAMES tf1 = PERIOD_M15;
input ENUM_TIMEFRAMES tf2 = PERIOD_H1;
input ENUM_TIMEFRAMES tf3 = PERIOD_H4;


//--- indicator buffers
double         BufferSignal[];
double         BufferSignal1[];
double         BufferSignal2[];
double         BufferSignal3[];
double         BufferSignal4[];

datetime       date_start=D'01.11.2019';

int prevTick = 0;
int handle0, handle1, handle2, handle3;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferSignal1, INDICATOR_DATA);
   SetIndexBuffer(2, BufferSignal2, INDICATOR_DATA);
   SetIndexBuffer(3, BufferSignal3, INDICATOR_DATA);
   SetIndexBuffer(4, BufferSignal4, INDICATOR_DATA);
   
   handle0 = iCustom(_Symbol, tf0, "Emilasp\\BW\\bw_fractals", fractalType);
   handle1 = iCustom(_Symbol, tf1, "Emilasp\\BW\\bw_fractals", fractalType);
   handle2 = iCustom(_Symbol, tf2, "Emilasp\\BW\\bw_fractals", fractalType);
   handle3 = iCustom(_Symbol, tf3, "Emilasp\\BW\\bw_fractals", fractalType);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         prevTick = prev_calculated;

         double buffer0[];
         double buffer1[];
         double buffer2[];
         double buffer3[];
         ArraySetAsSeries(buffer0, true);
         ArraySetAsSeries(buffer1, true);
         ArraySetAsSeries(buffer2, true);
         ArraySetAsSeries(buffer3, true);
         CopyBuffer(handle0, 2, time[i], 20, buffer0);
         CopyBuffer(handle1, 2, time[i], 20, buffer1);
         CopyBuffer(handle2, 2, time[i], 20, buffer2);
         CopyBuffer(handle3, 2, time[i], 20, buffer3);

         ResetLastError();
                  
         BufferSignal1[i] = buffer0[1];
         BufferSignal2[i] = buffer1[1];
         BufferSignal3[i] = buffer2[1];
         BufferSignal4[i] = buffer3[1];
         
         BufferSignal[i] = BufferSignal1[i] + BufferSignal2[i] + BufferSignal3[i] + BufferSignal4[i];

         DrawHelper::drawLabel("labelSignal1", "TF: " + DoubleToString(BufferSignal1[i], 0),   100, 20);
         DrawHelper::drawLabel("labelSignal2", "TF2: " + DoubleToString(BufferSignal2[i], 0), 100, 40);
         DrawHelper::drawLabel("labelSignal3", "TF3: " + DoubleToString(BufferSignal3[i], 0), 100, 60);
         DrawHelper::drawLabel("labelSignal4", "TF4: " + DoubleToString(BufferSignal4[i], 0), 100, 80);
         DrawHelper::drawLabel("labelSignalSum", "Sum: " + DoubleToString(BufferSignal[i], 0), 100, 110);
         
         // Filter
         /*if (BufferSignal[i] != 0) {
            if (prevBarType == ONLY_ONE_SECTOR && BufferOpenType[i] != 1) {
               BufferSignal[i] = 0;
            }
            if (prevBarType == ONLY_DIFFERENT_SECTOR && BufferOpenType[i] != 2) {
               BufferSignal[i] = 0;
            }
         }*/
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
