//+------------------------------------------------------------------+
//|                                              Demo_iAlligator.mq5 |
//|                        Copyright 2011, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2011, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property description "The indicator demonstrates how to obtain data"
#property description "of indicator buffers for the iAlligator technical indicator."
#property description "A symbol and timeframe used for calculation of the indicator,"
#property description "are set by the symbol and period parameters."
#property description "The method of creation of the handle is set through the 'type' parameter (function type)."
#property description "All the other parameters are similar to the standard Alligator."


#include <Emilasp\Indicators\SuperSystemIndicator\AlligatorIndicator.mqh>

#property indicator_chart_window
#property indicator_buffers 8
#property indicator_plots   8
//--- plot Jaws
#property indicator_label1  "Jaws"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Teeth
#property indicator_label2  "Teeth"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Lips
#property indicator_label3  "Lips"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrLime
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1

//--- input parameters

input int historyBarsAnalizeCount = 200; // количество баров для поиска минимального и максимального значения аллигатора(в начале)
input double flatPercentByMin = 1.5; // Считаем, что флет если дистанция аллигатор меньше чем flatPercentByMin от minDistance
input int trandByFlatDistance = 6; // Через сколько баров после пересечения считаем, что тренд наступил.
input int angleGoodColor = 20;

input int                  jaw_period=13;          // period of the Jaw line
input int                  jaw_shift=8;            // shift of the Jaw line
input int                  teeth_period=8;         // period of the Teeth line
input int                  teeth_shift=5;          // shift of the Teeth line
input int                  lips_period=5;          // period of the Lips line
input int                  lips_shift=3;           // shift of the Lips line
input ENUM_MA_METHOD       MA_method=MODE_SMMA;    // method of averaging of the Alligator lines
input ENUM_APPLIED_PRICE   applied_price=PRICE_MEDIAN;// type of price used for calculation of Alligator
//--- indicator buffers

AlligatorIndicator *indicator = new AlligatorIndicator;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{

   indicator.OnInit("Alligator", jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, MA_method, applied_price, historyBarsAnalizeCount, flatPercentByMin, trandByFlatDistance, angleGoodColor);

//--- show the symbol/timeframe the Alligator indicator is calculated for
   string short_name=StringFormat("iAlligator(%s/%s, %d,%d,%d,%d,%d,%d)", _Symbol, EnumToString(_Period), jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift);
   IndicatorSetString(INDICATOR_SHORTNAME, short_name);
//--- normal initialization of the indicator
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
   return(indicator.OnCalculate(rates_total, prev_calculated, time, open, high, low, close, tick_volume, volume, spread));
}

//+------------------------------------------------------------------+
//| Indicator deinitialization function                              |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   indicator.OnDeinit(reason);
}
//+------------------------------------------------------------------+
