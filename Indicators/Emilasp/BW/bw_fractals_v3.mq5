//+------------------------------------------------------------------+
//|                                              Demo_iAlligator.mq5 |
//|                        Copyright 2011, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2011, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property description "The indicator demonstrates how to obtain data"
#property description "of indicator buffers for the iAlligator technical indicator."
#property description "A symbol and timeframe used for calculation of the indicator,"
#property description "are set by the symbol and period parameters."
#property description "The method of creation of the handle is set through the 'type' parameter (function type)."
#property description "All the other parameters are similar to the standard Alligator."

#property indicator_chart_window
#property indicator_buffers 6
#property indicator_plots   6
//--- plot Jaws
#property indicator_label1  "Jaws"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Teeth
#property indicator_label2  "Teeth"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Lips
#property indicator_label3  "Lips"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrLime
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1

//////////////////////////////////
//////////////////////////////////

#include <Emilasp\Fractals\FractalsCollection.mqh>
#include <Emilasp\helpers\DrawHelper.mqh>

input ENUM_FRACTAL_TYPES fractalType = FRACTAL_TYPE_FIVE;
input int patternType = 1;

input int filterMinDistananceFractalsPt = 20;
input int filterAlligatorMinDistancePt = -1;
input bool isModeAdditionalFractals = false;

////////// System
int handleAlligator;

FractalsCollection fractalsAll;
FractalsCollection fractalsOdd;

Fractal lastFractalLine;
int lastTrandSignal = 0;
int breakAfterSignalCount = 0;

datetime       date_start=D'01.11.2019';
int prevTick = 0;

//////////--- indicator buffers
double         BufferSignal[];
double         BufferType[];
double         BufferTrand[];

//////////////////////////////////
//////////////////////////////////


//+------------------------------------------------------------------+
//| Enumeration of the methods of handle creation                    |
//+------------------------------------------------------------------+

//--- input parameters
input string               symbol=" ";             // symbol
input ENUM_TIMEFRAMES      period=PERIOD_CURRENT;  // timeframe
input int                  jaw_period=13;          // period of the Jaw line
input int                  jaw_shift=8;            // shift of the Jaw line
input int                  teeth_period=8;         // period of the Teeth line
input int                  teeth_shift=5;          // shift of the Teeth line
input int                  lips_period=5;          // period of the Lips line
input int                  lips_shift=3;           // shift of the Lips line
input ENUM_MA_METHOD       MA_method=MODE_SMMA;    // method of averaging of the Alligator lines
input ENUM_APPLIED_PRICE   applied_price=PRICE_MEDIAN;// type of price used for calculation of Alligator
//--- indicator buffers
double         JawsBuffer[];
double         TeethBuffer[];
double         LipsBuffer[];
//--- variable for storing the handle of the iAlligator indicator
int    handle;
//--- variable for storing
string name=symbol;
//--- name of the indicator on a chart
string short_name;
//--- we will keep the number of values in the Alligator indicator
int    bars_calculated=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- assignment of arrays to indicator buffers
   SetIndexBuffer(0, JawsBuffer, INDICATOR_DATA);
   SetIndexBuffer(1, TeethBuffer, INDICATOR_DATA);
   SetIndexBuffer(2, LipsBuffer, INDICATOR_DATA);

   SetIndexBuffer(3, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(4, BufferType, INDICATOR_DATA);
   SetIndexBuffer(5, BufferTrand, INDICATOR_DATA);

//--- set shift of each line
   PlotIndexSetInteger(0, PLOT_SHIFT, jaw_shift);
   PlotIndexSetInteger(1, PLOT_SHIFT, teeth_shift);
   PlotIndexSetInteger(2, PLOT_SHIFT, lips_shift);

   handle=iAlligator(_Symbol, period, jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, MA_method, applied_price);

   if(handle==INVALID_HANDLE) {
      PrintFormat("Failed to create handle of the iAlligator indicator for the symbol %s/%s, error code %d", name, EnumToString(period), GetLastError());
      return(INIT_FAILED);
   }
//--- show the symbol/timeframe the Alligator indicator is calculated for
   short_name=StringFormat("iAlligator(%s/%s, %d,%d,%d,%d,%d,%d)", _Symbol, EnumToString(period),
                           jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift);
   IndicatorSetString(INDICATOR_SHORTNAME, short_name);
//--- normal initialization of the indicator
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
//--- number of values copied from the iAlligator indicator
   int values_to_copy;
//--- determine the number of values calculated in the indicator
   int calculated=BarsCalculated(handle);
   if(calculated<=0) {
      PrintFormat("BarsCalculated() returned %d, error code %d", calculated, GetLastError());
      return(0);
   }
//--- if it is the first start of calculation of the indicator or if the number of values in the iAlligator indicator changed
//---or if it is necessary to calculated the indicator for two or more bars (it means something has changed in the price history)
   if(prev_calculated==0 || calculated!=bars_calculated || rates_total>prev_calculated+1) {
      //--- if the JawsBuffer array is greater than the number of values in the iAlligator indicator for symbol/period, then we don't copy everything
      //--- otherwise, we copy less than the size of indicator buffers
      if(calculated>rates_total)
         values_to_copy=rates_total;
      else
         values_to_copy=calculated;
   } else {
      //--- it means that it's not the first time of the indicator calculation, and since the last call of OnCalculate()
      //--- for calculation not more than one bar is added
      values_to_copy=(rates_total-prev_calculated)+1;
   }
//--- fill the arrays with values of the Alligator indicator
//--- if FillArraysFromBuffer returns false, it means the information is nor ready yet, quit operation
   if(!FillArraysFromBuffers(JawsBuffer, jaw_shift, TeethBuffer, teeth_shift, LipsBuffer, lips_shift, handle, values_to_copy)) return(0);
//--- form the message
   string comm=StringFormat("%s ==>  Updated value in the indicator %s: %d", TimeToString(TimeCurrent(), TIME_DATE|TIME_SECONDS), short_name, values_to_copy);
//--- display the service message on the chart
   Comment(comm);



///////////////////////////////
///////////////////////////////

   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         BufferSignal[i] = 0;
         BufferType[i] = 0;
         BufferTrand[i] = lastTrandSignal;

         ResetLastError();

         // Get fractal
         Fractal fractal = fractalsAll.getFractal(fractalType, i, time, high, low);

         // Filter by alligator
         if (!fractal.isEmpty && filterAlligatorMinDistancePt > -1) {
            //filterByAlligator(i, fractal, filterAlligatorMinDistancePt);
         }

         
         if (ArraySize(fractalsOdd.fractals) >= 2) {
            Fractal lastIddFractal = fractalsOdd.fractals[1];
            double avgBar = getAvgBar(i, 100, high, low);
            double barsKoef = 2;
            if (fractalsOdd.fractals[0].direct != fractal.direct) {
               if (avgBar * barsKoef > MathAbs(lastFractalLine.price - fractal.price)) {
                  fractal.isEmpty = true;
               }
            }
            
             double priceToAllow = lastFractalLine.direct == 1 ? lastFractalLine.price - avgBar * barsKoef : lastFractalLine.price + avgBar * barsKoef;
             DrawHelper::drawTrendLine(0, 0, "Trand2pLine22", lastFractalLine.time, priceToAllow, time[i], priceToAllow, clrGreen, STYLE_DASHDOTDOT, 2);
         }


         // Process fractal
         if (!fractal.isEmpty) {
            // add fractals to all
            fractalsAll.Add(fractal, true);

            // add fractals to odd
            if (ArraySize(fractalsOdd.fractals) == 0) {
               fractalsOdd.Add(fractal);
            } else {
               if (fractalsOdd.fractals[0].direct != fractal.direct) {
                  //double avgBar = getAvgBar(i, 40, high, low);
                  //double barsKoef = 2;
                  //if (avgBar * barsKoef < MathAbs(fractalsOdd.fractals[0].price - fractal.price)) {
                  //   fractalsOdd.Add(fractal, true);
                  //} else {
                  //   fractal.isEmpty = true;
                  //}

                  int distance = int(MathAbs(fractalsOdd.fractals[0].price - fractal.price) / _Point);
                  if (distance >= filterMinDistananceFractalsPt) {
                     fractalsOdd.Add(fractal, true);
                  }
               } else {
                  // MAx min
                  if (isModeAdditionalFractals) {
                     Fractal last = fractalsOdd.fractals[0];
                     if (fractal.direct = 1 && fractal.price > last.price) {
                        fractalsOdd.fractals[0] = fractal;
                     }
                     if (fractal.direct = -1 && fractal.price < last.price) {
                        fractalsOdd.fractals[0] = fractal;
                     }
                  }
               }

               //
            }
         }

         drawFractal(fractal);


         // Signals
         if (ArraySize(fractalsOdd.fractals) >= 2) {
            Fractal fractalPrev = fractalsOdd.fractals[1];

            for(int j=0; j<ArraySize(fractalsOdd.fractals); j++) {
               Fractal fractalOdd = fractalsOdd.fractals[j];
               if (lastTrandSignal == 1 && fractalOdd.direct == -1) {
                  if (lastFractalLine.time != fractalOdd.time) {
                     lastFractalLine = fractalPrev;
                     DrawHelper::drawTrendLine(0, 0, "Trand2pLine" + lastFractalLine.time, lastFractalLine.time, lastFractalLine.price, time[i], lastFractalLine.price, clrGray, STYLE_DOT, 2);
                  }
                  fractalPrev = fractalOdd;
                  break;
               }
               if (lastTrandSignal == -1 && fractalOdd.direct == 1) {
                  if (lastFractalLine.time != fractalOdd.time) {
                     lastFractalLine = fractalPrev;
                     DrawHelper::drawTrendLine(0, 0, "Trand2pLine" + lastFractalLine.time, lastFractalLine.time, lastFractalLine.price, time[i], lastFractalLine.price, clrGray, STYLE_DOT, 2);
                  }
                  fractalPrev = fractalOdd;
                  break;
               }
            }

            if (fractalPrev.direct == 1 && close[i] > fractalPrev.price) {
               if(lastTrandSignal != 1)
                  breakAfterSignalCount=0;
               else
                  breakAfterSignalCount++;
               lastTrandSignal = 1;
            }

            if (fractalPrev.direct == -1 && close[i] < fractalPrev.price) {
               if(lastTrandSignal != -1)
                  breakAfterSignalCount=0;
               else
                  breakAfterSignalCount++;
               lastTrandSignal = -1;
            }


            if (!fractalPrev.isEmpty) {
               DrawHelper::drawLabel("labelTrand2", "trand: " + DoubleToString(lastTrandSignal) + ", count: " + IntegerToString(breakAfterSignalCount), 200, 20, 3);
               DrawHelper::drawLabel("labelTrand2p", "price: " + DoubleToString(fractalPrev.price, 5), 200, 40, 3);
               DrawHelper::drawTrendLine(0, 0, "Trand2pLine", fractalPrev.time, fractalPrev.price, time[i], fractalPrev.price, clrGreenYellow, STYLE_DOT, 2);
            }
         }

         BufferTrand[i] = lastTrandSignal;


         // Draw last Fractals Odd
         if (ArraySize(fractalsOdd.fractals) >=2) {
            for(int j=0; j < 2; j++) {
               int colorArrow = fractalsOdd.fractals[j].direct < 0 ? clrGreen : clrRed;
               int iconArrow = fractalsOdd.fractals[j].direct < 0 ? 233 : 234;
               DrawHelper::drawArrow("fr-" + IntegerToString(j), fractalsOdd.fractals[j].time, fractalsOdd.fractals[j].price, iconArrow, colorArrow);
            }
         }
      }
   }
///////////////////////////////
///////////////////////////////


//--- memorize the number of values in the Alligator indicator
   bars_calculated=calculated;
//--- return the prev_calculated value for the next call
   return(rates_total);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawFractal(Fractal &fractal)
{
   int colorArrow = clrGray;

   // Draw fractals
   if (fractal.direct > 0) {
      colorArrow = !fractal.isEmpty ? clrRed : colorArrow;
      DrawHelper::drawArrow("long:price:" + TimeToString(fractal.time), fractal.time, fractal.price + 20 * _Point, 217, colorArrow, 0, 0, 14);
   }
   if (fractal.direct < 0) {
      colorArrow = !fractal.isEmpty ? clrGreen : colorArrow;
      DrawHelper::drawArrow("short:price:" + TimeToString(fractal.time), fractal.time, fractal.price - 7 * _Point, 218, colorArrow, 0, 0, 14);
   }
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getAvgBar(int currIndex, int historyBars, const double &high[], const double &low[])
{
   double summ = 0;
   for(int i=0; i<historyBars; i++) {
      summ += high[currIndex-i] - low[currIndex-i];
   }

   return summ / historyBars;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool filterByAlligator(int curIndex, Fractal &fractal, int minDistance)
{
   double max = JawsBuffer[curIndex] > LipsBuffer[curIndex] ? JawsBuffer[curIndex] : LipsBuffer[curIndex];
   double min = JawsBuffer[curIndex] < LipsBuffer[curIndex] ? JawsBuffer[curIndex] : LipsBuffer[curIndex];

   max = min = LipsBuffer[curIndex];

   if (fractal.direct == 1 && fractal.high < (max + minDistance * _Point)) {
      fractal.isEmpty = true;
   }
   if (fractal.direct == -1 && fractal.low > (min - minDistance * _Point)) {
      fractal.isEmpty = true;
   }
   return true;
}
//+------------------------------------------------------------------+




//+------------------------------------------------------------------+
//| Filling indicator buffers from the iAlligator indicator          |
//+------------------------------------------------------------------+
bool FillArraysFromBuffers(double &jaws_buffer[],  // indicator buffer for the Jaw line
                           int j_shift,            // shift of the Jaw line
                           double &teeth_buffer[], // indicator buffer for the Teeth line
                           int t_shift,            // shift of the Teeth line
                           double &lips_buffer[],  // indicator buffer for the Lips line
                           int l_shift,            // shift of the Lips line
                           int ind_handle,         // handle of the iAlligator indicator
                           int amount              // number of copied values
                          )
{
   ResetLastError();
   if(CopyBuffer(ind_handle, 0, -j_shift, amount, jaws_buffer)<0) {
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      return(false);
   }
   if(CopyBuffer(ind_handle, 1, -t_shift, amount, teeth_buffer)<0) {
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      return(false);
   }
   if(CopyBuffer(ind_handle, 2, -l_shift, amount, lips_buffer)<0) {
      PrintFormat("Failed to copy data from the iAlligator indicator, error code %d", GetLastError());
      return(false);
   }
   return(true);
}
//+------------------------------------------------------------------+
//| Indicator deinitialization function                              |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   if(handle!=INVALID_HANDLE)
      IndicatorRelease(handle);
//--- clear the chart after deleting the indicator
   Comment("");
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
