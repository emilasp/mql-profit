//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window //indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   3
#property indicator_maximum 1.1
#property indicator_minimum -1.1
//--- значения горизонтальных уровней индикатора
#property indicator_level1 -0.5
#property indicator_level2 0.5
//--- в качестве цвета линии горизонтального уровня использован синий цвет
#property indicator_levelcolor clrAliceBlue
//--- в линии горизонтального уровня использован короткий штрих-пунктир
#property indicator_levelstyle STYLE_DASHDOTDOT


#property indicator_label1  "IND"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\DrawHelper.mqh>


enum ENUM_FRACTAL_TYPES {
   FRACTAL_TYPE_THREE=3,
   FRACTAL_TYPE_FIVE=5,
   FRACTAL_TYPE_FIVE_STRICT=6,
};

input ENUM_FRACTAL_TYPES fractalType = FRACTAL_TYPE_FIVE;

//--- indicator buffers
double         BufferSignal[];
double         BufferType[];
double         BufferTrand[];

datetime       date_start=D'01.11.2019';

int prevTick = 0;
int lastTrand = 0;
double lastFractalHigh = 0;
double lastFractalLow = 0;

double lastFractalsSignal[3];
double lastFractalsPrices[3];
double lastFractalsTimes[3];
double lastTrandSignal = EMPTY_VALUE;
double lastTrandPattern = EMPTY_VALUE;
double lastTrandPrice = EMPTY_VALUE;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0, BufferSignal, INDICATOR_DATA);
   SetIndexBuffer(1, BufferType, INDICATOR_DATA);
   SetIndexBuffer(2, BufferTrand, INDICATOR_DATA);
//---

   ArrayFill(lastFractalsPrices, 0, 3, EMPTY_VALUE);

   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      if(time[i]<date_start)
         continue;

      // NEW BAR
      if(rates_total - prev_calculated == 1 && prevTick != prev_calculated) {
         prevTick = prev_calculated;

         BufferSignal[i] = 0;
         BufferType[i] = 0;
         BufferTrand[i] = lastTrand;

         ResetLastError();

         // Up fractal
         double up_0 = high[i];
         double up_1 = high[i-1];
         double up_2 = high[i-2];
         double up_3 = high[i-3];
         double up_4 = high[i-4];


         if (fractalType == FRACTAL_TYPE_THREE) {
            if (up_1 > up_0 && up_1 > up_2) {
               BufferSignal[i-1] = 1;
               BufferType[i-1] = 2;
            }
         }

         if (up_2 > up_1 && up_2 > up_3) {
            if (fractalType == FRACTAL_TYPE_FIVE_STRICT) {
               if (up_1 > up_0 && up_3 > up_4) {
                  BufferSignal[i-2] = 1;
                  BufferType[i-2] = 4;
               }
            }
            if (fractalType == FRACTAL_TYPE_FIVE) {
               if (up_2 > up_0 && up_2 > up_4) {
                  BufferSignal[i-2] = 1;
                  BufferType[i-2] = 3;
               }
            }
         }

         // DWN fractal
         double dw_0 = low[i];
         double dw_1 = low[i-1];
         double dw_2 = low[i-2];
         double dw_3 = low[i-3];
         double dw_4 = low[i-4];

         int centralNum = fractalType == FRACTAL_TYPE_THREE ? i-1 : i-2;

         if (fractalType == FRACTAL_TYPE_THREE) {
            if (dw_1 < dw_0 && dw_1 < dw_2) {
               BufferSignal[centralNum] = -1;
               BufferType[centralNum] = 2;
            }
         }

         if (dw_2 < dw_1 && dw_2 < dw_3) {
            if (fractalType == FRACTAL_TYPE_FIVE_STRICT) {
               if (dw_1 < dw_0 && dw_3 < dw_4) {
                  BufferSignal[centralNum] = -1;
                  BufferType[centralNum] = 4;
               }
            }
            if (fractalType == FRACTAL_TYPE_FIVE) {
               if (dw_2 < dw_0 && dw_2 < dw_4) {
                  BufferSignal[centralNum] = -1;
                  BufferType[centralNum] = 3;
               }
            }
         }

         if (BufferSignal[centralNum] != 0) {
            if (BufferSignal[centralNum] > 0) {
               lastFractalHigh = high[centralNum];

               DrawHelper::drawArrow("long:price:" + IntegerToString(i), time[centralNum], high[centralNum] + 20 * _Point, 172, clrRed, 0, 0, 14);
            }
            if (BufferSignal[centralNum] < 0) {
               lastFractalLow = low[centralNum];

               DrawHelper::drawArrow("short:price:" + IntegerToString(i), time[centralNum], low[centralNum] - 7 * _Point, 172, clrGreen, 0, 0, 14);
            }



            lastFractalsSignal[2] = lastFractalsSignal[1];
            lastFractalsPrices[2] = lastFractalsPrices[1];
            lastFractalsTimes[2] = lastFractalsTimes[1];
            lastFractalsSignal[1] = lastFractalsSignal[0];
            lastFractalsPrices[1] = lastFractalsPrices[0];
            lastFractalsTimes[1] = lastFractalsTimes[0];

            lastFractalsSignal[0] = BufferSignal[centralNum];
            lastFractalsPrices[0] = BufferSignal[centralNum] > 0 ? lastFractalHigh = high[centralNum] : lastFractalLow;
            lastFractalsTimes[0] = time[centralNum];

            int color1 = lastFractalsSignal[0] < 0 ? clrGreen : clrRed;
            int icon1 = lastFractalsSignal[0] < 0 ? 233 : 234;
            if (lastFractalsPrices[1] != EMPTY_VALUE) {
               //int color3 = lastFractalsSignal[2] < 0 ? clrGreen : clrRed;
               //int icon3 = lastFractalsSignal[2] < 0 ? 233 : 234;
               int color2 = lastFractalsSignal[1] < 0 ? clrGreen : clrRed;
               int icon2 = lastFractalsSignal[1] < 0 ? 233 : 234;

               //DrawHelper::drawArrow("fr-3",lastFractalsTimes[2],lastFractalsPrices[2],icon3,color3);
               DrawHelper::drawArrow("fr-2", lastFractalsTimes[1], lastFractalsPrices[1], icon2, color2);


               if (lastFractalsSignal[1] > 0 && lastFractalsSignal[0] < 0 && lastTrandSignal != 1) {
                  double minPrice = lastFractalsPrices[0];
                  DrawHelper::drawRectangle("fr-rect" + TimeToString(lastFractalsTimes[1]), lastFractalsTimes[0], lastFractalsPrices[1], lastFractalsTimes[0], minPrice, 0, clrRed);

                  lastTrandPattern = -1;
                  lastTrandPrice = lastFractalsPrices[1];
               }
               if (lastFractalsSignal[1] < 0 && lastFractalsSignal[0] > 0 && lastTrandSignal != -1) {
                  double minPrice = lastFractalsPrices[0];
                  DrawHelper::drawRectangle("fr-rect" + TimeToString(lastFractalsTimes[1]), lastFractalsTimes[0], lastFractalsPrices[1], lastFractalsTimes[0], minPrice, 0, clrGreen);

                  lastTrandPattern = 1;
                  lastTrandPrice = lastFractalsPrices[1];
               }

               //if (!(lastFractalsSignal[2] < 0 && lastFractalsSignal[1] > 0 && lastFractalsSignal[0] < 0) && !(lastFractalsSignal[2] > 0 && lastFractalsSignal[1] < 0 && lastFractalsSignal[0] > 0)) {
               //   lastTrandSignal = 0;
               //   lastTrandPattern = EMPTY_VALUE;
               //}

            }
            DrawHelper::drawArrow("fr-1", lastFractalsTimes[0], lastFractalsPrices[0], icon1, color1);

            //DrawHelper::drawLabel("labelSignal", "signal: " + DoubleToString(BufferSignal[i], 2), 20, 20);
            //DrawHelper::drawLabel("label1", "close(p): " + DoubleToString(lastClosePercent, 2), 20, 40);
            //DrawHelper::drawLabel("label2", "min percent: " + DoubleToString(minCloseFilterPercent, 2), 20, 60);
         }

         if (lastTrandPattern == -1 && close[i] > lastTrandPrice) {
            lastTrandSignal = 1;
            DrawHelper::drawLabel("labelTrand2", "trand: " + DoubleToString(lastTrandSignal), 200, 20, 3);
         }

         if (lastTrandPattern == 1 && close[i] < lastTrandPrice) {
            lastTrandSignal = -1;
            DrawHelper::drawLabel("labelTrand2", "trand: " + DoubleToString(lastTrandSignal), 200, 20, 3);
         }

         if (lastTrandPrice != EMPTY_VALUE) {
            DrawHelper::drawLabel("labelTrand2p", "price: " + DoubleToString(lastTrandPrice, 5), 200, 40, 3);
            DrawHelper::drawTrendLine(0, 0, "Trand2p", time[i-10], lastTrandPrice, time[i], lastTrandPrice, clrGreenYellow, STYLE_DOT, 2);
         }

         if (lastTrandSignal != EMPTY_VALUE) {
            BufferTrand[i] = lastTrandSignal;
         }

         //DrawHelper::drawLabel("labelTrand", "trand: " + DoubleToString(BufferTrand[i], 2), 20, 20);
         //DrawHelper::drawLabel("labelLast1", "last  low: " + DoubleToString(lastFractalLow, 5), 20, 40);
         //DrawHelper::drawLabel("labelLast2", "last high: " + DoubleToString(lastFractalHigh, 5), 20, 60);
      }
   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
