//+------------------------------------------------------------------+
//|                                              em_multy_tframe.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_minimum 0
#property indicator_maximum 100
#property indicator_buffers 2
#property indicator_plots   2
//--- plot BUY
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrCornflowerBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot SELL
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buffers
double         BUYBuffer[];
double         SELLBuffer[];

datetime       date_start=D'01.11.2020';

int total=19;
int Buy=0;
int Sell=0;
ENUM_TIMEFRAMES tf[19] =
  {
   PERIOD_M1,
   PERIOD_M2,
   PERIOD_M3,
   PERIOD_M4,
   PERIOD_M5,
   PERIOD_M6,
   PERIOD_M10,
   PERIOD_M12,
   PERIOD_M15,
   PERIOD_M20,
   PERIOD_M30,
   PERIOD_H1,
   PERIOD_H2,
   PERIOD_H3,
   PERIOD_H4,
   PERIOD_H6,
   PERIOD_H8,
   PERIOD_H12,
   PERIOD_D1,
  };



//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,BUYBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,SELLBuffer,INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   if(prev_calculated > 0)
   {
     int i = prev_calculated-1;
      
      Buy=0;
      Sell=0;
      
      BUYBuffer[i] = 0;
      SELLBuffer[i] = 0;
      
      for(int j=0;j<total;j++)
        {
         double H[];
         double L[];
         ArraySetAsSeries(H, true);
         ArraySetAsSeries(L, true);
         int res = CopyHigh(_Symbol,tf[j],time[i],2,H);
         if(res < 0)
            break;
         res = CopyLow(_Symbol,tf[j],time[i],2,L);
         if(res < 0)
            break;
         //--- Buy
         if(L[0] < L[1])
           Buy++;
         //--- Sell
          if(H[0] > H[1])
           Sell++;
        }
        
        BUYBuffer[i] = Buy/19.0*100.0;
        SELLBuffer[i] = Sell/19.0*100.0;
     }

   


//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
