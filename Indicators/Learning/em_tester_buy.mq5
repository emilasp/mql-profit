//+------------------------------------------------------------------+
//|                                                em_tester_buy.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_minimum -15
#property indicator_maximum 500
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Quant
#property indicator_label1  "Quant"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot TP
#property indicator_label2  "TP"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot SL
#property indicator_label3  "SL"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1


input int SL=100;
input int TP=100;
input datetime date_start=D'01.01.2020';

//--- indicator buffers
double         QuantBuffer[];
double         TPBuffer[];
double         SLBuffer[];

int h;
int prev_quant=-1;
int sum_TP=0;
int sum_SL=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
//--- indicator buffers mapping
   SetIndexBuffer(0,QuantBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,TPBuffer,INDICATOR_DATA);
   SetIndexBuffer(2,SLBuffer,INDICATOR_DATA);

   h=iCustom(_Symbol,_Period,"Learning/em_tester v4", SL, TP);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++) {
      int q=Quant(time[i]);
      if(prev_quant!=q) {
         prev_quant=q;
         sum_TP=0;
         sum_SL=0;
      }

      QuantBuffer[i]=q;
      
      double profit[];
      CopyBuffer(h,0,time[i],1, profit);
      if(profit[0] > 3)
         sum_TP++;
      
      if(profit[0] < 3)
         sum_SL++;
      
      TPBuffer[i] = QuantBuffer[i] + sum_TP;
      SLBuffer[i] = QuantBuffer[i] - sum_SL;
   }
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
//---

}
//+------------------------------------------------------------------+
int Quant(datetime t)
{
   int res=0;

   MqlDateTime time;
   TimeToStruct(t, time);

   double minut = (time.day_of_week - 1) * 24 * 60 + time.hour * 60 + time.min;
   double q = MathFloor(minut / 15);
   res = (int)q;
   return res;
}
//+------------------------------------------------------------------+
