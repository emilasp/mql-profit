//+------------------------------------------------------------------+
//|                                                       Tester.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_levelstyle STYLE_DASHDOT
#property indicator_minimum -7
#property indicator_maximum 7
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
#property indicator_level1  3
//--- plot Label2
#property indicator_label2  "SIGNAL"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1

#property indicator_level2  0
//--- plot Label3
#property indicator_label3  "SELL"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
#property indicator_level3  -3
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];

// BUY: open H -> close L; SELL: open L -> close H
input int SL=100; // SL in pt
input int TP=100; // TP in pt

datetime date_start=D'01.01.2020';

int iSell = 1;
int prev_iBay = 0;
int iBuy  = 1;
int prev_iSell = 0;

bool on_find_buy  = true;
bool on_find_sell = true;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);

   IndicatorSetString(INDICATOR_LEVELTEXT, 0, "track BUY");
   IndicatorSetString(INDICATOR_LEVELTEXT, 1, "track SIGNAL");
   IndicatorSetString(INDICATOR_LEVELTEXT, 2, "track SELL");
   IndicatorSetInteger(INDICATOR_DIGITS, 0);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

   for(int i=prev_calculated>0?prev_calculated-iBuy:0; i<rates_total; i++)
     {
      if(time[i] < date_start)
         continue;

      double SL_Buy = high[i] - SL * _Point;
      double TP_Buy = high[i] + TP * _Point;

      Label1Buffer[i] = 3;
      Label2Buffer[i] = 0;

      for(int j=i; j<rates_total; j++)
        {
         //-- BUY
         if(TP_Buy < low[j])
           {
            Label1Buffer[i] = 4;
            break;
           }
         if(SL_Buy > high[j])
           {
            Label1Buffer[i] = 2;
            break;
           }
        }

      if(Label1Buffer[i] == 3 && on_find_buy)
        {
         iBuy = i;
         on_find_buy = false;
        }
      if(Label1Buffer[i] != 3 && i==iBuy)
        {
         iBuy++;
         on_find_buy = true;
        }
     }

   for(int i=prev_calculated>0?prev_calculated-iSell:0; i<rates_total; i++)
     {
      if(time[i] < date_start)
         continue;

      double SL_Sell = low[i] + SL * _Point;
      double TP_Sell = low[i] - TP * _Point;

      //Label2Buffer[i] = 0;
      Label3Buffer[i] = -3;

      for(int j=i; j<rates_total; j++)
        {
         //-- BUY
         if(TP_Sell > high[j])
           {
            Label3Buffer[i] = -2;
            break;
           }
         if(SL_Sell < low[j])
           {
            Label3Buffer[i] = -4;
            break;
           }
        }

      if(Label1Buffer[i] == -3 && on_find_sell)
        {
         iSell = i;
         on_find_sell = false;
        }
      if(Label1Buffer[i] != -3 && i==iBuy)
        {
         iSell++;
         on_find_sell = true;
        }
     }

//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
