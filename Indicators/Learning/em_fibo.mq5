//+------------------------------------------------------------------+
//|                                                      em_fibo.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 11
#property indicator_plots   11
//--- plot Label1
#property indicator_label1  "MA"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "fibo +1"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_DOT
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "fibo +2"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_DOT
#property indicator_width3  1
//--- plot Label4
#property indicator_label4  "fibo +3"
#property indicator_type4   DRAW_LINE
#property indicator_color4  clrRed
#property indicator_style4  STYLE_DOT
#property indicator_width4  1
//--- plot Label5
#property indicator_label5  "fibo +4"
#property indicator_type5   DRAW_LINE
#property indicator_color5  clrRed
#property indicator_style5  STYLE_DOT
#property indicator_width5  1
//--- plot Label6
#property indicator_label6  "fibo +5"
#property indicator_type6   DRAW_LINE
#property indicator_color6  clrRed
#property indicator_style6  STYLE_DOT
#property indicator_width6  1



//--- plot Label7
#property indicator_label7  "fibo -1"
#property indicator_type7   DRAW_LINE
#property indicator_color7  clrRed
#property indicator_style7  STYLE_DOT
#property indicator_width7  1
//--- plot Label8
#property indicator_label8  "fibo -2"
#property indicator_type8   DRAW_LINE
#property indicator_color8  clrRed
#property indicator_style8  STYLE_DOT
#property indicator_width8  1
//--- plot Label9
#property indicator_label9  "fibo -3"
#property indicator_type9   DRAW_LINE
#property indicator_color9  clrRed
#property indicator_style9  STYLE_DOT
#property indicator_width9  1
//--- plot Label10
#property indicator_label10  "fibo -4"
#property indicator_type10   DRAW_LINE
#property indicator_color10  clrRed
#property indicator_style10  STYLE_DOT
#property indicator_width10  1
//--- plot Label11
#property indicator_label11  "fibo -5"
#property indicator_type11   DRAW_LINE
#property indicator_color11  clrRed
#property indicator_style11  STYLE_DOT
#property indicator_width11  1
//--- indicator buffers
double         Label1Buffer[]; // MA

double         Label2Buffer[]; // +21
double         Label3Buffer[]; // +34
double         Label4Buffer[]; // +55
double         Label5Buffer[]; // +89
double         Label6Buffer[]; // +144

double         Label7Buffer[];  // -21
double         Label8Buffer[];  // -34
double         Label9Buffer[];  // -55
double         Label10Buffer[]; // -89
double         Label11Buffer[]; // -144

datetime       date_start=D'01.11.2020';

input int period=144; //169


int handle;

int FI[5] ={21, 34, 55, 89, 144};


//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,Label4Buffer,INDICATOR_DATA);
   SetIndexBuffer(4,Label5Buffer,INDICATOR_DATA);
   SetIndexBuffer(5,Label6Buffer,INDICATOR_DATA);
   SetIndexBuffer(6,Label7Buffer,INDICATOR_DATA);
   SetIndexBuffer(7,Label8Buffer,INDICATOR_DATA);
   SetIndexBuffer(8,Label9Buffer,INDICATOR_DATA);
   SetIndexBuffer(9,Label10Buffer,INDICATOR_DATA);
   SetIndexBuffer(10,Label11Buffer,INDICATOR_DATA);
   
   handle = iMA(_Symbol, _Period, period, 0, MODE_SMA, PRICE_CLOSE);
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      //if(time[i]<date_start)
      //   continue;
         // 21 34 55 89 144
         double MA[];
         CopyBuffer(handle,0,time[i],1,MA);
         
         Label1Buffer[i] = MA[0];
         
         // 21
         Label2Buffer[i] = Label1Buffer[i] + FI[0] * _Point;
         Label7Buffer[i] = Label1Buffer[i] - FI[0] * _Point;
         // 34
         Label3Buffer[i] = Label1Buffer[i] + FI[1] * _Point;
         Label8Buffer[i] = Label1Buffer[i] - FI[1] * _Point;
         // 55
         Label4Buffer[i] = Label1Buffer[i] + FI[2] * _Point;
         Label9Buffer[i] = Label1Buffer[i] - FI[2] * _Point;
         // 89
         Label5Buffer[i] = Label1Buffer[i] + FI[3] * _Point;
         Label10Buffer[i] = Label1Buffer[i] - FI[3] * _Point;
         //144
         Label6Buffer[i] = Label1Buffer[i] + FI[4] * _Point;
         Label11Buffer[i] = Label1Buffer[i] - FI[4] * _Point;
         
      }
   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
