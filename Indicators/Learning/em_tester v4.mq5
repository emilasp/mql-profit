//+------------------------------------------------------------------+
//|                                                    em_tester.mq5 |
//|                           Copyright 2020, Sergey Pavlov (DC2008) |
//|                              http://www.mql5.com/ru/users/dc2008 |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, Sergey Pavlov (DC2008)"
#property link      "http://www.mql5.com/ru/users/dc2008"
#property version   "1.00"
#property indicator_separate_window
#property indicator_levelstyle STYLE_DASHDOT
#property indicator_maximum 5
#property indicator_minimum -5
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
#property indicator_level1  3
//--- plot Label2
#property indicator_label2  "Signal"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
#property indicator_level2  0
//--- plot Label3
#property indicator_label3  "SELL"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
#property indicator_level3  -3
// BUY: open H -> close L; SELL: open L -> close H
//---
input int SL=100;
input int TP=100;
input datetime date_start=D'01.01.2020';
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];

int            iBuy=1;
int            iSell=1;
bool           on_find_Buy=true;
bool           on_find_Sell=true;
double         price_SL;
double         price_TP;

datetime       prev_bar=0;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   price_SL=SL*_Point;
   price_TP=TP*_Point;
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);

   IndicatorSetString(INDICATOR_LEVELTEXT,0," track BUY");
   IndicatorSetString(INDICATOR_LEVELTEXT,1," track SIGNAL");
   IndicatorSetString(INDICATOR_LEVELTEXT,2," track SELL");
   IndicatorSetInteger(INDICATOR_DIGITS,0);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

   int index=iBuy;

   if(iSell < iBuy)
      index=iSell;

   for(int i=prev_calculated>0?prev_calculated-index:0; i<rates_total; i++)
     {
      if(time[i]<date_start)
         continue;

      if(prev_bar < time[i])
        {
         prev_bar = time[i];

         Label2Buffer[i]=0;


         /// BUY
         Label1Buffer[i]=3;
         double SL_Buy=high[i]-price_SL;
         double TP_Buy=high[i]+price_TP;
         for(int j=i; j<rates_total; j++)
           {
            //--- BUY
            if(TP_Buy<low[j])
              {
               Label1Buffer[i]=4;
               break;
              }
            if(SL_Buy>low[j])//
              {
               Label1Buffer[i]=2;
               break;
              }
           }
         if(Label1Buffer[i]==3 && on_find_Buy)
           {
            iBuy=i;
            on_find_Buy=false;
           }
         if(Label1Buffer[i]!=3 && i==iBuy)
           {
            iBuy++;
            on_find_Buy=true;
           }

         /// Sell
         Label3Buffer[i]=-3;
         double SL_Sell=low[i]+price_SL;
         double TP_Sell=low[i]-price_TP;
         for(int j=i; j<rates_total; j++)
           {
            if(TP_Sell>high[j])
              {
               Label3Buffer[i]=-4;
               break;
              }
            if(SL_Sell<high[j])//
              {
               Label3Buffer[i]=-2;
               break;
              }
           }
         if(Label3Buffer[i]==-3 && on_find_Sell)
           {
            iSell=i;
            on_find_Sell=false;
           }
         if(Label3Buffer[i]!=-3 && i==iSell)
           {
            iSell++;
            on_find_Sell=true;
           }
        }
     }


//---
//Comment(
//"index=",index, "\n",
// "     iBuy=", iBuy,  "    Date=",time[iBuy], "\n"
// "     iSell=",iSell, "    Date=",time[iSell]
//);
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
