//+------------------------------------------------------------------+
//|                                                 ExactZZ_Plus.mq5 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property version   "1.0"
#property copyright "2018, Anatoli Kazharski"
#property link      "https://www.mql5.com/ru/users/tol64"
//---
#property indicator_chart_window
#property indicator_buffers 6
#property indicator_plots   5
//---
#property indicator_color1  clrRed
#property indicator_color2  clrCornflowerBlue
#property indicator_color3  clrGold
#property indicator_color4  clrOrangeRed
#property indicator_color5  clrSkyBlue

//--- Внешние параметры
input int   NumberOfBars   =0;       // Number of bars
input int   MinImpulseSize =100;     // Minimum points in a ray
input bool  ShowAskBid     =false;   // Show ask/bid
input bool  ShowAllPoints  =false;   // Show all points
input color RayColor       =clrGold; // Ray color

//--- Индикаторные буферы:
double low_ask_buffer[];    // Минимальная цена Ask
double high_bid_buffer[];   // Максимальная цена Bid
double zz_H_buffer[];       // Максимумы
double zz_L_buffer[];       // Минимумы
double total_zz_h_buffer[]; // Все максимумы
double total_zz_l_buffer[]; // Все минимумы

//--- Для определения, с какого бара производить расчёт
int start=0;
//--- Переменные для ZZ
int    last_zz_max  =0;
int    last_zz_min  =0;
int    direction_zz =0;
double min_low_ask  =0;
double max_high_bid =0;
//---
int      check_bars_calc =0;
datetime first_date      =(_Period==PERIOD_D1 || _Period==PERIOD_W1 || _Period==PERIOD_MN1) ? 0 : D'01.01.2000';
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit(void)
  {
//--- Установим свойства индикатора
   SetPropertiesIndicator();
//--- Инициализация прошла успешно
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,const int prev_calculated,const datetime &time[],
                const double &open[],const double &high[],const double &low[],const double &close[],
                const long &tick_volume[],const long &volume[],const int &spread[])
  {
//--- Предотвращение расчёта на каждом тике
   if(prev_calculated==rates_total)
      return(rates_total);
//--- Если это первый расчёт
   if(prev_calculated==0)
     {
      //--- Обнулим индикаторные буферы
      ZeroIndicatorBuffers();
      //--- Обнуление переменных
      ZeroIndicatorData();
      //--- Проверяет количество доступных данных
      if(!CheckDataAvailable())
         return(0);
      //--- Если данных для копирования указано больше, чем есть, будем использовать столько, сколько есть
      DetermineNumberData();
      //--- Определим, с какого бара начинать отрисовку для каждого символа
      DetermineBeginForCalculate(rates_total);
     }
   else
     {
      //--- Считаем только последнее значение
      start=prev_calculated-1;
      ZeroIndex(start);
      ZeroIndex(prev_calculated);
     }
//--- Заполним индикаторные буферы High Bid и Low Ask
   for(int i=start; i<rates_total; i++)
      FillAskBidBuffers(i,time,high,low,spread);
//--- Заполним индикаторные буферы данными
   for(int i=start; i<rates_total-1; i++)
      FillIndicatorBuffers(i,time);
//--- Вернём размер массива данных
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| Обнуление индикаторных буферов                                   |
//+------------------------------------------------------------------+
void ZeroIndicatorBuffers(void)
  {
   ArrayInitialize(zz_H_buffer,0);
   ArrayInitialize(zz_L_buffer,0);
   ArrayInitialize(low_ask_buffer,0);
   ArrayInitialize(high_bid_buffer,0);
   ArrayInitialize(total_zz_h_buffer,0);
   ArrayInitialize(total_zz_l_buffer,0);
  }
//+------------------------------------------------------------------+
//| Обнуление переменных                                             |
//+------------------------------------------------------------------+
void ZeroIndicatorData(void)
  {
   start       =0;
   last_zz_max =0;
   last_zz_min =0;
  }
//+------------------------------------------------------------------+
//| Обнуление указанного элемента буферов                            |
//+------------------------------------------------------------------+
void ZeroIndex(const int index)
  {
   zz_H_buffer[index]       =0;
   zz_L_buffer[index]       =0;
   total_zz_h_buffer[index] =0;
   total_zz_l_buffer[index] =0;
   low_ask_buffer[index]    =0;
   high_bid_buffer[index]   =0;
  }
//+------------------------------------------------------------------+
//| Устанавливает свойства индикатора                                |
//+------------------------------------------------------------------+
void SetPropertiesIndicator(void)
  {
//--- Установим короткое имя
   IndicatorSetString(INDICATOR_SHORTNAME,"ExactZZ_Plus");
//--- Установим количество знаков
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits);
//--- Определим буферы для отрисовки
   SetIndexBuffer(0,high_bid_buffer,INDICATOR_DATA);
   SetIndexBuffer(1,low_ask_buffer,INDICATOR_DATA);
   SetIndexBuffer(2,zz_H_buffer,INDICATOR_DATA);
   SetIndexBuffer(3,zz_L_buffer,INDICATOR_DATA);
   SetIndexBuffer(4,total_zz_h_buffer,INDICATOR_DATA);
   SetIndexBuffer(5,total_zz_l_buffer,INDICATOR_DATA);
//--- Установим метки
   string text[]={"High Bid","Low Ask","ZZ","Total High ZZ","Total Low ZZ"};
   for(int i=0; i<indicator_plots; i++)
      PlotIndexSetString(i,PLOT_LABEL,text[i]);
//--- Установим тип
   ENUM_DRAW_TYPE draw_type_askbid    =(ShowAskBid)? DRAW_LINE : DRAW_NONE;
   ENUM_DRAW_TYPE draw_type_allpoints =(ShowAllPoints)? DRAW_ARROW : DRAW_NONE;
   PlotIndexSetInteger(0,PLOT_DRAW_TYPE,draw_type_askbid);
   PlotIndexSetInteger(1,PLOT_DRAW_TYPE,draw_type_askbid);
   PlotIndexSetInteger(2,PLOT_DRAW_TYPE,DRAW_ZIGZAG);
   PlotIndexSetInteger(3,PLOT_DRAW_TYPE,draw_type_allpoints);
   PlotIndexSetInteger(4,PLOT_DRAW_TYPE,draw_type_allpoints);
//--- Диск
   PlotIndexSetInteger(3,PLOT_ARROW,159);
   PlotIndexSetInteger(4,PLOT_ARROW,159);
//--- Цвет
   PlotIndexSetInteger(2,PLOT_LINE_COLOR,RayColor);
//--- Установим стиль
   for(int i=0; i<indicator_plots; i++)
      PlotIndexSetInteger(i,PLOT_LINE_STYLE,STYLE_SOLID);
//--- Установим толщину
   for(int i=0; i<indicator_plots; i++)
      PlotIndexSetInteger(i,PLOT_LINE_WIDTH,(i<2)? 1 : 2);
//--- Пустое значение для построения, для которого нет отрисовки
   for(int i=0; i<indicator_plots; i++)
      PlotIndexSetDouble(i,PLOT_EMPTY_VALUE,0.0);
  }
//+------------------------------------------------------------------+
//| Проверяет количество доступных данных у всех символов            |
//+------------------------------------------------------------------+
bool CheckDataAvailable(void)
  {
//--- Обнулим в памяти последнюю ошибку
   ResetLastError();
//--- Получим кол-во баров на текущем таймфрейме
   check_bars_calc=TerminalInfoInteger(TERMINAL_MAXBARS);
//--- Если была ошибка при получении данных, попробуем ещё раз
   if(check_bars_calc<=0)
      return(false);
//---
   return(true);
  }
//+------------------------------------------------------------------+
//| Определяет количество данных для отображения                     |
//+------------------------------------------------------------------+
void DetermineNumberData(void)
  {
//--- Если нужны не все бары
   if(NumberOfBars>0)
     {
      //--- Если указано больше, чем есть, сообщим об этом
      if(NumberOfBars>check_bars_calc)
         printf("%s: Not enough data to calculate! NumberOfBars: %d; Indicator data: %d",
                _Symbol,NumberOfBars,check_bars_calc);
      else
         check_bars_calc=NumberOfBars;
     }
  }
//+------------------------------------------------------------------+
//| Определение номера первого бара для отрисовки                    |
//+------------------------------------------------------------------+
void DetermineBeginForCalculate(const int rates_total)
  {
//--- Если имеется больше данных индикатора, чем есть на текущем символе, то
//    рисуем от первого доступного на текущем символе
   if(check_bars_calc>rates_total)
      start=1;
   else
      start=rates_total-check_bars_calc;
  }
//+------------------------------------------------------------------+
//| Заполняет индикаторные буферы High Bid и Low Ask                 |
//+------------------------------------------------------------------+
void FillAskBidBuffers(const int i,const datetime &time[],const double &high[],const double &low[],const int &spread[])
  {
//--- Выйти, если не дошли до начальной даты
   if(time[i]<first_date)
      return;
//---
   high_bid_buffer[i] =high[i];
   low_ask_buffer[i]  =low[i]+(spread[i]*_Point);
  }
//+------------------------------------------------------------------+
//| Заполняет индикаторные буферы ZZ                                 |
//+------------------------------------------------------------------+
void FillIndicatorBuffers(const int i,const datetime &time[])
  {
   if(time[i]<first_date)
      return;
//--- Если направление ZZ вверх
   if(direction_zz>0)
     {
      //--- Если новый максимум
      if(high_bid_buffer[i]>=max_high_bid)
        {
         zz_H_buffer[last_zz_max] =0;
         last_zz_max              =i;
         max_high_bid             =high_bid_buffer[i];
         zz_H_buffer[i]           =high_bid_buffer[i];
         total_zz_h_buffer[i]     =high_bid_buffer[i];
        }
      //--- Если направление изменилось (вниз)
      else
        {
         if(low_ask_buffer[i]<max_high_bid && 
            fabs(low_ask_buffer[i]-zz_H_buffer[last_zz_max])>MinImpulseSize*_Point)
           {
            last_zz_min          =i;
            direction_zz         =-1;
            min_low_ask          =low_ask_buffer[i];
            zz_L_buffer[i]       =low_ask_buffer[i];
            total_zz_l_buffer[i] =low_ask_buffer[i];
           }
        }
     }
//--- Если направление ZZ вниз
   else
     {
      //--- Если новый минимум
      if(low_ask_buffer[i]<=min_low_ask)
        {
         zz_L_buffer[last_zz_min] =0;
         last_zz_min              =i;
         min_low_ask              =low_ask_buffer[i];
         zz_L_buffer[i]           =low_ask_buffer[i];
         total_zz_l_buffer[i]     =low_ask_buffer[i];
        }
      //--- Если направление изменилось (вверх)
      else
        {
         if(high_bid_buffer[i]>min_low_ask && 
            fabs(high_bid_buffer[i]-zz_L_buffer[last_zz_min])>MinImpulseSize*_Point)
           {
            last_zz_max          =i;
            direction_zz         =1;
            max_high_bid         =high_bid_buffer[i];
            zz_H_buffer[i]       =high_bid_buffer[i];
            total_zz_h_buffer[i] =high_bid_buffer[i];
           }
        }
     }
  }
//+------------------------------------------------------------------+
