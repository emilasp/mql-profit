//+------------------------------------------------------------------+
//|                                                   str_redMan.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];


int handle_MA;
int handle_MACD;
int handle_SAR;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);


   handle_MA=iMA(_Symbol,_Period,7,0,MODE_SMA,PRICE_CLOSE);
   handle_MACD=iMACD(_Symbol,_Period,12,26,9,PRICE_CLOSE);
   handle_SAR=iSAR(_Symbol,_Period,0.02,0.2);

   ChartIndicatorAdd(0,0,handle_MA);
   ChartIndicatorAdd(0,1,handle_MACD);
   ChartIndicatorAdd(0,0,handle_SAR);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<100)
         continue;

      bool buyMA = false;
      bool sellMA = false;
      bool buySAR = false;
      bool sellSAR = false;
      bool buyMACD = false;
      bool sellMACD = false;


      Label1Buffer[i]=0;
      Label2Buffer[i]=0;

      //--- MA
      double MA[];
      ArraySetAsSeries(MA, true);
      CopyBuffer(handle_MA, 0, time[i], 2, MA);

      double SAR[];
      ArraySetAsSeries(SAR, true);
      CopyBuffer(handle_SAR, 0, time[i], 1, SAR);

      double MACD[];
      ArraySetAsSeries(MACD, true);
      CopyBuffer(handle_MACD, 0, time[i], 1, MACD);

      //--- BUY
      if(MA[0] > MA[1])
         if(MA[0] > open[i] && MA[0] < close[i])
            if(MA[0]-open[i] < close[i]-MA[0])
               buyMA = true;
      //--- SELL
      if(MA[0] < MA[1])
         if(MA[0] < open[i] && MA[0] > close[i])
            if(MA[0]-open[i] > close[i]-MA[0])
               sellMA = true;


      //--- BUY
      if(SAR[0] < low[i])
         buySAR = true;
      //--- SELL
      if(SAR[0] > high[i])
         sellSAR = true;

      //--- BUY
      if(MACD[0] > 0)
         buyMACD = true;
      //--- SELL
      if(MACD[0] < 0)
         sellMACD = true;


      if(buyMA && buySAR && buyMACD)
         Label1Buffer[i] = close[i];

      if(sellMA && sellSAR && sellMACD)
         Label2Buffer[i] = close[i];

     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
