//+------------------------------------------------------------------+
//|                                                     em_ch_ma.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Label1
#property indicator_label1  "MA"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrOrange
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "UP"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "DN"
#property indicator_type3   DRAW_ARROW
#property indicator_color3  clrRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1

#include <Emilasp\trade\ETrade.mqh>

//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];

input int bar=120;
input int cci_period=13;
input int cci_trand_period=100;

datetime start=D'01.01.2017';


int handle_CCI;
int handle_CCI_trand;
int handle_TMA;
int handle_ATR;

ETrade *trade = new ETrade(3453);

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);

   PlotIndexSetInteger(1,PLOT_ARROW,217);
   PlotIndexSetInteger(2,PLOT_ARROW,218);

   handle_TMA = iCustom(_Symbol, _Period, "Extreme_TMA_line_indicator");
   handle_CCI = iCCI(_Symbol, _Period,cci_period,PRICE_CLOSE);
   handle_CCI_trand = iCCI(_Symbol, _Period,cci_trand_period,PRICE_CLOSE);
   handle_ATR = iATR(_Symbol, _Period,30);
   
   ChartIndicatorAdd(0,0,handle_TMA);
   ChartIndicatorAdd(0,1,handle_CCI);
   ChartIndicatorAdd(0,2,handle_CCI_trand);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<bar)
         continue;
      if(time[i] < start)
         continue;

      double ATR[];
      ArraySetAsSeries(ATR, true);
      CopyBuffer(handle_ATR, 0, time[i], 1, ATR);

      double CCI[];
      ArraySetAsSeries(CCI, true);
      CopyBuffer(handle_CCI, 0, time[i], 2, CCI);
      double CCI_trand[];
      ArraySetAsSeries(CCI_trand, true);
      CopyBuffer(handle_CCI_trand, 0, time[i], 2, CCI_trand);
      
      double TMA_TREND[];
      ArraySetAsSeries(TMA_TREND, true);
      CopyBuffer(handle_TMA, 1, time[i], 1, TMA_TREND);
      double TMA_TREND_DIRECT[];
      ArraySetAsSeries(TMA_TREND_DIRECT, true);
      CopyBuffer(handle_TMA, 1, time[i], 5, TMA_TREND_DIRECT);

      Label1Buffer[i] = 0;
      Label2Buffer[i] = 0;
      Label3Buffer[i] = 0;

      //Trand
      int trand = 0;
      if(TMA_TREND_DIRECT[0] == 1 && TMA_TREND_DIRECT[1] == 1 && TMA_TREND_DIRECT[2] == 1 && TMA_TREND_DIRECT[3] == 1)
        {
         trand = -1;
        }
      if(TMA_TREND_DIRECT[0] == 0 && TMA_TREND_DIRECT[1] == 0 && TMA_TREND_DIRECT[2] == 0 && TMA_TREND_DIRECT[3] == 0)
        {
         trand = 1;
        }

      if(trand < 0 && CCI[0] < 100 && CCI[1] > 100)
        {
         Label3Buffer[i] = high[i] + 100 * _Point;
         Label1Buffer[i] = high[i] + ATR[0];
        }
        
      if(trand > 0 && CCI[0] > -100 && CCI[1] < -100)
        {
         Label2Buffer[i] = low[i] - 100 * _Point;
         Label1Buffer[i] = low[i] - ATR[0];
        }
      
     


     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
