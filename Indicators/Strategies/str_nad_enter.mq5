//+------------------------------------------------------------------+
//|                                                str_nad_enter.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
//#property indicator_minimum -1
//#property indicator_maximum 1
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "RSI_MA"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrOrange
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];

input int bar=9; //MA period
datetime start=D'01.01.2020';

int h_Bands;
int h_RSI;
int up_RSI=70;
int dn_RSI=30;

int RSI_MA_period=9;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);
   
   h_RSI=iRSI(_Symbol,_Period,9,PRICE_CLOSE);
   h_Bands=iBands(_Symbol,_Period,20,0,2,PRICE_CLOSE); 

   ChartIndicatorAdd(0,1,h_RSI);
   ChartIndicatorAdd(0,0,h_Bands);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
    for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<bar)
         continue;
      if(time[i] < start)
         continue;

      double RSI[];
      ArraySetAsSeries(RSI, true);
      CopyBuffer(h_RSI, 0, time[i], RSI_MA_period, RSI);


      Label1Buffer[i]=0;
      Label2Buffer[i]=0;
      Label3Buffer[i]=0;
      
      double sum=0;
      for(int j=0; j<RSI_MA_period; j++)
         sum += RSI[j];
      Label3Buffer[i] = sum/RSI_MA_period;


      //--- BUY
      bool signal_RSI_buy = false;
      bool signal_RSI_MA_buy = false;
      
      if(RSI[1] > RSI[0] && RSI[0] < 30)
         signal_RSI_buy = true;

      if(RSI[1] < Label3Buffer[i] && RSI[0] > Label3Buffer[i])
         signal_RSI_MA_buy = true;
         
      if(signal_RSI_MA_buy && signal_RSI_buy)   
         Label1Buffer[i] = 50;
      
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+


