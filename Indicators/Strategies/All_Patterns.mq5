//+------------------------------------------------------------------+
//|                                       absorptions_pattern_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
//--- plot enter
#property indicator_label1  "enter"
#property indicator_type1   DRAW_COLOR_ARROW
#property indicator_color1  clrRed,clrMediumSeaGreen, clrGoldenrod
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\helpers\BarsHelper.mqh>
#include <Emilasp\BarsPattern\Patterns\AbsorptionPattern.mqh>
#include <Emilasp\BarsPattern\Patterns\PinokioPattern.mqh>
#include <Emilasp\BarsPattern\Patterns\BarEat2PrevBarsPattern.mqh>

//--- indicator buffers
double         EnterBuffer[];
double         EnterColors[];

datetime start=D'01.01.2019';

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,EnterBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,EnterColors,INDICATOR_COLOR_INDEX);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start || i < 5)
         continue;

      EnterColors[i] = 0;
      EnterBuffer[i] = 0;
      
      
      // Вынести в менеджер паттернов
      int lastBarsType = BarsHelper::chainSomeBars(i, 4, open, close);
      double avgBar = BarsHelper::getAvgSizeBarBody(30,open,close);
      
      PrevBarsModel *prevBarsModel = new PrevBarsModel(i,time, open, close, high, low);
      
      AbsorptionPattern * pattern1 = new AbsorptionPattern(i,time, open, close, high, low);
      pattern1.calculatePattern();
      pattern1.drawInfo();

      BarEat2PrevBarsPattern * pattern2 = new BarEat2PrevBarsPattern(i,time, open, close, high, low, prevBarsModel);
      pattern2.calculatePattern();
      pattern2.drawInfo();

      PinokioPattern * pattern3 = new PinokioPattern(i,time, open, close, high, low);
      pattern3.calculatePattern();
      pattern3.drawInfo();

      if(pattern1.isPattern || pattern2.isPattern || pattern3.isPattern)
        {
         EnterBuffer[i] = high[i] + 50 * _Point;
         EnterColors[i] = 2;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
