//+------------------------------------------------------------------+
//|                                                   str_3bolid.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  3
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];

int handle_MA_14;
int handle_MA_21;
int handle_MA_50;
int handle_BB;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);

   handle_MA_14=iMA(_Symbol,_Period,14,0,MODE_EMA,PRICE_CLOSE);
   handle_MA_21=iMA(_Symbol,_Period,21,0,MODE_EMA,PRICE_CLOSE);
   handle_MA_50=iMA(_Symbol,_Period,50,0,MODE_EMA,PRICE_CLOSE);
   handle_BB=iBands(_Symbol,_Period,20,0,2,PRICE_CLOSE);

   ChartIndicatorAdd(0,0,handle_MA_14);
   ChartIndicatorAdd(0,0,handle_MA_21);
   ChartIndicatorAdd(0,0,handle_MA_50);
   ChartIndicatorAdd(0,0,handle_BB);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<100)
         continue;

      Label1Buffer[i]=0;
      Label2Buffer[i]=0;

      //--- MA
      double MA_14[];
      ArraySetAsSeries(MA_14, true);
      CopyBuffer(handle_MA_14, 0, time[i], 2, MA_14);
      
      double MA_21[];
      ArraySetAsSeries(MA_21, true);
      CopyBuffer(handle_MA_21, 0, time[i], 2, MA_21);
      
      double MA_50[];
      ArraySetAsSeries(MA_50, true);
      CopyBuffer(handle_MA_50, 0, time[i], 2, MA_50);
      
      double BB_TOP[];
      ArraySetAsSeries(BB_TOP, true);
      CopyBuffer(handle_BB, 0, time[i], 2, BB_TOP);
      double BB_BOTTOM[];
      ArraySetAsSeries(BB_BOTTOM, true);
      CopyBuffer(handle_BB, 1, time[i], 2, BB_BOTTOM);
      
      //if(MA_50[0]>BB_BOTTOM[0] && MA_50[0]<BB_TOP[0])
         if(MA_21[1]<MA_50[1] && MA_21[0]>MA_50[0])
            Label1Buffer[i]=close[i];
      
      
     }

//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
