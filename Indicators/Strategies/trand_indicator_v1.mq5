//+------------------------------------------------------------------+
//|                                           trand_indicator_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 9
#property indicator_plots   5
#property indicator_minimum -10
#property indicator_maximum 10
//--- plot Result
#property indicator_label1  "Result"
#property indicator_type1   DRAW_COLOR_LINE
#property indicator_color1  clrGray,clrBlue,clrBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

//--- plot TmaLine
#property indicator_label2  "Tma"
#property indicator_type2   DRAW_COLOR_ARROW
#property indicator_color2  clrGray,clrForestGreen,clrOrangeRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot CciLine
#property indicator_label3  "Cci"
#property indicator_type3   DRAW_COLOR_ARROW
#property indicator_color3  clrGray,clrForestGreen,clrOrangeRed
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1
//--- plot RsiLine
#property indicator_label4  "Rsi"
#property indicator_type4   DRAW_COLOR_ARROW
#property indicator_color4  clrGray,clrForestGreen,clrOrangeRed
#property indicator_style4  STYLE_SOLID
#property indicator_width4  1
//--- plot Rsi Flat detector
#property indicator_label5  "Rsi Flet detector"
#property indicator_type5   DRAW_ARROW
#property indicator_color5  clrGray
#property indicator_style5  STYLE_SOLID
#property indicator_width5  1


#include <Emilasp\Helpers\CoreHelper.mqh>;


//--- indicator buffers
double         TmaBuffer[];
double         TmaColors[];
double         CciBuffer[];
double         CciColors[];
double         RsiBuffer[];
double         RsiColors[];
double         RsiFlatDetectorBuffer[];
double         ResultBuffer[];
double         ResultColors[];

input bool enableTma=true;
input bool enableCci=true;
input bool enableRsi=true;

input int periodTma = 100;
input int periodCci = 100;
input int periodRsi = 210;

input int periodFlatRsi = 10;


datetime start=D'01.01.2020';

int h_TMA;
int h_CCI;
int h_RSI;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   SetIndexBuffer(0,ResultBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,ResultColors,INDICATOR_DATA);
   SetIndexBuffer(2,TmaBuffer,INDICATOR_DATA);
   SetIndexBuffer(3,TmaColors,INDICATOR_DATA);
   SetIndexBuffer(4,CciBuffer,INDICATOR_DATA);
   SetIndexBuffer(5,CciColors,INDICATOR_DATA);
   SetIndexBuffer(6,RsiBuffer,INDICATOR_DATA);
   SetIndexBuffer(7,RsiColors,INDICATOR_DATA);
   SetIndexBuffer(8,RsiFlatDetectorBuffer,INDICATOR_DATA);



   h_TMA = iCustom(_Symbol, _Period, "Extreme_TMA_line_indicator", periodTma);
   h_CCI = iCCI(_Symbol, _Period,periodCci,PRICE_CLOSE);
   h_RSI = iRSI(_Symbol, _Period,periodRsi,PRICE_CLOSE);


   ChartIndicatorAdd(0,0,h_TMA);
   //ChartIndicatorAdd(0,1,h_CCI);
   //ChartIndicatorAdd(0,2,h_RSI);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start)
         continue;

      TmaBuffer[i] = 0;
      TmaColors[i] = 0;
      CciBuffer[i] = 0;
      CciColors[i] = 0;
      RsiBuffer[i] = 0;
      RsiColors[i] = 0;
      RsiFlatDetectorBuffer[i]=0;
      ResultBuffer[i] = 0;
      ResultColors[i] = 0;

      //--- TMA
      double TMA[];
      ArraySetAsSeries(TMA, true);
      CopyBuffer(h_TMA, 0, time[i], 1, TMA);
      double TMA_colors[];
      ArraySetAsSeries(TMA_colors, true);
      CopyBuffer(h_TMA, 1, time[i], 1, TMA_colors);
      
      TmaBuffer[i] = TMA_colors[0] == 0 ? 2 : TMA_colors[0] == 1 ? -2 : 0;
      TmaColors[i] = TMA_colors[0] == 0 ? 1 : TMA_colors[0] == 1 ? 2 : 0;
      
      //--- CCI
      double CCI[];
      ArraySetAsSeries(CCI, true);
      CopyBuffer(h_CCI, 0, time[i], 1, CCI);
      
      CciBuffer[i] = CCI[0] > 100 ? 3 : CCI[0] < -100 ? -3 : 0;
      CciColors[i] = CciBuffer[i] > 0 ? 1 : CciBuffer[i] < 0 ? 2 : 0;
      
      //--- RSI
      double RSI[];
      ArraySetAsSeries(RSI, true);
      CopyBuffer(h_RSI, 0, time[i], periodFlatRsi, RSI);
      
      RsiBuffer[i] = RSI[0] > 50 ? 4 : RSI[0] < 50 ? -4 : 0;
      RsiColors[i] = RsiBuffer[i] > 0 ? 1 : RsiBuffer[i] < 0 ? 2 : 0;
      
      RsiFlatDetectorBuffer[i] = isFlatRSI(RSI, periodFlatRsi) ? -8 : 0;
      
      ResultBuffer[i] = TmaBuffer[i] + CciBuffer[i] + RsiBuffer[i];
      ResultColors[i] = ResultBuffer[i] > 0 ? 1 : ResultBuffer[i] < 0 ? 2 : 0;
      
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
bool isFlatRSI(double &RSI[], int period)
{
   double maxRange = 60;
   double minRange = 40;
   
   int countInRange = 0;
   
   for(int i=0;i<period;i++)
     {
      if(RSI[i] > minRange && RSI[i] < maxRange)
         countInRange++;
     }

   return countInRange / period > 0.8;
}