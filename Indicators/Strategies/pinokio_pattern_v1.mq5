//+------------------------------------------------------------------+
//|                                       absorptions_pattern_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
//--- plot enter
#property indicator_label1  "enter"
#property indicator_type1   DRAW_COLOR_ARROW
#property indicator_color1  clrRed,clrMediumSeaGreen, clrGoldenrod
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

#include <Emilasp\BarsPattern\Patterns\PinokioPattern.mqh>

//--- indicator buffers
double         EnterBuffer[];
double         EnterColors[];

datetime start=D'01.01.2019';

int handle_stoch;
int handle_CCI;
int handle_trix;

struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,EnterBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,EnterColors,INDICATOR_COLOR_INDEX);

   handle_trix  = iTriX(_Symbol, _Period, 50, PRICE_CLOSE);

   ChartIndicatorAdd(0,1,handle_trix);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start || i < 5)
         continue;

      EnterBuffer[i] = 0;

      PinokioPattern * pattern = new PinokioPattern(i,time, open, close, high, low);

      if(pattern.isPattern)
        {
         pattern.drawInfo();

         sSignal trixSignal = getTrixSignal(time[i], 2);

         //int isPinokio = isPinokioPattern(i, open, close, high, low);
         if(pattern.positionType == POSITION_TYPE_BUY && trixSignal.Sell)//stSignal.Buy && cciSignal.Buy && isPinokio < 0
           {
            EnterBuffer[i] = high[i] + 30 * _Point;
            EnterColors[i] = 1;
           }
         if(pattern.positionType == POSITION_TYPE_SELL && trixSignal.Buy)//stSignal.Buy && cciSignal.Buy && isPinokio > 0
           {
            EnterBuffer[i] = low[i] - 30 * _Point;
            EnterColors[i] = 0;
           }
        }

      delete pattern;
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int isPinokioPattern(int i, const double &open[], const double &close[], const double &high[], const double &low[])
  {
   int result = 0;

   bool isLongPosition = close[i-1] > open[i-1];

   if(isLongPosition)
     {
      double centralBarSizeBody = close[i-1] - open[i-1];
      double centralBarTopNoise = high[i-1] - close[i-1];

      if(centralBarSizeBody > 0 && centralBarTopNoise / centralBarSizeBody > 3)
        {
         double leftBarHigh = high[i - 2];
         double centralBarNoisePolovina = (high[i-1] - centralBarTopNoise / 2);
         if(leftBarHigh < centralBarNoisePolovina)
           {
            double rightBarHigh = high[i];
            if(rightBarHigh < centralBarNoisePolovina)
              {
               double rightBarClose = close[i];
               double centralBarLow = low[i-1];
               if(rightBarClose < centralBarLow)
                 {
                  result = -1;
                 }
              }
           }
        }
     }

   if(!isLongPosition)
     {
      double centralBarSizeBody = open[i-1] - close[i-1];
      double centralBarTopNoise = close[i-1] - low[i-1];

      if(centralBarSizeBody > 0 && centralBarTopNoise / centralBarSizeBody > 3)
        {
         double leftBarLow = low[i - 2];
         double centralBarNoisePolovina = (low[i-1] + centralBarTopNoise / 2);
         if(leftBarLow > centralBarNoisePolovina)
           {
            double rightBarLow = low[i];
            if(rightBarLow > centralBarNoisePolovina)
              {
               double rightBarClose = close[i];
               double centralBarHigh = high[i-1];
               if(rightBarClose > centralBarHigh)
                 {
                  result = -1;
                 }
              }
           }
        }
     }

   return result;
  }

//+------------------------------------------------------------------+
sSignal getTrixSignal(datetime time, int depth)
  {
   double TRIX[];
   ArraySetAsSeries(TRIX, true);
   CopyBuffer(handle_trix, 0, time, depth + 1, TRIX);

   sSignal res;
   for(int i=0; i<depth - 1; i++)
     {
      if(TRIX[i] > TRIX[i + 1])
         res.Buy=true;

      if(TRIX[i] < TRIX[i + 1])
         res.Sell=true;

     }
   return res;
  }
//+------------------------------------------------------------------+
sSignal getCciSignal(datetime time, int depth)
  {
   double CCI[];
   ArraySetAsSeries(CCI, true);
   CopyBuffer(handle_CCI, 0, time, depth + 1, CCI);

   sSignal res;
   for(int i=0; i<depth; i++)
     {
      if(CCI[i] <= -100)// && CCI[0] < CCI[1]
         res.Buy=true;

      if(CCI[i] >= 100)//
         res.Sell=true;

     }
   return res;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal getStochSignal(datetime time, int depth)
  {
   double Stoch[];
   ArraySetAsSeries(Stoch, true);
   CopyBuffer(handle_stoch, 0, time, depth + 1, Stoch);
   double StochSignal[];
   ArraySetAsSeries(StochSignal, true);
   CopyBuffer(handle_stoch, 1, time, depth + 1, StochSignal);

   sSignal res;
   for(int i=0; i<depth; i++)
     {
      if(Stoch[i] > StochSignal[i] && Stoch[i+1] < StochSignal[i+1] && StochSignal[i] > StochSignal[i+1])
         if(Stoch[i] < 35)
            res.Buy=true;

      if(Stoch[i] < StochSignal[i] && Stoch[i+1] > StochSignal[i+1] && StochSignal[i] < StochSignal[i+1])
         if(Stoch[i] > 65)
            res.Sell=true;

     }
   return res;
  }
//+------------------------------------------------------------------+
