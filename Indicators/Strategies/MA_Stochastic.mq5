//+------------------------------------------------------------------+
//|                                                     em_ch_ma.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 3
#property indicator_plots   3
//--- plot Label1
#property indicator_label1  "UP"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrBlueViolet
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "DN"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1


#include <Emilasp\trade\ETrade.mqh>

//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];


input int bar=120;
input int cci_period=13;
input int cci_trand_period=100;

datetime start=D'01.01.2017';


int handle_ma6;
int handle_ma11;
int handle_stoch;

ETrade *trade = new ETrade(3453);

struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   PlotIndexSetInteger(0,PLOT_ARROW,217);
   PlotIndexSetInteger(1,PLOT_ARROW,218);

   handle_ma6 = iMA(_Symbol, _Period,6, 0,MODE_SMA,PRICE_LOW);
   handle_ma11 = iMA(_Symbol, _Period,21, 0,MODE_SMA,PRICE_LOW);
   handle_stoch = iStochastic(_Symbol, _Period,5,3,3, MODE_SMA,STO_LOWHIGH);

   ChartIndicatorAdd(0,0,handle_ma6);
   ChartIndicatorAdd(0,0,handle_ma11);
   ChartIndicatorAdd(0,1,handle_stoch);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start)
         continue;

      Label1Buffer[i] = 0;
      Label2Buffer[i] = 0;


      sSignal maSignal = getMaSignal(time[i], 5);
      sSignal stSignal = getStochSignal(time[i], 3);

      if(maSignal.Buy && stSignal.Buy)
        {
         Label1Buffer[i] = low[i] - 55 * _Point;
        }
      if(maSignal.Sell && stSignal.Sell)
        {
         Label2Buffer[i] = high[i] + 55 * _Point;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal getStochSignal(datetime time, int depth)
  {
   double Stoch[];
   ArraySetAsSeries(Stoch, true);
   CopyBuffer(handle_stoch, 0, time, depth + 1, Stoch);
   double StochSignal[];
   ArraySetAsSeries(StochSignal, true);
   CopyBuffer(handle_stoch, 1, time, depth + 1, StochSignal);

   sSignal res;
   for(int i=0; i<depth; i++)
     {
      if(Stoch[i] > StochSignal[i] && Stoch[i+1] < StochSignal[i+1] && StochSignal[i] > StochSignal[i+1])
         if(Stoch[i] < 50)
            res.Buy=true;

      if(Stoch[i] < StochSignal[i] && Stoch[i+1] > StochSignal[i+1] && StochSignal[i] < StochSignal[i+1])
         if(Stoch[i] > 50)
            res.Sell=true;

     }
   return res;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal getMaSignal(datetime time, int depth)
  {
   double MA6[];
   ArraySetAsSeries(MA6, true);
   CopyBuffer(handle_ma6, 0, time, depth + 1, MA6);
   double MA11[];
   ArraySetAsSeries(MA11, true);
   CopyBuffer(handle_ma11, 0, time, depth + 1, MA11);

   bool isMaBuy=false;
   bool isMaSell=false;

   sSignal res;
   for(int i=0; i<depth; i++)
     {
      if(MA6[i] > MA11[i] && MA6[i+1] < MA11[i+1])
        {
         res.Buy=true;
        }
      if(MA6[i] < MA11[i] && MA6[i+1] > MA11[i+1])
        {
         res.Sell=true;
        }
     }
   return res;
  }
//+------------------------------------------------------------------+
