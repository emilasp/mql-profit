//+------------------------------------------------------------------+
//|                                             FractalIndicator.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_plots   2
//--- plot Fractal3
#property indicator_label1  "Fractal3"
#property indicator_type1   DRAW_COLOR_ARROW
#property indicator_color1  clrRed,clrMediumSeaGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Fractal5
#property indicator_label2  "Fractal5"
#property indicator_type2   DRAW_COLOR_ARROW
#property indicator_color2  clrRed,clrLimeGreen
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1

#include <Emilasp\BarsPattern\Patterns\FractalPattern.mqh>

//--- indicator buffers
double         Fractal3Buffer[];
double         Fractal3Colors[];
double         Fractal5Buffer[];
double         Fractal5Colors[];

datetime start = D'01.01.2019';



//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit() {
//--- indicator buffers mapping
   SetIndexBuffer(0, Fractal3Buffer, INDICATOR_DATA);
   SetIndexBuffer(1, Fractal3Colors, INDICATOR_COLOR_INDEX);
   SetIndexBuffer(2, Fractal5Buffer, INDICATOR_DATA);
   SetIndexBuffer(3, Fractal5Colors, INDICATOR_COLOR_INDEX);
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
   PlotIndexSetInteger(0, PLOT_ARROW, 159);
   PlotIndexSetInteger(1, PLOT_ARROW, 172);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
//---
   for(int i = prev_calculated > 0 ? prev_calculated - 1 : 0; i < rates_total; i++) {
      if(time[i] < start || i < 5)
         continue;

       FractalPattern * pattern3 = new FractalPattern(i,time, open, close, high, low, 3);
       pattern3.calculatePattern();
       
       Fractal3Buffer[i-1] = 0;
       if(pattern3.isPattern)
         {
           if (pattern3.positionType == POSITION_TYPE_BUY) {
             Fractal3Buffer[i-1] = high[i-1] + 20 * _Point;
             Fractal3Colors[i-1] = 1;
            } else {
             Fractal3Buffer[i-1] = low[i-1] - 20 * _Point;
             Fractal3Colors[i-1] = 0;
            }
         }
       
       FractalPattern * pattern5 = new FractalPattern(i,time, open, close, high, low, 5);
       pattern5.calculatePattern();
       
       Fractal5Buffer[i-2] = 0;
       Fractal5Colors[i-2] = 0;
       
       if(pattern5.isPattern)
         {
            if (pattern5.positionType == POSITION_TYPE_BUY) {
             Fractal5Buffer[i-2] = high[i-2] + 20 * _Point;
             Fractal5Colors[i-2] = 1;
            } else {
             Fractal5Buffer[i-2] = low[i-2] - 20 * _Point;
             Fractal5Colors[i-2] = 0;
            }
         }  
   }
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
