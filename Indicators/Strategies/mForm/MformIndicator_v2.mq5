//+------------------------------------------------------------------+
//|                                               MSAD_indicator.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "Label1"
#property indicator_type1   DRAW_COLOR_ARROW
#property indicator_color1  clrMintCream,clrSpringGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "Label2"
#property indicator_type2   DRAW_COLOR_ARROW
#property indicator_color2  clrRed,clrSpringGreen
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buwiffers
double         Label1Buffer[];
double         Label1Colors[];
double         Label2Buffer[];
double         Label2Colors[];


datetime start = D'01.01.2019';

int handle_fractal;
int handle_ma_pivot;

int prevPivots[];
bool prevPivotsDirect[];

string lines[];

int bars = 30;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0, Label1Buffer, INDICATOR_DATA);
   SetIndexBuffer(1, Label1Colors, INDICATOR_COLOR_INDEX);
   SetIndexBuffer(2, Label2Buffer, INDICATOR_DATA);
   SetIndexBuffer(3, Label2Colors, INDICATOR_COLOR_INDEX);
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
   PlotIndexSetInteger(1, PLOT_ARROW, 122);


   handle_fractal = iCustom(_Symbol, _Period, "FractalIndicator");
   //ChartIndicatorAdd(0, 0, handle_fractal);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i = prev_calculated > 0 ? prev_calculated - 1 : 0; i < rates_total; i++)
     {
      if(time[i] < start || i < bars)
         continue;

      Label1Buffer[i] = 0;

      double Fractals[];
      ArraySetAsSeries(Fractals, true);
      CopyBuffer(handle_fractal, 0, time[i], 1, Fractals);

      double FractalsColors[];
      ArraySetAsSeries(FractalsColors, true);
      CopyBuffer(handle_fractal, 1, time[i], 1, FractalsColors);

      if(Fractals[0] > 0)
        {
         bool isLongcurrentPivot = FractalsColors[0] == 1;

         // Is first pivot
         if(ArraySize(prevPivots) == 0)
           {
            prevPivotsAddPivot(i, isLongcurrentPivot);
            continue;
           }

         int sizePrevPivots   = ArraySize(prevPivots);
         int lastFractalIndex = prevPivots[sizePrevPivots - 1];
         bool isLongLastPivot = prevPivotsDirect[sizePrevPivots - 1];

         // Add pivot
         if(isLongLastPivot == isLongcurrentPivot)
           {
            prevPivotsAddPivot(i, isLongcurrentPivot);
           }
         // Is reverse pivot
         if(isLongLastPivot != isLongcurrentPivot)
           {
//            int prevPivotIndex = 0;
//            for(int k=1;i<sizePrevPivots;k++)
//              {
//               bool isLongIterPivot = prevPivotsDirect[k];
//               
//               if (isLongcurrentPivot && (prevPivotPrice == 0 || high[prevPivots[k]] > high[prevPivotIndex])) {
//                  prevPivotIndex = k;
//               }
//               
//               if (!isLongcurrentPivot && (prevPivotPrice == 0 || low[prevPivots[k]] < low[prevPivotIndex])) {
//                  prevPivotIndex = k;
//               }
//              }
//           
           
           if(isLongcurrentPivot)
              {
               Label1Buffer[i] = high[i];
               Label1Colors[i] = 1;
              }
            else
              {
               Label1Buffer[i] = low[i];
               Label1Colors[i] = 0;
              }
           
            ArrayFree(prevPivots);
            ArrayFree(prevPivotsDirect);
            prevPivotsAddPivot(lastFractalIndex, isLongLastPivot);
            prevPivotsAddPivot(i, isLongcurrentPivot);

            
           }
        }
     }

//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void prevPivotsAddPivot(int i, bool isLongPivot)
  {
   int size = ArraySize(prevPivots);
   ArrayResize(prevPivotsDirect, size + 1);
   ArrayResize(prevPivots, size + 1);
   prevPivots[size] = i;
   prevPivotsDirect[size] = isLongPivot;
  }
//+------------------------------------------------------------------+