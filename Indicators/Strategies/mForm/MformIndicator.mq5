//+------------------------------------------------------------------+
//|                                               MformIndicator.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 1
#property indicator_plots   1
//--- plot Label1
#property indicator_label1  "Label1"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- indicator buffers
double         Label1Buffer[];

datetime start = D'01.01.2019';

int handle_fractal;

string lines[];

int bars = 20;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit() {
//--- indicator buffers mapping
   SetIndexBuffer(0, Label1Buffer, INDICATOR_DATA);


   handle_fractal = iCustom(_Symbol, _Period, "FractalIndicator");
   ChartIndicatorAdd(0, 0, handle_fractal);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
//---
   for(int i = prev_calculated > 0 ? prev_calculated - 1 : 0; i < rates_total; i++) {
      if(time[i] < start || i < bars)
         continue;

      Label1Buffer[i] = 0;

      double Fractals[];
      ArraySetAsSeries(Fractals, true);
      CopyBuffer(handle_fractal, 0, time[i], bars, Fractals);

      double FractalsColors[];
      ArraySetAsSeries(FractalsColors, true);
      CopyBuffer(handle_fractal, 1, time[i], bars, FractalsColors);

      int pointFirst = 0;
      int pointSecond = 0;
      for(int j = 0; j < ArraySize(Fractals); j++) {
         if (Fractals[j] > 0) {
            if(pointFirst > 0 && pointSecond == 0) {
               pointSecond = i - j;

               string name = "line:" + IntegerToString(pointFirst) + ":" + IntegerToString(pointSecond);

               int linesSize = ArraySize(lines);

               bool needAdd = false;

               if (linesSize > 0) {
                  string lastLineName = lines[linesSize - 1];

                  if(lastLineName != name)
                     needAdd = true;
               } else {
                  needAdd = true;
               }

               if(needAdd) {
                  ArrayResize(lines, linesSize + 1);
                  lines[linesSize] = name;

                  bool isLongPrev = prevIsLong(j, Fractals, FractalsColors);

                  if (FractalsColors[j] > 0) {
                     ObjectCreate(0, name, OBJ_TREND, 0, time[pointFirst], low[pointFirst], time[pointSecond], high[pointSecond]);
                     ObjectSetInteger(0, name, OBJPROP_COLOR, clrGreen);
                  } else {
                     ObjectCreate(0, name, OBJ_TREND, 0, time[pointFirst], high[pointFirst], time[pointSecond], low[pointSecond]);
                     ObjectSetInteger(0, name, OBJPROP_COLOR, clrRed);
                  }



               }
            }

            if(pointFirst == 0) {
               pointFirst = i - j;
            }

         }
      }
   }
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool prevIsLong(int i, double &Fractals[], double &FractalsColors[] ) {
   int size = ArraySize(FractalsColors);
   
   i++;
   if(size > i) {
      for(int j = i; j < ArraySize(Fractals); j++) {
         if (Fractals[j] > 0) {
            if (FractalsColors[j] == 0)
              return false;
            else
              return true;
         }
      }
   }

   return true;
}
//+------------------------------------------------------------------+
