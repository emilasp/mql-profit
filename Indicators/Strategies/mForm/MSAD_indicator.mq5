//+------------------------------------------------------------------+
//|                                               MSAD_indicator.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "Label1"
#property indicator_type1   DRAW_COLOR_LINE
#property indicator_color1  clrMintCream,clrSpringGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "Label2"
#property indicator_type2   DRAW_COLOR_ARROW
#property indicator_color2  clrRed,clrSpringGreen
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buffers
double         Label1Buffer[];
double         Label1Colors[];
double         Label2Buffer[];
double         Label2Colors[];


datetime start = D'01.01.2019';

int handle_fractal;
int handle_ma_pivot;

string lines[];

int bars = 30;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit() {
//--- indicator buffers mapping
   SetIndexBuffer(0, Label1Buffer, INDICATOR_DATA);
   SetIndexBuffer(1, Label1Colors, INDICATOR_COLOR_INDEX);
   SetIndexBuffer(2, Label2Buffer, INDICATOR_DATA);
   SetIndexBuffer(3, Label2Colors, INDICATOR_COLOR_INDEX);
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
   PlotIndexSetInteger(1, PLOT_ARROW, 159);


   handle_fractal = iCustom(_Symbol, _Period, "FractalIndicator");
   ChartIndicatorAdd(0, 0, handle_fractal);
   
   handle_ma_pivot = iMA(_Symbol,_Period,1,0,MODE_SMA,PRICE_TYPICAL);
   ChartIndicatorAdd(0, 0, handle_ma_pivot);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
//---
   int avgPivotPrev = 0;
   for(int i = prev_calculated > 0 ? prev_calculated - 1 : 0; i < rates_total; i++) {
      if(time[i] < start || i < bars)
         continue;

      Label1Buffer[i] = 0;

      double Fractals[];
      ArraySetAsSeries(Fractals, true);
      CopyBuffer(handle_fractal, 0, time[i], bars, Fractals);

      double FractalsColors[];
      ArraySetAsSeries(FractalsColors, true);
      CopyBuffer(handle_fractal, 1, time[i], bars, FractalsColors);

      int avgPivot = getAvgPivotBars(i, Fractals, FractalsColors);

      //Print("AVG(" + i + "): " + avgPivot);

      Label1Buffer[i] = 0;
      if (avgPivotPrev == 0 || (avgPivot > 0 && avgPivotPrev != avgPivot)) {
         avgPivotPrev = avgPivot;
      }

      if (avgPivotPrev > 0) {
         double priceMa = getMaPrice(i, avgPivotPrev, high, low, close);

         Label1Buffer[i] = priceMa;

         //Print("Price: " + avgPivotPrev);
      }

   }

//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getAvgPivotBars(int i, double &Fractals[], double &FractalsColors[]) {
   int countPivots = 0;
   int sumBars = 0;
   int prevBarDirect = 0;
   int prevBarNum = 0;

   int size = ArraySize(FractalsColors);

   for(int j = 0; j < ArraySize(Fractals); j++) {
      if (Fractals[j] > 0) {
         bool isLong = FractalsColors[j] == 1;
         if(prevBarDirect == 0) {
            prevBarDirect = isLong ? 1 : -1;
            prevBarNum = j;
         } else {
            if(prevBarDirect == 1 && !isLong || prevBarDirect == 0 && isLong) {
               countPivots++;
               sumBars += j - prevBarNum;
               prevBarNum = j;
            }
         }
      }

      if (countPivots >= 3)
         break;
   }

   int avg = 0;
   if (countPivots > 0)
      avg = (int) sumBars / countPivots;

   return avg;
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getMaPrice(int j, int bars, const double &high[], const double &low[], const double &close[]) {
   double price = 0;
   double priceSum = 0;
   int count = 0;

   for(int i = 0; i < bars; i++) {
      int index = j - i;
      price = (high[index] + low[index]  + close[index]) / 3;
      priceSum += price;
      count++;

      if (count >= bars)
         break;
   }



//Print("COUNT: " + count + " : " + priceSum);
   if (count > 0) {
      return priceSum / count;
      //Print("COUNT: " + count + " : " + priceSum + " = " + (priceSum / count));
   }
   return 0;
}
//+------------------------------------------------------------------+
