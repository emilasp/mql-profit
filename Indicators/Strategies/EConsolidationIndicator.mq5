//+------------------------------------------------------------------+
//|                                                     em_ch_ma.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 4
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "Consolidation"
#property indicator_type1   DRAW_COLOR_LINE
#property indicator_color1  clrGray,clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2
//--- plot Label1
#property indicator_label2  "ConsolidationHeight"
#property indicator_type2   DRAW_COLOR_LINE
#property indicator_color2  clrGray,clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2

input int consolidationBars=20;
input int avgBars=100;
input int isConsolidationPercent=45;

//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
double         Label3Buffer[];
double         Label4Buffer[];

datetime start=D'01.01.2019';

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label3Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,Label4Buffer,INDICATOR_DATA);

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start)
         continue;
      if(i<avgBars || i<consolidationBars)
         continue;

      ConsolidationAvg avgBarSize = getAvgBar(i, avgBars, open, close, high, low);
      ConsolidationAvg consolidationBarSize = getAvgBar(i, consolidationBars, open, close, high, low);

      Label1Buffer[i] = consolidationBarSize.avgBar / avgBarSize.avgBar * 100;
      Label2Buffer[i] = Label1Buffer[i] < isConsolidationPercent ? 1 : 0;
      
      Label3Buffer[i] = consolidationBarSize.maxPercent + 10;
      Label4Buffer[i] = consolidationBarSize.maxPercent < 10 ? 1 : 0;
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

struct ConsolidationAvg
  {
   double            avgBar;
   double            maxHeigthBar;
   double            maxPercent;
   void              ConsolidationAvg()
     {
      avgBar = 0;
      maxHeigthBar = 0;
      maxPercent = 0;
     }
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ConsolidationAvg getAvgBar(int index, int countBars, const double &open[], const double &close[], const double &high[], const double &low[])
  {
   ConsolidationAvg avg;
   
   double sumBars = 0;
   double maxPrice = 0;
   double minPrice = DBL_MAX;

   for(int i=0; i<countBars; i++)
     {
      sumBars += MathAbs(close[index - i] - open[index - i]);

      if(maxPrice < high[index - i])
         maxPrice = high[index - i];
      if(minPrice > low[index - i])
         minPrice = low[index - i];
     }
     
   avg.avgBar = sumBars / countBars;
   avg.maxHeigthBar = maxPrice - minPrice;
   avg.maxPercent = avg.avgBar / avg.maxHeigthBar * 100;
   
   return avg;
  }
//+------------------------------------------------------------------+
