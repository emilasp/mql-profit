//+------------------------------------------------------------------+
//|                                       absorptions_pattern_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
//--- plot enter
#property indicator_label1  "enter"
#property indicator_type1   DRAW_COLOR_ARROW
#property indicator_color1  clrRed,clrMediumSeaGreen, clrGoldenrod
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- indicator buffers
double         EnterBuffer[];
double         EnterColors[];

datetime start=D'01.01.2019';

int handle_ma;
int handle_trix;

struct sSignal
  {
   bool              Buy;
   bool              Sell;
   void              sSignal()
     {
      Buy=false;
      Sell=false;
     }
  };

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,EnterBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,EnterColors,INDICATOR_COLOR_INDEX);

   handle_trix  = iTriX(_Symbol, _Period, 50, PRICE_CLOSE);
   handle_ma = iMA(_Symbol,_Period,30,0,MODE_EMA,PRICE_CLOSE);

   ChartIndicatorAdd(0,0,handle_ma);
   ChartIndicatorAdd(0,1,handle_trix);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start || i < 200)
         continue;

      EnterBuffer[i] = 0;

      sSignal trixSignal = getTrixSignal(time[i], 2);
      sSignal maSignal = getMASignal(time[i], 2);

      int isPattern = isPattern(i, 3, open, close, high, low);
      if(isPattern < 0 && trixSignal.Sell)// && maSignal.Sell
        {
         EnterBuffer[i] = high[i] + 30 * _Point;
         EnterColors[i] = 0;
        }
      if(isPattern > 0 && trixSignal.Buy)// && maSignal.Buy
        {
         EnterBuffer[i] = low[i] - 30 * _Point;
         EnterColors[i] = 1;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int isPattern(int i, int depth, const double &open[], const double &close[], const double &high[], const double &low[])
  {
   int result = 0;

   int countLongBars = 0;
   int countShortBars = 0;
   double barsSeriesSizes = 0;
   for(int j=1; j<depth+1; j++)
     {
      if(close[i-j] > open[i-j])
        {
         barsSeriesSizes += close[i-j] - open[i-j];
         countLongBars++;
        }
      if(close[i-j] < open[i-j])
        {
         barsSeriesSizes += open[i-j] - close[i-j];
         countShortBars++;
        }
     }

   bool currentBarIsLong = close[i] > open[i];
   bool currentBarIsShort = open[i] > close[i];

   if(barsSeriesSizes / getAvgBarsSize(i, 30, open, close) > 3)
     {
      if(currentBarIsShort && countLongBars > 0 && countShortBars == 0)
        {
         result = -1;
        }

      if(currentBarIsLong && countShortBars > 0 && countLongBars == 0)
        {
         result = 1;
        }
     }

   return result;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getAvgBarsSize(int i, int depth, const double &open[], const double &close[])
  {
   double barsSum = 0;
   for(int j=0; j<depth; j++)
     {
         barsSum += MathAbs(open[i-j] - close[i-j]);
     }
     return barsSum / depth;
  }

//+------------------------------------------------------------------+
sSignal getTrixSignal(datetime time, int depth)
  {
   double TRIX[];
   ArraySetAsSeries(TRIX, true);
   CopyBuffer(handle_trix, 0, time, depth + 1, TRIX);

   sSignal res;
   for(int i=0; i<depth - 1; i++)
     {
      if(TRIX[i] > TRIX[i + 1])
         res.Buy=true;

      if(TRIX[i] < TRIX[i + 1])
         res.Sell=true;

     }
   return res;
  }
//+------------------------------------------------------------------+
sSignal getMASignal(datetime time, int depth)
  {
   double MA[];
   ArraySetAsSeries(MA, true);
   CopyBuffer(handle_ma, 0, time, depth + 1, MA);

   int countBuy = 0;
   int countSell = 0;

   sSignal res;
   for(int i=0; i<depth; i++)
     {
      if(MA[i] > MA[i+1])// && CCI[0] < CCI[1]
         countBuy++;

      if(MA[i] < MA[i+1])//
         countSell++;

     }

   if(countBuy > 0 && countSell == 0)
      res.Buy=true;
   if(countSell > 0 && countBuy == 0)
      res.Sell=true;

   return res;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//sSignal getStochSignal(datetime time, int depth)
//  {
//   double Stoch[];
//   ArraySetAsSeries(Stoch, true);
//   CopyBuffer(handle_stoch, 0, time, depth + 1, Stoch);
//   double StochSignal[];
//   ArraySetAsSeries(StochSignal, true);
//   CopyBuffer(handle_stoch, 1, time, depth + 1, StochSignal);
//
//   sSignal res;
//   for(int i=0; i<depth; i++)
//     {
//      if(Stoch[i] > StochSignal[i] && Stoch[i+1] < StochSignal[i+1] && StochSignal[i] > StochSignal[i+1])
//         if(Stoch[i] < 35)
//            res.Buy=true;
//
//      if(Stoch[i] < StochSignal[i] && Stoch[i+1] > StochSignal[i+1] && StochSignal[i] < StochSignal[i+1])
//         if(Stoch[i] > 65)
//            res.Sell=true;
//
//     }
//   return res;
//  }
//+------------------------------------------------------------------+
