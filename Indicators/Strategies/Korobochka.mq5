//+------------------------------------------------------------------+
//|                                                   Korobochka.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "Label1"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot Label2
#property indicator_label2  "Label2"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- input parameters
input int      HS=1;
input int      MS=0;
input int      HF=9;
input int      MF=0;
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
datetime start=D'01.01.2020';

string lastDateKey;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(time[i] < start)
         continue;

      checkIsEndPattern(time[i], i);

     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void checkIsEndPattern(datetime tickTime, int ind)
  {
//Print("Date tick: " + tickTime);
   datetime checDate = D'01.01.2020';
   if(tickTime > checDate)
     {
      MqlDateTime tickDate;
      TimeToStruct(tickTime,tickDate);

      string dateKey = "" + tickDate.year + "" +tickDate.mon + "" + tickDate.day;

      if(dateKey != lastDateKey)
        {

         if(tickDate.hour == 9)
           {

            dateKey = lastDateKey;

            datetime begin = StringToTime(tickDate.year + "." + intPaddToStr(tickDate.mon) + "." + intPaddToStr(tickDate.day) + " " +  intPaddToStr((int)HS) + ":" + intPaddToStr((int)MS));
            datetime end = StringToTime(tickDate.year + "." + intPaddToStr(tickDate.mon) + "." + intPaddToStr(tickDate.day) + " " +  intPaddToStr((int)HF) + ":" + intPaddToStr((int)MF));
            Print("dateKey: " + begin + " -> " + end);
            double L[];
            double H[];

            CopyHigh(_Symbol,_Period,begin,end,H);
            CopyLow(_Symbol,_Period,begin,end,L);

            int indexMin = ArrayMinimum(L);
            int indexMax = ArrayMaximum(H);

            if(indexMin > -1 && indexMax > -1)
              {
               double min=L[indexMin];
               double max=H[indexMax];
               if(min > 0 && max > 0)
                 {
                  int sizePeriod = ArraySize(H);
                  for(int i=sizePeriod - 1; i >= 0; i--)
                    {
                     Label1Buffer[ind - i] = max;
                     Label2Buffer[ind - i] = min;
                    }
                 }

              }



           }
        }
     }
  }
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string intPaddToStr(int num)
  {
   if(num < 10)
      return "0" + IntegerToString(num);
   return IntegerToString(num);
  }
//+------------------------------------------------------------------+
