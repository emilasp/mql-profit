//+------------------------------------------------------------------+
//|                                                   str_chanal_ma_3.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 5
#property indicator_plots   4
//--- plot Label1
#property indicator_label1  "MA"
#property indicator_type1   DRAW_COLOR_LINE
#property indicator_color1  clrRed,clrRoyalBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot Label2
#property indicator_label2  "UP"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_DOT
#property indicator_width2  1
//--- plot Label3
#property indicator_label3  "DN"
#property indicator_type3   DRAW_LINE
#property indicator_color3  clrRed
#property indicator_style3  STYLE_DOT
#property indicator_width3  1
//--- plot Label4
#property indicator_label4  "AVG"
#property indicator_type4   DRAW_LINE
#property indicator_color4  clrOrange
#property indicator_style4  STYLE_DOT
#property indicator_width4  1
//--- indicator buffers
double         Label1Buffer[];
double         Label1Colors[];
double         Label2Buffer[];
double         Label3Buffer[];
double         Label4Buffer[];

input int bar=90; //MA period
datetime start=D'01.01.2020';

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label1Colors,INDICATOR_COLOR_INDEX);
   SetIndexBuffer(2,Label2Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,Label3Buffer,INDICATOR_DATA);
   SetIndexBuffer(4,Label4Buffer,INDICATOR_DATA);
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<bar)
         continue;
      if(time[i] < start)
         continue;

      double sum=0;
      for(int j=0; j<bar; j++)
         sum += close[i-j];
      Label1Buffer[i] = sum/bar;


      double max=-DBL_MAX;
      double min=DBL_MAX;

      for(int j=0; j<bar; j++)
        {
         double up = high[i-j]-Label1Buffer[i-j];
         if(up > max)
           {
            max = up;

           }

         double dn = low[i-j]-Label1Buffer[i-j];
         if(dn<min)
           {
            min = dn;
           }
        }

      Label2Buffer[i]=Label1Buffer[i] + max;
      Label3Buffer[i]=Label1Buffer[i] + min;
      Label4Buffer[i]=(Label2Buffer[i]+Label3Buffer[i])/2;
      
      if(Label1Buffer[i] > Label4Buffer[i])
         Label1Colors[i]=1;
      else
         Label1Colors[i]=0;

     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
