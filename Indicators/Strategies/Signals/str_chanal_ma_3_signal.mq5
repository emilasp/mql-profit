//+------------------------------------------------------------------+
//|                                       str_chanal_ma_3_signal.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_minimum -2
#property indicator_maximum 2
#property indicator_buffers 2
#property indicator_plots   2
//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_HISTOGRAM
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  3
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];

int h;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);

   string name = "Strategies\\Signals\\str_chanal_ma_3";//Strategies\\
   h = iCustom(_Symbol, _Period, name,90);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {

      double MA[];
      ArraySetAsSeries(MA, true);
      CopyBuffer(h, 0, time[i], 2, MA);

      double AVG[];
      ArraySetAsSeries(AVG, true);
      CopyBuffer(h, 4, time[i], 2, AVG);


      Label1Buffer[i] = 0;
      Label2Buffer[i] = 0;
      if(MA[1] < AVG[1] && MA[0] > AVG[0])
        {
         Label2Buffer[i]=1;
        }

      if(MA[1] > AVG[1] && MA[0] < AVG[0])
        {
         Label1Buffer[i]=1;
        }

     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
