#property copyright "IgorNewMail@mail.ru"
#property link      "https://www.mql5.com/ru/users/igor.i"
#property version   "7.40"
#property indicator_separate_window
#property indicator_buffers 6
#property indicator_plots   5
//--- plot shadow
#property indicator_label1  "shadow"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrYellow
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot current
#property indicator_label2  "current up"
#property indicator_type2   DRAW_HISTOGRAM
#property indicator_color2  clrBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2

#property indicator_label3  "current dn"
#property indicator_type3   DRAW_HISTOGRAM
#property indicator_color3  clrOrange
#property indicator_style3  STYLE_SOLID
#property indicator_width3  2

#property indicator_label4  "atr"
#property indicator_type4   DRAW_LINE
#property indicator_color4  clrSilver
#property indicator_style4  STYLE_SOLID
#property indicator_width4  1

#property indicator_label5  "atr"
#property indicator_type5   DRAW_LINE
#property indicator_color5  clrSilver
#property indicator_style5  STYLE_SOLID
#property indicator_width5  1

//--- input parameters

input bool   median=true;//Use Median price (if 'false' Close will be used)
input double fade=0.98;  //Fading factor (<1)
input bool   UseATRFilter=true;//Use ATR filter
input int    ATR_Period=200; //ATR period
input double ATR_Factor=2.3;//ATR multiplier
input int    LIMIT=10000;//Bars to draw objects on chart
input bool   arrows=true;//Draw impulse arrows
input bool   lines =true;//Draw impulse lines
input bool   mark_trend=false;//Draw trend on chart
//--- indicator buffers
double         shadow[];
double         current[],current_up[],current_dn[];
int bars,trend,cnt_bars;
double move_start,max,move_curr,move_prev,tr_curr,tr_prev,fade_calc;
MqlDateTime Time1,Time2;
string obj_pref="tr";
int ATR_handle;
double ATR[],atr_up[],atr_dn[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,shadow,INDICATOR_DATA);
   SetIndexBuffer(1,current_up,INDICATOR_DATA);
   SetIndexBuffer(2,current_dn,INDICATOR_DATA);
   
   SetIndexBuffer(3,atr_up,INDICATOR_DATA);
   SetIndexBuffer(4,atr_dn,INDICATOR_DATA);
   SetIndexBuffer(5,current,INDICATOR_DATA);
   PlotIndexSetInteger(0,PLOT_ARROW,159);
   
   bars=0;
   
   ATR_handle=iATR(_Symbol,PERIOD_CURRENT,ATR_Period);
   fade_calc=fmax(0.00001,fmin(fabs(fade),0.99999));
//---
   return(INIT_SUCCEEDED);
  }
  
void OnDeinit(const int reason)
 {
  ObjectsTotal(0);
  ObjectsDeleteAll(0,obj_pref);
  ChartRedraw();
 }

int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   
   if(bars==rates_total) return rates_total;
      bars= rates_total;

   int start;
  
   if(prev_calculated==0)
     { // at start
      current[0]=0;
      shadow[0]=0;
      max=0;
      trend=0;
      move_curr=move_prev=tr_curr=tr_prev=0;
      start=1;
     }
   else
      start=prev_calculated-1;

   for(int i=start;i<rates_total-1;i++)
    {
     double diff=median?(high[i]+low[i])/2-(high[i-1]+low[i-1])/2:(close[i]-close[i-1]);
     if(trend!=-1) 
       {
        if(diff<0) {trend=-1;current[i]=diff;}
        else
        current[i]=current[i-1]+diff;
       }
     else
     if(trend!=+1) 
       {
        if(diff>0) {trend=+1;current[i]=diff;}
        else
        current[i]=current[i-1]+diff;
       }

    shadow[i]=shadow[i-1]*fade_calc;

     if(UseATRFilter) 
      {
       CopyBuffer(ATR_handle,0,rates_total-i-1,1,ATR);
       atr_dn[i]=-ATR[0]*ATR_Factor;
       atr_up[i]=+ATR[0]*ATR_Factor;
      }

     if((!UseATRFilter && fabs(current[i])>fabs(shadow[i])) || 
        ( UseATRFilter && fabs(current[i])>fabs(shadow[i]) && fabs(current[i])>fabs(ATR[0]*ATR_Factor)))
         shadow[i]=current[i];

     if(i>rates_total-LIMIT)
      {
       if(mark_trend)
       {
        double d_open=iOpen(_Symbol,PERIOD_D1,iBarShift(_Symbol,PERIOD_D1,time[i]));
        Text ("dot", time[i],d_open,CharToString(127),(shadow[i]>0?(open[i]>d_open?clrSkyBlue:clrBlue):(open[i]<d_open?clrYellow:clrRed)),8,(shadow[i]>0?ANCHOR_LOWER:ANCHOR_UPPER),"Wingdings");
       }
      if((arrows || lines) && shadow[i]==current[i])
       {
        if(lines)
         {
          int begin=i;
          for(;begin>0 && current[i]*current[begin]>0;begin--) {}
          Line ("line", time[begin],time[i],(current[i]>0?low[begin]:high[begin]),(current[i]>0?high[i]:low[i]),(current[i]>0?clrBlue:clrRed),3);
         }
        if(arrows)
         {
          double pos_price=current[i]<0?low[i]:high[i];
          Text ("arrow", time[i],pos_price,CharToString(char(current[i]>0?233:234)),(current[i]>0?clrBlue:clrRed),14,(shadow[i]>0?ANCHOR_LOWER:ANCHOR_UPPER),"Wingdings");
         }
       }
      }
      current_up[i]=current[i]>0?current[i]:EMPTY_VALUE;
      current_dn[i]=current[i]<0?current[i]:EMPTY_VALUE;
    }
    
   ChartRedraw();
   current[rates_total-1]=shadow[rates_total-1]=
   atr_dn[rates_total-1]=atr_up[rates_total-1]=
   current_up[rates_total-1]=current_dn[rates_total-1]=EMPTY_VALUE;
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

void Text(string pref,datetime i,double p, string t_text=" ",color CLR=clrWheat,int fs=10,ENUM_ANCHOR_POINT anch=ANCHOR_LEFT_LOWER,string Font="Arial")
{
   string name=obj_pref+pref+(pref=="timer"?"":IntegerToString(i))+"_";
   if(ObjectCreate(0,name,OBJ_TEXT,0,i,p)) 
   {
    ObjectSetInteger(0,name,OBJPROP_ANCHOR,anch);
    ObjectSetInteger(0,name,OBJPROP_BACK,true);
    ObjectSetInteger(0,name,OBJPROP_SELECTABLE,false);
    ObjectSetInteger(0,name,OBJPROP_FONTSIZE,fs);
    ObjectSetString (0,name,OBJPROP_FONT,Font);
    ObjectSetInteger(0,name,OBJPROP_COLOR,CLR);
    ObjectSetString (0,name,OBJPROP_TEXT,t_text);   
   }
}

void Line(string pref,datetime i1,datetime i2,double p1,double p2,color CLR=clrWheat,int width=2)
{
   string name=obj_pref+pref+IntegerToString(i1)+"_";
   if(ObjectCreate(0,name,OBJ_TREND,0,i1,p1,i2,p2)) 
   {
    ObjectSetInteger(0,name,OBJPROP_RAY_LEFT,false);
    ObjectSetInteger(0,name,OBJPROP_RAY_RIGHT,false);
    ObjectSetInteger(0,name,OBJPROP_BACK,false);
    ObjectSetInteger(0,name,OBJPROP_SELECTABLE,false);
    ObjectSetInteger(0,name,OBJPROP_WIDTH,width);
    ObjectSetInteger(0,name,OBJPROP_COLOR,CLR);
   }
}
