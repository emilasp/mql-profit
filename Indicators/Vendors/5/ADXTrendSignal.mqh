//+------------------------------------------------------------------+
//|                                                    SignalADX.mqh |
//|                                                       Orangetree |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Orangetree"
#property link      "https://www.mql5.com"
#property version   "1.00"
#include "..\ExpertSignal.mqh"                  // класс CExpertSignal
// wizard description start
//+------------------------------------------------------------------+
//| Description of the class                                         |
//| Title=Signals of indicator 'ADXTrendSignal'                      |
//| Type=SignalAdvanced                                              |
//| Name=ADXTrend                                                    |
//| ShortName=ADXTrend                                               |
//| Class=SignalADX                                                  |
//| Page=????                                                        |
//| Parameter=PeriodADX,int,14,Период индикатора ADX                 |
//+------------------------------------------------------------------+
// wizard description end
//+------------------------------------------------------------------+
//| Class SignalADX.                                                 |
//| Purpose: Class of generator of trade signals based on            |
//|          the 'ADX' indicator.                                    |
//| Is derived from the CExpertSignal class.                         |
//+------------------------------------------------------------------+
class SignalADX : public CExpertSignal
  {
protected:
   CiADX m_adx;                                    // object-indicator
   int m_period_adx;                                      //Период ADX
 
public:
                     SignalADX();
                    ~SignalADX();
   //--- methods of setting adjustable parameters
   void              PeriodADX(int value)                { m_period_adx=value;}
   //--- method of verification of settings
   virtual bool      ValidationSettings(void);
   //--- method of creating the indicator and timeseries
   virtual bool      InitIndicators(CIndicators *indicators);
   //--- methods of checking if the market models are formed
   virtual int       LongCondition(void);
   virtual int       ShortCondition(void);
   
protected:
   //--- method of initialization of the indicator
   bool              InitADX(CIndicators *indicators);
   //--- methods of getting data
   double            MainADX(int index)                   { return(m_adx.Main(index));}
   double            ValueIDPlus(int index)               { return(m_adx.Plus(index));}
   double            ValueIDMinus(int index)              { return(m_adx.Minus(index));}
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalADX::SignalADX() : m_period_adx(14)
                         
  {
  //--- initialization of protected data
   m_used_series=USE_SERIES_OPEN+USE_SERIES_HIGH+USE_SERIES_LOW+USE_SERIES_CLOSE;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
SignalADX::~SignalADX()
  {
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|метод  провереряет входные параметры                              |
//+------------------------------------------------------------------+
bool SignalADX:: ValidationSettings()
  {
   // вызываем метод базового класса
   if(!CExpertSignal::ValidationSettings())  return(false);
   
   // период должен быть больше 1
   if(m_period_adx<2)
   {
   Print("Период должен быть больше 1");
   return false;
   }
   
   return true;
  }
//+------------------------------------------------------------------+
//| Create indicators.                                               |
//+------------------------------------------------------------------+
bool SignalADX::InitIndicators(CIndicators *indicators)
   {
//--- check pointer
   if(indicators==NULL)
      return(false);
//--- initialization of indicators and timeseries of additional filters
   if(!CExpertSignal::InitIndicators(indicators))
      return(false);  
//--- create and initialize ADX indicator
if(!InitADX(indicators))
      return(false);
//--- ok
   return(true);
   }
//+------------------------------------------------------------------+
//| Create ADX indicators.                                            |
//+------------------------------------------------------------------+  
bool SignalADX::InitADX(CIndicators *indicators)
   {
//--- check pointer
   if(indicators==NULL)
      return(false);
//--- add object to collection
   if(!indicators.Add(GetPointer(m_adx)))
     {
      printf(__FUNCTION__+": error adding object");
      return(false);
     }

//--- initialize object
   if(!m_adx.Create(m_symbol.Name(),m_period,m_period_adx))
     {
      printf(__FUNCTION__+": error initializing object");
      return(false);
     }

//--- ok
   return(true);   
   }
   
//+------------------------------------------------------------------+
//| "Voting" that trend is "Down".                                   |
//+------------------------------------------------------------------+
int SignalADX::LongCondition(void)
   {
   int idx   =StartIndex();
   if(ValueIDPlus(idx)>ValueIDMinus(idx)&&MainADX(idx)>MainADX(idx+1))
      return (100);
   else
      return (0);
   }
//+------------------------------------------------------------------+
//| "Voting" that trend is "UP".                                   |
//+------------------------------------------------------------------+
int SignalADX::ShortCondition(void)
   {
   int idx   =StartIndex();
   if(ValueIDPlus(idx)<ValueIDMinus(idx)&&MainADX(idx)>MainADX(idx+1))
      return (100);
   else
      return (0);
   }  