//+------------------------------------------------------------------+
//|                                              Extremum Filter.mq4 |
//|                                              Copyright 2017, Tor |
//|                                             http://einvestor.ru/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Tor"
#property link      "http://einvestor.ru/"
#property version   "3.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_plots   4

input int before=15; //Previous bars 1
input int before2=5; //Previous bars 2

double Extremum[],Extremum2[],buy[],sell[];
static int nowTrend=0;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   IndicatorShortName("Extremum Filter 3");
   IndicatorDigits(Digits);
//--- indicator lines
   SetIndexBuffer(0,Extremum);
   SetIndexBuffer(1,Extremum2);
   SetIndexBuffer(2,buy);
   SetIndexBuffer(3,sell);
   SetIndexStyle(0,DRAW_NONE);
   SetIndexStyle(1,DRAW_NONE);
   SetIndexStyle(2,DRAW_ARROW,STYLE_SOLID,1,clrBlue);
   SetIndexStyle(3,DRAW_ARROW,STYLE_SOLID,1,clrRed);
   SetIndexLabel(2,"Buy");
   SetIndexLabel(3,"Sell");
   SetIndexArrow(2,233);
   SetIndexArrow(3,234);
   SetIndexEmptyValue(2,EMPTY_VALUE);
   SetIndexEmptyValue(3,EMPTY_VALUE);
   SetLevelValue(0,0);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int limit;
//---
   if(rates_total<=1)
      return(0);
//--- last counted bar will be recounted
   limit=rates_total-prev_calculated;
   if(prev_calculated>0)
      limit=limit+1;

   for(int x=limit-1; x>=0; x--)
     {
      Extremum[x]=0; Extremum2[x]=0;
      if(iHighest(_Symbol,_Period,MODE_HIGH,before,x)>(x+before2))
        {
         Extremum[x]=1;
        }
      if(iLowest(_Symbol,_Period,MODE_LOW,before,x)>(x+before2))
        {
         Extremum2[x]=1;
        }
      if(Extremum[x]>0 && Extremum2[x]>0){ Extremum[x]=0; Extremum2[x]=0; }
      if(Extremum[x]>0 && nowTrend>-1)
        { // Sell
         nowTrend= -1;
         sell[x] = Open[x];
        }
      if(Extremum2[x]>0 && nowTrend<1)
        { // Buy
         nowTrend=1;
         buy[x]=Open[x];
        }
      if(nowTrend>0){ Extremum2[x] = 1; }
      if(nowTrend<0){ Extremum[x] = 1; }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
