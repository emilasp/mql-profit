//+------------------------------------------------------------------+
//|                                                         NRTR.mq5 |
//|                                                       Orangetree |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Orangetree"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 5 
#property indicator_plots   4

// Indicators lienes style
#property indicator_type1   DRAW_LINE
#property indicator_color1  Green
#property indicator_style1  STYLE_DASH

#property indicator_type2   DRAW_LINE
#property indicator_color2  Red
#property indicator_style2  STYLE_DASH

#property indicator_type3   DRAW_ARROW
#property indicator_color3  Green

#property indicator_type4   DRAW_ARROW
#property indicator_color4  Red
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

input int    period =12;      //динамический период
input double K =1;            //коэффициент масштаба

double Buff_Up[],Buff_Dn[];
double Sign_Up[],Sign_Dn[];
double Buff_ATR[];

int handle_atr;

int OnInit()
  {
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits);
   IndicatorSetString(INDICATOR_SHORTNAME,"NRTRvolatile");
      
   SetIndexBuffer(0,Buff_Up,INDICATOR_DATA);
   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetInteger(0,PLOT_LINE_COLOR,clrGreen);
   PlotIndexSetInteger(0,PLOT_LINE_WIDTH,2); 
   ArraySetAsSeries(Buff_Up,true);
   
   SetIndexBuffer(1,Buff_Dn,INDICATOR_DATA);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetInteger(1,PLOT_LINE_COLOR,clrRed);
   PlotIndexSetInteger(1,PLOT_LINE_WIDTH,2); 
   ArraySetAsSeries(Buff_Dn,true);
   
   SetIndexBuffer(2,Sign_Up,INDICATOR_DATA);
   PlotIndexSetDouble(2,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetInteger(2,PLOT_ARROW,236);
   PlotIndexSetInteger(2,PLOT_LINE_WIDTH,1);
   ArraySetAsSeries(Sign_Up,true);
   
   SetIndexBuffer(3,Sign_Dn,INDICATOR_DATA);
   PlotIndexSetDouble(3,PLOT_EMPTY_VALUE,0.0);
   PlotIndexSetInteger(3,PLOT_ARROW,238);
   PlotIndexSetInteger(3,PLOT_LINE_WIDTH,1);
   ArraySetAsSeries(Sign_Dn,true);
   
   SetIndexBuffer(4,Buff_ATR,INDICATOR_CALCULATIONS);
   ArraySetAsSeries(Buff_ATR,true);
         
   handle_atr =iATR(_Symbol,PERIOD_CURRENT,period);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
  
  int start=0;                                           //точка расчёта
  
  int trend=0;                                           //значеие тренда вверх 1 вниз -1
  static int trend_prev=0;
  
  double value=0;                                        //значения индикатора  
  static double value_prev=0;
  
  int dyn_period =1;                                     //значения периода                                    
  static int curr_period =1;
  
  
  double maxmin =0;                                      //техническая переменная для расчётов
  
  ArraySetAsSeries(close,true);
  
  if(rates_total<period) return(0);
  
     if(prev_calculated==0)                              // проверка на первый старт расчета индикатора
  {
      start=rates_total-1-period;                        // стартовый номер для расчета всех баров
  }
  
  else
     {
      start=rates_total-prev_calculated;                 // стартовый номер для расчета новых баров
     }

trend=trend_prev;
value=value_prev;
dyn_period =curr_period;

if(CopyBuffer(handle_atr,0,0,start+1,Buff_ATR)==-1)
  {
  return(0);
  Print("Не удалось скопировать данные в буфер ATR");
  }  
//-------------------------------------------------------------------+
//                        Основной цикл расчёта   
//-------------------------------------------------------------------+  
for(int i=start;i>=0;i--)
{
    Buff_Up[i] =0.0;
    Buff_Dn[i] =0.0;
    Sign_Up[i] =0.0;
    Sign_Dn[i] =0.0;
        
    if(curr_period>period) curr_period=period;
    if(dyn_period>period) dyn_period=period;
    
 //if trend ascending   
    if(trend>=0)
    {
    maxmin =close[ArrayMaximum(close,i,dyn_period)];
    value =maxmin-K*Buff_ATR[i];
    
    if(close[i]<value)
      {
      maxmin =close[i];
      value =maxmin+K*Buff_ATR[i];
      trend =-1;
      dyn_period =1;
      }
    }
  
//  if trend descending
    else
    {
    maxmin =close[ArrayMinimum(close,i,dyn_period)];
    value =maxmin+K*Buff_ATR[i];
    if(close[i]>value)
      {
      maxmin =close[i];
      value =maxmin-K*Buff_ATR[i];
      trend =1;
      dyn_period =1;
      }
    }  
 // trend changes 
  
      if(trend>0) Buff_Up[i]=value;
      if(trend<0) Buff_Dn[i]=value;

      if(trend_prev<0  &&  trend>0) 
      {
      Sign_Up[i]=value;
      Buff_Up[i]=0.0;
      }
      if(trend_prev>0 && trend<0)
      {
      Sign_Dn[i]=value;
      Buff_Dn[i]=0.0;
      }

  dyn_period++;
  
 // completed candle
   
  if(i)
  {
  trend_prev=trend;
  value_prev=value;
  if(dyn_period==2)curr_period=2;
  else curr_period++;
  }

}
   return(rates_total);                       
  
}
//+------------------------------------------------------------------+