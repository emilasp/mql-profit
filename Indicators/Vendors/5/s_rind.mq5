//+------------------------------------------------------------------+
//|                                                      s_r_ind.mq5 |
//|                                                         Shion.bd |
//|                                            https://investmany.ru |
//+------------------------------------------------------------------+
#property copyright "Shion.bd"
#property link      "https://investmany.ru"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot support
#property indicator_label1  "support"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot resistance
#property indicator_label2  "resistance"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrMediumBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- indicator buffers
double         supportBuffer[];
double         resistanceBuffer[];
double K,B;
int Dig;
//---
input uchar Period_RSI =8;    // ������ RSI
input int Analyze_Bars= 300;  // ������� ����� � ������� �������������
input double Low_RSI = 35.0;  // ������ ������� RSI ��� ���������� �����������
input double High_RSI= 65.0;  // ������� ������� RSI ��� ���������� �����������
input float Distans=13.0;     // �������� ������ RSI 
ENUM_TIMEFRAMES Period_Trade; // ������ �������
string Trade_Symbol;          // ������
bool First_Ext;               // ��� ������� ����������
int h_RSI;                    // ����� ���������� RSI
int Bars_H;                   // ����� ����� ��� �������
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct st_Bars //������������� ���������
  {
   int               Bar_1;
   int               Bar_2;
   int               Bar_3;
   int               Bar_4;
  };
st_Bars Bars_Ext; //���������� ���������� ���� ���������
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   Trade_Symbol=Symbol();
   Period_Trade=Period();
   Dig=(int)SymbolInfoInteger(Trade_Symbol,SYMBOL_DIGITS);//���������� ������ ����� ������� �������� �������
//--- indicator buffers mapping
   SetIndexBuffer(0,supportBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,resistanceBuffer,INDICATOR_DATA);
   h_RSI=iRSI(Trade_Symbol,Period_Trade,Period_RSI,PRICE_CLOSE);  //���������� ����� ���������� RSI
   if(h_RSI<0) Print("������������ ����� RSI");
   if(Analyze_Bars>Bars(Trade_Symbol,Period_Trade)) //���� � ������� ������ ����� ��� �������,
     {
      Print("� ������� ������ ",Analyze_Bars,"�����"); //��� ������ � ��������� bars, �� ����� ������� �� ����
      Bars_H=Bars(Trade_Symbol,Period_Trade);
      Print("����������� ����� � ������� = ",Bars_H);
     }
   else
   {
      Bars_H = Analyze_Bars;
   }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   IndicatorRelease(h_RSI); //������� ����� ��� ��������������� 
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   ArraySetAsSeries(supportBuffer,true);
   ArraySetAsSeries(resistanceBuffer,true);
   Bars_Ext.Bar_1=Ext_1(Low_RSI,High_RSI,Bars_H,h_RSI,Trade_Symbol,
                        Distans,Period_Trade); //������� ������ ���� ������� ����������
   if(Bars_Ext.Bar_1<0)
   {
      Print("� ������� ������������ ����� ��� �������");
      return(0);
   }
   if(Bars_Ext.Bar_1>0) First_Ext=One_ext(Bars_Ext,Trade_Symbol,h_RSI,Low_RSI,Period_Trade);
   Bars_Ext.Bar_2=Ext_2(Low_RSI,High_RSI,Bars_H,h_RSI,Trade_Symbol,
                        Bars_Ext,2,Distans,First_Ext,Period_Trade); //������� ������ ���� ������� ����������
   if(Bars_Ext.Bar_2<0)
   {
      Print("� ������� ������������ ����� ��� �������");
      return(0);
   }
   Bars_Ext.Bar_3=Ext_2(Low_RSI,High_RSI,Bars_H,h_RSI,Trade_Symbol,
                        Bars_Ext,3,Distans,First_Ext,Period_Trade); //������� ������ ���� �������� ����������
   if(Bars_Ext.Bar_3<0)
   {
      Print("� ������� ������������ ����� ��� �������");
      return(0);
   }
   Bars_Ext.Bar_4=Ext_2(Low_RSI,High_RSI,Bars_H,h_RSI,Trade_Symbol,
                        Bars_Ext,4,Distans,First_Ext,Period_Trade); //������� ������ ���� ���������� ����������
   if(Bars_Ext.Bar_4<0)
   {
      Print("� ������� ������������ ����� ��� �������");
      return(0);
   }
   Level(true,First_Ext,Bars_Ext,Trade_Symbol,Period_Trade); //������� ������������ k � b ��� ����� �������������
   for(int i=0;i<Bars_H;i++)
     {
      resistanceBuffer[i]=NormalizeDouble(K*i+B,Dig);
     }
   Level(false,First_Ext,Bars_Ext,Trade_Symbol,Period_Trade); //������� ������������ k � b ��� ����� ���������
   for(int i=0;i<Bars_H;i++)
     {
      supportBuffer[i]=NormalizeDouble(K*i+B,Dig);
     }
   return(rates_total);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Level(bool _line,              //��������, ������������ ����� (�������������/���������), ������������ � ������� ����� �����
           bool _first_ext,         //��� ������� ���������� (��� �������� ��� ��������)
           st_Bars &bars_ext,       //���������, ���������� ������� �����
           string _symbol,          //������
           ENUM_TIMEFRAMES _period) //������ �������
  {
   int bars=Bars_H;                 //���������� ������������� �����
   double m_high[],m_low[];         //������������� ��������
   ArraySetAsSeries(m_high,true);   //������� ������������� � ������� ��������
   ArraySetAsSeries(m_low,true);
   int h_high = CopyHigh(_symbol, _period, 0, bars, m_high); //��������� ������ ������������ ��� ������
   int h_low = CopyLow(_symbol, _period, 0, bars, m_low);    //��������� ������ ����������� ��� ������
   double price_1,price_2;
   int _bar1,_bar2;
   int digits=(int)SymbolInfoInteger(_symbol,SYMBOL_DIGITS);//���������� ������ ����� ������� �������� �������
   if(_line==true)                                          //���� ����� ����� �������������
     {
      if(_first_ext==true) //���� ������ ��������� ������������
        {
         price_1 = NormalizeDouble(m_high[bars_ext.Bar_1], digits);
         price_2 = NormalizeDouble(m_high[bars_ext.Bar_3], digits);
         _bar1 = bars_ext.Bar_1;
         _bar2 = bars_ext.Bar_3;
        }
      else                                                  //���� �����������
        {
         price_1 = NormalizeDouble(m_high[bars_ext.Bar_2], digits);
         price_2 = NormalizeDouble(m_high[bars_ext.Bar_4], digits);
         _bar1 = bars_ext.Bar_2;
         _bar2 = bars_ext.Bar_4;
        }
     }
   else                                                     //���� ����� ����� ���������
     {
      if(_first_ext==true) //���� ������ ��������� ������������
        {
         price_1 = NormalizeDouble(m_low[bars_ext.Bar_2], digits);
         price_2 = NormalizeDouble(m_low[bars_ext.Bar_4], digits);
         _bar1 = bars_ext.Bar_2;
         _bar2 = bars_ext.Bar_4;
        }
      else                                                  //���� �����������
        {
         price_1 = NormalizeDouble(m_low[bars_ext.Bar_1], digits);
         price_2 = NormalizeDouble(m_low[bars_ext.Bar_3], digits);
         _bar1 = bars_ext.Bar_1;
         _bar2 = bars_ext.Bar_3;
        }
     }
   K=(price_2-price_1)/(_bar2-_bar1);  //������� ����������� K
   B=price_1-K*_bar1;                  //������� ����������� B
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Ext_1(double low,      //������ ������� RSI, ������� ���������������
          double high,     //������� ������� RSI, ������� ���������������
          int bars,        //����� ������������� �����, ����� �� ���������� � ������� ������ ������
                           //����� ������ bars = 300
          int h_rsi,       //����� ���������� RSI
          string symbol,   //������ �������
          float distans,   //��������� ��� �������� ������ �� ������� ����������
                           //��������� ���������� ������� ������ ������� ���� - ����������
          ENUM_TIMEFRAMES period_trade) //������ �������
  {
   double m_rsi[],m_high[],m_low[]; //������������� ��������
   ArraySetAsSeries(m_rsi,true); //������� ������������� � ������� ��������
   ArraySetAsSeries(m_high,true);
   ArraySetAsSeries(m_low,true);
   int h_high = CopyHigh(symbol, period_trade, 0, bars, m_high); //��������� ������ ������������ ��� ������
   int h_low = CopyLow(symbol, period_trade, 0, bars, m_low);    //��������� ������ ����������� ��� ������
   if(CopyBuffer(h_rsi, 0, 0, bars, m_rsi)<bars)                 //��������� ������ � ������� ���������� RSI
     {
      Print("�� ������� ����������� ����� ����������!");
     }
   int index_bar = -1;     //������������� ����������, ������� ����� ��������� ������ �������� ����
   bool flag=false;        //��� ���������� �����, ����� �� ������������� ����� �� ������� ������������� ������
   bool ext_max = true;    //���������� ���� bool ������������ ��� ����, ����� � ������ ������ ���������� ������ �����
   bool ext_min = true;
   double min=100000.0;  //���������� ��� ��������� ������������ � ����������� ���
   double max= 0.0;
   int digits=(int)SymbolInfoInteger(symbol,SYMBOL_DIGITS);//���������� ������ ����� ������� �������� �������
   for(int i=0;i<bars;i++) //���� �� �����
     {
      double rsi = m_rsi[i];                                   //�������� �������� ���������� RSI
      double price_max = NormalizeDouble(m_high[i], digits);   //���� High
      double price_min = NormalizeDouble(m_low[i], digits);    //���� Low ���������� ����
      if(flag==false) //������� ��� ����, ����� �� ������ ������ ��������� �� ��������������� ������
        {
         if(rsi<=low||rsi>=high) //���� ������ ���� � ����� ��������. ��� ��������.,
            continue;            //�� ��������� � ���������� ����
         else flag = true;       //���� ���, �� ���������� ������
        }
      if(rsi<low) //���� ������� ����������� RSI c ������� low
        {
         if(ext_min==true) //���� RSI ��� �� ��������� ������� high
           {
            if(ext_max==true) //���� ��� �� ��������� ������ �� ����� ������������� ����������,
              {
               ext_max=false; //�� ��������� ������ ������������ ���������
               if(distans>=0) high=high-distans; //�������� ������� high, �� �������� �����
              }                                  //����� ������������� ����� ������� ����
            if(price_min<min) //���� � ���������� ������ ������� ����
              {               //��������� ���� Low ������
               min=price_min;
               index_bar=i;
              }
           }
         else break; /*������� �� �����, ��������� ��� ������ ����������� ��������� ��� ���������,
         ������ ������ ��� ������������*/
        }
      if(rsi>high) //����� �������� ��� ��, ������ �� ������ ������������� ����������
        {
         if(ext_max==true)
           {
            if(ext_min==true)
              {
               ext_min=false; //���� �����, ��������� ������ ����������� ��������� 
               if(distans>=0) low=low+distans;
              }
            if(price_max>max) //���� � ���������� ���������
              {
               max=price_max;
               index_bar=i;
              }
           }
         else break; /*������� �� �����, ��������� ��� ������ ������������ ��������� ��� ���������,
         ������ ������ ��� �����������*/
        }
     }
   return(index_bar);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Ext_2(double low,    //������ ������� RSI, ������� ���������������
          double high,   //������� ������� RSI, ������� ���������������
          int bars,      //����� ������������� �����, ����� �� ���������� � ������� ������ ������
                         //����� ������ bars = 300
          int h_rsi,     //����� ���������� RSI
          string symbol, //������ �������
          st_Bars &bars_ext,//���������, ���������� ������� ��������� �����
          char n_bar,    //���������� ����� ����, ������� ��������� ����� (2, 3 ��� 4)
          float distans, //��������� ��� �������� ������ �� ������� ����������
          bool first_ext,//��� ������� ����
          ENUM_TIMEFRAMES period_trade)//������ �������
  {
   double m_rsi[],m_high[],m_low[]; //������������� ��������
   ArraySetAsSeries(m_rsi,true);    //������� ������������� � ������� ��������
   ArraySetAsSeries(m_high,true);
   ArraySetAsSeries(m_low,true);
   int h_high= CopyHigh(symbol,period_trade,0,bars,m_high);    //��������� ������ ������������ ��� ������
   int h_low = CopyLow(symbol, period_trade, 0, bars, m_low);  //��������� ������ ����������� ��� ������
   if(CopyBuffer(h_rsi,0,0,bars,m_rsi)<bars)                   //��������� ������ � ������� ���������� RSI
     {
      Print("�� ������� ����������� ����� ����������!");
      //return(0);
     }
   int index_bar=-1;
   int bar_1=-1;    //������ �������� ����, ������ ����������� ����
   bool high_level=false; //���������� ��� ����������� ���� �������� ����
   bool low_level = false;
   bool _start=false;    //���������� ���� bool ������������ ��� ����, ����� � ������ ������ ���������� ������ �����
   double rsi,min,max,price_max,price_min;
   min=10000.0; max=0.0;
   int digits=(int)SymbolInfoInteger(symbol,SYMBOL_DIGITS);
//--- � ������ ����� ����������, �� ����� ����� (������������� ��� ���������) ������ ������ ������� ���������
   if(n_bar!=3)
     {
      if(first_ext==true)//���� ������ ����� ���� ������������
        {
         low_level=true;//�� ��� ������ ���� �����������
         if(distans>=0) low=low+distans;
        }
      else //���� �����������
        {
         high_level = true;
         if(distans>=0) high = high-distans;
        }
     }
   else
     {
      if(first_ext==false)//���� ������ ����� ���� �����������
        {
         low_level=true;//��  � ��� ������ ���� �����������
         if(distans>=0) high=high-distans;
        }
      else //���� ������������
        {
         high_level = true;
         if(distans>=0) low = low+distans;
        }
     }
//---
   switch(n_bar) //������� ������ ����������� ����
     {
      case 2: bar_1 = bars_ext.Bar_1; break;
      case 3: bar_1 = bars_ext.Bar_2; break;
      case 4: bar_1 = bars_ext.Bar_3; break;
     }
   for(int i=bar_1;i<bars;i++) //����������� ���������� ����
     {
      rsi=m_rsi[i];
      price_max = NormalizeDouble(m_high[i], digits);
      price_min = NormalizeDouble(m_low[i], digits);
      if(_start==true && ((low_level==true && rsi>=high) || (high_level==true && rsi<=low)))
        {
         break; //������� �� �����, ���� ������ ��������� ��� ������, � RSI ��� ������� ��������������� �������
        }
      if(low_level==true) //���� ���� ����������� ���������
        {
         if(rsi<=low)
           {
            if(_start==false) _start=true;
            if(price_min<min)
              {
               min=price_min;
               index_bar=i;
              }
           }
        }
      else //���� ���� ������������ ���������
        {
         if(rsi>=high)
           {
            if(_start==false) _start=true;
            if(price_max>=max)
              {
               max=price_max;
               index_bar=i;
              }
           }
        }
     }
   return(index_bar);
  }
//+------------------------------------------------------------------+
//| ����������, ����� ��� ������ ��� - max ��� min                   |
//+------------------------------------------------------------------+
bool One_ext(st_Bars &bars_ext, //���������� ���� ��������� ��� ��������� ������� ������� ����
             string symbol,     //������ �������
             int h_rsi,         //����� ����������
             double low,        //�������� ������� ��������������� RSI (����� ������������ � high �������)
             ENUM_TIMEFRAMES period_trade) //������ �������
  {
   double m_rsi[];               //������������� ������� ������ ����������
   ArraySetAsSeries(m_rsi,true); //������� ����������
   CopyBuffer(h_rsi,0,0,bars_ext.Bar_1+1,m_rsi); //���������� ������� ������� RSI
   double rsi=m_rsi[bars_ext.Bar_1]; //���������� �������� RSI �� ���� � ������ �����������
   if(rsi<=low)                      //���� �������� ������ ������� ������,
      return(false);                 //�� ������ ��������� ��� �����������
   else                              //���� ���,
   return(true);                     //�� ������������
  }
//+------------------------------------------------------------------+
