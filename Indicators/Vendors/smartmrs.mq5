//+------------------------------------------------------------------+
//|                                            Copyright 2019, EvgDc |
//|                                          https://tradingbots.pro |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, EvgDc"
#property link      "https://tradingbots.pro"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 5
#property indicator_plots   2

#property indicator_label1  "Red"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2 

#property indicator_label2  "Blue"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrBlue
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2 

double         Buffer1[];
double         Buffer2[];
double         iRSIBuffer[];
double         iMFIBuffer[];
double         iStochBuffer[];
int n=0;
int MfiHigh=0,MfiLow=0,RsiHigh=0,RsiLow=0,StochHigh=0,StochLow=0,
mfi_handle,rsi_handle,stoch_handle,MaximusPower=70;
int hbars=1000;
int mfi_bars_calculated = 0; 
int rsi_bars_calculated = 0;
int stoch_bars_calculated = 0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   ArraySetAsSeries(Buffer1,true);
   ArraySetAsSeries(Buffer2,true);

   PlotIndexSetDouble(0,PLOT_EMPTY_VALUE,-1);
   PlotIndexSetDouble(1,PLOT_EMPTY_VALUE,-1);

   SetIndexBuffer(0,Buffer1,INDICATOR_DATA);

   SetIndexBuffer(1,Buffer2,INDICATOR_DATA);

   mfi_handle=iMFI(NULL,0,14,VOLUME_TICK);
   rsi_handle=iRSI(NULL,0,14,PRICE_CLOSE);
   stoch_handle=iStochastic(NULL,0,14,1,1,MODE_SMA,STO_LOWHIGH);

   MfiHigh= MaximusPower;
   MfiLow = 100-MaximusPower;
   RsiHigh= MaximusPower-10;
   RsiLow = 100-MaximusPower+10;
   StochHigh= MaximusPower;
   StochLow = 100-MaximusPower;
   if(MaximusPower==0)
     {
      MfiHigh= 0; RsiHigh = 0; StochHigh = 0;
      MfiLow = 100; RsiLow = 100; StochLow = 100;
     }
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   ArraySetAsSeries(iMFIBuffer,true);
   ArraySetAsSeries(iRSIBuffer,true);
   ArraySetAsSeries(iStochBuffer,true);
   
   int mfi_values_to_copy; 
   int mfi_calculated = BarsCalculated(mfi_handle); 
   if(mfi_calculated <= 0){ 
      PrintFormat("BarsCalculated() вернул %d, код ошибки %d",mfi_calculated,GetLastError()); 
      return(0); 
     }  
   if(prev_calculated == 0 || mfi_calculated != mfi_bars_calculated || rates_total > prev_calculated + 1){ 
      if(mfi_calculated > rates_total) mfi_values_to_copy = rates_total; 
      else mfi_values_to_copy = mfi_calculated; 
     } else { 
      mfi_values_to_copy = (rates_total - prev_calculated) + 1; 
     } 
     
   int rsi_values_to_copy; 
   int rsi_calculated = BarsCalculated(rsi_handle); 
   if(rsi_calculated <= 0){ 
      PrintFormat("BarsCalculated() вернул %d, код ошибки %d",rsi_calculated,GetLastError()); 
      return(0); 
     }  
   if(prev_calculated == 0 || rsi_calculated != rsi_bars_calculated || rates_total > prev_calculated + 1){ 
      if(rsi_calculated > rates_total) rsi_values_to_copy = rates_total; 
      else rsi_values_to_copy = rsi_calculated; 
     } else { 
      rsi_values_to_copy = (rates_total - prev_calculated) + 1; 
     }
     
    int stoch_values_to_copy; 
   int stoch_calculated = BarsCalculated(stoch_handle); 
   if(stoch_calculated <= 0){ 
      PrintFormat("BarsCalculated() вернул %d, код ошибки %d",stoch_calculated,GetLastError()); 
      return(0); 
     }  
   if(prev_calculated == 0 || stoch_calculated != stoch_bars_calculated || rates_total > prev_calculated + 1){ 
      if(stoch_calculated > rates_total) stoch_values_to_copy = rates_total; 
      else stoch_values_to_copy = stoch_calculated; 
     } else { 
      stoch_values_to_copy = (rates_total - prev_calculated) + 1; 
     }

   CopyBuffer(mfi_handle,0,0,mfi_values_to_copy,iMFIBuffer);
   CopyBuffer(rsi_handle,0,0,rsi_values_to_copy,iRSIBuffer);
   CopyBuffer(stoch_handle,0,0,stoch_values_to_copy,iStochBuffer);

   for(int i = 0; i < mfi_values_to_copy; i++)
     {

      double mfi = iMFIBuffer[i];
      double rsi = iRSIBuffer[i];
      double stoch=iStochBuffer[i];

      if(mfi>=MfiHigh && rsi>=RsiHigh && stoch>=StochHigh)
        {
         Buffer1[i]=iHigh(Symbol(),Period(),i);
        }
      else Buffer1[i]=-1;
      if(mfi<=MfiLow && rsi<=RsiLow && stoch<=StochLow)
        {
         Buffer2[i]=iLow(Symbol(),Period(),i);
        }
      else Buffer2[i]=-1;
     }
   return(rates_total);
  }
//+------------------------------------------------------------------+

