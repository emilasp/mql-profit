//+------------------------------------------------------------------+
//|                                         Candlestick Patterns.mq5 |
//|                                                         VDV Soft |
//|                                                 vdv_2001@mail.ru |
//+------------------------------------------------------------------+
#property copyright "VDV Soft"
#property link      "vdv_2001@mail.ru"
#property version   "1.00"

#include <Emilasp\Candles\CandlesPattern.mqh>



#property indicator_chart_window

//--- plot 1
#property indicator_label1  ""
#property indicator_type1   DRAW_LINE
#property indicator_color1  Blue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
#property indicator_buffers 5
#property indicator_plots   1
//--- input parameters
input int   InpPeriodSMA   =10;         // Period of averaging
input bool  InpAlert       =true;       // Enable. signal
input int   InpCountBars   =1000;       // Amount of bars for calculation
input color InpColorBull   =DodgerBlue; // Color of bullish models
input color InpColorBear   =Tomato;     // Color of bearish models
input bool  InpCommentOn   =true;       // Enable comment
input int   InpTextFontSize=10;         // Font size


//---- indicator buffers
//--- indicator handles
//--- list global variable
string prefix="Patterns ";
datetime CurTime=0;

datetime date_start=D'01.01.2020';

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
   return(0);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
//--- We wait for a new bar
   if(rates_total==prev_calculated) {
      return(rates_total);
   }

////--- delete object
   string objname, comment;
//   for(int i=ObjectsTotal(0,0,-1)-1;i>=0;i--)
//     {
//      objname=ObjectName(0,i);
//      if(StringFind(objname,prefix)==-1)
//         continue;
//      else
//         ObjectDelete(0,objname);
//     }
   int objcount=0;
//---
   int limit;
   if(prev_calculated==0) {
      if(InpCountBars<=0 || InpCountBars>=rates_total)
         limit=InpPeriodSMA*2;
      else
         limit=rates_total-InpCountBars;
   } else
      limit=prev_calculated-1;
   if(!SeriesInfoInteger(Symbol(), 0, SERIES_SYNCHRONIZED))
      return(0);
// Variable of time when the signal should be given
   CurTime=time[rates_total-2];
// Determine the market (forex or not)

   bool _forex=false;
   if(SymbolInfoInteger(Symbol(), SYMBOL_TRADE_CALC_MODE)==(int)SYMBOL_CALC_MODE_FOREX) _forex=true;
   bool _language = (TerminalInfoString(TERMINAL_LANGUAGE)=="Russian") ? true : false; // Russian language of the terminal
//--- calculate Candlestick Patterns


   for(int i=limit; i<rates_total-1; i++) {
      if(time[i] < date_start)
         continue;

      datetime times[5];
      times[0] = time[i];
      times[1] = time[i-1];
      times[2] = time[i-2];
      times[3] = time[i-3];
      times[4] = time[i-4];

      CandlesPattern *candlesPattern= new CandlesPattern(_Symbol, PERIOD_CURRENT, InpPeriodSMA);
      candlesPattern.loadBars(times);

   } // end of cycle of checks
//--- return value of prev_calculated for next call
   return(rates_total);
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//----
   string objname;
   for(int i=ObjectsTotal(0, 0, -1)-1; i>=0; i--) {
      objname=ObjectName(0, i);
      if(StringFind(objname, prefix)==-1)
         continue;
      else
         ObjectDelete(0, objname);
   }
}
//+------------------------------------------------------------------+
