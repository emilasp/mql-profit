//+------------------------------------------------------------------+
//|                                           fast-start-example.mq5 |
//|                        Copyright 2012, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2012, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\SymbolInfo.mqh>

#include <Emilasp\CoreHelper.mqh>
#include <Emilasp\DrawHelper.mqh>
#include <Emilasp\IndicatorsHelper.mqh>
#include <Emilasp\CoreTrade.mqh>

//--- входные параметры

string            symbol;
ENUM_TIMEFRAMES   timeframe;

CoreHelper coreHelper;
DrawHelper drawHelper;
IndicatorsHelper indicatorsHelper;

CSymbolInfo      a_symbol;
CoreTrade        *m_CoreTrade;

int emaFastHandle;
int emaSlowHandle;
int macdHandle;
int rsiHandle;
int sthHandle;

// -------------------------------------------------------------------
input ulong          MagicNumber    = 12321;
input ulong          Slippage       = 13;
// -------------------------------------------------------------------
int takeProfitRaw = 60;
int stopLossRaw   = 20;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
      coreHelper = new CoreHelper;
      drawHelper = new DrawHelper;
      indicatorsHelper = new IndicatorsHelper;
      
      symbol=Symbol();
      
      m_CoreTrade = new CoreTrade(a_symbol, Period(), MagicNumber, Slippage);
      m_CoreTrade.accountInit();

      m_CoreTrade.RefreshRates();
      
      
      
      // Signals Initiate
      emaFastHandle = iMA(symbol,timeframe, 5, 0, MODE_EMA, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),0,emaFastHandle);  
      emaSlowHandle = iMA(symbol,timeframe, 10, 0, MODE_EMA, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),0,emaSlowHandle);  

      macdHandle = iMACD(symbol,timeframe, 8, 21, 1, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),1,macdHandle);  
      
      rsiHandle = iRSI(symbol, timeframe, 9, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),1,rsiHandle);
      
      sthHandle = iStochastic(symbol, timeframe, 5, 3, 3, MODE_SMA, STO_LOWHIGH);
      ChartIndicatorAdd(ChartID(),1,sthHandle);
      //
     return(0);
  }
  
  

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   //IndicatorRelease(wpHandle);
   //ArrayFree(wp_buf1);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
     Comment("РАБОТАЮ... ждите... ");

     m_CoreTrade.RefreshRates();
       
     int checkPatternResult = checkPattern();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
   
  }

  
  int checkPattern() 
  {
      MqlRates rates[];
      ArraySetAsSeries(rates,true);
      
      int countRates = 50;
      
      if(CopyRates(NULL,0,0,countRates,rates) <= 0)
         Print("Ошибка копирования ценовых данных ",GetLastError());
     
     
     // Set indicators buffers
      double emaFastBuf[];
      double emaSlowBuf[];
      double macdBuf[];
      double rsiBuf[];
      double stchBuf[];
      
      ArraySetAsSeries(emaFastBuf,true);   
      ArraySetAsSeries(emaSlowBuf,true);    
      ArraySetAsSeries(macdBuf,true);
      ArraySetAsSeries(rsiBuf,true);
      ArraySetAsSeries(stchBuf,true);
      
      CopyBuffer(emaFastHandle,0,0,countRates, emaFastBuf);
      CopyBuffer(emaSlowHandle,0,0,countRates, emaSlowBuf);
      CopyBuffer(macdHandle,0,0,countRates, macdBuf);
      CopyBuffer(rsiHandle,0,0,countRates ,rsiBuf);
      CopyBuffer(sthHandle,0,0,countRates, stchBuf);
     
     
      //MqlTick ticks_array[];
      //CopyTicks(symbol, ticks_array, COPY_TICKS_ALL, 0, countRates);  
     
      // Ищем паттерн на вход
      int minProfitsBars = 3;
      int maxProfitsBars = 6;
      int maxAnaliseBars = countRates - 10;
      for(int i = minProfitsBars; i < maxAnaliseBars; i++){
         // Если был сигнал на вход
         if (isPatternBuy(i, macdBuf, rsiBuf, stchBuf, emaFastBuf, emaSlowBuf)) {
            // Считаем TakeProfit и StopLoss  
            double takeProfit = m_CoreTrade.calcTakeProfit(POSITION_TYPE_BUY, takeProfitRaw, rates[i].open);
            double stopLoss   = m_CoreTrade.calcTakeProfit(POSITION_TYPE_BUY, stopLossRaw, rates[i].open);

            // Проверяем, что достигнут TakeProfit и не был выбит StopLoss
            if(isOverTakeProfitAndCheckStopLoss(rates, i, maxProfitsBars, takeProfit, stopLoss)){
               drawHelper.drawText("goodPattern" + MathRand(), "Profit", iTime(symbol, PERIOD_CURRENT, i), rates[i].close, clrGreen);
            } else {
                drawHelper.drawText("goodPattern" + MathRand(), "Loss", iTime(symbol, PERIOD_CURRENT, i), rates[i].close, clrRed);
            }
         }
      }
      
      // Пукупка в момента
      double tp = m_CoreTrade.calcTakeProfit(POSITION_TYPE_BUY, takeProfitRaw);
      double sl = m_CoreTrade.calcStopLoss(POSITION_TYPE_BUY, stopLossRaw);
      
      
      
      if (isPatternBuy(0, macdBuf, rsiBuf, stchBuf, emaFastBuf, emaSlowBuf)) {
         Print("SL: " + sl + ", TP: " + tp + ", PRICE: " + SymbolInfoDouble(symbol, SYMBOL_BID) + "Min: " + SymbolInfoInteger(symbol,SYMBOL_TRADE_STOPS_LEVEL));
         m_CoreTrade.openPositionBuy(0.1, tp, sl);
      }
      
     
     


     
     
      // Перебираем бары и ищем сигналы на покупку. function isPatternBuy(int barNum, double &macdBuf[], double &rsiBuf[],  double &stchBuf[])
      // Если сигнал найден, то смотрим, что после него в течении N свечей профит > profit
      
      int scaleHead = 3;


      //drawHelper.drawLabel("label", "Avg b: " + DoubleToString(avgSizeBar * scale,3), 10, 100);
    
      // PatternFinded
      //if (headBodySum > MathAbs(tailBodySum) * scaleHead) {
      //    string row = getRowPattern(rates, headBarsCount, tailBarsCount, tailTailBarsCount, false);

      //    coreHelper.saveDataToCsv("no_pattern_exportPatterns3_5_5_x3.csv", row, true);
      //}
  
      return 0;
  }
  
  bool isOverTakeProfitAndCheckStopLoss(MqlRates &rates[], int from, int maxProfitsBars, double takeProfit, double stopLoss)
  {   
      int profitBars = 0;
      for(int i = from; i >= 0; i--) {
         if (rates[i].low < stopLoss) {
            return false;
         }
         if (rates[i].high > takeProfit) {
            return true;
         }
         
         profitBars++;
         
         if (profitBars > maxProfitsBars) {
            break;
         }
      }
      return false;
  }
 
  
  /**
  * Проверяем, что на баре еть сигнал к покупке
  **/
  bool isPatternBuy(int barNum, double &macdBuf[], double &rsiBuf[],  double &stochBuf[], double &emaFastBuf[],  double &emaSlowBuf[])
  {

      bool isEmaSignal   = false;
      bool isMacdSignal  = false;
      bool isRsiSignal   = false;
      bool isStochSignal = false;
      
      
  
      // eMA signal
      double sigEmaFastPrev = emaFastBuf[barNum + 1];
      double sigEmaSlowPrev = emaSlowBuf[barNum + 1];
      double sigEmaFast = emaFastBuf[barNum];
      double sigEmaSlow = emaSlowBuf[barNum];
      
      if (sigEmaFastPrev < sigEmaSlowPrev && sigEmaFast > sigEmaSlow) {
         isEmaSignal = true;
      }
      
      
      double sigMacd = macdBuf[barNum];
      double sigRsi = rsiBuf[barNum];
      double sigStoch = stochBuf[barNum];
      
      if (sigRsi > 50 && rsiBuf[barNum + 1] < sigRsi) {
         isRsiSignal = true;
      }

      if (sigMacd > 0) {
         isMacdSignal  = true;
      }
      
      drawHelper.drawLabel("Signals", "Macd: " + DoubleToString(sigMacd) + ", Rsi: " + DoubleToString(sigRsi) + ", Stoch: " + DoubleToString(sigStoch), 10, 100);      
      
      return isEmaSignal && isRsiSignal && isMacdSignal; //&& isStochSignal
  }
  
  
  
  
  
  
  string getRowPattern(MqlRates &rates[], int headCount, int tailCount, int tailTailBarsCount, bool isPattern)
  {      
      //drawHelper.drawLabel("indicators 1", "MACD: " + macdBuf[headCount + 1] + ", RSI: " + rsiBuf[headCount + 1] + ", STCH: " + stchBuf[headCount + 1], 10, 200);
      
      //datetime timeBar =  iTime(symbol,timeframe, headCount + 1);
      
      //string row = "" +timeBar + "," + macdBuf[headCount + 1] + "," + rsiBuf[headCount + 1] + "," + stchBuf[headCount + 1];
  
      //for (int k = headCount + 1; k <=  headCount + 1 + tailTailBarsCount + tailTailBarsCount; k++) {
      //   row += "," + rates[k].time + "," + rates[k].open + "," + rates[k].close + "," + rates[k].high + "," + rates[k].low + "," + rates[k].tick_volume;
      //} 

      //return row + "," + (isPattern ? 1 : 0);
      
      return "";
  }
