//+------------------------------------------------------------------+
//|                                           fast-start-example.mq5 |
//|                        Copyright 2012, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2012, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Emilasp\CoreHelper.mqh>
#include <Emilasp\DrawHelper.mqh>
#include <Emilasp\IndicatorsHelper.mqh>
#include <Emilasp\BarsHelper.mqh>

//--- входные параметры
input int patternBarBeforeCounts = 30; // Количество баров до пробоя
input int patternBarAfterCounts  = 3; // Количество баров после пробоя

string            symbol;
ENUM_TIMEFRAMES   timeframe;

datetime lastPatternDate = NULL;
datetime lastNoPatternDate = NULL;

CoreHelper coreHelper;
DrawHelper drawHelper;
IndicatorsHelper indicatorsHelper;
BarsHelper barsHelper;


int macdHandle;
int rsiHandle;
int sthHandle;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
      coreHelper = new CoreHelper;
      drawHelper = new DrawHelper;
      indicatorsHelper = new IndicatorsHelper;
      barsHelper = new BarsHelper;
      
      symbol=Symbol();
      
      string  baseName = "patterns_";
      string  sPeriod = coreHelper.PeriodToStr(Period());
   
        
      lastPatternDate = iTime(symbol,timeframe,100);
      lastNoPatternDate = lastPatternDate;
      
      macdHandle = iMACD(symbol,timeframe, 8, 21, 1, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),1,macdHandle); 
      
      
      rsiHandle = iRSI(symbol, timeframe, 9, PRICE_CLOSE);
      ChartIndicatorAdd(ChartID(),1,rsiHandle);
      
      sthHandle = iStochastic(symbol, timeframe, 5, 3, 3, MODE_SMA, STO_LOWHIGH);
      ChartIndicatorAdd(ChartID(),1,sthHandle);
     return(0);
  }
  
  

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   //IndicatorRelease(wpHandle);
   //ArrayFree(wp_buf1);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
      Comment("РАБОТАЮ... ждите... ");


     int checkPatternResult = checkPattern();
  
     if (checkPatternResult > 0) {
          Alert("UP pattern");
     }
     
     if (checkPatternResult < 0) {
          Alert("DOWN pattern");
     }

  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
   
  }

  
  int checkPattern() 
  {
      MqlRates rates[];
      ArraySetAsSeries(rates,true);
      
      if(CopyRates(NULL,0,0,100,rates) <= 0)
         Print("Ошибка копирования ценовых данных ",GetLastError());
      //else 
         //Print("Скопировано ",ArraySize(rates)," баров");
  
  
      if (lastPatternDate > rates[20].time) {
         return 0;
      }

      int countToAvgSizeBar = 100;
  
      double avgSizeBar = barsHelper.getAvgSizeBarBody(rates, countToAvgSizeBar);
      
     
      
      int scaleHead = 3;
      int headBarsCount = 3;
      int tailBarsCount = 6;
      int tailTailBarsCount = 5;
      int analizeBarsCount = 10;
      int allBarsCount = headBarsCount + tailBarsCount + analizeBarsCount;
      
      //double minHeadBodySum = avgSizeBar * 0.4;
      //double maxTailBodySum = avgSizeBar * 0.2;
      
      //MqlRates analizeBars[analizeBarsCount];
      
      double headBodySum = MathAbs(coreHelper.getSumBodyRates(rates, 1, headBarsCount, false));
      double tailBodySum = coreHelper.getSumBodyRates(rates, headBarsCount + 1, headBarsCount + tailBarsCount, false);

      int scale = 1000;

      drawHelper.drawLabel("label", "Avg b: " + DoubleToString(avgSizeBar * scale,3), 10, 100);
      
      drawHelper.drawLabel("labe2",DoubleToString(headBodySum * scale,3) + ">" +  DoubleToString( tailBodySum * scaleHead * scale,3) + ", headBodySum >  tailBodySum * " + scaleHead, 10, 130);
    
      
      drawPoints(rates, 1, headBarsCount, tailBarsCount, tailTailBarsCount, "name");
    
    
      // PatternFinded
      if (headBodySum > MathAbs(tailBodySum) * scaleHead) {
         lastPatternDate = rates[1].time;
         
         drawHelper.drawText("name" + MathRand(), "UP", lastPatternDate, rates[1].close, clrBlue);

         drawPoints(rates, 1, headBarsCount, tailBarsCount, tailTailBarsCount, "name 1" + MathRand());

         string row = getRowPattern(rates, headBarsCount, tailBarsCount, tailTailBarsCount, true);
         string type = tailBodySum > 0 ? "long" : "short";

         coreHelper.saveDataToCsv(type + "_exportPatterns3_5_5_x3.csv", row, true);

         Print("Save pattern to file");
         Print("Row: " + row);
      } else {
         if (lastNoPatternDate < rates[20].time) {
            lastNoPatternDate = rates[1].time;
            string row = getRowPattern(rates, headBarsCount, tailBarsCount, tailTailBarsCount, false);

            coreHelper.saveDataToCsv("no_pattern_exportPatterns3_5_5_x3.csv", row, true);
         }
      }
  
      return 0;
  }
  
  string getRowPattern(MqlRates &rates[], int headCount, int tailCount, int tailTailBarsCount, bool isPattern)
  {
         double macdBuf[];
         double rsiBuf[];
         double stchBuf[];
         
         ArraySetAsSeries(macdBuf,true);
         ArraySetAsSeries(rsiBuf,true);
         ArraySetAsSeries(stchBuf,true);
         
         CopyBuffer(macdHandle,0,0,tailTailBarsCount,macdBuf);
         CopyBuffer(rsiHandle,0,0,tailTailBarsCount,rsiBuf);
         CopyBuffer(sthHandle,0,0,tailTailBarsCount,stchBuf);
         
         drawHelper.drawLabel("indicators 1", "MACD: " + macdBuf[headCount + 1] + ", RSI: " + rsiBuf[headCount + 1] + ", STCH: " + stchBuf[headCount + 1], 10, 200);
      
      datetime timeBar =  iTime(symbol,timeframe, headCount + 1);
      
      string row = "" +timeBar + "," + macdBuf[headCount + 1] + "," + rsiBuf[headCount + 1] + "," + stchBuf[headCount + 1];
  
      for (int k = headCount + 1; k <=  headCount + 1 + tailTailBarsCount + tailTailBarsCount; k++) {
         row += "," + rates[k].time + "," + rates[k].open + "," + rates[k].close + "," + rates[k].high + "," + rates[k].low + "," + rates[k].tick_volume;
      } 

      return row + "," + (isPattern ? 1 : 0);
  }
  
  
  void drawPoints(MqlRates &rates[], int headStart, int headCount, int tailCount, int tailTailBarsCount, string name)
  {
      for (int k = headStart; k <= headCount; k++) {
         drawHelper.drawText(name + "1" + k, "+", iTime(symbol,timeframe, k), rates[k].close, clrOrange);
      }
      
      for (int k = headCount + 1; k <= headCount + tailCount; k++) {
         drawHelper.drawText(name + "2" + k, "-", iTime(symbol,timeframe, k), rates[k].close, clrRed);
      }
      
      int tailTailStart = headCount + tailCount;
      for (int k = tailTailStart + 1; k <= tailTailStart + tailTailBarsCount; k++) {
         drawHelper.drawText(name + "3" + k, ".", iTime(symbol,timeframe, k), rates[k].close, clrYellow);
      }
  }
