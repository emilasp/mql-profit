//+------------------------------------------------------------------+
//|                                                         em_1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CEmTrade : public CTrade
  {
public:
                     CEmTrade(void) {};
                    ~CEmTrade(void) {};

   bool              emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
   bool              emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="");
  };

CEmTrade trade;

input int SL=100; // Level stopLoss [pt]
input int TP=100; //  Level taleProfit [pt]




string SL_virtual_name = "virtualSL";
string TP_virtual_name = "virtualTP";

//--
struct sSignal
  {
   bool              Buy;
   bool              Sell;

   void              sSignal()
     {
      Buy = false;
      Sell = false;
     }
  };


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
sSignal Buy_or_Sell()
  {
   sSignal res;

   double H[];
   ArraySetAsSeries(H, true);// Indexation from right to left
   CopyHigh(_Symbol, PERIOD_CURRENT, 0, 2, H);

   double L[];
   ArraySetAsSeries(L, true);// Indexation from right to left
   CopyLow(_Symbol, PERIOD_CURRENT, 0, 2, L);

   MqlTick last_tick;
   SymbolInfoTick(_Symbol, last_tick);

//--- BUY
//if(H[1] < last_tick.bid)
   if(L[1] > last_tick.bid)
      res.Buy = true;

//--- SELL
//if(L[1] > last_tick.bid)
   if(H[1] < last_tick.bid)
      res.Sell = true;

   return res;
  }


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
// Create Lines TP and SL
   moveHorizontalLine(SL_virtual_name, 0, true);
   moveHorizontalLine(TP_virtual_name, 0, true);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
  }


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   sSignal signal = Buy_or_Sell();

   if(signal.Buy == true)
     {
      if(!PositionSelect(_Symbol))
        {
         if(trade.Buy(0.01))
            draw_SL_TP_lines(POSITION_TYPE_BUY);
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
           {
            closePosition();

            if(trade.Buy(0.01)) //trade.emBuy();
               draw_SL_TP_lines(POSITION_TYPE_BUY);
           }
        }
     }
   if(signal.Sell == true)
     {
      if(!PositionSelect(_Symbol))
        {
         if(trade.Sell(0.01)) //trade.emBuy();
            draw_SL_TP_lines(POSITION_TYPE_SELL);
        }
      else
        {
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
           {
            closePosition();

            if(trade.Sell(0.01)) //trade.emBuy();
               draw_SL_TP_lines(POSITION_TYPE_SELL);
           }
        }
     }

//--- virtual SL|TP


   if(PositionSelect(_Symbol))
     {
      MqlTick last_tick;
      SymbolInfoTick(_Symbol, last_tick);
      //--- Buy
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
        {
         //--- SL
         double price = ObjectGetDouble(0,SL_virtual_name,OBJPROP_PRICE);
         if(price > 0 && last_tick.bid < price)
            closePosition();
         //--- TP
         price = ObjectGetDouble(0,TP_virtual_name,OBJPROP_PRICE);
         if(price > 0 && last_tick.bid > price)
            closePosition();
        }

      //--- Sell
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
        {
         //--- SL
         double price = ObjectGetDouble(0,SL_virtual_name,OBJPROP_PRICE);
         if(price > 0 && last_tick.ask > price)
            closePosition();
         //--- TP
         price = ObjectGetDouble(0,TP_virtual_name,OBJPROP_PRICE);
         if(price > 0 && last_tick.ask < price)
            closePosition();
        }
     }
//---

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {

  }

//+------------------------------------------------------------------+
//| Закрваем позицию                                                 |
//+------------------------------------------------------------------+
void closePosition()
  {
   if(trade.PositionClose(_Symbol))
     {
      moveHorizontalLine(SL_virtual_name, 0);
      moveHorizontalLine(TP_virtual_name, 0);
     }
  }

//+------------------------------------------------------------------+
//| Отрисовываем горизонтальные лини дял SL и TP                     |
//+------------------------------------------------------------------+
void draw_SL_TP_lines(int positionTypeBuy)
  {
   double price_open    = PositionGetDouble(POSITION_PRICE_OPEN);
   double price_buy_SL  = price_open - SL * _Point;
   double price_sell_SL = price_open + SL * _Point;
   double price_buy_TP  = price_open + TP * _Point;
   double price_sell_TP = price_open - TP * _Point;

   if(positionTypeBuy == POSITION_TYPE_BUY)
     {
      if(SL>0)
         moveHorizontalLine(SL_virtual_name, price_buy_SL);
      if(TP>0)
         moveHorizontalLine(TP_virtual_name, price_buy_TP);
     }
   else
     {
      if(SL>0)
         moveHorizontalLine(SL_virtual_name, price_sell_SL);
      if(TP>0)
         moveHorizontalLine(TP_virtual_name, price_sell_TP);
     }
  }


//+------------------------------------------------------------------+
void moveHorizontalLine(string name, double price, bool isCreate = false)
  {
   if(isCreate)
     {
      ObjectCreate(0,name,OBJ_HLINE,0,0,0);
      ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
      ObjectSetInteger(0, name, OBJPROP_SELECTED, true);
     }
   else
     {
      ObjectSetDouble(0,name,OBJPROP_PRICE,price);
     }
  }

//+------------------------------------------------------------------+
//| Buy operation                                                    |
//+------------------------------------------------------------------+
bool CEmTrade::emBuy(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_ASK);

   double price_SL = price-sl*_Point;
   double price_TP = price+tp*_Point;;

//---
   return(PositionOpen(symbol_name,ORDER_TYPE_BUY,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
//| Sell operation                                                   |
//+------------------------------------------------------------------+
bool CEmTrade::emSell(const double volume=0.01,const int sl=100,const int tp=100, const string symbol=NULL,double price=0.0,const string comment="")
  {
//--- check volume
   if(volume<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      return(false);
     }
//--- check symbol
   string symbol_name=(symbol==NULL) ? _Symbol : symbol;
//--- check price
   if(price==0.0)
      price=SymbolInfoDouble(symbol_name,SYMBOL_BID);

   double price_SL = price+sl*_Point;
   double price_TP = price-tp*_Point;;


//---
   return(PositionOpen(symbol_name,ORDER_TYPE_SELL,volume,price,price_SL,price_TP,comment));
  }
//+------------------------------------------------------------------+
