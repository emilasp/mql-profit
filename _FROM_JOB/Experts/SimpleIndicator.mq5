//+------------------------------------------------------------------+
//|                                           fast-start-example.mq5 |
//|                        Copyright 2012, MetaQuotes Software Corp. |
//|                                              https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2012, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <emilasp\CoreTrade.mqh>
#include <emilasp\TrailingStopBase.mqh> // подключение класса трейлинга

//--- входные параметры
input int    StopLoss           =    30; // Stop Loss
input int    TakeProfit         =   50; // Take Profit
input int    ADX_Period         =     8; // Период ADX
input int    MA_Period          =     8; // Период Moving Average
input int    EA_Magic           = 239310; // Magic Number советника
input double Adx_Min            =  22.0; // Минимальное значение ADX
input double Lot                =   0.2; // Количество лотов для торговли
input double TrailingSARStep    =  0.02; // Шаг Parabolic
input double TrailingSARMaximum =   0.1; // Максимум Parabolic


//--- глобальные переменные
int    adxHandle;                // хэндл индикатора ADX
int    maHandle;                 // хэндл индикатора Moving Average
double plsDI[],minDI[],adxVal[]; // динамические массивы для хранения численных значений +DI, -DI и ADX для каждого бара
double maVal[];                  // динамический массив для хранения значений индикатора Moving Average для каждого бара
double p_close;                  // переменная для хранения значения close бара
int    STP,TKP;                  // будут использованы для значений Stop Loss и Take Profit
//
int               iMA_handle;                                      //переменная для хранения хендла индикатора
double            iMA_buf[];                                       //динамический массив для хранения значений индикатора
double            Close_buf[];                                     //динамический массив для хранения цены закрытия каждого бара

string            my_symbol;                                       //переменная для хранения символа
ENUM_TIMEFRAMES   my_timeframe;                                    //переменная для хранения таймфрейма

CoreTrade         *m_CoreTrade;
CParabolicStop Trailing; // создание экземпляра класса 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {


//--- Достаточно ли количество баров для работы
   //--- общее количество баров на графике меньше 60?
   if(Bars(_Symbol,_Period)<60) 
     {
      Alert("На графике меньше 60 баров, советник не будет работать!!");
      return(-1);
     }
   
     
     
     //////
  
  
   m_CoreTrade = new CoreTrade(Symbol(), PERIOD_CURRENT, EA_Magic);
   m_CoreTrade.accountInit();
   m_CoreTrade.putToSocketData("yyy\\Files\\exportFromMql.dat", "New testetsd");
  
   my_symbol=Symbol();                                             //сохраним текущий символ графика для дальнейшей работы советника именно на этом символе
   my_timeframe=PERIOD_CURRENT;                                    //сохраним текущий период графика для дальнейшей работы советника именно на этом периоде
   iMA_handle=iMA(my_symbol,my_timeframe,40,0,MODE_SMA,PRICE_CLOSE);  //подключаем индикатор и получаем его хендл
   if(iMA_handle==INVALID_HANDLE)                                  //проверяем наличие хендла индикатора
   {
      Print("Не удалось получить хендл индикатора");               //если хендл не получен, то выводим сообщение в лог об ошибке
      return(-1);                                                  //завершаем работу с ошибкой
   }
   ChartIndicatorAdd(ChartID(),0,iMA_handle);                      //добавляем индикатор на ценовой график
   ArraySetAsSeries(iMA_buf,true);                                 //устанавливаем индексация для массива iMA_buf как в таймсерии
   ArraySetAsSeries(Close_buf,true);                              //устанавливаем индексация для массива Close_buf как в таймсерии
   
   //--- Инициализация (установка основных параметров)
   Trailing.Init(_Symbol,PERIOD_CURRENT,true,true,false); 
   //--- Установка параметров используемого типа трейлинг стопа
   if(!Trailing.SetParameters(TrailingSARStep,TrailingSARMaximum))
     { 
      Alert("trailing error");
      return(-1);
     }
   Trailing.StartTimer(); // Запуск таймера
   Trailing.On();         // Включение
   
   
   return(0);                                                      //возвращаем 0, инициализация завершена
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   IndicatorRelease(iMA_handle);                                   //удаляет хэндл индикатора и освобождает память занимаемую им
   ArrayFree(iMA_buf);                                             //освобождаем динамический массив iMA_buf от данных
   ArrayFree(Close_buf);                                           //освобождаем динамический массив Close_buf от данных
   //
   //--- Освобождаем хэндлы индикаторов
   IndicatorRelease(adxHandle);
   IndicatorRelease(maHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
       ////////// Trail Expert
      //Trailing.DoStoploss();

// Для сохранения значения времени бара мы используем static-переменную Old_Time.
// При каждом выполнении функции OnTick мы будем сравнивать время текущего бара с сохраненным временем.
// Если они не равны, это означает, что начал строится новый бар.

   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar=false;

// копируем время текущего бара в элемент New_Time[0]
   int copied=CopyTime(_Symbol,_Period,0,1,New_Time);
   if(copied>0)                 // ok, успешно скопировано
     {
      if(Old_Time!=New_Time[0]) // если старое время не равно
        {
         IsNewBar=true;         // новый бар
         if(MQL5InfoInteger(MQL5_DEBUGGING)) Print("Новый бар",New_Time[0],"старый бар",Old_Time);
         Old_Time=New_Time[0];  // сохраняем время бара
        }
     }
   else
     {
      Alert("Ошибка копирования времени, номер ошибки =",GetLastError());
      ResetLastError();
      return;
     }

//--- советник должен проверять условия совершения новой торговой операции только при новом баре
   if(IsNewBar==false)
     {
      return;
     }

 //--- Имеем ли мы достаточное количество баров на графике для работы
   int Mybars=Bars(_Symbol,_Period);
   if(Mybars<60) // если общее количество баров меньше 60
     {
      Alert("На графике менее 60 баров, советник работать не будет!!");
      return;
     } 
  
   

//--- есть ли открытые позиции?
   bool Buy_opened=false;  // переменные, в которых будет храниться информация 
   bool Sell_opened=false; // о наличии соответствующих открытых позиций

   // есть открытая позиция
   if(PositionSelect(_Symbol)==true) 
     {
      if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
        {
         Buy_opened=true;  //это длинная позиция
        }
      else if(PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
        {
         Sell_opened=true; // это короткая позиция
        }
     }

  
  
  /////////////////////
  /////////////////////
   int err1=0;                                                     //переменная для хранения результатов работы с буфером индикатора
   int err2=0;                                                     //переменная для хранения результатов работы с ценовым графиком
   
   err1=CopyBuffer(iMA_handle,0,1,2,iMA_buf);                      //копируем данные из индикаторного массива в динамический массив iMA_buf для дальнейшей работы с ними
   err2=CopyClose(my_symbol,my_timeframe,1,2,Close_buf);           //копируем данные ценового графика в динамический массив Close_buf  для дальнейшей работы с ними
   if(err1<0 || err2<0)                                            //если есть ошибки
   {
      Print("Не удалось скопировать данные из индикаторного буфера или буфера ценового графика");  //то выводим сообщение в лог об ошибке
      return;                                                                                      //и выходим из функции
   }

   if(iMA_buf[1]>Close_buf[1] && iMA_buf[0]<Close_buf[0])          //если значение индикатора были больше цены закрытия и стали меньше
     {
      m_CoreTrade.openPositionBuy(0.1, TakeProfit, StopLoss);
     }
   if(iMA_buf[1]<Close_buf[1] && iMA_buf[0]>Close_buf[0])          //если значение индикатора были меньше цены закрытия и стали больше
     {
      m_CoreTrade.openPositionSell(0.1, TakeProfit, StopLoss);                               //если дошли сюда, значит позиции нет, открываем ее
     }
     
     
     
     
     
     


     
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTimer()
  {
   Trailing.Refresh();
  }