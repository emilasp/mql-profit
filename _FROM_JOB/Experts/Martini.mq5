#include <Trade\PositionInfo.mqh>
#include <Trade\Trade.mqh>
#include <Trade\SymbolInfo.mqh>

CPositionInfo  a_position;
CSymbolInfo    a_symbol;

#include <Emilasp\CoreTrade.mqh>
#include <Emilasp\CoreHelper.mqh>
#include <Emilasp\DrawHelper.mqh>
#include <Emilasp\IndicatorsHelper.mqh>
#include <Emilasp\Strategies\MartingaleStrategy.mqh>

CoreTrade        *m_CoreTrade;
IndicatorsHelper indicatorsHelper;

MartingaleStrategy *m_artingaleStrategy;

//+------------------------------------------------------------------+
//|                                                      Martini.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


input string         MM             = "=  Money management =";
input double         Lots           = 0.01;
input double         Multiplier     = 2;
input ushort         Step           = 70;
input double         Profit         = 10;
// -------------------------------------------------------------------

input string         ST             = "= Stockastic =";
input int            Kperiod        = 26;
input int            Dperiod        = 20;
input int            Slowing        = 16;
input ENUM_MA_METHOD MaMethod       = MODE_SMA;
input ENUM_STO_PRICE PriceField     = STO_LOWHIGH;
input double         BuyLevel       = 12;
input double         SellLevel      = 88;
// -------------------------------------------------------------------
input ulong          MagicNumber    = 12321;
input ulong          Slippage       = 13;
// -------------------------------------------------------------------


int hStoch;
double points;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
   if (!a_symbol.Name(Symbol()))
      return (INIT_FAILED);

   m_CoreTrade = new CoreTrade(a_symbol, Period(), MagicNumber, Slippage);
   m_CoreTrade.accountInit();

   m_CoreTrade.RefreshRates();
   
   
   // Устанавливаем поинты и шаг для текущей валютной пары
   int digits = 1;
   if (a_symbol.Digits() == 3 || a_symbol.Digits() == 5)
      digits = 10;

   points = a_symbol.Point() * digits;
   double eStep  = Step * points;

   m_artingaleStrategy = new MartingaleStrategy(m_CoreTrade, MagicNumber, Lots, Multiplier, Step, Profit, eStep);

   // Инициализируем Стохастик
   hStoch = iStochastic(a_symbol.Name(), Period(), Kperiod, Dperiod, Slowing, MaMethod, PriceField);

   if (hStoch == INVALID_HANDLE) {
      Print("Не удалось создать индикатор Стохастик");
      return (INIT_FAILED);
   }

   return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
   if (!m_CoreTrade.RefreshRates())
      return;
   
   
   double mLine = GetStochastic(MAIN_LINE, 1);
   double sLine = GetStochastic(SIGNAL_LINE, 1);
   
   bool isBuy = false;
   if (mLine > sLine && sLine > BuyLevel) {
         isBuy = true;
   }
    bool isSell = false;
   if (mLine < sLine && sLine < SellLevel) {
      isSell = true;
   }
   
   m_artingaleStrategy.onTickExpert(isBuy, isSell);
   
}



/**
* Получаем значение индикатора на предыдущую свечу(от текущей)
*/
int GetStochastic(const int bufferNum, const int index)
{
   double Stochastic[];
   ResetLastError();

   indicatorsHelper.iGetArray(hStoch, bufferNum, index, 1, Stochastic);

   return Stochastic[0];
}





//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void OnTradeTransaction(const MqlTradeTransaction& trans, const MqlTradeRequest& request, const MqlTradeResult& result)
{
   m_artingaleStrategy.OnTradeTransactionExpert(trans, request, result);
}
//+------------------------------------------------------------------+
