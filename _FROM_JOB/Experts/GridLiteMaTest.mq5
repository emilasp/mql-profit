//+------------------------------------------------------------------+
//|                                               GridLiteMaTest.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


#include <Trade\Trade.mqh>                                         //подключаем библиотеку для совершения торговых операций
#include <Trade\PositionInfo.mqh>                                  //подключаем библиотеку для получения информации о позициях
#include <Trade\AccountInfo.mqh>
#include <Trade\SymbolInfo.mqh>

CPositionInfo  position;
CTrade         trade;
CSymbolInfo    symbol;

int handle_MA;
int ma_period = 100;
int ma_shift = 1;

double lots = 0.2;
double point;
int step = 30;
int profit = 50;
int slippage = 5;

int Magic = 234;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
//---
   Init();

   handle_MA = iMA(_Symbol, _Period, ma_period, ma_shift, MODE_EMA, PRICE_CLOSE);
   ChartIndicatorAdd(0, 0, handle_MA);

//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick() {
//---
   symbol.RefreshRates();

   double MA[];
   ArraySetAsSeries(MA, true);
   CopyBuffer(handle_MA, 0, 0, 2, MA);

   int countBuy = getCountOrders(POSITION_TYPE_BUY);
   int countSell = getCountOrders(POSITION_TYPE_SELL);

   Print("Count - buy: " + countBuy + ", sell: " + countSell);

   if((countBuy + countSell) == 0) {
      if(symbol.Bid() < MA[0]) {
         if(!trade.Sell(lots)) {
            Print("Не удалось создать позицию на продажу: " + trade.ResultComment());
         }
      }

      if(symbol.Ask() > MA[0]) {
         if(!trade.Buy(lots)) {
            Print("Не удалось создать позицию на покупку: " + trade.ResultComment());
         }
      }
   }

   if(countBuy > 0) {
      double lastPrice = getPriceLastOrder(POSITION_TYPE_BUY);

      if(lastPrice - symbol.Ask() >= step * point) {
         if(!trade.Buy(lots)) {
            Print("Не удалось создать позицию на покупку > 1: " + trade.ResultComment());
         }
      }
   }

   if(countSell > 0) {
      double lastPrice = getPriceLastOrder(POSITION_TYPE_SELL);
      
      //double raz = lastPrice + symbol.Bid();
      //double point = 
      
      if(lastPrice + symbol.Bid() >= step * point) {
          if(!trade.Sell(lots)) {
            Print("Не удалось создать позицию на продажу > 1: " + trade.ResultComment());
         }
      }
   }


   double op = getCalculatedProfit();
   if(op > profit) {
      closeAll();
   }

}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade() {
//---

}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getPriceLastOrder(ENUM_POSITION_TYPE type) {
   int oldTicket = 0;
   int ticket = 0;
   double oldOpenPrice = 0;
   
   
   for(int i = PositionsTotal() - 1; i >= 0; i--)
      if(position.SelectByIndex(i) && position.Symbol() == _Symbol && position.Magic() == Magic)
         if(position.Type() == type) {
            oldTicket = position.Ticket();

            if(oldTicket > ticket) {
               ticket = oldTicket;
               oldOpenPrice = position.PriceOpen();
            }

         }

   return oldOpenPrice;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int getCountOrders(ENUM_POSITION_TYPE type) {
   int count = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
      if(position.SelectByIndex(i) && position.Symbol() == _Symbol && position.Magic() == Magic)
         if(position.Type() == type) {
            count++;
            //Print("position ticket ", position.Ticket(), ", position opening in (msc)", position.TimeMsc());
         }
   return count;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getCalculatedProfit() {
   double oProfit = 0;

   for(int i = PositionsTotal() - 1; i >= 0; i--)
      if(position.SelectByIndex(i) && position.Symbol() == _Symbol && position.Magic() == Magic) {
         if(position.Type() == POSITION_TYPE_BUY) {
            oProfit += position.Profit();
         }
         if(position.Type() == POSITION_TYPE_SELL) {
            oProfit -= position.Profit();
         }
      }

   return oProfit;
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void closeAll() {
   for(int i = PositionsTotal() - 1; i >= 0; i--)
      if(position.SelectByIndex(i) && position.Symbol() == _Symbol && position.Magic() == Magic) {
         trade.PositionClose(_Symbol);
      }
}



//+------------------------------------------------------------------+
bool              Init(void) {
   symbol.Name(Symbol());
   if(!symbol.RefreshRates()) {
      Print("Error RefreshRates. Bid=", DoubleToString(symbol.Bid(), Digits()),
            ", Ask=", DoubleToString(symbol.Ask(), Digits()));
      return(false);
   }

   trade.SetExpertMagicNumber(Magic);
   trade.SetMarginMode();
   trade.SetTypeFillingBySymbol(Symbol());

   int digits = 1;
   if(symbol.Digits() == 3 || symbol.Digits() == 5)
      digits = 10;

   point = symbol.Point() * digits;
   trade.SetDeviationInPoints(3 * digits);

   return(true);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
