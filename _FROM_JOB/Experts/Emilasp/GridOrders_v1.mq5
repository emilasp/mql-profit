//+------------------------------------------------------------------+
//|                                                GridOrders_v1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"


#include <Emilasp\Grid\Base\EGrid.mqh>

int magic = 123213;
int channelHeight = 1000;
int blindSpot = 100;
int ordersDistance = 25;
double lot = 0.1;
double profitOrderExpected = 20;
int stopLoss = 20;
int trailing = 15;

bool sended = false;

EGrid *grid;

int h_ADX;
int h_MA;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {
//---
   grid = new EGrid(magic, channelHeight, blindSpot, ordersDistance, lot, profitOrderExpected, stopLoss,  trailing);

   h_ADX = iADX(_Symbol, _Period, 14);
   h_MA=iMA(_Symbol, _Period,20,1,MODE_EMA,PRICE_CLOSE);

   ChartIndicatorAdd(0,0, h_MA);
//---
   return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
//---

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick() {
//---
   if(TerminalInfoInteger(TERMINAL_TRADE_ALLOWED) && Bars(_Symbol, _Period) > 100) {

      double Signal[];
      ArraySetAsSeries(Signal, true);
      CopyBuffer(h_ADX, 0, 0, 2, Signal);


      double SignalMA[];
      ArraySetAsSeries(SignalMA, true);
      CopyBuffer(h_MA, 0, 0, 2, SignalMA);

      if(Signal[0] > Signal[1] && ((Signal[0] > 30 && Signal[0] < 35) || Signal[0] > 50)) {
      //if(
      //(SignalMA[0] < grid.e_trade.m_symbol.Ask() && SignalMA[1] > grid.e_trade.m_symbol.Ask() && SignalMA[0])
      //|| (SignalMA[0] > grid.e_trade.m_symbol.Ask() && SignalMA[1] < grid.e_trade.m_symbol.Ask() && SignalMA[0])
      //) {
         if(!grid.isActive) {
            grid.addGrid();
            Print("Add grid");
            sended = true;
         }
      }
   }
   if (grid.isActive) {
      grid.onTick();
   }
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade() {
//---

}
//+------------------------------------------------------------------+
