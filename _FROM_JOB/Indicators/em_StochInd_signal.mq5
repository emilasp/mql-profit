//+------------------------------------------------------------------+
//|                                                   em_oscMAMA.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 4
#property indicator_plots   4
//--- Stochastic
#property indicator_label1  "Stochastic"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrLightSeaGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- Stochastic Signal
#property indicator_label2  "Signal"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_DASHDOT
#property indicator_width2  1
//--- plot Label1
#property indicator_label3  "Label1"
#property indicator_type3   DRAW_HISTOGRAM
#property indicator_color3  clrGreen
#property indicator_style3  STYLE_SOLID
#property indicator_width3  4
//--- plot Label2
#property indicator_label4  "Label2"
#property indicator_type4   DRAW_HISTOGRAM
#property indicator_color4  clrRed
#property indicator_style4  STYLE_SOLID
#property indicator_width4  4
//---
input int                  Kperiod=5;                 // K-период (количество баров для расчетов)
input int                  Dperiod=3;                 // D-период (период первичного сглаживания)
input int                  slowing=3;                 // период для окончательного сглаживания      
input ENUM_MA_METHOD       ma_method=MODE_SMA;        // тип сглаживания   
input ENUM_STO_PRICE       price_field=STO_LOWHIGH;   // способ расчета стохастика


//--- indicator buffers
double         StochasticBuffer[];
double         SignalBuffer[];
double         Label1Buffer[];
double         Label2Buffer[];
//--- переменная для хранения хэндла индикатора iStochastic
int handle;
bool flagerror=false;
double buf[2];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,StochasticBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,SignalBuffer,INDICATOR_DATA);
   SetIndexBuffer(2,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(3,Label2Buffer,INDICATOR_DATA);
   
   handle=iStochastic(_Symbol,_Period,Kperiod,Dperiod,slowing,ma_method,price_field);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<100)
         continue;

    
//--- заполняем массивы значениями из индикатора iStochastic
//--- если FillArraysFromBuffers вернула false, значит данные не готовы - завершаем работу
   if(!FillArraysFromBuffers(StochasticBuffer,SignalBuffer,handle,1000)) return(0);
//--- сформируем сообщение
   string comm=StringFormat("%s ==>  Обновлено значений в индикаторе %s: %d",
                            TimeToString(TimeCurrent(),TIME_DATE|TIME_SECONDS),
                            "dfsd",
                            100);
//--- выведем на график служебное сообщение
   Comment(comm);
   
       if(StochasticBuffer[i] > SignalBuffer[i] && StochasticBuffer[i-1] < SignalBuffer[i-1])
        {
         Label1Buffer[i] = 20;
        }
        if(StochasticBuffer[i] < SignalBuffer[i] && StochasticBuffer[i-1] > SignalBuffer[i-1])
        {
         Label2Buffer[i] = 20;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+




//+------------------------------------------------------------------+
//| Заполняем индикаторные буферы из индикатора iStochastic          |
//+------------------------------------------------------------------+
bool FillArraysFromBuffers(double &main_buffer[],    // индикаторный буфер значений Stochastic Oscillator
                           double &signal_buffer[],  // индикаторный буфер сигнальной линии
                           int ind_handle,           // хэндл индикатора iStochastic
                           int amount                // количество копируемых значений
                           )
  {
//--- сбросим код ошибки
   ResetLastError();
//--- заполняем часть массива StochasticBuffer значениями из индикаторного буфера под индексом 0
   if(CopyBuffer(ind_handle,MAIN_LINE,0,amount,main_buffer)<0)
     {
      //--- если копирование не удалось, сообщим код ошибки
      PrintFormat("Не удалось скопировать данные из индикатора iStochastic, код ошибки %d",GetLastError());
      //--- завершим с нулевым результатом - это означает, что индикатор будет считаться нерассчитанным
      return(false);
     }
//--- заполняем часть массива SignalBuffer значениями из индикаторного буфера под индексом 1
   if(CopyBuffer(ind_handle,SIGNAL_LINE,0,amount,signal_buffer)<0)
     {
      //--- если копирование не удалось, сообщим код ошибки
      PrintFormat("Не удалось скопировать данные из индикатора iStochastic, код ошибки %d",GetLastError());
      //--- завершим с нулевым результатом - это означает, что индикатор будет считаться нерассчитанным
      return(false);
     }
//--- все получилось
   return(true);
  }