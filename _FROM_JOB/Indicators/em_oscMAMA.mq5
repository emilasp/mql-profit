//+------------------------------------------------------------------+
//|                                                   em_oscMAMA.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_separate_window
#property indicator_buffers 2
#property indicator_plots   2
#property indicator_minimum -2
#property indicator_maximum 2

//--- plot Label1
#property indicator_label1  "BUY"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrGreen
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3
//--- plot Label2
#property indicator_label2  "SELL"
#property indicator_type2   DRAW_HISTOGRAM
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  3
//--- indicator buffers
double         Label1Buffer[];
double         Label2Buffer[];
input int barsFast=9;
input int barsSlow=14;

double ma_fast_local[];
double ma_slow_local[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Label1Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,Label2Buffer,INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   ArrayResize(ma_fast_local, rates_total);
   ArrayResize(ma_slow_local, rates_total);

   for(int i=prev_calculated>0?prev_calculated-1:0; i<rates_total; i++)
     {
      if(i<barsFast || i<barsSlow)
         continue;

      ma_fast_local[i] = get_ma_for_index(i, close, barsFast);
      ma_slow_local[i] = get_ma_for_index(i, close, barsSlow);

      if(ma_fast_local[i] > ma_slow_local[i] && ma_fast_local[i-1] < ma_slow_local[i-1])
        {
         Label1Buffer[i] = 1;
        }

      if(ma_fast_local[i] < ma_slow_local[i] && ma_fast_local[i-1] > ma_slow_local[i-1])
        {
         Label2Buffer[i] = -1;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Поулчаем среднюю                                                 |
//+------------------------------------------------------------------+
double get_ma_for_index(int i, const double &close[], int countBars)
  {
   double sum=0;
   for(int j=0; j<countBars; j++)
      sum += close[i-j];

   return sum/countBars;
  }
//+------------------------------------------------------------------+
