//+------------------------------------------------------------------+
//|                                                   PatternRun.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//--- plot widthTop
#property indicator_label1  "widthTop"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrChartreuse
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//--- plot widthBottom
#property indicator_label2  "widthBottom"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrTomato
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- input parameters
input int      InputBarsTail;
input int      InputBarsHead;
input int      InputWidthChanel;
//--- indicator buffers
double         widthTopBuffer[];
double         widthBottomBuffer[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,widthTopBuffer,INDICATOR_DATA);
   SetIndexBuffer(1,widthBottomBuffer,INDICATOR_DATA);
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

   int first, bar, iii = 1;
   double Sum, SMA;
   
   for(bar = first; bar < rates_total; bar++)
    {
     Sum = 0.0;
     //---- цикл суммирования значений цены для усреднения на текущем баре
     for(iii = 0; iii < MAPeriod; iii++)
      Sum += price[bar - iii]; // Sum = Sum + price[bar - iii]; // эквивалент строки
     
     //---- получение среднего значения
     SMA = Sum / (iii + 1);
     
     //---- Инициализация ячейки индикаторного буфера полученным значением SMA
   
    }
    widthTopBuffer[bar] = SMA;

   
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
