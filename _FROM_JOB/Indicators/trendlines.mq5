//+------------------------------------------------------------------+
//|                                                   Trendlines.mq5 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 1
#property indicator_plots   1

#property indicator_label1  "Zpoint"
#property indicator_type1   DRAW_ARROW
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct trade_points //определяем структуру для эктремумов
  {
   double            price; // цена
   int               pos;   // расположение, номер бара
   bool              hpoint;// если да, тогда эта вершина относиться к максимумам
   bool              lpoint;// если да, тогда эта вершина относиться к манимумам
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct lines
  {
   int               a;
   int               b;
   double            ax;
   double            bx;
   double            dst;
   double            coef;
  };

//--- extern parameters
input int      ExtDepth=12;
input int      ExtDeviation=5;
input int      ExtBackstep=3;

input int      Min_dist=0;       // Minimum distance
input int      fibo=25;          // Fibo ratio
input int      tolerance=200;     // Tolerance
input int      Intersection_ab=1;//The allowed number of intersections from point a to point b
input int      Intersection_bc=1;//The allowed number of intersections from point b to point c

input string          s1="Up Trend";     // Настройки линий
input color           UColor=clrGreen;   // Цвет линии 
input ENUM_LINE_STYLE UStyle=STYLE_SOLID; // Стиль линии 
input int             UWidth=1;          // Толщина линии 
input bool            UBack=false;       // Линия на заднем плане 
input bool            USelection=true;   // Выделить для перемещений 
input bool            URayLeft=false;    // Продолжение линии влево 
input bool            URayRight=true;    // Продолжение линии вправо 
input bool            UHidden=true;      // Скрыт в списке объектов 
input long            UZOrder=0;         // Приоритет на нажатие мышью 

input string          s2="Down Trend";   // Настройки линий
input color           DColor=clrRed;     // Цвет линии 
input ENUM_LINE_STYLE DStyle=STYLE_SOLID; // Стиль линии 
input int             DWidth=1;          // Толщина линии 
input bool            DBack=false;       // Линия на заднем плане 
input bool            DSelection=true;   // Выделить для перемещений 
input bool            DRayLeft=false;    // Продолжение линии влево 
input bool            DRayRight=true;    // Продолжение линии вправо 
input bool            DHidden=true;      // Скрыт в списке объектов 
input long            DZOrder=0;         // Приоритет на нажатие мышью 


//--- indicator buffers

double         line0[];

//--- 

lines          UpTrend[];
lines          DownTrend[];

int ZZ_handle;
double ZigzagBuffer[1];
trade_points mass[];
int y=0;

int a,b,cross_ab,cross_bc;
double ax,bx,coef,deviation,price;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping

   SetIndexBuffer(0,line0,INDICATOR_DATA);

   ArraySetAsSeries(line0,true);
   ZZ_handle=iCustom(_Symbol,_Period,"Examples\\ZigZag",ExtDepth,ExtDeviation,ExtBackstep);

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+  
void OnDeinit(const int reason)
  {
   DelObs("Down");
   DelObs("Up");
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   int i,j,limit=0,shift=0;
   
   //

   ArraySetAsSeries(high,true);
   ArraySetAsSeries(low,true);
   ArraySetAsSeries(close,true);
   ArraySetAsSeries(time,true);

   double max=close[1];
   double min=close[1];
   int z=0;

   for(shift=0;shift<rates_total && !IsStopped();shift++)
     {
      CopyBuffer(ZZ_handle,0,shift,1,ZigzagBuffer);

      if(ZigzagBuffer[0]>0)
        {

         if(ZigzagBuffer[0]>=max && ZigzagBuffer[0]==high[shift])
           {
            ArrayResize(mass,z+1);
            max=ZigzagBuffer[0];
            mass[z].price=ZigzagBuffer[0];
            mass[z].pos=shift;
            mass[z].hpoint=true;
            mass[z].lpoint=false;
            z++;
           }

         if(ZigzagBuffer[0]<=min && ZigzagBuffer[0]==low[shift])
           {
            ArrayResize(mass,z+1);
            min=ZigzagBuffer[0];
            mass[z].price=ZigzagBuffer[0];
            mass[z].pos=shift;
            mass[z].lpoint=true;
            mass[z].hpoint=false;
            z++;
           }

        }
     }

//+------------------------------------------------------------------+

   for(i=0; i<z; i++)
     {

      if(mass[i].hpoint==true)
        {
         line0[mass[i].pos]=mass[i].price;
        }
      if(mass[i].lpoint==true)
        {
         line0[mass[i].pos]=mass[i].price;
        }
     }
//+------------------------------------------------------------------+    
   y=0;



   for(j=z-1; j>=0; j--)
     {
      if(mass[j].hpoint)
         for(i=j-1; i>=0; i--)
           {
            if(mass[i].hpoint)
               if(i<j)
                 {

                  a=mass[j].pos;
                  b=mass[i].pos;

                  double ratio=double((a-b)*100/a);

                  if(ratio>fibo && ratio<(100-fibo))
                     if(b>Min_dist && (a-b)>Min_dist)
                       {

                        ax=mass[j].price;
                        bx=mass[i].price;

                        coef=(ax-bx)/(a-b);

                        price=close[1];

                        deviation=(ax+coef*bx)-price;

                        cross_bc=0;
                        cross_ab=0;


                        if(MathAbs(deviation)<tolerance*_Point)
                          {
                           //количество пересечений от точки a до точки b
                           for(int n=a; n>b; n--)
                              if(close[n]>(ax+coef*(b-n)) && close[n]<=(ax+coef*(b-n+1)))
                                 cross_ab++;
                           //количество пересечений от точки b до конца
                           for(int n=b-1; n>=0; n--)
                              if(close[n]>(bx+coef*(b-n)) && close[n+1]<(bx+coef*(b-n+1)))
                                 cross_bc++;

                           if(cross_bc<=Intersection_bc && cross_bc<=Intersection_ab)
                             {

                              ArrayResize(DownTrend,y+1);
                              DownTrend[y].a=a;
                              DownTrend[y].b=b;
                              DownTrend[y].ax=ax;
                              DownTrend[y].bx=bx;
                              DownTrend[y].dst=MathAbs(deviation);
                              DownTrend[y].coef=coef;

                              y++;

                             }
                          }
                       }
                 }
           }
     }

   for(j=0; j<y; j++)
     {

      a=DownTrend[j].a;
      b=DownTrend[j].b;
      ax=DownTrend[j].ax;
      bx=DownTrend[j].bx;
      coef=DownTrend[j].coef;


      if(a>0 && b>0 && MathAbs(a-b)>0)
        {
         //--- создадим линии тренда 
         TrendCreate(0,"DownTrend "+string(j),0,time[a],ax,time[b],bx,DColor,DStyle,DWidth,DBack,DSelection,DRayLeft,DRayRight,DHidden,DZOrder);
         ChartRedraw();
        }
     }

//+------------------------------------------------------------------+    
   y=0;



   for(j=z-1; j>=0; j--)
     {
      if(mass[j].lpoint)
         for(i=j-1; i>=0; i--)
           {
            if(mass[i].lpoint)
               if(i<j)
                 {

                  a=mass[j].pos;
                  b=mass[i].pos;

                  double ratio=double((a-b)*100/a);

                  if(ratio>fibo && ratio<(100-fibo))
                     if(b>Min_dist && (a-b)>Min_dist)
                       {

                        ax=mass[j].price;
                        bx=mass[i].price;

                        coef=(ax-bx)/(a-b);

                        price=close[1];

                        deviation=(ax+coef*bx)-price;

                        cross_bc=0;
                        cross_ab=0;


                        if(MathAbs(deviation)<tolerance*_Point)
                          {

                           //количество пересечений от точки a до точки b
                           for(int n=a; n>b; n--)
                              if(close[n]>(ax+coef*(b-n)) && close[n]<=(ax+coef*(b-n+1)))
                                 cross_ab++;
                           //количество пересечений от точки b до конца
                           for(int n=b-1; n>=0; n--)
                              if(close[n]>(bx+coef*(b-n)) && close[n+1]<=(bx+coef*(b-n+1)))
                                 cross_bc++;

                           if(cross_bc<=Intersection_bc && cross_bc<=Intersection_ab)
                             {

                              ArrayResize(UpTrend,y+1);
                              UpTrend[y].a=a;
                              UpTrend[y].b=b;
                              UpTrend[y].ax=ax;
                              UpTrend[y].bx=bx;
                              UpTrend[y].dst=MathAbs(deviation);
                              UpTrend[y].coef=coef;

                              y++;

                             }
                          }
                       }
                 }
           }
     }

   for(j=0; j<y; j++)
     {

      a=UpTrend[j].a;
      b=UpTrend[j].b;
      ax=UpTrend[j].ax;
      bx=UpTrend[j].bx;
      coef=UpTrend[j].coef;


      if(a>0 && b>0 && MathAbs(a-b)>0)
        {
         //--- создадим линии тренда 
         TrendCreate(0,"UpTrend "+string(j),0,time[a],ax,time[b],bx,UColor,UStyle,UWidth,UBack,USelection,URayLeft,URayRight,UHidden,UZOrder);
         ChartRedraw();
        }
     }


//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+ 
//| Создает линию тренда по заданным координатам                     | 
//+------------------------------------------------------------------+ 
bool TrendCreate(const long            chart_ID=0,        // ID графика 
                 const string          name="TrendLine",  // имя линии 
                 const int             sub_window=0,      // номер подокна 
                 datetime              time1=0,           // время первой точки 
                 double                price1=0,          // цена первой точки 
                 datetime              time2=0,           // время второй точки 
                 double                price2=0,          // цена второй точки 
                 const color           clr=clrRed,        // цвет линии 
                 const ENUM_LINE_STYLE style=STYLE_SOLID, // стиль линии 
                 const int             width=1,           // толщина линии 
                 const bool            back=false,        // на заднем плане 
                 const bool            selection=true,    // выделить для перемещений 
                 const bool            ray_left=false,    // продолжение линии влево 
                 const bool            ray_right=false,   // продолжение линии вправо 
                 const bool            hidden=true,       // скрыт в списке объектов 
                 const long            z_order=0)         // приоритет на нажатие мышью 
  {

   ObjectDelete(chart_ID,name);

//--- создадим трендовую линию по заданным координатам 
   if(!ObjectCreate(chart_ID,name,OBJ_TREND,sub_window,time1,price1,time2,price2))
     {
      Print(__FUNCTION__,
            ": не удалось создать линию тренда! Код ошибки = ",GetLastError());
      return(false);
     }
//--- установим цвет линии 
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- установим стиль отображения линии 
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- установим толщину линии 
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- отобразим на переднем (false) или заднем (true) плане 
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- включим (true) или отключим (false) режим перемещения линии мышью 
//--- при создании графического объекта функцией ObjectCreate, по умолчанию объект 
//--- нельзя выделить и перемещать. Внутри же этого метода параметр selection 
//--- по умолчанию равен true, что позволяет выделять и перемещать этот объект 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- включим (true) или отключим (false) режим продолжения отображения линии влево 
   ObjectSetInteger(chart_ID,name,OBJPROP_RAY_LEFT,ray_left);
//--- включим (true) или отключим (false) режим продолжения отображения линии вправо 
   ObjectSetInteger(chart_ID,name,OBJPROP_RAY_RIGHT,ray_right);
//--- скроем (true) или отобразим (false) имя графического объекта в списке объектов 
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- установим приоритет на получение события нажатия мыши на графике 
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- успешное выполнение 
   return(true);
  }
//+------------------------------------------------------------------+ 
//| Функция удаления линий тренда                                    | 
//+------------------------------------------------------------------+   

void DelObs(string pref)
  {
   int n=ObjectsTotal(0,-1,-1);
   for(int i=n; i>=0; i--)
     {
      string sName=ObjectName(0,i,-1,-1);
      if(StringFind(sName,pref)!=-1)
        {
         ObjectDelete(0,sName);
        }
     }}
//+------------------------------------------------------------------+
