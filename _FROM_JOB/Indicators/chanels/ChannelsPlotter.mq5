//+------------------------------------------------------------------+
//|                                              ChannelsPlotter.mq5 |
//|                                           Copyright 2016, denkir |
//|                           https://login.mql5.com/ru/users/denkir |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, denkir"
#property link      "https://login.mql5.com/ru/users/denkir"
#property version   "1.00"
//--- include
#include "CFractalPoint.mqh"

//+------------------------------------------------------------------+
//| Inputs                                                           |
//+------------------------------------------------------------------+
sinput string Info_channels="+===--������--====+"; // +===--������--====+
input int InpPrevFracNum=3;                        // ���������� ���������
input int InpBarsBeside=5;                         // ����� �����/������ �� ��������
input int InpBarsBetween=1;                        // ������������� �����
input bool InpToDeletePrevious=true;               // ������� ���������� ������?
input bool InpIsAlt=true;                          // �������������� ���������?
input ENUM_RELEVANT_EXTREMUM InpRelevantPoint=RELEVANT_EXTREMUM_PREV; // ���������� �����
sinput bool InpIsArray=true;                       // ������������ ���?
sinput int InpLineWidth=3;                         // ������� �����
sinput bool InpToLog=true;                         // ����� ������?
//--- globals
CFractalSet myFractalSet;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if(!myFractalSet.Init(InpPrevFracNum,InpBarsBeside,InpBarsBetween,InpToDeletePrevious,
      InpIsAlt,InpRelevantPoint,InpIsArray,InpLineWidth,InpToLog))
     {
      Print("������ ������������� ������������ ������!");
      return INIT_FAILED;
     }
//---
   return INIT_SUCCEEDED;
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   myFractalSet.Deinit();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   myFractalSet.Process();
  }
//+------------------------------------------------------------------+
