//+------------------------------------------------------------------+
//|                                                      Channel.mqh |
//|                                             Copyright 2017, DNG® |
//|                                 http://www.mql5.com/ru/users/dng |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, DNG®"
#property link      "http://www.mql5.com/ru/users/dng"
#property version   "1.00"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
#include <Object.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct SPeackTrough
  {
   double            Val;
   int               Dir;
   datetime          Bar;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//struct SLevelParameters
//  {
//   int x1;
//   double y1;
//   int x2;
//   double y2;
//   double v;
//   int dir;
//   double target;
//   void Init()
//     {
//      x1=0;
//      y1=0;
//      x2=0;
//      y2=0;
//      v=0;
//      dir=0;   
//     }
//  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class CChannel : public CObject
  {
private:
   string            s_Symbol;      // Symbol
   ENUM_TIMEFRAMES   e_Timeframe;   // Timeframe
   int               i_Handle;      // Indicator's handle
   datetime          dt_LastCalc;   // Last calculated bar
   SPeackTrough      PeackTrough[]; // Array of ZigZag's peacks
   int               CurCount;      // Count of peaks
   int               PreDir;        // Previus ZigZag's leg direction
   int               CurDir;        // Current ZigZag's leg direction
   int               RequiredCount; // Minimal peacks in channel
   double            d_Diff;        
   bool              b_FoundChannel;
   bool              b_Breaked;
   datetime          dt_Breaked;
   double            d_BreakedPrice;                     

   void              RefreshLast(datetime time,double v);
   void              AddNew(datetime time,double v,int d);
   bool              CheckForm(double base);
   double            GetRessistPrice(SPeackTrough &start_peack, datetime time);
   double            GetSupportPrice(SPeackTrough &start_peack, datetime time);
   bool              DrawChannel(MqlRates &break_bar);
   bool              DrawChannel(void);
   bool              UnDrawChannel(void);

public:
                     CChannel(int handle,datetime start_time,string symbol,ENUM_TIMEFRAMES timeframe);
                    ~CChannel();
   bool              Calculate(ENUM_ORDER_TYPE &type,double &stop_loss,datetime &deal_time,bool &breaked,datetime &breaked_time);
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CChannel::CChannel(int handle,datetime start_time,string symbol,ENUM_TIMEFRAMES timeframe) : RequiredCount(4),
                                                                                             CurCount(0),
                                                                                             CurDir(0),
                                                                                             PreDir(0),
                                                                                             d_Diff(0.1),
                                                                                             b_Breaked(false),
                                                                                             dt_Breaked(0),
                                                                                             b_FoundChannel(false)
  {
   i_Handle=handle;
   dt_LastCalc=fmax(start_time-1,0);
   s_Symbol=symbol;
   e_Timeframe=timeframe;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CChannel::~CChannel()
  {
   UnDrawChannel();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CChannel::Calculate(ENUM_ORDER_TYPE &type,double &stop_loss,datetime &deal_time, bool &breaked,datetime &breaked_time)
  {
   MqlRates rates[];
   CurCount=ArraySize(PeackTrough);
   if(CurCount>0)
     {
      dt_LastCalc=PeackTrough[CurCount-1].Bar;
      CurDir=PeackTrough[CurCount-1].Dir;
     }
   int total=CopyRates(s_Symbol,e_Timeframe,fmax(dt_LastCalc-PeriodSeconds(e_Timeframe),0),TimeCurrent(),rates);
   if(total<=0)
      return false;
   stop_loss=-1;
   breaked=b_Breaked;
   breaked_time=dt_Breaked;
   deal_time=0;
   for(int i=0;i<total;i++)
     {
      if(rates[i].time>dt_LastCalc)
        {
         dt_LastCalc=rates[i].time;
         PreDir=CurDir;
        }
      else
         continue;

      // new max      

      double lhb[2];
      if(CopyBuffer(i_Handle,4,total-i-1,2,lhb)<=0)
         return false;

      if(lhb[0]!=lhb[1])
        {
         if(CurDir==1)
            RefreshLast(rates[i].time,rates[i].high);
         else
            AddNew(rates[i].time,rates[i].high,1);
        }

      // new min

      double llb[2];
      if(CopyBuffer(i_Handle,5,total-i-1,2,llb)<=0)
         return false;

      if(llb[0]!=llb[1])
        {
         if(CurDir==-1)
            RefreshLast(rates[i].time,rates[i].low);
         else
            AddNew(rates[i].time,rates[i].low,-1);
        }

      //===
      double base=(CurCount>=2 ? MathAbs(PeackTrough[1].Val-PeackTrough[0].Val) : 0);
   
      if(CurCount>=RequiredCount && !b_FoundChannel)
        {
         if(CurDir!=PreDir)
           {
            if(CheckForm(base))
              {
               b_FoundChannel=true;
              }
            else
              {
               UnDrawChannel();
               dt_LastCalc=PeackTrough[0].Bar+PeriodSeconds(e_Timeframe);
               ArrayFree(PeackTrough);
               CurCount=0;
               CurDir=0;
               PreDir=0;
               b_Breaked=false;
               dt_Breaked=0;
               b_FoundChannel=false;
               deal_time=0;
               total=CopyRates(s_Symbol,e_Timeframe,fmax(dt_LastCalc,0),TimeCurrent(),rates);
               i=-1;
               continue;
              }
           }
        }
      if(b_FoundChannel)
        {
         if(PeackTrough[0].Dir==1)
           {
            if(PeackTrough[0].Val>PeackTrough[2].Val)
              {
               if(!b_Breaked)
                 {
                  if((rates[i].close-GetRessistPrice(PeackTrough[0],rates[i].time))>=(d_Diff*base))
                    {
                     b_Breaked=breaked=true;
                     dt_Breaked=breaked_time=rates[i].time;
                     d_BreakedPrice=rates[i].high;
                     DrawChannel(rates[i]);
                     continue;
                    }
                  if(CurCount>4 && PeackTrough[CurCount-1].Dir==1 && (GetRessistPrice(PeackTrough[1],rates[i].time)-PeackTrough[CurCount-1].Val)>0)
                    {
                     int channels=ArraySize(ar_Channels);
                     if(ar_Channels[channels-1]==GetPointer(this))
                       {
                        SearchNewChannel(PeackTrough[CurCount-3].Bar-PeriodSeconds(e_Timeframe));
                       }
                    }
                 }
               else
                 {
                  if(rates[i].time<=dt_Breaked)
                     continue;
                  //---
                  double res_price=GetRessistPrice(PeackTrough[0],rates[i].time);
                  if(((rates[i].low-res_price)<=0 && (rates[i].close-res_price)>0 && (rates[i].close-res_price)<=(d_Diff*base)) || rates[i].close>d_BreakedPrice)
                    {
                     type=ORDER_TYPE_BUY;
                     stop_loss=res_price-base*(1+d_Diff);
                     deal_time=rates[i].time;
                     return true;
                    }
                 }
              }
            else
              {
               UnDrawChannel();
               dt_LastCalc=PeackTrough[0].Bar+PeriodSeconds(e_Timeframe);
               ArrayFree(PeackTrough);
               CurCount=0;
               CurDir=0;
               PreDir=0;
               b_Breaked=false;
               dt_Breaked=0;
               b_FoundChannel=false;
               deal_time=0;
               total=CopyRates(s_Symbol,e_Timeframe,fmax(dt_LastCalc,0),TimeCurrent(),rates);
               i=-1;
               continue;
              }
           }
         else
           {
            if(PeackTrough[0].Val<PeackTrough[2].Val)
              {
               if(!b_Breaked)
                 {
                  if((GetSupportPrice(PeackTrough[0],rates[i].time)-rates[i].close)>=(d_Diff*base))
                    {
                     b_Breaked=breaked=true;
                     dt_Breaked=breaked_time=rates[i].time;
                     d_BreakedPrice=rates[i].low;
                     DrawChannel(rates[i]);
                     continue;
                    }
                  if(CurCount>4 && PeackTrough[CurCount-1].Dir==-1 && (PeackTrough[CurCount-1].Val-GetSupportPrice(PeackTrough[1],rates[i].time))>0)
                    {
                     int channels=ArraySize(ar_Channels);
                     if(ar_Channels[channels-1]==GetPointer(this))
                       {
                        SearchNewChannel(PeackTrough[CurCount-3].Bar-PeriodSeconds(e_Timeframe));
                       }
                    }
                 }
               else
                 {
                  if(rates[i].time<=dt_Breaked)
                     continue;
                  double sup_price=GetSupportPrice(PeackTrough[0],rates[i].time);
                  if(((sup_price-rates[i].high)<=0 && (sup_price-rates[i].close)>0 && (sup_price-rates[i].close)<=(d_Diff*base)) || rates[i].close<d_BreakedPrice)
                    {
                     type=ORDER_TYPE_SELL;
                     stop_loss=sup_price+base*(1+d_Diff);
                     deal_time=rates[i].time;
                     return true;
                    }
                 }
              }
            else
              {
               UnDrawChannel();
               dt_LastCalc=PeackTrough[0].Bar+PeriodSeconds(e_Timeframe);
               ArrayFree(PeackTrough);
               CurCount=0;
               CurDir=0;
               PreDir=0;
               b_Breaked=false;
               dt_Breaked=0;
               b_FoundChannel=false;
               deal_time=0;
               total=CopyRates(s_Symbol,e_Timeframe,fmax(dt_LastCalc,0),TimeCurrent(),rates);
               i=-1;
               continue;
              }
           }
        }
     }
   return b_Breaked;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CChannel::RefreshLast(datetime time,double v)
  {
   PeackTrough[CurCount-1].Bar=time;
   PeackTrough[CurCount-1].Val=v;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
CChannel::AddNew(datetime time,double v,int d)
  {
   if(CurCount>=ArraySize(PeackTrough))
      ArrayResize(PeackTrough,ArraySize(PeackTrough)+1);

   PeackTrough[CurCount].Dir=d;
   PeackTrough[CurCount].Val=v;
   PeackTrough[CurCount].Bar=time;
   CurCount++;
   CurDir=d;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CChannel::CheckForm(double base)
  {
   int total=ArraySize(PeackTrough)-1;
   for(int i=0;(i+1)<total;i+=2)
     {
      double lv=MathAbs(PeackTrough[i].Val-PeackTrough[i+1].Val);
      if(MathAbs(lv-base)>d_Diff*base)
         return(false);
     }
   DrawChannel();
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CChannel::GetRessistPrice(SPeackTrough &start_peack, datetime time)
  {
   int total=ArraySize(PeackTrough);
   double price1=(PeackTrough[0].Dir==1 ? PeackTrough[0].Val : PeackTrough[1].Val);
   double result=DBL_MIN;
   int bar2=Bars(s_Symbol,e_Timeframe,start_peack.Bar,time);
   for(int i=2;(i<total && bar2>0);i++)
     {
      if(PeackTrough[i].Dir!=1)
         continue;
      if(PeackTrough[i].Bar>=time || (b_Breaked && PeackTrough[i].Bar>=dt_Breaked))
         break;
      double price2=PeackTrough[i].Val;
      int bar1=Bars(s_Symbol,e_Timeframe,(PeackTrough[0].Dir==1 ? PeackTrough[0].Bar : PeackTrough[1].Bar),PeackTrough[i].Bar);
      if(bar1==0)
         continue;
      //---
      double price3=start_peack.Val-(price1-price2)/bar1*bar2;
      result=MathMax(result,price3);
     }
   return (result==DBL_MIN ? DBL_MAX : result);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double CChannel::GetSupportPrice(SPeackTrough &start_peack, datetime time)
  {
   int total=ArraySize(PeackTrough);
   double price1=(PeackTrough[0].Dir==-1 ? PeackTrough[0].Val : PeackTrough[1].Val);
   int bar2=Bars(s_Symbol,e_Timeframe,start_peack.Bar,time);
   double result=DBL_MAX;
   for(int i=2;(i<total && bar2>0);i++)
     {
      if(PeackTrough[i].Dir!=-1)
         continue;
      if(PeackTrough[i].Bar>=time || (b_Breaked && PeackTrough[i].Bar>=dt_Breaked))
         break;
      double price2=PeackTrough[i].Val;
      int bar1=Bars(s_Symbol,e_Timeframe,(PeackTrough[0].Dir==-1 ? PeackTrough[0].Bar : PeackTrough[1].Bar),PeackTrough[i].Bar);
      if(bar1==0)
         continue;
      //---
      double price3=start_peack.Val-(price1-price2)/bar1*bar2;
      result=MathMin(result,price3);
     }
   return (result==DBL_MAX ? DBL_MIN : result);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CChannel::DrawChannel(MqlRates &break_bar)
  {
   string name="Channel_"+TimeToString(PeackTrough[0].Bar);
   if(!ObjectCreate(0,name,OBJ_CHANNEL,0,PeackTrough[0].Bar,PeackTrough[0].Val,PeackTrough[2].Bar,PeackTrough[2].Val,PeackTrough[1].Bar,PeackTrough[1].Val))
      {};//return false;
   ObjectSetInteger(0,name,OBJPROP_RAY_RIGHT,true);
   ObjectCreate(0,name+"_break",OBJ_ARROW_STOP,0,break_bar.time,(break_bar.open<break_bar.close ? break_bar.high : break_bar.low));
   ChartRedraw(0);
   //Sleep(0);
   return true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CChannel::DrawChannel(void)
  {
   string name="Channel_"+TimeToString(PeackTrough[0].Bar);
   if(!ObjectCreate(0,name,OBJ_CHANNEL,0,PeackTrough[0].Bar,PeackTrough[0].Val,PeackTrough[2].Bar,PeackTrough[2].Val,PeackTrough[1].Bar,PeackTrough[1].Val))
      return false;
   ObjectSetInteger(0,name,OBJPROP_RAY_RIGHT,true);
   //ObjectCreate(0,name+"_breal",OBJ_ARROW_STOP,0,break_bar.time,(break_bar.open<break_bar.close ? break_bar.high : break_bar.low));
   ChartRedraw(0);
   //Sleep(0);
   return true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CChannel::UnDrawChannel(void)
  {
   if(ArraySize(PeackTrough)<=0)
      return true;
   string name="Channel_"+TimeToString(PeackTrough[0].Bar);
   bool result=true;
   int total=ObjectsTotal(0);
   for(int i=total-1;i>=0;i--)
     {
      string obj_name=ObjectName(0,i);
      if(StringFind(obj_name,name,0)>=0)
         result=(result && ObjectDelete(0,obj_name));
     }
   ChartRedraw(0);
   return true;
  }
//+------------------------------------------------------------------+
