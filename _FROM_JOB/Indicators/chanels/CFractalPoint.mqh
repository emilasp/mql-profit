//+------------------------------------------------------------------+
//|                                                CFractalPoint.mqh |
//|                                           Copyright 2016, denkir |
//|                           https://login.mql5.com/ru/users/denkir |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, denkir"
#property link      "https://login.mql5.com/ru/users/denkir"
//--- include
#include <ChartObjects\ChartObjectsChannels.mqh>
#include <Arrays\ArrayObj.mqh>
#include  "CisNewBar.mqh"
//+------------------------------------------------------------------+
//| ��� ����������                                                   |
//+------------------------------------------------------------------+
enum ENUM_EXTREMUM_TYPE
  {
   EXTREMUM_TYPE_MIN=0, // �������
   EXTREMUM_TYPE_MAX=1, // ��������
  };
//+------------------------------------------------------------------+
//| ��� ������ ������������� �����                                   |
//+------------------------------------------------------------------+
enum ENUM_SET_TYPE
  {
   SET_TYPE_NONE=0,     // �� �����
   SET_TYPE_MINMAX=1,   // ���-����-���
   SET_TYPE_MAXMIN=2,   // ����-���-����                       
  };
//+------------------------------------------------------------------+
//| ��� ���������� �����                                             |
//+------------------------------------------------------------------+
enum ENUM_RELEVANT_EXTREMUM
  {
   RELEVANT_EXTREMUM_PREV=0, // ����������
   RELEVANT_EXTREMUM_LAST=1, // ���������
  };
//+------------------------------------------------------------------+
//| ������� ������ ��������                                          |
//+------------------------------------------------------------------+
struct SFracData
  {
   double            value; // ��������
   int               index; // ������ ����
   //--- �����������
   void SFracData::SFracData(void)
     {
      value=EMPTY_VALUE;
      index=WRONG_VALUE;
     }
  };
//+------------------------------------------------------------------+
//| ����� ����������� �����                                          |
//+------------------------------------------------------------------+
class CFractalPoint : public CObject
  {
   //--- === Data members === --- 
private:
   datetime          m_date;           // ���� � �����
   double            m_value;          // ��������
   ENUM_EXTREMUM_TYPE m_extreme_type;  // ��� ����������
   int               m_idx;            // ������ (�� 0 �� 2)

   //--- === Methods === --- 
public:
   //--- �����������/����������
   void              CFractalPoint(void);
   void              CFractalPoint(datetime _date,double _value,
                                   ENUM_EXTREMUM_TYPE _extreme_type,int _idx);
   void             ~CFractalPoint(void){};
   //--- get-������
   datetime          Date(void) const {return m_date;};
   double            Value(void) const {return m_value;};
   ENUM_EXTREMUM_TYPE FractalType(void) const {return m_extreme_type;};
   int               Index(void) const {return m_idx;};
   //--- set-������
   void              Date(const datetime _date) {m_date=_date;};
   void              Value(const double _value) {m_value=_value;};
   void              FractalType(const ENUM_EXTREMUM_TYPE extreme_type) {m_extreme_type=extreme_type;};
   void              Index(const int _bar_idx){m_idx=_bar_idx;};
   //--- ������
   void              Copy(const CFractalPoint &_source_frac);
   void              Print(void);
  };
//+------------------------------------------------------------------+
//| ����������� �� ���������                                         |
//+------------------------------------------------------------------+
void CFractalPoint::CFractalPoint(void)
  {
   m_date=0;
   m_value=0.;
   m_extreme_type=-1;
   m_idx=WRONG_VALUE;
  };
//+------------------------------------------------------------------+
//| ����������� �� ������� �������������                             |
//+------------------------------------------------------------------+
void CFractalPoint::CFractalPoint(datetime _date,double _value,
                                  ENUM_EXTREMUM_TYPE _extreme_type,int _idx):
                                  m_date(_date),
                                  m_value(_value),
                                  m_extreme_type(_extreme_type),
                                  m_idx(_idx){};
//+------------------------------------------------------------------+
//| �����������                                                      |
//+------------------------------------------------------------------+
void CFractalPoint::Copy(const CFractalPoint &_source_frac)
  {
   m_date=_source_frac.m_date;
   m_value=_source_frac.m_value;
   m_extreme_type=_source_frac.m_extreme_type;
   m_idx=_source_frac.m_idx;
  };
//+------------------------------------------------------------------+
//| ������                                                           |
//+------------------------------------------------------------------+
void CFractalPoint::Print(void)
  {
   Print("\n---=== ������ ����������� ����� ===---");
   Print("����: ",TimeToString(m_date));
   Print("����: ",DoubleToString(m_value,_Digits));
   Print("���: ",EnumToString(m_extreme_type));
   Print("������: ",IntegerToString(m_idx));
  }
//+------------------------------------------------------------------+
//| ����� ������ ����������� �����                                   |
//+------------------------------------------------------------------+
class CFractalSet : protected CArrayObj
  {
   //--- === Data members === --- 
private:
   ENUM_SET_TYPE     m_set_type;           // ��� ������ �����
   int               m_fractal_num;        // ������������� ����� �����
   int               m_fractals_ha;        // ����� ������������ ���������� 
   CisNewBar         m_new_bar;            // ������ ������ ����
   CArrayObj         m_channels_arr;       // ������ ������� ����������
   color             m_channel_colors[4];  // ����� �������
   bool              m_is_init;            // ���� �������������
   //--- ��������� ������
   int               m_prev_frac_num;      // ���������� ���������
   int               m_bars_beside;        // ����� �����/������ �� ��������
   int               m_bars_between;       // ����� ������������� �����  
   bool              m_to_delete_prev;     // ������� ���������� ������?
   bool              m_is_alt;             // �������������� ��������� ���������?
   ENUM_RELEVANT_EXTREMUM m_rel_frac;      // ���������� �����
   bool              m_is_array;           // ������������ ���?
   int               m_line_wid;           // ������� �����
   bool              m_to_log;             // ����� ������?

   //--- === Methods === --- 
public:
   //--- �����������/����������
   void              CFractalSet(void);
   void              CFractalSet(const CFractalSet &_src_frac_set);
   void             ~CFractalSet(void){};
   //---
   void              operator=(const CFractalSet &_src_frac_set);
   //--- �����������
   bool              Init(
                          int _prev_frac_num,
                          int _bars_beside,
                          int _bars_between=0,
                          bool _to_delete_prev=true,
                          bool _is_alt=false,
                          ENUM_RELEVANT_EXTREMUM _rel_frac=RELEVANT_EXTREMUM_PREV,
                          bool _is_arr=false,
                          int _line_wid=3,
                          bool _to_log=true
                          );
   void              Deinit(void);
   void              Process(void);
   //--- ������
   CChartObjectChannel *GetChannelByIdx(const int _ch_idx);
   int               ChannelsTotal(void) const {return m_channels_arr.Total();};

private:
   int               AddFrac(const int _buff_len);
   int               CheckSet(const SFracData &_fractals[]);
   ENUM_SET_TYPE     GetTypeOfSet(void) const {return m_set_type;};
   void              SetTypeOfSet(const ENUM_SET_TYPE _set_type) {m_set_type=_set_type;};
   bool              PlotChannel(void);
   bool              Crop(const uint _num_to_crop);
   void              BubbleSort(void);
  };
//+------------------------------------------------------------------+
//| �����������                                                      |
//+------------------------------------------------------------------+
void CFractalSet::CFractalSet(void)
  {
   m_set_type=WRONG_VALUE;
   m_fractal_num=3;
   m_prev_frac_num=0;
//--- �����
   m_channel_colors[0]=clrBlack;
   m_channel_colors[1]=clrGreen;
   m_channel_colors[2]=clrRed;
   m_channel_colors[3]=clrBlue;
   m_is_init=false;
  };
//+------------------------------------------------------------------+
//| ����������� �����������                                          |
//+------------------------------------------------------------------+
void CFractalSet::CFractalSet(const CFractalSet &_src_frac_set)
  {
   this.m_set_type=_src_frac_set.m_set_type;
   this.m_fractal_num=_src_frac_set.m_fractal_num;
   this.m_fractals_ha=_src_frac_set.m_fractals_ha;
   this.m_new_bar=_src_frac_set.m_new_bar;
//--- ����������� ��������
   if(!this.m_channels_arr.AssignArray(GetPointer(_src_frac_set.m_channels_arr)))
     {
      PrintFormat(__FUNCTION__+": ������ ������������ �����������!");
      return;
     }
   else
     {
      for(int idx=0;idx<ArraySize(m_channel_colors);idx++)
         m_channel_colors[idx]=_src_frac_set.m_channel_colors[idx];
     }
//---
   this.m_prev_frac_num=_src_frac_set.m_prev_frac_num;
   this.m_bars_beside=_src_frac_set.m_bars_beside;
   this.m_bars_between=_src_frac_set.m_bars_between;
   this.m_to_delete_prev=_src_frac_set.m_to_delete_prev;
   this.m_is_alt=_src_frac_set.m_is_alt;
   this.m_rel_frac=_src_frac_set.m_rel_frac;
   this.m_is_array=_src_frac_set.m_is_array;
   this.m_line_wid=_src_frac_set.m_line_wid;
   this.m_to_log=_src_frac_set.m_to_log;
  }
//+------------------------------------------------------------------+
//| �������� ������������                                            |
//+------------------------------------------------------------------+
void CFractalSet::operator=(const CFractalSet &_src_frac_set)
  {
   this.m_set_type=_src_frac_set.m_set_type;
   this.m_fractal_num=_src_frac_set.m_fractal_num;
   this.m_fractals_ha=_src_frac_set.m_fractals_ha;
   this.m_new_bar=_src_frac_set.m_new_bar;
   if(!this.m_channels_arr.FreeMode())
      this.m_channels_arr.FreeMode(true);
//--- ����������� ��������
   if(!this.m_channels_arr.AssignArray(GetPointer(_src_frac_set.m_channels_arr)))
     {
      PrintFormat(__FUNCTION__+": ������ ��������� ������������!");
      return;
     }
   else
     {
      for(int idx=0;idx<ArraySize(m_channel_colors);idx++)
         m_channel_colors[idx]=_src_frac_set.m_channel_colors[idx];
     }
   this.m_prev_frac_num=_src_frac_set.m_prev_frac_num;
   this.m_bars_beside=_src_frac_set.m_bars_beside;
   this.m_bars_between=_src_frac_set.m_bars_between;
   this.m_to_delete_prev=_src_frac_set.m_to_delete_prev;
   this.m_is_alt=_src_frac_set.m_is_alt;
   this.m_rel_frac=_src_frac_set.m_rel_frac;
   this.m_is_array=_src_frac_set.m_is_array;
   this.m_line_wid=_src_frac_set.m_line_wid;
   this.m_to_log=_src_frac_set.m_to_log;
  }
//+------------------------------------------------------------------+
//| �������������                                                    |
//+------------------------------------------------------------------+
bool CFractalSet::Init(
                       int _prev_frac_num,
                       int _bars_beside,
                       int _bars_between=0,
                       bool _to_delete_prev=true,
                       bool _is_alt=false,
                       ENUM_RELEVANT_EXTREMUM _rel_frac=RELEVANT_EXTREMUM_PREV,
                       bool _is_arr=false,
                       int _line_wid=3,
                       bool _to_log=true
                       )
  {
//--- ��������� ����� ���������� ����������� �����
   if((_prev_frac_num<0) || (_prev_frac_num>3))
     {
      Print("����������� ������ ����� ���������� ����������� �����!");
      return false;
     }
//--- ��������� ����� ����� �� ��������
   if(_bars_beside<1)
     {
      Print("����������� ������ ����� ����� ����� � ������ �� ��������!");
      return false;
     }

//--- ���������� �������
   if(!this.FreeMode())
      this.FreeMode(true);
   if(!m_channels_arr.FreeMode())
      m_channels_arr.FreeMode(true);
//--- ������� ��������� ���������
   m_is_alt=_is_alt;
   if(m_is_alt)
      m_fractals_ha=iCustom(_Symbol,_Period,"X-bars_Fractals",_bars_beside,_bars_beside);
   else
      m_fractals_ha=iFractals(_Symbol,_Period);
//---
   if(m_fractals_ha==INVALID_HANDLE)
     {
      Print("������ �������� ������ ���������� ���������!");
      return false;
     }
//--- �����-������
   m_prev_frac_num=_prev_frac_num;
   m_bars_between=_bars_between;
   m_rel_frac=_rel_frac;
   m_is_array=_is_arr;
   m_to_delete_prev=_to_delete_prev;
   m_line_wid=_line_wid;
   m_bars_beside=_bars_beside;
   m_to_log=_to_log;
//--- ���� �������� ���������� ����������� �����
   if(m_prev_frac_num>0)
     {
      //--- 1) �������� ������� [start]
      bool synchronized=false;
      //--- ������� �����
      int attempts=0;
      //--- 10 ������� ��������� �������������
      while(attempts<10)
        {
         if(SeriesInfoInteger(_Symbol,0,SERIES_SYNCHRONIZED))
           {
            synchronized=true;
            //--- ���� �������������, �������
            break;
           }
         //--- �������� �������
         attempts++;
         //--- �������� 50 ����������� �� ��������� ��������
         Sleep(50);
        }
      //---
      if(!synchronized)
        {
         Print("�� ������� �������� ���������� ����� �� ",_Symbol);
         return false;
        }
      int curr_bars_num=Bars(_Symbol,_Period);
      if(curr_bars_num>0)
        {
         PrintFormat("���������� ����� � ������� ��������� �� �������-������� �� ������ ������: %d",
                     curr_bars_num);
        }
      //--- 1) �������� ������� [end]

      //--- 2) ������������ ������ ��� �������������� ���������� [start]
      double Ups[];
      int i,copied=CopyBuffer(m_fractals_ha,0,0,curr_bars_num,Ups);
      if(copied<=0)
        {
         Sleep(50);
         for(i=0;i<100;i++)
           {
            if(BarsCalculated(m_fractals_ha)>0)
               break;
            Sleep(50);
           }
         copied=CopyBuffer(m_fractals_ha,0,0,curr_bars_num,Ups);
         if(copied<=0)
           {
            Print("�� ������� ����������� ������� ��������. Error = ",GetLastError(),
                  "i=",i,"    copied= ",copied);
            return false;
           }
         else
           {
            if(m_to_log)
               Print("������� ����������� ������� ��������.",
                     " i = ",i,"    copied = ",copied);
           }
        }
      else
        {
         if(m_to_log)
            Print("������� ����������� ������� ��������. ArraySize = ",ArraySize(Ups));
        }
      //--- 2) ������������ ������ ��� �������������� ���������� [end]

      //--- 3) ���������� ����������� ����� [start]
      int prev_fracs_num=AddFrac(curr_bars_num-1);
      if(m_to_log)
         if(prev_fracs_num>0)
            PrintFormat("��������� ���������� ���������: %d",prev_fracs_num);
      //--- ���� ����� ��������� �����
      if(prev_fracs_num==3)
         if(!this.PlotChannel())
            Print("�� ������� ���������� �����!");
      //--- 3) ���������� ����������� ����� [end]
     }
   m_is_init=true;
//---
   return true;
  }
//+------------------------------------------------------------------+
//| ���������������                                                  |
//+------------------------------------------------------------------+
void CFractalSet::Deinit(void)
  {
//--- ���� ����� ������������ - �� ������� ������
   if(MQLInfoInteger(MQL_TESTER))
     {
      //--- ���������� �������
      this.FreeMode(false);
      m_channels_arr.FreeMode(false);
     }
  }
//+------------------------------------------------------------------+
//| ���������                                                        |
//+------------------------------------------------------------------+
void CFractalSet::Process(void)
  {
//--- �������� ����� �������� �������� ����
   datetime times[1];
   if(CopyTime(_Symbol,_Period,0,1,times)!=1)
      return;
//---
   bool is_new_bar=m_new_bar.isNewBar(times[0]);
//--- �������� �� ��������� ������ ����
   if(is_new_bar)
     {
      //--- �������� ������� ����� �� ���������� ����
      //--- ����� ������� �����
      int ch_num=m_channels_arr.Total();
      if(ch_num>0)
        {
         CChartObjectChannel  *ptr_curr_ch=m_channels_arr.At(ch_num-1);
         if(CheckPointer(ptr_curr_ch)==POINTER_DYNAMIC)
           {
            datetime time1,time2;
            time1=(datetime)ptr_curr_ch.GetInteger(OBJPROP_TIME,0);
            time2=(datetime)ptr_curr_ch.GetInteger(OBJPROP_TIME,1);
            //--- ����� ����� 1-�� � 2-�� �������
            datetime bars_dates[];
            int bars_between=CopyTime(_Symbol,_Period,
                                      time1,time2,bars_dates
                                      );
            if(bars_between>1)
              {
               bars_between-=1;
               double price1=ptr_curr_ch.GetDouble(OBJPROP_PRICE,0);
               double price2=ptr_curr_ch.GetDouble(OBJPROP_PRICE,1);
               //--- ����� ������������
               double price_differential=MathAbs(price1-price2);
               //--- ������� �������� (������� ��������� �� 1-�� ����)
               double price_speed=price_differential/bars_between;
               //--- ����� ����� 2-�� ������ � ������� �����
               bars_between=CopyTime(_Symbol,_Period,
                                     time2,times[0],bars_dates
                                     );
               if(bars_between>1)
                 {
                  bars_between-=1;
                  //--- ���� ���������� �����
                  if(price1<price2)
                     price2+=(bars_between*price_speed);
                  //--- ��� ���� ���������� �����
                  else
                     price2-=(bars_between*price_speed);
                  //--- ����� ���������� 2-�� �����
                  ptr_curr_ch.SetPoint(1,times[0],price2);
                  //--- ����������� �����
                  ChartRedraw();
                 }
              }
           }
        }

      //--- �������� �� ��������� ������ ����������
      int frac_num=AddFrac(1);
      //--- ������ � �������
      string code_str=NULL;
      if(frac_num<0)
        {
         code_str="\n��������� ������";
        }
      else if(frac_num>0)
        {
         code_str=StringFormat("\n����� � ������: %d",frac_num);
         if(frac_num==m_fractal_num)
           {
            //--- ���������� �����
            if(!this.PlotChannel())
               Print("�� ������� ���������� �����!");
            code_str+="\n������� ����� ����� �����.";
           }
        }
      //---
      if(m_to_log)
         if(code_str!=NULL)
            Print("\n--==��������� ��������==--"+code_str);
     }
  }
//+------------------------------------------------------------------+
//| ��������� �� ����� �� �������                                    |
//+------------------------------------------------------------------+
CChartObjectChannel *CFractalSet::GetChannelByIdx(const int _ch_idx)
  {
   CChartObjectChannel *ptr_curr_ch=NULL;
//---
   if(m_channels_arr.Total()>0)
     {
      ptr_curr_ch=this.m_channels_arr.At(_ch_idx);
      if(ptr_curr_ch==NULL)
         PrintFormat("������ ��������� �������� ��� �������� %d",_ch_idx);
     }
//---
   return ptr_curr_ch;
  }
//+------------------------------------------------------------------+
//| ���������� ����������� �����                                     |
//+------------------------------------------------------------------+
int CFractalSet::AddFrac(const int _buff_len)
  {
   int fractal_num=0;
//--- �������� �� ��������� ������ ����������
   double up_vals[];
   double down_vals[];
   int bars_num=_buff_len-m_bars_beside;
   if(bars_num<0)
      bars_num=1;

//--- ������� �������
   if(CopyBuffer(m_fractals_ha,0,m_bars_beside,bars_num,up_vals)<0)
     {
      Print("������ ��������� �������� �������� ��������.");
      return -1;
     }
//--- ������ �������
   if(CopyBuffer(m_fractals_ha,1,m_bars_beside,bars_num,down_vals)<0)
     {
      Print("������ ��������� �������� ������� ��������.");
      return -1;
     }
//--- ��� ����-�����
   ArraySetAsSeries(up_vals,true);
   ArraySetAsSeries(down_vals,true);
//--- ��������� ������� ���������
   for(int idx=0;idx<_buff_len;idx++)
     {
      SFracData fracs[2]; //  ������ ������ ��������: [0]-������, [1]-�������
      bool is_new_down=false,is_new_up=false;
      if((down_vals[idx]<DBL_MAX) && (down_vals[idx]>0.))
        {
         is_new_down=true;
         fracs[0].index=m_bars_beside+idx;
         if(m_to_log)
            PrintFormat("\n������ �������: %."+IntegerToString(_Digits)+"f",down_vals[idx]);
        }
      if((up_vals[idx]<DBL_MAX) && (up_vals[idx]>0.))
        {
         is_new_up=true;
         fracs[1].index=m_bars_beside+idx;
         if(m_to_log)
            PrintFormat("\n������� �������: %."+IntegerToString(_Digits)+"f",up_vals[idx]);
        }
      //--- ���� ���� ����� �������
      if(is_new_down || is_new_up)
        {
         double frac_vals[2];// ������ ���������: [0]-������, [1]-�������
         fracs[0].value=down_vals[idx];
         fracs[1].value=up_vals[idx];
         //--- ��������� ��������� ������
         fractal_num=this.CheckSet(fracs);
         if(fractal_num==m_prev_frac_num)
            break;
        }
     }
//--- ���� �� ���� �������������
   if(!m_is_init)
     {
      this.BubbleSort();
      //--- ����������
      if(!this.IsSorted(1))
        {
         Print("������ ���������� ����������� �����.");
         return -1;
        }
     }
//---
   return fractal_num;
  }
//+------------------------------------------------------------------+
//| ���������� ���                                                   |
//+------------------------------------------------------------------+
int CFractalSet::CheckSet(const SFracData &_fractals[])
  {
//---  �������� ������������ ������ �� ���������� ����
   MqlRates rates[];
   for(int idx=0;idx<ArraySize(_fractals);idx++)
      if(_fractals[idx].index>WRONG_VALUE)
        {
         if(CopyRates(_Symbol,_Period,_fractals[idx].index,1,rates)!=1)
           {
            Print("������ ��������� ������������ ������ �� ���������� ����.");
            return -1;
           }
         break;
        }
//--- ���������� ����������� ����� �� ��������� �����
   CArrayObj temp_add_set;
   for(int idx=0;idx<ArraySize(_fractals);idx++)
      if((_fractals[idx].value<DBL_MAX) && (_fractals[idx].value>0.))
        {
         //--- ������� ������ ����� ��������
         CFractalPoint *ptr_new_fractal=new CFractalPoint;
         if(CheckPointer(ptr_new_fractal)!=POINTER_DYNAMIC)
            return -1;
         //---
         ENUM_EXTREMUM_TYPE new_fractal_type=WRONG_VALUE;
         //--- ������� ������ ��� ����� ��������
         //--- 1) �����
         ptr_new_fractal.Date(rates[0].time);
         //--- 2) ����
         if(idx==1)
           {
            new_fractal_type=EXTREMUM_TYPE_MAX;
            ptr_new_fractal.Value(rates[0].high);
           }
         else
           {
            new_fractal_type=EXTREMUM_TYPE_MIN;
            ptr_new_fractal.Value(rates[0].low);
           }
         //--- 3) ���
         ptr_new_fractal.FractalType(new_fractal_type);

         //--- ���������� � �����
         if(!temp_add_set.Add(ptr_new_fractal))
           {
            Print("������ ���������� ����� ��������!");
            delete ptr_new_fractal;
            return -1;
           }
        }
//--- ��������� ����� ����������� ����� �� ��������� �����
   int frac_num_to_add=temp_add_set.Total();
   if(frac_num_to_add<1)
      return -1;
//---
   bool is_emptied=false; // ����� ���������?
   int curr_fractal_num=0;
//--- ���������� ����� � �����
   for(int frac_idx=0;frac_idx<frac_num_to_add;frac_idx++)
     {
      CFractalPoint *ptr_temp_frac=temp_add_set.At(frac_idx);
      if(CheckPointer(ptr_temp_frac)!=POINTER_DYNAMIC)
        {
         Print("������ ��������� ������� ����������� ����� �� ���������� ������!");
         return -1;
        }
      //--- ���� ��������� ����� ����� ����� ��������� � ������� �������
      if(m_bars_between>0)
        {
         curr_fractal_num=this.Total();
         if(curr_fractal_num>0)
           {
            CFractalPoint *ptr_prev_frac=this.At(curr_fractal_num-1);
            if(CheckPointer(ptr_prev_frac)!=POINTER_DYNAMIC)
              {
               Print("������ ��������� ������� ����������� ����� �� ������!");
               return -1;
              }
            datetime time1,time2;
            time1=ptr_prev_frac.Date();
            time2=ptr_temp_frac.Date();
            //--- ����� ����� �������
            datetime bars_dates[];
            int bars_between=CopyTime(_Symbol,_Period,
                                      time1,time2,bars_dates
                                      );
            if(bars_between<0)
              {
               Print("������ ��������� ������ ������� �������� �����!");
               return -1;
              }
            bars_between-=2;
            //--- ���� �� ������ �����
            if(bars_between>=0)
               //--- ���� ������������� ����� ������������ 
               if(bars_between<m_bars_between)
                 {
                  bool to_delete_frac=false;
                  if(m_to_log)
                     Print("������������� ����� ������������. ����� ��������� ���� �����.");
                  //--- ���� ��������� ���������� �����
                  if(m_rel_frac==RELEVANT_EXTREMUM_PREV)
                    {
                     datetime curr_frac_date=time2;
                     //--- ���� ���� �������������
                     if(m_is_init)
                       {
                        continue;
                       }
                     //--- ���� ������������� �� ����
                     else
                       {
                        //--- ������� ������� �����
                        to_delete_frac=true;
                        curr_frac_date=time1;
                       }
                     if(m_to_log)
                       {
                        PrintFormat("������� ����� ����� ���������: %s",
                                    TimeToString(curr_frac_date));
                       }
                    }
                  //--- ���� ��������� ��������� �����
                  else
                    {
                     datetime curr_frac_date=time1;
                     //--- ���� ���� �������������
                     if(m_is_init)
                       {
                        //--- ������� ���������� �����
                        to_delete_frac=true;
                       }
                     //--- ���� ������������� �� ����
                     else
                       {
                        curr_frac_date=time2;
                       }
                     if(m_to_log)
                        PrintFormat("���������� ����� ����� ���������: %s",
                                    TimeToString(curr_frac_date));
                     if(curr_frac_date==time2)
                        continue;

                    }
                  //--- ���� ������� �����
                  if(to_delete_frac)
                    {
                     if(!this.Delete(curr_fractal_num-1))
                       {
                        Print("������ �������� ��������� ����� � ������!");
                        return -1;
                       }
                    }
                 }
           }
        }
      //--- ���������� ����������� ����� � ������� ����� - �����������
      CFractalPoint *ptr_new_fractal=new CFractalPoint;
      if(CheckPointer(ptr_new_fractal)==POINTER_DYNAMIC)
        {
         ptr_new_fractal.Copy(ptr_temp_frac);
         if(!this.Add(ptr_new_fractal))
           {
            Print("������ ���������� ����� �������� � ������� �����!");
            delete ptr_new_fractal;
            return -1;
           }
         //--- ������ �����
         ptr_new_fractal.Index(this.Total()-1);
        }
     }
//--- ��������� ������
   curr_fractal_num=this.Total();
//--- ���� ���� ������ �����
   if(curr_fractal_num>m_fractal_num)
     {
      uint num_to_crop=curr_fractal_num-m_fractal_num;
      //--- ��������� �����
      if(!this.Crop(num_to_crop))
        {
         Print("������ �������� ������ ����� �� ������!");
         return -1;
        }
      //--- �������� ����� ����� � ������
      curr_fractal_num=this.Total();
     }
//--- ���� ����� ����������
   if(curr_fractal_num==m_fractal_num)
     {
      //--- ���������� ��� ������
      int min_cnt,max_cnt; // ��������
      min_cnt=max_cnt=0;
      for(int frac_idx=0;frac_idx<curr_fractal_num;frac_idx++)
        {
         //--- �������� �����
         CFractalPoint *ptr_curr_frac=this.At(frac_idx);
         if(CheckPointer(ptr_curr_frac)!=POINTER_DYNAMIC)
           {
            Print("������ ��������� ������� ����������� ����� �� ������!");
            return -1;
           }
         //--- ���������� ���
         ENUM_EXTREMUM_TYPE curr_frac_type=ptr_curr_frac.FractalType();
         if(curr_frac_type==EXTREMUM_TYPE_MIN)
            min_cnt++;
         else if(curr_frac_type==EXTREMUM_TYPE_MAX)
            max_cnt++;
        }
      //--- ���� 2 �������� � 1 ��������
      if((min_cnt==2) && (max_cnt==1))
         this.SetTypeOfSet(SET_TYPE_MINMAX);
      //--- ���� 1 ������� � 2 ���������
      else if((min_cnt==1) && (max_cnt==2))
         this.SetTypeOfSet(SET_TYPE_MAXMIN);
      //--- ����� ������� ������ ��� ��������� �����, ���� ��� �� �����
      else
        {
         //--- ���� ���� ������������� - ������� ������
         if(m_is_init)
           {
            if(!this.Crop(1))
              {
               Print("������ �������� 1-�� ����� � ���������� ������!");
               this.Clear();
               return false;
              }
            if(m_to_log)
               Print("������� 1-�� ����� �� ����������� ������.");
           }
         //--- ���� ������������� �� ����  - ������� ���������
         else
           {
            if(!this.Delete(curr_fractal_num-1))
              {
               Print("������ �������� ��������� ����� � ���������� ������!");
               this.Clear();
               return false;
              }
            if(m_to_log)
               Print("������� ��������� ����� �� ����������� ������.");
           }
        }
     }
//---
   return this.Total();
  }
//+------------------------------------------------------------------+
//| ���������� �����                                                 |
//+------------------------------------------------------------------+
bool CFractalSet::PlotChannel(void)
  {
//--- �������� ����� �� ������
   bool is_1_point=false;
   datetime times[3];
   double prices[3];
   ArrayInitialize(times,0);
   ArrayInitialize(prices,0.);
   ENUM_SET_TYPE curr_set_type=this.GetTypeOfSet();
   if(curr_set_type<=SET_TYPE_NONE)
     {
      Print("����� �� ����� ���������: �� ����� ��� ������ ������������� �����!");
      return false;
     }
//--- ������� ��������-������� ����������
   for(int frac_idx=0;frac_idx<m_fractal_num;frac_idx++)
     {
      CFractalPoint *ptr_curr_point=this.At(frac_idx);
      if(CheckPointer(ptr_curr_point)!=POINTER_DYNAMIC)
        {
         Print("������ ��������� ����� ��������.");
         return false;
        }
      ENUM_EXTREMUM_TYPE curr_frac_type=ptr_curr_point.FractalType();
      datetime curr_point_time=ptr_curr_point.Date();
      double curr_point_price=ptr_curr_point.Value();
      //--- ����� �����
      if(curr_set_type==SET_TYPE_MINMAX)
        {
         if(curr_frac_type==EXTREMUM_TYPE_MIN)
           {
            //--- 1-� �������
            if(!is_1_point)
              {
               times[0]=curr_point_time;
               prices[0]=curr_point_price;
               is_1_point=true;
              }
            //--- 2-�� �������
            else
              {
               times[1]=curr_point_time;
               prices[1]=curr_point_price;
               //is_1_point=true;
              }
           }
         //--- ������������ ��������
         else
           {
            times[2]=curr_point_time;
            prices[2]=curr_point_price;
           }
        }
      else if(curr_set_type==SET_TYPE_MAXMIN)
        {
         if(curr_frac_type==EXTREMUM_TYPE_MAX)
           {
            //--- 1-� �������� 
            if(!is_1_point)
              {
               times[0]=curr_point_time;
               prices[0]=curr_point_price;
               is_1_point=true;
              }
            //--- 2-�� ��������
            else
              {
               times[1]=curr_point_time;
               prices[1]=curr_point_price;
              }
           }
         //--- ������������ �������
         else
           {
            times[2]=curr_point_time;
            prices[2]=curr_point_price;
           }
        }
     }
//--- �������� � ��������� ������
   int ch_num=m_channels_arr.Total();
   string ch_name="myChannel"+IntegerToString(ch_num+1);
   CChartObjectChannel  *ptr_new_channel=new CChartObjectChannel;
   if(CheckPointer(ptr_new_channel)==POINTER_DYNAMIC)
     {
      //--- �������� ������ ������ � ������
      if(!m_channels_arr.Add(ptr_new_channel))
        {
         Print("������ ���������� ������� ������!");
         delete ptr_new_channel;
         return false;
        }
      //--- 1) ���������� �������
      //--- ������ ������
      int first_date_idx=ArrayMinimum(times);
      if(first_date_idx<0)
        {
         Print("������ ��������� ���������� �������!");
         m_channels_arr.Delete(m_channels_arr.Total()-1);
         return false;
        }
      datetime first_point_date=times[first_date_idx];
      //--- ����� ������
      datetime dates[];
      if(CopyTime(_Symbol,_Period,0,1,dates)!=1)
        {
         Print("������ ��������� ������� ���������� ����!");
         m_channels_arr.Delete(m_channels_arr.Total()-1);
         return false;
        }
      datetime last_point_date=dates[0];

      //--- 2) ���������� ���
      //--- 2.1 ������ �����
      //--- ����� ����� 1-�� � 2-�� �������
      datetime bars_dates[];
      int bars_between=CopyTime(_Symbol,_Period,
                                times[0],times[1],bars_dates
                                );
      if(bars_between<2)
        {
         Print("������ ��������� ����� ����� ����� �������!");
         m_channels_arr.Delete(m_channels_arr.Total()-1);
         return false;
        }
      bars_between-=1;
      //--- ����� ������������
      double price_differential=MathAbs(prices[0]-prices[1]);
      //--- ������� �������� (������� ��������� �� 1-�� ����)
      double price_speed=price_differential/bars_between;
      //--- ����������� ������
      bool is_up=(prices[0]<prices[1]);

      //--- 2.2 ����� ���� 1-� ��� 3-�� �����  
      if(times[0]!=times[2])
        {
         datetime start,end;
         start=times[0];
         end=times[2];
         //--- ���� 3-�� ����� ������ ��� 1-��
         bool is_3_point_earlier=false;
         if(times[2]<times[0])
           {
            start=times[2];
            end=times[0];
            is_3_point_earlier=true;
           }
         //--- ����� ����� 1-�� � 3-�� �������
         int bars_between_1_3=CopyTime(_Symbol,_Period,
                                       start,end,bars_dates
                                       );
         if(bars_between_1_3<2)
           {
            Print("������ ��������� ����� ����� ����� �������!");
            m_channels_arr.Delete(m_channels_arr.Total()-1);
            return false;
           }
         bars_between_1_3-=1;

         //--- ���� ���������� �����
         if(is_up)
           {
            //--- ���� 3-�� ����� ���� ������
            if(is_3_point_earlier)
               prices[0]-=(bars_between_1_3*price_speed);
            else
               prices[2]-=(bars_between_1_3*price_speed);
           }
         //--- ��� ���� ���������� �����
         else
           {
            //--- ���� 3-�� ����� ���� ������
            if(is_3_point_earlier)
               prices[0]+=(bars_between_1_3*price_speed);
            else
               prices[2]+=(bars_between_1_3*price_speed);
           }
        }

      //--- 2.3 ����� ���� 2-�� ����� 
      if(times[1]<last_point_date)
        {
         datetime dates_for_last_bar[];
         //--- ����� ����� 2-�� ������ � ��������� �����
         bars_between=CopyTime(_Symbol,_Period,times[1],last_point_date,dates_for_last_bar);
         if(bars_between<2)
           {
            Print("������ ��������� ����� ����� ����� �������!");
            m_channels_arr.Delete(m_channels_arr.Total()-1);
            return false;
           }
         bars_between-=1;
         //--- ���� ���������� �����
         if(is_up)
            prices[1]+=(bars_between*price_speed);
         //--- ��� ���� ���������� �����
         else
            prices[1]-=(bars_between*price_speed);
        }

      //--- �������� ���������� ������� 
      times[0]=times[2]=first_point_date;
      times[1]=last_point_date;
      //--- ������������ ���
      for(int idx=0;idx<m_fractal_num;idx++)
         prices[idx]=NormalizeDouble(prices[idx],_Digits);

      //--- �������� ������
      if(!ptr_new_channel.Create(0,ch_name,0,times[0],prices[0],times[1],prices[1],
         times[2],prices[2]))
        {
         Print("������ �������� ������� ������!");
         m_channels_arr.Delete(m_channels_arr.Total()-1);
         return false;
        }
      ptr_new_channel.RayRight(m_is_array);
      ptr_new_channel.SetInteger(OBJPROP_WIDTH,m_line_wid);
      int color_idx=ch_num%4;
      if(color_idx<ArraySize(m_channel_colors))
         ptr_new_channel.Color(m_channel_colors[color_idx]);

      //--- ������� ������ �����
      if(!this.Crop(1)) // Delete(0)
        {
         Print("������ �������� 1-�� ����� � ������!");
         this.Clear();
         return false;
        }
      //--- ���� ������������ ���������� ������
      if(m_to_delete_prev || m_is_array)
         for(int ch_idx=0;ch_idx<ch_num;ch_idx++)
           {
            CChartObjectChannel  *ptr_curr_channel=m_channels_arr.At(ch_idx);
            if(CheckPointer(ptr_curr_channel)==POINTER_DYNAMIC)
              {
               //--- ���� �������� ���������� ������
               if(m_to_delete_prev)
                 {
                  if(ptr_curr_channel.GetInteger(OBJPROP_TIMEFRAMES)>0)
                     ptr_curr_channel.SetInteger(OBJPROP_TIMEFRAMES,0);
                 }
               //--- ��� ���� ������ ��� ������
               else if(m_is_array)
                  ptr_curr_channel.RayRight(false);
              }
           }
      //--- ����������� �����
      ChartRedraw();
     }
//---
   return true;
  }
//+------------------------------------------------------------------+
//| �������� ������                                                  |
//+------------------------------------------------------------------+
bool CFractalSet::Crop(const uint _num_to_crop)
  {
//--- ���� �� ������ ����� ��������� �����
   if(_num_to_crop<1)
      return false;
//---
   if(!this.DeleteRange(0,_num_to_crop-1))
     {
      Print("������ �������� ����������� �����!");
      return false;
     }
//---
   return true;
  }
//+------------------------------------------------------------------+
//| ����������� ����������                                           |
//+------------------------------------------------------------------+
void CFractalSet::BubbleSort(void)
  {
   m_sort_mode=-1;
//---
   uint arr_size=this.Total();
   for(uint passes=0;passes<arr_size-1;passes++)
      for(uint j=0;j<(arr_size-passes-1);j++)
        {
         CFractalPoint *ptr_p1=this.At(j);
         CFractalPoint *ptr_p2=this.At(j+1);
         //--- �������� �������� (�� ����)
         if(ptr_p1.Date()>ptr_p2.Date())
           {
            CFractalPoint *ptr_temp_p=new CFractalPoint;
            if(CheckPointer(ptr_temp_p)==POINTER_DYNAMIC)
              {
               ptr_temp_p.Copy(ptr_p2);
               if(!this.Insert(ptr_temp_p,j))
                  return;
               if(!this.Delete(j+2))
                  return;
              }
            else
               return;
           }
        }
   m_sort_mode=1;
  }
//+------------------------------------------------------------------+
