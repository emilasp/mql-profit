//+------------------------------------------------------------------+
//|                                                         Test.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window

#property indicator_buffers 2
#property indicator_plots 1

#property indicator_label1  "Color MA"
#property indicator_type1   DRAW_COLOR_LINE
#property indicator_color1  clrBlue,clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2

input int MAPeriod=22;

int ind=0;
double ma[];
double col[];


//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   SetIndexBuffer(0,ma,INDICATOR_DATA);
   SetIndexBuffer(1,col,INDICATOR_COLOR_INDEX);
   ind=iMA(NULL,0,MAPeriod,0,0,0);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---


      CopyBuffer(ind,0,0,rates_total-1,ma);

     for(int i=prev_calculated;i<=rates_total-1;i++)
     {
      if(ma[i]>ma[i+1])
        {
         col[i]=1;
        }
      if(ma[i]<ma[i+1])
        {
         col[i]=0;
        }
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
